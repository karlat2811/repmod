sap.ui.define([
    "abaRegressivePlanning/controller/BaseController",
    "sap/m/Button",
    "sap/m/Dialog",
    "sap/m/Label",
    "sap/m/MessageToast",
    "sap/m/Text",
    "sap/m/TextArea",
    "sap/ui/core/mvc/Controller",
    "sap/ui/layout/HorizontalLayout",
    "sap/ui/layout/VerticalLayout",
    "sap/ui/model/json/JSONModel"
], function (BaseController, Button, Dialog, Label, MessageToast, Text, TextArea, Controller, HorizontalLayout, VerticalLayout, JSONModel) {
    "use strict";

    return BaseController.extend("abaRegressivePlanning.controller.Master", {
        /**
         * Function to be fired when the controller is initialized
         */
        onInit: function () {
            var aja;
            let that = this;
            $.ajax({
                type: "GET",
                contentType: "application/json",
                url: "http://localhost:3009" + "/scenario/findAllScenario",
                dataType: "json",
                async: true,
                success: function (data) {
                    let dataModel = that.getView().getModel("data");
                    dataModel.setProperty("/scenarios", data.data);
                },
                error: function (request, status, error) {
                    console.log("Read failed");
                }
            });
            this.getRouter().getRoute("master")
                .attachPatternMatched(this._onMasterMatched, this);
        },
        dialog: null,
        /**
         * Function to be fired when the route 'master' is matched
         * @param {Object} oEvent event with the searchbox
         */
        _onMasterMatched: function (oEvent) {
            console.log("on master matched");
        },
        onSelectionChange: function (oEvent) {
            this.cleanFields();
            let dataModel = this.getModel("data");
            let listType = dataModel.getProperty("/listType");
            var oModel = null, oModel2;
            let that = this;
            if (listType == "SingleSelectMaster") {
                let object = oEvent.getParameters().listItem.getBindingContext("data").getObject();
                //todo hacer popup que muestre años
                let start = oEvent.getParameters().listItem.getBindingContext("data").getObject().date_start.substr(0, 4);
                let end = oEvent.getParameters().listItem.getBindingContext("data").getObject().date_end.substr(0, 4);
                let dates = [];
                for (let i = start; i <= end; i++) {
                    dates.push({"year": parseInt(i.toString())});
                }
                this.getModel("data").setProperty("/dates", dates);
                let oItemTemplate = new sap.ui.core.Item({
                    key: "{data>year}",
                    text: "{data>year}"
                });
                var erLabel = new Label({"text": "Escenario: "});
                var erLabel2 = new Label({"text": object.name});
                this.getModel("data").setProperty("/idSelected", object.scenario_id);
                var combo3 = new sap.m.MultiComboBox({
                    placeholder: "Año(s)",
                    items: {
                        path: "data>/dates",
                        template: oItemTemplate
                    },
                    selectedKeys: {
                        path: "data>/selectedDates",
                        template: "{selected}"
                    },
                    change: function (oControlEvent) {
                        jQuery.sap.log.info("Event fired: \"change\" value property to " + oControlEvent.getParameter("value") + " on "
                            + this);
                    },
                    selectionChange: function (oControlEvent) {
                        jQuery.sap.log.info("Event fired: \"selectionChange\" value property to " + oControlEvent.getParameter("changedItem")
                            + " with selected=" + oControlEvent.getParameter("selected") + " on " + this);
                    },
                    selectionFinish: function (oControlEvent) {
                        jQuery.sap.log.info("Event fired: \"selectionFinish\" value property to " + oControlEvent.getParameter("selectedItems")
                            + " on " + this);
                    }
                });
                this.dialog = new Dialog({
                    title: "Elija los años del escenario que desea evaluar",
                    type: "Message",
                    content: [erLabel, erLabel2, combo3],
                    beginButton: new Button({
                        text: "Ok",
                        press: function () {
                            let selectedDates = this.getModel("data").getProperty("/selectedDates");
                            let initialDate = this.getModel("data").getProperty("/dates")[0].year;
                            let idSelected = this.getModel("data").getProperty("/idSelected");
                            //ahora los mando a backend junto al id del scenario seleccionado
                            let stats;
                            // //solicito respuesta
                            $.ajax({
                                type: "POST",
                                contentType: "application/json",
                                url: "http://localhost:3009/abaResultsGeneration",
                                dataType: "json",
                                data: JSON.stringify({
                                    "idScenario": idSelected,
                                    "years": selectedDates,
                                    "initialYear": initialDate
                                }),
                                async: false,
                                success: function (data) {
                                    oModel = new JSONModel(data.data);
                                    oModel2 = new JSONModel(data.data);
                                    stats = data.stats;
                                },
                                error: function (request) {
                                    var msg = request.statusText;
                                    that.onToast("Error: " + "El escenario no tiene datos cargados");
                                    console.log("Read failed: ", request);
                                }
                            });

                            let tempModel = new Array();
                            let tempObj = {
                                "scenario": object.name,
                                "text": stats[0].text,
                                "quantity": stats[0].quantity
                            };
                            let tempObj2 = {
                                "scenario": object.name,
                                "text": stats[1].text,
                                "quantity": stats[1].quantity
                            };
                            tempModel.push(tempObj);
                            tempModel.push(tempObj2);
                            this.getModel("data").setProperty("/stats", tempModel);
                            // let that = this;
                            let i = 1;
                            oModel.oData.equivalents.forEach(function (element) {
                                //aqui estoy en el primer nivel
                                //buscar en segundo mapa
                                //el omodel2 puede ser reasignable en funcion a la cantidad de escenarios
                                oModel2.oData.equivalents.forEach(function (element3) {
                                    //aqui estoy en el primer nivel
                                    if (element.id == element3.id) {
                                        element["quantity" + i] = element3.quantity;
                                        //agregar cantidad
                                    } else {
                                        element3.equivalents.forEach(function (element4) {
                                            //aqui estoy en el primer nivel
                                            //buscar en segundo mapa
                                            if (element.id == element4.id) {
                                                //agregar cantidad
                                                element["quantity" + i] = element4.quantity;
                                            }
                                        });
                                    }
                                });
                                //aqui busco en el segundo nivel del primer mapa
                                element.equivalents.forEach(function (element2) {
                                    //aqui estoy en el primer nivel
                                    //buscar en segundo mapa
                                    oModel2.oData.equivalents.forEach(function (element3) {
                                        //aqui estoy en el primer nivel
                                        if (element2.id == element3.id) {
                                            //agregar cantidad
                                            element2["quantity" + i] = element3.quantity;
                                        } else {
                                            element3.equivalents.forEach(function (element4) {
                                                //aqui estoy en el primer nivel
                                                //buscar en segundo mapa
                                                if (element2.id == element4.id) {
                                                    //agregar cantidad
                                                    element2["quantity" + i] = element4.quantity;
                                                }
                                            });
                                        }
                                    });
                                });
                            });
                            this.getModel("data").setProperty("/columns/columnName" + 1, "Macroelementos");
                            this.getModel("data").setProperty("/columns/visible" + 1, true);
                            this.getModel("data").setProperty("/columns/columnName" + 2, object.name);
                            this.getModel("data").setProperty("/columns/visible" + 2, true);
                            this.getModel("data").setProperty("/tableInfo", oModel.getData());
                            // MessageToast.show('Submit pressed!');
                            this.dialog.close();
                        }.bind(this)
                    }),
                    endButton: new Button({
                        text: "Cancelar",
                        press: function () {
                            this.dialog.close();
                            // dialog.destroy();
                        }.bind(this)
                    }),
                    afterClose: function () {
                        //todo
                        // this.dialog.destroy();
                    }
                }
                );
                if (oModel != null) {
                    this.getModel("data").setProperty("/tableInfo", oModel.getData());
                }
                this.getView().addDependent(this.dialog);
                this.getView().byId("scenarioList").removeSelections(true);
                this.dialog.open();
            }
        },
        activateComparation: function (oEvent) {
            let dataModel = this.getModel("data");
            let listType = dataModel.getProperty("/listType");
            if (listType == "SingleSelectMaster") {
                dataModel.setProperty("/listType", "MultiSelect");
                dataModel.setProperty("/compareButton/text", "Cancelar");
                dataModel.setProperty("/compareButton/type", "Reject");
                dataModel.setProperty("/calculateButton/visible", true);
            } else {
                dataModel.setProperty("/listType", "SingleSelectMaster");
                dataModel.setProperty("/compareButton/text", "Comparar");
                dataModel.setProperty("/compareButton/type", "Emphasized");
                dataModel.setProperty("/calculateButton/visible", false);

            }
        },
        calculate: function (oEvent) {
            this.cleanFields();
            var oModel = null, oModel2;
            let selected = this.getView().byId("scenarioList").getSelectedItems();
            let arrayOfScenariosAndDates = new Array();

            //activa columna de macroelementos
            for (let i = 0; i < selected.length; i++) {
                let tempPath = this.getView().byId("scenarioList").getSelectedItems()[i].getBindingContext("data").getPath();
                let actualObject = this.getModel("data").getProperty(tempPath);
                //genero años
                let start = actualObject.date_start.substr(0, 4);
                let end = actualObject.date_end.substr(0, 4);
                let dates = [];
                for (let i = start; i <= end; i++) {
                    dates.push({"year": parseInt(i.toString())});
                }
                //armo el objeto
                let tempObject =
                    {
                        "id": actualObject.scenario_id,
                        "name": actualObject.name,
                        "dates": dates,
                        "selectedDates": []
                    };
                arrayOfScenariosAndDates.push(tempObject);
                //debo colocar el nombre de la columna +1
                this.getModel("data").setProperty("/columns/columnName" + (i + 2), actualObject.name);
            }
            this.getModel("data").setProperty("/scenariosAndDates", arrayOfScenariosAndDates);
            //para cada uno de los escenarios genero
            let arrayOfObjects = new Array();
            for (let i = 0; i < selected.length; i++) {
                let oItemTemplate = new sap.ui.core.Item({
                    key: "{data>year}",
                    text: "{data>year}"
                });
                var erLabel = new Label({"text": "Escenario: "});
                console.log(selected);
                var erLabel2 = new Label({"text": selected[i].mProperties.title});
                var combo3 = new sap.m.MultiComboBox({
                    placeholder: "Año(s)",
                    items: {
                        path: "data>/scenariosAndDates/" + i.toString() + "/dates",
                        template: oItemTemplate
                    },
                    selectedKeys: {
                        path: "data>/scenariosAndDates/" + i.toString() + "/selectedDates",
                        template: "{selected}"
                    },
                    change: function (oControlEvent) {
                        jQuery.sap.log.info("Event fired: \"change\" value property to " + oControlEvent.getParameter("value") + " on "
                            + this);
                    },
                    selectionChange: function (oControlEvent) {
                        jQuery.sap.log.info("Event fired: \"selectionChange\" value property to " + oControlEvent.getParameter("changedItem")
                            + " with selected=" + oControlEvent.getParameter("selected") + " on " + this);
                    },
                    selectionFinish: function (oControlEvent) {
                        //verificar el modelo con los selectedDates, y mandar a backend
                        // jQuery.sap.log.info('Event fired: "selectionFinish" value property to ' + oControlEvent.getParameter("selectedItems")
                        //     + " on " + this);
                    }
                });
                arrayOfObjects.push(erLabel, erLabel2, combo3);
            }
            var stats;
            // var that = this;
            this.dialog = new Dialog({
                title: "Elija los años del escenario que desea evaluar",
                type: "Message",
                content: arrayOfObjects,
                beginButton: new Button({
                    text: "Ok",
                    press: function () {
                        this.getModel("data").setProperty("/columns/columnName" + 1, "Macroelementos");
                        this.getModel("data").setProperty("/columns/visible" + 1, true);
                        for (let i = 0; i < selected.length; i++) {
                            this.getModel("data").setProperty("/columns/visible" + (i + 2), true);
                        }
                        let tempInfo = this.getModel("data").getProperty("/scenariosAndDates");
                        let selectedDates = this.getModel("data").getProperty("/scenariosAndDates/" + 0 + "/selectedDates");
                        let initialDate = this.getModel("data").getProperty("/scenariosAndDates/" + 0 + "/dates")[0].year;
                        let idSelected = this.getModel("data").getProperty("/scenariosAndDates/" + 0 + "/id");
                        let actualName = this.getModel("data").getProperty("/scenariosAndDates/" + 0 + "/name");
                        //para cada uno de los datos enviar a backend años y id del scenario
                        let that = this;
                        $.ajax({
                            type: "POST",
                            contentType: "application/json",
                            url: "http://localhost:3009/abaResultsGeneration",
                            dataType: "json",
                            data: JSON.stringify({
                                "idScenario": idSelected,
                                "years": selectedDates,
                                "initialYear": initialDate
                            }),
                            async: false,
                            success: function (data) {
                                oModel = new JSONModel(data.data);
                                stats = data.stats;
                            },
                            error: function (request) {
                                var msg = request.statusText;
                                that.onToast("Error: " + "El escenario no tiene datos cargados");
                                console.log("Read failed: ", request);
                            }
                        });
                        //aqui añado el primero
                        let tempModel = new Array();
                        let tempObj = {
                            "scenario": actualName,
                            "text": stats[0].text,
                            "quantity": stats[0].quantity
                        };
                        let tempObj2 = {
                            "scenario": actualName,
                            "text": stats[1].text,
                            "quantity": stats[1].quantity
                        };
                        tempModel.push(tempObj);
                        tempModel.push(tempObj2);
                        this.getModel("data").setProperty("/stats", tempModel);
                        //ya esta listo el primer elemento ahora continuo
                        for (let j = 1; j < tempInfo.length; j++) {
                            //para cada uno de los datos enviar a backend años y id del scenario
                            let selectedDates = this.getModel("data").getProperty("/scenariosAndDates/" + j + "/selectedDates");
                            let initialDate = this.getModel("data").getProperty("/scenariosAndDates/" + j + "/dates")[0].year;
                            let idSelected = this.getModel("data").getProperty("/scenariosAndDates/" + j + "/id");
                            let actualName = this.getModel("data").getProperty("/scenariosAndDates/" + j + "/name");
                            //ahora los mando a backend junto al id del scenario seleccionado
                            // //solicito respuesta
                            $.ajax({
                                type: "POST",
                                contentType: "application/json",
                                url: "http://localhost:3009/abaResultsGeneration",
                                dataType: "json",
                                data: JSON.stringify({
                                    "idScenario": idSelected,
                                    "years": selectedDates,
                                    "initialYear": initialDate
                                }),
                                async: false,
                                success: function (data) {
                                    //todo reemplazar mas adelante
                                    oModel2 = new JSONModel(data.data);
                                    stats = data.stats;
                                },
                                error: function (request) {
                                    var msg = request.statusText;
                                    that.onToast("Error: " + "El escenario no tiene datos cargados");
                                    console.log("Read failed: ", request);
                                }
                            });
                            //aqui añado el primero
                            let tempModel = this.getModel("data").getProperty("/stats");
                            let tempObj = {
                                "scenario": actualName,
                                "text": stats[0].text,
                                "quantity": stats[0].quantity
                            };
                            let tempObj2 = {
                                "scenario": actualName,
                                "text": stats[1].text,
                                "quantity": stats[1].quantity
                            };
                            tempModel.push(tempObj);
                            tempModel.push(tempObj2);
                            this.getModel("data").setProperty("/stats", tempModel);
                            this.getModel("data").setProperty("/scenariosAndDates", arrayOfScenariosAndDates);
                            // let z = 1;
                            oModel.oData.equivalents.forEach(function (element) {
                                //aqui estoy en el primer nivel
                                //buscar en segundo mapa
                                //el omodel2 puede ser reasignable en funcion a la cantidad de escenarios
                                oModel2.oData.equivalents.forEach(function (element3) {
                                    //aqui estoy en el primer nivel
                                    if (element.id == element3.id) {
                                        element["quantity" + j] = element3.quantity;
                                        //agregar cantidad
                                    } else {
                                        element3.equivalents.forEach(function (element4) {
                                            //aqui estoy en el primer nivel
                                            //buscar en segundo mapa
                                            if (element.id == element4.id) {
                                                //agregar cantidad
                                                element["quantity" + j] = element4.quantity;
                                            }
                                        });
                                    }
                                });
                                //aqui busco en el segundo nivel del primer mapa
                                element.equivalents.forEach(function (element2) {
                                    //aqui estoy en el primer nivel
                                    //buscar en segundo mapa
                                    oModel2.oData.equivalents.forEach(function (element3) {
                                        //aqui estoy en el primer nivel
                                        if (element2.id == element3.id) {
                                            //agregar cantidad
                                            element2["quantity" + j] = element3.quantity;
                                        } else {
                                            element3.equivalents.forEach(function (element4) {
                                                //aqui estoy en el primer nivel
                                                //buscar en segundo mapa
                                                if (element2.id == element4.id) {
                                                    //agregar cantidad
                                                    element2["quantity" + j] = element4.quantity;
                                                }
                                            });
                                        }
                                    });
                                });
                            });
                        }
                        this.getModel("data").setProperty("/tableInfo", oModel.getData());
                        this.getModel("data").getProperty("/stats");
                        // MessageToast.show('Submit pressed!');
                        this.dialog.close();
                    }.bind(this)
                }),
                endButton: new Button({
                    text: "Cancelar",
                    press: function () {
                        this.dialog.close();
                        // dialog.destroy();
                    }.bind(this)
                }),
                afterClose: function () {
                    //todo
                    // this.dialog.destroy();
                }
            }
            );
            this.getView().addDependent(this.dialog);
            this.dialog.open();
        },
        cleanFields: function () {
            let columnsQuantity = 5;
            this.getModel("data").setProperty("/stats", []);
            for (let i = 1; i <= columnsQuantity; i++) {
                this.getModel("data").setProperty("/columns/columnName" + i, "");
                this.getModel("data").setProperty("/columns/visible" + i, false);
            }
        }
    });
}, /* bExport= */ true);
