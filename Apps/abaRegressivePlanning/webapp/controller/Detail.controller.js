sap.ui.define([
    "abaRegressivePlanning/controller/BaseController",
    "jquery.sap.global",
    "sap/ui/model/json/JSONModel",
    "sap/ui/core/mvc/Controller"
], function (BaseController, jQuery, JSONModel, Controller) {
    "use strict";

    return BaseController.extend("abaRegressivePlanning.controller.Detail", {
        /**
         * Called when a controller is instantiated and its View controls (if available) are already created.
         * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
         * @memberOf MasterDetail.view.Detail
         */
        tableExist: false,
        onInit: function () {
            this.getModel("data");
            this.getRouter().getRoute("master").attachMatched(this._onRouteMatched, this);
            console.log("aqui");
        },
        showPopOver: function (oEvent) {
            //coloco en el popUp el valor correspondiente, deberia de estar en stats
            let stats = this.getModel("data").getProperty("/stats");
            let actualScenarioName = oEvent.oSource.mProperties.text;
            //busco en stats a con nombre igual al anterior
            let toShow = stats.filter(data => data.scenario == actualScenarioName);
            this.getModel("data").setProperty("/popOverData/scenarioName",
                actualScenarioName);
            this.getModel("data").setProperty("/popOverData/criaYLevante",
                toShow[0].text + ": " + toShow[0].quantity);
            this.getModel("data").setProperty("/popOverData/engorde",
                toShow[1].text + ": " + toShow[1].quantity);
            // create popover
            if (!this._oPopover) {
                this._oPopover = sap.ui.xmlfragment("abaRegressivePlanning.view.Popover", this);
                this.getView().addDependent(this._oPopover);
            }
            this._oPopover.openBy(oEvent.getSource());
        },
        onCollapseAll: function() {
            var oTreeTable = this.byId("TreeTableBasic");
            oTreeTable.collapseAll();
        },
        onCollapseSelection: function() {
            var oTreeTable = this.byId("TreeTableBasic");
            oTreeTable.collapse(oTreeTable.getSelectedIndices());
        },
        onExpandFirstLevel: function() {
            var oTreeTable = this.byId("TreeTableBasic");
            oTreeTable.expandToLevel(1);
        },
        onExpandSelection: function() {
            var oTreeTable = this.byId("TreeTableBasic");
            oTreeTable.expand(oTreeTable.getSelectedIndices());
        },
        /**
         * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
         * (NOT before the first rendering! onInit() is used for that one!).
         * @memberOf MasterDetail.view.Detail
         */
        _onRouteMatched: function (oEvent) {
            console.log("on route matched");
        },
        goToLaunchpad: function(){
            var dummy = this.getView().getModel("dummy");
            window.location.href = "/Apps/launchpad/webapp";
        }
    });
});
