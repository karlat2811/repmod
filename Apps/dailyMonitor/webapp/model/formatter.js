sap.ui.define([], function() 
{
    "use strict";

    return {
        calculateResidue: function(projected_quantity, residue) 
        {
            return (projected_quantity-residue).toLocaleString();
        },
        formatMiles: function(number)
        {
            return (number !== "" && !isNaN(number)) ? parseInt(number).toLocaleString() : number;
        },
    };
});
