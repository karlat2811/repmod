sap.ui.define([], function() {
    "use strict";

    return {
        formatApps: function(app){

            switch (app) {
            case "technicalConfiguration": 
                return ("Configuracion tecnica");
            case "calendario": 
                return ("Calendario");
            case "mantenimiento-escenarios":
                return ("Mantenimiento de escenario");
            case "liftBreedingPlanningM":
                return ("Levante y cria");
            case "breedingPlanningM":
                return ("Reproductora");
            case "annualPostureCurve":
                return ("Curva anual de Postura");
            case "incubatorPlanningM": 
                return ("Incubadora");
            case "broilersPlanningM":
                return ("Engorde");
            case "broilerEviction": 
                return ("Desalojo");
            case "userManagement":
                return ("Gestion de Usuarios");
            case "dailyMonitor": 
                return ("Monitor diario");
            case "synchronizer":
                return ("Sincronizacion");
            case "dataImport":
                return ("Carga Masiva de Materiales");
            case "coldRoom":
                return ("Cuarto Frio");
            }
        }

    };
});