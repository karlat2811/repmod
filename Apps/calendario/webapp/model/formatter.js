sap.ui.define([
], function () {
    "use strict";

    return {
        /**
			 * Rounds the currency value to 2 digits
			 *
			 * @public
			 * @param {string} sValue value to be formatted
			 * @returns {string} formatted currency value with 2 digits
			 */
        status : function (sValue) {
            if (sValue === 1) {
                return "Success";
            }
            else {
                return "None";
            }
        },
        statusText: function(sValue) {
            if (sValue) {
                return "Generado";
            }
            else {
                return "";
            }
        },
        visibleItem: function(sValue) {
            console.log(this.getView().getModel("calendar").getProperty("/calendarSelected/code"));
            if($.trim(sValue) !== $.trim(this.getView().getModel("calendar").getProperty("/calendarSelected/code"))){
                return true;
            }
            return false;
        },
        statusHeader: function(sValue) {
            if (sValue) {
                return false;
            }
            else {
                return true;
            }
        },
        dateFormat: function(day, month, year) {
            if(year !== 0) {
                return day + " / " + this._months[month] + " / " + year;
            }
            else {
                return day + " / " + this._months[month];
            }
        }
    };

}
);
