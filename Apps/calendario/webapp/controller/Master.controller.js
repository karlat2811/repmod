sap.ui.define([
    "MasterDetail/controller/BaseController",
    "sap/ui/model/Filter",
    "sap/m/Dialog",
    "sap/m/Button",
    "sap/m/Text",
    "MasterDetail/model/formatter",
    "MasterDetail/controller/MasterUserAppController"
], function (BaseController, Filter, Dialog, Button, Text, formatter, MasterUserpController) {
    "use strict";

    return BaseController.extend("MasterDetail.controller.Master", {
        formatter: formatter,
        /**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf MasterDetail.view.Master
		 */
        onInit: function () {
            console.log("/////////dummy//////////////");
            console.log(document.URL);
            console.log(document.URL.split("/"));

            let prefixUrl = document.URL.split("/");
            let aprefixUrl = prefixUrl[2].split(":");

            console.log("/////////dummy//////////////");


            this.getRouter().getRoute("master").attachPatternMatched(this._onMasterMatched, this);

            this.formCalendar = sap.ui.xmlfragment("MasterDetail.view.FormCalendar", this);

            this.getView().addDependent(this.copyDialog);

        },
        _onMasterMatched: function(oEvent) {

            var that = this,
                oModel = this.getView().getModel("calendar"),
                dummy = this.getView().getModel("dummy");
            dummy.attachRequestCompleted(function() {
                console.log(document.URL.split("/"));
                let prefixUrl = document.URL.split("/");
                let aprefixUrl = prefixUrl[2].split(":");

                dummy.setProperty("/serviceUrl", "http://"+aprefixUrl[0]+":"+aprefixUrl[1]);
                console.log(dummy);
            });


            if(dummy.getProperty("/serviceUrl") !== undefined) {
                console.log("!== undefined");
                var settings = {
                    async: true,
                    crossDomain: true,
                    url: dummy.getProperty("/serviceUrl") + "/calendar/getAllCalendar",
                    method: "GET",
                    beforeSend: function( xhr ) {
							    that.byId("listCalendars").setBusy(true);
                    },
                    success: function(res) {
                        console.log(res);
                        oModel.setProperty("/calendars", res.data);
                        that.byId("listCalendars").setBusy(false);
                        //that.byId("listCalendars").setSelectedItem(that.byId("listCalendars").getItems()[0], true);
                        //that.findItemInList();
                    },
                    error: function(err) {
                        console.log(err);
                        that.onToast("Error cargando codigos de calendarios", null);
                        that.byId("listCalendars").setBusy(false);
                    }
                };
                $.ajax(settings);
            }
            else {
                dummy.attachRequestCompleted(function() {
						    that.serverUrl = dummy.getProperty("/serviceUrl");
						    that.getView().getModel("dummy").setProperty("/phone/",
                        that.getOwnerComponent().getContentDensityClass() === "sapUiSizeCozy");
                    var settings = {
                        async: true,
                        crossDomain: true,
                        url: dummy.getProperty("/serviceUrl") + "/calendar/getAllCalendar",
                        method: "GET",
                        beforeSend: function( xhr ) {
								    that.byId("listCalendars").setBusy(true);
                        },
                        success: function(res) {
                            console.log(res);
                            oModel.setProperty("/calendars", res.data);
                            that.byId("listCalendars").setBusy(false);
                        },
                        error: function(err) {
                            console.log(err);
                            that.onToast("Error cargando codigos de calendarios", null);
                            that.byId("listCalendars").setBusy(false);
                            //that.byId("listCalendars").setBusy(false);
                        }
                    };
                    $.ajax(settings);
                });
            }

        },
        onSelectionChange : function(oEvent){
            var oObject = oEvent.getSource().oBindingContexts.calendar.getObject();
            var oModel = this.getView().getModel("calendar");

            oModel.setProperty("/calendarSelected/", oObject);
            this.getRouter().navTo("detail", {
                id: oObject.code
            }, false /*create history*/ );
        },
        addCalendar: function(oEvent) {
            sap.ui.getCore().byId("inputCode").setValue("");
            sap.ui.getCore().byId("inputDescp").setValue("");
            sap.ui.getCore().byId("inputAnho1").setValue("");
            sap.ui.getCore().byId("inputAnho2").setValue("");
            this.formCalendar.open();
        },
        cancelCalendar: function(oEvent) {
            this.formCalendar.close();
        },
        saveCalendar: function(oEvent) {
            var oModel = this.getView().getModel("calendar"),
                dummy = this.getView().getModel("dummy");

            oModel.setProperty("/formCalendar/startDate", sap.ui.getCore().byId("inputAnho1").getValue());
            oModel.setProperty("/formCalendar/endDate", sap.ui.getCore().byId("inputAnho2").getValue());
            oModel.setProperty("/formCalendar/code", sap.ui.getCore().byId("inputCode").getValue());
            oModel.setProperty("/formCalendar/description", sap.ui.getCore().byId("inputDescp").getValue());
            var yearStart = parseInt(oModel.getProperty("/formCalendar/startDate"),10),
				    yearEnd = oModel.getProperty("/formCalendar/endDate"),
				    code = oModel.getProperty("/formCalendar/code"),
				    descp = oModel.getProperty("/formCalendar/description"),
				    that = this;
            yearEnd = ($.trim(yearEnd) === "") ? 0 : parseInt(yearEnd,10);

            if(($.trim(code) !== "" && $.trim(descp) !== "" && yearStart > 0 && (yearEnd === NaN || yearEnd >= 0)) &&
				   (yearStart > 1999 && yearStart < 3000 && (yearEnd > 1999 && yearEnd < 3000 || (yearEnd === NaN || yearEnd === 0))) &&
				   (yearStart <= yearEnd || (yearEnd === NaN || yearEnd === 0)) ){
				  	oModel.setProperty("/formCalendar/code", code.toUpperCase());

                if(yearEnd === NaN || yearEnd === 0) yearEnd = yearStart;

                //oModel.setProperty("/formCalendar/initWeek", parseInt(sap.ui.getCore().byId("initWeek").getSelectedKey(), 10));
                /*sap.ui.getCore().byId("calendar1").setNonWorkingDays([parseInt(sap.ui.getCore().byId("sundayOff").getSelectedKey(),10),
																parseInt(sap.ui.getCore().byId("saturdayOff").getSelectedKey(),10)]);*/
					
                // "saturday": (parseInt(sap.ui.getCore().byId("saturdayOff").getSelectedKey(),10) === -1 ? "Laborable" : "NO Laborable"),
				    // "sunday": (parseInt(sap.ui.getCore().byId("sundayOff").getSelectedKey(),10) === -1 ? "Laborable" : "NO Laborable"),
				    // "week_start": (parseInt(sap.ui.getCore().byId("initWeek").getSelectedKey(),10) === 0 ? "Sunday" : "Monday"),

                var oObject = {
                    "description": descp,
					    "saturday":  "Laborable",
					    "sunday": "Laborable",
					    "week_start": (parseInt(sap.ui.getCore().byId("initWeek").getSelectedKey(),10) === 0 ? "Sunday" : "Monday"),
					    "code": $.trim(code.toUpperCase()),
					    "year_start": parseInt(yearStart,10),
					    "year_end": parseInt(yearEnd,10)
                };
                sap.ui.getCore().byId("formCalendar").setBusy(true);
                var settings = {
                    async: true,
                    crossDomain: true,
                    url: dummy.getProperty("/serviceUrl") + "/calendar/",
                    method: "POST",
                    data: oObject,
                    success: function(res) {
                        sap.ui.getCore().byId("formCalendar").setBusy(false);
                        var dialog;
                        if(res.status === 200) {
                            oModel.setProperty("/calendar_id", res.calendarId.calendar_id);
                            oObject.calendar_id = oModel.getProperty("/calendar_id");
                            oObject.generated = 0;
                            console.log(oObject);
                            oModel.setProperty("/calendarSelected/", oObject);
                            dialog = new Dialog({
                                title: "Información",
                                type: "Message",
                                state: "Success",
                                content: new Text({
                                    text: "Calendario guardado con éxito."
                                }),
                                beginButton: new Button({
                                    text: "OK",
                                    press: function () {
                                        dialog.close();
                                        that._onMasterMatched(); // ACTUALIZA DATOS DEL MASTER
                                        // that.getRouter().navTo("detail", {
                                        // 	id: oObject.code
                                        // }, false);
                                        that.formCalendar.close();
                                    }
                                }),
                                afterClose: function() {
                                    dialog.destroy();
                                }
                            });

                            dialog.open();
                        }
                        else if(res.mgs === "Existe") {
                            dialog = new Dialog({
                                title: "Aviso",
                                type: "Message",
                                state: "Warning",
                                content: new Text({
                                    text: "El código asignado al calendario que intenta crear ya se encuentra registrado."
                                }),
                                beginButton: new Button({
                                    text: "OK",
                                    press: function () {
                                        dialog.close();
                                    }
                                }),
                                afterClose: function() {
                                    dialog.destroy();
                                }
                            });

                            dialog.open();
                        }
                    },
                    error: function(err) {
                        console.log(err);
                        sap.ui.getCore().byId("formCalendar").setBusy(false);
                        that.onToast("Error al guardar calendario", null);
                    }
                };
                $.ajax(settings);
            }
            else if($.trim(code) === "" || $.trim(descp) === "" || $.trim(yearStart) === "") {
                this.onToast("Existen campos que deben ser cargados", null);
            }
            else if(yearStart.length !== 4 && yearEnd !== NaN && (yearEnd.length !== 4 || yearEnd.length !== 0)) {
                this.onToast("Corrija el rango de años", null);
            }
            else if(yearEnd !== 0 || yearEnd !== NaN && yearStart > yearEnd ) {
                this.onToast("Año de inicio debe ser menor", null);
            }
            else {
                this.onToast("Todos los campos son obligatorios", null);
            }
        },
        findItemInList: function() {
            var list = this.getView().byId("listCalendars").getItems();
            var oModel = this.getView().getModel("calendar");
            list.forEach(val => {
                if($.trim(val.getProperty("title")) === $.trim(oModel.getProperty("/calendarSelected/code"))) {
                    list.setSelectedItem(val, true);
                }
            });
        },
        onSearch: function(oEvent){
            var aFilters = [],
                sQuery = oEvent.getSource().getValue(),
                binding = this.getView().byId("listCalendars").getBinding("items");

            if (sQuery && sQuery.length > 0) {
                var filter1 = new sap.ui.model.Filter("code", sap.ui.model.FilterOperator.Contains, sQuery),
                    filter2 = new sap.ui.model.Filter("description", sap.ui.model.FilterOperator.Contains, sQuery);

                aFilters = new sap.ui.model.Filter([filter1, filter2]);
            }
            binding.filter(aFilters);
        },
        onDeletePress: function(oEvent) {
            console.log(this.byId("listCalendars").getMode());
            if(this.byId("listCalendars").getMode() === "Delete")
                this.byId("listCalendars").setMode("None");
            else
                this.byId("listCalendars").setMode("Delete");
        },
        handleEntryListDelete: function(oEvent){
            var that = this;
            var codeCalendar = $.trim(oEvent.getParameters().listItem.mProperties.title);
            var dialog = new Dialog({
                title: "Confirmación",
                type: "Message",
                content: new Text({
                    text: "Se procedera a eliminar calendario " + oEvent.getParameters().listItem.mProperties.title

                }),
                beginButton: new Button({
                    text: "Continuar",
                    press: function () {
                        dialog.close();
                        that.deleteCalendar(codeCalendar);
                    }
                }),
                endButton: new Button({
                    text: "Cancelar",
                    press: function () {
                        dialog.close();
                    }
                }),
                afterClose: function() {
                    dialog.destroy();
                }
            });

            dialog.open();
        },
        deleteCalendar: function(codeCalendar) {
            var that = this,
                dummy = this.getView().getModel("dummy"),
                calendar = this.getView().getModel("calendar");
            //list = this.byId("listCalendars");
            //list.unbindItems();
            var settings = {
                async: true,
                crossDomain: true,
                url: dummy.getProperty("/serviceUrl") + "/calendar",
                method: "DELETE",
                data: {
                    "code": codeCalendar
                },
                success: function(res) {
                    that._onMasterMatched();
                    that.getRouter().navTo("master");

                },
                error: function (err) {
                    console.log(err);
                    that.onToast("Error al eliminar calendario", null);
                }
            };
            $.ajax(settings);
        },
        goToLaunchpad: function () {
            // var dummy = this.getView().getModel("dummy");
            // console.log(dummy);
            // window.location.href = dummy.getProperty("/serviceUrl") + "/Apps/launchpad/webapp";
            window.location.href = "/Apps/launchpad/webapp";
        },

        validateIntInput: function (o) {
            console.log("validateIntInput");
            let input= o.getSource();
            let length = 4;
            let value = input.getValue();
            let regex = new RegExp(`/^[0-9]{1,${length}}$/`);

            if (regex.test(value)) {
                return true;
            }
            else {
                let aux = value.split("").filter(char => {
                    if (/^[0-9]$/.test(char)) {
                        if (char !== ".") {
                            return true;
                        }
                    }
                }).join("");

                value = aux.substring(0, length);
                input.setValue(value);
                return false;
            }
        }

    });

});
