sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/m/Dialog",
    "sap/m/Button",
    "sap/m/Text"
], function(Controller, Dialog, Button, Text) {
    "use strict";

    return Controller.extend("MasterDetail.controller.App", {
        onInit: function() {
            this.getView().addStyleClass(this.getOwnerComponent().getContentDensityClass());
        }
    });
});
