sap.ui.define([
    "MasterDetail/controller/BaseController",
    "sap/ui/model/json/JSONModel",
    "sap/ui/model/Filter",
    "sap/m/Dialog",
    "sap/m/Button",
    "sap/m/Text"
], function(BaseController, JSONModel, Filter, Dialog, Button, Text, formatter) {
    "use strict";
    return BaseController.extend("MasterDetail.controller.Detail", {
        formatter: formatter,
        _months : ["null", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        _monthsIndex : {
            Enero:1,
            Febrero:2,
            Marzo:3,
            Abril:4,
            Mayo:5,
            Junio:6,
            Julio:7,
            Agosto:8,
            Septiembre:9,
            Octubre:10,
            Noviembre:11,
            Diciembre:12
        },
        currentDate : {
            year: 0,
            month: 0
        },
        /**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf MasterDetail.view.Detail
		 */
        onInit: function() {
            this.editFormCalendar = sap.ui.xmlfragment("MasterDetail.view.EditFormCalendar", this);

            this.getView().addDependent(this.editFormCalendar);

            this.copyDialog = sap.ui.xmlfragment("MasterDetail.view.CopyCalendarDialog", this);

            this.getView().addDependent(this.copyDialog);

            this.getRouter().getRoute("detail").attachPatternMatched(this._onRouteMatched, this);
        },
        /**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf MasterDetail.view.Detail
		 */
        _onRouteMatched: function(oEvent) {
            var dummy = this.getView().getModel("dummy");
            var oModel = this.getView().getModel("calendar");
            dummy.setProperty("/phone/", this.getOwnerComponent().getContentDensityClass() === "sapUiSizeCozy");

            this.oModelSpecial = new JSONModel();
            this.oModelSpecial.setData({
                minDate: "",
                maxDate: "",
                special: []
            });

            console.log(oModel.getProperty("/calendarSelected"));
            this.oModelSpecial.setProperty("/minDate", new Date(oModel.getProperty("/calendarSelected/year_start"), "00", "01", "0", "0"));
            if(isNaN(parseInt(oModel.getProperty("/calendarSelected/year_end"),10))) {
                this.oModelSpecial.setProperty("/maxDate", new Date(oModel.getProperty("/calendarSelected/year_end"), "11", "31", "0", "0"));
            }
            else {
                this.oModelSpecial.setProperty("/maxDate", new Date(oModel.getProperty("/calendarSelected/year_end"), "11", "31", "0", "0"));
            }
            console.log(this.oModelSpecial.getProperty("/minDate"),  this.oModelSpecial.getProperty("/maxDate"));
            this.getView().setModel(this.oModelSpecial);
            this.getCalendarAndDays();
            //this.getView().byId("__header0").bindElement("data>/data/" + index + "/");

        },
        getCalendarAndDays: function() {
            var oModel = this.getView().getModel("calendar"),
                dummy = this.getView().getModel("dummy"),
                that = this;
            dummy.setProperty("/busy", true);
            console.log(oModel.getProperty("/calendarSelected"));
            dummy.setProperty("/visibleVbox1", false);
            dummy.setProperty("/visibleVbox2", false);
            var settings = {
                async: true,
                crossDomain: true,
                url: dummy.getProperty("/serviceUrl") + "/holiday/findHolidaysCalendar/" + oModel.getProperty("/calendarSelected/code"),
                method: "GET",
                success: function(res) {
                    dummy.setProperty("/busy", false);
                    oModel.setProperty("/specialDates", res.data);
                    oModel.setProperty("/listHolidays", res.data);
                    console.log(res.data);
                    if(res.statusCode === 200) {
                        if(!oModel.getProperty("/calendarSelected/generated")) { // No ha sido generado
                            dummy.setProperty("/visibleVbox1", true);
                            dummy.setProperty("/visibleVbox2", false);
                            that.byId("buttonAcceptTab1").setVisible(true);
                            that.byId("buttonDeleteTab1").setVisible(true);
                            that.byId("buttonAcceptTab2").setVisible(false);
                            that.byId("buttonDeleteTab2").setVisible(false);
                            that.byId("buttonCopy").setVisible(true);
                            that.byId("buttonGenerate").setVisible(true);
                            that.loadSpecialDates();

                        }
                        else {
                            console.log("entro por aca");
                            dummy.setProperty("/visibleVbox1", false);
                            dummy.setProperty("/visibleVbox2", true);
                            that.byId("listHolidays").destroyItems();
                            that.byId("buttonAcceptTab1").setVisible(false);
                            that.byId("buttonDeleteTab1").setVisible(false);
                            that.byId("buttonAcceptTab2").setVisible(false);
                            that.byId("buttonDeleteTab2").setVisible(false);
                            that.byId("buttonCopy").setVisible(false);
                            that.byId("buttonGenerate").setVisible(false);
                            that.loadSpecialDatesInCalendar(oModel.getProperty("/specialDates"));
                        }

                    }
                },
                error: function(err) {
                    console.log(err);
                    dummy.setProperty("/busy", false);
                    that.onToast("Error al obtener calendario y sus días feriados", null);
                    //that.byId("floatingFooterPage").setBusy(false);
                }
            };
            $.ajax(settings);
        },
        getCalendarAndDaysGenerated: function() {
            var oModel = this.getView().getModel("calendar"),
                dummy = this.getView().getModel("dummy"),
                that = this;
            dummy.setProperty("/busy", true);
            console.log(oModel.getProperty("/calendarSelected"));
            dummy.setProperty("/visibleVbox1", false);
            dummy.setProperty("/visibleVbox2", false);
            var settings = {
                async: true,
                crossDomain: true,
                url: dummy.getProperty("/serviceUrl") + "/holiday/findHolidaysCalendar/" + oModel.getProperty("/calendarSelected/code"),
                method: "GET",
                success: function(res) {
                    dummy.setProperty("/busy", false);
                    oModel.setProperty("/specialDates", res.data);
                    oModel.setProperty("/listHolidays", res.data);

                    dummy.setProperty("/visibleVbox1", false);
                    dummy.setProperty("/visibleVbox2", true);
                    that.byId("buttonAcceptTab1").setVisible(false);
                    that.byId("buttonDeleteTab1").setVisible(false);
                    that.byId("buttonAcceptTab2").setVisible(false);
                    that.byId("buttonDeleteTab2").setVisible(false);
                    that.byId("buttonCopy").setVisible(false);
                    that.byId("buttonGenerate").setVisible(false);
                    that.byId("listHolidays").destroyItems();
                    that.loadSpecialDatesInCalendar(oModel.getProperty("/specialDates"));

                },
                error: function(err) {
                    console.log(err);
                    dummy.setProperty("/busy", false);
                    that.onToast("Error al obtener calendario y sus días feriados", null);
                    //that.byId("floatingFooterPage").setBusy(false);
                }
            };
            $.ajax(settings);
        },
        loadSpecialDatesInCalendar: function(array) {
            var oModel = this.getView().getModel("calendar");
            console.log(oModel);
            var date = new Date(),
                yearStart = parseInt(oModel.getProperty("/calendarSelected/year_start"),10),
				    yearEnd = parseInt(oModel.getProperty("/calendarSelected/year_end"),10);
				    console.log(yearStart, yearEnd);
            var sunday = (oModel.getProperty("/calendarSelected/sunday") === "NO Laborable" ? 0 : -1),
                saturday = (oModel.getProperty("/calendarSelected/saturday") === "NO Laborable" ? 6 : -1);
            if(oModel.getProperty("/calendarSelected/week_start").search("Sunday") > -1) {
                this.byId("calendar1").setFirstDayOfWeek(0);
            }
            else if (oModel.getProperty("/calendarSelected/week_start").search("Monday") > -1){
                this.byId("calendar1").setFirstDayOfWeek(1);
            }
				    this.byId("calendar1").setNonWorkingDays([saturday, sunday]);
            oModel.setProperty("/specialDates", []);
            console.log(array);
            array.forEach(function(val, index) {
                if(val.year !== 0) {
                    console.log(val);
                    oModel.getProperty("/specialDates").push(
                        {
                            startDate : new Date( val.year,val.month - 1,val.day),
                            type : "Type02",
                            tooltip : val.description
                        }
                    );
                }
                else {
                    var yearCont = yearStart;
                    for (var i = 0; i <= yearEnd - yearStart; i++) {
                        oModel.getProperty("/specialDates").push({
                            startDate : new Date( yearCont,val.month - 1,val.day),
                            type : "Type02",
                            tooltip : val.description
                        });
                        yearCont = yearCont + 1;
                    }

                }
            });
            console.log(oModel.getProperty("/specialDates"));
            oModel.refresh();
        },
        handleCalendarSelect: function(oEvent) {
            var oModel = this.getView().getModel("calendar"),
                oCalendar = oEvent.oSource;
            var aSelectedDates = oCalendar.getSelectedDates();
            var oDate, date, arr = [];
            var dateFormatFrom = sap.ui.core.format.DateFormat.getDateInstance({pattern: "yyy-MM-dd"});
            if (aSelectedDates.length > 0 ) {
                oDate = aSelectedDates[0].getStartDate();
                oDate = dateFormatFrom.format(oDate);
                date = oDate.split("-");
                date[0] = parseInt(date[0], 10);
                date[1] = parseInt(date[1], 10);
                date[2] = parseInt(date[2], 10);

                arr = oModel.getProperty("/specialDates");

                //if(date[0] !== this.currentDate.year  || date[1] !== (this.currentDate.month)) {
                oModel.setProperty("/selectedMonth", []);

                arr.forEach(function(val, index) {

                    if(val.startDate.getFullYear() === date[0] && (val.startDate.getMonth() + 1) === date[1]) {
                        oModel.getProperty("/selectedMonth").push({
                            "title": val.tooltip,
                            "year": date[0],
                            "month": date[1],
                            "day": val.startDate.getDate()
                        });
                    }
                });
                this.currentDate.year = date[0];
                this.currentDate.month = date[1];
                //}
                oModel.refresh();

            } else {
                console.log("No Date Selected");
            }
        },
        loadSpecialDates: function() {
            var oModel = this.getView().getModel("calendar"),
                dates = oModel.getProperty("/specialDates"),
                list1 = this.getView().byId("list"),
                list2 = this.getView().byId("list_"),
                that = this;
            list1.destroyItems();
            list2.destroyItems();
            oModel.setProperty("/dates1", []);
            oModel.setProperty("/dates2", []);
            dates.forEach(function(val, ind){
                if(val.year === 0) {
                    var day = val.day;
                    var month = val.month;
                    var date1 = {}, date2 = {}, oDate, i, index = -1,
                        size = oModel.getProperty("/dates1").length;

                    if(size !== 0) {
                        i = 0;
                        while(i < size && index === -1) {

                            date1.month = that._monthsIndex[oModel.getProperty("/dates1")[i].month];
                            date1.day = oModel.getProperty("/dates1")[i].day;
                            oDate = oModel.getProperty("/dates1")[i + 1];
                            //console.log(day, month, date1, oDate);
                            if((month < date1.month) ||
										  (month === date1.month && day <= date1.day) )
                            {
                                index = i;
                            }
                            else if(oDate !== undefined) {
                                date2.month = that._monthsIndex[oDate.month];
                                date2.day = oDate.day;
                                if((month > date1.month && month < date2.month) ||
											   (month === date1.month &&  month < date2.month && day > date1.day) ||
											   (month === date1.month &&  month === date2.month && day > date1.day && day < date2.day)) {
                                    index = (i + 1);
                                }

                            }
                            else if((month > date1.month) ||
												(month === date1.month && day > date1.day)) {
                                index = size;
                            }
                            i++;
                        }
                    }else {
                        index = 0;
                        oModel.setProperty("/dates1", []);
                        console.log("vacio");
                    }
                    oModel.getProperty("/dates1").splice(index, 0, {
                        "day": val.day,
                        "month": that._months[val.month],
                        "description": val.description
                    });
                }
                else {
                    var date = [];
                    date[0] = val.day;
                    date[1] = val.month;
                    date[2] = val.year;
                    var date1 = {}, date2 = {}, oDate, i, index = -1,
                        size = oModel.getProperty("/dates2").length;
                    if(size !== 0) {
                        i = 0;
                        while(i < size && index === -1) {
                            date1.month = that._monthsIndex[oModel.getProperty("/dates2")[i].month];
                            date1.day = oModel.getProperty("/dates2")[i].day;
                            date1.year = oModel.getProperty("/dates2")[i].year;
                            oDate = oModel.getProperty("/dates2")[i + 1];

                            if((date[2] < date1.year) ||
										  (date[2] === date1.year && date[1] < date1.month) ||
										  (date[2] === date1.year && date[1] === date1.month && date[0] <= date1.day) )
                            {
                                index = i;
                            }
                            else if(oDate !== undefined) {
                                date2.month = that._monthsIndex[oDate.month];
                                date2.day = oDate.day;
                                date2.year = oDate.year;
                                if((date[2] > date1.year &&  date[2] < date2.year) || //entre años
											   (date[2] === date1.year &&  date[2] < date2.year && date[1] > date1.month) || //mismo año se chequea mes mayor
											   (date[2] === date1.year &&  date[2] < date2.year && date[1] === date1.month && date[0] > date1.day) ||
											   (date[2] === date1.year &&  date[2] === date2.year && date[1] > date1.month && date[1] < date2.month) ||
											   (date[2] === date1.year &&  date[2] === date2.year && date[1] === date1.month && date[0] > date1.day && date[0] < date2.day)) {
                                    index = (i + 1);
                                }

                            }
                            else if((date[2] > date1.year) ||
												(date[2] === date1.year && date[1] > date1.month) ||
												(date[2] === date1.year && date[1] === date1.month && date[0] > date1.day)) {
                                index = size;
                            }
                            i++;
                        }
                    }else {
                        index = 0;
                    }

                    oModel.getProperty("/dates2").splice(index, 0, {
                        "day": val.day,
                        "month": that._months[val.month],
                        "year": val.year,
                        "description": val.description
                    });
                    oModel.refresh();
                    that.byId("datePicker").setValue("");
                    that.byId("inputPartDesc").setValue("");

                    that.oModelSpecial.getProperty("/special").push(
                        {
                            startDate : new Date( val.year,val.month - 1,val.day),
                            type : "Type02"
                        }
                    );
                    that.oModelSpecial.refresh();
                }
                oModel.refresh();
            });
        },
        saveRecuDates: function(oEvent) {
            var oModel = this.getView().getModel("calendar");
            if(this.byId("selectRecuDays").getSelectedKey() !== "0" && this.byId("selectRecuMonths").getSelectedKey() !== "0" &&
					$.trim(this.byId("inputRecuDesc").getValue()) !== "") {
                var day = parseInt(this.byId("selectRecuDays").getSelectedItem().getText(),10);
                var month = this._monthsIndex[this.byId("selectRecuMonths").getSelectedItem().getText()];
                var date1 = {}, date2 = {}, oDate, that = this, i, index = -1,
                    size = oModel.getProperty("/dates1").length;
                console.log(oModel.getProperty("/dates1"));
                this.byId("list").destroyItems();
                if(size !== 0) {
                    i = 0;
                    while(i < size && index === -1) {

                        date1.month = that._monthsIndex[oModel.getProperty("/dates1")[i].month];
                        date1.day = oModel.getProperty("/dates1")[i].day;
                        oDate = oModel.getProperty("/dates1")[i + 1];
                        //console.log(day, month, date1, oDate);
                        if((month < date1.month) ||
									  (month === date1.month && day <= date1.day) )
                        {
                            index = i;
                        }
                        else if(oDate !== undefined) {
                            date2.month = that._monthsIndex[oDate.month];
                            date2.day = oDate.day;
                            if((month > date1.month && month < date2.month) ||
										   (month === date1.month &&  month < date2.month && day > date1.day) ||
										   (month === date1.month &&  month === date2.month && day > date1.day && day < date2.day)) {
                                index = (i + 1);
                            }

                        }
                        else if((month > date1.month) ||
											(month === date1.month && day > date1.day)) {
                            index = size;
                        }
                        i++;
                    }
                }else {
                    index = 0;
                    oModel.setProperty("/dates1", []);
                    console.log("vacio");
                }
                oModel.getProperty("/dates1").splice(index, 0, {
                    "day": parseInt(this.byId("selectRecuDays").getSelectedItem().getText(),10),
                    "month": this.byId("selectRecuMonths").getSelectedItem().getText(),
                    "description": this.byId("inputRecuDesc").getValue()
                });
                oModel.refresh();
                this.byId("selectRecuDays").setSelectedKey("0");
                this.byId("selectRecuMonths").setSelectedKey("0");
                this.byId("inputRecuDesc").setValue("");

            }
            else {
                this.onToast("En días recurrentes los campos son obligatorios", null);
            }
        },
        savePartDates: function(oEvent) {
            var oModel = this.getView().getModel("calendar");
            var dateValue = this.byId("datePicker").getDateValue(),
                dateDescp = this.byId("inputPartDesc").getValue(),
                dateFormatFrom, date;
            if(dateValue !== "" && $.trim(dateDescp) !== "") {
                dateFormatFrom = sap.ui.core.format.DateFormat.getDateInstance({pattern: "yyyMMdd"}).format(dateValue);
                date = this.byId("datePicker").getValue().split("-");
                date[0] = parseInt(date[0], 10);
                date[1] = parseInt(date[1], 10);
                date[2] = parseInt(date[2], 10);
                var date1 = {}, date2 = {}, oDate, that = this, i, index = -1,
                    size = oModel.getProperty("/dates2").length;
                this.byId("list_").destroyItems();
                if(size !== 0) {
                    i = 0;
                    while(i < size && index === -1) {
                        date1.month = that._monthsIndex[oModel.getProperty("/dates2")[i].month];
                        date1.day = oModel.getProperty("/dates2")[i].day;
                        date1.year = oModel.getProperty("/dates2")[i].year;
                        oDate = oModel.getProperty("/dates2")[i + 1];

                        if((date[2] < date1.year) ||
								  (date[2] === date1.year && date[1] < date1.month) ||
								  (date[2] === date1.year && date[1] === date1.month && date[0] <= date1.day) )
                        {
                            index = i;
                        }
                        else if(oDate !== undefined) {
                            date2.month = that._monthsIndex[oDate.month];
                            date2.day = oDate.day;
                            date2.year = oDate.year;
                            if((date[2] > date1.year &&  date[2] < date2.year) || //entre años
									   (date[2] === date1.year &&  date[2] < date2.year && date[1] > date1.month) || //mismo año se chequea mes mayor
									   (date[2] === date1.year &&  date[2] < date2.year && date[1] === date1.month && date[0] > date1.day) ||
									   (date[2] === date1.year &&  date[2] === date2.year && date[1] > date1.month && date[1] < date2.month) ||
									   (date[2] === date1.year &&  date[2] === date2.year && date[1] === date1.month && date[0] > date1.day && date[0] < date2.day)) {
                                index = (i + 1);
                            }

                        }
                        else if((date[2] > date1.year) ||
										(date[2] === date1.year && date[1] > date1.month) ||
										(date[2] === date1.year && date[1] === date1.month && date[0] > date1.day)) {
                            index = size;
                        }
                        i++;
                    }
                }else {
                    index = 0;
                }

                oModel.getProperty("/dates2").splice(index, 0, {
                    "day": date[0],
                    "month": that._months[date[1]],
                    "year": date[2],
                    "description": dateDescp
                });
                oModel.refresh();
                this.byId("datePicker").setValue("");
                this.byId("inputPartDesc").setValue("");

                this.oModelSpecial.getProperty("/special").push(
                    {
                        startDate : new Date( date[2],parseInt(date[1], 10) - 1,date[0]),
                        type : "Type02"
                    }
                );
                this.oModelSpecial.refresh();
            }
            else {
                this.onToast("En días particulares los campos son obligatorios", null);
            }
        },
        handleDeleteRecu: function(oEvent) {
            var oModel = this.getView().getModel("calendar"),
                oItem = oEvent.getParameter("listItem"),
                index = oItem.oBindingContexts.calendar.getPath().split("/")[2];

            oModel.getProperty("/dates1").splice(index, 1);
            oModel.refresh();
        },
        handleDeletePart: function(oEvent) {
            console.log("entro por aca");
            var oModel = this.getView().getModel("calendar"),
                oItem = oEvent.getParameter("listItem"),
                index = oItem.oBindingContexts.calendar.getPath().split("/")[2];

            oModel.getProperty("/dates2").splice(index, 1);
            this.oModelSpecial.getProperty("/special").splice(index, 1);
            oModel.refresh();
            this.oModelSpecial.refresh();
        },
        saveDatesRecu: function() {
            var oModel = this.getView().getModel("calendar"),
                dummy = this.getView().getModel("dummy"),
                that = this;
            var dates = [];
            this.byId("list").setBusy(true);
            oModel.getProperty("/dates1").forEach(function(val, index){
                dates.push({
                    day: val.day,
                    month: that._monthsIndex[val.month],
                    year: 0,
                    description: val.description,
                    calendar_id: oModel.getProperty("/calendarSelected/calendar_id")
                });
            });
            var settings = {
                contentType: "application/json",
	             	dataType: "json",
                url: dummy.getProperty("/serviceUrl") + "/holiday/addNewHolidaysCalendar",
                method: "POST",
                data: JSON.stringify({
                    "data": dates
                }),
                beforeSend: function( xhr ) {
					    that.byId("list").setBusy(true);
                },
                success: function(res) {
                    that.byId("list").setBusy(false);
                    console.log(res);
                    if(res.statusCode === 200) {
                        var dialog = new Dialog({
                            title: "Información",
                            type: "Message",
                            state: "Success",
                            content: new Text({
                                text: "Fechas recurrentes guardadas con éxito."
                            }),
                            beginButton: new Button({
                                text: "OK",
                                press: function () {
                                    dialog.close();
                                    /*that.byId("buttonAcceptTab1").setVisible(false);
										that.byId("buttonEditTab1").setVisible(true);
										that.byId("buttonDeleteTab1").setVisible(true);*/
                                }
                            }),
                            afterClose: function() {
                                dialog.destroy();
                            }
                        });

                        dialog.open();
                    }
                },
                error: function(err) {
                    console.log(err);
                    that.onToast("Error al guardar fechas recurrentes", null);
                    that.byId("list").setBusy(false);
                }
            };
            $.ajax(settings);
        },
        cancelDatesRecu: function() {
            var that = this;
            var dialog = new Dialog({
                title: "Aviso",
                type: "Message",
                state: "Warning",
                content: new Text({
                    text: "Se procedera a eliminar los días recurrentes cargados."
                }),
                beginButton: new Button({
                    text: "OK",
                    press: function () {
                        dialog.close();
                        that.deleteDatesRecu();
                    }
                }),
                endButton: new Button({
                    text: "Cancelar",
                    press: function () {
                        dialog.close();
                    }
                }),
                afterClose: function() {
                    dialog.destroy();
                }
            });

            dialog.open();
        },
        deleteDatesRecu: function() {
            var oModel = this.getView().getModel("calendar"),
                dummy = this.getView().getModel("dummy"),
                that = this;
            console.log(oModel.getProperty("/calendarSelected/code"));
            var settings = {
                url: dummy.getProperty("/serviceUrl") + "/holiday/deleteHoliday/",
                method: "DELETE",
                data: {
                    "code": oModel.getProperty("/calendarSelected/code"),
                    "recurrent": 1
                },
                beforeSend: function( xhr ) {
					    that.byId("list").setBusy(true);
                },
                success: function(res) {
                    that.byId("list").setBusy(false);
                    console.log(res);
                    if(res.statusCode === 200) {
                        var dialog = new Dialog({
                            title: "Información",
                            type: "Message",
                            state: "Success",
                            content: new Text({
                                text: "Fechas recurrentes eliminadas con éxito."
                            }),
                            beginButton: new Button({
                                text: "OK",
                                press: function () {
                                    dialog.close();
                                    /*that.byId("buttonAcceptTab1").setVisible(true);
										that.byId("buttonEditTab1").setVisible(false);
										that.byId("buttonDeleteTab1").setVisible(false);*/
                                    that.byId("list").destroyItems();
                                }
                            }),
                            afterClose: function() {
                                dialog.destroy();
                            }
                        });

                        dialog.open();
                    }
                },
                error: function(err) {
                    console.log(err);
                    that.onToast("Error al borrar días recurrentes", null);
                    that.byId("list").setBusy(false);
                }
            };
            $.ajax(settings);
        },
        editDatesRecu: function() {
            /*this.byId("buttonAcceptTab1").setVisible(true);
			this.byId("buttonEditTab1").setVisible(false);
			this.byId("buttonDeleteTab1").setVisible(true);*/
        },
        handleTabSelect: function(oEvent) {
            var dummy = this.getView().getModel("dummy");
            console.log(oEvent.getParameters());
            if(oEvent.getParameters().key === "particulares") {
                this.byId("buttonAcceptTab1").setVisible(false);
                //this.byId("buttonEditTab1").setVisible(false);
                this.byId("buttonDeleteTab1").setVisible(false);
                this.byId("buttonAcceptTab2").setVisible(true);
                //this.byId("buttonEditTab2").setVisible(true);
                this.byId("buttonDeleteTab2").setVisible(true);
                /*dummy.setProperty("/recuButtons/buttonAcceptTab1", true);
				dummy.setProperty("/recuButtons/buttonEditTab1", true);
				dummy.setProperty("/recuButtons/buttonDeleteTab1", true);*/
            }
            else {
                this.byId("buttonAcceptTab1").setVisible(true);
                //this.byId("buttonEditTab1").setVisible(true);
                this.byId("buttonDeleteTab1").setVisible(true);
                this.byId("buttonAcceptTab2").setVisible(false);
                //this.byId("buttonEditTab2").setVisible(false);
                this.byId("buttonDeleteTab2").setVisible(false);

            }
        },
        saveDatesPart: function() {
            var oModel = this.getView().getModel("calendar"),
                dummy = this.getView().getModel("dummy"),
                that = this;
            var dates = [];
            this.byId("list_").setBusy(true);
            oModel.getProperty("/dates2").forEach(function(val, index){
                dates.push({
                    "day": val.day,
                    "month": that._monthsIndex[val.month],
                    "year": val.year,
                    "description": val.description,
                    "calendar_id":  oModel.getProperty("/calendarSelected/calendar_id")
                });
            });
            console.log(dates);
            var settings = {
                async: true,
                contentType: "application/json",
             	dataType: "json",
                url: dummy.getProperty("/serviceUrl") + "/holiday/addNewHolidaysCalendar",
                method: "POST",
                data: JSON.stringify({
                    "data": dates
                }),
                success: function(res) {
                    that.byId("list_").setBusy(false);
                    console.log(res);
                    if(res.statusCode === 200) {
                        var dialog = new Dialog({
                            title: "Información",
                            type: "Message",
                            state: "Success",
                            content: new Text({
                                text: "Fechas particulares guardadas con éxito."
                            }),
                            beginButton: new Button({
                                text: "OK",
                                press: function () {
                                    dialog.close();
                                }
                            }),
                            afterClose: function() {
                                dialog.destroy();
                            }
                        });

                        dialog.open();
                    }
                },
                error: function(err) {
                    console.log(err);
                    that.onToast("Error al guardar fechas recurrentes", null);
                    that.byId("list_").setBusy(false);
                }
            };
            $.ajax(settings);
        },
        cancelDatesPart: function() {
            var that = this;
            var dialog = new Dialog({
                title: "Aviso",
                type: "Message",
                state: "Warning",
                content: new Text({
                    text: "Se procederá a eliminar los días particulares cargados."
                }),
                beginButton: new Button({
                    text: "OK",
                    press: function () {
                        dialog.close();
                        that.deleteDatesPart();
                    }
                }),
                endButton: new Button({
                    text: "Cancelar",
                    press: function () {
                        dialog.close();
                    }
                }),
                afterClose: function() {
                    dialog.destroy();
                }
            });

            dialog.open();
        },
        deleteDatesPart: function() {
            var oModel = this.getView().getModel("calendar"),
                dummy = this.getView().getModel("dummy"),
                that = this;
            this.byId("list_").setBusy(true);
            console.log(oModel.getProperty("/calendarSelected/code"));
            var settings = {
                url: dummy.getProperty("/serviceUrl") + "/holiday/deleteHoliday/",
                method: "DELETE",
                data: {
                    "code": oModel.getProperty("/calendarSelected/code"),
                    "recurrent": 0
                },
                success: function(res) {
                    that.byId("list_").setBusy(false);
                    console.log(res);
                    if(res.statusCode === 200) {
                        var dialog = new Dialog({
                            title: "Información",
                            type: "Message",
                            state: "Success",
                            content: new Text({
                                text: "Fechas particulares eliminadas con éxito."
                            }),
                            beginButton: new Button({
                                text: "OK",
                                press: function () {
                                    dialog.close();
                                    that.byId("list_").destroyItems();
                                }
                            }),
                            afterClose: function() {
                                dialog.destroy();
                            }
                        });

                        dialog.open();
                    }
                },
                error: function(err) {
                    console.log(err);
                    that.onToast("Error al borrar días particulares", null);
                    that.byId("list_").setBusy(false);
                }
            };
            $.ajax(settings);
        },
        handleTitlePress: function() {
            var oModel = this.getView().getModel("calendar");
            if(oModel.getProperty("/calendarSelected/generated")) {
                sap.ui.getCore().byId("saturdayOff_").setEnabled(false);
                sap.ui.getCore().byId("sundayOff_").setEnabled(false);
                sap.ui.getCore().byId("initWeek_").setEnabled(false);
                sap.ui.getCore().byId("inputCode_").setEnabled(false);
                sap.ui.getCore().byId("inputDescp_").setEnabled(false);
                sap.ui.getCore().byId("inputAnho1_").setEnabled(false);
                sap.ui.getCore().byId("inputAnho2_").setEnabled(false);
            }
            else {
                sap.ui.getCore().byId("saturdayOff_").setEnabled(true);
                sap.ui.getCore().byId("sundayOff_").setEnabled(true);
                sap.ui.getCore().byId("initWeek_").setEnabled(true);
                sap.ui.getCore().byId("inputCode_").setEnabled(true);
                sap.ui.getCore().byId("inputDescp_").setEnabled(true);
                sap.ui.getCore().byId("inputAnho1_").setEnabled(true);
                sap.ui.getCore().byId("inputAnho2_").setEnabled(true);
            }
            if(oModel.getProperty("/calendarSelected/saturday") === "NO Laborable") {
                sap.ui.getCore().byId("saturdayOff_").setSelectedKey(6);
            }
            else {
                sap.ui.getCore().byId("saturdayOff_").setSelectedKey(-1);
            }
            if(oModel.getProperty("/calendarSelected/sunday") === "NO Laborable") {
                sap.ui.getCore().byId("sundayOff_").setSelectedKey(0);
            }
            else {
                sap.ui.getCore().byId("sundayOff_").setSelectedKey(-1);
            }
            if(oModel.getProperty("/calendarSelected/week_start").search("Sunday") > -1) {
                sap.ui.getCore().byId("initWeek_").setSelectedKey(0);
            }
            else if (oModel.getProperty("/calendarSelected/week_start").search("Monday") > -1){
                sap.ui.getCore().byId("initWeek_").setSelectedKey(1);
            }
            this.editFormCalendar.open();
        },
        cancelCalendar: function(oEvent) {
            this.editFormCalendar.close();
        },
        editCalendar: function() {
            var oModel = this.getView().getModel("calendar"),
                dummy = this.getView().getModel("dummy"),
                that = this,
                calendar = {
                    "code": oModel.getProperty("/calendarSelected/code"),
                    "description": oModel.getProperty("/calendarSelected/description"),
                    "saturday": (parseInt(sap.ui.getCore().byId("saturdayOff_").getSelectedKey(),10) === -1 ? "Laborable" : "NO Laborable"),
                    "sunday": (parseInt(sap.ui.getCore().byId("sundayOff_").getSelectedKey(),10) === -1 ? "Laborable" : "NO Laborable"),
                    "week_start": (parseInt(sap.ui.getCore().byId("initWeek_").getSelectedKey(),10) === 0 ? "Sunday" : "Monday"),
                    "year_start": parseInt(oModel.getProperty("/calendarSelected/year_start"), 10),
                    "year_end": parseInt(oModel.getProperty("/calendarSelected/year_end"),10)
                };
            console.log(calendar);
            var settings = {
                url: dummy.getProperty("/serviceUrl") + "/calendar/",
                method: "PUT",
                data: calendar,
                success: function(res) {
                    console.log(res);
                    if(res.statusCode === 200) {
                        var dialog = new Dialog({
                            title: "Información",
                            type: "Message",
                            state: "Success",
                            content: new Text({
                                text: "Calendario actualizado con éxito."
                            }),
                            beginButton: new Button({
                                text: "OK",
                                press: function () {
                                    dialog.close();
                                    if(dummy.getProperty("/visibleVbox2")) {
                                        var sunday = (parseInt(sap.ui.getCore().byId("sundayOff_").getSelectedKey(),10) === -1 ? -1 : 0),
                                            saturday = (parseInt(sap.ui.getCore().byId("saturdayOff_").getSelectedKey(),10) === -1 ? -1 : 6),
                                            startWeek = (parseInt(sap.ui.getCore().byId("initWeek_").getSelectedKey(),10) ? 1 : 0);

                                        that.byId("calendar1").setFirstDayOfWeek(startWeek);
									    that.byId("calendar1").setNonWorkingDays([saturday, sunday]);
                                    }
                                    that.cancelCalendar();
                                }
                            }),
                            afterClose: function() {
                                dialog.destroy();
                            }
                        });

                        dialog.open();
                    }
                },
                error: function(err) {
                    console.log(err);
                    that.onToast("Error al actualizar datos del calendario", null);
                }
            };
            $.ajax(settings);
        },
        handleCopyPress: function(oEvent) {
            var oModel = this.getView().getModel("calendar");
            var list = sap.ui.getCore().byId("selectDialog");
            var that = this;
            var items = list.getItems();
            var oSorter = new sap.ui.model.Sorter("calendar_id", true); // sort descending
            list.unbindAggregation();
            var oTemplate = new sap.m.StandardListItem({
                title: "{calendar>code}",
                visible: {
                    parts: [
                        {path: "calendar>code"}
                    ],
                    formatter: function(sValue){
                        console.log(sValue);
                        if($.trim(sValue) !== $.trim(that.getView().getModel("calendar").getProperty("/calendarSelected/code"))){
                            return true;
                        }
                        return false;
                    }
                }
            });
            list.bindAggregation("items", "calendar>/calendars", oTemplate, oSorter);
            this.copyDialog.open();

        },
        onConfirmCopy: function(oEvent){
            var oModel = this.getView().getModel("calendar"),
                dummy = this.getView().getModel("dummy"),
                that = this;
            var oSelectedItem = oEvent.getParameter("selectedItem"),
                sPath = oSelectedItem.oBindingContexts.calendar.getPath(),
                oObject = oModel.getProperty(sPath);
            console.log(oObject, oModel.getProperty("/calendarSelected/code"));
            var dialog = new Dialog({
                title: "Confirmación",
                type: "Message",
                content: new Text({
                    text: "Se procederá a copiar fechas del calendario " + oObject.code + " al calendario " + oModel.getProperty("/calendarSelected/code")
                }),
                beginButton: new Button({
                    text: "Continuar",
                    press: function () {
                        dialog.close();
                        that.copyCalendar(oObject.code);
                    }
                }),
                endButton: new Button({
                    text: "Cancelar",
                    press: function () {
                        dialog.close();
                    }
                }),
                afterClose: function() {
                    dialog.destroy();
                }
            });

            dialog.open();
        },
        copyCalendar: function(codeOrigen) {
            var oModel = this.getView().getModel("calendar"),
                dummy = this.getView().getModel("dummy"),
                that = this;
            if(oModel.getProperty("/calendarSelected/calendar_id") !== - 1) {
                console.log(codeOrigen, oModel.getProperty("/calendarSelected/code"));
                var settings = {
                    async: true,
                    crossDomain: true,
                    url: dummy.getProperty("/serviceUrl") + "/holiday/addHolidaysCalendar",
                    method: "POST",
                    data: {
                        "codeO": codeOrigen,
                        "codeD": oModel.getProperty("/calendarSelected/code")
                    },
                    success: function(res) {
                        console.log(res);
                        if(res.statusCode === 200) {
                            var dialog = new Dialog({
                                title: "Información",
                                type: "Message",
                                state: "Success",
                                content: new Text({
                                    text: "Fechas copiadas con éxito desde calendario " + codeOrigen + " a calendario " + oModel.getProperty("/calendarSelected/code")
                                }),
                                beginButton: new Button({
                                    text: "OK",
                                    press: function () {
                                        dialog.close();
                                        oModel.refresh();
                                        that.navToCalendarsAndDays(oModel.getProperty("/calendarSelected/code"));
                                    }
                                }),
                                afterClose: function() {
                                    dialog.destroy();
                                }
                            });

                            dialog.open();
                        }
                        else if(res.statusCode === 500) {
                            that.onToast("No existen elementos a insertar", null);
                        }
                    },
                    error: function(err) {
                        console.log(err);
                    }
                };
                $.ajax(settings);
            }
            else {
                this.onToast("Error al copiar.", null);
            }
        },
        onCancelCopy: function() {
            //var oModel = this.getView().getModel("calendar");
            //console.log(oModel.setProperty("/calendarSelected", []));
            //this.copyDialog.close();
        },
        confirmGenerateCalendar: function() {
            var that = this;
            var dialog = new Dialog({
                title: "Confirmación",
                type: "Message",
                content: new Text({
                    text: "Se procederá a generar calendario. Una vez generado no se podrá cargar nuevas fechas."
                }),
                beginButton: new Button({
                    text: "Continuar",
                    press: function () {
                        dialog.close();
                        that.generateCalendar();
                    }
                }),
                endButton: new Button({
                    text: "Cancelar",
                    press: function () {
                        dialog.close();
                    }
                }),
                afterClose: function() {
                    dialog.destroy();
                }
            });

            dialog.open();
        },
        generateCalendar: function() {
            var oModel = this.getView().getModel("calendar"),
                dummy = this.getView().getModel("dummy"),
                that = this;
            var settings = {
                async: true,
                crossDomain: true,
                url: dummy.getProperty("/serviceUrl") + "/calendarDay/generateDays",
                method: "POST",
                data: {
                    "calendarId": oModel.getProperty("/calendarSelected/calendar_id")
                },
                success: function(res) {
                    console.log(res);
                    if(res.statusCode === 200) {
                        var dialog = new Dialog({
                            title: "Información",
                            type: "Message",
                            state: "Success",
                            content: new Text({
                                text: "Calendario generado con éxito."
                            }),
                            beginButton: new Button({
                                text: "OK",
                                press: function () {
                                    dialog.close();
                                    oModel.refresh();
                                    that.navToCalendars();
                                    //that.getCalendarAndDays();
                                }
                            }),
                            afterClose: function() {
                                dialog.destroy();
                            }
                        });

                        dialog.open();
                    }
                },
                error: function(err) {
                    console.log(err);
                    that.onToast("Error al guardar fechas recurrentes", null);
                }
            };
            $.ajax(settings);
        },
        handleStartDateChange: function(oEvent) {
            //console.log(oEvent.getSource());
            this.byId("listHolidays").destroyItems();
        },
        navToCalendars: function() {
            var dummy = this.getView().getModel("dummy"),
                oModel = this.getView().getModel("calendar");
            oModel.refresh();
            //console.log(oModel.getProperty("/specialDates"));
            console.log(this.getView().getModel("dummy").getProperty("/phone/"));
            if(!this.getView().getModel("dummy").getProperty("/phone/")) {
                this.getRouter().navTo("master");
                //dummy.setProperty("/visibleVbox1", false);
                //dummy.setProperty("/visibleVbox2", true);
                //this.byId("listHolidays").destroyItems();
                this.getCalendarAndDaysGenerated();
            }
        },
        navToCalendarsAndDays: function(codeCalendar) {
            var list1 = this.byId("list");
            var list2 = this.byId("list_");
            list1.unbindItems();
            list2.unbindItems();
            var oTemplate1 = new sap.m.StandardListItem({
		        title: "{calendar>description}",
		        info: "{calendar>day} / {calendar>month}"
		    });
		    var oTemplate2 = new sap.m.StandardListItem({
		        title: "{calendar>description}",
		        info: "{calendar>day} / {calendar>month} / {calendar>year}"
		    });
		    this.getCalendarAndDays();
		    list1.bindItems("calendar>/dates1", oTemplate1);
		    list2.bindItems("calendar>/dates2", oTemplate2);
            /*console.log(this.getView().getModel("dummy").getProperty("/phone/"));
			if(!this.getView().getModel("dummy").getProperty("/phone/")) {
				this.getRouter().navTo("detail", {
					id: codeCalendar
				}, false );
			}*/
        },
        _handleHelpSearch: function(oEvent) {
            var aFilters = [],
                sQuery = oEvent.getParameter("value"),
                binding = sap.ui.getCore().byId("selectDialog").getBinding("items");

            if (sQuery && sQuery.length > 0) {
                var filter1 = new sap.ui.model.Filter("code", sap.ui.model.FilterOperator.Contains, sQuery);

                aFilters = new sap.ui.model.Filter([filter1]);
            }
            binding.filter(aFilters);
        }

        /**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf MasterDetail.view.Detail
		 */
        //	onAfterRendering: function() {
        //
        //	},

        /**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf MasterDetail.view.Detail
		 */
        //	onExit: function() {
        //
        //	}

    });

});
