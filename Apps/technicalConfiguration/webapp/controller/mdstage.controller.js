sap.ui.define([
    "technicalConfiguration/controller/BaseController",
    "sap/ui/model/json/JSONModel",
    "sap/m/Dialog",
    "sap/m/Button"
], function(BaseController, JSONModel, Dialog, Button) {
    "use strict";

    return BaseController.extend("technicalConfiguration.controller.mdstage", {

        onInit: function() {
            //ruta para la vista principal
            this.getOwnerComponent().getRouter().getRoute("mdstage").attachPatternMatched(this._onRouteMatched, this);
            //ruta para la vista de detalles de un registro
            this.getOwnerComponent().getRouter().getRoute("mdstage_Record").attachPatternMatched(this._onRecordMatched, this);
            //ruta para la vista de creación de un registro
            this.getOwnerComponent().getRouter().getRoute("mdstage_Create").attachPatternMatched(this._onCreateMatched, this);
        },

        /**
     * Coincidencia de ruta para acceder a la vista principal
     * @param  {Event} oEvent Evento que llamó esta función
     */
        _onRouteMatched: function(oEvent) {
            /**
       * @type {Controller} that        Referencia a este controlador
       * @type {JSONModel} util         Referencia al modelo "util"
       * @type {JSONModel} MDSTAGE      Referencia al modelo "MdSTAGE"
       */

            var that = this,
                util = this.getView().getModel("util"),
                mdstage = this.getView().getModel("MDSTAGE");

            //dependiendo del dispositivo, establece la propiedad "phone"
            this.getView().getModel("util").setProperty("/phone/",
                this.getOwnerComponent().getContentDensityClass() === "sapUiSizeCozy");

            //establece MDSTAGE como la entidad seleccionada
            util.setProperty("/selectedEntity/", "mdstage");

            //obtiene los registros de mdstage
            this.onRead(that, util, mdstage);
        },
        /**
     * Obtiene todos los registros de MDSTAGE
     * @param  {Controller} that   Referencia al controlador que llama esta función
     * @param  {JSONModel} util    Referencia al modelo "util"
     * @param  {JSONModel} MDSTAGE Referencia al modelo "MDSTAGE"
     */
        onRead: function(that, util, mdstage) {

            /** @type {Object} settings opciones de la llamada a la función ajax */

            var serviceUrl = util.getProperty("/serviceUrl");
            var settings = {
                url: serviceUrl + "/stage",
                method: "GET",
                success: function(res) {
                    console.log(res.data);
                    util.setProperty("/busy/", false);
                    mdstage.setProperty("/records/", res.data);
                    console.log(mdstage);
                },
                error: function(err) {
                    util.setProperty("/error/status", err.status);
                    util.setProperty("/error/statusText", err.statusText);
                }
            };
            console.log(util);
            util.setProperty("/busy/", true);
            //borra los registros MDSTAGE que estén almacenados actualmente
            mdstage.setProperty("/records/", []);
            //realiza la llamada ajax
            $.ajax(settings);
        },
        /**
     * Coincidencia de ruta para acceder a la creación de un registro
     * @param  {Event} oEvent Evento que llamó esta función
     */
        _onCreateMatched: function(oEvent) {

            this._resetRecordValues();
            this._editRecordValues(true);
            this._editRecordRequired(true);
        },
        /**
     * Resetea todos los valores existentes en el formulario del registro
     */
        _resetRecordValues: function() {
            /**
       * @type {JSONModel} MDSTAGE Referencia al modelo "MDSTAGE"
       */
            var mdstage = this.getView().getModel("MDSTAGE");

            mdstage.setProperty("/stage_id/value", "");

            mdstage.setProperty("/order_/editable", true);
            mdstage.setProperty("/order_/value", "");
            mdstage.setProperty("/order_/state", "None");
            mdstage.setProperty("/order_/stateText", "");

            mdstage.setProperty("/name/value", "");
            mdstage.setProperty("/name/state", "None");
            mdstage.setProperty("/name/stateText", "");

        },
        /**
     * Habilita/deshabilita la edición de los datos de un registro MDSTAGE
     * @param  {Boolean} edit "true" si habilita la edición, "false" si la deshabilita
     */
        _editRecordValues: function(edit) {

            var mdstage = this.getView().getModel("MDSTAGE");
            mdstage.setProperty("/order_/editable", edit);
            mdstage.setProperty("/name/editable", edit);

        },
        /**
     * Se define un campo como obligatorio o no, de un registro MDSTAGE
     * @param  {Boolean} edit "true" si es campo obligatorio, "false" si no es obligatorio
     */
        _editRecordRequired: function(edit) {
            var mdstage = this.getView().getModel("MDSTAGE");
            mdstage.setProperty("/order_/required", edit);
            mdstage.setProperty("/name/required", edit);

        },
        /**
     * Navega a la vista para crear un nuevo registro
     * @param  {Event} oEvent Evento que llamó esta función
     */
        onNewRecord: function(oEvent) {
            this.getRouter().navTo("mdstage_Create", {}, false /*create history*/ );
        },
        /**
     * Cancela la creación de un registro MDSTAGE, y regresa a la vista principal
     * @param  {Event} oEvent Evento que llamó esta función
     */
        onCancelCreate: function(oEvent) {
            this._resetRecordValues();
            this.onNavBack(oEvent);
        },
        /**
     * Regresa a la vista principal de la entidad seleccionada actualmente
     * @param  {Event} oEvent Evento que llamó esta función
     */
        onNavBack: function(oEvent) {
            /** @type {JSONModel} util Referencia al modelo "util" */
            var util = this.getView().getModel("util");

            this.getRouter().navTo(util.getProperty("/selectedEntity"), {}, false /*create history*/ );
        },
        /**
     * Solicita al servicio correspondiente crear un registro MDSTAGE
     * @param  {Event} oEvent Evento que llamó esta función
     */
        onCreate: function(oEvent) {
            //Si el registro que se desea crear es válido
            if (this._validRecord()) {
                var that = this;
                var util = this.getView().getModel("util");
                var mdstage = this.getView().getModel("MDSTAGE");
                console.log(mdstage.getProperty("/order/value"));
                var serviceUrl = util.getProperty("/serviceUrl");
                $.ajax({
                    type: "POST",
                    contentType: "application/json",
                    data: JSON.stringify({
                        "order_": mdstage.getProperty("/order_/value"),
                        "name": mdstage.getProperty("/name/value"),
                    }),
                    url: serviceUrl + "/stage/",
                    dataType: "json",
                    async: true,
                    success: function(data) {
                        util.setProperty("/busy/", false);
                        that._resetRecordValues();
                        that.onToast(that.getI18n().getText("OS.recordCreated"));
                        that.getRouter().navTo("mdstage", {}, true /*no history*/ );
                    },
                    error: function(error) {
                        that.onToast("Error: " + error.responseText);
                        console.log("Read failed ");
                    }
                });
            }
        },
        /**
     * Valida la correctitud de los datos existentes en el formulario del registro
     * @return {Boolean} Devuelve "true" si los datos son correctos, y "false" si son incorrectos
     */
        _validRecord: function() {
            /**
       * @type {JSONModel} MDSTAGE Referencia al modelo "MDSTAGE"
       * @type {Boolean} flag "true" si los datos son válidos, "false" si no lo son
       */
            var mdstage = this.getView().getModel("MDSTAGE"),
                flag = true,
                that = this,
                regExp_only_letters = /^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$/,
                regExp_only_number = /^\d+$/;


            if (mdstage.getProperty("/name/value") === "") {
                flag = false;
                mdstage.setProperty("/name/state", "Error");
                mdstage.setProperty("/name/stateText", this.getI18n().getText("enter.FIELD"));
            } else if (!regExp_only_letters.test(mdstage.getProperty("/name/value"))) {
                flag = false;
                mdstage.setProperty("/name/state", "Error");
                mdstage.setProperty("/name/stateText", this.getI18n().getText("enter.FIELD.WN"));
            } else {
                mdstage.setProperty("/name/state", "None");
                mdstage.setProperty("/name/stateText", "");
            }

            if (mdstage.getProperty("/order_/value") === "") {
                flag = false;
                mdstage.setProperty("/order_/state", "Error");
                mdstage.setProperty("/order_/stateText", this.getI18n().getText("enter.FIELD"));
            } else if (!regExp_only_number.test(mdstage.getProperty("/order_/value"))) {
                flag = false;
                mdstage.setProperty("/order_/state", "Error");
                mdstage.setProperty("/order_/stateText", this.getI18n().getText("enter.FIELD.SENE"));
            } else {
                mdstage.setProperty("/order_/state", "None");
                mdstage.setProperty("/order_/stateText", "");
            }

            return flag;
        },
        /**
     * Visualiza los detalles de un registro MDSTAGE
     * @param  {Event} oEvent Evento que llamó esta función
     */
        onViewStageRecord: function(oEvent) {

            var mdstage = this.getView().getModel("MDSTAGE");
            mdstage.setProperty("/save/", false);
            mdstage.setProperty("/cancel/", false);
            mdstage.setProperty("/selectedRecordPath/", oEvent.getSource().getBindingContext("MDSTAGE"));
            mdstage.setProperty("/stage_id/value", oEvent.getSource().getBindingContext("MDSTAGE").getObject().stage_id);
            mdstage.setProperty("/order_/value", oEvent.getSource().getBindingContext("MDSTAGE").getObject().order_);
            mdstage.setProperty("/name/value", oEvent.getSource().getBindingContext("MDSTAGE").getObject().name);

            this.getRouter().navTo("mdstage_Record", {}, false /*create history*/ );
        },
        /**
     * Coincidencia de ruta para acceder a los detalles de un registro
     * @param  {Event} oEvent Evento que llamó esta función
     */
        _onRecordMatched: function(oEvent) {

            this._viewOptions();

        },
        /**
     * Cambia las opciones de visualización disponibles en la vista de detalles de un registro
     */
        _viewOptions: function() {
            var mdstage = this.getView().getModel("MDSTAGE");
            mdstage.setProperty("/save/", false);
            mdstage.setProperty("/cancel/", false);
            mdstage.setProperty("/modify/", true);
            mdstage.setProperty("/delete/", true);

            this._editRecordValues(false);
            this._editRecordRequired(false);
        },
        /**
     * Ajusta la vista para editar los datos de un registro
     * @param  {Event} oEvent Evento que llamó esta función
     */
        onEdit: function(oEvent) {

            var mdstage = this.getView().getModel("MDSTAGE");
            mdstage.setProperty("/save/", true);
            mdstage.setProperty("/cancel/", true);
            mdstage.setProperty("/modify/", false);
            mdstage.setProperty("/delete/", false);
            this._editRecordRequired(true);
            this._editRecordValues(true);
        },

        /**
     * Cancela la edición de un registro MDSTAGE
     * @param  {Event} oEvent Evento que llamó esta función
     */
        onCancelEdit: function(oEvent) {
            /** @type {JSONModel} MDSTAGE  Referencia al modelo MDSTAGE */

            this.onView();
        },
        /**
     * Ajusta la vista para visualizar los datos de un registro
     */
        onView: function() {
            this._viewOptions();
        },
        /**
     * Solicita al servicio correspondiente actualizar un registro MDSTAGE
     * @param  {Event} oEvent Evento que llamó esta función
     */
        onUpdate: function(oEvent) {
            /**
       * Si el registro que se quiere actualizar es válido y hubo algún cambio
       * con respecto a sus datos originales
       */
            if (this._validRecord() && this._recordChanged()) {
                /**
         * @type {JSONModel} MDSTAGE       Referencia al modelo "MDSTAGE"
         * @type {JSONModel} util         Referencia al modelo "util"
         * @type {Controller} that         Referencia a este controlador
         */
                var mdstage = this.getView().getModel("MDSTAGE");
                var util = this.getView().getModel("util");
                var that = this;
                var serviceUrl = util.getProperty("/serviceUrl");
                $.ajax({
                    type: "PUT",
                    contentType: "application/json",
                    data: JSON.stringify({
                        "stage_id": mdstage.getProperty("/stage_id/value"),
                        "order_": mdstage.getProperty("/order_/value"),
                        "name": mdstage.getProperty("/name/value"),
                    }),
                    url: serviceUrl + "/stage/",
                    dataType: "json",
                    async: true,
                    success: function(data) {

                        util.setProperty("/busy/", false);
                        that._resetRecordValues();
                        that._viewOptions();
                        that.onToast(that.getI18n().getText("OS.recordUpdated"));
                        that.getRouter().navTo("mdstage", {}, true /*no history*/ );

                    },
                    error: function(request, status, error) {
                        that.onToast("Error de comunicación");
                        console.log("Read failed");
                    }
                });
            }
        },
        /**
     * Verifica si el registro seleccionado tiene algún cambio con respecto a sus valores originales
     * @return {Boolean} Devuelve "true" el registro cambió, y "false" si no cambió
     */
        _recordChanged: function() {
            /**
       * @type {JSONModel} MDSTAGE         Referencia al modelo "MDSTAGE"
       * @type {Boolean} flag            "true" si el registro cambió, "false" si no cambió
       */
            var mdstage = this.getView().getModel("MDSTAGE"),
                flag = false;

            if (mdstage.getProperty("/order_/value") !== mdstage.getProperty(mdstage.getProperty("/selectedRecordPath/") + "/order_")) {
                flag = true;
            }

            if (mdstage.getProperty("/name/value") !== mdstage.getProperty(mdstage.getProperty("/selectedRecordPath/") + "/name")) {
                flag = true;
            }

            if (!flag) this.onToast("No se detectaron cambios");

            return flag;
        },
        onstageSearch: function(oEvent) {
            var aFilters = [],
                sQuery = oEvent.getSource().getValue(),
                binding = this.getView().byId("stageTable").getBinding("items");

            if (sQuery && sQuery.length > 0) {
                /** @type {Object} filter1 Primer filtro de la búsqueda */
                var filter1 = new sap.ui.model.Filter("name", sap.ui.model.FilterOperator.Contains, sQuery);
                aFilters = new sap.ui.model.Filter([filter1]);
            }

            //se actualiza el binding de la lista
            binding.filter(aFilters);

        },
        onConfirmDelete: function(oEvent) {

            var oBundle = this.getView().getModel("i18n").getResourceBundle();
            var deleteRecord = oBundle.getText("deleteRecord");
            var confirmation = oBundle.getText("confirmation");
            var util = this.getView().getModel("util");

            var that = this;
            var dialog = new Dialog({
                title: confirmation,
                type: "Message",
                content: new sap.m.Text({
                    text: deleteRecord
                }),

                beginButton: new Button({
                    text: "Si",
                    press: function() {
                        util.setProperty("/busy/", true);
                        var mdstage = that.getView().getModel("MDSTAGE");
                        console.log("Eliminar: " + mdstage.getProperty("/stage_id/value"));
                        console.log("Eliminar: " + mdstage.getProperty("/name/value"));
                        var serviceUrl = util.getProperty("/serviceUrl");
                        $.ajax({
                            type: "DELETE",
                            contentType: "application/json",
                            data: JSON.stringify({
                                "stage_id": mdstage.getProperty("/stage_id/value")
                            }),
                            url: serviceUrl + "/stage/",
                            dataType: "json",
                            async: true,
                            success: function(data) {

                                util.setProperty("/busy/", false);
                                that.getRouter().navTo("mdstage", {}, true);
                                dialog.close();
                                dialog.destroy();

                            },
                            error: function(request, status, error) {
                                that.onToast("Error de comunicación");
                                console.log("Read failed");
                            }
                        });

                    }
                }),
                endButton: new Button({
                    text: "No",
                    press: function() {
                        dialog.close();
                        dialog.destroy();
                    }
                })
            });

            dialog.open();

        }

    });
});
