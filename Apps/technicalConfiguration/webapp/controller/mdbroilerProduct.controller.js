sap.ui.define([
    "technicalConfiguration/controller/BaseController",
    "sap/ui/model/json/JSONModel",
    "sap/m/Dialog",
    "sap/m/Text",
    "sap/m/Button"
], function(BaseController, JSONModel, Dialog, Text, Button) {
    "use strict";

    return BaseController.extend("technicalConfiguration.controller.mdbroilerProduct", {

        onChangeSelectBroilerProduct: function(event) {
        // console.log("entra al change del select broilerProduct");
        // console.log(event.getSource().getSelectedItem().getKey()); // works ok
        // var path = event.getSource().getSelectedItem().getBindingContext().getPath(); // Fails
            var key = event.getSource().getSelectedItem().getKey();
            // console.log("val=" + key);

            // var oModel = sap.ui.getCore().getModel();
            var mdbroilerProduct = this.getView().getModel("MDBROILERPRODUCT");
            mdbroilerProduct.setProperty("/days_eviction/value", key);
            // var theName = oModel.getProperty(path);
            // console.log("You selected " + mdbroilerProduct.getProperty("/days_eviction/value") );
        
        },

        onInit: function() {
            //ruta para la vista principal
            console.log("Controller de broiler product");
            this.getOwnerComponent().getRouter().getRoute("mdbroilerProduct").attachPatternMatched(this._onRouteMatched, this);
            //ruta para la vista de detalles de un registro
            this.getOwnerComponent().getRouter().getRoute("mdbroilerProduct_Record").attachPatternMatched(this._onRecordMatched, this);
            //ruta para la vista de creación de un registro
            this.getOwnerComponent().getRouter().getRoute("mdbroilerProduct_Create").attachPatternMatched(this._onCreateMatched, this);
        },

        /**
         * Coincidencia de ruta para acceder a la vista principal
         * @param  {Event} oEvent Evento que llamó esta función
         */
        _onRouteMatched: function(oEvent) {
            /**
             * @type {Controller} that         Referencia a este controlador
             * @type {JSONModel} util         Referencia al modelo "util"
             * @type {JSONModel} OS            Referencia al modelo "OS"
             * @type {JSONModel} MDSTAGE        Referencia al modelo "MdSTAGE"
             */

            let that = this,
                util = this.getView().getModel("util"),
                mdbroilerProduct = this.getView().getModel("MDBROILERPRODUCT");

            //dependiendo del dispositivo, establece la propiedad "phone"
            this.getView().getModel("util").setProperty("/phone/",
                this.getOwnerComponent().getContentDensityClass() === "sapUiSizeCozy");

            //establece MDSTAGE como la entidad seleccionada
            util.setProperty("/selectedEntity/", "mdbroilerProduct");

            //obtiene los registros de mdbroilerProduct
            this.onRead(that, util, mdbroilerProduct);
        },
        /**
         * Obtiene todos los registros de MDBROILERPRODUCT
         * @param  {Controller} that         Referencia al controlador que llama esta función
         * @param  {JSONModel} util         Referencia al modelo "util"
         * @param  {JSONModel} MDBROILERPRODUCT Referencia al modelo "MDBROILERPRODUCT"
         */
        onRead: function(that, util, mdbroilerProduct) {
            /** @type {Object} settings opciones de la llamada a la función ajax */
            var service = util.getProperty("/serviceUrl");
            var settings = {
                url: service+"/broiler_product",
                method: "GET",
                success: function(res) {
                    console.log(res.data);

                    util.setProperty("/busy/", false);
                    mdbroilerProduct.setProperty("/records/", res.data);
                    console.log(mdbroilerProduct);
                },
                error: function(err) {
                    util.setProperty("/error/status", err.status);
                    util.setProperty("/error/statusText", err.statusText);
                    //that.onConnectionError();
                }
            };
            console.log(util);
            util.setProperty("/busy/", true);
            mdbroilerProduct.setProperty("/records/", []);
            $.ajax(settings);
        },
        /**
         * Navega a la vista para crear un nuevo registro
         * @param  {Event} oEvent Evento que llamó esta función
         */
        onNewRecord: function(oEvent) {
            this.getRouter().navTo("mdbroilerProduct_Create", {}, false /*create history*/ );
        },
        /**
         * Coincidencia de ruta para acceder a la creación de un registro
         * @param  {Event} oEvent Evento que llamó esta función
         */
        _onCreateMatched: function(oEvent) {

            this._resetRecordValues();
            this._editRecordValues(true);
            this._editRecordRequired(true);
        },
        /**
         * Resetea todos los valores existentes en el formulario del registro
         */
        _resetRecordValues: function() {
            /**
             * @type {JSONModel} MDSTAGE Referencia al modelo "MDBROILERPRODUCT"
             */
            var mdbroilerProduct = this.getView().getModel("MDBROILERPRODUCT");

            mdbroilerProduct.setProperty("/name/editable", true);
            mdbroilerProduct.setProperty("/name/value", "");
            mdbroilerProduct.setProperty("/name/state", "None");
            mdbroilerProduct.setProperty("/name/stateText", "");

            mdbroilerProduct.setProperty("/weight/editable", true);
            mdbroilerProduct.setProperty("/weight/value", "");
            mdbroilerProduct.setProperty("/weight/state", "None");
            mdbroilerProduct.setProperty("/weight/stateText", "");

            mdbroilerProduct.setProperty("/days_eviction/editable", true);
            mdbroilerProduct.setProperty("/days_eviction/value", "");
            mdbroilerProduct.setProperty("/days_eviction/state", "None");
            mdbroilerProduct.setProperty("/days_eviction/stateText", "");

            mdbroilerProduct.setProperty("/code/editable", true);
            mdbroilerProduct.setProperty("/code/value", "");
            mdbroilerProduct.setProperty("/code/state", "None");
            mdbroilerProduct.setProperty("/code/stateText", "");

        },
        /**
         * Habilita/deshabilita la edición de los datos de un registro MDBROILERPRODUCT
         * @param  {Boolean} edit "true" si habilita la edición, "false" si la deshabilita
         */
        _editRecordValues: function(edit) {

            var mdbroilerProduct = this.getView().getModel("MDBROILERPRODUCT");
            mdbroilerProduct.setProperty("/name/editable", edit);
            mdbroilerProduct.setProperty("/weight/editable", edit);
            mdbroilerProduct.setProperty("/days_eviction/editable", edit);
        },
        /**
         * Se define un campo como obligatorio o no, de un registro MDBROILERPRODUCT
         * @param  {Boolean} edit "true" si es campo obligatorio, "false" si no es obligatorio
         */
        _editRecordRequired: function(edit) {
            var mdbroilerProduct = this.getView().getModel("MDBROILERPRODUCT");
            mdbroilerProduct.setProperty("/name/required", edit);
            mdbroilerProduct.setProperty("/weight/required", edit);
            mdbroilerProduct.setProperty("/days_eviction/required", edit);
            mdbroilerProduct.setProperty("/code/required", edit);
        },
        /**
       * Solicita al servicio correspondiente crear un registro MDBROILERPRODUCT
       * @param  {Event} oEvent Evento que llamó esta función
       */
        onCreate: function(oEvent) {
            //Si el registro que se desea crear es válido
            if (this._validRecord()) {
                console.log("Voy a insertar");
                var that = this,
                    util = this.getView().getModel("util"),
                    serviceUrl = util.getProperty("/serviceUrl"),
                    mdbroilerProduct = this.getView().getModel("MDBROILERPRODUCT");

                $.ajax({
                    type: "POST",
                    contentType: "application/json",
                    data: JSON.stringify({
                        "name": mdbroilerProduct.getProperty("/name/value"),
                        "weight": mdbroilerProduct.getProperty("/weight/value"),
                        "days_eviction": mdbroilerProduct.getProperty("/days_eviction/value"),
                        "code": mdbroilerProduct.getProperty("/code/value")
                    }),
                    url: serviceUrl+"/broiler_product",
                    dataType: "json",
                    async: true,
                    success: function(data) {
                        util.setProperty("/busy/", false);
                        that._resetRecordValues();
                        that.onToast(that.getI18n().getText("OS.recordCreated"));
                        that.getRouter().navTo("mdbroilerProduct", {}, true /*no history*/ );

                    },
                    error: function(request) {
                        var msg = request.statusText;
                        that.onToast("Error: "+msg);
                        console.log("Read failed: ", request);
                    }
                });

            }
        },

        validateFloatInput: function (o) {
            let input= o.getSource();
            let floatLength=10,
                intLength = 10;
            console.log("entro en la funcion v");
            let value = input.getValue();
            let regex = new RegExp(`/^([0-9]{1,${intLength}})([.][0-9]{0,${floatLength}})?$/`);
            if (regex.test(value)) {
                input.setValueState("None");
                input.setValueStateText("");
                return true;
            }
            else {
                let pNumber = 0;
                let aux = value
                    .split("")
                    .filter(char => {
                        if (/^[0-9.]$/.test(char)) {
                            if (char !== ".") {
                                return true;
                            }
                            else {
                                if (pNumber === 0) {
                                    pNumber++;
                                    return true;
                                }
                            }
                        }
                    })
                    .join("")
                    .split(".");
                value = aux[0].substring(0, intLength);

                if (aux[1] !== undefined) {
                    value += "." + aux[1].substring(0, floatLength);
                }
                input.setValue(value);
                return false;
            }
        },
        /**
       * Valida la correctitud de los datos existentes en el formulario del registro
       * @return {Boolean} Devuelve "true" si los datos son correctos, y "false" si son incorrectos
       */
        _validRecord: function() {
            /**
           * @type {JSONModel} MDBROILERPRODUCT Referencia al modelo "MDBROILERPRODUCT"
           * @type {Boolean} flag "true" si los datos son válidos, "false" si no lo son
           */
            let mdbroilerProduct = this.getView().getModel("MDBROILERPRODUCT"),
                flag = true,
                that = this,
                Without_SoL = /^\d+$/,
                Without_Num = /^[a-zA-Z\s]*$/;

            mdbroilerProduct.setProperty("/name/state", "None");
            mdbroilerProduct.setProperty("/weight/state", "None");
            mdbroilerProduct.setProperty("/days_eviction/state", "None");
            mdbroilerProduct.setProperty("/code/state", "None");

            mdbroilerProduct.setProperty("/name/stateText", "");
            mdbroilerProduct.setProperty("/weight/stateText", "");
            mdbroilerProduct.setProperty("/days_eviction/stateText", "");
            mdbroilerProduct.setProperty("/code/stateText", "");

          
            if (mdbroilerProduct.getProperty("/name/value") === "" || mdbroilerProduct.getProperty("/name/value") === null) {
                flag = false;
                mdbroilerProduct.setProperty("/name/state", "Error");
                mdbroilerProduct.setProperty("/name/stateText", this.getI18n().getText("enter.FIELD"));
            }

            if (mdbroilerProduct.getProperty("/weight/value") === "" || mdbroilerProduct.getProperty("/weight/value") === null) {
                flag = false;
                mdbroilerProduct.setProperty("/weight/state", "Error");
                mdbroilerProduct.setProperty("/weight/stateText", this.getI18n().getText("enter.FIELD"));
            }

            if (mdbroilerProduct.getProperty("/days_eviction/value") === "" || mdbroilerProduct.getProperty("/days_eviction/value") === null) {
                flag = false;
                mdbroilerProduct.setProperty("/days_eviction/state", "Error");
                mdbroilerProduct.setProperty("/days_eviction/stateText", this.getI18n().getText("enter.FIELD"));
            }

            if (mdbroilerProduct.getProperty("/code/value") === "" || mdbroilerProduct.getProperty("/code/value") === null) {
                flag = false;
                mdbroilerProduct.setProperty("/code/state", "Error");
                mdbroilerProduct.setProperty("/code/stateText", this.getI18n().getText("enter.FIELD"));
            }


            return flag;
        },
        /**
       * Coincidencia de ruta para acceder a los detalles de un registro
       * @param  {Event} oEvent Evento que llamó esta función
       */
        _onRecordMatched: function(oEvent) {

            this._viewOptions();

        },
        /**
       * Cambia las opciones de visualización disponibles en la vista de detalles de un registro
       */
        _viewOptions: function() {
            var mdbroilerProduct = this.getView().getModel("MDBROILERPRODUCT");
            mdbroilerProduct.setProperty("/save/", false);
            mdbroilerProduct.setProperty("/cancel/", false);
            mdbroilerProduct.setProperty("/modify/", true);
            mdbroilerProduct.setProperty("/delete/", true);

            this._editRecordValues(false);
            this._editRecordRequired(false);
        },
        /**
       * Solicita al servicio correspondiente actualizar un registro MDBROILERPRODUCT
       * @param  {Event} oEvent Evento que llamó esta función
       */
        onUpdate: function(oEvent) {
            /**
           * Si el registro que se quiere actualizar es válido y hubo algún cambio
           */
            if (this._validRecord() && this._recordChanged()) {
                /**
               * @type {JSONModel} MDBROILERPRODUCT       Referencia al modelo "MDBROILERPRODUCT"
               * @type {JSONModel} util         Referencia al modelo "util"
               * @type {Controller} that         Referencia a este controlador
               */
                var mdbroilerProduct = this.getView().getModel("MDBROILERPRODUCT");
                var util = this.getView().getModel("util");
                var serviceUrl = util.getProperty("/serviceUrl");
                var that = this;
                console.log("Actualizar el ID: ", mdbroilerProduct.getProperty("/selectedRecord/broiler_product_id/"));
                // console.log("VALOR DEL PESO:" + mdbroilerProduct.getProperty("/weight/value"));
                // console.log("DIAS DE DESALOJO:" + mdbroilerProduct.getProperty("/days_eviction/value"));
                $.ajax({
                    type: "PUT",
                    contentType: "application/json",
                    data: JSON.stringify({
                        "broiler_product_id": mdbroilerProduct.getProperty("/selectedRecord/broiler_product_id/"),
                        "name": mdbroilerProduct.getProperty("/name/value"),
                        "weight": mdbroilerProduct.getProperty("/weight/value"),
                        "days_eviction": mdbroilerProduct.getProperty("/days_eviction/value"),
                        "code": mdbroilerProduct.getProperty("/code/value")
                    }),
                    url: serviceUrl+"/broiler_product",
                    dataType: "json",
                    async: true,
                    success: function(data) {

                        util.setProperty("/busy/", false);
                        that._resetRecordValues();
                        that._viewOptions();
                        that.onToast(that.getI18n().getText("OS.recordUpdated"));
                        that.getRouter().navTo("mdbroilerProduct", {}, true /*no history*/ );

                    },
                    error: function(request) {
                        let msg = request.statusText;

                        that.onToast("Error: "+msg);
                        console.log("Read failed: ", request);
                    }
                });
            }
        },
        /**
       * Verifica si el registro seleccionado tiene algún cambio con respecto a sus valores originales
       * @return {Boolean} Devuelve "true" el registro cambió, y "false" si no cambió
       */
        _recordChanged: function() {
            /**
           * @type {JSONModel} MDBREED         Referencia al modelo "MDSTAGE"
           * @type {Boolean} flag            "true" si el registro cambió, "false" si no cambió
           */
            var mdbroilerProduct = this.getView().getModel("MDBROILERPRODUCT"),
                flag = false;

            if (mdbroilerProduct.getProperty("/name/value") !== mdbroilerProduct.getProperty(mdbroilerProduct.getProperty("/selectedRecordPath/") + "/name")
              || mdbroilerProduct.getProperty("/days_eviction/value") !== mdbroilerProduct.getProperty(mdbroilerProduct.getProperty("/selectedRecordPath/") + "/days_eviction")
              || mdbroilerProduct.getProperty("/weight/value") !== mdbroilerProduct.getProperty(mdbroilerProduct.getProperty("/selectedRecordPath/") + "/weight")
              || mdbroilerProduct.getProperty("/code/value") !== mdbroilerProduct.getProperty(mdbroilerProduct.getProperty("/selectedRecordPath/") + "/code") ) {
                flag = true;
            }

            if(!flag) this.onToast("No se detectaron cambios");

            return flag;
        },
        /**
      * Ajusta la vista para editar los datos de un registro
      * @param  {Event} oEvent Evento que llamó esta función
      */
        onEdit: function(oEvent) {

            var mdbroilerProduct = this.getView().getModel("MDBROILERPRODUCT");
            mdbroilerProduct.setProperty("/save/", true);
            mdbroilerProduct.setProperty("/cancel/", true);
            mdbroilerProduct.setProperty("/modify/", false);
            mdbroilerProduct.setProperty("/delete/", false);
            this._editRecordRequired(true);
            this._editRecordValues(true);
        },

        onVerifyIsUsed: async function(broiler_product_id){
            let ret;
    
            const response = await fetch("/broiler_product/isBeingUsed", {
                headers: {
                    "Content-Type": "application/json"
                },
                method: "POST",
                body: JSON.stringify({
                    broiler_product_id: broiler_product_id
                })
            });
    
            if (response.status !== 200 && response.status !== 409) {
                console.log("Looks like there was a problem. Status Code: " +
                response.status);
                return;
            }
            if(response.status === 200){
                const res = await response.json();
                console.log(res);
                ret = res.data.used;
            }
            console.log(response);
            console.log(ret);
            return ret;
        
        },

        onConfirmDelete: async function(oEvent) {

            let oBundle = this.getView().getModel("i18n").getResourceBundle(),
                deleteRecord = oBundle.getText("deleteRecord"),
                confirmation = oBundle.getText("confirmation"),
                util = this.getView().getModel("util"),
                serviceUrl = util.getProperty("/serviceUrl"),
                that = this,
                mdbroilerProduct = this.getView().getModel("MDBROILERPRODUCT"),
                broiler_product_id = mdbroilerProduct.getProperty("/selectedRecord/broiler_product_id");


            let cond = await this.onVerifyIsUsed(broiler_product_id);
            console.log("La cond el mio: ", cond);
            if(cond){
                var dialog = new Dialog({
                    title: "Información",
                    type: "Message",
                    state: "Warning",
                    content: new Text({
                        text: "No se puede eliminar el Producto, porque está siendo utilizado."
                    }),
                    beginButton: new Button({
                        text: "OK",
                        press: function() {
                            dialog.close();
                            that.confirmDeleteDlg.close();
                        }
                    }),
                    afterClose: function() {
                        dialog.destroy();
                    }
                });
        
                dialog.open();
            }else{
                let dialog = new Dialog({
                    title: confirmation,
                    type: "Message",
                    content: new sap.m.Text({
                        text: deleteRecord
                    }),

                    beginButton: new Button({
                        text: "Si",
                        press: function() {
                            util.setProperty("/busy/", true);
                                

                            $.ajax({
                                type: "DELETE",
                                contentType: "application/json",
                                data: JSON.stringify({
                                    "broiler_product_id": broiler_product_id
                                }),
                                url: serviceUrl+"/broiler_product",
                                dataType: "json",
                                async: true,
                                success: function(data) {

                                    util.setProperty("/busy/", false);
                                    that.getRouter().navTo("mdbroilerProduct", {}, true);
                                    dialog.close();
                                    dialog.destroy();

                                },
                                error: function(request, status, error) {
                                    that.onToast("Error de comunicación");
                                    console.log("Read failed");
                                }
                            });

                        }
                    }),
                    endButton: new Button({
                        text: "No",
                        press: function() {
                            dialog.close();
                            dialog.destroy();
                        }
                    })
                });

                dialog.open();
            }



         

        },



        /**
         * Cancela la creación de un registro MDSTAGE, y regresa a la vista principal
         * @param  {Event} oEvent Evento que llamó esta función
         */
        onCancelCreate: function(oEvent) {
            this._resetRecordValues();
            this.onNavBack(oEvent);
        },
        /**
         * Regresa a la vista principal de la entidad seleccionada actualmente
         * @param  {Event} oEvent Evento que llamó esta función
         */
        onNavBack: function(oEvent) {
            /** @type {JSONModel} OS Referencia al modelo "OS" */
            var util = this.getView().getModel("util");

            this.getRouter().navTo(util.getProperty("/selectedEntity"), {}, false /*create history*/ );
        },
        /**
         * Visualiza los detalles de un registro MDSTAGE
         * @param  {Event} oEvent Evento que llamó esta función
         */
        onViewBreedRecord: function(oEvent) {

            var mdbreed = this.getView().getModel("MDBREED");
            mdbreed.setProperty("/save/", false);
            mdbreed.setProperty("/cancel/", false);
            mdbreed.setProperty("/selectedRecordPath/", oEvent.getSource().getBindingContext("MDBREED"));
            mdbreed.setProperty("/breed_id/value", oEvent.getSource().getBindingContext("MDBREED").getObject().breed_id);
            mdbreed.setProperty("/code/value", oEvent.getSource().getBindingContext("MDBREED").getObject().code);
            mdbreed.setProperty("/name/value", oEvent.getSource().getBindingContext("MDBREED").getObject().name);
            mdbreed.setProperty("/type/value", oEvent.getSource().getBindingContext("MDBREED").getObject().type);
            this.getRouter().navTo("mdbreed_Record", {}, false /*create history*/ );
        },


        /**
         * Cancela la edición de un registro MDSTAGE
         * @param  {Event} oEvent Evento que llamó esta función
         */
        onCancelEdit: function(oEvent) {
            /** @type {JSONModel} MDSTAGE  Referencia al modelo MDSTAGE */

            this.onView();
        },
        /**
         * Ajusta la vista para visualizar los datos de un registro
         */
        onView: function() {
            this._viewOptions();
        },

        onbroilerProductSearch: function(oEvent){
            var aFilters = [],
                sQuery = oEvent.getSource().getValue(),
                binding = this.getView().byId("broilerProductTable").getBinding("items");

            if (sQuery && sQuery.length > 0) {
                /** @type {Object} filter1 Primer filtro de la búsqueda */
                var filter1 = new sap.ui.model.Filter("name", sap.ui.model.FilterOperator.Contains, sQuery);
                aFilters = new sap.ui.model.Filter([filter1]);
            }

            //se actualiza el binding de la lista
            binding.filter(aFilters);

        }

    });
});
