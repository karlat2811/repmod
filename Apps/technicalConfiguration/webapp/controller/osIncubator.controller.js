sap.ui.define([
    "technicalConfiguration/controller/BaseController",
    "sap/ui/model/json/JSONModel",
    "sap/m/Dialog",
    "sap/m/Button"
], function(BaseController, JSONModel,Dialog,Button) {
    "use strict";

    return BaseController.extend("technicalConfiguration.controller.osIncubator", {

        onInit: function() {
            //ruta para la vista principal
            this.getOwnerComponent().getRouter().getRoute("osIncubator").attachPatternMatched(this._onRouteMatched, this);
            //ruta para la vista de detalles de un registro
            this.getOwnerComponent().getRouter().getRoute("osIncubator_Record").attachPatternMatched(this._onRecordMatched, this);
            //ruta para la vista de creación de un registro
            this.getOwnerComponent().getRouter().getRoute("osIncubator_Create").attachPatternMatched(this._onCreateMatched, this);
        },

        /**
    		 * Coincidencia de ruta para acceder a la vista principal
    		 * @param  {Event} oEvent Evento que llamó esta función
    		 */
    		_onRouteMatched: function(oEvent) {
    			/**
    			 * @type {Controller} that          Referencia a este controlador
    			 * @type {JSONModel} dummy          Referencia al modelo "dummy"
    			 * @type {JSONModel} OS             Referencia al modelo "OS"
    			 * @type {JSONModel} PARTNERSHIP  Referencia al modelo "PARTNERSHIP"
    			 * @type {JSONModel} BROILERSFARM Referencia al modelo "BROILERSFARM"
    			 * @type {JSONModel} CENTER       Referencia al modelo "CENTER"
    			 */
    			 console.log("Llegue");
    			var that = this,
    				util = this.getView().getModel("util"),
    				ospartnership = this.getView().getModel("OSPARTNERSHIP"),
    				osincubatorplant = this.getView().getModel("OSINCUBATORPLANT"),
    				osincubator = this.getView().getModel("OSINCUBATOR");

    				//dependiendo del dispositivo, establece la propiedad "phone"
    				util.setProperty("/phone/",
    				this.getOwnerComponent().getContentDensityClass() === "sapUiSizeCozy");
    				console.log(util);

    			ospartnership.setProperty("/settings/tableMode", "SingleSelect");
    			osincubatorplant.setProperty("/settings/tableMode", "SingleSelect");
    			osincubator.setProperty("/settings/tableMode", "None");

    			//si la entidad seleccionada antes de acceder a esta vista es diferente a CENTER
    			if (util.getProperty("/selectedEntity") !== "osIncubator") {

    				//establece CENTER como la entidad seleccionada
    				util.setProperty("/selectedEntity", "osIncubator");

    				//establece el tab de la tabla PARTNERSHIP como el tab seleccionado
    				this.getView().byId("tabBar").setSelectedKey("kpartnershipFilter");

    				//limpio selectedRecord
    				ospartnership.setProperty("/selectedRecord", "");
    				osincubatorplant.setProperty("/selectedRecord", "");
    				//borra cualquier selección que se haya hecho en la tabla PARTNERSHIP
    				this.getView().byId("partnershipTable").removeSelections(true);

    				//borra cualquier selección que se haya hecho en la tabla incubatorPlant
    				this.getView().byId("incubatorPlantTable").removeSelections(true);

    				//establece que no hay ningún registro PARTNERSHIP seleccionado
    				ospartnership.setProperty("/selectedRecordPath/", "");

    				//establece que no hay ningún registro BROILERSFARM seleccionado
    				osincubatorplant.setProperty("/selectedRecordPath/", "");

    				osincubatorplant.setProperty("/records/", []);

    				osincubator.setProperty("/records/", []);

    				//deshabilita el tab de la tabla de registros incubatorPLant
    				osincubatorplant.setProperty("/settings/enabledTab", false);

    				//deshabilita el tab de la tabla de registros CENTER
    				osincubator.setProperty("/settings/enabledTab", false);

    				//deshabilita la opción de crear un registro CENTER
    				osincubator.setProperty("/new", false);

    				//obtiene los registros de PARTNERSHIP
    				sap.ui.controller("technicalConfiguration.controller.ospartnership").onRead(that, util, ospartnership);

    			} else if (ospartnership.getProperty("/selectedRecordPath/") !== "" &&
    				osincubatorplant.getProperty("/selectedRecordPath/") !== "") {

    				//habilita el tab de la tabla de registros BROILERSFARM
    				osincubatorplant.setProperty("/settings/enabledTab", true);

    				//habilita el tab de la tabla de registros CENTER
    				osincubator.setProperty("/settings/enabledTab", true);

    				//habilita la opción de crear un registro CENTER
    				osincubator.setProperty("/new", true);

    				//obtiene los registros de CENTER
    				this.onRead(that, util, ospartnership, osincubatorplant, osincubator);
    			}
    		},


        validateIntInput: function (o) {
            let input= o.getSource();
            let length = 10;
            let value = input.getValue();
            let regex = new RegExp(`/^[0-9]{1,${length}}$/`);

            if (regex.test(value)) {
                return true;
            }
            else {
                let aux = value
                    .split("")
                    .filter(char => {
                        if (/^[0-9]$/.test(char)) {
                            if (char !== ".") {
                                return true;
                            }
                        }
                    })
                    .join("");
                value = aux.substring(0, length);
                input.setValue(value);
                return false;
            }
        },

        
        onSelectPartnershipRecord: function(oEvent){

            var that = this,
                util = this.getView().getModel("util"),
                ospartnership = this.getView().getModel("OSPARTNERSHIP"),
                osincubatorplant = this.getView().getModel("OSINCUBATORPLANT");

            //guarda la ruta del registro PARTNERSHIP que fue seleccionado
            ospartnership.setProperty("/selectedRecordPath/", oEvent.getSource()["_aSelectedPaths"][0]);
            ospartnership.setProperty("/selectedRecord/", ospartnership.getProperty(ospartnership.getProperty("/selectedRecordPath/")));

            //habilita el tab de la tabla de registros BROILERSFARM
            osincubatorplant.setProperty("/settings/enabledTab", true);

            //habilita la opción de crear un registro BROILERSFARM
            osincubatorplant.setProperty("/new", true);

            //establece el tab de la tabla incubatorPlant como el tab seleccionado
            this.getView().byId("tabBar").setSelectedKey("kincubatorPlantFilter");


            sap.ui.controller("technicalConfiguration.controller.osIncubatorPlant").onRead(that, util, ospartnership, osincubatorplant);


        },
        /**
       * @type {Controller} that          Referencia a este controlador
       * @type {JSONModel} dummy          Referencia al modelo "dummy"
       * @type {JSONModel} PARTNERSHIP  Referencia al modelo "PARTNERSHIP"
       * @type {JSONModel} BROILERSFARM Referencia al modelo "BROILERSFARM"
       * @type {JSONModel} CENTER       Referencia al modelo "CENTER"
       */

        onSelectIncubatorPlantRecord: function(oEvent){
            var that = this,
                util = this.getView().getModel("util"),
                ospartnership = this.getView().getModel("OSPARTNERSHIP"),
                osincubatorplant = this.getView().getModel("OSINCUBATORPLANT"),
                osincubator = this.getView().getModel("OSINCUBATOR");

            //guarda la ruta del registro BROILERSFARM que fue seleccionado
            osincubatorplant.setProperty("/selectedRecordPath/", oEvent.getSource()["_aSelectedPaths"][0]);

            osincubatorplant.setProperty("/selectedRecord/", osincubatorplant.getProperty(osincubatorplant.getProperty("/selectedRecordPath/")));

            //habilita el tab de la tabla de registros CENTER
            osincubator.setProperty("/settings/enabledTab", true);

            //habilita la opción de crear un registro CENTER
            osincubator.setProperty("/new", true);

            //establece el tab de la tabla CENTER como el tab seleccionado
            this.getView().byId("tabBar").setSelectedKey("kincubatorFilter");

            //obtiene los registros de CENTER
            this.onRead(that, util, ospartnership, osincubatorplant, osincubator);
        },
        /**
    		 * Obtiene todos los registros de BROILERSFARM, dado un cliente y una sociedad
    		 * @param  {Controller} that          Referencia al controlador que llama esta función
    		 * @param  {JSONModel} dummy          Referencia al modelo "dummy"
    		 * @param  {JSONModel} PARTNERSHIP  Referencia al modelo "PARTNERSHIP"
    		 * @param  {JSONModel} BROILERSFARM Referencia al modelo "BROILERSFARM"
    		 * @param  {JSONModel} CENTER       Referencia al modelo "CENTER"
    		 */
    		onRead: function(that, util, ospartnership, osincubatorplant, osincubator) {
    			/** @type {Object} settings opciones de la llamada a la función ajax */
    			var serviceUrl = util.getProperty("/serviceUrl");
    			var incubator_plant_id = osincubatorplant.getProperty(osincubatorplant.getProperty("/selectedRecordPath/") + "/incubator_plant_id");
    			console.log(incubator_plant_id);
    			var settings = {
    				type: "POST",
    				contentType: "application/json",
    				data: JSON.stringify({
    						"incubator_plant_id": incubator_plant_id,
    				}),
    				url: serviceUrl+"/incubator/findIncubatorByPlant",
    				dataType: "json",
    				async: true,
    				success: function(res) {
    						console.log(res.data);
    						util.setProperty("/busy/", false);
    						osincubator.setProperty("/records/", res.data);
    						//console.log(oscenter);
    				},
    				error: function(err) {
    						util.setProperty("/error/status", err.status);
    						util.setProperty("/error/statusText", err.statusText);
    						//that.onConnectionError();
    				}
    			};
    			util.setProperty("/busy/", true);
    			//borra los registros CENTER que estén almacenados actualmente
    			osincubator.setProperty("/records/", []);
    			//realiza la llamada ajax
    			$.ajax(settings);
    		},


        onPartnershipSearch: function(oEvent){
            var aFilters = [],
                sQuery = oEvent.getSource().getValue(),
                binding = this.getView().byId("partnershipTable").getBinding("items");

            if (sQuery && sQuery.length > 0) {
                /** @type {Object} filter1 Primer filtro de la búsqueda */
                var filter1 = new sap.ui.model.Filter("name", sap.ui.model.FilterOperator.Contains, sQuery);
                aFilters = new sap.ui.model.Filter([filter1]);
            }

            //se actualiza el binding de la lista
            binding.filter(aFilters);

        },


        onIncubatorPlantSearch: function(oEvent){
            var aFilters = [],
                sQuery = oEvent.getSource().getValue(),
                binding = this.getView().byId("incubatorPlantTable").getBinding("items");

            if (sQuery && sQuery.length > 0) {
                /** @type {Object} filter1 Primer filtro de la búsqueda */
                var filter1 = new sap.ui.model.Filter("name", sap.ui.model.FilterOperator.Contains, sQuery);
                aFilters = new sap.ui.model.Filter([filter1]);
            }
            //se actualiza el binding de la lista
            binding.filter(aFilters);

        },

        onIncubatorSearch: function(oEvent){
            var aFilters = [],
                sQuery = oEvent.getSource().getValue(),
                binding = this.getView().byId("incubatorTable").getBinding("items");

            if (sQuery && sQuery.length > 0) {
            /** @type {Object} filter1 Primer filtro de la búsqueda */
                var filter1 = new sap.ui.model.Filter("name", sap.ui.model.FilterOperator.Contains, sQuery);
                aFilters = new sap.ui.model.Filter([filter1]);
            }

            //se actualiza el binding de la lista
            binding.filter(aFilters);

        },
        /**
       * Navega a la vista para crear un nuevo registro
       * @param  {Event} oEvent Evento que llamó esta función
       */
        onNewRecord: function(oEvent) {
            this.getRouter().navTo("osIncubator_Create", {}, false /*create history*/ );
        },
        /**
       * Coincidencia de ruta para acceder a la creación de un registro
       * @param  {Event} oEvent Evento que llamó esta función
       */
        _onCreateMatched: function(oEvent) {
            this._resetRecordValues();
            this._editRecordValues(true);
            this._editRecordRequired(true);
        },
        /**
       * Resetea todos los valores existentes en el formulario del registro
       */
        _resetRecordValues: function() {
            /**
           * @type {JSONModel} OSINCUBATORPLANT Referencia al modelo "OSINCUBATORPLANT"
           */
            var osincubator = this.getView().getModel("OSINCUBATOR");

            osincubator.setProperty("/name/editable", true);
            osincubator.setProperty("/name/value", "");
            osincubator.setProperty("/name/state", "None");
            osincubator.setProperty("/name/stateText", "");

            osincubator.setProperty("/code/editable", true);
            osincubator.setProperty("/code/value", "");
            osincubator.setProperty("/code/state", "None");
            osincubator.setProperty("/code/stateText", "");

            osincubator.setProperty("/description/editable", true);
            osincubator.setProperty("/description/value", "");
            osincubator.setProperty("/description/state", "None");
            osincubator.setProperty("/description/stateText", "");

            osincubator.setProperty("/capacity/editable", true);
            osincubator.setProperty("/capacity/value", "");
            osincubator.setProperty("/capacity/state", "None");
            osincubator.setProperty("/capacity/stateText", "");

            osincubator.setProperty("/sunday/enabled", true);
            osincubator.setProperty("/sunday/value", 0);

            osincubator.setProperty("/monday/enabled", true);
            osincubator.setProperty("/monday/value", 0);

            osincubator.setProperty("/tuesday/enabled", true);
            osincubator.setProperty("/tuesday/value", 0);

            osincubator.setProperty("/wednesday/enabled", true);
            osincubator.setProperty("/wednesday/value", 0);

            osincubator.setProperty("/thursday/enabled", true);
            osincubator.setProperty("/thursday/value", 0);

            osincubator.setProperty("/friday/enabled", true);
            osincubator.setProperty("/friday/value", 0);

            osincubator.setProperty("/saturday/enabled", true);
            osincubator.setProperty("/saturday/value", 0);

        },
        /**
       * Habilita/deshabilita la edición de los datos de un registro OSINCUBATORPLANT
       * @param  {Boolean} edit "true" si habilita la edición, "false" si la deshabilita
       */
        _editRecordValues: function(edit) {

            var osincubator = this.getView().getModel("OSINCUBATOR");
            osincubator.setProperty("/name/editable", edit);
            osincubator.setProperty("/code/editable", edit);
            osincubator.setProperty("/description/editable", edit);
            osincubator.setProperty("/capacity/editable", edit);

            osincubator.setProperty("/sunday/enabled", edit);
            osincubator.setProperty("/monday/enabled", edit);
            osincubator.setProperty("/tuesday/enabled", edit);
            osincubator.setProperty("/wednesday/enabled", edit);
            osincubator.setProperty("/thursday/enabled", edit);
            osincubator.setProperty("/friday/enabled", edit);
            osincubator.setProperty("/saturday/enabled", edit);

        },
        /**
       * Se define un campo como obligatorio o no, de un registro MDSTAGE
       * @param  {Boolean} edit "true" si es campo obligatorio, "false" si no es obligatorio
       */
        _editRecordRequired: function(edit) {
            var osincubator = this.getView().getModel("OSINCUBATOR");
            osincubator.setProperty("/name/required", edit);
            osincubator.setProperty("/code/required", edit);
            osincubator.setProperty("/description/required", edit);
            osincubator.setProperty("/capacity/required", edit);

        },
        /**
      * Solicita al servicio correspondiente crear un registro MDSTAGE
      * @param  {Event} oEvent Evento que llamó esta función
      */
        onCreate: function(oEvent) {
        //Si el registro que se desea crear es válido
            console.log("Validando");
            if (this._validRecord()) {
                console.log("Voy a insertar");
                var osincubator = this.getView().getModel("OSINCUBATOR"),
                    osincubatorplant = this.getView().getModel("OSINCUBATORPLANT"),
                    util = this.getView().getModel("util"),
                    that = this,
                    serviceUrl= util.getProperty("/serviceUrl");

                $.ajax({
                    type: "POST",
                    contentType: "application/json",
                    data: JSON.stringify({
                        "incubator_plant_id" :	osincubatorplant.getProperty(osincubatorplant.getProperty("/selectedRecordPath/") + "/incubator_plant_id"),
                        "name": osincubator.getProperty("/name/value"),
                        "code": osincubator.getProperty("/code/value"),
                        "description": osincubator.getProperty("/description/value"),
                        "capacity": osincubator.getProperty("/capacity/value"),
                        "sunday": osincubator.getProperty("/sunday/value") == true? 1:0,
                        "monday": osincubator.getProperty("/monday/value")== true? 1:0,
                        "tuesday": osincubator.getProperty("/tuesday/value")== true? 1:0,
                        "wednesday": osincubator.getProperty("/wednesday/value")== true? 1:0,
                        "thursday": osincubator.getProperty("/thursday/value")== true? 1:0,
                        "friday": osincubator.getProperty("/friday/value")== true? 1:0,
                        "saturday": osincubator.getProperty("/saturday/value")== true? 1:0,
                        "available": osincubator.getProperty("/capacity/value")
                    }),
                    url: serviceUrl+"/incubator/",
                    dataType: "json",
                    async: true,
                    success: function(data) {
                        util.setProperty("/busy/", false);
                        that._resetRecordValues();
                        that.onToast(that.getI18n().getText("OS.recordCreated"));
                        that.getRouter().navTo("osIncubator", {}, true /*no history*/ );

                    },
                    error: function(error) {
                        that.onToast("Error: " + error.responseText);
                        console.log("Read failed ");
                    }
                });

            }
        },
        /**
      * Valida la correctitud de los datos existentes en el formulario del registro
      * @return {Boolean} Devuelve "true" si los datos son correctos, y "false" si son incorrectos
      */
        _validRecord: function() {
        /**
         * @type {Boolean} flag "true" si los datos son válidos, "false" si no lo son
         */
            var osincubator = this.getView().getModel("OSINCUBATOR"),
                flag = true,
                that = this,
                Without_SoL = /^\d+$/,
                Without_Num = /^[a-zA-Z\s]*$/;

            if (osincubator.getProperty("/name/value") === "") {
                flag = false;
                console.log("1");
                osincubator.setProperty("/name/state", "Error");
                osincubator.setProperty("/name/stateText", this.getI18n().getText("enter.FIELD"));
            } else {
                osincubator.setProperty("/name/state", "None");
                osincubator.setProperty("/name/stateText", "");
            }

            if (osincubator.getProperty("/code/value") === "") {
                flag = false;
                console.log("2");
                osincubator.setProperty("/code/state", "Error");
                osincubator.setProperty("/code/stateText", this.getI18n().getText("enter.FIELD"));
            } else {
                osincubator.setProperty("/code/state", "None");
                osincubator.setProperty("/code/stateText", "");
            }

            if (osincubator.getProperty("/description/value") === "") {
                flag = false;
                console.log("3");
                osincubator.setProperty("/description/state", "Error");
                osincubator.setProperty("/description/stateText", this.getI18n().getText("enter.FIELD"));
            } else {
                osincubator.setProperty("/description/state", "None");
                osincubator.setProperty("/description/stateText", "");
            }

            if (osincubator.getProperty("/capacity/value") === "") {
                flag = false;
                console.log("4");
                osincubator.setProperty("/capacity/state", "Error");
                osincubator.setProperty("/capacity/stateText", this.getI18n().getText("enter.FIELD"));
            } else {
                osincubator.setProperty("/capacity/state", "None");
                osincubator.setProperty("/capacity/stateText", "");
            }
            console.log(flag);
            return flag;
        },
        /**
       * Regresa a la vista principal de la entidad seleccionada actualmente
       * @param  {Event} oEvent Evento que llamó esta función
       */
        onNavBack: function(oEvent) {
            /** @type {JSONModel} OS Referencia al modelo "OS" */
            var util = this.getView().getModel("util");

            this.getRouter().navTo(util.getProperty("/selectedEntity"), {}, false /*create history*/ );
        },
        /**
       * Coincidencia de ruta para acceder a los detalles de un registro
       * @param  {Event} oEvent Evento que llamó esta función
       */
        _onRecordMatched: function(oEvent) {

            this._viewOptions();

        },
        /**
       * Cambia las opciones de visualización disponibles en la vista de detalles de un registro
       */
        _viewOptions: function() {
            console.log("entro a la funcion");
            var osincubator = this.getView().getModel("OSINCUBATOR");
            osincubator.setProperty("/save/", false);
            osincubator.setProperty("/cancel/", false);
            osincubator.setProperty("/modify/", true);
            osincubator.setProperty("/delete/", true);
            this._editRecordValues(false);
            this._editRecordRequired(false);
        },
        /**
      * Ajusta la vista para editar los datos de un registro
      * @param  {Event} oEvent Evento que llamó esta función
      */
        onEdit: function(oEvent) {

            var osincubator = this.getView().getModel("OSINCUBATOR");
            osincubator.setProperty("/save/", true);
            osincubator.setProperty("/cancel/", true);
            osincubator.setProperty("/modify/", false);
            osincubator.setProperty("/delete/", false);
            this._editRecordRequired(true);
            this._editRecordValues(true);

        },
        /**
      * Cancela la edición de un registro OSINCUBATORPLANT
      * @param  {Event} oEvent Evento que llamó esta función
      */
        onCancelEdit: function(oEvent) {
            /** @type {JSONModel} OSINCUBATORPLANT  Referencia al modelo OSINCUBATORPLANT */

            this.onView();
        },
        /**
      * Cancela la creación de un registro OSINCUBATORPLANT, y regresa a la vista principal
      * @param  {Event} oEvent Evento que llamó esta función
      */
        onCancelCreate: function(oEvent) {
            this._resetRecordValues();
            this.onNavBack(oEvent);
        },
        /**
      * Ajusta la vista para visualizar los datos de un registro
      */
        onView: function() {
            this._viewOptions();
        },
        onConfirmDelete: function(oEvent){

            var oBundle = this.getView().getModel("i18n").getResourceBundle();
            var deleteRecord = oBundle.getText("deleteRecord");
            var confirmation = oBundle.getText("confirmation");
            var util = this.getView().getModel("util");

            var that = this;
            var dialog = new Dialog({
                title: confirmation,
                type: "Message",
                content: new sap.m.Text({
                    text: deleteRecord
                }),

                beginButton: new Button({
                    text: "Si",
                    press: function() {
                        util.setProperty("/busy/", true);
                        var osincubator = that.getView().getModel("OSINCUBATOR");
                        var serviceUrl= util.getProperty("/serviceUrl");
                        $.ajax({
                            type: "DELETE",
                            contentType: "application/json",
                            data: JSON.stringify({
                                "incubator_id": osincubator.getProperty(osincubator.getProperty("/selectedRecordPath/") + "/incubator_id")
                            }),
                            url: serviceUrl+"/incubator/",
                            dataType: "json",
                            async: true,
                            success: function(data) {

                                util.setProperty("/busy/", false);
                                that.getRouter().navTo("osIncubator", {}, true);
                                dialog.close();
                                dialog.destroy();

                            },
                            error: function(request, status, error) {
                                that.onToast("Error de comunicación");
                                console.log("Read failed");
                            }
                        });

                    }
                }),
                endButton: new Button({
                    text: "No",
                    press: function() {
                        dialog.close();
                        dialog.destroy();
                    }
                })
            });

            dialog.open();

        },
        /**
       * Solicita al servicio correspondiente actualizar un registro MDSTAGE
       * @param  {Event} oEvent Evento que llamó esta función
       */
        onUpdate: function(oEvent) {
            /**
           * Si el registro que se quiere actualizar es válido y hubo algún cambio
           * con respecto a sus datos originales
           */
            if (this._validRecord() && this._recordChanged()) {
                /**
               * @type {JSONModel} MDSTAGE       Referencia al modelo "MDSTAGE"
               * @type {JSONModel} util         Referencia al modelo "util"
               * @type {Controller} that         Referencia a este controlador
               */
                var osincubator = this.getView().getModel("OSINCUBATOR");
                var util = this.getView().getModel("util");
                var that = this;
                var serviceUrl= util.getProperty("/serviceUrl");
                $.ajax({
                    type: "PUT",
                    contentType: "application/json",
                    data: JSON.stringify({

                        "incubator_id": osincubator.getProperty(osincubator.getProperty("/selectedRecordPath/") + "/incubator_id"),
                        "name": osincubator.getProperty("/name/value"),
                        "code": osincubator.getProperty("/code/value"),
                        "description": osincubator.getProperty("/description/value"),
                        "capacity": osincubator.getProperty("/capacity/value"),
                        "sunday": osincubator.getProperty("/sunday/value") == true? 1:0,
                        "monday": osincubator.getProperty("/monday/value")== true? 1:0,
                        "tuesday": osincubator.getProperty("/tuesday/value")== true? 1:0,
                        "wednesday": osincubator.getProperty("/wednesday/value")== true? 1:0,
                        "thursday": osincubator.getProperty("/thursday/value")== true? 1:0,
                        "friday": osincubator.getProperty("/friday/value")== true? 1:0,
                        "saturday": osincubator.getProperty("/saturday/value")== true? 1:0,
                        "available": osincubator.getProperty("/available/value")
                    }),
                    url: serviceUrl+"/incubator/",
                    dataType: "json",
                    async: true,
                    success: function(data) {

                        util.setProperty("/busy/", false);
                        that._resetRecordValues();
                        that._viewOptions();
                        that.onToast(that.getI18n().getText("OS.recordUpdated"));
                        that.getRouter().navTo("osIncubator", {}, true /*no history*/ );

                    },
                    error: function(request, status, error) {
                        that.onToast("Error de comunicación");
                        console.log("Read failed");
                    }
                });
            }
        },
        /**
       * Verifica si el registro seleccionado tiene algún cambio con respecto a sus valores originales
       * @return {Boolean} Devuelve "true" el registro cambió, y "false" si no cambió
       */
        _recordChanged: function() {
            /**
           * @type {JSONModel} OSINCUBATORPLANT         Referencia al modelo "OSINCUBATORPLANT"
           * @type {Boolean} flag            "true" si el registro cambió, "false" si no cambió
           */
            var osincubator = this.getView().getModel("OSINCUBATOR"),
                flag = false;

            if (osincubator.getProperty("/name/value") !== osincubator.getProperty(osincubator.getProperty("/selectedRecordPath/") + "/name")) {
                flag = true;
            }

            if (osincubator.getProperty("/code/value") !== osincubator.getProperty(osincubator.getProperty("/selectedRecordPath/") + "/code")) {
                flag = true;
            }

            if (osincubator.getProperty("/description/value") !== osincubator.getProperty(osincubator.getProperty("/selectedRecordPath/") + "/description")) {
                flag = true;
            }

            if (osincubator.getProperty("/capacity/value") !== osincubator.getProperty(osincubator.getProperty("/selectedRecordPath/") + "/capacity")) {
                flag = true;
            }

            if (osincubator.getProperty("/sunday/value") !== osincubator.getProperty(osincubator.getProperty("/selectedRecordPath/") + "/sunday")) {
                flag = true;
            }
            if (osincubator.getProperty("/monday/value") !== osincubator.getProperty(osincubator.getProperty("/selectedRecordPath/") + "/monday")) {
                flag = true;
            }
            if (osincubator.getProperty("/tuesday/value") !== osincubator.getProperty(osincubator.getProperty("/selectedRecordPath/") + "/tuesday")) {
                flag = true;
            }
            if (osincubator.getProperty("/wednesday/value") !== osincubator.getProperty(osincubator.getProperty("/selectedRecordPath/") + "/wednesday")) {
                flag = true;
            }
            if (osincubator.getProperty("/thursday/value") !== osincubator.getProperty(osincubator.getProperty("/selectedRecordPath/") + "/thursday")) {
                flag = true;
            }
            if (osincubator.getProperty("friday/value") !== osincubator.getProperty(osincubator.getProperty("/selectedRecordPath/") + "/friday")) {
                flag = true;
            }
            if (osincubator.getProperty("/saturday/value") !== osincubator.getProperty(osincubator.getProperty("/selectedRecordPath/") + "/saturday")) {
                flag = true;
            }

            if(!flag) this.onToast("No se detectaron cambios");

            return flag;
        }

    });
});
