sap.ui.define([
    "technicalConfiguration/controller/BaseController",
    "sap/ui/model/json/JSONModel",
    "sap/m/Dialog",
    "sap/m/Button"
], function(BaseController, JSONModel,Dialog,Button) {
    "use strict";

    return BaseController.extend("technicalConfiguration.controller.osslaughterhouse", {

        onInit: function() {
            //ruta para la vista principal
            this.getOwnerComponent().getRouter().getRoute("osslaughterhouse").attachPatternMatched(this._onRouteMatched, this);
            //ruta para la vista de detalles de un registro
            this.getOwnerComponent().getRouter().getRoute("osslaughterhouse_Record").attachPatternMatched(this._onRecordMatched, this);
            //ruta para la vista de creaci�n de un registro
            this.getOwnerComponent().getRouter().getRoute("osslaughterhouse_Create").attachPatternMatched(this._onCreateMatched, this);
        },

        /**
         * Coincidencia de ruta para acceder a la vista principal
         * @param  {Event} oEvent Evento que llam� esta funci�n
         */
        _onRouteMatched: function(oEvent) {
            /**
             * @type {Controller} that         Referencia a este controlador
             * @type {JSONModel} util         Referencia al modelo "util"
             * @type {JSONModel} OS            Referencia al modelo "OS"
             * @type {JSONModel} MDOSSLAUGHTERHOUSE        Referencia al modelo "osslaughterhouse"
             */

            var that = this,
                util = this.getView().getModel("util"),
                osslaughterhouse = this.getView().getModel("OSSLAUGHTERHOUSE");

            //dependiendo del dispositivo, establece la propiedad "phone"
            this.getView().getModel("util").setProperty("/phone/",
                this.getOwnerComponent().getContentDensityClass() === "sapUiSizeCozy");


            //establece osslaughterhouse como la entidad seleccionada
            util.setProperty("/selectedEntity/", "osslaughterhouse");
            osslaughterhouse.setProperty("/settings/tableMode", "None");

            //obtiene los registros de osslaughterhouse
            this.onRead(that, util, osslaughterhouse);
        },
        /**
         * Obtiene todos los registros de osslaughterhouse
         * @param  {Controller} that         Referencia al controlador que llama esta funci�n
         * @param  {JSONModel} util         Referencia al modelo "util"
         * @param  {JSONModel} osslaughterhouse Referencia al modelo "osslaughterhouse"
         */
        onRead: function(that, util, osslaughterhouse) {
            console.log("llegue");
            /** @type {Object} settings opciones de la llamada a la funci�n ajax */
            var serviceUrl = util.getProperty("/serviceUrl");
            var settings = {
                url: serviceUrl+"/slaughterhouse",
                method: "GET",
                success: function(res) {
                    console.log(res.data);
                    util.setProperty("/busy/", false);
                    osslaughterhouse.setProperty("/records/", res.data);

                },
                error: function(err) {
                    util.setProperty("/error/status", err.status);
                    //==QuitarLuego========================================//
                    util.setProperty("/busy/", false);
                    //==QuitarLuego========================================//
                    util.setProperty("/error/statusText", err.statusText);
                    //that.onConnectionError();
                }
            };
            console.log(util);
            util.setProperty("/busy/", true);
            //borra los registros OSSLAUGHTERHOUSE que est�n almacenados actualmente
            osslaughterhouse.setProperty("/records/", []);
            //realiza la llamada ajax
            $.ajax(settings);
        },
        /**
         * Coincidencia de ruta para acceder a la creaci�n de un registro
         * @param  {Event} oEvent Evento que llam� esta funci�n
         */
        _onCreateMatched: function(oEvent) {

            this._resetRecordValues();
            this._editRecordValues(true);
            this._editRecordRequired(true);
        },
        /**
         * Resetea todos los valores existentes en el formulario del registro
         */
        _resetRecordValues: function() {
            /**
             * @type {JSONModel} osslaughterhouse Referencia al modelo "osslaughterhouse"
             */
            var osslaughterhouse = this.getView().getModel("OSSLAUGHTERHOUSE");

            osslaughterhouse.setProperty("/slaughterhouse_id/value", "");

            osslaughterhouse.setProperty("/name/editable", true);
            osslaughterhouse.setProperty("/name/value", "");
            osslaughterhouse.setProperty("/name/state", "None");
            osslaughterhouse.setProperty("/name/stateText", "");

            osslaughterhouse.setProperty("/code/editable", true);
            osslaughterhouse.setProperty("/code/value", "");
            osslaughterhouse.setProperty("/code/state", "None");
            osslaughterhouse.setProperty("/code/stateText", "");

            osslaughterhouse.setProperty("/capacity/editable", true);
            osslaughterhouse.setProperty("/capacity/value", "");
            osslaughterhouse.setProperty("/capacity/state", "None");
            osslaughterhouse.setProperty("/capacity/stateText", "");

            osslaughterhouse.setProperty("/description/editable", true);
            osslaughterhouse.setProperty("/description/value", "");
            osslaughterhouse.setProperty("/description/state", "None");
            osslaughterhouse.setProperty("/description/stateText", "");

            osslaughterhouse.setProperty("/address/editable", true);
            osslaughterhouse.setProperty("/address/value", "");
            osslaughterhouse.setProperty("/address/state", "None");
            osslaughterhouse.setProperty("/address/stateText", "");


        },
        /**
         * Habilita/deshabilita la edici�n de los datos de un registro osslaughterhouse
         * @param  {Boolean} edit "true" si habilita la edici�n, "false" si la deshabilita
         */
        _editRecordValues: function(edit) {

            var osslaughterhouse = this.getView().getModel("OSSLAUGHTERHOUSE");
            osslaughterhouse.setProperty("/name/editable", edit);
            osslaughterhouse.setProperty("/code/editable", edit);
            osslaughterhouse.setProperty("/capacity/editable", edit);
            osslaughterhouse.setProperty("/description/editable", edit);
            osslaughterhouse.setProperty("/address/editable", edit);
        },
        /**
         * Se define un campo como obligatorio o no, de un registro osslaughterhouse
         * @param  {Boolean} edit "true" si es campo obligatorio, "false" si no es obligatorio
         */
        _editRecordRequired: function(edit) {
            var osslaughterhouse = this.getView().getModel("OSSLAUGHTERHOUSE");

            osslaughterhouse.setProperty("/name/required", edit);
            osslaughterhouse.setProperty("/code/required", edit);
            osslaughterhouse.setProperty("/capacity/required", edit);
            osslaughterhouse.setProperty("/description/required", edit);
            osslaughterhouse.setProperty("/address/required", edit);
        },


        validateIntInput: function (o) {
            let input= o.getSource();
            let length = 10;
            let value = input.getValue();
            let regex = new RegExp(`/^[0-9]{1,${length}}$/`);

            if (regex.test(value)) {
                return true;
            }
            else {
                let aux = value
                    .split("")
                    .filter(char => {
                        if (/^[0-9]$/.test(char)) {
                            if (char !== ".") {
                                return true;
                            }
                        }
                    })
                    .join("");
                value = aux.substring(0, length);
                input.setValue(value);
                return false;
            }
        },
        
        /**
         * Navega a la vista para crear un nuevo registro
         * @param  {Event} oEvent Evento que llam� esta funci�n
         */
        onNewRecord: function(oEvent) {
            this.getRouter().navTo("osslaughterhouse_Create", {}, false /*create history*/ );
        },
        /**
         * Cancela la creaci�n de un registro osslaughterhouse, y regresa a la vista principal
         * @param  {Event} oEvent Evento que llam� esta funci�n
         */
        onCancelCreate: function(oEvent) {
            this._resetRecordValues();
            this.onNavBack(oEvent);
        },
        /**
         * Regresa a la vista principal de la entidad seleccionada actualmente
         * @param  {Event} oEvent Evento que llam� esta funci�n
         */
        onNavBack: function(oEvent) {
            /** @type {JSONModel} OS Referencia al modelo "OS" */
            var util = this.getView().getModel("util");

            this.getRouter().navTo(util.getProperty("/selectedEntity"), {}, false /*create history*/ );
        },
        /**
         * Solicita al servicio correspondiente crear un registro osslaughterhouse
         * @param  {Event} oEvent Evento que llam� esta funci�n
         */
        onCreate: function(oEvent) {
            //Si el registro que se desea crear es v�lido
            if (this._validRecord()) {
                var that = this,
                    util = this.getView().getModel("util"),
                    osslaughterhouse = this.getView().getModel("OSSLAUGHTERHOUSE"),
                    serviceUrl = util.getProperty("/serviceUrl");

                console.log("la ruta");
                console.log(serviceUrl);

                /*console.log("la capacidad");
                    console.log(osslaughterhouse.getProperty("/capacity/value"));*/
                $.ajax({
                    type: "POST",
                    contentType: "application/json",
                    data: JSON.stringify({
                        "name": osslaughterhouse.getProperty("/name/value"),
                        "code": osslaughterhouse.getProperty("/code/value"),
                        "capacity": osslaughterhouse.getProperty("/capacity/value"),
                        "description": osslaughterhouse.getProperty("/description/value"),
                        "address": osslaughterhouse.getProperty("/address/value"),
                        "capacity": osslaughterhouse.getProperty("/capacity/value"),
                        
                    }),
                    url: serviceUrl+"/slaughterhouse",
                    dataType: "json",
                    async: true,
                    success: function(data) {
                        util.setProperty("/busy/", false);
                        that._resetRecordValues();
                        that.onToast(that.getI18n().getText("OS.recordCreated"));
                        that.getRouter().navTo("osslaughterhouse", {}, true /*no history*/ );

                    },
                    error: function(error) {
                        that.onToast("Error: " + error.responseText);
                        console.log("Read failed ");
                    }


                });
                console.log("salio perfecto");
            }
        },
        /**
         * Valida la correctitud de los datos existentes en el formulario del registro
         * @return {Boolean} Devuelve "true" si los datos son correctos, y "false" si son incorrectos
         */
        _validRecord: function() {
            /**
             * @type {JSONModel} osslaughterhouse Referencia al modelo "osslaughterhouse"
             * @type {Boolean} flag "true" si los datos son v�lidos, "false" si no lo son
             */
            var osslaughterhouse = this.getView().getModel("OSSLAUGHTERHOUSE"),
                flag = true,
                that = this;

            if (osslaughterhouse.getProperty("/name/value") === "") {
                flag = false;
                osslaughterhouse.setProperty("/name/state", "Error");
                osslaughterhouse.setProperty("/name/stateText", this.getI18n().getText("enter.FIELD"));
            } else {
                osslaughterhouse.setProperty("/name/state", "None");
                osslaughterhouse.setProperty("/name/stateText", "");
            }

            if (osslaughterhouse.getProperty("/code/value") === "") {
                flag = false;
                osslaughterhouse.setProperty("/code/state", "Error");
                osslaughterhouse.setProperty("/code/stateText", this.getI18n().getText("enter.FIELD"));
            } else {
                osslaughterhouse.setProperty("/code/state", "None");
                osslaughterhouse.setProperty("/code/stateText", "");
            }

            if (osslaughterhouse.getProperty("/capacity/value") === "") {
                flag = false;
                osslaughterhouse.setProperty("/capacity/state", "Error");
                osslaughterhouse.setProperty("/capacity/stateText", this.getI18n().getText("enter.FIELD"));
            } else {
                osslaughterhouse.setProperty("/capacity/state", "None");
                osslaughterhouse.setProperty("/capacity/stateText", "");
            }

            if (osslaughterhouse.getProperty("/description/value") === "") {
                flag = false;
                osslaughterhouse.setProperty("/description/state", "Error");
                osslaughterhouse.setProperty("/description/stateText", this.getI18n().getText("enter.FIELD"));
            } else {
                osslaughterhouse.setProperty("/description/state", "None");
                osslaughterhouse.setProperty("/description/stateText", "");
            }

            if (osslaughterhouse.getProperty("/address/value") === "") {
                flag = false;
                osslaughterhouse.setProperty("/address/state", "Error");
                osslaughterhouse.setProperty("/address/stateText", this.getI18n().getText("enter.FIELD"));
            }  else {
                osslaughterhouse.setProperty("/address/state", "None");
                osslaughterhouse.setProperty("/address/stateText", "");
            }

            return flag;
        },
        /**
         * Coincidencia de ruta para acceder a los detalles de un registro
         * @param  {Event} oEvent Evento que llam� esta funci�n
         */
        _onRecordMatched: function(oEvent) {

            this._viewOptions();

        },
        /**
         * Cambia las opciones de visualizaci�n disponibles en la vista de detalles de un registro
         */
        _viewOptions: function() {
            var osslaughterhouse = this.getView().getModel("OSSLAUGHTERHOUSE");
            osslaughterhouse.setProperty("/save/", false);
            osslaughterhouse.setProperty("/cancel/", false);
            osslaughterhouse.setProperty("/modify/", true);
            osslaughterhouse.setProperty("/delete/", true);

            this._editRecordValues(false);
            this._editRecordRequired(false);
        },
        /**
         * Ajusta la vista para editar los datos de un registro
         * @param  {Event} oEvent Evento que llam� esta funci�n
         */
        onEdit: function(oEvent) {

            var osslaughterhouse = this.getView().getModel("OSSLAUGHTERHOUSE");
            osslaughterhouse.setProperty("/save/", true);
            osslaughterhouse.setProperty("/cancel/", true);
            osslaughterhouse.setProperty("/modify/", false);
            osslaughterhouse.setProperty("/delete/", false);
            this._editRecordRequired(true);
            this._editRecordValues(true);
        },

        /**
         * Cancela la edici�n de un registro osslaughterhouse
         * @param  {Event} oEvent Evento que llam� esta funci�n
         */
        onCancelEdit: function(oEvent) {
            /** @type {JSONModel} osslaughterhouse  Referencia al modelo osslaughterhouse */

            this.onView();
        },
        /**
         * Ajusta la vista para visualizar los datos de un registro
         */
        onView: function() {
            this._viewOptions();
        },
        /**
         * Solicita al servicio correspondiente actualizar un registro osslaughterhouse
         * @param  {Event} oEvent Evento que llam� esta funci�n
         */
        onUpdate: function(oEvent) {
            /**
             * Si el registro que se quiere actualizar es v�lido y hubo alg�n cambio
             * con respecto a sus datos originales
             */
            if (this._validRecord() && this._recordChanged()) {
                /**
                 * @type {JSONModel} osslaughterhouse       Referencia al modelo "osslaughterhouse"
                 * @type {JSONModel} util         Referencia al modelo "util"
                 * @type {Controller} that         Referencia a este controlador
                 */
                var osslaughterhouse = this.getView().getModel("OSSLAUGHTERHOUSE"),
                    util = this.getView().getModel("util"),
                    that = this,
                    serviceUrl = util.getProperty("/serviceUrl");

                $.ajax({
                    type: "PUT",
                    contentType: "application/json",
                    data: JSON.stringify({
                        "slaughterhouse_id": osslaughterhouse.getProperty("/slaughterhouse_id/value"),
                        "name": osslaughterhouse.getProperty("/name/value"),
                        "code": osslaughterhouse.getProperty("/code/value"),
                        "capacity": osslaughterhouse.getProperty("/capacity/value"),
                        "description": osslaughterhouse.getProperty("/description/value"),
                        "address": osslaughterhouse.getProperty("/address/value"),
                    }),
                    url: serviceUrl+"/slaughterhouse/",
                    dataType: "json",
                    async: true,
                    success: function(data) {

                        util.setProperty("/busy/", false);
                        that._resetRecordValues();
                        that._viewOptions();
                        that.onToast(that.getI18n().getText("OS.recordUpdated"));
                        that.getRouter().navTo("osslaughterhouse", {}, true /*no history*/ );

                    },
                    error: function(request, status, error) {
                        that.onToast("Error de comunicaci�n");
                        console.log("Read failed");
                    }
                });
            }
        },
        /**
         * Verifica si el registro seleccionado tiene alg�n cambio con respecto a sus valores originales
         * @return {Boolean} Devuelve "true" el registro cambi�, y "false" si no cambi�
         */
        _recordChanged: function() {
            /**
             * @type {JSONModel} OSSLAUGHTERHOUSE Referencia al modelo "OSSLAUGHTERHOUSE"
             * @type {Boolean} flag            "true" si el registro cambi�, "false" si no cambi�
             */
            var osslaughterhouse = this.getView().getModel("OSSLAUGHTERHOUSE"),
                flag = false;

            if (osslaughterhouse.getProperty("/name/value") !== osslaughterhouse.getProperty(osslaughterhouse.getProperty("/selectedRecordPath/") + "/name")) {
                flag = true;
            }

            if (osslaughterhouse.getProperty("/code/value") !== osslaughterhouse.getProperty(osslaughterhouse.getProperty("/selectedRecordPath/") + "/code")) {
                flag = true;
            }

            if (osslaughterhouse.getProperty("/capacity/value") !== osslaughterhouse.getProperty(osslaughterhouse.getProperty("/selectedRecordPath/") + "/capacity")) {
                flag = true;
            }

            if (osslaughterhouse.getProperty("/description/value") !== osslaughterhouse.getProperty(osslaughterhouse.getProperty("/selectedRecordPath/") + "/description")) {
                flag = true;
            }

            if (osslaughterhouse.getProperty("/address/value") !== osslaughterhouse.getProperty(osslaughterhouse.getProperty("/selectedRecordPath/") + "/address")) {
                flag = true;
            }

            if(!flag) this.onToast("No se detectaron cambios");

            return flag;
        },
        onSlaughterhouseSearch: function(oEvent){
            var aFilters = [],
                sQuery = oEvent.getSource().getValue(),
                binding = this.getView().byId("slaughterhouseTable").getBinding("items");

            if (sQuery && sQuery.length > 0) {
                /** @type {Object} filter1 Primer filtro de la b�squeda */
                var filter1 = new sap.ui.model.Filter("name", sap.ui.model.FilterOperator.Contains, sQuery);
                aFilters = new sap.ui.model.Filter([filter1]);
            }

            //se actualiza el binding de la lista
            binding.filter(aFilters);

        },
        onConfirmDelete: function(oEvent){

            var oBundle = this.getView().getModel("i18n").getResourceBundle(),
                deleteRecord = oBundle.getText("deleteRecord"),
                confirmation = oBundle.getText("confirmation"),
                util = this.getView().getModel("util"),
                serviceUrl = util.getProperty("/serviceUrl");

            var that = this;
            var dialog = new Dialog({
                title: confirmation,
                type: "Message",
                content: new sap.m.Text({
                    text: deleteRecord
                }),

                beginButton: new Button({
                    text: "Si",
                    press: function() {
                        util.setProperty("/busy/", true);
                        var osslaughterhouse = that.getView().getModel("OSSLAUGHTERHOUSE");
                        $.ajax({
                            type: "DELETE",
                            contentType: "application/json",
                            data: JSON.stringify({
                                "slaughterhouse_id": osslaughterhouse.getProperty("/slaughterhouse_id/value")
                            }),
                            url: serviceUrl+"/slaughterhouse/",
                            dataType: "json",
                            async: true,
                            success: function(data) {

                                util.setProperty("/busy/", false);
                                that.getRouter().navTo("osslaughterhouse", {}, true);
                                dialog.close();
                                dialog.destroy();

                            },
                            error: function(request, status, error) {
                                that.onToast("Error de comunicaci�n");
                                console.log("Read failed");
                            }
                        });

                    }
                }),
                endButton: new Button({
                    text: "No",
                    press: function() {
                        dialog.close();
                        dialog.destroy();
                    }
                })
            });

            dialog.open();

        }

    });
});
