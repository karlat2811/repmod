sap.ui.define([
    "technicalConfiguration/controller/BaseController",
    "technicalConfiguration/model/formatter",
    "sap/m/Dialog",
    "sap/m/Button",
    "sap/m/Text"
], function(BaseController, formatter, Dialog, Button, Text) {
    "use strict";

    return BaseController.extend("technicalConfiguration.controller.restrainingOrder", {
        formatter: formatter,
        onInit: function() {
            console.log("restrainingOrder");
            // this.getView().byId("farmSaveOrdenButt").setEnabled(false);
			
            this.getOwnerComponent().getRouter().getRoute("restrainingOrder").attachPatternMatched(this._onRouteMatched, this);
	
            this.getOwnerComponent().getRouter().getRoute("restrainingOrder_Create").attachPatternMatched(this._onCreateMatched, this);
	
            this.getOwnerComponent().getRouter().getRoute("restrainingOrder_Record").attachPatternMatched(this._onRecordMatched, this);
        },

        _onRouteMatched: function(oEvent) {

            var that = this,
                util = this.getView().getModel("util"),
                ospartnership = this.getView().getModel("OSPARTNERSHIP"),
                osfarm = this.getView().getModel("OSFARM"),
                oscenter = this.getView().getModel("OSCENTER"),
                osshed = this.getView().getModel("OSSHED");
                
            //console.log(util.getProperty("/selectedEntity"));    
            //console.log(osshed);    

            //dependiendo del dispositivo, establece la propiedad "phone"
            util.setProperty("/phone/",
                this.getOwnerComponent().getContentDensityClass() === "sapUiSizeCozy");

            ospartnership.setProperty("/settings/tableMode", "SingleSelect");
            osfarm.setProperty("/settings/tableMode", "SingleSelect");
            oscenter.setProperty("/settings/tableMode", "SingleSelect");

            //si la estructura seleccionada antes de acceder a esta vista es diferente a restrainingOrder
            if (util.getProperty("/selectedEntity") !== "restrainingOrder") {

                //establece galpón como la estructura seleccionada
                util.setProperty("/selectedEntity", "restrainingOrder");

                //establece el tab de la tabla PARTNERSHIP como el tab seleccionado
                this.getView().byId("tabBar").setSelectedKey(this.getView().getId() + "--" + "partnershipFilterWithOrden");

                //borra cualquier selección que se haya hecho en la tabla PARTNERSHIP
                this.getView().byId("partnershipTable").removeSelections(true);

                //borra cualquier selección que se haya hecho en la tabla BROILERSFARM
                this.getView().byId("farmTableWithOrden").removeSelections(true);

                //borra cualquier selección que se haya hecho en la tabla CENTER
                this.getView().byId("centerTable").removeSelections(true);

                //borra cualquier selección que se haya hecho en la tabla silo
                this.getView().byId("shedTable").removeSelections(true);

                //establece que no hay ningún registro PARTNERSHIP seleccionado
                ospartnership.setProperty("/selectedRecordPath/", "");

                //establece que no hay ningún registro BROILERSFARM seleccionado
                osfarm.setProperty("/selectedRecordPath/", "");

                osfarm.setProperty("/records/", []);

                oscenter.setProperty("/records/", []);

                osshed.setProperty("/records/", []);

                //establece que no hay ningún registro BROILERSFARM seleccionado
                oscenter.setProperty("/selectedRecordPath/", "");

                //deshabilita el tab de la tabla de registros BROILERSFARM
                osfarm.setProperty("/settings/enabledTab", false);

                //deshabilita el tab de la tabla de registros CENTER
                oscenter.setProperty("/settings/enabledTab", false);

                //deshabilita el tab de la tabla de silos
                osshed.setProperty("/settings/enabledTab", false);

                //deshabilita la opción de crear un galpón
                osshed.setProperty("/new", false);

                //obtiene las sociedades financieras
                sap.ui.controller("technicalConfiguration.controller.ospartnership").onRead(that, util, ospartnership);

            } else if (ospartnership.getProperty("/selectedRecordPath/") !== "" &&
				osfarm.getProperty("/selectedRecordPath/") !== "" &&
				oscenter.getProperty("/selectedRecordPath/") !== "") {

                //habilita el tab de la tabla de granjas
                osfarm.setProperty("/settings/enabledTab", true);

                //habilita el tab de la tabla de núcleos
                oscenter.setProperty("/settings/enabledTab", true);

                //habilita el tab de la tabla de galpones
                osshed.setProperty("/settings/enabledTab", true);

                //habilita la opción de crear un galpón
                osshed.setProperty("/new", true);

                //obtiene los galpones
                this.onRead(that, util, ospartnership, osfarm, oscenter, osshed);
            }
        },
        onPartnershipSearch: function(oEvent){
            var aFilters = [],
                sQuery = oEvent.getSource().getValue(),
                binding = this.getView().byId("partnershipTable").getBinding("items");

            if (sQuery && sQuery.length > 0) {
                /** @type {Object} filter1 Primer filtro de la búsqueda */
                var filter1 = new sap.ui.model.Filter("name", sap.ui.model.FilterOperator.Contains, sQuery);
                aFilters = new sap.ui.model.Filter([filter1]);
            }

            //se actualiza el binding de la lista
            binding.filter(aFilters);

        },
        onLoadSheds: function(){
            let osshed = this.getModel("osshed"),
                util = this.getModel("util"),
                serviceUrl = util.getProperty("/serviceUrl")+"/shed_status/";
            //console.log(osshed);
            fetch(serviceUrl)
                .then(
                    function(response) {
                        if (response.status !== 200) {
                            console.log("Looks like there was a problem. Status Code: " +
								response.status);
                            return;
                        }

                        // Examine the text in the response
                        response.json().then(function(res) {
                            //console.log(res.data);
                            util.setProperty("/busy/", false);
                            //console.log(osshed);
                            osshed.setProperty("/statusRecords", res.data);
                        });
                    }
                )
                .catch(function(err) {
                    console.log("Fetch Error: ", err);
                });

        },
        onFarmSearch: function(oEvent) {
            var aFilters = [],
                sQuery = oEvent.getSource().getValue(),
                binding = this.getView().byId("farmTableWithOrden").getBinding("items");
            if (sQuery && sQuery.length > 0) {
                /** @type {Object} filter1 Primer filtro de la búsqueda */
                var filter1 = new sap.ui.model.Filter("name", sap.ui.model.FilterOperator.Contains, sQuery);
                aFilters = new sap.ui.model.Filter([filter1]);
            }
            //se actualiza el binding de la lista
            binding.filter(aFilters);

        },
        onCenterSearch: function(oEvent) {
            var aFilters = [],
                sQuery = oEvent.getSource().getValue(),
                binding = this.getView().byId("centerTable").getBinding("items");
            if (sQuery && sQuery.length > 0) {
                /** @type {Object} filter1 Primer filtro de la búsqueda */
                var filter1 = new sap.ui.model.Filter("code", sap.ui.model.FilterOperator.Contains, sQuery);
                aFilters = new sap.ui.model.Filter([filter1]);
            }
            //se actualiza el binding de la lista
            binding.filter(aFilters);

        },
        onShedSearch: function(oEvent) {
            var aFilters = [],
                sQuery = oEvent.getSource().getValue(),
                binding = this.getView().byId("shedTable").getBinding("items");
            if (sQuery && sQuery.length > 0) {
                /** @type {Object} filter1 Primer filtro de la búsqueda */
                var filter1 = new sap.ui.model.Filter("code", sap.ui.model.FilterOperator.Contains, sQuery);
                aFilters = new sap.ui.model.Filter([filter1]);
            }
            //se actualiza el binding de la lista
            binding.filter(aFilters);

        },
        selfarmtype: function(oEvent){
            var aFilters = [],
                sQuery =  this.getView().byId("selectFarmtype").mProperties.selectedKey,
                binding = this.getView().byId("farmTableWithOrden").getBinding("items");
			 
            let osfarm = this.getView().getModel("OSFARM");
            osfarm.getProperty("/records");
            //console.log(osfarm.getProperty("/records"))
            //console.log(osfarm.getProperty("/All"))
			

            if (sQuery != this.getI18n().getText("all")) {
                //this.getView().byId("farmSaveOrdenButt").setEnabled(true);
                osfarm.getProperty("/All").forEach(element => {
                    if (sQuery == element.farm_name) {
                        aFilters.push(element);
                    }
                });
            }else{
                //this.getView().byId("farmSaveOrdenButt").setEnabled(false);
                aFilters = osfarm.getProperty("/All");
            }
		
            osfarm.setProperty("/records/",aFilters);			
        },

        /**
		 * Regresa a la vista principal de la entidad seleccionada actualmente
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        onNavBack: function(oEvent) {
            /** @type {JSONModel} OS Referencia al modelo "OS" */
            var util = this.getView().getModel("util");

            this._resetRecordValues();
            this.getRouter().navTo(util.getProperty("/selectedEntity"), {}, true);
        },

        /**
		 * Selecciona un registro PARTNERSHIP y habilita la tabla de registros BROILERSFARM
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        onSelectPartnershipRecord: function(oEvent) {
            //console.log("Yes");
		
            var that = this,
                util = this.getView().getModel("util"),
                ospartnership = this.getView().getModel("OSPARTNERSHIP"),
                osfarm = this.getView().getModel("OSFARM"),
                oscenter = this.getView().getModel("OSCENTER"),
                osshed = this.getView().getModel("OSSHED");

            //guarda la ruta del registro PARTNERSHIP que fue seleccionado
            ospartnership.setProperty("/selectedRecordPath/", oEvent.getSource()["_aSelectedPaths"][0]);
            ospartnership.setProperty("/selectedRecord/", ospartnership.getProperty(ospartnership.getProperty("/selectedRecordPath/")));

            osfarm.setProperty("/selectedRecordPath/", "");
            osfarm.setProperty("/selectedRecord/", {});

            oscenter.setProperty("/selectedRecordPath/", "");
            oscenter.setProperty("/selectedRecord/", {});

            //habilita el tab de la tabla de granjas
            osfarm.setProperty("/settings/enabledTab", true);

            //deshabilita el tab de la tabla de núcleos
            oscenter.setProperty("/settings/enabledTab", false);

            //deshabilita el tab de la tabla de galpones
            osshed.setProperty("/settings/enabledTab", false);

            //deshabilita la opción de crear un galpón
            osshed.setProperty("/new", false);
            osshed.setProperty("/farmSaveOrdenButt", false);
            osshed.setProperty("/farmModOrdenButt", true);

            //establece el tab de la tabla de granjas como el tab seleccionado
            this.getView().byId("tabBar").setSelectedKey(this.getView().getId() + "--" + "farmFilterWithOrden");

            //borra cualquier selección que se haya hecho en la tabla de granjas
            this.getView().byId("farmTableWithOrden").removeSelections(true);

            //obtiene las granjas
            sap.ui.controller("technicalConfiguration.controller.osfarm").onRead(that, util, ospartnership, osfarm);
        },

        /**
		 * Selecciona un registro BROILERSFARM y habilita la tabla de registros CENTER
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        onSelectFarmRecord: function(oEvent) {
	
            var that = this,
                util = this.getView().getModel("util"),
                ospartnership = this.getView().getModel("OSPARTNERSHIP"),
                osfarm = this.getView().getModel("OSFARM"),
                oscenter = this.getView().getModel("OSCENTER"),
                osshed = this.getView().getModel("OSSHED");
			

            //guarda la ruta del registro BROILERSFARM que fue seleccionado
            console.log(osshed.getProperty("farmModOrdenButt"));
            if (osshed.getProperty("/farmModOrdenButt") === true) {
                osfarm.setProperty("/selectedRecordPath/", oEvent.getSource()["_aSelectedPaths"][0]);
                osfarm.setProperty("/selectedRecord/", osfarm.getProperty(osfarm.getProperty("/selectedRecordPath/")));
	
                oscenter.setProperty("/selectedRecordPath/", "");
                oscenter.setProperty("/selectedRecord/", {});
	
                //habilita el tab de la tabla de registros CENTER
                oscenter.setProperty("/settings/enabledTab", true);
	
                //deshabilita el tab de la tabla de galpones
                osshed.setProperty("/settings/enabledTab", false);
	
                //deshabilita la opción de crear un galpón
                osshed.setProperty("/new", false);
                osshed.setProperty("/farmSaveOrdenButt", false);
                osshed.setProperty("/farmModOrdenButt", false);
                osshed.setProperty("/centerSaveOrdenButt", false);
                osshed.setProperty("/centerModOrdenButt", true);
				
                //establece el tab de la tabla CENTER como el tab seleccionado
                that.getView().byId(this.getView().getId() + "--" + "tabBar").setSelectedKey(this.getView().getId() + "--" + "centerFilterWithOrden");
	
                //borra cualquier selección que se haya hecho en la tabla BROILERSFARM
                that.getView().byId("centerTable").removeSelections(true);
	
                //obtiene los registros de CENTER
                sap.ui.controller("technicalConfiguration.controller.oscenter").onRead(that, util, ospartnership, osfarm, oscenter);
            }else{
                that.getView().byId("farmTableWithOrden").removeSelections(true);
            }

		
        },

        /**
		 * Selecciona un registro BROILERSFARM y habilita la tabla de registros CENTER
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        onSelectCenterRecord: function(oEvent) {
	
            var that = this,
                util = this.getView().getModel("util"),
                ospartnership = this.getView().getModel("OSPARTNERSHIP"),
                osfarm = this.getView().getModel("OSFARM"),
                oscenter = this.getView().getModel("OSCENTER"),
                osshed = this.getView().getModel("OSSHED");

            //guarda la ruta del registro BROILERSFARM que fue seleccionado
            if (osshed.getProperty("/centerModOrdenButt") === true) {
                oscenter.setProperty("/selectedRecordPath/", oEvent.getSource()["_aSelectedPaths"][0]);

                oscenter.setProperty("/selectedRecord/", oscenter.getProperty(oscenter.getProperty("/selectedRecordPath/")));
	
                //habilita el tab de la tabla de galpones
                osshed.setProperty("/settings/enabledTab", true);
	
                //deshabilita la opción de crear un galpón
                osshed.setProperty("/new", true);
                osshed.setProperty("/shedSaveOrdenButt", false);
                osshed.setProperty("/shedModOrdenButt", true);
	
                osshed.setProperty("/centerSaveOrdenButt", false);
                osshed.setProperty("/farmSaveOrdenButt", false);
                osshed.setProperty("/centerModOrdenButt", false);
                osshed.setProperty("/farmModOrdenButt", false);
	
                //establece el tab de la tabla de galpones como el tab seleccionado
                this.getView().byId(this.getView().getId() + "--" + "tabBar").setSelectedKey(this.getView().getId() + "--" + "shedFilterWithOrden");
	
                //borra cualquier selección que se haya hecho en la tabla BROILERSFARM
                this.getView().byId("shedTable").removeSelections(true);
	
                //obtiene los galpones
                this.onRead(that, util, ospartnership, osfarm, oscenter, osshed);
            }else{
                that.getView().byId("centerTable").removeSelections(true);
            }
		
        },

        /**
		 * Obtiene todos los registros de BROILERSFARM, dado un cliente y una sociedad
		 */
        onRead: function(that, util, ospartnership, osfarm, oscenter, osshed) {
            var serviceUrl = util.getProperty("/serviceUrl");
            var center_id = oscenter.getProperty(oscenter.getProperty("/selectedRecordPath/") + "/center_id");
            //console.log(center_id);
            var old = [];
            var settings = {
                type: "POST",
                contentType: "application/json",
                data: JSON.stringify({
                    "center_id": center_id,
                }),
                url: serviceUrl+"/shed/findShedByCenter/",
                dataType: "json",
                async: true,
		 		success: function(res) {
						
						 res.data.forEach(element => {
                        element.old = element.order;
                    });
                    osshed.setProperty("/records",res.data);
						
                    console.log(osshed);
		 		},
		 		error: function(err) {
		 				util.setProperty("/error/status", err.status);
		 				util.setProperty("/error/statusText", err.statusText);
		 		}
		 };
            util.setProperty("/busy/", true);
            //borra los registros de galpones que estén almacenados actualmente
            osshed.setProperty("/records/", []);
            //realiza la llamada ajax
            $.ajax(settings);
        },

	
        onNewRecord: function(oEvent) {
            this.getRouter().navTo("restrainingOrder_Create", {}, true);
        },
        onActModifyOrden: function(oEvent) {
            let sId = oEvent.getSource().sId.substring(12, oEvent.getSource().sId.length),
                data;
            var that = this,
                util = this.getView().getModel("util"),
                osfarm = this.getView().getModel("OSFARM"),
                oscenter = this.getView().getModel("OSCENTER"),
                osshed = this.getView().getModel("OSSHED");
			
            console.log(sId);

            if (sId == "farmModOrdenButt") {
                data = osfarm.getProperty("/records");
            }
            if (sId == "centerModOrdenButt") {
				
                data = oscenter.getProperty("/records");
            }
            if (sId == "shedModOrdenButt") {
				
                data = osshed.getProperty("/records");
            }

            data.forEach(element => {
                if (element.availableorder == false) {
                    element.availableorder = true;
                }
            });

            console.log(data);

            if (sId == "farmModOrdenButt") {
                osshed.setProperty("/farmSaveOrdenButt",true);
                osshed.setProperty("/farmCancelOrdenButt",true);
                osshed.setProperty("/farmModOrdenButt",false);
                osfarm.setProperty("/records",data);
            }
            if (sId == "centerModOrdenButt") {
                osshed.setProperty("/centerSaveOrdenButt",true);
                osshed.setProperty("/centerCancelOrdenButt",true);
                osshed.setProperty("/centerModOrdenButt",false);
                oscenter.setProperty("/records",data);
            }
            if (sId == "shedModOrdenButt") {
                osshed.setProperty("/shedSaveOrdenButt",true);
                osshed.setProperty("/shedCancelOrdenButt",true);
                osshed.setProperty("/shedModOrdenButt",false);
                osshed.setProperty("/records",data);
            }
        },

        onCancelEdit: function(oEvent) {
            let sId = oEvent.getSource().sId.substring(12, oEvent.getSource().sId.length),
                data = [];
            var that = this,
                util = this.getView().getModel("util"),
                osfarm = this.getView().getModel("OSFARM"),
                oscenter = this.getView().getModel("OSCENTER"),
                osshed = this.getView().getModel("OSSHED");
			
            if (sId == "farmCancelOrdenButt") {
                data = osfarm.getProperty("/records");
                data.forEach(element => {
                    element.state = "None";
                    element.stateText = "";
                    element.order = element.old;
                    element.availableorder = false;
                });
                osfarm.setProperty("/records",data);
                osshed.setProperty("/farmSaveOrdenButt",false);
                osshed.setProperty("/farmCancelOrdenButt",false);
                osshed.setProperty("/farmModOrdenButt",true);
            }
            if (sId == "centerCancelOrdenButt") {
                data = oscenter.getProperty("/records");
                data.forEach(element => {
                    element.state = "None";
                    element.stateText = "";
                    element.order = element.old;
                    element.availableorder = false;
                });
                oscenter.setProperty("/records",data);
                osshed.setProperty("/centerSaveOrdenButt",false);
                osshed.setProperty("/centerCancelOrdenButt",false);
                osshed.setProperty("/centerModOrdenButt",true);
            }
            if (sId == "shedCancelOrdenButt") {
                data = osshed.getProperty("/records");
                data.forEach(element => {
                    element.state = "None";
                    element.stateText = "";
                    element.order = element.old;
                    element.availableorder = false;
                });
                osshed.setProperty("/records",data);
                osshed.setProperty("/shedSaveOrdenButt",false);
                osshed.setProperty("/shedCancelOrdenButt",false);
                osshed.setProperty("/shedModOrdenButt",true);
            }
	
        },
        
        onSaveOrden: function(oEvent) {
            //console.log(oEvent);
            let sId = oEvent.getSource().sId.substring(12, oEvent.getSource().sId.length);
            //console.log(sId);
            let servername;
            let data;
            let bFlag = false;
            let Flag = false;
            let mess = "El valor no puede ser vacio, repetido, menor a 0 ni mayor a la cantidad total de elementos a ordenar";
            let sQuery =  this.getView().byId("selectFarmtype").mProperties.selectedKey;
            var j=0;

            var that = this,
                util = this.getView().getModel("util"),
                osfarm = this.getView().getModel("OSFARM"),
                oscenter = this.getView().getModel("OSCENTER"),
                osshed = this.getView().getModel("OSSHED");

            if (sId == "farmSaveOrdenButt") {
                servername =  "/farm/updateFarmOrder";
                data = osfarm.getProperty("/records");

            }
            if (sId == "centerSaveOrdenButt") {
                servername =  "/center/updateCenterOrder"; 
                data = oscenter.getProperty("/records");
            }
            if (sId == "shedSaveOrdenButt") {
                servername =  "/shed/updateShedOrder"; 
                data = osshed.getProperty("/records");
            }
		
            console.log(sId);
            console.log(sQuery);
            if (sQuery == "Todos" && sId == "farmSaveOrdenButt") {
                for(var i=0;i<data.length;i++)
                {
                    // console.log("-->" + data[i].order,data[i].name);
                    // if (data[i].order != null) {
                    while(j<data.length && !Flag)
                    {
                        // console.log(data[j].order,data[j].name);
                        if(i!=j)
                        {
                            if (data[j].order != null ) {
                                if((data[i].order == data[j].order && data[i].farm_name == data[j].farm_name)
									|| data[j].order <= 0 || data[j].order > data.length ){
										
                                    data[j].state = "Error";
                                    data[j].stateText = mess;
                                    console.log("NO!", data[j].name);
                                    bFlag = true;
                                    Flag = true;
										
                                }
                            }
                        }
                        if (j == data.length-1 && !Flag) {
                            data[i].state = "None";
                            data[i].stateText = "";
                            console.log("Clear!", data[j].name);
                        }
                        j++;
                    }
                    // }
                    // else{
                    // 	data[i].state = 'Error'
                    // 	data[i].stateText = mess;
                    // 	console.log("INO!", data[i].name);
                    // 	bFlag = true;
                    // }
                    Flag = false;
                    j=0;
                }
				
		
            }else{
                console.log("A");
                for(var i=0;i<data.length;i++)
                {
                    console.log("-->" + data[i].order,data[i].name);
                    if (data[i].order >= 0 && data[i].order <= data.length) {
                        while(j<data.length && !Flag)
                        {
                            console.log(data[j].order,data[j].name);
                            if(i!=j)
                            {
                                if (data[j].order != null ) {
                                    if((data[i].order == data[j].order)
									|| data[j].order <= 0 || data[j].order > data.length ){
										
                                        data[j].state = "Error";
                                        data[j].stateText = mess;
                                        console.log("NO!", data[j].name);
                                        bFlag = true;
                                        Flag = true;
										
                                    }
                                }							
                            }
                            console.log(j,data.length-1,Flag);
							
                            if (j == data.length-1 || !Flag) {
                                data[i].state = "None";
                                data[i].stateText = "";
                                console.log("Clear!", data[j].name);
                            }
                            j++;
                        }
                    }
                    else{
                        data[i].state = "Error";
                        data[i].stateText = mess;
                        console.log("INO!", data[i].name);
                        bFlag = true;
                    }
                    Flag = false;
                    j=0;
                }
            }
		
            //console.log(data);
            if (sId == "farmSaveOrdenButt") {
                osfarm.setProperty("/records",data);
            }
            if (sId == "centerSaveOrdenButt") {
                oscenter.setProperty("/records",data);
            }
            if (sId == "shedSaveOrdenButt") {
                osshed.setProperty("/records",data);
            }
            if(!bFlag)
            {
                fetch(servername, {
                    method: "PUT",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    credentials: "same-origin",
                    body: JSON.stringify({
                        data: data
                    })
                }).then(  function (Orden) {
                    if (Orden.status == 500) {
                        console.log("Looks like there was a problem. Status Code: " + StatusRespo.status);
                    } else {
                        Orden.json().then(async function (Orden) {
                            var inf = "Actualización de orden de forma exitosa.";
                            var dialog = new Dialog({
                                title: "Modificación",
                                type: "Message",
                                state: "Success",
                                content: new Text({
                                    text: inf
                                }),
                                beginButton: new Button({
                                    text: "Continuar",
                                    press: function () {
                                        data.forEach(element => {
                                            element.state = "None";
                                            element.stateText = "";
                                            element.availableorder = false;
                                        });
                                        if (sId == "farmSaveOrdenButt") {
                                            osfarm.setProperty("/records",data);
                                            data.forEach(element => {
                                                element.old = element.order;
                                            });
                                            osshed.setProperty("/farmSaveOrdenButt",false);
                                            osshed.setProperty("/farmCancelOrdenButt",false);
                                            osshed.setProperty("/farmModOrdenButt",true);
                                        }
                                        if (sId == "centerSaveOrdenButt") {
                                            data.forEach(element => {
                                                element.old = element.order;
                                            });
                                            oscenter.setProperty("/records",data);
                                            osshed.setProperty("/centerSaveOrdenButt",false);
                                            osshed.setProperty("/centerCancelOrdenButt",false);
                                            osshed.setProperty("/centerModOrdenButt",true);
                                        }
                                        if (sId == "shedSaveOrdenButt") {
                                            osshed.setProperty("/records",data);
                                            data.forEach(element => {
                                                element.old = element.order;
                                            });
                                            osshed.setProperty("/shedSaveOrdenButt",false);
                                            osshed.setProperty("/shedCancelOrdenButt",false);
                                            osshed.setProperty("/shedModOrdenButt",true);
                                        }
                                        dialog.close();
                                    }
                                }),
                                afterClose: function () {
                                    dialog.destroy();
                                }
                            });
                            dialog.open();
                        });
                    }
                });
            }
        },
        onTabSelection: function (ev) {
            var selectedKey = ev.getSource().getSelectedKey();
            var osshed = this.getView().getModel("OSSHED");
            let data;
            var that = this,
                util = this.getView().getModel("util"),
                osfarm = this.getView().getModel("OSFARM"),
                oscenter = this.getView().getModel("OSCENTER"),
                osshed = this.getView().getModel("OSSHED");
            //console.log(selectedKey);
            if (selectedKey === "__xmlview3--farmFilterWithOrden") {
		
                data = osfarm.getProperty("/records");
                data.forEach(element => {
                    element.state = "None";
                    element.stateText = "";
                    element.order = element.old;
                    element.availableorder = false;
                });
                osfarm.setProperty("/records",data);

                osshed.setProperty("/farmSaveOrdenButt", false);
                osshed.setProperty("/farmModOrdenButt", true);
                osshed.setProperty("/farmCancelOrdenButt", false);
				
                osshed.setProperty("/centerSaveOrdenButt", false);
                osshed.setProperty("/centerModOrdenButt", false);
                osshed.setProperty("/centerCancelOrdenButt", false);

                osshed.setProperty("/shedSaveOrdenButt", false);
                osshed.setProperty("/shedModOrdenButt", false);
                osshed.setProperty("/shedCancelOrdenButt", false);
			
            }
            if (selectedKey === "__xmlview3--centerFilterWithOrden") {
                data = oscenter.getProperty("/records");
                data.forEach(element => {
                    element.state = "None";
                    element.stateText = "";
                    element.order = element.old;
                    element.availableorder = false;
                });
                oscenter.setProperty("/records",data);
				
                osshed.setProperty("/farmSaveOrdenButt", false);
                osshed.setProperty("/farmModOrdenButt", false);
                osshed.setProperty("/farmCancelOrdenButt", false);

                osshed.setProperty("/centerSaveOrdenButt", false);
                osshed.setProperty("/centerModOrdenButt", true);
                osshed.setProperty("/centerCancelOrdenButt", false);

                osshed.setProperty("/shedSaveOrdenButt", false);
                osshed.setProperty("/shedModOrdenButt", false);
            	osshed.setProperty("/shedCancelOrdenButt", false);
            }
            if (selectedKey === "__xmlview3--shedFilterWithOrden") {
                data = osshed.getProperty("/records");
                data.forEach(element => {
                    element.state = "None";
                    element.stateText = "";
                    element.order = element.old;
                    element.availableorder = false;
                });
                osshed.setProperty("/records",data);

                osshed.setProperty("/farmSaveOrdenButt", false);
                osshed.setProperty("/farmModOrdenButt", false);
                osshed.setProperty("/farmCancelOrdenButt", false);

                osshed.setProperty("/centerSaveOrdenButt", false);
                osshed.setProperty("/centerModOrdenButt", false);
                osshed.setProperty("/centerCancelOrdenButt", false);

                osshed.setProperty("/shedSaveOrdenButt", false);
                osshed.setProperty("/shedModOrdenButt", true);
                osshed.setProperty("/shedCancelOrdenButt", false);
            }
            if (selectedKey === "__xmlview3--partnershipFilterWithOrden") {
			
                osshed.setProperty("/farmSaveOrdenButt", false);
                osshed.setProperty("/farmModOrdenButt", false);
                osshed.setProperty("/farmCancelOrdenButt", false);
				
                osshed.setProperty("/centerSaveOrdenButt", false);
                osshed.setProperty("/centerModOrdenButt", false);
                osshed.setProperty("/centerCancelOrdenButt", false);

                osshed.setProperty("/shedSaveOrdenButt", false);
                osshed.setProperty("/shedModOrdenButt", false);
                osshed.setProperty("/shedCancelOrdenButt", false);
			
            }
            
        },

        onChange:function(o){
            //console.log("Aqui");
            let sId = o.getSource().sId.substring(12, 27);
            //console.log(sId);
            let input= o.getSource();
            let length = 10;
            let value = input.getValue();
            let regex = new RegExp(`/^[0-9]{1,${length}}$/`);
            let data;
            var  osfarm = this.getView().getModel("OSFARM"),
                oscenter = this.getView().getModel("OSCENTER"),
                osshed = this.getView().getModel("OSSHED");
            var found = new Array();

            // switch (sId) {
            //     case "farm_order_text":
            //         found =  osfarm.getProperty("/records").find(function(element) {
            //             return value == element.order;
            //         });
            //         console.log(found)
            //         if (found != undefined) {
            // 			value = ''
            // 			input.setValue(value)
            // 			return false
            //         }else{
            // 			if (value <= 0 && value >  osfarm.getProperty("/records").length) {
            // 				value = ''
            // 				input.setValue(value)
            // 				return false
            // 			}else{
						
            // 			}
            // 		}

            //     break;

            //     case "cent_order_text":
            //         found =  oscenter.getProperty("/records").find(function(element) {
            //             return value == element.order;
            //         });
            //     break;

            //     case "ossh_order_text":
            //         found =  osshed.getProperty("/records").find(function(element) {
            //             return value == element.order;
            //         });
            //     break;
            // }
            //console.log(found);

            if (regex.test(value)) {
                return true;
		   }
		   else {
			   let aux = value.split("").filter(char => {
			   if (/^[0-9]$/.test(char)) {
				   if (char !== ".") {
					   return true;
				   }
			   }
			   }).join("");
				   value = aux.substring(0, length);
				   //console.log(value);
				   input.setValue(value);
				   return false;
		   }
       
        },      

        /**
		 * Coincidencia de ruta para acceder a la creación de un registro
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        _onCreateMatched: function(oEvent) {
            //resetea y habilita los campos del formulario para su edición
            this._resetRecordValues();
            this._editRecordValues(true);
            this._visibility();
            this.onLoadSheds();
        },

        _visibility: function(){

            //console.log(this.getModel("OSFARM"));
            var osshed = this.getView().getModel("OSSHED");
            osshed.setProperty("/rotation_days/visible", true);
        },

        /**
		 * Resetea todos los valores existentes en el formulario del registro
		 */
        _resetRecordValues: function() {
            /** @type {JSONModel} SHED Referencia al modelo "SHED" */
            var osshed = this.getView().getModel("OSSHED");

            osshed.setProperty("/name/value", "");
            osshed.setProperty("/name/state", "None");
            osshed.setProperty("/name/stateText", "");

            osshed.setProperty("/stall_height/value", "");
            osshed.setProperty("/stall_height/state", "None");
            osshed.setProperty("/stall_height/stateText", "");

            osshed.setProperty("/stall_width/value", "");
            osshed.setProperty("/stall_width/state", "None");
            osshed.setProperty("/stall_width/stateText", "");

            osshed.setProperty("/status/value", "0");
            osshed.setProperty("/status/state", "None");
            osshed.setProperty("/status/stateText", "");

            osshed.setProperty("/environment/value", "0");
            osshed.setProperty("/environment/state", "None");
            osshed.setProperty("/environment/stateText", "");

            osshed.setProperty("/capmin/value", "0");
            osshed.setProperty("/capmin/state", "None");
            osshed.setProperty("/capmin/stateText", "");

            osshed.setProperty("/capmax/value", "0");
            osshed.setProperty("/capmax/state", "None");
            osshed.setProperty("/capmax/stateText", "");
        },

        /**
		 * Habilita/deshabilita la edición de los datos de un registro
		 * @param  {Boolean} edit "true" si habilita la edición, "false" si la deshabilita
		 */
        _editRecordValues: function(edit) {
            /** @type {JSONModel} SILO Referencia al modelo "SHED" */
            var osshed = this.getView().getModel("OSSHED");

            osshed.setProperty("/name/editable", edit);
            osshed.setProperty("/stall_height/editable", edit);
            osshed.setProperty("/stall_width/editable", edit);
            osshed.setProperty("/status/editable", edit);
            osshed.setProperty("/environment/editable", edit);
            osshed.setProperty("/capmin/editable", edit);
            osshed.setProperty("/capmax/editable", edit);
            osshed.setProperty("/rotation_days/editable", edit);
            osshed.setProperty("/quantity_nests/editable", edit);
            osshed.setProperty("/quantity_cages/editable", edit);

            osshed.setProperty("/name/required", edit);
            osshed.setProperty("/stall_height/required", edit);
            osshed.setProperty("/stall_width/required", edit);
            osshed.setProperty("/status/required", edit);
            osshed.setProperty("/environment/required", edit);
            osshed.setProperty("/capmin/required", edit);
            osshed.setProperty("/capmax/required", edit);
            osshed.setProperty("/rotation_days/required", edit);
            osshed.setProperty("/quantity_nests/required", edit);
            osshed.setProperty("/quantity_cages/required", edit);
        },

        /**
		 * Valida la correctitud de los datos existentes en el formulario del registro
		 * @return {Boolean} Devuelve "true" si los datos son correctos, y "false" si son incorrectos
		 */
        _validRecord: function() {
            /**
			 * @type {JSONModel} SHED Referencia al modelo "SHED"
			 * @type {Boolean} flag             "true" si los datos son válidos, "false" si no lo son
			 */
            var osshed = this.getView().getModel("OSSHED"),
                flag = true;

            if (osshed.getProperty("/name/value") === "") {
                flag = false;
                osshed.setProperty("/name/state", "Error");
                osshed.setProperty("/name/stateText", this.getI18n().getText("enter.NAME"));
            } else {
                osshed.setProperty("/name/state", "None");
                osshed.setProperty("/name/stateText", "");
            }

            if (osshed.getProperty("/stall_height/value") === "") {
                flag = false;
                osshed.setProperty("/stall_height/state", "Error");
                osshed.setProperty("/stall_height/stateText", "Introduzca el ancho");
            } else if (osshed.getProperty("/stall_height/value") <= 0) {
                flag = false;
                osshed.setProperty("/stall_height/state", "Error");
                osshed.setProperty("/stall_height/stateText", "Introduzca un valor válido");
            } else {
                osshed.setProperty("/stall_height/state", "None");
                osshed.setProperty("/stall_height/stateText", "");
            }

            if (osshed.getProperty("/stall_width/value") === "") {
                flag = false;
                osshed.setProperty("/stall_width/state", "Error");
                osshed.setProperty("/stall_width/stateText", "Introduzca el largo");
            } else if (osshed.getProperty("/stall_width/value") <= 0) {
                flag = false;
                osshed.setProperty("/stall_width/state", "Error");
                osshed.setProperty("/stall_width/stateText", "Introduzca un valor válido");
            } else {
                osshed.setProperty("/stall_width/state", "None");
                osshed.setProperty("/stall_width/stateText", "");
            }

            if (osshed.getProperty("/status/value") === "" || osshed.getProperty("/status/value") === "0") {
                console.log("entro en el error");
                flag = false;
                osshed.setProperty("/status/state", "Error");
                osshed.setProperty("/status/stateText", "Seleccione un estatus");
            } else {
                osshed.setProperty("/status/state", "None");
                osshed.setProperty("/status/stateText", "");
            }

            if (osshed.getProperty("/environment/value") === "" || osshed.getProperty("/environment/value") === "0") {
                flag = false;
                osshed.setProperty("/environment/state", "Error");
                osshed.setProperty("/environment/stateText", "Seleccione un tipo de ambiente");
            } else {
                osshed.setProperty("/environment/state", "None");
                osshed.setProperty("/environment/stateText", "");
            }


            if (osshed.getProperty("/capmin/value") === "" || osshed.getProperty("/capmin/value") === "0" ) {
                flag = false;
                osshed.setProperty("/capmin/state", "Error");
                osshed.setProperty("/capmin/stateText", "Ingrese un valor");
            } else {
                osshed.setProperty("/capmin/state", "None");
                osshed.setProperty("/capmin/stateText", "");
            }

            if (osshed.getProperty("/capmax/value") === "" || osshed.getProperty("/capmax/value") === "0") {
                flag = false;
                osshed.setProperty("/capmax/state", "Error");
                osshed.setProperty("/capmax/stateText", "Ingrese un valor");
            } else {
                osshed.setProperty("/capmax/state", "None");
                osshed.setProperty("/capmax/stateText", "");
            }

            return flag;
        },

        onStatus: function(oEvent) {
            //console.log("llego a onstatus");
            var campo=oEvent.getParameters().selectedItem.mProperties.key;
            //console.log(campo);
            var osshed = this.getView().getModel("OSSHED");

            osshed.setProperty("/status/value", campo);
            //console.log(oEvent.getParameters().selectedItem.mProperties.key);
        },
        onEnvironment:function(oEvent){
            var osshed = this.getView().getModel("OSSHED");

            osshed.setProperty("/environment/value", oEvent.getParameters().selectedItem.mProperties.key);
            //console.log(oEvent.getParameters().selectedItem.mProperties.key);
        },

        /**
		 * Solicita al servicio correspondiente crear un registro CENTER,
		 * dado un cliente, una sociedad y una granja
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        onCreate: function(oEvent) {
            //Si el registro que se desea crear es válido
            //console.log("llego al oncreate")
            if (this._validRecord()) {
                /**
				 * @type {JSONModel} CENTER Referencia al modelo "CENTER"
				 * @type {JSONModel} dummy    Referencia al modelo "dummy"
				 * @type {Controller} that    Referencia a este controlador
				 * @type {Object} json        Objeto a enviar en la solicitud
				 * @type {Object} settings    Opciones de la llamada a la función ajax
				 */
                var ospartnership = this.getView().getModel("OSPARTNERSHIP"),
                    osfarm = this.getView().getModel("OSFARM"),
                    oscenter = this.getView().getModel("OSCENTER"),
                    osshed = this.getView().getModel("OSSHED"),
                    util = this.getView().getModel("util"),
                    that = this,
                    serviceUrl = util.getProperty("/serviceUrl"),
                    json = {
                        "partnership_id": ospartnership.getProperty(ospartnership.getProperty("/selectedRecordPath/") + "/partnership_id"),
                        "farm_id": osfarm.getProperty(osfarm.getProperty("/selectedRecordPath/") + "/farm_id"),
                        "center_id": oscenter.getProperty(oscenter.getProperty("/selectedRecordPath/") + "/center_id"),
                        "code": osshed.getProperty("/name/value"),
                        "stall_height": (osshed.getProperty("/stall_height/value")).toString(),
                        "stall_width": (osshed.getProperty("/stall_width/value")).toString(),
                        "status_id": (osshed.getProperty("/status/value")),
                        "environment": (osshed.getProperty("/environment/value")),
                        "capacity_min": (osshed.getProperty("/capmin/value")),
                        "capacity_max": (osshed.getProperty("/capmax/value")),
                        "rotation_days": (osshed.getProperty("/rotation_days/value"))
                    },

                    settings = {
                        async: true,
                        url: serviceUrl + "/shed",
                        method: "POST",
                        data: JSON.stringify(json),
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function(res) {
                            util.setProperty("/busy/", false);
                            that._resetRecordValues();
                            that.onToast(that.getI18n().getText("OS.recordCreated"));
                            that.getRouter().navTo("restrainingOrder", {}, true /*no history*/ );
                        },
                        error: function(err) {
                            that.onToast("Error: " + err);
                            console.log("Read failed ");
                        }
                    };
                util.setProperty("/busy/", true);
                //realiza la llamada ajax
                $.ajax(settings);
            }
        },

        /**
		 * Coincidencia de ruta para acceder a los detalles de un registro
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        _onRecordMatched: function(oEvent) {
            //Establece las opciones disponibles al visualizar el registro
            this._viewOptions();
            this.onLoadSheds();

        },

        /**
		 * Cambia las opciones de visualización disponibles en la vista de detalles de un registro
		 */
        _viewOptions: function() {
            /**
			 * @type {JSONModel} OS       Referencia al modelo "OS"
			 * @type {JSONModel} SHED Referencia al modelo "SHED"
			 */
            var util = this.getView().getModel("util"),
                osshed = this.getView().getModel("OSSHED");

            if (util.getProperty("/selectedEntity/") === "osshed") {
                osshed.setProperty("/modify/", true);
                osshed.setProperty("/delete/", true);
            } else {
                osshed.setProperty("/modify/", false);
                osshed.setProperty("/delete/", false);
            }

            osshed.setProperty("/save/", false);
            osshed.setProperty("/cancel/", false);

            this._editRecordValues(false);
        },

        /**
		 * Ajusta la vista para visualizar los datos de un registro
		 */
        onView: function() {
            this._viewOptions();
        },

        /**
		 * Cambia las opciones de edición disponibles en la vista de detalles de un registro
		 */
        _editOptions: function() {
            /** @type {JSONModel} SHED Referencia al modelo "SHED" */
            var osshed = this.getView().getModel("OSSHED");

            osshed.setProperty("/modify/", false);
            osshed.setProperty("/delete/", false);
            osshed.setProperty("/save/", true);
            osshed.setProperty("/cancel/", true);

            this._editRecordValues(true);
        },

        /**
		 * Ajusta la vista para editar los datos de un registro
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        onEdit: function(oEvent) {
            this._editOptions();
        },

        /**
		 * Verifica si el registro seleccionado tiene algún cambio con respecto a sus valores originales
		 * @return {Boolean} Devuelve "true" el registro cambió, y "false" si no cambió
		 */
        _recordChanged: function() {
            /**
			 * @type {JSONModel} SHED Referencia al modelo "SHED"
			 * @type {Boolean} flag       "true" si el registro cambió, "false" si no cambió
			 */
            var osshed = this.getView().getModel("OSSHED"),
                flag = false;

            if (osshed.getProperty("/name/value") !==
				osshed.getProperty(osshed.getProperty("/selectedRecordPath/") + "/code")) {
                flag = true;
                //console.log(osshed.getProperty("/name/value"));
                console.log(osshed.getProperty(osshed.getProperty("/selectedRecordPath/") + "/code"));
            }

            if (osshed.getProperty("/stall_height/value").toString() !==
				osshed.getProperty(osshed.getProperty("/selectedRecordPath/") + "/stall_height").toString()) {
                flag = true;
                //this.onToast(osshed.getProperty("/stall_height/value"));
            }

            if (osshed.getProperty("/stall_width/value").toString() !==
				osshed.getProperty(osshed.getProperty("/selectedRecordPath/") + "/stall_width").toString()) {
                flag = true;
            }

            if (osshed.getProperty("/status/value") !==
				osshed.getProperty(osshed.getProperty("/selectedRecordPath/") + "/status_id")) {
                flag = true;
                //console.log(osshed.getProperty("/status/value"));
                //console.log(osshed.getProperty(osshed.getProperty("/selectedRecordPath/")));
            }

            if (osshed.getProperty("/environment/value") !==
				osshed.getProperty(osshed.getProperty("/selectedRecordPath/") + "/environment_id")) {
                flag = true;
                //console.log(osshed.getProperty("/environment/value"));
                //console.log(osshed.getProperty(osshed.getProperty("/selectedRecordPath/")));
            }

            if (osshed.getProperty("/capmin/value") !==
				osshed.getProperty(osshed.getProperty("/selectedRecordPath/") + "/capacity_min")) {
                flag = true;
                //console.log(osshed.getProperty("/capmin/value"));
                //console.log(osshed.getProperty(osshed.getProperty("/selectedRecordPath/")));
            }

            if (osshed.getProperty("/capmax/value").toString() !==
				osshed.getProperty(osshed.getProperty("/selectedRecordPath/") + "/capacity_max").toString()) {
                flag = true;
            }

            if (osshed.getProperty("/rotation_days/value") !==
				osshed.getProperty(osshed.getProperty("/selectedRecordPath/") + "/rotation_days")) {
                flag = true;
            }

            /*			if (osshed.getProperty("/quantity_nests/value") !==
				osshed.getProperty(osshed.getProperty("/selectedRecordPath/") + "/nests_quantity")) {
				flag = true;
			}

			if (osshed.getProperty("/quantity_cages/value").toString() !==
				osshed.getProperty(osshed.getProperty("/selectedRecordPath/") + "/cages_quantity")) {
				flag = true;
			}
*/
            if (!flag) this.onToast("No se detectaron cambios");

            return flag;
        },


        validateFloatInput: function (o) {
            //console.log("ENTRO VAL FLOAT")
            let input= o.getSource();
            let floatLength=10,
                intLength = 10;
            //console.log("entro en la funcion v")
            let value = input.getValue();
            let regex = new RegExp(`/^([0-9]{1,${intLength}})([.][0-9]{0,${floatLength}})?$/`);
            if (regex.test(value)) {
                input.setValueState("None");
                input.setValueStateText("");
                return true;
            }
            else {
                let pNumber = 0;
                let aux = value
                    .split("")
                    .filter(char => {
                        if (/^[0-9.]$/.test(char)) {
                            if (char !== ".") {
                                return true;
                            }
                            else {
                                if (pNumber === 0) {
                                    pNumber++;
                                    return true;
                                }
                            }
                        }
                    })
                    .join("")
                    .split(".");
                value = aux[0].substring(0, intLength);

                if (aux[1] !== undefined) {
                    value += "." + aux[1].substring(0, floatLength);
                }
                input.setValue(value);
                return false;
            }
        },

        validateIntInput: function (o) {
            let input= o.getSource();
            let length = 10;
            let value = input.getValue();
            let regex = new RegExp(`/^[0-9]{1,${length}}$/`);

            if (regex.test(value)) {
                return true;
            }
            else {
                let aux = value
                    .split("")
                    .filter(char => {
                        if (/^[0-9]$/.test(char)) {
                            if (char !== ".") {
                                return true;
                            }
                        }
                    })
                    .join("");
                value = aux.substring(0, length);
                input.setValue(value);
                return false;
            }
        },

        /**
		 * Solicita al servicio correspondiente actualizar un silo
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        onUpdate: function(oEvent) {
            /**
			 * Si el registro que se quiere actualizar es válido y hubo algún cambio
			 * con respecto a sus datos originales
			 */


            if (this._validRecord() && this._recordChanged()) {

                /**
				 * @type {JSONModel} CENTER Referencia al modelo "CENTER"
				 * @type {JSONModel} dummy    Referencia al modelo "dummy"
				 * @type {Controller} that    Referencia a este controlador
				 */
                var osshed = this.getView().getModel("OSSHED"),
                    util = this.getView().getModel("util"),
                    that = this,
                    serviceUrl = util.getProperty("/serviceUrl");

                /** @type {Object} settings Opciones de la llamada a la función ajax */
                //console.log(osshed);
                var settings = {
                    async: true,
                    url: serviceUrl + "/shed",
                    method: "PUT",
                    data: JSON.stringify({
                        "shed_id": osshed.getProperty(osshed.getProperty("/selectedRecordPath/") + "/shed_id"),
                        "code": osshed.getProperty("/name/value"),
                        "stall_height": osshed.getProperty("/stall_height/value"),
                        "stall_width": osshed.getProperty("/stall_width/value"),
                        "status_id": osshed.getProperty("/status/value"),
                        "environment_id": osshed.getProperty("/environment/value"),
                        "capacity_max": osshed.getProperty("/capmax/value"),
                        "capacity_min": osshed.getProperty("/capmin/value"),
                        "rotation_days": (osshed.getProperty("/rotation_days/value"))
                    }),
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function(res) {
                        //console.log(res);
                        util.setProperty("/busy/", false);
                        that._resetRecordValues();
                        that._viewOptions();
                        that.onToast(that.getI18n().getText("OS.recordUpdated"));
                        that.getRouter().navTo("restrainingOrder", {}, true /*no history*/ );
                    },
                    error: function(err) {
                        that.onToast("Error de comunicación");
                        console.log("Read failed ",err);
                    }
                };
                util.setProperty("/busy/", true);
                $.ajax(settings);
                //console.log(json);
            }
        },

        /**
		 * Solicita al servicio correspondiente eliminar un galpon
		 * @param  {Controller} that         Referencia al controlador que llama esta función
		 * @param  {JSONModel} dummy         Referencia al modelo "dummy"
		 * @param  {JSONModel} PARTNERSHIP Referencia al modelo "PARTNERSHIP"
		 */
        onDelete: function(that, util, osshed) {

            /**
			 * @type {Object} json     Objeto a enviar en la solicitud
			 * @type {Object} settings Opciones de la llamada a la función ajax
			 */
            var json = {
                    "shed_id": osshed.getProperty(osshed.getProperty("/selectedRecordPath/") + "/shed_id")
                },
                serviceUrl = util.getProperty("/serviceUrl"),
                settings = {
                    async: true,
                    url: serviceUrl + "/shed",
                    method: "DELETE",
                    data: JSON.stringify(json),
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function(res) {
                        util.setProperty("/busy/", false);
                        that.onToast(that.getI18n().getText("El galpon se ha eliminado"));
                        that.getRouter().navTo("restrainingOrder", {}, true);
                    },
                    error: function(err) {
                        that.onToast("Error de comunicación");
                        console.log("Read failed", err);
                    }
                };
            util.setProperty("/busy/", true);
            //Realiza la llamada ajax
            $.ajax(settings);
        }
    });
});
