sap.ui.define([
    "technicalConfiguration/controller/BaseController",
    "sap/ui/model/json/JSONModel",
    "sap/m/Dialog",
    "sap/m/Button",
    "sap/m/Text"
], function(BaseController, JSONModel,Dialog,Button, Text) {
    "use strict";

    return BaseController.extend("technicalConfiguration.controller.ospartnership", {

        onInit: function() {
            //ruta para la vista principal
            this.getOwnerComponent().getRouter().getRoute("ospartnership").attachPatternMatched(this._onRouteMatched, this);
            //ruta para la vista de detalles de un registro
            this.getOwnerComponent().getRouter().getRoute("ospartnership_Record").attachPatternMatched(this._onRecordMatched, this);
            //ruta para la vista de creación de un registro
            this.getOwnerComponent().getRouter().getRoute("ospartnership_Create").attachPatternMatched(this._onCreateMatched, this);
        },

        /**
         * Coincidencia de ruta para acceder a la vista principal
         * @param  {Event} oEvent Evento que llamó esta función
         */
        _onRouteMatched: function(oEvent) {
            /**
             * @type {Controller} that         Referencia a este controlador
             * @type {JSONModel} util         Referencia al modelo "util"
             * @type {JSONModel} OS            Referencia al modelo "OS"
             * @type {JSONModel} MDOSPARTNERSHIP        Referencia al modelo "ospartnership"
             */

            var that = this,
                util = this.getView().getModel("util"),
                ospartnership = this.getView().getModel("OSPARTNERSHIP");

            //dependiendo del dispositivo, establece la propiedad "phone"
            this.getView().getModel("util").setProperty("/phone/",
                this.getOwnerComponent().getContentDensityClass() === "sapUiSizeCozy");


            //establece ospartnership como la entidad seleccionada
            util.setProperty("/selectedEntity/", "ospartnership");
            ospartnership.setProperty("/settings/tableMode", "None");

            //obtiene los registros de ospartnership
            this.onRead(that, util, ospartnership);
        },
        /**
         * Obtiene todos los registros de ospartnership
         * @param  {Controller} that         Referencia al controlador que llama esta función
         * @param  {JSONModel} util         Referencia al modelo "util"
         * @param  {JSONModel} ospartnership Referencia al modelo "ospartnership"
         */
        onRead: function(that, util, ospartnership) {
            console.log("llegue");
            /** @type {Object} settings opciones de la llamada a la función ajax */
            var serviceUrl = util.getProperty("/serviceUrl");
            var settings = {
                url: serviceUrl+"/partnership",
                method: "GET",
                success: function(res) {
                    console.log(res.data);
                    util.setProperty("/busy/", false);
                    ospartnership.setProperty("/records/", res.data);

                },
                error: function(err) {
                    util.setProperty("/error/status", err.status);
                    util.setProperty("/error/statusText", err.statusText);
                    //that.onConnectionError();
                }
            };
            console.log(util);
            util.setProperty("/busy/", true);
            //borra los registros OSPARTNERSHIP que estén almacenados actualmente
            ospartnership.setProperty("/records/", []);
            //realiza la llamada ajax
            $.ajax(settings);
        },
        /**
         * Coincidencia de ruta para acceder a la creación de un registro
         * @param  {Event} oEvent Evento que llamó esta función
         */
        _onCreateMatched: function(oEvent) {

            this._resetRecordValues();
            this._editRecordValues(true);
            this._editRecordRequired(true);
        },
        /**
         * Resetea todos los valores existentes en el formulario del registro
         */
        _resetRecordValues: function() {
            /**
             * @type {JSONModel} ospartnership Referencia al modelo "ospartnership"
             */
            var ospartnership = this.getView().getModel("OSPARTNERSHIP");

            ospartnership.setProperty("/stage_id/value", "");

            ospartnership.setProperty("/name/editable", true);
            ospartnership.setProperty("/name/value", "");
            ospartnership.setProperty("/name/state", "None");
            ospartnership.setProperty("/name/stateText", "");

            ospartnership.setProperty("/code/editable", true);
            ospartnership.setProperty("/code/value", "");
            ospartnership.setProperty("/code/state", "None");
            ospartnership.setProperty("/code/stateText", "");

            ospartnership.setProperty("/description/editable", true);
            ospartnership.setProperty("/description/value", "");
            ospartnership.setProperty("/description/state", "None");
            ospartnership.setProperty("/description/stateText", "");

            ospartnership.setProperty("/address/editable", true);
            ospartnership.setProperty("/address/value", "");
            ospartnership.setProperty("/address/state", "None");
            ospartnership.setProperty("/address/stateText", "");

            ospartnership.setProperty("/disable/value", false);

        },
        /**
         * Habilita/deshabilita la edición de los datos de un registro ospartnership
         * @param  {Boolean} edit "true" si habilita la edición, "false" si la deshabilita
         */
        _editRecordValues: function(edit) {

            var ospartnership = this.getView().getModel("OSPARTNERSHIP");
            ospartnership.setProperty("/name/editable", edit);
            ospartnership.setProperty("/code/editable", edit);
            ospartnership.setProperty("/description/editable", edit);
            ospartnership.setProperty("/address/editable", edit);
            ospartnership.setProperty("/disable/editable", edit);

        },
        _editRecordValues2: function(edit) {

            var ospartnership = this.getView().getModel("OSPARTNERSHIP");
            // ospartnership.setProperty("/name/editable", edit);
            // ospartnership.setProperty("/code/editable", edit);
            ospartnership.setProperty("/description/editable", edit);
            ospartnership.setProperty("/address/editable", edit);
            ospartnership.setProperty("/disable/editable", edit);

        },
        /**
         * Se define un campo como obligatorio o no, de un registro ospartnership
         * @param  {Boolean} edit "true" si es campo obligatorio, "false" si no es obligatorio
         */
        _editRecordRequired: function(edit) {
            var ospartnership = this.getView().getModel("OSPARTNERSHIP");

            ospartnership.setProperty("/name/required", edit);
            ospartnership.setProperty("/code/required", edit);
            ospartnership.setProperty("/description/required", edit);
            ospartnership.setProperty("/address/required", edit);

        },
        /**
         * Navega a la vista para crear un nuevo registro
         * @param  {Event} oEvent Evento que llamó esta función
         */
        onNewRecord: function(oEvent) {
            this.getRouter().navTo("ospartnership_Create", {}, false /*create history*/ );
        },
        /**
         * Cancela la creación de un registro ospartnership, y regresa a la vista principal
         * @param  {Event} oEvent Evento que llamó esta función
         */
        onCancelCreate: function(oEvent) {
            this._resetRecordValues();
            this.onNavBack(oEvent);
        },
        /**
         * Regresa a la vista principal de la entidad seleccionada actualmente
         * @param  {Event} oEvent Evento que llamó esta función
         */
        onNavBack: function(oEvent) {
            /** @type {JSONModel} OS Referencia al modelo "OS" */
            var util = this.getView().getModel("util");

            this.getRouter().navTo(util.getProperty("/selectedEntity"), {}, false /*create history*/ );
        },
        /**
         * Solicita al servicio correspondiente crear un registro ospartnership
         * @param  {Event} oEvent Evento que llamó esta función
         */
        onCreate: function(oEvent) {
            //Si el registro que se desea crear es válido
            if (this._validRecord()) {
                var that = this,
                    util = this.getView().getModel("util"),
                    ospartnership = this.getView().getModel("OSPARTNERSHIP"),
                    serviceUrl = util.getProperty("/serviceUrl");
                console.log(ospartnership.getProperty("/disable/value"));
                $.ajax({
                    type: "POST",
                    contentType: "application/json",
                    data: JSON.stringify({
                        "name": ospartnership.getProperty("/name/value"),
                        "code": ospartnership.getProperty("/code/value"),
                        "description": ospartnership.getProperty("/description/value"),
                        "address": ospartnership.getProperty("/address/value"),
                        "os_disable": ospartnership.getProperty("/disable/value")
                        
                    }),
                    url: serviceUrl+"/partnership",
                    dataType: "json",
                    async: true,
                    success: function(data) {
                        util.setProperty("/busy/", false);
                        that._resetRecordValues();
                        that.onToast(that.getI18n().getText("OS.recordCreated"));
                        that.getRouter().navTo("ospartnership", {}, true /*no history*/ );

                    },
                    error: function(error) {
                        that.onToast("Error: " + error.responseText);
                        console.log("Read failed ");
                    }
                });

            }
        },
        /**
         * Valida la correctitud de los datos existentes en el formulario del registro
         * @return {Boolean} Devuelve "true" si los datos son correctos, y "false" si son incorrectos
         */
        _validRecord: function() {
            /**
             * @type {JSONModel} ospartnership Referencia al modelo "ospartnership"
             * @type {Boolean} flag "true" si los datos son válidos, "false" si no lo son
             */
            var ospartnership = this.getView().getModel("OSPARTNERSHIP"),
                flag = true,
                that = this;

            if (ospartnership.getProperty("/name/value") === "") {
                flag = false;
                ospartnership.setProperty("/name/state", "Error");
                ospartnership.setProperty("/name/stateText", this.getI18n().getText("enter.FIELD"));
            } else {
                ospartnership.setProperty("/name/state", "None");
                ospartnership.setProperty("/name/stateText", "");
            }

            if (ospartnership.getProperty("/code/value") === "") {
                flag = false;
                ospartnership.setProperty("/code/state", "Error");
                ospartnership.setProperty("/code/stateText", this.getI18n().getText("enter.FIELD"));
            } else {
                ospartnership.setProperty("/code/state", "None");
                ospartnership.setProperty("/code/stateText", "");
            }

            if (ospartnership.getProperty("/description/value") === "") {
                flag = false;
                ospartnership.setProperty("/description/state", "Error");
                ospartnership.setProperty("/description/stateText", this.getI18n().getText("enter.FIELD"));
            }  else {
                ospartnership.setProperty("/description/state", "None");
                ospartnership.setProperty("/description/stateText", "");
            }

            if (ospartnership.getProperty("/address/value") === "") {
                flag = false;
                ospartnership.setProperty("/address/state", "Error");
                ospartnership.setProperty("/address/stateText", this.getI18n().getText("enter.FIELD"));
            }  else {
                ospartnership.setProperty("/address/state", "None");
                ospartnership.setProperty("/address/stateText", "");
            }

            return flag;
        },
        _validRecord2: function() {
            /**
             * @type {JSONModel} ospartnership Referencia al modelo "ospartnership"
             * @type {Boolean} flag "true" si los datos son válidos, "false" si no lo son
             */
            var ospartnership = this.getView().getModel("OSPARTNERSHIP"),
                flag = true,
                that = this;

            // if (ospartnership.getProperty("/name/value") === "") {
            //     flag = false;
            //     ospartnership.setProperty("/name/state", "Error");
            //     ospartnership.setProperty("/name/stateText", this.getI18n().getText("enter.FIELD"));
            // } else {
            //     ospartnership.setProperty("/name/state", "None");
            //     ospartnership.setProperty("/name/stateText", "");
            // }

            // if (ospartnership.getProperty("/code/value") === "") {
            //     flag = false;
            //     ospartnership.setProperty("/code/state", "Error");
            //     ospartnership.setProperty("/code/stateText", this.getI18n().getText("enter.FIELD"));
            // } else {
            //     ospartnership.setProperty("/code/state", "None");
            //     ospartnership.setProperty("/code/stateText", "");
            // }

            if (ospartnership.getProperty("/description/value") === "") {
                flag = false;
                ospartnership.setProperty("/description/state", "Error");
                ospartnership.setProperty("/description/stateText", this.getI18n().getText("enter.FIELD"));
            }  else {
                ospartnership.setProperty("/description/state", "None");
                ospartnership.setProperty("/description/stateText", "");
            }

            if (ospartnership.getProperty("/address/value") === "") {
                flag = false;
                ospartnership.setProperty("/address/state", "Error");
                ospartnership.setProperty("/address/stateText", this.getI18n().getText("enter.FIELD"));
            }  else {
                ospartnership.setProperty("/address/state", "None");
                ospartnership.setProperty("/address/stateText", "");
            }

            return flag;
        },
        /**
         * Coincidencia de ruta para acceder a los detalles de un registro
         * @param  {Event} oEvent Evento que llamó esta función
         */
        _onRecordMatched: function(oEvent) {

            this._viewOptions();

        },
        /**
         * Cambia las opciones de visualización disponibles en la vista de detalles de un registro
         */
        _viewOptions: function() {
            var ospartnership = this.getView().getModel("OSPARTNERSHIP");
            ospartnership.setProperty("/save/", false);
            ospartnership.setProperty("/cancel/", false);
            ospartnership.setProperty("/modify/", true);
            ospartnership.setProperty("/delete/", true);

            this._editRecordValues(false);
            this._editRecordRequired(false);
        },
        /**
         * Ajusta la vista para editar los datos de un registro
         * @param  {Event} oEvent Evento que llamó esta función
         */
        onEdit: function(oEvent) {

            var ospartnership = this.getView().getModel("OSPARTNERSHIP");
            ospartnership.setProperty("/save/", true);
            ospartnership.setProperty("/cancel/", true);
            ospartnership.setProperty("/modify/", false);
            ospartnership.setProperty("/delete/", false);
            this._editRecordRequired(true);
            this._editRecordValues2(true);
        },

        /**
         * Cancela la edición de un registro ospartnership
         * @param  {Event} oEvent Evento que llamó esta función
         */
        onCancelEdit: function(oEvent) {
            /** @type {JSONModel} ospartnership  Referencia al modelo ospartnership */

            this.onView();
        },
        /**
         * Ajusta la vista para visualizar los datos de un registro
         */
        onView: function() {
            this._viewOptions();
        },
        /**
         * Solicita al servicio correspondiente actualizar un registro ospartnership
         * @param  {Event} oEvent Evento que llamó esta función
         */
        onUpdate: function(oEvent) {
            /**
             * Si el registro que se quiere actualizar es válido y hubo algún cambio
             * con respecto a sus datos originales
             */
            if (this._validRecord() && this._recordChanged2()) {
                /**
                 * @type {JSONModel} ospartnership       Referencia al modelo "ospartnership"
                 * @type {JSONModel} util         Referencia al modelo "util"
                 * @type {Controller} that         Referencia a este controlador
                 */
                var ospartnership = this.getView().getModel("OSPARTNERSHIP"),
                    util = this.getView().getModel("util"),
                    that = this,
                    serviceUrl = util.getProperty("/serviceUrl");

                $.ajax({
                    type: "PUT",
                    contentType: "application/json",
                    data: JSON.stringify({
                        "partnership_id": ospartnership.getProperty("/partnership_id/value"),
                        "name": ospartnership.getProperty("/name/value"),
                        "code": ospartnership.getProperty("/code/value"),
                        "description": ospartnership.getProperty("/description/value"),
                        "address": ospartnership.getProperty("/address/value"),
                        "os_disable": ospartnership.getProperty("/disable/value")
                        
                    }),
                    url: serviceUrl+"/partnership/",
                    dataType: "json",
                    async: true,
                    success: function(data) {

                        util.setProperty("/busy/", false);
                        that._resetRecordValues();
                        that._viewOptions();
                        that.onToast(that.getI18n().getText("OS.recordUpdated"));
                        that.getRouter().navTo("ospartnership", {}, true /*no history*/ );

                    },
                    error: function(request, status, error) {
                        that.onToast("Error de comunicación");
                        console.log("Read failed");
                    }
                });
            }
        },
        /**
         * Verifica si el registro seleccionado tiene algún cambio con respecto a sus valores originales
         * @return {Boolean} Devuelve "true" el registro cambió, y "false" si no cambió
         */
        _recordChanged: function() {
            /**
             * @type {JSONModel} OSPARTNERSHIP Referencia al modelo "OSPARTNERSHIP"
             * @type {Boolean} flag            "true" si el registro cambió, "false" si no cambió
             */
            var ospartnership = this.getView().getModel("OSPARTNERSHIP"),
                flag = false;

            if (ospartnership.getProperty("/name/value") !== ospartnership.getProperty(ospartnership.getProperty("/selectedRecordPath/") + "/name")) {
                flag = true;
            }

            if (ospartnership.getProperty("/code/value") !== ospartnership.getProperty(ospartnership.getProperty("/selectedRecordPath/") + "/code")) {
                flag = true;
            }

            if (ospartnership.getProperty("/description/value") !== ospartnership.getProperty(ospartnership.getProperty("/selectedRecordPath/") + "/description")) {
                flag = true;
            }

            if (ospartnership.getProperty("/address/value") !== ospartnership.getProperty(ospartnership.getProperty("/selectedRecordPath/") + "/address")) {
                flag = true;
            }

            if(!flag) this.onToast("No se detectaron cambios");

            return flag;
        },
        _recordChanged2: function() {
            /**
             * @type {JSONModel} OSPARTNERSHIP Referencia al modelo "OSPARTNERSHIP"
             * @type {Boolean} flag            "true" si el registro cambió, "false" si no cambió
             */
            var ospartnership = this.getView().getModel("OSPARTNERSHIP"),
                flag = false;

            // if (ospartnership.getProperty("/name/value") !== ospartnership.getProperty(ospartnership.getProperty("/selectedRecordPath/") + "/name")) {
            //     flag = true;
            // }

            // if (ospartnership.getProperty("/code/value") !== ospartnership.getProperty(ospartnership.getProperty("/selectedRecordPath/") + "/code")) {
            //     flag = true;
            // }

            if (ospartnership.getProperty("/description/value") !== ospartnership.getProperty(ospartnership.getProperty("/selectedRecordPath/") + "/description")) {
                flag = true;
            }

            if (ospartnership.getProperty("/address/value") !== ospartnership.getProperty(ospartnership.getProperty("/selectedRecordPath/") + "/address")) {
                flag = true;
            }

            if(!flag) this.onToast("No se detectaron cambios");

            return flag;
        },
        onPartnershipSearch: function(oEvent){
            var aFilters = [],
                sQuery = oEvent.getSource().getValue(),
                binding = this.getView().byId("partnershipTable").getBinding("items");

            if (sQuery && sQuery.length > 0) {
                /** @type {Object} filter1 Primer filtro de la búsqueda */
                var filter1 = new sap.ui.model.Filter("name", sap.ui.model.FilterOperator.Contains, sQuery);
                aFilters = new sap.ui.model.Filter([filter1]);
            }

            //se actualiza el binding de la lista
            binding.filter(aFilters);

        },
        onVerifyIsUsed: async function(partnership_id){
            let ret;

            const response = await fetch("/partnership/isBeingUsed", {
                headers: {
                    "Content-Type": "application/json"
                },
                method: "POST",
                body: JSON.stringify({
                    partnership_id: partnership_id
                })
            });

            if (response.status !== 200 && response.status !== 409) {
                console.log("Looks like there was a problem. Status Code: " +
                    response.status);
                return;
            }
            if(response.status === 200){
                const res = await response.json();

                ret = res.data.used;
            }
            

            // .then(
            //     function(response) {
            //       if (response.status !== 200 && response.status !== 409) {
            //         console.log('Looks like there was a problem. Status Code: ' +
            //           response.status);
            //         return;
            //       }
                  
            //       if(response.status === 200){
            //         response.json().then(function(res) {
            //             console.log("La res: ",res.data.used)
            //           resolve( res.data.used)
                    
            //         });
            //       }
                  
            //     }
            //   )
            // .catch(function(err) {
            //     console.log('Fetch Error :-S', err);
            // });
            console.log(ret);
            return ret;
            
        },
        onConfirmDelete: async function(oEvent){

            let oBundle = this.getView().getModel("i18n").getResourceBundle(),
                deleteRecord = oBundle.getText("deleteRecord"),
                confirmation = oBundle.getText("confirmation"),
                util = this.getView().getModel("util"),
                serviceUrl = util.getProperty("/serviceUrl"),
                that = this,
                ospartnership = that.getView().getModel("OSPARTNERSHIP"),
                partnership_id = ospartnership.getProperty("/partnership_id/value");
            
            let cond = await this.onVerifyIsUsed(partnership_id);
            if(cond){
                var dialog = new Dialog({
                    title: "Información",
                    type: "Message",
                    state: "Warning",
                    content: new Text({
                        text: "No se puede eliminar la empresa, porque está siendo utilizada."
                    }),
                    beginButton: new Button({
                        text: "OK",
                        press: function() {
                            dialog.close();
                        }
                    }),
                    afterClose: function() {
                        dialog.destroy();
                    }
                });
    
                dialog.open();
            }else{
                var dialog = new Dialog({
                    title: confirmation,
                    type: "Message",
                    content: new sap.m.Text({
                        text: deleteRecord
                    }),

                    beginButton: new Button({
                        text: "Si",
                        press: function() {
                            util.setProperty("/busy/", true);
                        
                            $.ajax({
                                type: "DELETE",
                                contentType: "application/json",
                                data: JSON.stringify({
                                    "partnership_id": partnership_id
                                }),
                                url: serviceUrl+"/partnership/",
                                dataType: "json",
                                async: true,
                                success: function(data) {

                                    util.setProperty("/busy/", false);
                                    that.getRouter().navTo("ospartnership", {}, true);
                                    dialog.close();
                                    dialog.destroy();

                                },
                                error: function(request, status, error) {
                                    that.onToast("Error de comunicación");
                                    console.log("Read failed");
                                }
                            });

                        }
                    }),
                    endButton: new Button({
                        text: "No",
                        press: function() {
                            dialog.close();
                            dialog.destroy();
                        }
                    })
                });

                dialog.open();
            }
            console.log(cond);
            
            

        }

    });
});
