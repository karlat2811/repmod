sap.ui.define([
    "technicalConfiguration/controller/BaseController",
    "sap/ui/model/json/JSONModel",
    "sap/m/Dialog",
    "sap/m/Button",
    "sap/m/Text"
], function(BaseController, JSONModel,Dialog,Button, Text) {
    "use strict";

    return BaseController.extend("technicalConfiguration.controller.osfarm", {

        onInit: function() {
            //ruta para la vista principal
            this.getOwnerComponent().getRouter().getRoute("osfarm").attachPatternMatched(this._onRouteMatched, this);
            //ruta para la vista de detalles de un registro
            this.getOwnerComponent().getRouter().getRoute("osfarm_Record").attachPatternMatched(this._onRecordMatched, this);
            //ruta para la vista de creación de un registro
            this.getOwnerComponent().getRouter().getRoute("osfarm_Create").attachPatternMatched(this._onCreateMatched, this);
        },

        /**
         * Coincidencia de ruta para acceder a la vista principal
         * @param  {Event} oEvent Evento que llamó esta función
         */
        _onRouteMatched: function(oEvent) {
            /**
             * @type {Controller} that         Referencia a este controlador
             * @type {JSONModel} util         Referencia al modelo "util"
             * @type {JSONModel} OS            Referencia al modelo "OS"
             * @type {JSONModel} MDSTAGE        Referencia al modelo "MdSTAGE"
             */

            var that = this,
                util = this.getView().getModel("util"),
                ospartnership = this.getView().getModel("OSPARTNERSHIP"),
                osfarm = this.getView().getModel("OSFARM");

            //dependiendo del dispositivo, establece la propiedad "phone"
            util.setProperty("/phone/",
                this.getOwnerComponent().getContentDensityClass() === "sapUiSizeCozy");

            ospartnership.setProperty("/settings/tableMode", "SingleSelect");
          			osfarm.setProperty("/settings/tableMode", "None");

            //si la entidad seleccionada antes de acceder a esta vista es diferente a BROILERSFARM
            console.log(util.getProperty("/selectedEntity"));
            if (util.getProperty("/selectedEntity") !== "osfarm") {

                //establece BROILERSFARM como la entidad seleccionada
                util.setProperty("/selectedEntity", "osfarm");

                //limpio selectedRecord
                ospartnership.setProperty("/selectedRecord", "");

                //establece el tab de la tabla PARTNERSHIP como el tab seleccionado
                this.getView().byId("tabBar").setSelectedKey(this.getView().getId() + "--" + "partnershipFilter");

                //borra cualquier selección que se haya hecho en la tabla PARTNERSHIP
                this.getView().byId("partnershipTable").removeSelections(true);

                osfarm.setProperty("/records/", []);

                //establece que no hay ningún registro PARTNERSHIP seleccionado
                ospartnership.setProperty("/selectedRecordPath/", "");

                //deshabilita el tab de la tabla de registros BROILERSFARM
                osfarm.setProperty("/settings/enabledTab", false);

                //deshabilita la opción de crear un registro BROILERSFARM
                osfarm.setProperty("/new", false);

                //obtiene los registros de PARTNERSHIP
                sap.ui.controller("technicalConfiguration.controller.ospartnership").onRead(that, util, ospartnership);

            } else if (ospartnership.getProperty("/selectedRecordPath/") !== "") {

                //habilita el tab de la tabla de registros BROILERSFARM
                osfarm.setProperty("/settings/enabledTab", true);

                //habilita la opción de crear un registro BROILERSFARM
                osfarm.setProperty("/new", true);

                //obtiene los registros de BROILERSFARM
                this.onRead(that, util, ospartnership, osfarm);
            }
        },
        /**
         * Obtiene todos los registros de MDSTAGE
         * @param  {Controller} that         Referencia al controlador que llama esta función
         * @param  {JSONModel} util         Referencia al modelo "util"
         * @param  {JSONModel} MDSTAGE Referencia al modelo "MDSTAGE"
         */
        onRead: function(that, util, ospartnership, osfarm) {
     
            var serviceUrl = util.getProperty("/serviceUrl");
            var partnership_id = ospartnership.getProperty(ospartnership.getProperty("/selectedRecordPath/") + "/partnership_id");
            var allFarmsTypes = [];
            // var all = this.getI18n().getText("all");
            console.log(partnership_id);
            var settings = {
                type: "POST",
                contentType: "application/json",
                data: JSON.stringify({
                    "partnership_id": partnership_id,
                }),
                url: serviceUrl+"/farm/findFarmByPartnetship",
                dataType: "json",
                async: true,
                success: function(res) {
                    console.log(res.data);
                    util.setProperty("/busy/", false);
                    res.data.forEach(element => {
                        element.old = element.order;
                    });
                    osfarm.setProperty("/records/", res.data);

                    if (res.types.length > 0) {
                        allFarmsTypes.push({"farmtype":"Todos"});
                        res.types.forEach(element => {
                            allFarmsTypes.push({"farmtype":element.farmtype});
                        });
                        //console.log(allFarmsTypes);
                        osfarm.setProperty("/farmtypes/", allFarmsTypes);
                        osfarm.setProperty("/All/",osfarm.getProperty("/records"));
                    }
            
                    console.log("osfarm");
                    console.log(osfarm);
                },
                error: function(err) {
                    util.setProperty("/error/status", err.status);
                    util.setProperty("/error/statusText", err.statusText);
                    //that.onConnectionError();
                }
            };
            console.log(util);
            util.setProperty("/busy/", true);
            //borra los registros OSPARTNERSHIP que estén almacenados actualmente
            osfarm.setProperty("/records/", []);
            //realiza la llamada ajax
            $.ajax(settings);
        },
        /**
         * Coincidencia de ruta para acceder a la creación de un registro
         * @param  {Event} oEvent Evento que llamó esta función
         */
        _onCreateMatched: function(oEvent) {

            this._resetRecordValues();
            this._editRecordValues(true);
            this._editRecordRequired(true);
            this.getFarmType(true);
            this.getIncubatorPlant(true);
        },
        /**
         * Resetea todos los valores existentes en el formulario del registro
         */
        _resetRecordValues: function() {
            /**
             * @type {JSONModel} MDSTAGE Referencia al modelo "MDSTAGE"
             */
            var osfarm = this.getView().getModel("OSFARM");

            osfarm.setProperty("/name/editable", true);
            osfarm.setProperty("/name/value", "");
            osfarm.setProperty("/name/state", "None");
            osfarm.setProperty("/name/stateText", "");

            osfarm.setProperty("/code/editable", true);
            osfarm.setProperty("/code/value", "");
            osfarm.setProperty("/code/state", "None");
            osfarm.setProperty("/code/stateText", "");

            osfarm.setProperty("/farm_type/editable", true);
            osfarm.setProperty("/farm_type/value", "");
            osfarm.setProperty("/farm_type/state", "None");
            osfarm.setProperty("/farm_type/stateText", "");

            osfarm.setProperty("/disable/value", false);
        },
        /**
         * Habilita/deshabilita la edición de los datos de un registro MDSTAGE
         * @param  {Boolean} edit "true" si habilita la edición, "false" si la deshabilita
         */
        _editRecordValues: function(edit) {

            var osfarm = this.getView().getModel("OSFARM");
            osfarm.setProperty("/name/editable", edit);
            osfarm.setProperty("/code/editable", edit);
            osfarm.setProperty("/farm_type/editable", edit);
            osfarm.setProperty("/disable/editable", edit);
            //osfarm.setProperty("/incubator_plant/editable", edit);

        },
        _editRecordValues2: function(edit) {

            var osfarm = this.getView().getModel("OSFARM");
            // osfarm.setProperty("/name/editable", edit);
            // osfarm.setProperty("/code/editable", edit);
            osfarm.setProperty("/farm_type/editable", edit);
            osfarm.setProperty("/disable/editable", edit);
            //osfarm.setProperty("/incubator_plant/editable", edit);

        },
        /**
         * Se define un campo como obligatorio o no, de un registro MDSTAGE
         * @param  {Boolean} edit "true" si es campo obligatorio, "false" si no es obligatorio
         */
        _editRecordRequired: function(edit) {
            var osfarm = this.getView().getModel("OSFARM");
            osfarm.setProperty("/name/required", edit);
            osfarm.setProperty("/code/required", edit);

        },
        /**
         * Navega a la vista para crear un nuevo registro
         * @param  {Event} oEvent Evento que llamó esta función
         */
        onNewRecord: function(oEvent) {
            this.getRouter().navTo("osfarm_Create", {}, false /*create history*/ );
        },
        /**
         * Cancela la creación de un registro MDSTAGE, y regresa a la vista principal
         * @param  {Event} oEvent Evento que llamó esta función
         */
        onCancelCreate: function(oEvent) {
            this._resetRecordValues();
            this.onNavBack(oEvent);
        },
        /**
         * Regresa a la vista principal de la entidad seleccionada actualmente
         * @param  {Event} oEvent Evento que llamó esta función
         */
        onNavBack: function(oEvent) {
            /** @type {JSONModel} OS Referencia al modelo "OS" */
            var util = this.getView().getModel("util");

            this.getRouter().navTo(util.getProperty("/selectedEntity"), {}, false /*create history*/ );
        },
        /**
         * Solicita al servicio correspondiente crear un registro MDSTAGE
         * @param  {Event} oEvent Evento que llamó esta función
         */
        onCreate: function(oEvent) {
            //Si el registro que se desea crear es válido
            if (this._validRecord()) {

                var ospartnership = this.getView().getModel("OSPARTNERSHIP"),
                    osfarm = this.getView().getModel("OSFARM"),
                    util = this.getView().getModel("util"),
                    that = this,
                    serviceUrl= util.getProperty("/serviceUrl");

                $.ajax({
                    type: "POST",
                    contentType: "application/json",
                    data: JSON.stringify({
                        "partnership_id" :	ospartnership.getProperty(ospartnership.getProperty("/selectedRecordPath/") + "/partnership_id"),
                        "code": osfarm.getProperty("/code/value"),
                        "name": osfarm.getProperty("/name/value"),
                        "farm_type_id": osfarm.getProperty("/type_id/value"),
                        "os_disable": osfarm.getProperty("/disable/value")
                    }),
                    url: serviceUrl+"/farm/",
                    dataType: "json",
                    async: true,
                    success: function(data) {
                        util.setProperty("/busy/", false);
                        that._resetRecordValues();
                        that.onToast(that.getI18n().getText("OS.recordCreated"));
                        that.getRouter().navTo("osfarm", {}, true /*no history*/ );

                    },
                    error: function(error) {
                        that.onToast("Error: " + error.responseText);
                        console.log("Read failed ");
                    }
                });

            }
        },
        /**
         * Valida la correctitud de los datos existentes en el formulario del registro
         * @return {Boolean} Devuelve "true" si los datos son correctos, y "false" si son incorrectos
         */
        _validRecord: function() {
            /**
             * @type {JSONModel} MDSTAGE Referencia al modelo "MDSTAGE"
             * @type {Boolean} flag "true" si los datos son válidos, "false" si no lo son
             */
            var osfarm = this.getView().getModel("OSFARM"),
                flag = true,
                that = this,
                Without_SoL = /^\d+$/,
                Without_Num = /^[a-zA-Z\s]*$/;

            if (osfarm.getProperty("/name/value") === "") {
                flag = false;
                osfarm.setProperty("/name/state", "Error");
                osfarm.setProperty("/name/stateText", this.getI18n().getText("enter.FIELD"));
            } else {
                osfarm.setProperty("/name/state", "None");
                osfarm.setProperty("/name/stateText", "");
            }

            if (osfarm.getProperty("/code/value") === "") {
                flag = false;
                osfarm.setProperty("/code/state", "Error");
                osfarm.setProperty("/code/stateText", this.getI18n().getText("enter.FIELD"));
            } else {
                osfarm.setProperty("/code/state", "None");
                osfarm.setProperty("/code/stateText", "");
            }

            return flag;
        },
        _validRecord2: function() {
            /**
             * @type {JSONModel} MDSTAGE Referencia al modelo "MDSTAGE"
             * @type {Boolean} flag "true" si los datos son válidos, "false" si no lo son
             */
            var osfarm = this.getView().getModel("OSFARM"),
                flag = true,
                that = this,
                Without_SoL = /^\d+$/,
                Without_Num = /^[a-zA-Z\s]*$/;

            if (osfarm.getProperty("/name/value") === "") {
                flag = false;
                osfarm.setProperty("/name/state", "Error");
                osfarm.setProperty("/name/stateText", this.getI18n().getText("enter.FIELD"));
            } else {
                osfarm.setProperty("/name/state", "None");
                osfarm.setProperty("/name/stateText", "");
            }

            if (osfarm.getProperty("/code/value") === "") {
                flag = false;
                osfarm.setProperty("/code/state", "Error");
                osfarm.setProperty("/code/stateText", this.getI18n().getText("enter.FIELD"));
            } else {
                osfarm.setProperty("/code/state", "None");
                osfarm.setProperty("/code/stateText", "");
            }

            return flag;
        },
        /**
         * Coincidencia de ruta para acceder a los detalles de un registro
         * @param  {Event} oEvent Evento que llamó esta función
         */
        _onRecordMatched: function(oEvent) {

            this._viewOptions();

        },
        /**
         * Cambia las opciones de visualización disponibles en la vista de detalles de un registro
         */
        _viewOptions: function() {
            console.log("entro a la funcion");
            var osfarm = this.getView().getModel("OSFARM");
            osfarm.setProperty("/save/", false);
            osfarm.setProperty("/cancel/", false);
            osfarm.setProperty("/modify/", true);
            osfarm.setProperty("/delete/", true);
            this.getFarmType(false);
            this.getIncubatorPlant(false);
            this._editRecordValues(false);
            this._editRecordRequired(false);
        },
        /**
         * Ajusta la vista para editar los datos de un registro
         * @param  {Event} oEvent Evento que llamó esta función
         */
        onEdit: function(oEvent) {

            var osfarm = this.getView().getModel("OSFARM");
            osfarm.setProperty("/save/", true);
            osfarm.setProperty("/cancel/", true);
            osfarm.setProperty("/modify/", false);
            osfarm.setProperty("/delete/", false);
            // this._editRecordRequired(true);
            this._editRecordValues2(true);

        },

        /**
         * Cancela la edición de un registro MDSTAGE
         * @param  {Event} oEvent Evento que llamó esta función
         */
        onCancelEdit: function(oEvent) {
            /** @type {JSONModel} MDSTAGE  Referencia al modelo MDSTAGE */

            this.onView();
        },
        /**
         * Ajusta la vista para visualizar los datos de un registro
         */
        onView: function() {
            this._viewOptions();
        },
        /**
         * Solicita al servicio correspondiente actualizar un registro MDSTAGE
         * @param  {Event} oEvent Evento que llamó esta función
         */
        onUpdate: function(oEvent) {
            /**
             * Si el registro que se quiere actualizar es válido y hubo algún cambio
             * con respecto a sus datos originales
             */
            if (this._validRecord2() && this._recordChanged2()) {
                /**
                 * @type {JSONModel} MDSTAGE       Referencia al modelo "MDSTAGE"
                 * @type {JSONModel} util         Referencia al modelo "util"
                 * @type {Controller} that         Referencia a este controlador
                 */
                var osfarm = this.getView().getModel("OSFARM");
                var util = this.getView().getModel("util");
                var that = this;
                var serviceUrl= util.getProperty("/serviceUrl");

                $.ajax({
                    type: "PUT",
                    contentType: "application/json",
                    data: JSON.stringify({

                        "farm_id": osfarm.getProperty(osfarm.getProperty("/selectedRecordPath/") + "/farm_id"),
                        "name": osfarm.getProperty("/name/value"),
                        "code": osfarm.getProperty("/code/value"),
                        "farm_type_id": osfarm.getProperty("/type_id/value"),
                        "os_disable": osfarm.getProperty("/disable/value")


                    }),
                    url: serviceUrl+"/farm/",
                    dataType: "json",
                    async: true,
                    success: function(data) {

                        util.setProperty("/busy/", false);
                        that._resetRecordValues();
                        that._viewOptions();
                        that.onToast(that.getI18n().getText("OS.recordUpdated"));
                        that.getRouter().navTo("osfarm", {}, true /*no history*/ );

                    },
                    error: function(request, status, error) {
                        that.onToast("Error de comunicación");
                        console.log("Read failed");
                    }
                });
            }
        },

        getFarmType: function(isNew){
            const util = this.getModel("util");
            let osfarm = this.getModel("OSFARM");

            $.ajax({
                url: util.getProperty("/serviceUrl") + "/farm_type",
                method: "GET",
                async: true,
                dataType:"json",
                success: function(res) {
                    console.log("tipo de granja");
                    console.log(res);
                    osfarm.setProperty("/farm_type/record", res.data);
                    if(res.data.length>0 && isNew){
                        osfarm.setProperty("/type_id/value", res.data[0].farm_type_id);
                        console.log(osfarm);
                    }
                    if(res.data[0].farm_type_id == 1){
                        osfarm.setProperty("/incubator_plant/editable", true);
                    }else{
                        osfarm.setProperty("/incubator_plant/editable", false);
                    }

                },
                error: function(err) {    console.log("err: ", err);   }  });
    		},
        updateIncubatorPlantModel: function(){
            console.log("Entere");
            let incubator_plant_id = this.getView().byId("incubator_plant_select").getSelectedKey();
            console.log("incubator_plant_id: ", incubator_plant_id);
        },
        getIncubatorPlant: function(isNew){
            const util = this.getModel("util");
            let osfarm = this.getModel("OSFARM"),
                ospartnership = this.getModel("OSPARTNERSHIP"),
                partnership_id = ospartnership.getProperty(ospartnership.getProperty("/selectedRecordPath/") + "/partnership_id");
            $.ajax({
                url: util.getProperty("/serviceUrl") + "/incubator_plant/findIncPlantByPartnetship",
                method: "POST",
                async: true,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({
                    "partnership_id": partnership_id
                }),
                success: function(res) {
                    console.log("tipo de granja");
                    console.log(res);
                    osfarm.setProperty("/incubator_plant/record", res.data);
                    if(res.data.length>0 && isNew){
                        osfarm.setProperty("/incubator_plant/value", res.data[0].incubator_plant_id);
                        console.log(osfarm);
                    }

                },
                error: function(err) {
                    console.log("err: ", err);
                }
            });
        },

        /**
         * Verifica si el registro seleccionado tiene algún cambio con respecto a sus valores originales
         * @return {Boolean} Devuelve "true" el registro cambió, y "false" si no cambió
         */
        _recordChanged: function() {
            /**
             * @type {JSONModel} MDSTAGE         Referencia al modelo "MDSTAGE"
             * @type {Boolean} flag            "true" si el registro cambió, "false" si no cambió
             */
            var osfarm = this.getView().getModel("OSFARM"),
                flag = false;

            if (osfarm.getProperty("/name/value") !== osfarm.getProperty(osfarm.getProperty("/selectedRecordPath/") + "/name")) {
                flag = true;
            }

            if (osfarm.getProperty("/code/value") !== osfarm.getProperty(osfarm.getProperty("/selectedRecordPath/") + "/code")) {
                flag = true;
            }

            if (osfarm.getProperty("/farm_type/record") !== osfarm.getProperty(osfarm.getProperty("/selectedRecordPath/") + "/record")) {
                flag = true;
            }

            if(!flag) this.onToast("No se detectaron cambios");

            return flag;
        },
        _recordChanged2: function() {
            /**
             * @type {JSONModel} MDSTAGE         Referencia al modelo "MDSTAGE"
             * @type {Boolean} flag            "true" si el registro cambió, "false" si no cambió
             */
            var osfarm = this.getView().getModel("OSFARM"),
                flag = false;

            // if (osfarm.getProperty("/name/value") !== osfarm.getProperty(osfarm.getProperty("/selectedRecordPath/") + "/name")) {
            //     flag = true;
            // }

            // if (osfarm.getProperty("/code/value") !== osfarm.getProperty(osfarm.getProperty("/selectedRecordPath/") + "/code")) {
            //     flag = true;
            // }

            if (osfarm.getProperty("/farm_type/record") !== osfarm.getProperty(osfarm.getProperty("/selectedRecordPath/") + "/record")) {
                flag = true;
            }

            if(!flag) this.onToast("No se detectaron cambios");

            return flag;
        },
        onPartnershipSearch: function(oEvent){
            var aFilters = [],
                sQuery = oEvent.getSource().getValue(),
                binding = this.getView().byId("partnershipTable").getBinding("items");

            if (sQuery && sQuery.length > 0) {
                /** @type {Object} filter1 Primer filtro de la búsqueda */
                var filter1 = new sap.ui.model.Filter("name", sap.ui.model.FilterOperator.Contains, sQuery);
                aFilters = new sap.ui.model.Filter([filter1]);
            }

            //se actualiza el binding de la lista
            binding.filter(aFilters);

        },
        onFarmSearch: function(oEvent){
            var aFilters = [],
                sQuery = oEvent.getSource().getValue(),
                binding = this.getView().byId("farmTable").getBinding("items");
            if (sQuery && sQuery.length > 0) {
                /** @type {Object} filter1 Primer filtro de la búsqueda */
                var filter1 = new sap.ui.model.Filter("name", sap.ui.model.FilterOperator.Contains, sQuery);
                aFilters = new sap.ui.model.Filter([filter1]);
            }

            //se actualiza el binding de la lista
            binding.filter(aFilters);

        },
        onVerifyIsUsed: async function(farm_id){
            let ret;

            const response = await fetch("/farm/isBeingUsed", {
                headers: {
                    "Content-Type": "application/json"
                },
                method: "POST",
                body: JSON.stringify({
                    farm_id: farm_id
                })
            });

            if (response.status !== 200 && response.status !== 409) {
                console.log("Looks like there was a problem. Status Code: " +
                    response.status);
                return;
            }
            if(response.status === 200){
                const res = await response.json();

                ret = res.data.used;
            }
            
            console.log(ret);
            return ret;
            
        },
        onConfirmDelete: async function(oEvent){

            var oBundle = this.getView().getModel("i18n").getResourceBundle();
            var deleteRecord = oBundle.getText("deleteRecord");
            var confirmation = oBundle.getText("confirmation");
            var util = this.getView().getModel("util");
            let osfarm = this.getView().getModel("OSFARM"),
                farm_id = osfarm.getProperty(osfarm.getProperty("/selectedRecordPath/") + "/farm_id"),
                that = this;


            let cond = await this.onVerifyIsUsed(farm_id);
            if(cond){
                var dialog = new Dialog({
                    title: "Información",
                    type: "Message",
                    state: "Warning",
                    content: new Text({
                        text: "No se puede eliminar la Granja, porque está siendo utilizada."
                    }),
                    beginButton: new Button({
                        text: "OK",
                        press: function() {
                            dialog.close();
                        }
                    }),
                    afterClose: function() {
                        dialog.destroy();
                    }
                });
        
                dialog.open();
            }else{
                var dialog = new Dialog({
                    title: confirmation,
                    type: "Message",
                    content: new sap.m.Text({
                        text: deleteRecord
                    }),

                    beginButton: new Button({
                        text: "Si",
                        press: function() {
                            util.setProperty("/busy/", true);
                            var serviceUrl= util.getProperty("/serviceUrl");
                            $.ajax({
                                type: "DELETE",
                                contentType: "application/json",
                                data: JSON.stringify({
                                    "farm_id": farm_id
                                }),
                                url: serviceUrl+"/farm/",
                                dataType: "json",
                                async: true,
                                success: function(data) {

                                    util.setProperty("/busy/", false);
                                    that.getRouter().navTo("osfarm", {}, true);
                                    dialog.close();
                                    dialog.destroy();

                                },
                                error: function(request, status, error) {
                                    that.onToast("Error de comunicación");
                                    console.log("Read failed");
                                }
                            });

                        }
                    }),
                    endButton: new Button({
                        text: "No",
                        press: function() {
                            dialog.close();
                            dialog.destroy();
                        }
                    })
                });
                dialog.open();
            }    
            

        },
        onSelectPartnershipRecord: function(oEvent){

            var that = this,
                util = this.getView().getModel("util"),
                ospartnership = this.getView().getModel("OSPARTNERSHIP"),
                osfarm = this.getView().getModel("OSFARM");

            //guarda la ruta del registro PARTNERSHIP que fue seleccionado
            ospartnership.setProperty("/selectedRecordPath/", oEvent.getSource()["_aSelectedPaths"][0]);
            ospartnership.setProperty("/selectedRecord/", ospartnership.getProperty(ospartnership.getProperty("/selectedRecordPath/")));

            //habilita el tab de la tabla de registros BROILERSFARM
            osfarm.setProperty("/settings/enabledTab", true);

            //habilita la opción de crear un registro BROILERSFARM
            osfarm.setProperty("/new", true);

            //establece el tab de la tabla BROILERSFARM como el tab seleccionado
            this.getView().byId("tabBar").setSelectedKey(this.getView().getId() + "--" + "farmFilter");

            //obtiene los registros de BROILERSFARM
            this.onRead(that, util, ospartnership, osfarm);

        },
        updateFarmTypeModel: function(){
            console.log("type");
            let osfarm = this.getView().getModel("OSFARM"),
                farm_type_model = this.getView().byId("farm_type_model").getSelectedKey();

            osfarm.setProperty("/type_id/value", farm_type_model);

            if(farm_type_model==1){
                osfarm.setProperty("/incubator_plant/editable", true);
            }else{
                osfarm.setProperty("/incubator_plant/editable", false);
            }


        },
        validateIntInput: function (o) {
            let input= o.getSource();
            let length = 10;
            let value = input.getValue();
            let regex = new RegExp(`/^[0-9]{1,${length}}$/`);
  
            if (regex.test(value)) {
                return true;
            }
            else {
                let aux = value
                    .split("")
                    .filter(char => {
                        if (/^[0-9]$/.test(char)) {
                            if (char !== ".") {
                                return true;
                            }
                        }
                    })
                    .join("");
                value = aux.substring(0, length);
                input.setValue(value);
                return false;
            }
        },
    });
});
