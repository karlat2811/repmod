sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "technicalConfiguration/model/formatter"
], function(Controller,formatter ) {
    "use strict";
    var  confirmDeleteDlg, showWarehousesDlg, selectWarehousesDlg, selectShedsDlg, showShedsDlg;
    return Controller.extend("technicalConfiguration.controller.BaseController", {
        formatter: formatter,
        _setFragments: function() {
            //asigna el fragmento de confirmar eliminación a la vista y lo agrega al ciclo de vida de su modelo
            this.confirmDeleteDlg = sap.ui.xmlfragment("technicalConfiguration.view.DeleteConfirmationDialog", this);
            confirmDeleteDlg = this.confirmDeleteDlg;
            this.getView().addDependent(confirmDeleteDlg);

            showWarehousesDlg = sap.ui.xmlfragment("technicalConfiguration.view.center.oswarehouses-ShowDialog", this);
            this.getView().addDependent(showWarehousesDlg);

            selectWarehousesDlg = sap.ui.xmlfragment("technicalConfiguration.view.center.oswarehouses-SelectDialog", this);
            this.getView().addDependent(selectWarehousesDlg);

            selectShedsDlg = sap.ui.xmlfragment("technicalConfiguration.view.silo.ossheds-SelectDialog", this);
            this.getView().addDependent(selectShedsDlg);

            showShedsDlg = sap.ui.xmlfragment("technicalConfiguration.view.silo.ossheds-ShowDialog", this);
            this.getView().addDependent(showShedsDlg);

        },

        getI18n: function() {
            return this.getOwnerComponent().getModel("i18n").getResourceBundle();
        },
        getResourceBundle : function () {
            return this.getOwnerComponent().getModel("i18n").getResourceBundle();
        },
        getRouter: function() {
            return sap.ui.core.UIComponent.getRouterFor(this);
        },

        getModel : function (sName) {
            return this.getView().getModel(sName);
        },

        setModel : function (oModel, sName) {
            return this.getView().setModel(oModel, sName);
        },

        onNavMaster: function(oEvent) {
            this.getRouter().navTo("master", {}, true /*no history*/ );
        },

        onToast: function(message, f) {
            sap.m.MessageToast.show(message, {
                width: "22em",
                closeOnBrowserNavigation: false,
                onClose: f
            });
        },

        sendRequest : function (url, method, data, successFunc, srvErrorFunc, connErrorFunc) {
            const util = this.getModel("util");
            const $settings = {
                url: url,
                method: method,
                data: JSON.stringify(data),
                contentType: "application/json",
                error: err => {
                // console.log("error", err);
                    util.setProperty("/connectionError/status", err.status);
                    util.setProperty("/connectionError/message", err.statusText);
                    this.onConnectionError();
                    if(connErrorFunc) connErrorFunc(err);

                },
                success: res => {
                // console.log("respuesta", res);
                    if(res.statusCode !== 200) {
                        util.setProperty("/serviceError/status", res.statusCode);
                        util.setProperty("/serviceError/message", res.msg);
                        this.onServiceError();
                        if(srvErrorFunc) srvErrorFunc(res);
                    } else {
                        successFunc(res);
                    }
                }
            };
            console.log("datos", data);
            $.ajax($settings);
        },
        /**
     * Visualiza los detalles de un registro ospartnership
     * @param  {Event} oEvent Evento que llamó esta función
     */
        onViewSlaughterhouseRecord: function(oEvent) {

            var osslaughterhouse = this.getView().getModel("OSSLAUGHTERHOUSE");
            osslaughterhouse.setProperty("/save/", false);
            osslaughterhouse.setProperty("/cancel/", false);
            osslaughterhouse.setProperty("/selectedRecordPath/", oEvent.getSource().getBindingContext("OSSLAUGHTERHOUSE"));
            osslaughterhouse.setProperty("/slaughterhouse_id/value", oEvent.getSource().getBindingContext("OSSLAUGHTERHOUSE").getObject().slaughterhouse_id);
            osslaughterhouse.setProperty("/name/value", oEvent.getSource().getBindingContext("OSSLAUGHTERHOUSE").getObject().name);
            osslaughterhouse.setProperty("/code/value", oEvent.getSource().getBindingContext("OSSLAUGHTERHOUSE").getObject().code);
            osslaughterhouse.setProperty("/description/value", oEvent.getSource().getBindingContext("OSSLAUGHTERHOUSE").getObject().description);
            osslaughterhouse.setProperty("/address/value", oEvent.getSource().getBindingContext("OSSLAUGHTERHOUSE").getObject().address);
            osslaughterhouse.setProperty("/capacity/value", oEvent.getSource().getBindingContext("OSSLAUGHTERHOUSE").getObject().capacity);

            this.getRouter().navTo("osslaughterhouse_Record", {}, false /*create history*/ );
        },
        onViewPartnershipRecord: function(oEvent) {

            var ospartnership = this.getView().getModel("OSPARTNERSHIP");
            ospartnership.setProperty("/save/", false);
            ospartnership.setProperty("/cancel/", false);
            ospartnership.setProperty("/selectedRecordPath/", oEvent.getSource().getBindingContext("OSPARTNERSHIP"));
            ospartnership.setProperty("/partnership_id/value", oEvent.getSource().getBindingContext("OSPARTNERSHIP").getObject().partnership_id);
            ospartnership.setProperty("/name/value", oEvent.getSource().getBindingContext("OSPARTNERSHIP").getObject().name);
            ospartnership.setProperty("/disable/value", oEvent.getSource().getBindingContext("OSPARTNERSHIP").getObject().os_disable);
            ospartnership.setProperty("/code/value", oEvent.getSource().getBindingContext("OSPARTNERSHIP").getObject().code);
            ospartnership.setProperty("/description/value", oEvent.getSource().getBindingContext("OSPARTNERSHIP").getObject().description);
            ospartnership.setProperty("/address/value", oEvent.getSource().getBindingContext("OSPARTNERSHIP").getObject().address);

            this.getRouter().navTo("ospartnership_Record", {}, false /*create history*/ );
        },
        /**
     * Visualiza los detalles de un registro MDSTAGE
     * @param  {Event} oEvent Evento que llamó esta función
     */
        onViewFarmRecord: function(oEvent) {

            /** @type {JSONModel} FARM Referencia al modelo "FARM" */
            var osfarm = this.getView().getModel("OSFARM");
            var record = JSON.parse(JSON.stringify(oEvent.getSource().getBindingContext("OSFARM").getObject()));
            console.log(record);
            osfarm.setProperty("/selectedRecordPath/", oEvent.getSource().getBindingContext("OSFARM")["sPath"]);
            osfarm.setProperty("/name/value", record.name);
            osfarm.setProperty("/disable/value", record.os_disable);
            osfarm.setProperty("/code/value", record.code);
            console.log(record.farm_type_id);
            osfarm.setProperty("/type_id/value", record.farm_type_id);
            console.log(osfarm);
            this.getRouter().navTo("osfarm_Record", {}, true);
        },
        /**
     * Visualiza los detalles de un registro CENTER
     * @param  {Event} oEvent Evento que llamó esta función
     */
        onViewCenterRecord: function(oEvent) {
            /** @type {JSONModel} OSCENTER Referencia al modelo "CENTER" */
            var oscenter = this.getView().getModel("OSCENTER");
            var record = JSON.parse(JSON.stringify(oEvent.getSource().getBindingContext("OSCENTER").getObject()));

            oscenter.setProperty("/selectedRecordPath/", oEvent.getSource().getBindingContext("OSCENTER")["sPath"]);
            oscenter.setProperty("/code/value", record.code);
            oscenter.setProperty("/disable/value", record.os_disable);
            oscenter.setProperty("/name/value", record.name);

            this.getRouter().navTo("oscenter_Record", {}, true);
        },
        /**
     * Visualiza los detalles de un registro CENTER
     * @param  {Event} oEvent Evento que llamó esta función
     */
        onViewWarehouseRecord: function(oEvent) {
            /** @type {JSONModel} WAREHOUSE Referencia al modelo "WAREHOUSE" */
            var oswarehouse = this.getView().getModel("OSWAREHOUSE");
            var record = JSON.parse(JSON.stringify(oEvent.getSource().getBindingContext("OSWAREHOUSE").getObject()));

            oswarehouse.setProperty("/selectedRecordPath/", oEvent.getSource().getBindingContext("OSWAREHOUSE")["sPath"]);
            oswarehouse.setProperty("/code/value", record.code);
            oswarehouse.setProperty("/name/value", record.name);

            this.getRouter().navTo("oswarehouse_Record", {}, true);
        },

        /**
     * Abre el diálogo para confirmar la eliminación de un registro
     * @param  {Event} oEvent Evento que llamó esta función
     */
        onConfirmDelete: function(oEvent) {

            var util = this.getView().getModel("util");

            switch (util.getProperty("/selectedEntity/")) {

            case "PARTNERSHIP":
                dummy.setProperty("/delete/message", "Las estructuras asociadas a esta empresa serán eliminadas.");
                break;

            case "BROILERSFARM":
                dummy.setProperty("/delete/message", "Las estructuras asociadas a esta granja serán eliminadas.");
                break;

            case "oscenter":
                util.setProperty("/delete/message", "Las estructuras asociadas a este núcleo serán eliminadas.");
                break;

            case "oswarehouse":
                util.setProperty("/delete/message", "¿Seguro que desea eliminar este almacén?");
                break;

            case "ossilo":
                util.setProperty("/delete/message", "¿Seguro que desea eliminar este silo?");
                break;

            case "osshed":
                util.setProperty("/delete/message", "¿Seguro que desea eliminar este galpón?");
                break;
            }

            confirmDeleteDlg.open();
        },
        /**
     * Llama a la función para eliminar un registro, de acuerdo a la entidad seleccionada
     * @param  {Event} oEvent Evento que llamó esta función
     */
        onDelete: function(oEvent) {
            /**
       * @type {Controller} that          Referencia a este controlador
       * @type {JSONModel} dummy          Referencia al modelo "dummy"
       * @type {JSONModel} PARTNERSHIP  Referencia al modelo "PARTNERSHIP"
       * @type {JSONModel} BROILERSFARM Referencia al modelo "BROILERSFARM"
       * @type {JSONModel} CENTER Referencia al modelo "CENTER"
       */
            var that = this,
                util = this.getView().getModel("util"),
                ospartnership = this.getView().getModel("OSPARTNERSHIP"),
                osfarm = this.getView().getModel("OSFARM"),
                oscenter = this.getView().getModel("OSCENTER"),
                oswarehouse = this.getView().getModel("OSWAREHOUSE"),
                ossilo = this.getView().getModel("OSSILO"),
                osshed = this.getView().getModel("OSSHED");

            /**
       * Determina la entidad que está seleccionada actualmente, y llama a su función
       * de eliminación de registro correspondiente
       */
            switch (util.getProperty("/selectedEntity/")) {

            case "PARTNERSHIP":
                sap.ui.controller("MDSFConfiguracion.controller.PARTNERSHIP").onDelete(that, dummy, PARTNERSHIP);
                break;

            case "BROILERSFARM":
                sap.ui.controller("MDSFConfiguracion.controller.BROILERSFARM").onDelete(that, dummy, BROILERSFARM);
                break;

            case "oscenter":
                sap.ui.controller("technicalConfiguration.controller.oscenter").onDelete(that, util, oscenter);
                break;

            case "oswarehouse":
                sap.ui.controller("technicalConfiguration.controller.oswarehouse").onDelete(that, util, oswarehouse);
                break;

            case "ossilo":
                sap.ui.controller("technicalConfiguration.controller.ossilo").onDelete(that, util, ossilo);
                break;

            case "osshed":
                sap.ui.controller("technicalConfiguration.controller.osshed").onDelete(that, util, osshed);
                break;
            }
        },
        /**
     * Cierra el diálogo para confirmar la eliminación de un registro
     * @param  {Event} oEvent Evento que llamó esta función
     */
        onCancelDelete: function(oEvent) {
            confirmDeleteDlg.close();
        },
        /**
		 * Visualiza los detalles de un registro SHED
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        onViewShedRecord: function(oEvent) {
            /** @type {JSONModel} SHED Referencia al modelo "SHED" */
            var osshed = this.getView().getModel("OSSHED");
            var mdbreed = this.getView().getModel("MDBREED");
            console.log(oEvent.getSource().getBindingContext("OSSHED").getObject());
            osshed.setProperty("/selectedRecordPath/", oEvent.getSource().getBindingContext("OSSHED")["sPath"]);
            osshed.setProperty("/name/value", oEvent.getSource().getBindingContext("OSSHED").getObject().code);
            osshed.setProperty("/stall_height/value", parseFloat(oEvent.getSource().getBindingContext("OSSHED").getObject().stall_height));
            osshed.setProperty("/stall_width/value", parseFloat(oEvent.getSource().getBindingContext("OSSHED").getObject().stall_width));
            osshed.setProperty("/status/value", oEvent.getSource().getBindingContext("OSSHED").getObject().statusshed_id);
            osshed.setProperty("/environment/value", oEvent.getSource().getBindingContext("OSSHED").getObject().environment_id);
            osshed.setProperty("/disable/value", oEvent.getSource().getBindingContext("OSSHED").getObject().os_disable);
            mdbreed.setProperty("/selectedKey", oEvent.getSource().getBindingContext("OSSHED").getObject().breed_id);
            osshed.setProperty("/capmin/value", oEvent.getSource().getBindingContext("OSSHED").getObject().capmin);
            osshed.setProperty("/capmax/value", oEvent.getSource().getBindingContext("OSSHED").getObject().capmax);
            osshed.setProperty("/rotation_days/value", oEvent.getSource().getBindingContext("OSSHED").getObject().rotation_days);
            this.getRouter().navTo("osshed_Record", {}, true);
            console.log(mdbreed);
        },
        /**
     * Visualiza los detalles de un registro SILO
     * @param  {Event} oEvent Evento que llamó esta función
     */
        onViewSiloRecord: function(oEvent) {
            /** @type {JSONModel} SILO Referencia al modelo "SILO" */
            var ossilo = this.getView().getModel("OSSILO");
            var record = JSON.parse(JSON.stringify(oEvent.getSource().getBindingContext("OSSILO").getObject()));

            ossilo.setProperty("/selectedRecordPath/", oEvent.getSource().getBindingContext("OSSILO")["sPath"]);
            ossilo.setProperty("/silo_number/value", record.silo_number);
            ossilo.setProperty("/height/value", parseFloat(record.height));
            ossilo.setProperty("/diameter/value", parseFloat(record.diameter));
            ossilo.setProperty("/total_capacity_1/value", parseFloat(record.total_capacity_1));
            ossilo.setProperty("/total_rings_quantity/value", parseFloat(record.total_rings_quantity));
            ossilo.setProperty("/rings_height/value", parseFloat(record.rings_height));
            ossilo.setProperty("/cone_degrees/value", parseFloat(record.cone_degrees));

            this.getRouter().navTo("ossilo_Record", {}, true);
        },
        /**
     * Visualiza los detalles de un registro MDSTAGE
     * @param  {Event} oEvent Evento que llamó esta función
     */
        onViewIncubatorPlantRecord: function(oEvent) {

            /** @type {JSONModel} incubatorPlant Referencia al modelo "incubatorPlant" */
            var osincubatorplant = this.getView().getModel("OSINCUBATORPLANT");
            var record = JSON.parse(JSON.stringify(oEvent.getSource().getBindingContext("OSINCUBATORPLANT").getObject()));
            console.log(record);
            osincubatorplant.setProperty("/selectedRecordPath/", oEvent.getSource().getBindingContext("OSINCUBATORPLANT")["sPath"]);
            osincubatorplant.setProperty("/name/value", record.name);
            osincubatorplant.setProperty("/code/value", record.code);
            osincubatorplant.setProperty("/description/value", record.description);
            osincubatorplant.setProperty("/max_storage/value", record.max_storage);
            osincubatorplant.setProperty("/min_storage/value", record.min_storage);
    
            osincubatorplant.setProperty("/acclimatized/value", record.acclimatized);
            osincubatorplant.setProperty("/suitable/value", record.suitable);
            osincubatorplant.setProperty("/expired/value", record.expired);

            this.getRouter().navTo("osIncubatorPlant_Record", {}, true);
        },
        onViewIncubatorRecord: function(oEvent) {

            /** @type {JSONModel} incubatorPlant Referencia al modelo "incubatorPlant" */
            var osincubator = this.getView().getModel("OSINCUBATOR");
            var record = JSON.parse(JSON.stringify(oEvent.getSource().getBindingContext("OSINCUBATOR").getObject()));
            console.log(record);
            osincubator.setProperty("/selectedRecordPath/", oEvent.getSource().getBindingContext("OSINCUBATOR")["sPath"]);
            osincubator.setProperty("/name/value", record.name);
            osincubator.setProperty("/code/value", record.code);
            osincubator.setProperty("/description/value", record.description);
            osincubator.setProperty("/capacity/value", record.capacity);
            osincubator.setProperty("/sunday/value", record.sunday);
            osincubator.setProperty("/monday/value", record.monday);
            osincubator.setProperty("/tuesday/value", record.tuesday);
            osincubator.setProperty("/wednesday/value", record.wednesday);
            osincubator.setProperty("/thursday/value", record.thursday);
            osincubator.setProperty("/friday/value", record.friday);
            osincubator.setProperty("/saturday/value", record.saturday);
            osincubator.setProperty("/available/value", record.available);
            this.getRouter().navTo("osIncubator_Record", {}, true);
        },
        onViewBroilerProductRecord: function(oEvent){
            /** @type {JSONModel} incubatorPlant Referencia al modelo "MDBROILERPRODUCT" */
            var mdbroilerProduct = this.getView().getModel("MDBROILERPRODUCT");
            var record = JSON.parse(JSON.stringify(oEvent.getSource().getBindingContext("MDBROILERPRODUCT").getObject()));
            console.log(record);

            // console.log("SE PRECARGA LA VISTA:::");
            console.log(record);

            mdbroilerProduct.setProperty("/selectedRecordPath/", oEvent.getSource().getBindingContext("MDBROILERPRODUCT")["sPath"]);
            mdbroilerProduct.setProperty("/selectedRecord", record);
            mdbroilerProduct.setProperty("/name/value", record.name);
            mdbroilerProduct.setProperty("/days_eviction/value", record.days_eviction);
            // this.getView().byId("broilerProduct_days_input").setSelectedItemId(mdbroilerProduct.getProperty("/days_eviction/value"));

            mdbroilerProduct.setProperty("/weight/value", record.weight);
            mdbroilerProduct.setProperty("/code/value", record.code);
            this.getRouter().navTo("mdbroilerProduct_Record", {}, true);
        },
        _openShowWarehouses: function() {
            showWarehousesDlg.open();
        },
        _closeShowWarehouses: function() {
            showWarehousesDlg.close();
        },
        _openSelectWarehouses: function() {
            selectWarehousesDlg.open();
        },
        _closeSelectWarehouses: function() {
            selectWarehousesDlg.close();
        },
        _openShowSheds: function() {
            showShedsDlg.open();
        },
        _closeShowSheds: function() {
            showShedsDlg.close();
        },
        _openSelectSheds: function() {
            selectShedsDlg.open();
        },
        _closeSelectSheds: function() {
            selectShedsDlg.close();
        }

    });
});
