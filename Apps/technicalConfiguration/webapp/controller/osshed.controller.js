sap.ui.define([
    "technicalConfiguration/controller/BaseController",
    "technicalConfiguration/model/formatter",
	
], function(BaseController, formatter, Dialog) {
    "use strict";

    return BaseController.extend("technicalConfiguration.controller.osshed", {
        formatter: formatter,
        onInit: function() {
            //ruta para la vista principal de galpones
            this.getOwnerComponent().getRouter().getRoute("osshed").attachPatternMatched(this._onRouteMatched, this);
            //ruta para los detalles de un galpón
            this.getOwnerComponent().getRouter().getRoute("osshed_Create").attachPatternMatched(this._onCreateMatched, this);
            //ruta para los detalles de un galpón
            this.getOwnerComponent().getRouter().getRoute("osshed_Record").attachPatternMatched(this._onRecordMatched, this);
        },

        /**
		 * Coincidencia de ruta para acceder a la vista principal
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        _onRouteMatched: function(oEvent) {
            /**
			 * @type {Controller} that          Referencia a este controlador
			 * @type {JSONModel} dummy          Referencia al modelo "dummy"
			 * @type {JSONModel} OS             Referencia al modelo "OS"
			 * @type {JSONModel} PARTNERSHIP  Referencia al modelo "PARTNERSHIP"
			 * @type {JSONModel} BROILERSFARM Referencia al modelo "BROILERSFARM"
			 * @type {JSONModel} CENTER       Referencia al modelo "CENTER"
			 */

            var that = this,
                util = this.getView().getModel("util"),
                ospartnership = this.getView().getModel("OSPARTNERSHIP"),
                osfarm = this.getView().getModel("OSFARM"),
                oscenter = this.getView().getModel("OSCENTER"),
                osshed = this.getView().getModel("OSSHED");

            //dependiendo del dispositivo, establece la propiedad "phone"
            util.setProperty("/phone/",
                this.getOwnerComponent().getContentDensityClass() === "sapUiSizeCozy");

            ospartnership.setProperty("/settings/tableMode", "SingleSelect");
            osfarm.setProperty("/settings/tableMode", "SingleSelect");
            oscenter.setProperty("/settings/tableMode", "SingleSelect");

            //si la estructura seleccionada antes de acceder a esta vista es diferente a galpón
            if (util.getProperty("/selectedEntity") !== "osshed") {

                //establece galpón como la estructura seleccionada
                util.setProperty("/selectedEntity", "osshed");

                //establece el tab de la tabla PARTNERSHIP como el tab seleccionado
                this.getView().byId("tabBar").setSelectedKey(this.getView().getId() + "--" + "partnershipFilter");

                //borra cualquier selección que se haya hecho en la tabla PARTNERSHIP
                this.getView().byId("partnershipTable").removeSelections(true);

                //borra cualquier selección que se haya hecho en la tabla BROILERSFARM
                this.getView().byId("farmTable").removeSelections(true);

                //borra cualquier selección que se haya hecho en la tabla CENTER
                this.getView().byId("centerTable").removeSelections(true);

                //borra cualquier selección que se haya hecho en la tabla silo
                this.getView().byId("shedTable").removeSelections(true);

                //establece que no hay ningún registro PARTNERSHIP seleccionado
                ospartnership.setProperty("/selectedRecordPath/", "");

                //establece que no hay ningún registro BROILERSFARM seleccionado
                osfarm.setProperty("/selectedRecordPath/", "");

                osfarm.setProperty("/records/", []);

                oscenter.setProperty("/records/", []);

                osshed.setProperty("/records/", []);

                //establece que no hay ningún registro BROILERSFARM seleccionado
                oscenter.setProperty("/selectedRecordPath/", "");

                //deshabilita el tab de la tabla de registros BROILERSFARM
                osfarm.setProperty("/settings/enabledTab", false);

                //deshabilita el tab de la tabla de registros CENTER
                oscenter.setProperty("/settings/enabledTab", false);

                //deshabilita el tab de la tabla de silos
                osshed.setProperty("/settings/enabledTab", false);

                //deshabilita la opción de crear un galpón
                osshed.setProperty("/new", false);

                //obtiene las sociedades financieras
                sap.ui.controller("technicalConfiguration.controller.ospartnership").onRead(that, util, ospartnership);

            } else if (ospartnership.getProperty("/selectedRecordPath/") !== "" &&
				osfarm.getProperty("/selectedRecordPath/") !== "" &&
				oscenter.getProperty("/selectedRecordPath/") !== "") {

                //habilita el tab de la tabla de granjas
                osfarm.setProperty("/settings/enabledTab", true);

                //habilita el tab de la tabla de núcleos
                oscenter.setProperty("/settings/enabledTab", true);

                //habilita el tab de la tabla de galpones
                osshed.setProperty("/settings/enabledTab", true);

                //habilita la opción de crear un galpón
                osshed.setProperty("/new", true);

                //obtiene los galpones
                this.onRead(that, util, ospartnership, osfarm, oscenter, osshed);
            }
        },
        onLoadSheds: function(){
            let osshed = this.getModel("OSSHED"),
                util = this.getModel("util"),
                serviceUrl = util.getProperty("/serviceUrl")+"/shed_status/";
            console.log(osshed);
            fetch(serviceUrl)
                .then(
                    function(response) {
                        if (response.status !== 200) {
                            console.log("Looks like there was a problem. Status Code: " +
								response.status);
                            return;
                        }

                        // Examine the text in the response
                        response.json().then(function(res) {
                            console.log(res.data);
                            util.setProperty("/busy/", false);
                            console.log(osshed);
                            osshed.setProperty("/statusRecords", res.data);
                        });
                    }
                )
                .catch(function(err) {
                    console.log("Fetch Error: ", err);
                });

        },
        onShedSearch: function(oEvent) {
            var aFilters = [],
                sQuery = oEvent.getSource().getValue(),
                binding = this.getView().byId("shedTable").getBinding("items");

            if (sQuery && sQuery.length > 0) {
                /** @type {Object} filter1 Primer filtro de la búsqueda */
                var filter1 = new sap.ui.model.Filter("code", sap.ui.model.FilterOperator.Contains, sQuery);
                aFilters = new sap.ui.model.Filter([filter1]);
            }

            //se actualiza el binding de la lista
            binding.filter(aFilters);

        },

        /**
		 * Regresa a la vista principal de la entidad seleccionada actualmente
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        onNavBack: function(oEvent) {
            /** @type {JSONModel} OS Referencia al modelo "OS" */
            var util = this.getView().getModel("util");

            this._resetRecordValues();
            this.getRouter().navTo(util.getProperty("/selectedEntity"), {}, true);
        },

        /**
		 * Selecciona un registro PARTNERSHIP y habilita la tabla de registros BROILERSFARM
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        onSelectPartnershipRecord: function(oEvent) {
            /**
			 * @type {Controller} that          Referencia a este controlador
			 * @type {JSONModel} dummy          Referencia al modelo "dummy"
			 * @type {JSONModel} PARTNERSHIP  Referencia al modelo "PARTNERSHIP"
			 * @type {JSONModel} BROILERSFARM Referencia al modelo "BROILERSFARM"
			 * @type {JSONModel} CENTER       Referencia al modelo "CENTER"
			 */
            var that = this,
                util = this.getView().getModel("util"),
                ospartnership = this.getView().getModel("OSPARTNERSHIP"),
                osfarm = this.getView().getModel("OSFARM"),
                oscenter = this.getView().getModel("OSCENTER"),
                osshed = this.getView().getModel("OSSHED");

            //guarda la ruta del registro PARTNERSHIP que fue seleccionado
            ospartnership.setProperty("/selectedRecordPath/", oEvent.getSource()["_aSelectedPaths"][0]);
            ospartnership.setProperty("/selectedRecord/", ospartnership.getProperty(ospartnership.getProperty("/selectedRecordPath/")));

            osfarm.setProperty("/selectedRecordPath/", "");
            osfarm.setProperty("/selectedRecord/", {});

            oscenter.setProperty("/selectedRecordPath/", "");
            oscenter.setProperty("/selectedRecord/", {});

            //habilita el tab de la tabla de granjas
            osfarm.setProperty("/settings/enabledTab", true);

            //deshabilita el tab de la tabla de núcleos
            oscenter.setProperty("/settings/enabledTab", false);

            //deshabilita el tab de la tabla de galpones
            osshed.setProperty("/settings/enabledTab", false);

            //deshabilita la opción de crear un galpón
            osshed.setProperty("/new", false);

            //establece el tab de la tabla de granjas como el tab seleccionado
            this.getView().byId("tabBar").setSelectedKey(this.getView().getId() + "--" + "farmFilter");

            //borra cualquier selección que se haya hecho en la tabla de granjas
            this.getView().byId("farmTable").removeSelections(true);

            //obtiene las granjas
            sap.ui.controller("technicalConfiguration.controller.osfarm").onRead(that, util, ospartnership, osfarm);
        },

        /**
		 * Selecciona un registro BROILERSFARM y habilita la tabla de registros CENTER
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        onSelectFarmRecord: function(oEvent) {
            /**
			 * @type {Controller} that          Referencia a este controlador
			 * @type {JSONModel} dummy          Referencia al modelo "dummy"
			 * @type {JSONModel} PARTNERSHIP  Referencia al modelo "PARTNERSHIP"
			 * @type {JSONModel} BROILERSFARM Referencia al modelo "BROILERSFARM"
			 * @type {JSONModel} CENTER       Referencia al modelo "CENTER"
			 */
            var that = this,
                util = this.getView().getModel("util"),
                ospartnership = this.getView().getModel("OSPARTNERSHIP"),
                osfarm = this.getView().getModel("OSFARM"),
                oscenter = this.getView().getModel("OSCENTER"),
                osshed = this.getView().getModel("OSSHED");

            //guarda la ruta del registro BROILERSFARM que fue seleccionado
            osfarm.setProperty("/selectedRecordPath/", oEvent.getSource()["_aSelectedPaths"][0]);
            osfarm.setProperty("/selectedRecord/", osfarm.getProperty(osfarm.getProperty("/selectedRecordPath/")));

            oscenter.setProperty("/selectedRecordPath/", "");
            oscenter.setProperty("/selectedRecord/", {});

            //habilita el tab de la tabla de registros CENTER
            oscenter.setProperty("/settings/enabledTab", true);

            //deshabilita el tab de la tabla de galpones
            osshed.setProperty("/settings/enabledTab", false);

            //deshabilita la opción de crear un galpón
            osshed.setProperty("/new", false);

            //establece el tab de la tabla CENTER como el tab seleccionado
            this.getView().byId(this.getView().getId() + "--" + "tabBar").setSelectedKey(this.getView().getId() + "--" + "centerFilter");

            //borra cualquier selección que se haya hecho en la tabla BROILERSFARM
            this.getView().byId("centerTable").removeSelections(true);

            //obtiene los registros de CENTER
            sap.ui.controller("technicalConfiguration.controller.oscenter").onRead(that, util, ospartnership, osfarm, oscenter);
        },

        /**
		 * Selecciona un registro BROILERSFARM y habilita la tabla de registros CENTER
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        onSelectCenterRecord: function(oEvent) {
            /**
			 * @type {Controller} that          Referencia a este controlador
			 * @type {JSONModel} dummy          Referencia al modelo "dummy"
			 * @type {JSONModel} PARTNERSHIP  Referencia al modelo "PARTNERSHIP"
			 * @type {JSONModel} BROILERSFARM Referencia al modelo "BROILERSFARM"
			 * @type {JSONModel} CENTER       Referencia al modelo "CENTER"
			 */
            var that = this,
                util = this.getView().getModel("util"),
                ospartnership = this.getView().getModel("OSPARTNERSHIP"),
                osfarm = this.getView().getModel("OSFARM"),
                oscenter = this.getView().getModel("OSCENTER"),
                osshed = this.getView().getModel("OSSHED");

            //guarda la ruta del registro BROILERSFARM que fue seleccionado
            oscenter.setProperty("/selectedRecordPath/", oEvent.getSource()["_aSelectedPaths"][0]);

            oscenter.setProperty("/selectedRecord/", oscenter.getProperty(oscenter.getProperty("/selectedRecordPath/")));

            //habilita el tab de la tabla de galpones
            osshed.setProperty("/settings/enabledTab", true);

            //deshabilita la opción de crear un galpón
            osshed.setProperty("/new", true);

            //establece el tab de la tabla de galpones como el tab seleccionado
            this.getView().byId(this.getView().getId() + "--" + "tabBar").setSelectedKey(this.getView().getId() + "--" + "shedFilter");

            //borra cualquier selección que se haya hecho en la tabla BROILERSFARM
            this.getView().byId("shedTable").removeSelec(true);

            //obtiene los galpones
            this.onRead(that, util, ospartnership, osfarm, oscenter, osshed);
        },

        /**
		 * Obtiene todos los registros de BROILERSFARM, dado un cliente y una sociedad
		 * @param  {Controller} that          Referencia al controlador que llama esta función
		 * @param  {JSONModel} dummy          Referencia al modelo "dummy"
		 * @param  {JSONModel} PARTNERSHIP  Referencia al modelo "PARTNERSHIP"
		 * @param  {JSONModel} BROILERSFARM Referencia al modelo "BROILERSFARM"
		 * @param  {JSONModel} CENTER       Referencia al modelo "CENTER"
		 */
        onRead: function(that, util, ospartnership, osfarm, oscenter, osshed) {
            /** @type {Object} settings opciones de la llamada a la función ajax */
            var serviceUrl = util.getProperty("/serviceUrl");
            var center_id = oscenter.getProperty(oscenter.getProperty("/selectedRecordPath/") + "/center_id");
            console.log(center_id);

            var settings = {
                type: "POST",
                contentType: "application/json",
                data: JSON.stringify({
                    "center_id": center_id,
                }),
                url: serviceUrl+"/shed/findShedByCenter3/",
                dataType: "json",
                async: true,
		 		success: function(res) {
		 				osshed.setProperty("/records",res.data);
                    console.log(osshed);
		 		},
		 		error: function(err) {
		 				util.setProperty("/error/status", err.status);
		 				util.setProperty("/error/statusText", err.statusText);
		 		}
		 };
            util.setProperty("/busy/", true);
            //borra los registros de galpones que estén almacenados actualmente
            osshed.setProperty("/records/", []);
            //realiza la llamada ajax
            $.ajax(settings);
        },
        /**
		 * Navega a la vista para crear un nuevo registro
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        onNewRecord: function(oEvent) {
			
            this.getRouter().navTo("osshed_Create", {}, true);
        },

        /**
		 * Coincidencia de ruta para acceder a la creación de un registro
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        _onCreateMatched: function(oEvent) {
            //resetea y habilita los campos del formulario para su edición
            this._resetRecordValues();
            this._editRecordValues(true);
            this._visibility();
            this.onLoadSheds();
        },

        _visibility: function(){

            console.log(this.getModel("OSFARM"));
            var osshed = this.getView().getModel("OSSHED");
            osshed.setProperty("/rotation_days/visible", true);
        },

        /**
		 * Resetea todos los valores existentes en el formulario del registro
		 */
        _resetRecordValues: function() {
            /** @type {JSONModel} SHED Referencia al modelo "SHED" */
            var osshed = this.getView().getModel("OSSHED");

            osshed.setProperty("/name/value", "");
            osshed.setProperty("/name/state", "None");
            osshed.setProperty("/name/stateText", "");

            osshed.setProperty("/stall_height/value", "");
            osshed.setProperty("/stall_height/state", "None");
            osshed.setProperty("/stall_height/stateText", "");

            osshed.setProperty("/stall_width/value", "");
            osshed.setProperty("/stall_width/state", "None");
            osshed.setProperty("/stall_width/stateText", "");

            osshed.setProperty("/status/value", "0");
            osshed.setProperty("/status/state", "None");
            osshed.setProperty("/status/stateText", "");

            osshed.setProperty("/environment/value", "0");
            osshed.setProperty("/environment/state", "None");
            osshed.setProperty("/environment/stateText", "");

            osshed.setProperty("/capmin/value", "0");
            osshed.setProperty("/capmin/state", "None");
            osshed.setProperty("/capmin/stateText", "");

            osshed.setProperty("/capmax/value", "0");
            osshed.setProperty("/capmax/state", "None");
            osshed.setProperty("/capmax/stateText", "");
        },

        /**
		 * Habilita/deshabilita la edición de los datos de un registro
		 * @param  {Boolean} edit "true" si habilita la edición, "false" si la deshabilita
		 */
        _editRecordValues: function(edit) {
            /** @type {JSONModel} SILO Referencia al modelo "SHED" */
            var osshed = this.getView().getModel("OSSHED");

            osshed.setProperty("/name/editable", edit);
            osshed.setProperty("/stall_height/editable", edit);
            osshed.setProperty("/stall_width/editable", edit);
            osshed.setProperty("/status/editable", edit);
            osshed.setProperty("/environment/editable", edit);
            osshed.setProperty("/capmin/editable", edit);
            osshed.setProperty("/capmax/editable", edit);
            osshed.setProperty("/rotation_days/editable", edit);
            osshed.setProperty("/quantity_nests/editable", edit);
            osshed.setProperty("/quantity_cages/editable", edit);

            osshed.setProperty("/name/required", edit);
            osshed.setProperty("/stall_height/required", edit);
            osshed.setProperty("/stall_width/required", edit);
            osshed.setProperty("/status/required", edit);
            osshed.setProperty("/environment/required", edit);
            osshed.setProperty("/capmin/required", edit);
            osshed.setProperty("/capmax/required", edit);
            osshed.setProperty("/rotation_days/required", edit);
            osshed.setProperty("/quantity_nests/required", edit);
            osshed.setProperty("/quantity_cages/required", edit);
        },

        /**
		 * Valida la correctitud de los datos existentes en el formulario del registro
		 * @return {Boolean} Devuelve "true" si los datos son correctos, y "false" si son incorrectos
		 */
        _validRecord: function() {
            /**
			 * @type {JSONModel} SHED Referencia al modelo "SHED"
			 * @type {Boolean} flag             "true" si los datos son válidos, "false" si no lo son
			 */
            var osshed = this.getView().getModel("OSSHED"),
                flag = true;

            if (osshed.getProperty("/name/value") === "") {
                flag = false;
                osshed.setProperty("/name/state", "Error");
                osshed.setProperty("/name/stateText", this.getI18n().getText("enter.NAME"));
            } else {
                osshed.setProperty("/name/state", "None");
                osshed.setProperty("/name/stateText", "");
            }

            if (osshed.getProperty("/stall_height/value") === "") {
                flag = false;
                osshed.setProperty("/stall_height/state", "Error");
                osshed.setProperty("/stall_height/stateText", "Introduzca el ancho");
            } else if (osshed.getProperty("/stall_height/value") <= 0) {
                flag = false;
                osshed.setProperty("/stall_height/state", "Error");
                osshed.setProperty("/stall_height/stateText", "Introduzca un valor válido");
            } else {
                osshed.setProperty("/stall_height/state", "None");
                osshed.setProperty("/stall_height/stateText", "");
            }

            if (osshed.getProperty("/stall_width/value") === "") {
                flag = false;
                osshed.setProperty("/stall_width/state", "Error");
                osshed.setProperty("/stall_width/stateText", "Introduzca el largo");
            } else if (osshed.getProperty("/stall_width/value") <= 0) {
                flag = false;
                osshed.setProperty("/stall_width/state", "Error");
                osshed.setProperty("/stall_width/stateText", "Introduzca un valor válido");
            } else {
                osshed.setProperty("/stall_width/state", "None");
                osshed.setProperty("/stall_width/stateText", "");
            }

            if (osshed.getProperty("/status/value") === "" || osshed.getProperty("/status/value") === "0") {
                console.log("entro en el error");
                flag = false;
                osshed.setProperty("/status/state", "Error");
                osshed.setProperty("/status/stateText", "Seleccione un estatus");
            } else {
                osshed.setProperty("/status/state", "None");
                osshed.setProperty("/status/stateText", "");
            }

            if (osshed.getProperty("/environment/value") === "" || osshed.getProperty("/environment/value") === "0") {
                flag = false;
                osshed.setProperty("/environment/state", "Error");
                osshed.setProperty("/environment/stateText", "Seleccione un tipo de ambiente");
            } else {
                osshed.setProperty("/environment/state", "None");
                osshed.setProperty("/environment/stateText", "");
            }


            if (osshed.getProperty("/capmin/value") === "" || osshed.getProperty("/capmin/value") === "0" ) {
                flag = false;
                osshed.setProperty("/capmin/state", "Error");
                osshed.setProperty("/capmin/stateText", "Ingrese un valor");
            } else {
                osshed.setProperty("/capmin/state", "None");
                osshed.setProperty("/capmin/stateText", "");
            }

            if (osshed.getProperty("/capmax/value") === "" || osshed.getProperty("/capmax/value") === "0") {
                flag = false;
                osshed.setProperty("/capmax/state", "Error");
                osshed.setProperty("/capmax/stateText", "Ingrese un valor");
            } else {
                osshed.setProperty("/capmax/state", "None");
                osshed.setProperty("/capmax/stateText", "");
            }

            return flag;
        },

        onStatus: function(oEvent) {
            console.log("llego a onstatus");
            var campo=oEvent.getParameters().selectedItem.mProperties.key;
            console.log(campo);
            var osshed = this.getView().getModel("OSSHED");

            osshed.setProperty("/status/value", campo);
            console.log(oEvent.getParameters().selectedItem.mProperties.key);
        },
        onEnvironment:function(oEvent){
            var osshed = this.getView().getModel("OSSHED");

            osshed.setProperty("/environment/value", oEvent.getParameters().selectedItem.mProperties.key);
            console.log(oEvent.getParameters().selectedItem.mProperties.key);
        },

        /**
		 * Solicita al servicio correspondiente crear un registro CENTER,
		 * dado un cliente, una sociedad y una granja
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        onCreate: function(oEvent) {
            //Si el registro que se desea crear es válido
            console.log("llego al oncreate");
            if (this._validRecord()) {
                /**
				 * @type {JSONModel} CENTER Referencia al modelo "CENTER"
				 * @type {JSONModel} dummy    Referencia al modelo "dummy"
				 * @type {Controller} that    Referencia a este controlador
				 * @type {Object} json        Objeto a enviar en la solicitud
				 * @type {Object} settings    Opciones de la llamada a la función ajax
				 */
                var ospartnership = this.getView().getModel("OSPARTNERSHIP"),
                    osfarm = this.getView().getModel("OSFARM"),
                    oscenter = this.getView().getModel("OSCENTER"),
                    osshed = this.getView().getModel("OSSHED"),
                    util = this.getView().getModel("util"),
                    that = this,
                    serviceUrl = util.getProperty("/serviceUrl"),
                    json = {
                        "partnership_id": ospartnership.getProperty(ospartnership.getProperty("/selectedRecordPath/") + "/partnership_id"),
                        "farm_id": osfarm.getProperty(osfarm.getProperty("/selectedRecordPath/") + "/farm_id"),
                        "center_id": oscenter.getProperty(oscenter.getProperty("/selectedRecordPath/") + "/center_id"),
                        "code": osshed.getProperty("/name/value"),
                        "stall_height": (osshed.getProperty("/stall_height/value")).toString(),
                        "stall_width": (osshed.getProperty("/stall_width/value")).toString(),
                        "status_id": (osshed.getProperty("/status/value")),
                        "environment": (osshed.getProperty("/environment/value")),
                        "capacity_min": (osshed.getProperty("/capmin/value")),
                        "capacity_max": (osshed.getProperty("/capmax/value")),
                        "rotation_days": (osshed.getProperty("/rotation_days/value"))
                    },

                    settings = {
                        async: true,
                        url: serviceUrl + "/shed",
                        method: "POST",
                        data: JSON.stringify(json),
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function(res) {
                            util.setProperty("/busy/", false);
                            that._resetRecordValues();
                            that.onToast(that.getI18n().getText("OS.recordCreated"));
                            that.getRouter().navTo("osshed", {}, true /*no history*/ );
                        },
                        error: function(err) {
                            that.onToast("Error: " + err);
                            console.log("Read failed ");
                        }
                    };
                util.setProperty("/busy/", true);
                //realiza la llamada ajax
                $.ajax(settings);
            }
        },

        /**
		 * Coincidencia de ruta para acceder a los detalles de un registro
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        _onRecordMatched: function(oEvent) {
            //Establece las opciones disponibles al visualizar el registro
            this._viewOptions();
            this.onLoadSheds();

        },

        /**
		 * Cambia las opciones de visualización disponibles en la vista de detalles de un registro
		 */
        _viewOptions: function() {
            /**
			 * @type {JSONModel} OS       Referencia al modelo "OS"
			 * @type {JSONModel} SHED Referencia al modelo "SHED"
			 */
            var util = this.getView().getModel("util"),
                osshed = this.getView().getModel("OSSHED");

            if (util.getProperty("/selectedEntity/") === "osshed") {
                osshed.setProperty("/modify/", true);
                osshed.setProperty("/delete/", true);
            } else {
                osshed.setProperty("/modify/", false);
                osshed.setProperty("/delete/", false);
            }

            osshed.setProperty("/save/", false);
            osshed.setProperty("/cancel/", false);

            this._editRecordValues(false);
        },

        /**
		 * Ajusta la vista para visualizar los datos de un registro
		 */
        onView: function() {
            this._viewOptions();
        },

        /**
		 * Cambia las opciones de edición disponibles en la vista de detalles de un registro
		 */
        _editOptions: function() {
            /** @type {JSONModel} SHED Referencia al modelo "SHED" */
            var osshed = this.getView().getModel("OSSHED");

            osshed.setProperty("/modify/", false);
            osshed.setProperty("/delete/", false);
            osshed.setProperty("/save/", true);
            osshed.setProperty("/cancel/", true);

            this._editRecordValues(true);
        },

        /**
		 * Ajusta la vista para editar los datos de un registro
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        onEdit: function(oEvent) {
            this._editOptions();
        },

        /**
		 * Verifica si el registro seleccionado tiene algún cambio con respecto a sus valores originales
		 * @return {Boolean} Devuelve "true" el registro cambió, y "false" si no cambió
		 */
        _recordChanged: function() {
            /**
			 * @type {JSONModel} SHED Referencia al modelo "SHED"
			 * @type {Boolean} flag       "true" si el registro cambió, "false" si no cambió
			 */
            var osshed = this.getView().getModel("OSSHED"),
                flag = false;

            if (osshed.getProperty("/name/value") !==
				osshed.getProperty(osshed.getProperty("/selectedRecordPath/") + "/code")) {
                flag = true;
                console.log(osshed.getProperty("/name/value"));
                console.log(osshed.getProperty(osshed.getProperty("/selectedRecordPath/") + "/code"));
            }

            if (osshed.getProperty("/stall_height/value").toString() !==
				osshed.getProperty(osshed.getProperty("/selectedRecordPath/") + "/stall_height").toString()) {
                flag = true;
                //this.onToast(osshed.getProperty("/stall_height/value"));
            }

            if (osshed.getProperty("/stall_width/value").toString() !==
				osshed.getProperty(osshed.getProperty("/selectedRecordPath/") + "/stall_width").toString()) {
                flag = true;
            }

            if (osshed.getProperty("/status/value") !==
				osshed.getProperty(osshed.getProperty("/selectedRecordPath/") + "/status_id")) {
                flag = true;
                console.log(osshed.getProperty("/status/value"));
                console.log(osshed.getProperty(osshed.getProperty("/selectedRecordPath/")));
            }

            if (osshed.getProperty("/environment/value") !==
				osshed.getProperty(osshed.getProperty("/selectedRecordPath/") + "/environment_id")) {
                flag = true;
                console.log(osshed.getProperty("/environment/value"));
                console.log(osshed.getProperty(osshed.getProperty("/selectedRecordPath/")));
            }

            if (osshed.getProperty("/capmin/value") !==
				osshed.getProperty(osshed.getProperty("/selectedRecordPath/") + "/capacity_min")) {
                flag = true;
                console.log(osshed.getProperty("/capmin/value"));
                console.log(osshed.getProperty(osshed.getProperty("/selectedRecordPath/")));
            }

            if (osshed.getProperty("/capmax/value").toString() !==
				osshed.getProperty(osshed.getProperty("/selectedRecordPath/") + "/capacity_max").toString()) {
                flag = true;
            }

            if (osshed.getProperty("/rotation_days/value") !==
				osshed.getProperty(osshed.getProperty("/selectedRecordPath/") + "/rotation_days")) {
                flag = true;
            }

            /*			if (osshed.getProperty("/quantity_nests/value") !==
				osshed.getProperty(osshed.getProperty("/selectedRecordPath/") + "/nests_quantity")) {
				flag = true;
			}

			if (osshed.getProperty("/quantity_cages/value").toString() !==
				osshed.getProperty(osshed.getProperty("/selectedRecordPath/") + "/cages_quantity")) {
				flag = true;
			}
*/
            if (!flag) this.onToast("No se detectaron cambios");

            return flag;
        },


        validateFloatInput: function (o) {
            console.log("ENTRO VAL FLOAT");
            let input= o.getSource();
            let floatLength=10,
                intLength = 10;
            console.log("entro en la funcion v");
            let value = input.getValue();
            let regex = new RegExp(`/^([0-9]{1,${intLength}})([.][0-9]{0,${floatLength}})?$/`);
            if (regex.test(value)) {
                input.setValueState("None");
                input.setValueStateText("");
                return true;
            }
            else {
                let pNumber = 0;
                let aux = value
                    .split("")
                    .filter(char => {
                        if (/^[0-9.]$/.test(char)) {
                            if (char !== ".") {
                                return true;
                            }
                            else {
                                if (pNumber === 0) {
                                    pNumber++;
                                    return true;
                                }
                            }
                        }
                    })
                    .join("")
                    .split(".");
                value = aux[0].substring(0, intLength);

                if (aux[1] !== undefined) {
                    value += "." + aux[1].substring(0, floatLength);
                }
                input.setValue(value);
                return false;
            }
        },

        validateIntInput: function (o) {
            let input= o.getSource();
            let length = 10;
            let value = input.getValue();
            let regex = new RegExp(`/^[0-9]{1,${length}}$/`);

            if (regex.test(value)) {
                return true;
            }
            else {
                let aux = value
                    .split("")
                    .filter(char => {
                        if (/^[0-9]$/.test(char)) {
                            if (char !== ".") {
                                return true;
                            }
                        }
                    })
                    .join("");
                value = aux.substring(0, length);
                input.setValue(value);
                return false;
            }
        },

        /**
		 * Cancela la edición de un galpón
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        onCancelEdit: function(oEvent) {
            /** @type {JSONModel} SHED Referencia al modelo SHED */

            this.onView();
        },

        /**
		 * Solicita al servicio correspondiente actualizar un silo
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        onUpdate: function(oEvent) {
            /**
			 * Si el registro que se quiere actualizar es válido y hubo algún cambio
			 * con respecto a sus datos originales
			 */


            if (this._validRecord() && this._recordChanged()) {

                /**
				 * @type {JSONModel} CENTER Referencia al modelo "CENTER"
				 * @type {JSONModel} dummy    Referencia al modelo "dummy"
				 * @type {Controller} that    Referencia a este controlador
				 */
                var osshed = this.getView().getModel("OSSHED"),
                    util = this.getView().getModel("util"),
                    that = this,
                    serviceUrl = util.getProperty("/serviceUrl");

                /** @type {Object} settings Opciones de la llamada a la función ajax */
                console.log(osshed);
                var settings = {
                    async: true,
                    url: serviceUrl + "/shed",
                    method: "PUT",
                    data: JSON.stringify({
                        "shed_id": osshed.getProperty(osshed.getProperty("/selectedRecordPath/") + "/shed_id"),
                        "code": osshed.getProperty("/name/value"),
                        "stall_height": osshed.getProperty("/stall_height/value"),
                        "stall_width": osshed.getProperty("/stall_width/value"),
                        "status_id": osshed.getProperty("/status/value"),
                        "environment_id": osshed.getProperty("/environment/value"),
                        "capacity_max": osshed.getProperty("/capmax/value"),
                        "capacity_min": osshed.getProperty("/capmin/value"),
                        "rotation_days": (osshed.getProperty("/rotation_days/value"))
                    }),
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function(res) {
                        console.log(res);
                        util.setProperty("/busy/", false);
                        that._resetRecordValues();
                        that._viewOptions();
                        that.onToast(that.getI18n().getText("OS.recordUpdated"));
                        that.getRouter().navTo("osshed", {}, true /*no history*/ );
                    },
                    error: function(err) {
                        that.onToast("Error de comunicación");
                        console.log("Read failed ",err);
                    }
                };
                util.setProperty("/busy/", true);
                $.ajax(settings);
                //console.log(json);
            }
        },

        /**
		 * Solicita al servicio correspondiente eliminar un galpon
		 * @param  {Controller} that         Referencia al controlador que llama esta función
		 * @param  {JSONModel} dummy         Referencia al modelo "dummy"
		 * @param  {JSONModel} PARTNERSHIP Referencia al modelo "PARTNERSHIP"
		 */
        onDelete: function(that, util, osshed) {

            /**
			 * @type {Object} json     Objeto a enviar en la solicitud
			 * @type {Object} settings Opciones de la llamada a la función ajax
			 */
            var json = {
                    "shed_id": osshed.getProperty(osshed.getProperty("/selectedRecordPath/") + "/shed_id")
                },
                serviceUrl = util.getProperty("/serviceUrl"),
                settings = {
                    async: true,
                    url: serviceUrl + "/shed",
                    method: "DELETE",
                    data: JSON.stringify(json),
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function(res) {
                        util.setProperty("/busy/", false);
                        that.onToast(that.getI18n().getText("El galpon se ha eliminado"));
                        that.getRouter().navTo("osshed", {}, true);
                    },
                    error: function(err) {
                        that.onToast("Error de comunicación");
                        console.log("Read failed", err);
                    }
                };
            util.setProperty("/busy/", true);
            //Realiza la llamada ajax
            $.ajax(settings);
        }
    });
});
sap.ui.define([
    "technicalConfiguration/controller/BaseController",
    "technicalConfiguration/model/formatter",
    "sap/m/Dialog",
    "sap/m/Text",
    "sap/m/Button"
], function(BaseController, formatter, Dialog, Text, Button) {
    "use strict";

    return BaseController.extend("technicalConfiguration.controller.osshed", {
        formatter: formatter,
        onInit: function() {
            //ruta para la vista principal de galpones
            this.getOwnerComponent().getRouter().getRoute("osshed").attachPatternMatched(this._onRouteMatched, this);
            //ruta para los detalles de un galpón
            this.getOwnerComponent().getRouter().getRoute("osshed_Create").attachPatternMatched(this._onCreateMatched, this);
            //ruta para los detalles de un galpón
            this.getOwnerComponent().getRouter().getRoute("osshed_Record").attachPatternMatched(this._onRecordMatched, this);
        },

        /**
		 * Coincidencia de ruta para acceder a la vista principal
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        _onRouteMatched: function(oEvent) {
            /**
			 * @type {Controller} that          Referencia a este controlador
			 * @type {JSONModel} dummy          Referencia al modelo "dummy"
			 * @type {JSONModel} OS             Referencia al modelo "OS"
			 * @type {JSONModel} PARTNERSHIP  Referencia al modelo "PARTNERSHIP"
			 * @type {JSONModel} BROILERSFARM Referencia al modelo "BROILERSFARM"
			 * @type {JSONModel} CENTER       Referencia al modelo "CENTER"
			 */

            var that = this,
                util = this.getView().getModel("util"),
                ospartnership = this.getView().getModel("OSPARTNERSHIP"),
                osfarm = this.getView().getModel("OSFARM"),
                oscenter = this.getView().getModel("OSCENTER"),
                osshed = this.getView().getModel("OSSHED");

            //dependiendo del dispositivo, establece la propiedad "phone"
            util.setProperty("/phone/",
                this.getOwnerComponent().getContentDensityClass() === "sapUiSizeCozy");

            ospartnership.setProperty("/settings/tableMode", "SingleSelect");
            osfarm.setProperty("/settings/tableMode", "SingleSelect");
            oscenter.setProperty("/settings/tableMode", "SingleSelect");
            this.onBreedLoad();
            //si la estructura seleccionada antes de acceder a esta vista es diferente a galpón
            if (util.getProperty("/selectedEntity") !== "osshed") {

                //establece galpón como la estructura seleccionada
                util.setProperty("/selectedEntity", "osshed");

                //establece el tab de la tabla PARTNERSHIP como el tab seleccionado
                this.getView().byId("tabBar").setSelectedKey(this.getView().getId() + "--" + "partnershipFilter");

                //borra cualquier selección que se haya hecho en la tabla PARTNERSHIP
                this.getView().byId("partnershipTable").removeSelections(true);

                //borra cualquier selección que se haya hecho en la tabla BROILERSFARM
                this.getView().byId("farmTable").removeSelections(true);

                //borra cualquier selección que se haya hecho en la tabla CENTER
                this.getView().byId("centerTable").removeSelections(true);

                //borra cualquier selección que se haya hecho en la tabla silo
                this.getView().byId("shedTable").removeSelections(true);

                //establece que no hay ningún registro PARTNERSHIP seleccionado
                ospartnership.setProperty("/selectedRecordPath/", "");

                //establece que no hay ningún registro BROILERSFARM seleccionado
                osfarm.setProperty("/selectedRecordPath/", "");

                osfarm.setProperty("/records/", []);

                oscenter.setProperty("/records/", []);

                osshed.setProperty("/records/", []);

                //establece que no hay ningún registro BROILERSFARM seleccionado
                oscenter.setProperty("/selectedRecordPath/", "");

                //deshabilita el tab de la tabla de registros BROILERSFARM
                osfarm.setProperty("/settings/enabledTab", false);

                //deshabilita el tab de la tabla de registros CENTER
                oscenter.setProperty("/settings/enabledTab", false);

                //deshabilita el tab de la tabla de silos
                osshed.setProperty("/settings/enabledTab", false);

                //deshabilita la opción de crear un galpón
                osshed.setProperty("/new", false);

                //obtiene las sociedades financieras
                sap.ui.controller("technicalConfiguration.controller.ospartnership").onRead(that, util, ospartnership);

            } else if (ospartnership.getProperty("/selectedRecordPath/") !== "" &&
				osfarm.getProperty("/selectedRecordPath/") !== "" &&
				oscenter.getProperty("/selectedRecordPath/") !== "") {

                //habilita el tab de la tabla de granjas
                osfarm.setProperty("/settings/enabledTab", true);

                //habilita el tab de la tabla de núcleos
                oscenter.setProperty("/settings/enabledTab", true);

                //habilita el tab de la tabla de galpones
                osshed.setProperty("/settings/enabledTab", true);

                //habilita la opción de crear un galpón
                osshed.setProperty("/new", true);

                //obtiene los galpones
                this.onRead(that, util, ospartnership, osfarm, oscenter, osshed);
            }
        },
        onLoadSheds: function(){
            let osshed = this.getModel("OSSHED"),
                util = this.getModel("util"),
                serviceUrl = util.getProperty("/serviceUrl")+"/shed_status/";
            console.log(osshed);
            console.log("mi back: ",serviceUrl);
            fetch(serviceUrl)
                .then(
                    function(response) {
                        if (response.status !== 200) {
                            console.log("Looks like there was a problem. Status Code: " +
								response.status);
                            return;
                        }

                        // Examine the text in the response
                        response.json().then(function(res) {
                            console.log(res.data);
                            util.setProperty("/busy/", false);
                            console.log(osshed);
                            osshed.setProperty("/statusRecords", res.data);
                        });
                    }
                )
                .catch(function(err) {
                    console.log("Fetch Error: ", err);
                });

        },
        onPartnershipSearch: function(oEvent){
            var aFilters = [],
                sQuery = oEvent.getSource().getValue(),
                binding = this.getView().byId("partnershipTable").getBinding("items");

            if (sQuery && sQuery.length > 0) {
                /** @type {Object} filter1 Primer filtro de la búsqueda */
                var filter1 = new sap.ui.model.Filter("name", sap.ui.model.FilterOperator.Contains, sQuery);
                aFilters = new sap.ui.model.Filter([filter1]);
            }

            //se actualiza el binding de la lista
            binding.filter(aFilters);

        },
        onFarmSearch: function(oEvent){
            var aFilters = [],
                sQuery = oEvent.getSource().getValue(),
                binding = this.getView().byId("farmTable").getBinding("items");
            if (sQuery && sQuery.length > 0) {
                /** @type {Object} filter1 Primer filtro de la búsqueda */
                var filter1 = new sap.ui.model.Filter("name", sap.ui.model.FilterOperator.Contains, sQuery);
                aFilters = new sap.ui.model.Filter([filter1]);
            }

            //se actualiza el binding de la lista
            binding.filter(aFilters);

        },
        onBroilersCenterSearch: function(oEvent) {
            var aFilters = [],
                sQuery = oEvent.getSource().getValue(),
                binding = this.getView().byId("centerTable").getBinding("items");

            if (sQuery && sQuery.length > 0) {
                /** @type {Object} filter1 Primer filtro de la búsqueda */
                var filter1 = new sap.ui.model.Filter("name", sap.ui.model.FilterOperator.Contains, sQuery);
                aFilters = new sap.ui.model.Filter([filter1]);
            }

            //se actualiza el binding de la lista
            binding.filter(aFilters);

        },
        onShedSearch: function(oEvent) {
            var aFilters = [],
                sQuery = oEvent.getSource().getValue(),
                binding = this.getView().byId("shedTable").getBinding("items");

            if (sQuery && sQuery.length > 0) {
                /** @type {Object} filter1 Primer filtro de la búsqueda */
                var filter1 = new sap.ui.model.Filter("code", sap.ui.model.FilterOperator.Contains, sQuery);
                aFilters = new sap.ui.model.Filter([filter1]);
            }

            //se actualiza el binding de la lista
            binding.filter(aFilters);

        },

        /**
		 * Regresa a la vista principal de la entidad seleccionada actualmente
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        onNavBack: function(oEvent) {
            /** @type {JSONModel} OS Referencia al modelo "OS" */
            var util = this.getView().getModel("util");

            this._resetRecordValues();
            this.getRouter().navTo(util.getProperty("/selectedEntity"), {}, true);
        },

        /**
		 * Selecciona un registro PARTNERSHIP y habilita la tabla de registros BROILERSFARM
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        onSelectPartnershipRecord: function(oEvent) {
            /**
			 * @type {Controller} that          Referencia a este controlador
			 * @type {JSONModel} dummy          Referencia al modelo "dummy"
			 * @type {JSONModel} PARTNERSHIP  Referencia al modelo "PARTNERSHIP"
			 * @type {JSONModel} BROILERSFARM Referencia al modelo "BROILERSFARM"
			 * @type {JSONModel} CENTER       Referencia al modelo "CENTER"
			 */
            var that = this,
                util = this.getView().getModel("util"),
                ospartnership = this.getView().getModel("OSPARTNERSHIP"),
                osfarm = this.getView().getModel("OSFARM"),
                oscenter = this.getView().getModel("OSCENTER"),
                osshed = this.getView().getModel("OSSHED");

            //guarda la ruta del registro PARTNERSHIP que fue seleccionado
            ospartnership.setProperty("/selectedRecordPath/", oEvent.getSource()["_aSelectedPaths"][0]);
            ospartnership.setProperty("/selectedRecord/", ospartnership.getProperty(ospartnership.getProperty("/selectedRecordPath/")));

            osfarm.setProperty("/selectedRecordPath/", "");
            osfarm.setProperty("/selectedRecord/", {});

            oscenter.setProperty("/selectedRecordPath/", "");
            oscenter.setProperty("/selectedRecord/", {});

            //habilita el tab de la tabla de granjas
            osfarm.setProperty("/settings/enabledTab", true);

            //deshabilita el tab de la tabla de núcleos
            oscenter.setProperty("/settings/enabledTab", false);

            //deshabilita el tab de la tabla de galpones
            osshed.setProperty("/settings/enabledTab", false);

            //deshabilita la tab de galpones // DANGER
            osshed.setProperty("/settings/enabledTab2", false);

            //deshabilita la opción de crear un galpón
            osshed.setProperty("/new", false);

            //establece el tab de la tabla de granjas como el tab seleccionado
            this.getView().byId("tabBar").setSelectedKey(this.getView().getId() + "--" + "farmFilter");

            //borra cualquier selección que se haya hecho en la tabla de granjas
            this.getView().byId("farmTable").removeSelections(true);

            //obtiene las granjas
            sap.ui.controller("technicalConfiguration.controller.osfarm").onRead(that, util, ospartnership, osfarm);
        },

        /**
		 * Selecciona un registro BROILERSFARM y habilita la tabla de registros CENTER
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        onSelectFarmRecord: function(oEvent) {
            /**
			 * @type {Controller} that          Referencia a este controlador
			 * @type {JSONModel} dummy          Referencia al modelo "dummy"
			 * @type {JSONModel} PARTNERSHIP  Referencia al modelo "PARTNERSHIP"
			 * @type {JSONModel} BROILERSFARM Referencia al modelo "BROILERSFARM"
			 * @type {JSONModel} CENTER       Referencia al modelo "CENTER"
			 */
            var that = this,
                util = this.getView().getModel("util"),
                ospartnership = this.getView().getModel("OSPARTNERSHIP"),
                osfarm = this.getView().getModel("OSFARM"),
                oscenter = this.getView().getModel("OSCENTER"),
                osshed = this.getView().getModel("OSSHED");

            //guarda la ruta del registro BROILERSFARM que fue seleccionado
            osfarm.setProperty("/selectedRecordPath/", oEvent.getSource()["_aSelectedPaths"][0]);
            osfarm.setProperty("/selectedRecord/", osfarm.getProperty(osfarm.getProperty("/selectedRecordPath/")));

            oscenter.setProperty("/selectedRecordPath/", "");
            oscenter.setProperty("/selectedRecord/", {});

            //habilita el tab de la tabla de registros CENTER
            oscenter.setProperty("/settings/enabledTab", true);

            //deshabilita el tab de la tabla de galpones
            osshed.setProperty("/settings/enabledTab", false);

            //deshabilita la opción de crear un galpón
            osshed.setProperty("/new", false);

            //establece el tab de la tabla CENTER como el tab seleccionado
            this.getView().byId(this.getView().getId() + "--" + "tabBar").setSelectedKey(this.getView().getId() + "--" + "centerFilter");

            //borra cualquier selección que se haya hecho en la tabla BROILERSFARM
            this.getView().byId("centerTable").removeSelections(true);

            //obtiene los registros de CENTER
            sap.ui.controller("technicalConfiguration.controller.oscenter").onRead(that, util, ospartnership, osfarm, oscenter);
        },

        /**
		 * Selecciona un registro BROILERSFARM y habilita la tabla de registros CENTER
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        onSelectCenterRecord: function(oEvent) {
            /**
			 * @type {Controller} that          Referencia a este controlador
			 * @type {JSONModel} dummy          Referencia al modelo "dummy"
			 * @type {JSONModel} PARTNERSHIP  Referencia al modelo "PARTNERSHIP"
			 * @type {JSONModel} BROILERSFARM Referencia al modelo "BROILERSFARM"
			 * @type {JSONModel} CENTER       Referencia al modelo "CENTER"
			 */
            var that = this,
                util = this.getView().getModel("util"),
                ospartnership = this.getView().getModel("OSPARTNERSHIP"),
                osfarm = this.getView().getModel("OSFARM"),
                oscenter = this.getView().getModel("OSCENTER"),
                osshed = this.getView().getModel("OSSHED");

            //guarda la ruta del registro BROILERSFARM que fue seleccionado
            oscenter.setProperty("/selectedRecordPath/", oEvent.getSource()["_aSelectedPaths"][0]);

            oscenter.setProperty("/selectedRecord/", oscenter.getProperty(oscenter.getProperty("/selectedRecordPath/")));

            //habilita el tab de la tabla de galpones
            osshed.setProperty("/settings/enabledTab", true);

            //deshabilita la opción de crear un galpón
            osshed.setProperty("/new", true);

            //establece el tab de la tabla de galpones como el tab seleccionado
            this.getView().byId(this.getView().getId() + "--" + "tabBar").setSelectedKey(this.getView().getId() + "--" + "shedFilter");

            //borra cualquier selección que se haya hecho en la tabla BROILERSFARM
            this.getView().byId("shedTable").removeSelections(true);

            //obtiene los galpones
            this.onRead(that, util, ospartnership, osfarm, oscenter, osshed);
        },

        /**
		 * Obtiene todos los registros de BROILERSFARM, dado un cliente y una sociedad
		 * @param  {Controller} that          Referencia al controlador que llama esta función
		 * @param  {JSONModel} dummy          Referencia al modelo "dummy"
		 * @param  {JSONModel} PARTNERSHIP  Referencia al modelo "PARTNERSHIP"
		 * @param  {JSONModel} BROILERSFARM Referencia al modelo "BROILERSFARM"
		 * @param  {JSONModel} CENTER       Referencia al modelo "CENTER"
		 */
        onRead: function(that, util, ospartnership, osfarm, oscenter, osshed) {
            /** @type {Object} settings opciones de la llamada a la función ajax */
            var serviceUrl = util.getProperty("/serviceUrl");
            var center_id = oscenter.getProperty(oscenter.getProperty("/selectedRecordPath/") + "/center_id");
            console.log(center_id);

            if(osfarm.getProperty("/selectedRecord/farm_type_id")===2){
                osfarm.setProperty("/reproduccion",true);
                console.log("Lo logré señor stark");
				
            }else{
                osfarm.setProperty("/reproduccion",false);
            }


            var settings = {
                type: "POST",
                contentType: "application/json",
                data: JSON.stringify({
                    "center_id": center_id,
                }),
                url: serviceUrl+"/shed/findShedByCenter3/",
                dataType: "json",
                async: true,
		 		success: function(res) {
						
						 res.data.forEach(element => {
                        element.old = element.order;
                    });
                    osshed.setProperty("/records",res.data);
                    console.log(osshed);
		 		},
		 		error: function(err) {
		 				util.setProperty("/error/status", err.status);
		 				util.setProperty("/error/statusText", err.statusText);
		 		}
		 };
            util.setProperty("/busy/", true);
            //borra los registros de galpones que estén almacenados actualmente
            osshed.setProperty("/records/", []);
            //realiza la llamada ajax
            $.ajax(settings);
        },

        /**
		 * Navega a la vista para crear un nuevo registro
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        onNewRecord: function(oEvent) {
            let osfarm = this.getModel("OSFARM");
            console.log(osfarm.getProperty("/selectedRecord/farm_type_id"));
            this.getRouter().navTo("osshed_Create", {}, true);
        },
        onNewShow: function(oEvent) {
            let oView = this.getView();
            var osshed = this.getView().getModel("OSSHED");
            oView.byId("tabBar").setSelectedKey("edu");
            //habilita el tab de la tabla de galpones
            osshed.setProperty("/settings2/enabledTab",true);
            osshed.setProperty("/new", false);
            //sap.ui.core.Fragment.load("edu");
            //this.byId("search").setVisible(false);
            //this.byId("shedTable").setProperty("plantTable");
            //this.byId("cancelButton").setVisible(false);
			
		
            /*var fragmentManager = getFragmentManager (); 
		fragmentManager.beginTransaction().replace(, new ShowInfo()).commit();*/
        },
        onBreedLoad: function() {
            const util = this.getModel("util"),
			  serverName = "/breed" ;
			  console.log(serverName);
            let mdbreed = this.getView().getModel("MDBREED"),
			  that = this;
            console.log(mdbreed);
            mdbreed.setProperty("/records", []);
			
            console.log("las razas");
            console.log(mdbreed);
	  
            let isRecords = new Promise((resolve, reject) => {
			  fetch(serverName)
                    .then(
				  function(response) {
                            if (response.status !== 200) {
	  
					  console.log("Looks like there was a problem. Status Code: " +
						response.status);
					  return;
                            }
                            // Examine the text in the response
                            response.json().then(function(data) {
					  //console.log(data);
					  resolve(data);
                            });
				  }
                    )
                    .catch(function(err) {
				  console.log("Fetch Error :-S", err);
                    });
            });
	  
	  
            isRecords.then((res) => {
			  if (res.data.length > 0) {
                    mdbreed.setProperty("/records", res.data);
                    // mdbreed.setProperty("/value", mdbreed.getProperty("/records/0/breed_id"));
                    console.log("las razas2");
                    console.log(mdbreed);
			  }
            });
		  },
        /**
		 * Coincidencia de ruta para acceder a la creación de un registro
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        _onCreateMatched: function(oEvent) {
            //resetea y habilita los campos del formulario para su edición
            this._resetRecordValues();
            this._editRecordValues(true);
            this._visibility();
            this.onLoadSheds();
        },

        _visibility: function(){

            console.log(this.getModel("OSFARM"));
        },

        /**
		 * Resetea todos los valores existentes en el formulario del registro
		 */
        _resetRecordValues: function() {
            /** @type {JSONModel} SHED Referencia al modelo "SHED" */
            var osshed = this.getView().getModel("OSSHED");

            osshed.setProperty("/name/value", "");
            osshed.setProperty("/name/state", "None");
            osshed.setProperty("/name/stateText", "");

            osshed.setProperty("/stall_height/value", "");
            osshed.setProperty("/stall_height/state", "None");
            osshed.setProperty("/stall_height/stateText", "");

            osshed.setProperty("/stall_width/value", "");
            osshed.setProperty("/stall_width/state", "None");
            osshed.setProperty("/stall_width/stateText", "");

            osshed.setProperty("/status/value", "0");
            osshed.setProperty("/status/state", "None");
            osshed.setProperty("/status/stateText", "");

            // osshed.setProperty("/environment/value", "0");
            // osshed.setProperty("/environment/state", "None");
            // osshed.setProperty("/environment/stateText", "");

            osshed.setProperty("/capmin/value", "0");
            osshed.setProperty("/capmin/state", "None");
            osshed.setProperty("/capmin/stateText", "");

            osshed.setProperty("/capmax/value", "0");
            osshed.setProperty("/capmax/state", "None");
            osshed.setProperty("/capmax/stateText", "");

            osshed.setProperty("/rotation_days/value", "0");
            osshed.setProperty("/rotation_days/state", "None");
            osshed.setProperty("/rotation_days/stateText", "");

            osshed.setProperty("/disable/value", false);
        },

        /**
		 * Habilita/deshabilita la edición de los datos de un registro
		 * @param  {Boolean} edit "true" si habilita la edición, "false" si la deshabilita
		 */
        _editRecordValues: function(edit) {
            /** @type {JSONModel} SILO Referencia al modelo "SHED" */
            var osshed = this.getView().getModel("OSSHED");

            osshed.setProperty("/name/editable", edit);
            osshed.setProperty("/stall_height/editable", edit);
            osshed.setProperty("/stall_width/editable", edit);
            osshed.setProperty("/status/editable", edit);
            osshed.setProperty("/breed", edit);
            // osshed.setProperty("/environment/editable", edit);
            osshed.setProperty("/capmin/editable", edit);
            osshed.setProperty("/capmax/editable", edit);
            osshed.setProperty("/rotation_days/editable", edit);
            osshed.setProperty("/quantity_nests/editable", edit);
            osshed.setProperty("/quantity_cages/editable", edit);
            osshed.setProperty("/rotation_days/editable", edit);
            osshed.setProperty("/disable/editable", edit);

            osshed.setProperty("/name/required", edit);
            osshed.setProperty("/stall_height/required", edit);
            osshed.setProperty("/stall_width/required", edit);
            osshed.setProperty("/status/required", edit);
            // osshed.setProperty("/environment/required", edit);
            osshed.setProperty("/capmin/required", edit);
            osshed.setProperty("/capmax/required", edit);
            osshed.setProperty("/rotation_days/required", edit);
            osshed.setProperty("/quantity_nests/required", edit);
            osshed.setProperty("/quantity_cages/required", edit);
        },
        _editRecordValues2: function(edit) {
            /** @type {JSONModel} SILO Referencia al modelo "SHED" */
            var osshed = this.getView().getModel("OSSHED");

            // osshed.setProperty("/name/editable", edit);
            osshed.setProperty("/stall_height/editable", edit);
            osshed.setProperty("/stall_width/editable", edit);
            osshed.setProperty("/breed", edit);
            // osshed.setProperty("/status/editable", edit);
            // osshed.setProperty("/environment/editable", edit);
            osshed.setProperty("/capmin/editable", edit);
            osshed.setProperty("/capmax/editable", edit);
            osshed.setProperty("/rotation_days/editable", edit);
            osshed.setProperty("/quantity_nests/editable", edit);
            osshed.setProperty("/quantity_cages/editable", edit);
            osshed.setProperty("/rotation_days/editable", edit);
            osshed.setProperty("/disable/editable", edit);

            osshed.setProperty("/name/required", edit);
            osshed.setProperty("/stall_height/required", edit);
            osshed.setProperty("/stall_width/required", edit);
            // osshed.setProperty("/status/required", edit);
            // osshed.setProperty("/status/required", edit);
            // osshed.setProperty("/environment/required", edit);
            osshed.setProperty("/capmin/required", edit);
            osshed.setProperty("/capmax/required", edit);
            osshed.setProperty("/rotation_days/required", edit);
            osshed.setProperty("/quantity_nests/required", edit);
            osshed.setProperty("/quantity_cages/required", edit);
        },

        /**
		 * Valida la correctitud de los datos existentes en el formulario del registro
		 * @return {Boolean} Devuelve "true" si los datos son correctos, y "false" si son incorrectos
		 */
        _validRecord: function() {
            /**
			 * @type {JSONModel} SHED Referencia al modelo "SHED"
			 * @type {Boolean} flag             "true" si los datos son válidos, "false" si no lo son
			 */
            var osshed = this.getView().getModel("OSSHED"),
                flag = true;

            if (osshed.getProperty("/name/value") === "") {
                flag = false;
                osshed.setProperty("/name/state", "Error");
                osshed.setProperty("/name/stateText", this.getI18n().getText("enter.NAME"));
            } else {
                osshed.setProperty("/name/state", "None");
                osshed.setProperty("/name/stateText", "");
            }

            if (osshed.getProperty("/stall_height/value") === "") {
                flag = false;
                osshed.setProperty("/stall_height/state", "Error");
                osshed.setProperty("/stall_height/stateText", "Introduzca el ancho");
            } else if (osshed.getProperty("/stall_height/value") <= 0) {
                flag = false;
                osshed.setProperty("/stall_height/state", "Error");
                osshed.setProperty("/stall_height/stateText", "Introduzca un valor válido");
            } else {
                osshed.setProperty("/stall_height/state", "None");
                osshed.setProperty("/stall_height/stateText", "");
            }

            if (osshed.getProperty("/stall_width/value") === "") {
                flag = false;
                osshed.setProperty("/stall_width/state", "Error");
                osshed.setProperty("/stall_width/stateText", "Introduzca el largo");
            } else if (osshed.getProperty("/stall_width/value") <= 0) {
                flag = false;
                osshed.setProperty("/stall_width/state", "Error");
                osshed.setProperty("/stall_width/stateText", "Introduzca un valor válido");
            } else {
                osshed.setProperty("/stall_width/state", "None");
                osshed.setProperty("/stall_width/stateText", "");
            }

            if (osshed.getProperty("/status/value") === "" || osshed.getProperty("/status/value") === "0") {
                console.log("entro en el error");
                flag = false;
                osshed.setProperty("/status/state", "Error");
                osshed.setProperty("/status/stateText", "Seleccione un estatus");
            } else {
                osshed.setProperty("/status/state", "None");
                osshed.setProperty("/status/stateText", "");
            }

            // if (osshed.getProperty("/environment/value") === "" || osshed.getProperty("/environment/value") === "0") {
            // 	flag = false;
            // 	osshed.setProperty("/environment/state", "Error");
            // 	osshed.setProperty("/environment/stateText", "Seleccione un tipo de ambiente");
            // } else {
            // 	osshed.setProperty("/environment/state", "None");
            // 	osshed.setProperty("/environment/stateText", "");
            // }


            if (osshed.getProperty("/capmin/value") === "" || osshed.getProperty("/capmin/value") === "0" ) {
                flag = false;
                osshed.setProperty("/capmin/state", "Error");
                osshed.setProperty("/capmin/stateText", "Ingrese un valor");
            } else {
                osshed.setProperty("/capmin/state", "None");
                osshed.setProperty("/capmin/stateText", "");
            }

            if (osshed.getProperty("/capmax/value") === "" || osshed.getProperty("/capmax/value") === "0") {
                flag = false;
                osshed.setProperty("/capmax/state", "Error");
                osshed.setProperty("/capmax/stateText", "Ingrese un valor");
            } else {
                osshed.setProperty("/capmax/state", "None");
                osshed.setProperty("/capmax/stateText", "");
            }

            if (osshed.getProperty("/rotation_days/value") === "" || osshed.getProperty("/rotation_days/value") === "0") {
                flag = false;
                osshed.setProperty("/rotation_days/state", "Error");
                osshed.setProperty("/rotation_days/stateText", "Ingrese dias");
            } else {
                osshed.setProperty("/rotation_days/state", "None");
                osshed.setProperty("/rotation_days/stateText", "");
            }



            return flag;
        },
        _validRecord2: function() {
            /**
			 * @type {JSONModel} SHED Referencia al modelo "SHED"
			 * @type {Boolean} flag             "true" si los datos son válidos, "false" si no lo son
			 */
            var osshed = this.getView().getModel("OSSHED"),
                flag = true;

			

            if (osshed.getProperty("/stall_height/value") === "") {
                flag = false;
                osshed.setProperty("/stall_height/state", "Error");
                osshed.setProperty("/stall_height/stateText", "Introduzca el ancho");
            } else if (osshed.getProperty("/stall_height/value") <= 0) {
                flag = false;
                osshed.setProperty("/stall_height/state", "Error");
                osshed.setProperty("/stall_height/stateText", "Introduzca un valor válido");
            } else {
                osshed.setProperty("/stall_height/state", "None");
                osshed.setProperty("/stall_height/stateText", "");
            }

            if (osshed.getProperty("/stall_width/value") === "") {
                flag = false;
                osshed.setProperty("/stall_width/state", "Error");
                osshed.setProperty("/stall_width/stateText", "Introduzca el largo");
            } else if (osshed.getProperty("/stall_width/value") <= 0) {
                flag = false;
                osshed.setProperty("/stall_width/state", "Error");
                osshed.setProperty("/stall_width/stateText", "Introduzca un valor válido");
            } else {
                osshed.setProperty("/stall_width/state", "None");
                osshed.setProperty("/stall_width/stateText", "");
            }
            if (osshed.getProperty("/capmin/value") === "" || osshed.getProperty("/capmin/value") === "0" ) {
                flag = false;
                osshed.setProperty("/capmin/state", "Error");
                osshed.setProperty("/capmin/stateText", "Ingrese un valor");
            } else {
                osshed.setProperty("/capmin/state", "None");
                osshed.setProperty("/capmin/stateText", "");
            }

            if (osshed.getProperty("/capmax/value") === "" || osshed.getProperty("/capmax/value") === "0") {
                flag = false;
                osshed.setProperty("/capmax/state", "Error");
                osshed.setProperty("/capmax/stateText", "Ingrese un valor");
            } else {
                osshed.setProperty("/capmax/state", "None");
                osshed.setProperty("/capmax/stateText", "");
            }

            if (osshed.getProperty("/rotation_days/value") === "" || osshed.getProperty("/rotation_days/value") === "0") {
                flag = false;
                osshed.setProperty("/rotation_days/state", "Error");
                osshed.setProperty("/rotation_days/stateText", "Ingrese dias");
            } else {
                osshed.setProperty("/rotation_days/state", "None");
                osshed.setProperty("/rotation_days/stateText", "");
            }
            return flag;
        },

        onStatus: function(oEvent) {
            console.log("llego a onstatus");
            var campo=oEvent.getParameters().selectedItem.mProperties.key;
            console.log(campo);
            var osshed = this.getView().getModel("OSSHED");

            osshed.setProperty("/status/value", campo);
            console.log(oEvent.getParameters().selectedItem.mProperties.key);
        },
        onCheck: function(oEvent) {
            console.log("llego a Check");
            var campo=oEvent.getParameters().selectedItem.mProperties.key;
            console.log(campo);
            var osshed = this.getView().getModel("OSSHED");

            osshed.setProperty("/disable/value", campo);
            console.log(oEvent.getParameters().selectedItem.mProperties.key);
        },
        onBreed: function(oEvent) {
            console.log("llego a Breed");
            var campo=oEvent.getParameters().selectedItem.mProperties.key;
            console.log(campo);
            var mdbreed = this.getView().getModel("MDBREED");

            mdbreed.setProperty("/selectedKey", campo);
            console.log(oEvent.getParameters().selectedItem.mProperties.key);
        },
        onEnvironment:function(oEvent){
            var osshed = this.getView().getModel("OSSHED");

            osshed.setProperty("/environment/value", oEvent.getParameters().selectedItem.mProperties.key);
            console.log(oEvent.getParameters().selectedItem.mProperties.key);
        },

        /**
		 * Solicita al servicio correspondiente crear un registro CENTER,
		 * dado un cliente, una sociedad y una granja
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        onCreate: function(oEvent) {
            //Si el registro que se desea crear es válido
            console.log("llego al oncreate");
            if (this._validRecord()) {
                /**
				 * @type {JSONModel} CENTER Referencia al modelo "CENTER"
				 * @type {JSONModel} dummy    Referencia al modelo "dummy"
				 * @type {Controller} that    Referencia a este controlador
				 * @type {Object} json        Objeto a enviar en la solicitud
				 * @type {Object} settings    Opciones de la llamada a la función ajax
				 */
                var ospartnership = this.getView().getModel("OSPARTNERSHIP"),
                    osfarm = this.getView().getModel("OSFARM"),
                    oscenter = this.getView().getModel("OSCENTER"),
                    mdbreed = this.getView().getModel("MDBREED"),
                    osshed = this.getView().getModel("OSSHED"),
                    util = this.getView().getModel("util"),
                    that = this,
                    serviceUrl = util.getProperty("/serviceUrl"),
                    json = {
                        "client_id": 1,
                        "partnership_id": ospartnership.getProperty(ospartnership.getProperty("/selectedRecordPath/") + "/partnership_id"),
                        "farm_id": osfarm.getProperty(osfarm.getProperty("/selectedRecordPath/") + "/farm_id"),
                        "center_id": oscenter.getProperty(oscenter.getProperty("/selectedRecordPath/") + "/center_id"),
                        "code": osshed.getProperty("/name/value"),
                        "stall_height": (osshed.getProperty("/stall_height/value")).toString(),
                        "stall_width": (osshed.getProperty("/stall_width/value")).toString(),
                        "status_id": (osshed.getProperty("/status/value")),
                        // "environment": (osshed.getProperty("/environment/value")),
                        "capacity_min": (osshed.getProperty("/capmin/value")),
                        "capacity_max": (osshed.getProperty("/capmax/value")),
                        "rotation_days": (osshed.getProperty("/rotation_days/value")),
                        "breed_id": (mdbreed.getProperty("/selectedKey")),
                        "os_disable": (osshed.getProperty("/disable/value"))
                    },

                    settings = {
                        async: true,
                        url: serviceUrl + "/shed",
                        method: "POST",
                        data: JSON.stringify(json),
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function(res) {
                            util.setProperty("/busy/", false);
                            that._resetRecordValues();
                            that.onToast(that.getI18n().getText("OS.recordCreated"));
                            that.getRouter().navTo("osshed", {}, true /*no history*/ );
                        },
                        error: function(err) {
                            that.onToast("Error: " + err);
                            console.log("Read failed ", err);
                        }
                    };
                console.log("este si es mi back: ", serviceUrl);
                console.log("mi key de breed: ", mdbreed.getProperty("/selectedKey"));
                util.setProperty("/busy/", true);
                //realiza la llamada ajax
                $.ajax(settings);
            }
        },

        /**
		 * Coincidencia de ruta para acceder a los detalles de un registro
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        _onRecordMatched: function(oEvent) {
            //Establece las opciones disponibles al visualizar el registro
            this._viewOptions();
            this.onLoadSheds();

        },

        /**
		 * Cambia las opciones de visualización disponibles en la vista de detalles de un registro
		 */
        _viewOptions: function() {
            /**
			 * @type {JSONModel} OS       Referencia al modelo "OS"
			 * @type {JSONModel} SHED Referencia al modelo "SHED"
			 */
            var util = this.getView().getModel("util"),
                osshed = this.getView().getModel("OSSHED");

            if (util.getProperty("/selectedEntity/") === "osshed") {
                osshed.setProperty("/modify/", true);
                osshed.setProperty("/delete/", true);
            } else {
                osshed.setProperty("/modify/", false);
                osshed.setProperty("/delete/", false);
            }

            osshed.setProperty("/save/", false);
            osshed.setProperty("/cancel/", false);

            this._editRecordValues(false);
        },

        /**
		 * Ajusta la vista para visualizar los datos de un registro
		 */
        onView: function() {
            this._viewOptions();
        },

        /**
		 * Cambia las opciones de edición disponibles en la vista de detalles de un registro
		 */
        _editOptions: function() {
            /** @type {JSONModel} SHED Referencia al modelo "SHED" */
            var osshed = this.getView().getModel("OSSHED");

            osshed.setProperty("/modify/", false);
            osshed.setProperty("/delete/", false);
            osshed.setProperty("/save/", true);
            osshed.setProperty("/cancel/", true);

            this._editRecordValues2(true);
        },

        /**
		 * Ajusta la vista para editar los datos de un registro
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        onEdit: function(oEvent) {
            let osshed = this.getView().getModel("OSSHED"),
                selected = osshed.getProperty(osshed.getProperty("/selectedRecordPath/"));
			
            if(selected.statusshed_id !== 1){
                let dialog = new Dialog({
                    title: "Información",
                    type: "Message",
                    state: "Warning",
                    content: new Text({
                        text: "No se puede modificar el galpón, debido a que está siendo utilizado en un proceso."
                    }),
                    beginButton: new Button({
                        text: "OK",
                        press: function () {
                            dialog.close();
                        }
                    }),
                    afterClose: function() {
                        dialog.destroy();
                    }
				  });
                dialog.open();
            }else{
                this._editOptions();
            }	
            console.log(selected);

			
        },

        /**
		 * Verifica si el registro seleccionado tiene algún cambio con respecto a sus valores originales
		 * @return {Boolean} Devuelve "true" el registro cambió, y "false" si no cambió
		 */
        _recordChanged: function() {
            /**
			 * @type {JSONModel} SHED Referencia al modelo "SHED"
			 * @type {Boolean} flag       "true" si el registro cambió, "false" si no cambió
			 */
            var osshed = this.getView().getModel("OSSHED"),
                flag = false;

            if (osshed.getProperty("/name/value") !==
				osshed.getProperty(osshed.getProperty("/selectedRecordPath/") + "/code")) {
                flag = true;
                console.log(osshed.getProperty("/name/value"));
                console.log(osshed.getProperty(osshed.getProperty("/selectedRecordPath/") + "/code"));
            }
            console.log(osshed);
            console.log(osshed.getProperty("/selectedRecordPath/") + "/stall_height");
            console.log(osshed.getProperty("/stall_height/value"));
            if (osshed.getProperty("/stall_height/value").toString() !==
				osshed.getProperty(osshed.getProperty("/selectedRecordPath/") + "/stall_height").toString()) {
                flag = true;
                //this.onToast(osshed.getProperty("/stall_height/value"));
            }

            if (osshed.getProperty("/stall_width/value").toString() !==
				osshed.getProperty(osshed.getProperty("/selectedRecordPath/") + "/stall_width").toString()) {
                flag = true;
            }

            if (osshed.getProperty("/status/value") !==
				osshed.getProperty(osshed.getProperty("/selectedRecordPath/") + "/statusshed_id")) {
                flag = true;
                console.log(osshed.getProperty("/status/value"));
                console.log(osshed.getProperty(osshed.getProperty("/selectedRecordPath/")));
            }

            // if (osshed.getProperty("/environment/value") !==
            // 	osshed.getProperty(osshed.getProperty("/selectedRecordPath/") + "/environment_id")) {
            // 	flag = true;
            // 	console.log(osshed.getProperty("/environment/value"));
            // 	console.log(osshed.getProperty(osshed.getProperty("/selectedRecordPath/")));
            // }

            if (osshed.getProperty("/capmin/value") !==
				osshed.getProperty(osshed.getProperty("/selectedRecordPath/") + "/capmin")) {
                flag = true;
                console.log(osshed.getProperty("/capmin/value"));
                console.log(osshed.getProperty(osshed.getProperty("/selectedRecordPath/")));
            }

            if (osshed.getProperty("/capmax/value").toString() !==
				osshed.getProperty(osshed.getProperty("/selectedRecordPath/") + "/capmax").toString()) {
                flag = true;
            }

            if (osshed.getProperty("/rotation_days/value") !==
				osshed.getProperty(osshed.getProperty("/selectedRecordPath/") + "/rotation_days")) {
                flag = true;
            }
            /*
			if (osshed.getProperty("/quantity_nests/value") !==
				osshed.getProperty(osshed.getProperty("/selectedRecordPath/") + "/nests_quantity")) {
				flag = true;
			}

			if (osshed.getProperty("/quantity_cages/value").toString() !==
				osshed.getProperty(osshed.getProperty("/selectedRecordPath/") + "/cages_quantity")) {
				flag = true;
			}
*/
            if (!flag) this.onToast("No se detectaron cambios");

            return flag;
        },
        _recordChanged2: function() {
            /**
			 * @type {JSONModel} SHED Referencia al modelo "SHED"
			 * @type {Boolean} flag       "true" si el registro cambió, "false" si no cambió
			 */
            var osshed = this.getView().getModel("OSSHED"),
                mdbreed = this.getView().getModel("MDBREED"),
                flag = false;

			
            if (osshed.getProperty("/stall_height/value").toString() !==
				osshed.getProperty(osshed.getProperty("/selectedRecordPath/") + "/stall_height").toString()) {
                flag = true;
                //this.onToast(osshed.getProperty("/stall_height/value"));
            }

            if (osshed.getProperty("/stall_width/value").toString() !==
				osshed.getProperty(osshed.getProperty("/selectedRecordPath/") + "/stall_width").toString()) {
                flag = true;
            }

            if (osshed.getProperty("/capmin/value") !==
				osshed.getProperty(osshed.getProperty("/selectedRecordPath/") + "/capmin")) {
                flag = true;
                console.log(osshed.getProperty("/capmin/value"));
                console.log(osshed.getProperty(osshed.getProperty("/selectedRecordPath/")));
            }

            if (osshed.getProperty("/capmax/value").toString() !==
				osshed.getProperty(osshed.getProperty("/selectedRecordPath/") + "/capmax").toString()) {
                flag = true;
            }

            if (osshed.getProperty("/rotation_days/value") !==
				osshed.getProperty(osshed.getProperty("/selectedRecordPath/") + "/rotation_days")) {
                flag = true;
            }
            if (osshed.getProperty("/disable/value") !==
				osshed.getProperty(osshed.getProperty("/selectedRecordPath/") + "/os_disable")) {
                flag = true;
            }
            if (mdbreed.getProperty("/selectedKey") !==
				osshed.getProperty(osshed.getProperty("/selectedRecordPath/") + "/breed_id")) {
                flag = true;
            }
            if (!flag) this.onToast("No se detectaron cambios");

            return flag;
        },

        /**
		 * Cancela la edición de un galpón
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        onCancelEdit: function(oEvent) {
            /** @type {JSONModel} SHED Referencia al modelo SHED */

            this.onView();
        },

        /**
		 * Solicita al servicio correspondiente actualizar un silo
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        onUpdate: function(oEvent) {
            /**
			 * Si el registro que se quiere actualizar es válido y hubo algún cambio
			 * con respecto a sus datos originales
			 */


            if (this._validRecord2() && this._recordChanged2()) {

                /**
				 * @type {JSONModel} CENTER Referencia al modelo "CENTER"
				 * @type {JSONModel} dummy    Referencia al modelo "dummy"
				 * @type {Controller} that    Referencia a este controlador
				 */
                var osshed = this.getView().getModel("OSSHED"),
                    mdbreed = this.getView().getModel("MDBREED"),
                    util = this.getView().getModel("util"),
                    that = this,
                    serviceUrl = util.getProperty("/serviceUrl");

                /** @type {Object} settings Opciones de la llamada a la función ajax */
                console.log(osshed);
                console.log("el value del check: ",osshed.getProperty("/disable/value"));
                var settings = {
                    async: true,
                    url: serviceUrl + "/shed",
                    method: "PUT",
                    data: JSON.stringify({
                        "shed_id": osshed.getProperty(osshed.getProperty("/selectedRecordPath/") + "/shed_id"),
                        "code": osshed.getProperty("/name/value"),
                        "stall_height": osshed.getProperty("/stall_height/value"),
                        "stall_width": osshed.getProperty("/stall_width/value"),
                        "status_id": osshed.getProperty("/status/value"),
                        // "environment_id": osshed.getProperty("/environment/value"),
                        "capacity_max": osshed.getProperty("/capmax/value"),
                        "capacity_min": osshed.getProperty("/capmin/value"),
                        "rotation_days": osshed.getProperty("/rotation_days/value"),
                        "breed_id": mdbreed.getProperty("/selectedKey"),
                        "os_disable": osshed.getProperty("/disable/value")
                    }),
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function(res) {
                        console.log(res);
                        util.setProperty("/busy/", false);
                        that._resetRecordValues();
                        that._viewOptions();
                        that.onToast(that.getI18n().getText("OS.recordUpdated"));
                        that.getRouter().navTo("osshed", {}, true /*no history*/ );
                    },
                    error: function(err) {
                        that.onToast("Error de comunicación");
                        console.log("Read failed ",err);
                    }
                };
                util.setProperty("/busy/", true);
                $.ajax(settings);
                //console.log(json);
            }
        },

        consult: function(oEvent){
            var oView = this.getView();
            var oDialog = oView.byId("galpon");
            var osshed = this.getView().getModel("OSSHED");
            let sheds = osshed.getProperty("/records");
            let dateSheds = "";
            let aux = [];
            let flag;

		
            if(!$.isEmptyObject(oDialog.getValue())){ 
				
		
                for( let i = 0;i < sheds.length;i++){
                    console.log(sheds[i].avaliable_date);

                    if(sheds[i].avaliable_date != null){
						    dateSheds = this.formatDate(sheds[i].avaliable_date);
						  
						    
                        flag = this.compareDate(dateSheds, oDialog.getValue());
						
						
                        if(!flag){
                            sheds[i].statusshed_id= "No Disponible";
                            aux.push(sheds[i]);
                        }else
                        {
                            sheds[i].statusshed_id= "Disponible";
                            aux.push(sheds[i]);
                        }
                    }else
                    if(sheds[i].avaliable_date == null){
                        console.log("entro en el condicional");								
                        sheds[i].statusshed_id= "Disponible";
                        console.log("NULL");
                        aux.push(sheds[i]);
                    }

                }
                osshed.setProperty("/asd",aux);
            }
		
		
        },
        onVerifyIsUsed: async function(shed_id){
            let ret;

            const response = await fetch("/shed/isBeingUsed", {
                headers: {
                    "Content-Type": "application/json"
                },
                method: "POST",
                body: JSON.stringify({
                    shed_id: shed_id
                })
            });

            if (response.status !== 200 && response.status !== 409) {
                console.log("Looks like there was a problem. Status Code: " +
                    response.status);
                return;
            }
            if(response.status === 200){
                const res = await response.json();
                console.log(res);
                ret = res.data.used;
            }
            console.log(response);
            console.log(ret);
            return ret;
            
        },
        
        formatDate: function(date){
            console.log("Format: "+ date);
            if(date!== null){
                let c= "-";//caracter separatos
                date= new Date(date.toString());
                // if((typeof date.getMonth === 'function'))
                if(!isNaN( date.getFullYear() )){
                    date= ( ((date.getDate()<10)? "0"+date.getDate(): date.getDate() )+c+
							((date.getMonth()+1<10)? "0"+(date.getMonth()+1): date.getMonth()+1 )+c+
							date.getFullYear() );
                }
                else
                    date=null;
            }
            return (date);
        },
        
        compareDate: function(date1, date2){
            let nDate = date1.split("-"),
                nDate2 = date2.split("-");
            let	flag;
            console.log("Vector:", nDate2);
            console.log("Vector:", nDate);
            //año galpon mayor
            console.log("ESTOY EN COMPARE DATE!!!");
            if(nDate[2] < nDate2[2]){
                flag= true;
            }else
            if(nDate[2] === nDate2[2]){
                //años iguales
                if(nDate[1] === nDate2[1]){
                    //meses iguales
                    if(nDate[0] === nDate2[0]){
                        //dias iguales
                        flag = false;
                    }else
                    if(nDate[0] < nDate2[0]){
                        //años iguales, meses iguales dia mayor
                        flag = true;
                    }else
                    {
                        flag = false;
                    }
				
                }else
                if(nDate[1] < nDate2[1]){
                    flag = true;
                }else{
                    flag = false;
                }

            }else{
                flag = false;
            }
				
            return flag;
        },
        /**
		 * Solicita al servicio correspondiente eliminar un galpon
		 * @param  {Controller} that         Referencia al controlador que llama esta función
		 * @param  {JSONModel} dummy         Referencia al modelo "dummy"
		 * @param  {JSONModel} PARTNERSHIP Referencia al modelo "PARTNERSHIP"
		 */
        onDelete: async function(that, util, osshed) {

            /**
			 * @type {Object} json     Objeto a enviar en la solicitud
			 * @type {Object} settings Opciones de la llamada a la función ajax
			 */


            let shed_id = osshed.getProperty(osshed.getProperty("/selectedRecordPath/") + "/shed_id");
            let cond = await this.onVerifyIsUsed(shed_id);
            console.log("La cond el mio: ", cond);
            if(cond){
                var dialog = new Dialog({
                    title: "Información",
                    type: "Message",
                    state: "Warning",
                    content: new Text({
                        text: "No se puede eliminar el Galpón, porque está siendo utilizado."
                    }),
                    beginButton: new Button({
                        text: "OK",
                        press: function() {
                            dialog.close();
                            that.confirmDeleteDlg.close();
                        }
                    }),
                    afterClose: function() {
                        dialog.destroy();
                    }
                });
        
                dialog.open();
            }else{
                var json = {
                        "shed_id": shed_id
                    },
                    serviceUrl = util.getProperty("/serviceUrl"),
                    settings = {
                        async: true,
                        url: serviceUrl + "/shed",
                        method: "DELETE",
                        data: JSON.stringify(json),
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function(res) {
                            util.setProperty("/busy/", false);
                            that.onToast(that.getI18n().getText("El galpon se ha eliminado"));
                            that.getRouter().navTo("osshed", {}, true);
                        },
                        error: function(err) {
                            that.onToast("Error de comunicación");
                            console.log("Read failed", err);
                        }
                    };
                util.setProperty("/busy/", true);
                //Realiza la llamada ajax
                $.ajax(settings);
            } 
			
        }
    });
});
