sap.ui.define([
    "technicalConfiguration/controller/BaseController",
    "sap/ui/model/json/JSONModel",
    "sap/ui/model/Sorter",
    "sap/ui/model/Filter",
    "sap/m/Dialog",
    "sap/m/Button"
], function(BaseController, JSONModel,Sorter,Filter,Dialog, Button) {
    "use strict";

    return BaseController.extend("technicalConfiguration.controller.mdparameter", {

        onInit: function() {
            //ruta para la vista principal
            this.getOwnerComponent().getRouter().getRoute("mdparameter").attachPatternMatched(this._onRouteMatched, this);
            //ruta para la vista de detalles de un registro
            this.getOwnerComponent().getRouter().getRoute("mdparameter_Record").attachPatternMatched(this._onRecordMatched, this);
            //ruta para la vista de creación de un registro
            this.getOwnerComponent().getRouter().getRoute("mdparameter_Create").attachPatternMatched(this._onCreateMatched, this);
        },

        /**
         * Coincidencia de ruta para acceder a la vista principal
         * @param  {Event} oEvent Evento que llamó esta función
         */
        _onRouteMatched: function(oEvent) {

            const util = this.getModel("util");
            const mdparameter = this.getModel("MDPARAMETER");
            //dependiendo del dispositivo, establece la propiedad "phone"
            util.setProperty("/phone/",
                this.getOwnerComponent().getContentDensityClass() === "sapUiSizeCozy");

            //establece MDPARAMETER como la entidad seleccionada
            util.setProperty("/selectedEntity/", "mdparameter");

            util.setProperty("/busy", true);

            //obtiene los registros de MDPARAMETER
            this.getParametersData()
                .then(records => {
                    //console.log(records);
                    mdparameter.setProperty("/records/", records.data);
                    console.log(mdparameter);
                    util.setProperty("/busy", false);
                })
                .catch(err => {
                    console.log(err);
                });
        },

        /**
        * Obtiene todos los registros de MDPARAMETER
        */
        getParametersData: function() {
            const util = this.getModel("util");
            const mdparameter = this.getModel("MDPARAMETER");
            var serviceUrl= util.getProperty("/serviceUrl");
            const url = serviceUrl+"/parameter/";
            console.log("url", url);
            const method = "GET";
            const data = {};

            return new Promise((resolve, reject) => {
                function getParameters(res) {
                    resolve(res);
                }

                function error(err) {
                    reject(err);
                }

                this.sendRequest.call(this, url, method, data, getParameters, error, error);
            });
        },
        /**
         * Coincidencia de ruta para acceder a la creación de un registro
         * @param  {Event} oEvent Evento que llamó esta función
         */
        _onCreateMatched: function(oEvent) {
            var util = this.getView().getModel("util");
            var create = true;
            this.loadProcessForC();
            this.loadMeasure(create);
           
            console.log("here i am");
            this._resetRecordValues();
            this._editRecordValues(true);
            this._editRecordRequired(true);
            util.setProperty("/busy/", false);

        },

        loadProcess: function(create){
            console.log("loadprocess");
            var util = this.getView().getModel("util"),
                mdprocess = this.getView().getModel("MDPROCESS"),
                mdparameter = this.getModel("MDPARAMETER");
            var serviceUrl= util.getProperty("/serviceUrl");
            var settings = {
                url: serviceUrl+"/process/",
                method: "GET",
                success: function(res) {
                    mdprocess.setProperty("/records",res.data);
                    console.log("entre");
                    if(create) {mdparameter.setProperty("/process_id", res.data[0].process_id);}
                },
                error: function(err) {
                    console.log(err);
                    util.setProperty("/error/status", err.status);
                    util.setProperty("/error/statusText", err.statusText);

                }
            };


            //borra los registros MDPROCESS que estén almacenados actualmente
            mdprocess.setProperty("/records/", []);
            //realiza la llamada ajax
            $.ajax(settings);
            console.log("listo");
        },
        /**
        *Carga los valores de medidas
        */
        loadMeasure: function(create){
            console.log("loadmeasure");
            var mdmeasure = this.getView().getModel("MDMEASURE"),
                util = this.getView().getModel("util"),
                mdparameter = this.getModel("MDPARAMETER");
            var serviceUrl= util.getProperty("/serviceUrl");
            var settings = {
                url: serviceUrl+"/measure/",
                method: "GET",
                success: function(res) {
                    mdmeasure.setProperty("/records",res.data);
                    if(create) mdparameter.setProperty("/measure_id/value",res.data[0].measure_id);
                },
                error: function(err) {
                    console.log(err);
                    util.setProperty("/error/status", err.status);
                    util.setProperty("/error/statusText", err.statusText);

                }
            };
            //borra los registros MDPROCESS que estén almacenados actualmente
            mdmeasure.setProperty("/records/", []);
            //realiza la llamada ajax
            $.ajax(settings);

        },
        /**
         * Resetea todos los valores existentes en el formulario del registro
         */
        _resetRecordValues: function() {
            /**
             * @type {JSONModel} MDPARAMETER Referencia al modelo "MDPARAMETER"
             */
            var mdparameter = this.getView().getModel("MDPARAMETER");

            mdparameter.setProperty("/name/editable", true);
            mdparameter.setProperty("/name/value", "");
            mdparameter.setProperty("/name/state", "None");
            mdparameter.setProperty("/name/stateText", "");

            mdparameter.setProperty("/process_id/value", "");
            mdparameter.setProperty("/process_id/state", "None");
            mdparameter.setProperty("/process_id/stateText", "");


            mdparameter.setProperty("/type/value", "");
            mdparameter.setProperty("/type/state", "None");
            mdparameter.setProperty("/type/stateText", "");

            mdparameter.setProperty("/description/value", "");
            mdparameter.setProperty("/description/state", "None");
            mdparameter.setProperty("/description/stateText", "");

            mdparameter.setProperty("/name/ok", false);

        },
        /**
         * Habilita/deshabilita la edición de los datos de un registro MDPARAMETER
         * @param  {Boolean} edit "true" si habilita la edición, "false" si la deshabilita
         */
        _editRecordValues: function(edit) {

            var mdparameter = this.getView().getModel("MDPARAMETER");
            mdparameter.setProperty("/name/editable", edit);
            mdparameter.setProperty("/description/editable", edit);
            mdparameter.setProperty("/process_id/editable", edit);
            mdparameter.setProperty("/measure_id/editable", edit);
            mdparameter.setProperty("/type/editable", edit);

        },
        /**
         * Se define un campo como obligatorio o no, de un registro MDPARAMETER
         * @param  {Boolean} edit "true" si es campo obligatorio, "false" si no es obligatorio
         */
        _editRecordRequired: function(edit) {
            var mdparameter = this.getView().getModel("MDPARAMETER");
            mdparameter.setProperty("/name/required", edit);
            mdparameter.setProperty("/description/required", edit);
            mdparameter.setProperty("/process_id/required", edit);
            mdparameter.setProperty("/measure_id/required", edit);
            mdparameter.setProperty("/type/required", edit);

        },
        /**
         * Navega a la vista para crear un nuevo registro
         * @param  {Event} oEvent Evento que llamó esta función
         */
        onNewRecord: function(oEvent) {
            this.getRouter().navTo("mdparameter_Create", {}, false /*create history*/ );
        },
        /**
         * Cancela la creación de un registro MDPARAMETER, y regresa a la vista principal
         * @param  {Event} oEvent Evento que llamó esta función
         */
        onCancelCreate: function(oEvent) {
            this._resetRecordValues();
            this.onNavBack(oEvent);
        },
        /**
         * Regresa a la vista principal de la entidad seleccionada actualmente
         * @param  {Event} oEvent Evento que llamó esta función
         */
        onNavBack: function(oEvent) {
            /** @type {JSONModel} OS Referencia al modelo "OS" */
            var util = this.getView().getModel("util");

            this.getRouter().navTo(util.getProperty("/selectedEntity"), {}, false /*create history*/ );
        },
        /**
         * Solicita al servicio correspondiente crear un registro MDPARAMETER
         * @param  {Event} oEvent Evento que llamó esta función
         */
        onCreate: function(oEvent) {
            //Si el registro que se desea crear es válido
            if (this._validRecord()) {
                /**
                 * @type {JSONModel} MDPARAMETER   Referencia al modelo "MDPARAMETER"
                 * @type {JSONModel} util    Referencia al modelo "util"
                 * @type {Controller} that    Referencia a este controlador
                 * @type {Object} json        Objeto a enviar en la solicitud
                 * @type {Object} settings    Opciones de la llamada a la función ajax
                 */
                var that = this;
                var util = this.getView().getModel("util");
                var mdparameter = this.getView().getModel("MDPARAMETER");
                var mdprocess = this.getView().getModel("MDPROCESS");
                console.log(mdprocess);
                console.log(this.getView().byId("parameter_nameProcess_model"));
                var serviceUrl= util.getProperty("/serviceUrl");
                $.ajax({
                    type: "POST",
                    contentType: "application/json",
                    data: JSON.stringify({
                        "name": mdparameter.getProperty("/name/value"),
                        "description": mdparameter.getProperty("/description/value"),
                        "type": mdparameter.getProperty("/type/value"),
                        "measure_id": mdparameter.getProperty("/measure_id/value"),
                        "process_id": mdparameter.getProperty("/process_id/value")
                    }),
                    url: serviceUrl+"/parameter/",
                    dataType: "json",
                    async: true,
                    success: function(data) {
                        util.setProperty("/busy/", false);
                        that._resetRecordValues();
                        that.onToast(that.getI18n().getText("OS.recordCreated"));
                        that.getRouter().navTo("mdparameter", {}, true /*no history*/ );

                    },
                    error: function(error) {
                        that.onToast("Error: " + error.responseText);
                        console.log("Read failed ");
                    }
                });
                console.log(mdparameter.getProperty("/measure_id/value"), mdparameter.getProperty("/process_id/value"), mdparameter.getProperty("/type/value"));
            }
        },
        updateProcessModel: function(){
            var mdparameter = this.getView().getModel("MDPARAMETER");
            mdparameter.setProperty("/process_id/value",this.getView().byId("parameter_nameProcess_model").getSelectedKey());
            console.log(mdparameter.getProperty("/process_id/value"));
        },
        updateMeasureModel: function(){
            var mdparameter = this.getView().getModel("MDPARAMETER");
            console.log(this.getView().byId("parameter_measure_model").getSelectedKey());
            mdparameter.setProperty("/measure_id/value",this.getView().byId("parameter_measure_model").getSelectedKey());

        },
        /**
         * Valida la correctitud de los datos existentes en el formulario del registro
         * @return {Boolean} Devuelve "true" si los datos son correctos, y "false" si son incorrectos
         */
        _validRecord: function() {
            /**
             * @type {JSONModel} MDPARAMETER Referencia al modelo "MDPARAMETER"
             * @type {Boolean} flag "true" si los datos son válidos, "false" si no lo son
             */
            var mdparameter = this.getView().getModel("MDPARAMETER"),
                flag = true,
                that = this,
                Without_SoL = /^\d+$/,
                Without_Num = /^[a-zA-Z\s]*$/;

            if (mdparameter.getProperty("/name/value") === "") {
                flag = false;
                mdparameter.setProperty("/name/state", "Error");
                mdparameter.setProperty("/name/stateText", this.getI18n().getText("enter.FIELD"));
            } else if (!Without_Num.test(mdparameter.getProperty("/order_/value"))) {
                flag = false;
                mdparameter.setProperty("/name/state", "Error");
                mdparameter.setProperty("/name/stateText", this.getI18n().getText("enter.FIELD.WN"));
            } else {
                mdparameter.setProperty("/name/state", "None");
                mdparameter.setProperty("/name/stateText", "");
            }


            if (mdparameter.getProperty("/description/value") === "") {
                flag = false;
                mdparameter.setProperty("/description/state", "Error");
                mdparameter.setProperty("/description/stateText", this.getI18n().getText("enter.FIELD"));
            }else {
                mdparameter.setProperty("/description/state", "None");
                mdparameter.setProperty("/description/stateText", "");
            }

            
            if (mdparameter.getProperty("/type/value") === "") {
                flag = false;
                mdparameter.setProperty("/type/state", "Error");
                mdparameter.setProperty("/type/stateText", this.getI18n().getText("enter.FIELD"));
            }else {
                mdparameter.setProperty("/type/state", "None");
                mdparameter.setProperty("/type/stateText", "");
            }



            return flag;
        },
        /**
         * Visualiza los detalles de un registro MDPARAMETER
         * @param  {Event} oEvent Evento que llamó esta función
         */
        onViewParameterRecord: function(oEvent) {
            this._resetRecordValues();
            var mdparameter = this.getView().getModel("MDPARAMETER");
            var mdprocess = this.getView().getModel("MDPROCESS");
            var create = false;
            this.loadProcess(create);
            this.loadMeasure(create);
            mdparameter.setProperty("/save/", false);
            mdparameter.setProperty("/cancel/", false);
            mdparameter.setProperty("/selectedRecordPath/", oEvent.getSource().getBindingContext("MDPARAMETER"));
            mdparameter.setProperty("/parameter_id/value", oEvent.getSource().getBindingContext("MDPARAMETER").getObject().parameter_id);
            mdparameter.setProperty("/name/value", oEvent.getSource().getBindingContext("MDPARAMETER").getObject().name);
            mdparameter.setProperty("/name/excepcion", oEvent.getSource().getBindingContext("MDPARAMETER").getObject().name);
            mdparameter.setProperty("/description/value", oEvent.getSource().getBindingContext("MDPARAMETER").getObject().description);
            mdparameter.setProperty("/type/value", oEvent.getSource().getBindingContext("MDPARAMETER").getObject().type);
            mdparameter.setProperty("/measure_id/value", oEvent.getSource().getBindingContext("MDPARAMETER").getObject().measure_id);
            mdparameter.setProperty("/process_id/value", oEvent.getSource().getBindingContext("MDPARAMETER").getObject().process_id);
            mdprocess.setProperty("/process_id/value", oEvent.getSource().getBindingContext("MDPARAMETER").getObject().process_id);
            mdparameter.setProperty("/name/ok", true);
            console.log(mdparameter);
            this.getRouter().navTo("mdparameter_Record", {}, false /*create history*/ );
        },
        /**
        *   Carga la lista de procesos del formulario
        *
        */
        loadProcess: function(){
            var mdprocess = this.getView().getModel("MDPROCESS");
            var util = this.getView().getModel("util");
            var serviceUrl= util.getProperty("/serviceUrl");
            var settings = {
                url: serviceUrl+"/process/",
                method: "GET",
                success: function(res) {
                    mdprocess.setProperty("/records",res.data);
                    console.log(res.data);
                    
                    console.log("entre al vacio");
                },
                error: function(err) {
                    console.log(err);
                    util.setProperty("/error/status", err.status);
                    util.setProperty("/error/statusText", err.statusText);
                }
            };

            //util.setProperty("/busy/", true);
            //borra los registros MDPROCESS que estén almacenados actualmente
            mdprocess.setProperty("/records/", []);
            //realiza la llamada ajax
            $.ajax(settings);

        },
        loadProcessForC: function(){
            var mdprocess = this.getView().getModel("MDPROCESS"),
                mdparameter = this.getModel("MDPARAMETER"),
                mdprocess = this.getView().getModel("MDPROCESS");
            var util = this.getView().getModel("util");
            var serviceUrl= util.getProperty("/serviceUrl");
            var settings = {
                url: serviceUrl+"/process/",
                method: "GET",
                success: function(res) {
                    mdprocess.setProperty("/records",res.data);
                    console.table(res.data);
                    mdparameter.setProperty("/process_id/value",mdprocess.getProperty("/records")[0].process_id);
                    console.log("entre al vacio");
                },
                error: function(err) {
                    console.log(err);
                    util.setProperty("/error/status", err.status);
                    util.setProperty("/error/statusText", err.statusText);
                }
            };

            //util.setProperty("/busy/", true);
            //borra los registros MDPROCESS que estén almacenados actualmente
            mdprocess.setProperty("/records/", []);
            //realiza la llamada ajax
            $.ajax(settings);

        },
        /**
         * Coincidencia de ruta para acceder a los detalles de un registro
         * @param  {Event} oEvent Evento que llamó esta función
         */
        _onRecordMatched: function(oEvent) {
            this._viewOptions();
        },
        /**
         * Cambia las opciones de visualización disponibles en la vista de detalles de un registro
         */
        _viewOptions: function() {
            var mdparameter = this.getView().getModel("MDPARAMETER");
            mdparameter.setProperty("/cancel/", false);
            mdparameter.setProperty("/modify/", true);
            mdparameter.setProperty("/delete/", true);
            mdparameter.setProperty("/save/", false);
            this._editRecordValues(false);
            this._editRecordRequired(false);

        },
        /**
         * Ajusta la vista para editar los datos de un registro
         * @param  {Event} oEvent Evento que llamó esta función
         */
        onEdit: function(oEvent) {

            var mdparameter = this.getView().getModel("MDPARAMETER");
            mdparameter.setProperty("/save/", true);
            mdparameter.setProperty("/cancel/", true);
            mdparameter.setProperty("/modify/", false);
            mdparameter.setProperty("/delete/", false);
            this._editRecordRequired(true);
            this._editRecordValues(true);
        },

        /**
         * Cancela la edición de un registro MDPARAMETER
         * @param  {Event} oEvent Evento que llamó esta función
         */
        onCancelEdit: function(oEvent) {
            /** @type {JSONModel} MDPARAMETER  Referencia al modelo MDPARAMETER */

            this.onView();
        },
        /**
         * Ajusta la vista para visualizar los datos de un registro
         */
        onView: function() {
            this._viewOptions();
        },
        /**
         * Solicita al servicio correspondiente actualizar un registro MDPARAMETER
         * @param  {Event} oEvent Evento que llamó esta función
         */
        onUpdate: function(oEvent) {
            /**
             * Si el registro que se quiere actualizar es válido y hubo algún cambio
             * con respecto a sus datos originales
             */
            if (this._validRecord() && this._recordChanged()) {
                /**
                 * @type {JSONModel} MDPARAMETER   Referencia al modelo "MDPARAMETER"
                 * @type {JSONModel} util         Referencia al modelo "util"
                 * @type {Controller} that         Referencia a este controlador
                 */
                var mdparameter = this.getView().getModel("MDPARAMETER");
                console.log(mdparameter);
                var util = this.getView().getModel("util");
                var that = this;
                var serviceUrl= util.getProperty("/serviceUrl");
                $.ajax({
                    type: "PUT",
                    contentType: "application/json",
                    data: JSON.stringify({
                        "parameter_id": mdparameter.getProperty("/parameter_id/value"),
                        "name": mdparameter.getProperty("/name/value"),
                        "description": mdparameter.getProperty("/description/value"),
                        "type": mdparameter.getProperty("/type/value"),
                        "measure_id": mdparameter.getProperty("/measure_id/value"),
                        "process_id": mdparameter.getProperty("/process_id/value")
                    }),
                    url: serviceUrl+"/parameter/",
                    dataType: "json",
                    async: true,
                    success: function(data) {

                        util.setProperty("/busy/", false);
                        that._resetRecordValues();
                        that._viewOptions();
                        that.onToast(that.getI18n().getText("OS.recordUpdated"));
                        that.getRouter().navTo("mdparameter", {}, true /*no history*/ );

                    },
                    error: function(request, status, error) {
                        that.onToast("Error de comunicación");
                        console.log("Read failed");
                    }
                });
            }
        },
        /**
         * Verifica si el registro seleccionado tiene algún cambio con respecto a sus valores originales
         * @return {Boolean} Devuelve "true" el registro cambió, y "false" si no cambió
         */
        _recordChanged: function() {
            /**
             * @type {JSONModel} MDPARAMETER         Referencia al modelo "MDPARAMETER"
             * @type {Boolean} flag            "true" si el registro cambió, "false" si no cambió
             */
            var mdparameter = this.getView().getModel("MDPARAMETER"),
                mdprocess = this.getView().getModel("MDPROCESS"),
                flag = false;

            if (mdparameter.getProperty("/name/value") !== mdparameter.getProperty(mdparameter.getProperty("/selectedRecordPath/") + "/name")) {
                flag = true;
            }

            if (mdparameter.getProperty("/description/value") !== mdparameter.getProperty(mdparameter.getProperty("/selectedRecordPath/") + "/description")) {
                flag = true;
            }


            if (mdparameter.getProperty("/type/value") !== mdparameter.getProperty(mdparameter.getProperty("/selectedRecordPath/") + "/type")) {
                flag = true;
            }

            if (mdparameter.getProperty("/measure_id/value") !== mdparameter.getProperty(mdparameter.getProperty("/selectedRecordPath/") + "/measure_id")) {
                flag = true;
            }

            if (mdparameter.getProperty("/process_id/value") !== mdparameter.getProperty(mdparameter.getProperty("/selectedRecordPath/") + "/process_id")) {
                flag = true;
            }


            if(!flag) this.onToast("No se detectaron cambios");

            return flag;
        },
        onDialogFilters: function(oEvent) {
            console.log("Filter");
            if (!this._oDialog) {
                this._oDialog = sap.ui.xmlfragment("technicalConfiguration.view.parameter.mdparameter_Filter", this);
            }

            jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialog);
            this._oDialog.open();
        },

        handleConfirm: function(oEvent) {

            var oView = this.getView();
            var oTable = oView.byId("parameterTable");

            var mParams = oEvent.getParameters();
            var oBinding = oTable.getBinding("items");

            //apply sorted
            var aSorters = [];
            var sPath = mParams.sortItem.getKey();
            var bDescending = mParams.sortDescending;
            aSorters.push(new Sorter(sPath, bDescending));
            oBinding.sort(aSorters);

            // apply filters to binding
            var aFilters = [];
            jQuery.each(mParams.filterItems, function(i, oItem) {
                var aSplit = oItem.getKey().split("___");
                var sPath = aSplit[0];
                var sOperator = aSplit[1];
                var sValue1 = aSplit[2];
                var sValue2 = aSplit[3];
                var oFilter = new Filter(sPath, sOperator, sValue1, sValue2);
                aFilters.push(oFilter);
            });
            console.log(aFilters);
            oBinding.filter(aFilters);

        },
        onRefresh: function(oEvent) {
            var oView = this.getView();
            var oTable = oView.byId("parameterTable");
            var oBinding = oTable.getBinding("items");
            oBinding.filter([]);
        },
        onParameterSearch: function(oEvent){
            var aFilters = [],
                sQuery = oEvent.getSource().getValue(),
                binding = this.getView().byId("parameterTable").getBinding("items");

            if (sQuery && sQuery.length > 0) {
                /** @type {Object} filter1 Primer filtro de la búsqueda */
                var filter1 = new sap.ui.model.Filter("name", sap.ui.model.FilterOperator.Contains, sQuery);
                aFilters = new sap.ui.model.Filter([filter1]);
            }

            //se actualiza el binding de la lista
            binding.filter(aFilters);

        },
        onConfirmDelete: function(oEvent){

            var oBundle = this.getView().getModel("i18n").getResourceBundle();
            var deleteRecord = oBundle.getText("deleteRecord");
            var confirmation = oBundle.getText("confirmation");
            var util = this.getView().getModel("util");

            var that = this;
            var dialog = new Dialog({
                title: confirmation,
                type: "Message",
                content: new sap.m.Text({
                    text: deleteRecord
                }),

                beginButton: new Button({
                    text: "Si",
                    press: function() {
                        util.setProperty("/busy/", true);
                        console.log(util);
                        var mdparameter = that.getView().getModel("MDPARAMETER");
                        console.log("Eliminar: "+mdparameter.getProperty("/parameter_id/value"));
                        var serviceUrl= util.getProperty("/serviceUrl");
                        $.ajax({
                            type: "DELETE",
                            contentType: "application/json",
                            data: JSON.stringify({
                                "parameter_id": mdparameter.getProperty("/parameter_id/value")
                            }),
                            url: serviceUrl+"/parameter/",
                            dataType: "json",
                            async: true,
                            success: function(data) {

                                util.setProperty("/busy/", false);
                                that.getRouter().navTo("mdparameter", {}, true);
                                dialog.close();
                                dialog.destroy();

                            },
                            error: function(request, status, error) {
                                that.onToast("Error de comunicación");
                                console.log("Read failed");
                            }
                        });

                    }
                }),
                endButton: new Button({
                    text: "No",
                    press: function() {
                        dialog.close();
                        dialog.destroy();
                    }
                })
            });

            dialog.open();
        },

        changeName: function(oEvent){
            let input= oEvent.getSource();
            input.setValue(input.getValue().trim());
            let model= this.getModel("MDPARAMETER");

            let excepcion= model.getProperty("/name/excepcion");
            this.checkChange(input.getValue().toString(), excepcion.toString(), "/name", "changeName");
        },


        checkChange: function(name, excepcion,field, funct){

            let util = this.getModel("util");
            let model= this.getModel("MDPARAMETER");
            let serverName  = "/parameter/"+funct;
            // console.log(serverName);
            console.log("check: ", name, excepcion);
            if (name=="" || name===null){
                model.setProperty(field+"/state", "None");
                model.setProperty(field+"/stateText", "");
                model.setProperty(field+"/ok", false);
            }
            else{
                fetch(serverName, {
                    method: "POST",
                    headers: { "Content-Type": "application/json" },
                    body: JSON.stringify({
                        name: name,
                        diff: excepcion
                    })
                })
                    .then(function(response) {
                        if (response.status !== 200) {
                            console.log("Looks like there was a problem. Status Code: " +
                        response.status);
                            return;
                        }
                        response.json().then(function(res) {
                            if(res.data.length>0){
                                console.log("false");
                                model.setProperty(field+"/state", "Error");
                                model.setProperty(field+"/stateText", "nombre repetido");
                                model.setProperty(field+"/ok", false);
                            }
                            else{
                                console.log("true");

                                model.setProperty(field+"/state", "Success");
                                model.setProperty(field+"/stateText", "");
                                model.setProperty(field+"/ok", true);
                            }
                        });
                    })
                    .catch(function(err) {
                        console.log("Fetch Error: ", err);
                    });
            }
            
        },




    });
});
