sap.ui.define([
    "technicalConfiguration/controller/BaseController",
    "sap/ui/model/json/JSONModel",
    "sap/m/Dialog",
    "sap/m/Text",
    "sap/m/Button"
], function(BaseController, JSONModel, Dialog, Text, Button) {
    "use strict";

    return BaseController.extend("technicalConfiguration.controller.mdproduct", {

        onInit: function() {
            //ruta para la vista principal
            this.getOwnerComponent().getRouter().getRoute("mdproduct").attachPatternMatched(this._onRouteMatched, this);
            //ruta para la vista de detalles de un registro
            this.getOwnerComponent().getRouter().getRoute("mdproduct_Record").attachPatternMatched(this._onRecordMatched, this);
            //ruta para la vista de creación de un registro
            this.getOwnerComponent().getRouter().getRoute("mdproduct_Create").attachPatternMatched(this._onCreateMatched, this);
        },

        _onRouteMatched: function(oEvent) {
            /**
             * @type {Controller} that         Referencia a este controlador
             * @type {JSONModel} util         Referencia al modelo "util"
             * @type {JSONModel} OS            Referencia al modelo "OS"
             * @type {JSONModel} DRIVER        Referencia al modelo "MDPRODUCT"
             */

            var that = this,
                util = this.getView().getModel("util"),
                mdproduct = this.getView().getModel("MDPRODUCT");

            //dependiendo del dispositivo, establece la propiedad "phone"
            this.getView().getModel("util").setProperty("/phone/",
                this.getOwnerComponent().getContentDensityClass() === "sapUiSizeCozy");

            //establece DRIVER como la entidad seleccionada
            util.setProperty("/selectedEntity/", "mdproduct");


            //obtiene los registros de MDPRODUCT
            this.onRead(that, util, mdproduct);
        },
        /**
         * Obtiene todos los registros de MDPRODUCT
         * @param  {Controller} that         Referencia al controlador que llama esta función
         * @param  {JSONModel} util         Referencia al modelo "util"
         * @param  {JSONModel} MDPRODUCT Referencia al modelo "MDPRODUCT"
         */
        onRead: function(that, util, mdproduct) {
            /** @type {Object} settings opciones de la llamada a la función ajax */
            var serviceUrl= util.getProperty("/serviceUrl");
            var settings = {
                url: serviceUrl+"/product",
                method: "GET",
                success: function(res) {
                    console.log(res.data);
                    util.setProperty("/busy/", false);
                    mdproduct.setProperty("/records/", res.data);
                    console.log(mdproduct);
                },
                error: function(err) {
                    util.setProperty("/error/status", err.status);
                    util.setProperty("/error/statusText", err.statusText);
                    //that.onConnectionError();
                }
            };
            console.log(util);
            util.setProperty("/busy/", true);
            //borra los registros OSPARTNERSHIP que estén almacenados actualmente
            mdproduct.setProperty("/records/", []);
            //realiza la llamada ajax
            $.ajax(settings);
        },
        /**
         * Coincidencia de ruta para acceder a la creación de un registro
         * @param  {Event} oEvent Evento que llamó esta función
         */
        _onCreateMatched: function(oEvent) {

            this._resetRecordValues();
            this._editRecordValues(true);
            this._editRecordRequired(true);
        },
        /**
         * Resetea todos los valores existentes en el formulario del registro
         */
        _resetRecordValues: function() {
            /**
             * @type {JSONModel} MDPRODUCT Referencia al modelo "MDPRODUCT"
             */
            var mdproduct = this.getView().getModel("MDPRODUCT");

            mdproduct.setProperty("/_ID/value", "");

            
            mdproduct.setProperty("/name/value", "");
            mdproduct.setProperty("/name/state", "None");
            mdproduct.setProperty("/name/stateText", "");

            mdproduct.setProperty("/code/value", "");
            mdproduct.setProperty("/code/state", "None");
            mdproduct.setProperty("/code/stateText", "");

            mdproduct.setProperty("/name/ok", false);
            mdproduct.setProperty("/code/ok", false);

        },
        /**
         * Habilita/deshabilita la edición de los datos de un registro MDPRODUCT
         * @param  {Boolean} edit "true" si habilita la edición, "false" si la deshabilita
         */
        _editRecordValues: function(edit) {

            var mdproduct = this.getView().getModel("MDPRODUCT");
            mdproduct.setProperty("/name/editable", edit);
            mdproduct.setProperty("/code/editable", edit);

        },
        /**
         * Se define un campo como obligatorio o no, de un registro MDPRODUCT
         * @param  {Boolean} edit "true" si es campo obligatorio, "false" si no es obligatorio
         */
        _editRecordRequired: function(edit) {
            var mdproduct = this.getView().getModel("MDPRODUCT");
            mdproduct.setProperty("/name/required", edit);
            mdproduct.setProperty("/code/required", edit);

        },
        /**
         * Navega a la vista para crear un nuevo registro
         * @param  {Event} oEvent Evento que llamó esta función
         */
        onNewRecord: function(oEvent) {
            this.getRouter().navTo("mdproduct_Create", {}, false /*create history*/ );
        },
        /**
         * Cancela la creación de un registro MDPRODUCT, y regresa a la vista principal
         * @param  {Event} oEvent Evento que llamó esta función
         */
        onCancelCreate: function(oEvent) {
            this._resetRecordValues();
            this.onNavBack(oEvent);
        },
        /**
         * Regresa a la vista principal de la entidad seleccionada actualmente
         * @param  {Event} oEvent Evento que llamó esta función
         */
        onNavBack: function(oEvent) {
            /** @type {JSONModel} OS Referencia al modelo "OS" */
            var util = this.getView().getModel("util");

            this.getRouter().navTo(util.getProperty("/selectedEntity"), {}, false /*create history*/ );
        },
        /**
         * Solicita al servicio correspondiente crear un registro DRIVER
         * @param  {Event} oEvent Evento que llamó esta función
         */
        onCreate: function(oEvent) {
            //Si el registro que se desea crear es válido
            if (this._validRecord()) {
                /**
                 * @type {JSONModel} MDPRODUCT   Referencia al modelo "MDPRODUCT"
                 * @type {JSONModel} util    Referencia al modelo "util"
                 * @type {Controller} that    Referencia a este controlador
                 * @type {Object} json        Objeto a enviar en la solicitud
                 * @type {Object} settings    Opciones de la llamada a la función ajax
                 */
                console.log(serviceUrl);
                var that = this;
                var util = this.getView().getModel("util");
                var mdproduct = this.getView().getModel("MDPRODUCT");
                var serviceUrl= util.getProperty("/serviceUrl");
                $.ajax({
                    type: "POST",
                    contentType: "application/json",
                    data: JSON.stringify({
                        "name": mdproduct.getProperty("/name/value"),
                        "code": mdproduct.getProperty("/code/value")
                    }),
                    url: serviceUrl+"/product/",
                    dataType: "json",
                    async: true,
                    success: function(data) {
                        util.setProperty("/busy/", false);
                        that._resetRecordValues();
                        that.onToast(that.getI18n().getText("OS.recordCreated"));
                        that.getRouter().navTo("mdproduct", {}, true /*no history*/ );

                    },
                    error: function(error) {
                        that.onToast("Error: " + error.responseText);
                        console.log("Read failed ");
                    }
                });

            }
        },
        /**
         * Valida la correctitud de los datos existentes en el formulario del registro
         * @return {Boolean} Devuelve "true" si los datos son correctos, y "false" si son incorrectos
         */
        _validRecord: function() {
            /**
             * @type {JSONModel} MDPRODUCT Referencia al modelo "MDPRODUCT"
             * @type {Boolean} flag "true" si los datos son válidos, "false" si no lo son
             */
            var mdproduct = this.getView().getModel("MDPRODUCT"),
                flag = true,
                eDriver = false,
                that = this,
                Without_SoL = /^\d+$/,
                Without_Num = /^[a-zA-Z\s]*$/;

            if (mdproduct.getProperty("/name/value") === "") {
                flag = false;
                mdproduct.setProperty("/name/state", "Error");
                mdproduct.setProperty("/name/stateText", this.getI18n().getText("enter.FIELD"));
            } else if (!Without_Num.test(mdproduct.getProperty("/order_/value"))) {
                flag = false;
                mdproduct.setProperty("/name/state", "Error");
                mdproduct.setProperty("/name/stateText", this.getI18n().getText("enter.FIELD.WN"));
            } else {
                mdproduct.setProperty("/name/state", "None");
                mdproduct.setProperty("/name/stateText", "");
            }
            if (mdproduct.getProperty("/code/value") === "") {
                flag = false;
                mdproduct.setProperty("/code/state", "Error");
                mdproduct.setProperty("/code/stateText", this.getI18n().getText("enter.FIELD"));
            } else if (!Without_Num.test(mdproduct.getProperty("/order_/value"))) {
                flag = false;
                mdproduct.setProperty("/code/state", "Error");
                mdproduct.setProperty("/code/stateText", this.getI18n().getText("enter.FIELD.WN"));
            } else {
                mdproduct.setProperty("/code/state", "None");
                mdproduct.setProperty("/code/stateText", "");
            }

            return flag;
        },
        /**
         * Visualiza los detalles de un registro MDPRODUCT
         * @param  {Event} oEvent Evento que llamó esta función
         */
        onViewProductRecord: function(oEvent) {
            this._resetRecordValues();
            var mdproduct = this.getView().getModel("MDPRODUCT");
            mdproduct.setProperty("/save/", false);
            mdproduct.setProperty("/cancel/", false);
            mdproduct.setProperty("/selectedRecordPath/", oEvent.getSource().getBindingContext("MDPRODUCT"));
            mdproduct.setProperty("/_ID/value", oEvent.getSource().getBindingContext("MDPRODUCT").getObject().product_id);
            mdproduct.setProperty("/product_id/value", oEvent.getSource().getBindingContext("MDPRODUCT").getObject().product_id);
            mdproduct.setProperty("/name/value", oEvent.getSource().getBindingContext("MDPRODUCT").getObject().name);
            mdproduct.setProperty("/name/excepcion", oEvent.getSource().getBindingContext("MDPRODUCT").getObject().name);
            mdproduct.setProperty("/code/value", oEvent.getSource().getBindingContext("MDPRODUCT").getObject().code);
            mdproduct.setProperty("/code/excepcion", oEvent.getSource().getBindingContext("MDPRODUCT").getObject().code);
            this.getRouter().navTo("mdproduct_Record", {}, false /*create history*/ );

            mdproduct.setProperty("/name/ok", true);
            mdproduct.setProperty("/code/ok", true);
        },
        /**
         * Coincidencia de ruta para acceder a los detalles de un registro
         * @param  {Event} oEvent Evento que llamó esta función
         */
        _onRecordMatched: function(oEvent) {
            this._viewOptions();
        },
        /**
         * Cambia las opciones de visualización disponibles en la vista de detalles de un registro
         */
        _viewOptions: function() {
            var mdproduct = this.getView().getModel("MDPRODUCT");
            mdproduct.setProperty("/cancel/", false);
            mdproduct.setProperty("/modify/", true);
            mdproduct.setProperty("/delete/", true);
            mdproduct.setProperty("/save/", false);
            this._editRecordValues(false);
            this._editRecordRequired(false);

        },
        /**
         * Ajusta la vista para editar los datos de un registro
         * @param  {Event} oEvent Evento que llamó esta función
         */
        onEdit: function(oEvent) {

            var mdproduct = this.getView().getModel("MDPRODUCT");
            mdproduct.setProperty("/save/", true);
            mdproduct.setProperty("/cancel/", true);
            mdproduct.setProperty("/modify/", false);
            mdproduct.setProperty("/delete/", false);
            this._editRecordRequired(true);
            this._editRecordValues(true);
        },

        /**
         * Cancela la edición de un registro MDPRODUCT
         * @param  {Event} oEvent Evento que llamó esta función
         */
        onCancelEdit: function(oEvent) {
            /** @type {JSONModel} MDPRODUCT  Referencia al modelo MDPRODUCT */

            this.onView();
        },
        /**
         * Ajusta la vista para visualizar los datos de un registro
         */
        onView: function() {
            this._viewOptions();
        },
        /**
         * Solicita al servicio correspondiente actualizar un registro MDPRODUCT
         * @param  {Event} oEvent Evento que llamó esta función
         */
        onUpdate: function(oEvent) {
            /**
             * Si el registro que se quiere actualizar es válido y hubo algún cambio
             * con respecto a sus datos originales
             */
            if (this._validRecord() && this._recordChanged()) {
                /**
                 * @type {JSONModel} MDPRODUCT       Referencia al modelo "MDPRODUCT"
                 * @type {JSONModel} util         Referencia al modelo "util"
                 * @type {Controller} that         Referencia a este controlador
                 */
                var mdproduct = this.getView().getModel("MDPRODUCT");
                var util = this.getView().getModel("util");
                var that = this;
                var serviceUrl= util.getProperty("/serviceUrl");
                $.ajax({
                    type: "PUT",
                    contentType: "application/json",
                    data: JSON.stringify({
                        "product_id": mdproduct.getProperty("/_ID/value"),
                        "name": mdproduct.getProperty("/name/value"),
                        "code": mdproduct.getProperty("/code/value")
                    }),
                    url: serviceUrl+"/product/",
                    dataType: "json",
                    async: true,
                    success: function(data) {

                        util.setProperty("/busy/", false);
                        that._resetRecordValues();
                        that._viewOptions();
                        that.onToast(that.getI18n().getText("OS.recordUpdated"));
                        that.getRouter().navTo("mdproduct", {}, true /*no history*/ );

                    },
                    error: function(request, status, error) {
                        that.onToast("Error de comunicación");
                        console.log("Read failed");
                    }
                });
            }
        },
        /**
         * Verifica si el registro seleccionado tiene algún cambio con respecto a sus valores originales
         * @return {Boolean} Devuelve "true" el registro cambió, y "false" si no cambió
         */
        _recordChanged: function() {
            /**
             * @type {JSONModel} MDPRODUCT         Referencia al modelo "MDPRODUCT"
             * @type {Boolean} flag            "true" si el registro cambió, "false" si no cambió
             */
            var mdproduct = this.getView().getModel("MDPRODUCT"),
                flag = false;

            if (mdproduct.getProperty("/name/value") !== mdproduct.getProperty(mdproduct.getProperty("/selectedRecordPath/") + "/name")) {
                flag = true;
            }

            if (mdproduct.getProperty("/code/value") !== mdproduct.getProperty(mdproduct.getProperty("/selectedRecordPath/") + "/code")) {
                flag = true;
            }


            if(!flag) this.onToast("No se detectaron cambios");

            return flag;
        },
        onProductSearch: function(oEvent){
            var aFilters = [],
                sQuery = oEvent.getSource().getValue(),
                binding = this.getView().byId("productTable").getBinding("items");

            if (sQuery && sQuery.length > 0) {
                /** @type {Object} filter1 Primer filtro de la búsqueda */
                var filter1 = new sap.ui.model.Filter("name", sap.ui.model.FilterOperator.Contains, sQuery);
                aFilters = new sap.ui.model.Filter([filter1]);
            }

            //se actualiza el binding de la lista
            binding.filter(aFilters);

        },

        onVerifyIsUsed: async function(product_id){
            let ret;
        
            const response = await fetch("/product/isBeingUsed", {
                headers: {
                    "Content-Type": "application/json"
                },
                method: "POST",
                body: JSON.stringify({
                    product_id: product_id
                })
            });
        
            if (response.status !== 200 && response.status !== 409) {
                console.log("Looks like there was a problem. Status Code: " +
                    response.status);
                return;
            }
            if(response.status === 200){
                const res = await response.json();
                console.log(res);
                ret = res.data.used;
            }
            console.log(response);
            console.log(ret);
            return ret;
            
        },

        onConfirmDelete: async function(oEvent){

            let oBundle = this.getView().getModel("i18n").getResourceBundle(),
                deleteRecord = oBundle.getText("deleteRecord"),
                confirmation = oBundle.getText("confirmation"),
                util = this.getView().getModel("util"),
                mdproduct = this.getView().getModel("MDPRODUCT"),
                product_id = mdproduct.getProperty("/product_id/value"),
                that = this;


            let cond = await this.onVerifyIsUsed(product_id);
            console.log("La cond el mio: ", cond);
            if(cond){
                var dialog = new Dialog({
                    title: "Información",
                    type: "Message",
                    state: "Warning",
                    content: new Text({
                        text: "No se puede eliminar el Producto, porque está siendo utilizado."
                    }),
                    beginButton: new Button({
                        text: "OK",
                        press: function() {
                            dialog.close();
                            that.confirmDeleteDlg.close();
                        }
                    }),
                    afterClose: function() {
                        dialog.destroy();
                    }
                });
        
                dialog.open();
            }else{
                var dialog = new Dialog({
                    title: confirmation,
                    type: "Message",
                    content: new sap.m.Text({
                        text: deleteRecord
                    }),

                    beginButton: new Button({
                        text: "Si",
                        press: function() {
                            util.setProperty("/busy/", true);
                            console.log(util);
                                
                            var serviceUrl= util.getProperty("/serviceUrl");
                            $.ajax({
                                type: "DELETE",
                                contentType: "application/json",
                                data: JSON.stringify({
                                    "product_id": product_id
                                }),
                                url: serviceUrl+"/product/",
                                dataType: "json",
                                async: true,
                                success: function(data) {

                                    util.setProperty("/busy/", false);
                                    that.getRouter().navTo("mdproduct", {}, true);
                                    dialog.close();
                                    dialog.destroy();

                                },
                                error: function(request, status, error) {
                                    that.onToast("Error de comunicación");
                                    console.log("Read failed");
                                }
                            });

                        }
                    }),
                    endButton: new Button({
                        text: "No",
                        press: function() {
                            dialog.close();
                            dialog.destroy();
                        }
                    })
                });

                dialog.open();
            }  
        },

        changeName: function(oEvent){
            let input= oEvent.getSource();
            input.setValue(input.getValue().trim());
            let mdModel= this.getModel("MDPRODUCT");
            let excepcion= mdModel.getProperty("/name/excepcion");

            this.checkChange(input.getValue().toString(), excepcion.toString(), "/name", "changeName");
        },

        changeCode: function(oEvent){
            let input= oEvent.getSource();
            input.setValue(input.getValue().trim());
            let mdModel= this.getModel("MDPRODUCT");
            let excepcion= mdModel.getProperty("/code/excepcion");

            this.checkChange(input.getValue().toString(), excepcion.toString(), "/code", "changeCode");
        },

        checkChange: function(name, excepcion,field, funct){
            let util = this.getModel("util");
            let mdModel= this.getModel("MDPRODUCT");
            let serverName  = "/product/"+funct;
            // console.log(serverName);
            console.log("check: ", name, excepcion);
            if (name=="" || name===null){
                mdModel.setProperty(field+"/state", "None");
                mdModel.setProperty(field+"/stateText", "");
                mdModel.setProperty(field+"/ok", false);
            }
            else{
                fetch(serverName, {
                    method: "POST",
                    headers: { "Content-Type": "application/json" },
                    body: JSON.stringify({
                        name: name,
                        diff: excepcion
                    })
                })
                    .then(function(response) {
                        if (response.status !== 200) {
                            console.log("Looks like there was a problem. Status Code: " +
                        response.status);
                            return;
                        }
                        response.json().then(function(res) {
                            if(res.data.length>0){
                                mdModel.setProperty(field+"/state", "Error");
                                mdModel.setProperty(field+"/stateText", "código o nombre repetido");
                                mdModel.setProperty(field+"/ok", false);
                            
                            }
                            else{
                                mdModel.setProperty(field+"/state", "Success");
                                mdModel.setProperty(field+"/stateText", "");
                                mdModel.setProperty(field+"/ok", true);
                            }
                        });
                    })
                    .catch(function(err) {
                        console.log("Fetch Error: ", err);
                    });
            }
        }




    });
});
