sap.ui.define([
    "technicalConfiguration/controller/BaseController",
    "sap/ui/model/json/JSONModel",
    "sap/ui/model/Sorter",
    "sap/ui/model/Filter",
    "sap/m/Dialog",
    "sap/m/Button"
], function(BaseController, JSONModel,Sorter,Filter, Dialog, Button) {
    "use strict";

    return BaseController.extend("technicalConfiguration.controller.mdprocess", {

        onInit: function() {
            //ruta para la vista principal
            this.getOwnerComponent().getRouter().getRoute("mdprocess").attachPatternMatched(this._onRouteMatched, this);
            //ruta para la vista de detalles de un registro
            this.getOwnerComponent().getRouter().getRoute("mdprocess_Record").attachPatternMatched(this._onRecordMatched, this);
            //ruta para la vista de creación de un registro
            this.getOwnerComponent().getRouter().getRoute("mdprocess_Create").attachPatternMatched(this._onCreateMatched, this);
        },

        /**
         * Coincidencia de ruta para acceder a la vista principal
         * @param  {Event} oEvent Evento que llamó esta función
         */
        _onRouteMatched: function(oEvent) {
            /**
             * @type {Controller} that         Referencia a este controlador
             * @type {JSONModel} util         Referencia al modelo "util"
             * @type {JSONModel} OS            Referencia al modelo "OS"
             * @type {JSONModel} MDPROCESS        Referencia al modelo "MDPROCESS"
             */

            var that = this,
                util = this.getView().getModel("util"),
                mdprocess = this.getView().getModel("MDPROCESS");

            //this.onRefresh();
            //dependiendo del dispositivo, establece la propiedad "phone"
            this.getView().getModel("util").setProperty("/phone/",
                this.getOwnerComponent().getContentDensityClass() === "sapUiSizeCozy");

            //establece process como la entidad seleccionada
            util.setProperty("/selectedEntity/", "mdprocess");


            //obtiene los registros de MDPROCESS
            this.onRead(that, util, mdprocess);
        },
        /**
         * Obtiene todos los registros de MDPROCESS
         * @param  {Controller} that         Referencia al controlador que llama esta función
         * @param  {JSONModel} util         Referencia al modelo "util"
         * @param  {JSONModel} MDPROCESS Referencia al modelo "MDPROCESS"
         */
        onRead: function(that, util, mdprocess) {
            /** @type {Object} settings opciones de la llamada a la función ajax */
            var util = this.getView().getModel("util");
            var serviceUrl= util.getProperty("/serviceUrl");
            var settings = {
                url: serviceUrl+"/process/AllProcessJ",
                method: "GET",
                success: function(res) {
                    util.setProperty("/busy/", false);
                    mdprocess.setProperty("/records/", res.data);
                    console.log(mdprocess);
                },
                error: function(err) {
                    util.setProperty("/error/status", err.status);
                    util.setProperty("/error/statusText", err.statusText);
                    //that.onConnectionError();
                }
            };

            util.setProperty("/busy/", true);
            //borra los registros OSPARTNERSHIP que estén almacenados actualmente
            mdprocess.setProperty("/records/", []);
            //realiza la llamada ajax
            $.ajax(settings);
        },
        /**
         * Coincidencia de ruta para acceder a la creación de un registro
         * @param  {Event} oEvent Evento que llamó esta función
         */
        _onCreateMatched: function(oEvent) {
            var create = true;
            this.loadCalendars(create);
            this.loadProducts(create);
            this.loadStages(create);
            this.loadPredecessor(create);
            this.loadBreed(create);

            this._resetRecordValues();
            this._editRecordValues(true, false);
            this._editRecordRequired(true);

        },
        /**
        * Carga en el modelo TXCALENDAR todos los calendarios registrados en el sistema, los cuales han sido generados. Estatus de generado 1
        */
        loadCalendars: function(create){
            var txcalendar = this.getView().getModel("TXCALENDAR"),
                mdprocess = this.getView().getModel("MDPROCESS");
            var util = this.getView().getModel("util");
            var serviceUrl= util.getProperty("/serviceUrl");
            var settings = {
                url: serviceUrl+"/calendar/getCalendars?generated=1",
                method: "GET",
                success: function(res) {
                    txcalendar.setProperty("/records",res.calendars);
                    console.log("calendar_id = "+res.calendars[0].calendar_id);
                    if(create) mdprocess.setProperty("/calendar_id/value",res.calendars[0].calendar_id);
                },
                error: function(err) {
                    console.log(err);
                    util.setProperty("/error/status", err.status);
                    util.setProperty("/error/statusText", err.statusText);
                }
            };
            //borra los registros MDPROCESS que estén almacenados actualmente
            txcalendar.setProperty("/records/", []);
            //realiza la llamada ajax
            $.ajax(settings);

        },
        /*
        *   obtener todos los productos
        */
        loadProducts: function(create){
            var mdproduct = this.getView().getModel("MDPRODUCT"),
                mdprocess = this.getView().getModel("MDPROCESS");
            var util = this.getView().getModel("util");
            var serviceUrl= util.getProperty("/serviceUrl");
            var settings = {
                url: serviceUrl+"/product/",
                method: "GET",
                success: function(res) {
                    mdproduct.setProperty("/records",res.data);
                    console.log("product_id = "+res.data[0].product_id);
                    if(create) mdprocess.setProperty("/product_id/value",res.data[0].product_id);

                },
                error: function(err) {
                    console.log(err);
                    util.setProperty("/error/status", err.status);
                    util.setProperty("/error/statusText", err.statusText);
                }
            };
            //borra los registros MDPRODUCT que estén almacenados actualmente
            mdproduct.setProperty("/records/", []);
            //realiza la llamada ajax
            $.ajax(settings);

        },
        /*
        *   obtener todos las etapas
        */
        loadStages: function(create){
            var mdstage = this.getView().getModel("MDSTAGE"),
                mdprocess = this.getView().getModel("MDPROCESS");
            var util = this.getView().getModel("util");
            var serviceUrl= util.getProperty("/serviceUrl");
            var settings = {
                url: serviceUrl+"/stage/",
                method: "GET",
                success: function(res) {
                    mdstage.setProperty("/records",res.data);
                    console.log("stage_id = "+res.data[0].stage_id);
                    if(create) mdprocess.setProperty("/stage_id/value", res.data[0].stage_id);

                },
                error: function(err) {
                    console.log(err);
                    util.setProperty("/error/status", err.status);
                    util.setProperty("/error/statusText", err.statusText);
                }
            };
            //borra los registros MDSTAGE que estén almacenados actualmente
            mdstage.setProperty("/records/", []);
            //realiza la llamada ajax
            $.ajax(settings);

        },
        loadPredecessor: function(create){

            var mdprocess = this.getView().getModel("MDPROCESS");
            var process_id = mdprocess.getProperty("/process_id/value");
            var aPredecessor = mdprocess.getProperty("/records").slice();
            var dPredecessor = false;
            var i = 0;
            if(!create){
                while(!dPredecessor || i<aPredecessor.length-1){
                    if(aPredecessor[i].process_id === process_id){
                        aPredecessor.splice(i, 1);
                        dPredecessor = true;
                    }
                    i++;
                }
            }else if(aPredecessor.count>0){
                mdprocess.setProperty("/process_id/value", aPredecessor[0].process_id);
            }else{
                mdprocess.setProperty("/process_id/value", 0);
            }
            mdprocess.setProperty("/predecessor",aPredecessor);
        },
        loadBreed: function(create){
            var mdbreed = this.getView().getModel("MDBREED"),
                mdprocess = this.getView().getModel("MDPROCESS");
            var util = this.getView().getModel("util");
            var serviceUrl= util.getProperty("/serviceUrl");
            var settings = {
                url: serviceUrl+"/breed/",
                method: "GET",
                success: function(res) {
                    mdbreed.setProperty("/records",res.data);
                    console.log("mdbreed");
                    console.log(mdbreed);
                    //console.log('stage_id = '+res.data[0].stage_id);
                    if(create) mdprocess.setProperty("/breed_id/value", res.data[0].breed_id);
                },
                error: function(err) {
                    console.log(err);
                    util.setProperty("/error/status", err.status);
                    util.setProperty("/error/statusText", err.statusText);
                }
            };
            //borra los registros MDSTAGE que estén almacenados actualmente
            mdbreed.setProperty("/records/", []);
            //realiza la llamada ajax
            $.ajax(settings);
        },
        /**
         * Resetea todos los valores existentes en el formulario del registro
         */
        _resetRecordValues: function() {
            /**
             * @type {JSONModel} MDPROCESS Referencia al modelo "MDPROCESS"
             */
            var mdprocess = this.getView().getModel("MDPROCESS");


            mdprocess.setProperty("/name/editable", true);
            mdprocess.setProperty("/name/value", "");
            mdprocess.setProperty("/name/state", "None");
            mdprocess.setProperty("/name/stateText", "");

            mdprocess.setProperty("/process_order/value", "");
            mdprocess.setProperty("/process_order/state", "None");
            mdprocess.setProperty("/process_order/stateText", "");

            mdprocess.setProperty("/capacity/value", "");
            mdprocess.setProperty("/capacity/state", "None");
            mdprocess.setProperty("/capacity/stateText", "");

            /*
            mdprocess.setProperty("/percen_distribution/value", "");
            mdprocess.setProperty("/percen_distribution/state", "None");
            mdprocess.setProperty("/percen_distribution/stateText", "");
            */
            mdprocess.setProperty("/historical_decrease/value", "");
            mdprocess.setProperty("/historical_decrease/state", "None");
            mdprocess.setProperty("/historical_decrease/stateText", "");

            mdprocess.setProperty("/theoretical_decrease/value", "");
            mdprocess.setProperty("/theoretical_decrease/state", "None");
            mdprocess.setProperty("/theoretical_decrease/stateText", "");


            mdprocess.setProperty("/historical_weight/value", "");
            mdprocess.setProperty("/historical_weight/state", "None");
            mdprocess.setProperty("/historical_weight/stateText", "");


            mdprocess.setProperty("/theoretical_weight/value", "");
            mdprocess.setProperty("/theoretical_weight/state", "None");
            mdprocess.setProperty("/theoretical_weight/stateText", "");

            mdprocess.setProperty("/historical_duration/value", "");
            mdprocess.setProperty("/historical_duration/state", "None");
            mdprocess.setProperty("/historical_duration/stateText", "");

            mdprocess.setProperty("/theoretical_duration/value", "");
            mdprocess.setProperty("/theoretical_duration/state", "None");
            mdprocess.setProperty("/theoretical_duration/stateText", "");

            mdprocess.setProperty("/ispredecessor/value", 0);
            /*
            mdprocess.setProperty("/historical_accum_duration/value", "");
            mdprocess.setProperty("/historical_accum_duration/state", "None");
            mdprocess.setProperty("/historical_accum_duration/stateText", "");


            mdprocess.setProperty("/theoretical_accum_duration/value", "");
            mdprocess.setProperty("/theoretical_accum_duration/state", "None");
            mdprocess.setProperty("/theoretical_accum_duration/stateText", "");
            */


        },
        /**
         * Habilita/deshabilita la edición de los datos de un registro MDPROCESS
         * @param  {Boolean} edit "true" si habilita la edición, "false" si la deshabilita
         */
        _editRecordValues: function(edit, isEdit) {

            var mdprocess = this.getView().getModel("MDPROCESS");
            mdprocess.setProperty("/process_order/editable", edit);
            mdprocess.setProperty("/product_id/editable", edit);
            mdprocess.setProperty("/stage_id/editable", edit);
            mdprocess.setProperty("/historical_decrease/editable", edit);
            mdprocess.setProperty("/theoretical_decrease/editable", edit);
            mdprocess.setProperty("/historical_weight/editable", edit);
            mdprocess.setProperty("/theoretical_weight/editable", edit);
            mdprocess.setProperty("/historical_duration/editable", edit);
            mdprocess.setProperty("/theoretical_duration/editable", edit);
            //mdprocess.setProperty("/historical_accum_duration/editable", edit);
            //mdprocess.setProperty("/theoretical_accum_duration/editable", edit);
            //mdprocess.setProperty("/percen_distribution/editable", edit);
            mdprocess.setProperty("/breed_id/editable", edit);
            mdprocess.setProperty("/calendar_id/editable", edit);
            mdprocess.setProperty("/visible/editable", edit);
            mdprocess.setProperty("/biological_active/editable", edit);
            mdprocess.setProperty("/sync_considered/editable", edit);
            mdprocess.setProperty("/name/editable", edit);
            mdprocess.setProperty("/capacity/editable", edit);

            mdprocess.setProperty("/ispredecessor/editable", edit);

            console.log("isEdit: "+isEdit);
            if( (isEdit &&  (mdprocess.getProperty("/ispredecessor/value")==1)) ){
                mdprocess.setProperty("/predecessor_id/editable", true);
                console.log("1");
            }else{
                console.log("EL value es : "+mdprocess.getProperty("/ispredecessor/value"));
                mdprocess.setProperty("/predecessor_id/editable", false);
                console.log("2");
            }

        },
        /**
         * Se define un campo como obligatorio o no, de un registro MDPROCESS
         * @param  {Boolean} edit "true" si es campo obligatorio, "false" si no es obligatorio
         */
        _editRecordRequired: function(edit) {
            var mdprocess = this.getView().getModel("MDPROCESS");
            mdprocess.setProperty("/process_order/required", edit);
            mdprocess.setProperty("/historical_decrease/required", edit);
            mdprocess.setProperty("/theoretical_decrease/required", edit);
            mdprocess.setProperty("/historical_weight/required", edit);
            mdprocess.setProperty("/theoretical_weight/required", edit);
            mdprocess.setProperty("/historical_duration/required", edit);
            mdprocess.setProperty("/theoretical_duration/required", edit);
            //mdprocess.setProperty("/historical_accum_duration/required", edit);
            //mdprocess.setProperty("/theoretical_accum_duration/required", edit);
            //mdprocess.setProperty("/percen_distribution/required", edit);
            mdprocess.setProperty("/visible/required", edit);
            mdprocess.setProperty("/active/required", edit);
            mdprocess.setProperty("/name/required", edit);
            mdprocess.setProperty("/capacity/required", edit);

        },
        /**
         * Navega a la vista para crear un nuevo registro
         * @param  {Event} oEvent Evento que llamó esta función
         */
        onNewRecord: function(oEvent) {
            this.getRouter().navTo("mdprocess_Create", {}, false /*create history*/ );
        },
        /**
         * Cancela la creación de un registro MDPROCESS, y regresa a la vista principal
         * @param  {Event} oEvent Evento que llamó esta función
         */
        onCancelCreate: function(oEvent) {
            this._resetRecordValues();
            this.onNavBack(oEvent);
        },
        /**
         * Regresa a la vista principal de la entidad seleccionada actualmente
         * @param  {Event} oEvent Evento que llamó esta función
         */
        onNavBack: function(oEvent) {
            /** @type {JSONModel} OS Referencia al modelo "OS" */
            var util = this.getView().getModel("util");

            this.getRouter().navTo(util.getProperty("/selectedEntity"), {}, false /*create history*/ );
        },
        /**
         * Solicita al servicio correspondiente crear un registro process
         * @param  {Event} oEvent Evento que llamó esta función
         */
        onCreate: function(oEvent) {
            //Si el registro que se desea crear es válido
            if (this._validRecord()) {
                /**
                 * @type {JSONModel} MDPROCESS   Referencia al modelo "MDPROCESS"
                 * @type {JSONModel} util    Referencia al modelo "util"
                 * @type {Controller} that    Referencia a este controlador
                 * @type {Object} json        Objeto a enviar en la solicitud
                 * @type {Object} settings    Opciones de la llamada a la función ajax
                 */
                var that = this;
                var util = this.getView().getModel("util");
                var mdparameter = this.getView().getModel("MDPARAMETER");
                var mdprocess = this.getView().getModel("MDPROCESS");
                var mdproduct = this.getView().getModel("MDPRODUCT");
                var mdstage = this.getView().getModel("MDSTAGE");
                var txcalendar = this.getView().getModel("TXCALENDAR");

                var predecessor_id = 0;

                if(mdprocess.getProperty("/ispredecessor/value")==1){
                    predecessor_id = mdprocess.getProperty("/predecessor_id/value");
                }
                var serviceUrl= util.getProperty("/serviceUrl");
                /// "visible": mdprocess.getProperty("/visible/value") == 1 ? true:false
                $.ajax({
                    type: "POST",
                    contentType: "application/json",
                    data: JSON.stringify({
                        "process_order": mdprocess.getProperty("/process_order/value"),
                        "product_id": mdprocess.getProperty("/product_id/value"),
                        "stage_id": mdprocess.getProperty("/stage_id/value"),
                        //"percen_diserviceUrlstribution": mdprocess.getProperty("/percen_distribution/value"),
                        "breed_id": mdprocess.getProperty("/breed_id/value"),
                        "calendar_id": mdprocess.getProperty("/calendar_id/value"),
                        "name": mdprocess.getProperty("/name/value"),
                        "predecessor_id": predecessor_id,
                        "capacity": mdprocess.getProperty("/capacity/value"),
                        "historical_decrease": mdprocess.getProperty("/historical_decrease/value"),
                        "theoretical_decrease": mdprocess.getProperty("/theoretical_decrease/value"),
                        "historical_weight": mdprocess.getProperty("/historical_weight/value"),
                        "theoretical_weight": mdprocess.getProperty("/theoretical_weight/value"),
                        "historical_duration": mdprocess.getProperty("/historical_duration/value"),
                        "theoretical_duration": mdprocess.getProperty("/theoretical_duration/value"),
                        "gender": "Masculino",
                        "fattening_goal": 0,
                        "type_posture": "Joven",
                        "process_class_id": 2,
                        "biological_active": mdprocess.getProperty("/biological_active/value") == 1 ? true:false,
                        "sync_considered": mdprocess.getProperty("/sync_considered/value") == 1 ? true:false,
                        "visible": true
                    }),
                    url: serviceUrl+"/process/",
                    dataType: "json",
                    async: true,
                    success: function(data) {
                        util.setProperty("/busy/", false);
                        that._resetRecordValues();
                        that.onToast(that.getI18n().getText("OS.recordCreated"));
                        that.getRouter().navTo("mdprocess", {}, true /*no history*/ );

                    },
                    error: function(error) {
                        that.onToast("Error: " + error.responseText);
                        console.log("Read failed ");
                    }
                });

            }
        },
        updateCalendarModel: function(){
            var mdprocess = this.getView().getModel("MDPROCESS");
            mdprocess.setProperty("/calendar_id/value",this.getView().byId("process_calendar_model").getSelectedKey());
        },
        updateProductModel: function(oEvent){
            var mdprocess = this.getView().getModel("MDPROCESS");
            mdprocess.setProperty("/product_id/value",this.getView().byId("process_nameProduct_model").getSelectedKey());
        },
        updateStageModel: function(){
            var mdprocess = this.getView().getModel("MDPROCESS");
            mdprocess.setProperty("/stage_id/value",this.getView().byId("process_stage_model").getSelectedKey());
        },
        updatePredecessorModel: function(){
            var mdprocess = this.getView().getModel("MDPROCESS");
            mdprocess.setProperty("/predecessor_id/value",this.getView().byId("process_predecessor_model").getSelectedKey());
        },
        updateBreedModel: function(){
            var mdprocess = this.getView().getModel("MDPROCESS");
            console.log(this.getView().byId("process_breed_model").getSelectedKey());
            mdprocess.setProperty("/breed_id/value",this.getView().byId("process_breed_model").getSelectedKey());
        },
        /**
         * Valida la correctitud de los datos existentes en el formulario del registro
         * @return {Boolean} Devuelve "true" si los datos son correctos, y "false" si son incorrectos
         */
        _validRecord: function() {
            /**
             * @type {JSONModel} MDPROCESS Referencia al modelo "MDPROCESS"
             * @type {Boolean} flag "true" si los datos son válidos, "false" si no lo son
             */
            var mdprocess = this.getView().getModel("MDPROCESS"),
                flag = true,
                that = this,
                Without_SoL = /^\d+$/,
                Without_Num = /^[a-zA-Zñáéíóú| ]*$/,
                onlyDecimals = /^[0-9]*\.?[0-9]*$/;

            if (mdprocess.getProperty("/name/value") === "") {
                flag = false;
                mdprocess.setProperty("/name/state", "Error");
                mdprocess.setProperty("/name/stateText", this.getI18n().getText("enter.FIELD"));
            } else if (!Without_Num.test(mdprocess.getProperty("/name/value"))) {
                flag = false;
                mdprocess.setProperty("/name/state", "Error");
                mdprocess.setProperty("/name/stateText", this.getI18n().getText("enter.FIELD.WN"));
            } else {
                mdprocess.setProperty("/name/state", "None");
                mdprocess.setProperty("/name/stateText", "");
            }


            if (mdprocess.getProperty("/process_order/value") === "") {
                flag = false;
                mdprocess.setProperty("/process_order/state", "Error");
                mdprocess.setProperty("/process_order/stateText", this.getI18n().getText("enter.FIELD"));
            }else if(!Without_SoL.test(mdprocess.getProperty("/process_order/value"))){
                flag = false;
                mdprocess.setProperty("/process_order/state", "Error");
                mdprocess.setProperty("/process_order/stateText", this.getI18n().getText("enter.FIELD.WS"));
            }else {
                mdprocess.setProperty("/process_order/state", "None");
                mdprocess.setProperty("/process_order/stateText", "");
            }


            if (mdprocess.getProperty("/capacity/value") === "") {
                flag = false;
                mdprocess.setProperty("/capacity/state", "Error");
                mdprocess.setProperty("/capacity/stateText", this.getI18n().getText("enter.FIELD"));
            }else if(!Without_SoL.test(mdprocess.getProperty("/capacity/value"))){
                flag = false;
                mdprocess.setProperty("/capacity/state", "Error");
                mdprocess.setProperty("/capacity/stateText", this.getI18n().getText("enter.FIELD.WS"));
            }else {
                mdprocess.setProperty("/capacity/state", "None");
                mdprocess.setProperty("/capacity/stateText", "");
            }

            if (mdprocess.getProperty("/historical_duration/value") === "") {
                flag = false;
                mdprocess.setProperty("/historical_duration/state", "Error");
                mdprocess.setProperty("/historical_duration/stateText", this.getI18n().getText("enter.FIELD"));
            }else if(!Without_SoL.test(mdprocess.getProperty("/historical_duration/value"))){
                flag = false;
                mdprocess.setProperty("/historical_duration/state", "Error");
                mdprocess.setProperty("/historical_duration/stateText", this.getI18n().getText("enter.FIELD.WS"));
            }else {
                mdprocess.setProperty("/historical_duration/state", "None");
                mdprocess.setProperty("/historical_duration/stateText", "");
            }

            if (mdprocess.getProperty("/theoretical_duration/value") === "") {
                flag = false;
                mdprocess.setProperty("/theoretical_duration/state", "Error");
                mdprocess.setProperty("/theoretical_duration/stateText", this.getI18n().getText("enter.FIELD"));
            }else if(!Without_SoL.test(mdprocess.getProperty("/theoretical_duration/value"))){
                flag = false;
                mdprocess.setProperty("/theoretical_duration/state", "Error");
                mdprocess.setProperty("/theoretical_duration/stateText", this.getI18n().getText("enter.FIELD.WS"));
            }else {
                mdprocess.setProperty("/theoretical_duration/state", "None");
                mdprocess.setProperty("/theoretical_duration/stateText", "");
            }

            /*if (mdprocess.getProperty("/historical_accum_duration/value") === "") {
                flag = false;
                mdprocess.setProperty("/historical_accum_duration/state", "Error");
                mdprocess.setProperty("/historical_accum_duration/stateText", this.getI18n().getText("enter.FIELD"));
            }else if(!Without_SoL.test(mdprocess.getProperty("/historical_accum_duration/value"))){
                flag = false;
                mdprocess.setProperty("/historical_accum_duration/state", "Error");
                mdprocess.setProperty("/historical_accum_duration/stateText", this.getI18n().getText("enter.FIELD.WS"));
            }else {
                mdprocess.setProperty("/historical_accum_duration/state", "None");
                mdprocess.setProperty("/historical_accum_duration/stateText", "");
            }


            if (mdprocess.getProperty("/theoretical_accum_duration/value") === "") {
                flag = false;
                mdprocess.setProperty("/theoretical_accum_duration/state", "Error");
                mdprocess.setProperty("/theoretical_accum_duration/stateText", this.getI18n().getText("enter.FIELD"));
            }else if(!Without_SoL.test(mdprocess.getProperty("/theoretical_accum_duration/value"))){
                flag = false;
                mdprocess.setProperty("/theoretical_accum_duration/state", "Error");
                mdprocess.setProperty("/theoretical_accum_duration/stateText", this.getI18n().getText("enter.FIELD.WS"));
            }else {
                mdprocess.setProperty("/theoretical_accum_duration/state", "None");
                mdprocess.setProperty("/theoretical_accum_duration/stateText", "");
            }*/

            if (mdprocess.getProperty("/historical_decrease/value") === "") {
                flag = false;
                mdprocess.setProperty("/historical_decrease/state", "Error");
                mdprocess.setProperty("/historical_decrease/stateText", this.getI18n().getText("enter.FIELD"));
            }else if(!onlyDecimals.test(mdprocess.getProperty("/historical_decrease/value"))){
                flag = false;
                mdprocess.setProperty("/historical_decrease/state", "Error");
                mdprocess.setProperty("/historical_decrease/stateText", this.getI18n().getText("enter.FIELD.onlyD"));
            }else {
                mdprocess.setProperty("/historical_decrease/state", "None");
                mdprocess.setProperty("/historical_decrease/stateText", "");
            }


            if (mdprocess.getProperty("/theoretical_decrease/value") === "") {
                flag = false;
                mdprocess.setProperty("/theoretical_decrease/state", "Error");
                mdprocess.setProperty("/theoretical_decrease/stateText", this.getI18n().getText("enter.FIELD"));
            }else if(!onlyDecimals.test(mdprocess.getProperty("/theoretical_decrease/value"))){
                flag = false;
                mdprocess.setProperty("/theoretical_decrease/state", "Error");
                mdprocess.setProperty("/theoretical_decrease/stateText", this.getI18n().getText("enter.FIELD.onlyD"));
            }else {
                mdprocess.setProperty("/theoretical_decrease/state", "None");
                mdprocess.setProperty("/theoretical_decrease/stateText", "");
            }

            if (mdprocess.getProperty("/historical_weight/value") === "") {
                flag = false;
                mdprocess.setProperty("/historical_weight/state", "Error");
                mdprocess.setProperty("/historical_weight/stateText", this.getI18n().getText("enter.FIELD"));
            }else if(!onlyDecimals.test(mdprocess.getProperty("/historical_weight/value"))){
                flag = false;
                mdprocess.setProperty("/historical_weight/state", "Error");
                mdprocess.setProperty("/historical_weight/stateText", this.getI18n().getText("enter.FIELD.onlyD"));
            }else {
                mdprocess.setProperty("/historical_weight/state", "None");
                mdprocess.setProperty("/historical_weight/stateText", "");
            }

            if (mdprocess.getProperty("/theoretical_weight/value") === "") {
                flag = false;
                mdprocess.setProperty("/theoretical_weight/state", "Error");
                mdprocess.setProperty("/theoretical_weight/stateText", this.getI18n().getText("enter.FIELD"));
            }else if(!onlyDecimals.test(mdprocess.getProperty("/theoretical_weight/value"))){
                flag = false;
                mdprocess.setProperty("/theoretical_weight/state", "Error");
                mdprocess.setProperty("/theoretical_weight/stateText", this.getI18n().getText("enter.FIELD.onlyD"));
            }else {
                mdprocess.setProperty("/theoretical_weight/state", "None");
                mdprocess.setProperty("/theoretical_weight/stateText", "");
            }


            /*  if (mdprocess.getProperty("/percen_distribution/value") === "") {
                flag = false;
                mdprocess.setProperty("/percen_distribution/state", "Error");
                mdprocess.setProperty("/percen_distribution/stateText", this.getI18n().getText("enter.FIELD"));
            }else if(!onlyDecimals.test(mdprocess.getProperty("/percen_distribution/value"))){
                flag = false;
                mdprocess.setProperty("/percen_distribution/state", "Error");
                mdprocess.setProperty("/percen_distribution/stateText", this.getI18n().getText("enter.FIELD.onlyD"));
            }else {
                mdprocess.setProperty("/percen_distribution/state", "None");
                mdprocess.setProperty("/percen_distribution/stateText", "");
            }*/

            return flag;
        },
        /**
         * Visualiza los detalles de un registro MDPROCESS
         * @param  {Event} oEvent Evento que llamó esta función
         */
        onViewProcessRecord: function(oEvent) {

            var mdprocess = this.getView().getModel("MDPROCESS");
            var create = false;
            this.loadProducts(create);
            this.loadStages(create);
            this.loadCalendars(create);
            this.loadBreed(create);
            //this.loadPredecessor(oEvent.getSource().getBindingContext("MDPROCESS").getObject().process_id, mdprocess);
            //this.loadPredecessor(oEvent, mdprocess)

            mdprocess.setProperty("/save/", false);
            mdprocess.setProperty("/cancel/", false);
            mdprocess.setProperty("/selectedRecordPath/", oEvent.getSource().getBindingContext("MDPROCESS"));
            //console.log(oEvent.getSource().getBindingContext("MDPROCESS").getObject().process_id);
            mdprocess.setProperty("/process_id/value", oEvent.getSource().getBindingContext("MDPROCESS").getObject().process_id);
            mdprocess.setProperty("/process_order/value", oEvent.getSource().getBindingContext("MDPROCESS").getObject().process_order);
            mdprocess.setProperty("/product_id/value", oEvent.getSource().getBindingContext("MDPROCESS").getObject().product_id);
            mdprocess.setProperty("/stage_id/value", oEvent.getSource().getBindingContext("MDPROCESS").getObject().stage_id);
            mdprocess.setProperty("/historical_decrease/value", oEvent.getSource().getBindingContext("MDPROCESS").getObject().historical_decrease);
            mdprocess.setProperty("/theoretical_decrease/value", oEvent.getSource().getBindingContext("MDPROCESS").getObject().theoretical_decrease);
            mdprocess.setProperty("/historical_weight/value", oEvent.getSource().getBindingContext("MDPROCESS").getObject().historical_weight);
            mdprocess.setProperty("/theoretical_weight/value", oEvent.getSource().getBindingContext("MDPROCESS").getObject().theoretical_weight);
            mdprocess.setProperty("/historical_duration/value", oEvent.getSource().getBindingContext("MDPROCESS").getObject().historical_duration);
            mdprocess.setProperty("/theoretical_duration/value", oEvent.getSource().getBindingContext("MDPROCESS").getObject().theoretical_duration);
            //mdprocess.setProperty("/historical_accum_duration/value", oEvent.getSource().getBindingContext("MDPROCESS").getObject().historical_accum_duration);
            //mdprocess.setProperty("/theoretical_accum_duration/value", oEvent.getSource().getBindingContext("MDPROCESS").getObject().theoretical_accum_duration);
            //mdprocess.setProperty("/percen_distribution/value", oEvent.getSource().getBindingContext("MDPROCESS").getObject().percen_distribution);
            mdprocess.setProperty("/breed_id/value", oEvent.getSource().getBindingContext("MDPROCESS").getObject().breed_id);
            mdprocess.setProperty("/calendar_id/value", oEvent.getSource().getBindingContext("MDPROCESS").getObject().calendar_id);
            //mdprocess.setProperty("/calendar_id/value", oEvent.getSource().getBindingContext("MDPROCESS").getObject().calendar_id);
            /// mdprocess.setProperty("/visible/value", oEvent.getSource().getBindingContext("MDPROCESS").getObject().visible);
            mdprocess.setProperty("/visible/value", true);
            mdprocess.setProperty("/name/value", oEvent.getSource().getBindingContext("MDPROCESS").getObject().name);
            mdprocess.setProperty("/predecessor_id/value", oEvent.getSource().getBindingContext("MDPROCESS").getObject().predecessor_id);
            mdprocess.setProperty("/capacity/value", oEvent.getSource().getBindingContext("MDPROCESS").getObject().capacity);
            mdprocess.setProperty("/ispredecessor/value", oEvent.getSource().getBindingContext("MDPROCESS").getObject().ispredecessor);
            mdprocess.setProperty("/biological_active/value", oEvent.getSource().getBindingContext("MDPROCESS").getObject().biological_active);
            mdprocess.setProperty("/sync_considered/value", oEvent.getSource().getBindingContext("MDPROCESS").getObject().sync_considered);

            this.loadPredecessor(create);
            this.getRouter().navTo("mdprocess_Record", {}, false /*create history*/ );
        },
        /**
         * Coincidencia de ruta para acceder a los detalles de un registro
         * @param  {Event} oEvent Evento que llamó esta función
         */
        _onRecordMatched: function(oEvent) {
            this._viewOptions();
        },
        /**
         * Cambia las opciones de visualización disponibles en la vista de detalles de un registro
         */
        _viewOptions: function() {
            var mdprocess = this.getView().getModel("MDPROCESS");
            mdprocess.setProperty("/cancel/", false);
            mdprocess.setProperty("/modify/", true);
            mdprocess.setProperty("/delete/", true);
            mdprocess.setProperty("/save/", false);
            this._editRecordValues(false,false);
            this._editRecordRequired(false);

        },
        /**
         * Ajusta la vista para editar los datos de un registro
         * @param  {Event} oEvent Evento que llamó esta función
         */
        onEdit: function(oEvent) {

            var mdprocess = this.getView().getModel("MDPROCESS");
            mdprocess.setProperty("/save/", true);
            mdprocess.setProperty("/cancel/", true);
            mdprocess.setProperty("/modify/", false);
            mdprocess.setProperty("/delete/", false);
            this._editRecordRequired(true);
            /*
            * Envio que los datos deben ser editables y que estoy llamando a la funcion desde edit
            */
            this._editRecordValues(true,true);
        },

        /**
         * Cancela la edición de un registro MDPROCESS
         * @param  {Event} oEvent Evento que llamó esta función
         */
        onCancelEdit: function(oEvent) {
            /** @type {JSONModel} MDPROCESS  Referencia al modelo MDPROCESS */

            this.onView();
        },
        /**
         * Ajusta la vista para visualizar los datos de un registro
         */
        onView: function() {
            this._viewOptions();
        },
        /**
         * Solicita al servicio correspondiente actualizar un registro MDPROCESS
         * @param  {Event} oEvent Evento que llamó esta función
         */
        onUpdate: function(oEvent) {
            console.log("Llegue a update");
            console.log(this._validRecord(), this._recordChanged());
            /**
             * Si el registro que se quiere actualizar es válido y hubo algún cambio
             * con respecto a sus datos originales
             */
            if (this._validRecord() && this._recordChanged()) {
                /**
                 * @type {JSONModel} MDPROCESS   Referencia al modelo "MDPROCESS"
                 * @type {JSONModel} util         Referencia al modelo "util"
                 * @type {Controller} that         Referencia a este controlador
                 */
                var mdprocess = this.getView().getModel("MDPROCESS");
                var util = this.getView().getModel("util");
                var that = this;
                var predecessor_id = 0;

                if(mdprocess.getProperty("/ispredecessor/value")==1){
                    predecessor_id = mdprocess.getProperty("/predecessor_id/value");
                }
                var serviceUrl= util.getProperty("/serviceUrl");
                console.log("serviceurl: ",serviceUrl);
                /// "visible": mdprocess.getProperty("/visible/value") == 1 ? true:false,
                $.ajax({
                    type: "PUT",
                    contentType: "application/json",
                    data: JSON.stringify({
                        "process_order": mdprocess.getProperty("/process_order/value"),
                        "product_id": mdprocess.getProperty("/product_id/value"),
                        "stage_id": mdprocess.getProperty("/stage_id/value"),
                        "breed_id": mdprocess.getProperty("/breed_id/value"),
                        //"percen_distribution": mdprocess.getProperty("/percen_distribution/value"),
                        "calendar_id": mdprocess.getProperty("/calendar_id/value"),
                        "name": mdprocess.getProperty("/name/value"),
                        "predecessor_id": predecessor_id,
                        "capacity": mdprocess.getProperty("/capacity/value"),
                        "historical_decrease": mdprocess.getProperty("/historical_decrease/value"),
                        "theoretical_decrease": mdprocess.getProperty("/theoretical_decrease/value"),
                        "historical_weight": mdprocess.getProperty("/historical_weight/value"),
                        "theoretical_weight": mdprocess.getProperty("/theoretical_weight/value"),
                        "historical_duration": mdprocess.getProperty("/historical_duration/value"),
                        "theoretical_duration": mdprocess.getProperty("/theoretical_duration/value"),
                        "gender": "Masculino",
                        "fattening_goal": 0,
                        "type_posture": "Joven",
                        "process_class_id": 2,
                        "biological_active": mdprocess.getProperty("/biological_active/value") == 1 ? true:false,
                        "sync_considered": mdprocess.getProperty("/sync_considered/value") == 1 ? true:false,
                        "visible": true,
                        "process_id": mdprocess.getProperty("/process_id/value")
                    }),
                    url: serviceUrl+"/process/",
                    dataType: "json",
                    async: true,
                    success: function(data) {

                        util.setProperty("/busy/", false);
                        that._resetRecordValues();
                        that._viewOptions();
                        that.onToast(that.getI18n().getText("OS.recordUpdated"));
                        that.getRouter().navTo("mdprocess", {}, true /*no history*/ );

                    },
                    error: function(request, status, error) {
                        that.onToast("Error de comunicación");
                        console.log("Read failed");
                    }
                });
            }
        },
        /**
         * Verifica si el registro seleccionado tiene algún cambio con respecto a sus valores originales
         * @return {Boolean} Devuelve "true" el registro cambió, y "false" si no cambió
         */
        _recordChanged: function() {
            /**
             * @type {JSONModel} MDPROCESS     Referencia al modelo "MDPROCESS"
             * @type {Boolean} flag            "true" si el registro cambió, "false" si no cambió
             */
            var mdprocess = this.getView().getModel("MDPROCESS"),
                flag = false;

            if (mdprocess.getProperty("/name/value") !== mdprocess.getProperty(mdprocess.getProperty("/selectedRecordPath/") + "/name")) {
                flag = true;
            }

            if (mdprocess.getProperty("/process_order/value") !== mdprocess.getProperty(mdprocess.getProperty("/selectedRecordPath/") + "/process_order")) {
                flag = true;
            }

            if (mdprocess.getProperty("/calendar_id/value") !== mdprocess.getProperty(mdprocess.getProperty("/selectedRecordPath/") + "/calendar_id")) {
                flag = true;
            }


            if (mdprocess.getProperty("/capacity/value") !== mdprocess.getProperty(mdprocess.getProperty("/selectedRecordPath/") + "/capacity")) {
                flag = true;
            }
            /*
            if (mdprocess.getProperty("/percen_distribution/value") !== mdprocess.getProperty(mdprocess.getProperty("/selectedRecordPath/") + "/percen_distribution")) {
                flag = true;
            }*/

            if (mdprocess.getProperty("/historical_decrease/value") !== mdprocess.getProperty(mdprocess.getProperty("/selectedRecordPath/") + "/historical_decrease")) {
                flag = true;
            }

            if (mdprocess.getProperty("/theoretical_decrease/value") !== mdprocess.getProperty(mdprocess.getProperty("/selectedRecordPath/") + "/theoretical_decrease")) {
                flag = true;
            }

            if (mdprocess.getProperty("/historical_weight/value") !== mdprocess.getProperty(mdprocess.getProperty("/selectedRecordPath/") + "/historical_weight")) {
                flag = true;
            }

            if (mdprocess.getProperty("/theoretical_weight/value") !== mdprocess.getProperty(mdprocess.getProperty("/selectedRecordPath/") + "/theoretical_weight")) {
                flag = true;
            }

            if (mdprocess.getProperty("/historical_duration/value") !== mdprocess.getProperty(mdprocess.getProperty("/selectedRecordPath/") + "/historical_duration")) {
                flag = true;
            }

            if (mdprocess.getProperty("/theoretical_duration/value") !== mdprocess.getProperty(mdprocess.getProperty("/selectedRecordPath/") + "/theoretical_duration")) {
                flag = true;
            }
            /*
        if (mdprocess.getProperty("/historical_accum_duration/value") !== mdprocess.getProperty(mdprocess.getProperty("/selectedRecordPath/") + "/historical_accum_duration")) {
                flag = true;
            }*/

            if (mdprocess.getProperty("/product_id/value") !== mdprocess.getProperty(mdprocess.getProperty("/selectedRecordPath/") + "/product_id")) {
                flag = true;
            }

            if (mdprocess.getProperty("/stage_id/value") !== mdprocess.getProperty(mdprocess.getProperty("/selectedRecordPath/") + "/stage_id")) {
                flag = true;
            }

            if (mdprocess.getProperty("/predecessor_id/value") !== mdprocess.getProperty(mdprocess.getProperty("/selectedRecordPath/") + "/predecessor_id")) {
                flag = true;
            }

            if (mdprocess.getProperty("/visible/value") !== mdprocess.getProperty(mdprocess.getProperty("/selectedRecordPath/") + "/visible")) {
                flag = true;
            }

            if (mdprocess.getProperty("/ispredecessor/value") !== mdprocess.getProperty(mdprocess.getProperty("/selectedRecordPath/") + "/ispredecessor")) {
                flag = true;
            }

            if (mdprocess.getProperty("/biological_active/value") !== mdprocess.getProperty(mdprocess.getProperty("/selectedRecordPath/") + "/biological_active")) {
                flag = true;
            }
            if (mdprocess.getProperty("/sync_considered/value") !== mdprocess.getProperty(mdprocess.getProperty("/selectedRecordPath/") + "/sync_considered")) {
                flag = true;
            }

            if(!flag) this.onToast("No se detectaron cambios");

            return flag;
        },

        onProcessSearch: function(oEvent){
            var aFilters = [],
                sQuery = oEvent.getSource().getValue(),
                binding = this.getView().byId("processTable").getBinding("items");

            if (sQuery && sQuery.length > 0) {
                /** @type {Object} filter1 Primer filtro de la búsqueda */
                var filter1 = new sap.ui.model.Filter("name", sap.ui.model.FilterOperator.Contains, sQuery);
                aFilters = new sap.ui.model.Filter([filter1]);
            }

            //se actualiza el binding de la lista
            binding.filter(aFilters);

        },
        onDialogFilters: function(oEvent) {
            console.log("Filter");
            if (!this._oDialog) {
                this._oDialog = sap.ui.xmlfragment("technicalConfiguration.view.Dialog", this);
            }

            jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialog);
            this._oDialog.open();
        },

        handleConfirm: function(oEvent) {

            var oView = this.getView();
            var oTable = oView.byId("processTable");

            var mParams = oEvent.getParameters();
            var oBinding = oTable.getBinding("items");

            //apply sorted
            var aSorters = [];
            var sPath = mParams.sortItem.getKey();
            var bDescending = mParams.sortDescending;
            aSorters.push(new Sorter(sPath, bDescending));
            oBinding.sort(aSorters);

            // apply filters to binding
            var aFilters = [];
            jQuery.each(mParams.filterItems, function(i, oItem) {
                var aSplit = oItem.getKey().split("___");
                var sPath = aSplit[0];
                var sOperator = aSplit[1];
                var sValue1 = aSplit[2];
                var sValue2 = aSplit[3];
                var oFilter = new Filter(sPath, sOperator, sValue1, sValue2);
                aFilters.push(oFilter);
            });
            console.log(aFilters);
            oBinding.filter(aFilters);

        },
        onRefresh: function(oEvent) {
            var oView = this.getView();
            var oTable = oView.byId("processTable");
            var oBinding = oTable.getBinding("items");
            oBinding.filter([]);
        },
        onConfirmDelete: function(oEvent){

            var oBundle = this.getView().getModel("i18n").getResourceBundle();
            var deleteRecord = oBundle.getText("deleteRecord");
            var confirmation = oBundle.getText("confirmation");
            var util = this.getView().getModel("util");

            var that = this;
            var dialog = new Dialog({
                title: confirmation,
                type: "Message",
                content: new sap.m.Text({
                    text: deleteRecord
                }),

                beginButton: new Button({
                    text: "Si",
                    press: function() {
                        util.setProperty("/busy/", true);
                        console.log(util);
                        var mdprocess = that.getView().getModel("MDPROCESS");
                        var serviceUrl= util.getProperty("/serviceUrl");
                        $.ajax({
                            type: "DELETE",
                            contentType: "application/json",
                            data: JSON.stringify({
                                "process_id": mdprocess.getProperty("/process_id/value")
                            }),
                            url: serviceUrl+"/process/",
                            dataType: "json",
                            async: true,
                            success: function(data) {

                                util.setProperty("/busy/", false);
                                that.getRouter().navTo("mdprocess", {}, true);
                                dialog.close();
                                dialog.destroy();

                            },
                            error: function(request, status, error) {
                                that.onToast("Error de comunicación");
                                console.log("Read failed");
                            }
                        });

                    }
                }),
                endButton: new Button({
                    text: "No",
                    press: function() {
                        dialog.close();
                        dialog.destroy();
                    }
                })
            });

            dialog.open();
        },
        
        onChangePredecessor: function(oEvent){
            var mdprocess = this.getView().getModel("MDPROCESS");
            if(this.getView().byId("ispredecessor_checkbox").getSelected()){
                mdprocess.setProperty("/predecessor_id/editable", true);
                console.log("cambiar a true");
            }else{
                mdprocess.setProperty("/predecessor_id/editable", false);
                console.log("cambiar a false");
            }
        },

        validateFloatInput: function (o) {
            let input= o.getSource();
            let floatLength=10;
            let intLength = 10;

            let value = input.getValue();
            let regex = new RegExp(`/^([0-9]{1,${intLength}})([.][0-9]{0,${floatLength}})?$/`);

            if (regex.test(value)) {
                input.setValueState("None");
                input.setValueStateText("");
                return true;
            }
            else {
                let pNumber = 0;
                let aux = value.split("")
                    .filter(char => {
                        if (/^[0-9.]$/.test(char)) {
                            if (char !== ".") {
                                return true;
                            }
                            else {
                                if (pNumber === 0) {
                                    pNumber++;
                                    return true;
                                }
                            }
                        }
                    }).join("").split(".");
                value = aux[0].substring(0, intLength);

                if (aux[1] !== undefined) {
                    value += "." + aux[1].substring(0, floatLength);
                }
                input.setValue(value);
                return false;
            }
        },

        validateIntInput: function (o) {
            console.log("validateIntInput");
            let input= o.getSource();
            let length = 10;
            let value = input.getValue();
            let regex = new RegExp(`/^[0-9]{1,${length}}$/`);

            if (regex.test(value)) {
                return true;
            }
            else {
                let aux = value.split("").filter(char => {
                    if (/^[0-9]$/.test(char)) {
                        if (char !== ".") {
                            return true;
                        }
                    }
                }).join("");

                value = aux.substring(0, length);
                input.setValue(value);
                return false;
            }
        },



    });
});
