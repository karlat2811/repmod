sap.ui.define([
    "technicalConfiguration/controller/BaseController"
], function(BaseController) {
    "use strict";
    //var selectShedsDlg, showShedsDlg;

    return BaseController.extend("technicalConfiguration.controller.ossilo", {

        onInit: function() {
            //ruta para la vista principal de silos
            this.getOwnerComponent().getRouter().getRoute("ossilo").attachPatternMatched(this._onRouteMatched, this);
            //ruta para los detalles de un silo
            this.getOwnerComponent().getRouter().getRoute("ossilo_Create").attachPatternMatched(this._onCreateMatched, this);
            //ruta para los detalles de un silo
            this.getOwnerComponent().getRouter().getRoute("ossilo_Record").attachPatternMatched(this._onRecordMatched, this);
        },

        /**
		 * Coincidencia de ruta para acceder a la vista principal
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        _onRouteMatched: function(oEvent) {
            var that = this,
                util = this.getView().getModel("util"),
                ospartnership = this.getView().getModel("OSPARTNERSHIP"),
                osfarm = this.getView().getModel("OSFARM"),
                oscenter = this.getView().getModel("OSCENTER"),
                ossilo = this.getView().getModel("OSSILO");

            //dependiendo del dispositivo, establece la propiedad "phone"
            util.setProperty("/phone/",
                this.getOwnerComponent().getContentDensityClass() === "sapUiSizeCozy");

            ospartnership.setProperty("/settings/tableMode", "SingleSelect");
            osfarm.setProperty("/settings/tableMode", "SingleSelect");
            oscenter.setProperty("/settings/tableMode", "SingleSelect");

            //si la estructura seleccionada antes de acceder a esta vista es diferente a silo
            if (util.getProperty("/selectedEntity") !== "ossilo") {

                //establece CENTER como la entidad seleccionada
                util.setProperty("/selectedEntity", "ossilo");

                //establece el tab de la tabla PARTNERSHIP como el tab seleccionado
                this.getView().byId("tabBar").setSelectedKey(this.getView().getId() + "--" + "partnershipFilter");

                //borra cualquier selección que se haya hecho en la tabla PARTNERSHIP
                this.getView().byId("partnershipTable").removeSelections(true);

                //borra cualquier selección que se haya hecho en la tabla BROILERSFARM
                this.getView().byId("farmTable").removeSelections(true);

                //borra cualquier selección que se haya hecho en la tabla CENTER
                this.getView().byId("centerTable").removeSelections(true);

                //borra cualquier selección que se haya hecho en la tabla silo
                this.getView().byId("siloTable").removeSelections(true);

                //establece que no hay ningún registro PARTNERSHIP seleccionado
                ospartnership.setProperty("/selectedRecordPath/", "");

                //establece que no hay ningún registro BROILERSFARM seleccionado
                osfarm.setProperty("/selectedRecordPath/", "");

                //establece que no hay ningún registro BROILERSFARM seleccionado
                oscenter.setProperty("/selectedRecordPath/", "");

                osfarm.setProperty("/records/", []);

                oscenter.setProperty("/records/", []);

                ossilo.setProperty("/records/", []);

                //deshabilita el tab de la tabla de registros BROILERSFARM
                osfarm.setProperty("/settings/enabledTab", false);

                //deshabilita el tab de la tabla de registros CENTER
                oscenter.setProperty("/settings/enabledTab", false);

                //deshabilita el tab de la tabla de registros silo
                ossilo.setProperty("/settings/enabledTab", false);

                //deshabilita la opción de crear un registro CENTER
                ossilo.setProperty("/new", false);

                //obtiene los registros de PARTNERSHIP
                sap.ui.controller("technicalConfiguration.controller.ospartnership").onRead(that, util, ospartnership);

            } else if (ospartnership.getProperty("/selectedRecordPath/") !== "" &&
				osfarm.getProperty("/selectedRecordPath/") !== "" &&
				oscenter.getProperty("/selectedRecordPath/") !== "") {

                //habilita el tab de la tabla de registros BROILERSFARM
                osfarm.setProperty("/settings/enabledTab", true);

                //habilita el tab de la tabla de registros CENTER
                oscenter.setProperty("/settings/enabledTab", true);

                //habilita el tab de la tabla de registros CENTER
                ossilo.setProperty("/settings/enabledTab", true);

                //habilita la opción de crear un registro CENTER
                ossilo.setProperty("/new", true);

                //obtiene los registros de silo
                this.onRead(that, util, ospartnership, osfarm, oscenter, ossilo);
            }
        },

        /**
		 * Regresa a la vista principal de la entidad seleccionada actualmente
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        onNavBack: function(oEvent) {
            /** @type {JSONModel} OS Referencia al modelo "OS" */
            var util = this.getView().getModel("util");

            this._resetRecordValues();
            this.getRouter().navTo(util.getProperty("/selectedEntity"), {}, true);
        },

        /**
		 * Selecciona un registro PARTNERSHIP y habilita la tabla de registros BROILERSFARM
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        onSelectPartnershipRecord: function(oEvent) {
            /**
			 * @type {Controller} that          Referencia a este controlador
			 * @type {JSONModel} dummy          Referencia al modelo "dummy"
			 * @type {JSONModel} PARTNERSHIP  Referencia al modelo "PARTNERSHIP"
			 * @type {JSONModel} BROILERSFARM Referencia al modelo "BROILERSFARM"
			 * @type {JSONModel} CENTER       Referencia al modelo "CENTER"
			 */
            var that = this,
                util = this.getView().getModel("util"),
                ospartnership = this.getView().getModel("OSPARTNERSHIP"),
                osfarm = this.getView().getModel("OSFARM"),
                oscenter = this.getView().getModel("OSCENTER"),
                ossilo = this.getView().getModel("OSSILO");

            //guarda la ruta del registro PARTNERSHIP que fue seleccionado
            ospartnership.setProperty("/selectedRecordPath/", oEvent.getSource()["_aSelectedPaths"][0]);
            ospartnership.setProperty("/selectedRecord/", ospartnership.getProperty(ospartnership.getProperty("/selectedRecordPath/")));

            osfarm.setProperty("/selectedRecordPath/", "");
            osfarm.setProperty("/selectedRecord/", {});

            oscenter.setProperty("/selectedRecordPath/", "");
            oscenter.setProperty("/selectedRecord/", {});

            //habilita el tab de la tabla de registros BROILERSFARM
            osfarm.setProperty("/settings/enabledTab", true);

            //deshabilita el tab de la tabla de registros CENTER
            oscenter.setProperty("/settings/enabledTab", false);

            //deshabilita el tab de la tabla de registros SILO
            ossilo.setProperty("/settings/enabledTab", false);

            //deshabilita la opción de crear un registro CENTER
            ossilo.setProperty("/new", false);

            //establece el tab de la tabla BROILERSFARM como el tab seleccionado
            this.getView().byId("tabBar").setSelectedKey(this.getView().getId() + "--" + "farmFilter");

            //borra cualquier selección que se haya hecho en la tabla BROILERSFARM
            this.getView().byId("farmTable").removeSelections(true);

            //obtiene los registros de BROILERSFARM
            sap.ui.controller("technicalConfiguration.controller.osfarm").onRead(that, util, ospartnership, osfarm);
        },

        /**
		 * Selecciona un registro BROILERSFARM y habilita la tabla de registros CENTER
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        onSelectFarmRecord: function(oEvent) {
            /**
			 * @type {Controller} that          Referencia a este controlador
			 * @type {JSONModel} dummy          Referencia al modelo "dummy"
			 * @type {JSONModel} PARTNERSHIP  Referencia al modelo "PARTNERSHIP"
			 * @type {JSONModel} BROILERSFARM Referencia al modelo "BROILERSFARM"
			 * @type {JSONModel} CENTER       Referencia al modelo "CENTER"
			 */
            var that = this,
                util = this.getView().getModel("util"),
                ospartnership = this.getView().getModel("OSPARTNERSHIP"),
                osfarm = this.getView().getModel("OSFARM"),
                oscenter = this.getView().getModel("OSCENTER"),
                ossilo = this.getView().getModel("OSSILO");

            //guarda la ruta del registro BROILERSFARM que fue seleccionado
            osfarm.setProperty("/selectedRecordPath/", oEvent.getSource()["_aSelectedPaths"][0]);
            osfarm.setProperty("/selectedRecord/", osfarm.getProperty(osfarm.getProperty("/selectedRecordPath/")));

            oscenter.setProperty("/selectedRecordPath/", "");
            oscenter.setProperty("/selectedRecord/", {});

            //habilita el tab de la tabla de registros CENTER
            oscenter.setProperty("/settings/enabledTab", true);

            //deshabilita el tab de la tabla de registros SILO
            ossilo.setProperty("/settings/enabledTab", false);

            //habilita la opción de crear un registro CENTER
            ossilo.setProperty("/new", false);

            //establece el tab de la tabla CENTER como el tab seleccionado
            this.getView().byId("tabBar").setSelectedKey(this.getView().getId() + "--" + "centerFilter");

            //borra cualquier selección que se haya hecho en la tabla BROILERSFARM
            this.getView().byId("centerTable").removeSelections(true);

            //obtiene los registros de CENTER
            sap.ui.controller("technicalConfiguration.controller.oscenter").onRead(that, util, ospartnership, osfarm, oscenter);
        },
        /**
		 * Selecciona un registro BROILERSFARM y habilita la tabla de registros CENTER
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        onSelectCenterRecord: function(oEvent) {
            /**
			 * @type {Controller} that          Referencia a este controlador
			 * @type {JSONModel} dummy          Referencia al modelo "dummy"
			 * @type {JSONModel} PARTNERSHIP  Referencia al modelo "PARTNERSHIP"
			 * @type {JSONModel} BROILERSFARM Referencia al modelo "BROILERSFARM"
			 * @type {JSONModel} CENTER       Referencia al modelo "CENTER"
			 */
            var that = this,
                util = this.getView().getModel("util"),
                ospartnership = this.getView().getModel("OSPARTNERSHIP"),
                osfarm = this.getView().getModel("OSFARM"),
                oscenter = this.getView().getModel("OSCENTER"),
                ossilo = this.getView().getModel("OSSILO");

            //guarda la ruta del registro BROILERSFARM que fue seleccionado
            oscenter.setProperty("/selectedRecordPath/", oEvent.getSource()["_aSelectedPaths"][0]);

            oscenter.setProperty("/selectedRecord/", oscenter.getProperty(oscenter.getProperty("/selectedRecordPath/")));

            //habilita el tab de la tabla de registros SILO
            ossilo.setProperty("/settings/enabledTab", true);

            //habilita la opción de crear un registro SILO
            ossilo.setProperty("/new", true);

            //establece el tab de la tabla SILO como el tab seleccionado
            this.getView().byId("tabBar").setSelectedKey(this.getView().getId() + "--" + "siloFilter");

            //borra cualquier selección que se haya hecho en la tabla BROILERSFARM
            this.getView().byId("siloTable").removeSelections(true);

            //obtiene los registros de SILO
            this.onRead(that, util, ospartnership, osfarm, oscenter, ossilo);
        },

        /**
		 * Obtiene todos los registros de BROILERSFARM, dado un cliente y una sociedad
		 * @param  {Controller} that          Referencia al controlador que llama esta función
		 * @param  {JSONModel} dummy          Referencia al modelo "dummy"
		 * @param  {JSONModel} PARTNERSHIP  Referencia al modelo "PARTNERSHIP"
		 * @param  {JSONModel} BROILERSFARM Referencia al modelo "BROILERSFARM"
		 * @param  {JSONModel} CENTER       Referencia al modelo "CENTER"
		 */
        onRead: function(that, util, ospartnership, osfarm, oscenter, ossilo) {
            /** @type {Object} settings opciones de la llamada a la función ajax */
            var serviceUrl = util.getProperty("/serviceUrl");
            console.log("Center: "+oscenter.getProperty(oscenter.getProperty("/selectedRecordPath/") + "/center_id"));
            var settings = {
                type: "POST",
                contentType: "application/json",
                data: JSON.stringify({
                    "center_id": oscenter.getProperty(oscenter.getProperty("/selectedRecordPath/") + "/center_id")
                }),
                url: serviceUrl+"/silo/findSiloByCenter",
                dataType: "json",
                async: true,
                success: function(res) {
                    console.log(res.data);
                    util.setProperty("/busy/", false);
                    ossilo.setProperty("/records/", res.data);
                    console.log(ossilo);
                },
                error: function(err) {
                    console.log(err);
                    util.setProperty("/error/status", err.status);
                    util.setProperty("/error/statusText", err.statusText);
                }
            };

            util.setProperty("/busy/", true);
            //borra los registros SILO que estén almacenados actualmente
            ossilo.setProperty("/records/", []);
            //realiza la llamada ajax
            $.ajax(settings);
        },

        /**
		 * Navega a la vista para crear un nuevo registro
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        onNewRecord: function(oEvent) {
            this.getRouter().navTo("ossilo_Create", {}, true);
        },

        /**
		 * Coincidencia de ruta para acceder a la creación de un registro
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        _onCreateMatched: function(oEvent) {
            //resetea y habilita los campos del formulario para su edición
            this._resetRecordValues();
            this._editRecordValues(true);
        },

        /**
		 * Resetea todos los valores existentes en el formulario del registro
		 */
        _resetRecordValues: function() {
            /** @type {JSONModel} CENTER Referencia al modelo "SILO" */
            var ossilo = this.getView().getModel("OSSILO");

            ossilo.setProperty("/silo_number/value", "");
            ossilo.setProperty("/silo_number/state", "None");
            ossilo.setProperty("/silo_number/stateText", "");

            ossilo.setProperty("/height/value", "");
            ossilo.setProperty("/height/state", "None");
            ossilo.setProperty("/height/stateText", "");

            ossilo.setProperty("/diameter/value", "");
            ossilo.setProperty("/diameter/state", "None");
            ossilo.setProperty("/diameter/stateText", "");

            ossilo.setProperty("/total_capacity_1/value", "");
            ossilo.setProperty("/total_capacity_1/state", "None");
            ossilo.setProperty("/total_capacity_1/stateText", "");

            ossilo.setProperty("/total_rings_quantity/value", "");
            ossilo.setProperty("/total_rings_quantity/state", "None");
            ossilo.setProperty("/total_rings_quantity/stateText", "");

            ossilo.setProperty("/rings_quantity/value", "");
            ossilo.setProperty("/rings_quantity/state", "None");
            ossilo.setProperty("/rings_quantity/stateText", "");

            ossilo.setProperty("/cone_degrees/value", "");
            ossilo.setProperty("/cone_degrees/state", "None");
            ossilo.setProperty("/cone_degrees/stateText", "");
        },

        /**
		 * Habilita/deshabilita la edición de los datos de un registro
		 * @param  {Boolean} edit "true" si habilita la edición, "false" si la deshabilita
		 */
        _editRecordValues: function(edit) {
            /** @type {JSONModel} SILO Referencia al modelo "SILO" */
            var ossilo = this.getView().getModel("OSSILO");

            ossilo.setProperty("/silo_number/editable", edit);
            ossilo.setProperty("/silo_number/required", edit);

            ossilo.setProperty("/height/editable", edit);
            ossilo.setProperty("/height/required", edit);

            ossilo.setProperty("/diameter/editable", edit);
            ossilo.setProperty("/diameter/required", edit);

            ossilo.setProperty("/total_capacity_1/editable", edit);
            ossilo.setProperty("/total_capacity_1/required", edit);

            ossilo.setProperty("/total_rings_quantity/editable", edit);
            ossilo.setProperty("/total_rings_quantity/required", edit);

            ossilo.setProperty("/rings_height/editable", edit);
            ossilo.setProperty("/rings_height/required", edit);

            ossilo.setProperty("/cone_degrees/editable", edit);
            ossilo.setProperty("/cone_degrees/required", edit);
        },

        /**
		 * Valida la correctitud de los datos existentes en el formulario del registro
		 * @return {Boolean} Devuelve "true" si los datos son correctos, y "false" si son incorrectos
		 */
        _validRecord: function() {
            /**
			 * @type {JSONModel} SILO Referencia al modelo "SILO"
			 * @type {Boolean} flag             "true" si los datos son válidos, "false" si no lo son
			 */
            var ossilo = this.getView().getModel("OSSILO"),
                flag = true;
            console.log("Flag: "+flag);
            if (ossilo.getProperty("/silo_number/value") === "") {
                flag = false;
                ossilo.setProperty("/silo_number/state", "Error");
                ossilo.setProperty("/silo_number/stateText", this.getI18n().getText("enter.SILONAME"));
            } else {
                ossilo.setProperty("/silo_number/state", "None");
                ossilo.setProperty("/silo_number/stateText", "");
            }
            console.log("Flag: "+flag);
            if (ossilo.getProperty("/height/value") === "") {
                flag = false;
                ossilo.setProperty("/height/state", "Error");
                ossilo.setProperty("/height/stateText", "Introduzca la altura del silo");
            } else if (ossilo.getProperty("/height/value") <= 0) {
                flag = false;
                ossilo.setProperty("/height/state", "Error");
                ossilo.setProperty("/height/stateText", "Introduzca una altura válida");
            } else {
                ossilo.setProperty("/height/state", "None");
                ossilo.setProperty("/height/stateText", "");
            }
            console.log("Flag: "+flag);
            if (ossilo.getProperty("/diameter/value") === "") {
                flag = false;
                ossilo.setProperty("/diameter/state", "Error");
                ossilo.setProperty("/diameter/stateText", "Introduzca el diámetro del silo");
            } else if (ossilo.getProperty("/diameter/value") <= 0) {
                flag = false;
                ossilo.setProperty("/diameter/state", "Error");
                ossilo.setProperty("/diameter/stateText", "Introduzca un diámetro válido");
            } else {
                ossilo.setProperty("/diameter/state", "None");
                ossilo.setProperty("/diameter/stateText", "");
            }
            console.log("Flag: "+flag);
            if (ossilo.getProperty("/total_capacity_1/value") === "") {
                flag = false;
                ossilo.setProperty("/total_capacity_1/state", "Error");
                ossilo.setProperty("/total_capacity_1/stateText", "Introduzca la capacidad del silo");
            } else if (ossilo.getProperty("/total_capacity_1/value") <= 0) {
                flag = false;
                ossilo.setProperty("/total_capacity_1/state", "Error");
                ossilo.setProperty("/total_capacity_1/stateText", "Introduzca una capacidad válida");
            } else {
                ossilo.setProperty("/total_capacity_1/state", "None");
                ossilo.setProperty("/total_capacity_1/stateText", "");
            }
            console.log("Flag: "+flag);
            if (ossilo.getProperty("/total_rings_quantity/value") === "") {
                flag = false;
                ossilo.setProperty("/total_rings_quantity/state", "Error");
                ossilo.setProperty("/total_rings_quantity/stateText", "Introduzca la cantidad de anillos");
            } else if (ossilo.getProperty("/total_rings_quantity/value") <= 0) {
                flag = false;
                ossilo.setProperty("/total_rings_quantity/state", "Error");
                ossilo.setProperty("/total_rings_quantity/stateText", "Introduzca una cantidad válida");
            } else {
                ossilo.setProperty("/total_rings_quantity/state", "None");
                ossilo.setProperty("/total_rings_quantity/stateText", "");
            }
            console.log("Flag: "+flag);
            if (ossilo.getProperty("/rings_height/value") === "") {
                flag = false;
                ossilo.setProperty("/rings_height/state", "Error");
                ossilo.setProperty("/rings_height/stateText", "Introduzca la altura de los anillos");
            } else if (ossilo.getProperty("/rings_height/value") <= 0) {
                flag = false;
                ossilo.setProperty("/rings_height/state", "Error");
                ossilo.setProperty("/rings_height/stateText", "Introduzca una altura válida");
            } else {
                ossilo.setProperty("/rings_height/state", "None");
                ossilo.setProperty("/rings_height/stateText", "");
            }
            console.log("Flag: "+flag);
            if (ossilo.getProperty("/cone_degrees/value") === "") {
                flag = false;
                ossilo.setProperty("/cone_degrees/state", "Error");
                ossilo.setProperty("/cone_degrees/stateText", "Introduzca el ángulo de la tolva");
            } else {
                ossilo.setProperty("/cone_degrees/state", "None");
                ossilo.setProperty("/cone_degrees/stateText", "");
            }
            console.log("Flag: "+flag);
            return flag;
        },

        /**
		 * Solicita al servicio correspondiente crear un registro CENTER,
		 * dado un cliente, una sociedad y una granja
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        onCreate: function(oEvent) {
            //Si el registro que se desea crear es válido
            if (this._validRecord()) {
                console.log("Entre");
                var ospartnership = this.getView().getModel("OSPARTNERSHIP"),
                    osfarm = this.getView().getModel("OSFARM"),
                    oscenter = this.getView().getModel("OSCENTER"),
                    ossilo = this.getView().getModel("OSSILO"),
                    util = this.getView().getModel("util"),
                    that = this,
                    json = {
                        "client_id": 1,
                        "partnership_id": ospartnership.getProperty(ospartnership.getProperty("/selectedRecordPath/") + "/partnership_id"),
                        "farm_id": osfarm.getProperty(osfarm.getProperty("/selectedRecordPath/") + "/farm_id"),
                        "center_id": oscenter.getProperty(oscenter.getProperty("/selectedRecordPath/") + "/center_id"),
                        "silo_number": ossilo.getProperty("/silo_number/value"),
                        "height": ossilo.getProperty("/height/value"),
                        "diameter": ossilo.getProperty("/diameter/value"),
                        "total_capacity_1": ossilo.getProperty("/total_capacity_1/value"),
                        "total_rings_quantity": ossilo.getProperty("/total_rings_quantity/value"),
                        "rings_height": ossilo.getProperty("/rings_height/value"),
                        "cone_degrees": ossilo.getProperty("/cone_degrees/value")
                    },
                    serviceUrl = util.getProperty("/serviceUrl"),
                    settings = {
                        async: true,
                        url: serviceUrl + "/silo",
                        method: "POST",
                        data: JSON.stringify(json),
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function(res) {
                            console.log(res);
                            that._resetRecordValues();
                            that.onToast(that.getI18n().getText("El silo se ha creado"));
                            that.getRouter().navTo("ossilo", {}, true);
                        },
                        error: function(err) {
                            console.log(err);
                            util.setProperty("/error/status", err.status);
                            util.setProperty("/error/statusText", err.statusText);
                            //that.onConnectionError();
                        }
                    };
                util.setProperty("/busy/", true);
                //realiza la llamada ajax
                console.log("Voy a llamr");
                $.ajax(settings);
            }
        },

        /**
		 * Coincidencia de ruta para acceder a los detalles de un registro
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        _onRecordMatched: function(oEvent) {
            //Establece las opciones disponibles al visualizar el registro
            this._viewOptions();
            this.getAssociatedSheds();
        },
        getAssociatedSheds: function(){
            var util = this.getView().getModel("util"),
                ospartnership = this.getView().getModel("OSPARTNERSHIP"),
                osfarm = this.getView().getModel("OSFARM"),
                oscenter = this.getView().getModel("OSCENTER"),
                ossilo = this.getView().getModel("OSSILO"),
                osshed = this.getView().getModel("OSSHED"),
                that = this;
            var settings = {
                async: true,
                url: util.getProperty("/serviceUrl") + "/silo/findShedBySilo",
                data: {
                    "client_id": util.getProperty("/clientID"),
                    "partnership_id": ospartnership.getProperty(ospartnership.getProperty("/selectedRecordPath/") + "/partnership_id"),
                    "farm_id": osfarm.getProperty(osfarm.getProperty("/selectedRecordPath/") + "/farm_id"),
                    "center_id": oscenter.getProperty(oscenter.getProperty("/selectedRecordPath/") + "/center_id"),
                    "silo_id": ossilo.getProperty(ossilo.getProperty("/selectedRecordPath/") + "/silo_id")
                    //"&ASSOCIATION=SHEDS"
                },
                method: "POST",
                success: function(res) {
                    console.log("Resultado", res);
                    ossilo.setProperty("/sheds/records", res.data);
                    console.log("Resultado", osshed);
                },
                error: function(err) {
                    console.log(err);
                    util.setProperty("/error/status", err.status);
                    util.setProperty("/error/statusText", err.statusText);
                    //that.onConnectionError();
                }
            };

            /*
				$.ajax({
					url: util.getProperty("/serviceUrl") + "/silo/findSiloByShed",
					method: "POST",
					async: true,
					data: {
						"client_id": util.getProperty("/clientID"),
						"partnership_id": ospartnership.getProperty(ospartnership.getProperty("/selectedRecordPath/") + "/partnership_id"),
						"farm_id": osfarm.getProperty(osfarm.getProperty("/selectedRecordPath/") + "/farm_id"),
						"center_id": oscenter.getProperty(oscenter.getProperty("/selectedRecordPath/") + "/center_id"),
						"silo_id": ossilo.getProperty(ossilo.getProperty("/selectedRecordPath/") + "/silo_id")
					},
					dataType:"json",
					success: function(res) {
						 // CODE HERE!!!
					 },
						error: function(err) {    console.log("err: ", err);   }  });
						*/
            util.setProperty("/busy/", true);
            //borra los registros almacenados actualmente de galpones asociados
            ossilo.setProperty("/sheds/records/", []);
            ossilo.setProperty("/sheds/showRecords/", []);
            ossilo.setProperty("/sheds/originalRecords/", []);
            //realiza la llamada ajax
            $.ajax(settings);
        },

        /**
		 * Cambia las opciones de visualización disponibles en la vista de detalles de un registro
		 */
        _viewOptions: function() {
            /**
			 * @type {JSONModel} util       Referencia al modelo "OS"
			 * @type {JSONModel} OSSILO Referencia al modelo "SILO"
			 */
            var util = this.getView().getModel("util"),
                ossilo = this.getView().getModel("OSSILO");

            if (util.getProperty("/selectedEntity/") === "ossilo") {
                ossilo.setProperty("/modify/", true);
                ossilo.setProperty("/delete/", true);
            } else {
                ossilo.setProperty("/modify/", false);
                ossilo.setProperty("/delete/", false);
            }

            ossilo.setProperty("/save/", false);
            ossilo.setProperty("/cancel/", false);

            this._editRecordValues(false);
        },

        /**
		 * Ajusta la vista para visualizar los datos de un registro
		 */
        onView: function() {
            this._viewOptions();
        },

        /**
		 * Cambia las opciones de edición disponibles en la vista de detalles de un registro
		 */
        _editOptions: function() {
            /** @type {JSONModel} OSSILO Referencia al modelo CENTER */
            var ossilo = this.getView().getModel("OSSILO");

            ossilo.setProperty("/modify/", false);
            ossilo.setProperty("/delete/", false);
            ossilo.setProperty("/save/", true);
            ossilo.setProperty("/cancel/", true);

            this._editRecordValues(true);
        },

        /**
		 * Ajusta la vista para editar los datos de un registro
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        onEdit: function(oEvent) {
            this._editOptions();
        },

        /**
		 * Verifica si el registro seleccionado tiene algún cambio con respecto a sus valores originales
		 * @return {Boolean} Devuelve "true" el registro cambió, y "false" si no cambió
		 */
        _recordChanged: function() {
            var ossilo = this.getView().getModel("OSSILO"),
                flag = false;

            ossilo.setProperty("/changes/", {});

            if (ossilo.getProperty("/silo_number/value") !==
				ossilo.getProperty(ossilo.getProperty("/selectedRecordPath/") + "/silo_number")) {
                ossilo.setProperty("/changes/silo_number/", ossilo.getProperty("/silo_number/value"));
                flag = true;
            }

            if (ossilo.getProperty("/height/value").toString() !==
				ossilo.getProperty(ossilo.getProperty("/selectedRecordPath/") + "/height")) {
                ossilo.setProperty("/changes/height/", ossilo.getProperty("/height/value").toString());
                flag = true;
            }

            if (ossilo.getProperty("/diameter/value").toString() !==
				ossilo.getProperty(ossilo.getProperty("/selectedRecordPath/") + "/diameter")) {

                if(ossilo.getProperty("/changes/diameter/") === undefined){
                    ossilo.setProperty("/changes/diameter/", {});
                }

                ossilo.setProperty("/changes/diameter/", ossilo.getProperty("/diameter/value").toString());
                flag = true;
            }

            if (ossilo.getProperty("/total_capacity_1/value").toString() !==
				ossilo.getProperty(ossilo.getProperty("/selectedRecordPath/") + "/total_capacity_1")) {

                if(ossilo.getProperty("/changes/total_capacity_1/") === undefined){
                    ossilo.setProperty("/changes/total_capacity_1/", {});
                }

                ossilo.setProperty("/changes/total_capacity_1/", ossilo.getProperty("/total_capacity_1/value").toString());
                flag = true;
            }

            if (ossilo.getProperty("/total_rings_quantity/value").toString() !==
				ossilo.getProperty(ossilo.getProperty("/selectedRecordPath/") + "/total_rings_quantity")) {

                if(ossilo.getProperty("/changes/total_rings_quantity/") === undefined){
                    ossilo.setProperty("/changes/total_rings_quantity/", {});
                }

                ossilo.setProperty("/changes/total_rings_quantity/", ossilo.getProperty("/total_rings_quantity/value").toString());
                flag = true;
            }

            if (ossilo.getProperty("/rings_height/value").toString() !==
				ossilo.getProperty(ossilo.getProperty("/selectedRecordPath/") + "/rings_height")) {

                if(ossilo.getProperty("/changes/rings_height/") === undefined){
                    ossilo.setProperty("/changes/rings_height/", {});
                }

                ossilo.setProperty("/changes/rings_height/", ossilo.getProperty("/rings_height/value").toString());
                flag = true;
            }

            if (ossilo.getProperty("/cone_degrees/value").toString() !==
				ossilo.getProperty(ossilo.getProperty("/selectedRecordPath/") + "/cone_degrees")) {

                if(ossilo.getProperty("/changes/cone_degrees/") === undefined){
                    ossilo.setProperty("/changes/cone_degrees/", {});
                }

                ossilo.setProperty("/changes/cone_degrees/", ossilo.getProperty("/cone_degrees/value").toString());
                flag = true;
            }

            /*var original = SILO.getProperty("/sheds/originalRecords/"),
				actual = SILO.getProperty("/sheds/records/"),
				aux = [];

				original.forEach(function(element, index){
					if(element.ASSOCIATED !== actual[index].ASSOCIATED){
						aux.push({
							ID: actual[index].ID,
							ASSOCIATED: actual[index].ASSOCIATED
						});
					}
				});

				if(aux.length > 0){
					SILO.setProperty("/changes/SHEDS/", aux);
					flag = true;
				}*/

            if (!flag) this.onToast("No se detectaron cambios");
            return flag;
        },

        /**
		 * Cancela la edición de un silo
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        onCancelEdit: function(oEvent) {

            this.onView();
        },

        /**
		 * Solicita al servicio correspondiente actualizar un silo
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        onUpdate: function(oEvent) {

 			if (this._validRecord() && this._recordChanged()) {

                var ossilo = this.getView().getModel("OSSILO"),
                    util = this.getView().getModel("util"),
                    that = this,
                    serviceUrl= util.getProperty("/serviceUrl");
                $.ajax({
                    type: "PUT",
                    contentType: "application/json",
                    data: JSON.stringify({
                        "silo_id": ossilo.getProperty(ossilo.getProperty("/selectedRecordPath/") + "/silo_id"),
                        "silo_number": ossilo.getProperty("/silo_number/value"),
                        "height": ossilo.getProperty("/height/value"),
                        "diameter": ossilo.getProperty("/diameter/value"),
                        "total_capacity_1": ossilo.getProperty("/total_capacity_1/value"),
                        "total_rings_quantity": ossilo.getProperty("/total_rings_quantity/value"),
                        "rings_height": ossilo.getProperty("/rings_height/value"),
                        "cone_degrees": ossilo.getProperty("/cone_degrees/value"),
                    }),
                    url: serviceUrl+"/silo/",
                    dataType: "json",
                    async: true,
                    success: function(data) {

                        util.setProperty("/busy/", false);
                        that._resetRecordValues();
                        that._viewOptions();
                        that.onToast(that.getI18n().getText("OS.recordUpdated"));
                        that.getRouter().navTo("ossilo", {}, true /*no history*/ );

                    },
                    error: function(request, status, error) {
                        that.onToast("Error de comunicación");
                        console.log("Read failed");
                    }
                });
            }
        },
        onAssociatedSheds: function(oEvent){
            var SILO = this.getView().getModel("OSSILO");

            if(SILO.getProperty("/modify/") === true){
                /*console.log(showShedsDlg);
				showShedsDlg.open();*/
                this._openShowSheds();
            }else{
                /*console.log(selectShedsDlg);
				selectShedsDlg.open();*/
                this._openSelectSheds();
            }
        },
        /**
		 * Solicita al servicio correspondiente eliminar un galpon
		 * @param  {Controller} that         Referencia al controlador que llama esta función
		 * @param  {JSONModel} dummy         Referencia al modelo "dummy"
		 * @param  {JSONModel} PARTNERSHIP Referencia al modelo "PARTNERSHIP"
		 */
        onDelete: function(that, util, ossilo) {

            var ossilo = that.getView().getModel("OSSILO"),
                serviceUrl = util.getProperty("/serviceUrl"),
                json = {
                    "silo_id": ossilo.getProperty(ossilo.getProperty("/selectedRecordPath/") + "/silo_id")
                },
                settings = {
                    async: true,
                    url: serviceUrl + "/silo",
                    method: "DELETE",
                    data: JSON.stringify(json),
                    dataType: "json",
                    contentType: "application/json",
                    success: function(res) {
                        util.setProperty("/busy/", false);
                        that.onToast(that.getI18n().getText("El silo se ha eliminado"));
                        that.getRouter().navTo("ossilo", {}, true);
                    },
                    error: function(err) {
                        that.onToast("Error de comunicación");
                        console.log("Read failed");
                    }
                };
            util.setProperty("/busy/", true);
            //Realiza la llamada ajax
            $.ajax(settings);
        }

        /*,
		onAssociatedShedsClose: function(oEvent){
			var SILO = this.getView().getModel("SILO");
			console.log("entra en cerrar galpones asociados");
			if(SILO.getProperty("/modify/") === true){
				this._closeShowSheds();
			}else{
				this._closeSelectSheds();
			}
		},
		onExit: function() {
			if (showShedsDlg) {
				showShedsDlg.destroy(true);
			}

			if (selectShedsDlg) {
				selectShedsDlg.destroy(true);
			}
		}*/
    });
});
