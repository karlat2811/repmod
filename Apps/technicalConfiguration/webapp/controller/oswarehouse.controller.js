sap.ui.define([
    "technicalConfiguration/controller/BaseController"
], function(BaseController) {
    "use strict";

    return BaseController.extend("technicalConfiguration.controller.oswarehouse", {

        onInit: function() {
            //ruta para la vista principal de almacenes
            this.getOwnerComponent().getRouter().getRoute("oswarehouse").attachPatternMatched(this._onRouteMatched, this);
            //ruta para la vista de creación de un almacén
            this.getOwnerComponent().getRouter().getRoute("oswarehouse_Create").attachPatternMatched(this._onCreateMatched, this);
            //ruta para la vista de detalles de un almacén
            this.getOwnerComponent().getRouter().getRoute("oswarehouse_Record").attachPatternMatched(this._onRecordMatched, this);
        },

        /**
		 * Coincidencia de ruta para acceder a la vista principal
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        _onRouteMatched: function(oEvent) {
            /**
			 * @type {Controller} that          Referencia a este controlador
			 * @type {JSONModel} dummy          Referencia al modelo "dummy"
			 * @type {JSONModel} OS             Referencia al modelo "OS"
			 * @type {JSONModel} PARTNERSHIP  Referencia al modelo "PARTNERSHIP"
			 * @type {JSONModel} BROILERSFARM Referencia al modelo "BROILERSFARM"
			 * @type {JSONModel} WAREHOUSE       Referencia al modelo "WAREHOUSE"
			 */
            var that = this,
                util = this.getView().getModel("util"),
                ospartnership = this.getView().getModel("OSPARTNERSHIP"),
                osfarm = this.getView().getModel("OSFARM"),
                oswarehouse = this.getView().getModel("OSWAREHOUSE");

            //dependiendo del dispositivo, establece la propiedad "phone"
            util.setProperty("/phone/",
                this.getOwnerComponent().getContentDensityClass() === "sapUiSizeCozy");

            ospartnership.setProperty("/settings/tableMode", "SingleSelect");
            osfarm.setProperty("/settings/tableMode", "SingleSelect");
            oswarehouse.setProperty("/settings/tableMode", "None");

            //si la entidad seleccionada antes de acceder a esta vista es diferente a WAREHOUSE
            if (util.getProperty("/selectedEntity") !== "oswarehouse") {

                //establece WAREHOUSE como la entidad seleccionada
                util.setProperty("/selectedEntity", "oswarehouse");

                //establece el tab de la tabla PARTNERSHIP como el tab seleccionado
                this.getView().byId("tabBar").setSelectedKey(this.getView().getId() + "--" + "partnershipFilter");

                //borra cualquier selección que se haya hecho en la tabla PARTNERSHIP
                this.getView().byId("partnershipTable").removeSelections(true);

                //borra cualquier selección que se haya hecho en la tabla BROILERSFARM
                this.getView().byId("farmTable").removeSelections(true);

                //establece que no hay ningún registro PARTNERSHIP seleccionado
                ospartnership.setProperty("/selectedRecordPath/", "");

                //establece que no hay ningún registro BROILERSFARM seleccionado
                osfarm.setProperty("/selectedRecordPath/", "");

                osfarm.setProperty("/records/", []);

                oswarehouse.setProperty("/records/", []);

                //deshabilita el tab de la tabla de registros BROILERSFARM
                osfarm.setProperty("/settings/enabledTab", false);

                //deshabilita el tab de la tabla de registros WAREHOUSE
                oswarehouse.setProperty("/settings/enabledTab", false);

                //deshabilita la opción de crear un registro WAREHOUSE
                oswarehouse.setProperty("/new", false);

                //obtiene los registros de PARTNERSHIP
                sap.ui.controller("technicalConfiguration.controller.ospartnership").onRead(that, util, ospartnership);

            } else if (ospartnership.getProperty("/selectedRecordPath/") !== "" &&
				osfarm.getProperty("/selectedRecordPath/") !== "") {

                //habilita el tab de la tabla de registros BROILERSFARM
                osfarm.setProperty("/settings/enabledTab", true);

                //habilita el tab de la tabla de registros WAREHOUSE
                oswarehouse.setProperty("/settings/enabledTab", true);

                //habilita la opción de crear un registro WAREHOUSE
                oswarehouse.setProperty("/new", true);

                //obtiene los registros de WAREHOUSE
                this.onRead(that, util, ospartnership, osfarm, oswarehouse);
            }
        },
        onPartnershipSearch: function(oEvent){
            var aFilters = [],
                sQuery = oEvent.getSource().getValue(),
                binding = this.getView().byId("partnershipTable").getBinding("items");

            if (sQuery && sQuery.length > 0) {
                /** @type {Object} filter1 Primer filtro de la búsqueda */
                var filter1 = new sap.ui.model.Filter("name", sap.ui.model.FilterOperator.Contains, sQuery);
                aFilters = new sap.ui.model.Filter([filter1]);
            }

            //se actualiza el binding de la lista
            binding.filter(aFilters);

        },
        onFarmSearch: function(oEvent){
            var aFilters = [],
                sQuery = oEvent.getSource().getValue(),
                binding = this.getView().byId("farmTable").getBinding("items");
            if (sQuery && sQuery.length > 0) {
                /** @type {Object} filter1 Primer filtro de la búsqueda */
                var filter1 = new sap.ui.model.Filter("name", sap.ui.model.FilterOperator.Contains, sQuery);
                aFilters = new sap.ui.model.Filter([filter1]);
            }

            //se actualiza el binding de la lista
            binding.filter(aFilters);

        },
        onWarehouseSearch: function(oEvent) {
            var aFilters = [],
                sQuery = oEvent.getSource().getValue(),
                binding = this.getView().byId("warehouseTable").getBinding("items");

            if (sQuery && sQuery.length > 0) {
                /** @type {Object} filter1 Primer filtro de la búsqueda */
                var filter1 = new sap.ui.model.Filter("name", sap.ui.model.FilterOperator.Contains, sQuery);
                aFilters = new sap.ui.model.Filter([filter1]);
            }

            //se actualiza el binding de la lista
            binding.filter(aFilters);

        },

        /**
		 * Regresa a la vista principal de la entidad seleccionada actualmente
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        onNavBack: function(oEvent) {
            /** @type {JSONModel} util Referencia al modelo "OS" */
            var util = this.getView().getModel("util");

            this._resetRecordValues();
            this.getRouter().navTo(util.getProperty("/selectedEntity"), {}, true);
        },

        /**
		 * Selecciona un registro PARTNERSHIP y habilita la tabla de registros BROILERSFARM
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        onSelectPartnershipRecord: function(oEvent) {
            /**
			 * @type {Controller} that          Referencia a este controlador
			 * @type {JSONModel} dummy          Referencia al modelo "dummy"
			 * @type {JSONModel} PARTNERSHIP  Referencia al modelo "PARTNERSHIP"
			 * @type {JSONModel} BROILERSFARM Referencia al modelo "BROILERSFARM"
			 * @type {JSONModel} WAREHOUSE       Referencia al modelo "WAREHOUSE"
			 */
            var that = this,
                util = this.getView().getModel("util"),
                ospartnership = this.getView().getModel("OSPARTNERSHIP"),
                osfarm = this.getView().getModel("OSFARM"),
                oswarehouse = this.getView().getModel("OSWAREHOUSE");

            //guarda la ruta del registro PARTNERSHIP que fue seleccionado
            ospartnership.setProperty("/selectedRecordPath/", oEvent.getSource()["_aSelectedPaths"][0]);
            ospartnership.setProperty("/selectedRecord/", ospartnership.getProperty(ospartnership.getProperty("/selectedRecordPath/")));

            osfarm.setProperty("/selectedRecordPath/", "");
            osfarm.setProperty("/selectedRecord/", {});

            //habilita el tab de la tabla de registros BROILERSFARM
            osfarm.setProperty("/settings/enabledTab", true);

            //deshabilita el tab de la tabla de registros WAREHOUSE
            oswarehouse.setProperty("/settings/enabledTab", false);

            //deshabilita la opción de crear un registro WAREHOUSE
            oswarehouse.setProperty("/new", false);

            //establece el tab de la tabla BROILERSFARM como el tab seleccionado
            this.getView().byId("tabBar").setSelectedKey(this.getView().getId() + "--" + "farmFilter");

            //borra cualquier selección que se haya hecho en la tabla BROILERSFARM
            this.getView().byId("farmTable").removeSelections(true);

            //obtiene los registros de BROILERSFARM
            sap.ui.controller("technicalConfiguration.controller.osfarm").onRead(that, util, ospartnership, osfarm);
        },

        /**
		 * Selecciona un registro BROILERSFARM y habilita la tabla de registros WAREHOUSE
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        onSelectFarmRecord: function(oEvent) {
            /**
			 * @type {Controller} that          Referencia a este controlador
			 * @type {JSONModel} dummy          Referencia al modelo "dummy"
			 * @type {JSONModel} PARTNERSHIP  Referencia al modelo "PARTNERSHIP"
			 * @type {JSONModel} BROILERSFARM Referencia al modelo "BROILERSFARM"
			 * @type {JSONModel} WAREHOUSE       Referencia al modelo "WAREHOUSE"
			 */
            var that = this,
                util = this.getView().getModel("util"),
                ospartnership = this.getView().getModel("OSPARTNERSHIP"),
                osfarm = this.getView().getModel("OSFARM"),
                oswarehouse = this.getView().getModel("OSWAREHOUSE");

            //guarda la ruta del registro BROILERSFARM que fue seleccionado
            osfarm.setProperty("/selectedRecordPath/", oEvent.getSource()["_aSelectedPaths"][0]);

            osfarm.setProperty("/selectedRecord/", osfarm.getProperty(osfarm.getProperty("/selectedRecordPath/")));

            //habilita el tab de la tabla de registros WAREHOUSE
            oswarehouse.setProperty("/settings/enabledTab", true);

            //habilita la opción de crear un registro WAREHOUSE
            oswarehouse.setProperty("/new", true);

            //establece el tab de la tabla WAREHOUSE como el tab seleccionado
            this.getView().byId("tabBar").setSelectedKey(this.getView().getId() + "--" + "warehouseFilter");

            //obtiene los registros de WAREHOUSE
            this.onRead(that, util, ospartnership, osfarm, oswarehouse);
        },

        /**
		 * Obtiene todos los registros de BROILERSFARM, dado un cliente y una sociedad
		 * @param  {Controller} that          Referencia al controlador que llama esta función
		 * @param  {JSONModel} dummy          Referencia al modelo "dummy"
		 * @param  {JSONModel} PARTNERSHIP  Referencia al modelo "PARTNERSHIP"
		 * @param  {JSONModel} BROILERSFARM Referencia al modelo "BROILERSFARM"
		 * @param  {JSONModel} WAREHOUSE       Referencia al modelo "WAREHOUSE"
		 */
        onRead: function(that, util, ospartnership, osfarm, oswarehouse) {
            /** @type {Object} settings opciones de la llamada a la función ajax */
            var serviceUrl = util.getProperty("/serviceUrl");
            var farm_id = osfarm.getProperty(osfarm.getProperty("/selectedRecordPath/") + "/farm_id");

            var settings = {
                type: "POST",
                contentType: "application/json",
                data: JSON.stringify({
                    "farm_id": farm_id,
                }),
                url: serviceUrl+"/warehouse/findWarehouseByFarm",
                dataType: "json",
                async: true,
                success: function(res) {
                    console.log(res.data);
                    util.setProperty("/busy/", false);
                    oswarehouse.setProperty("/records/", res.data);
                    console.log(oswarehouse);
                },
                error: function(err) {
                    util.setProperty("/error/status", err.status);
                    util.setProperty("/error/statusText", err.statusText);
                    //that.onConnectionError();
                }
            };
            util.setProperty("/busy/", true);
            //borra los registros WAREHOUSE que estén almacenados actualmente
            oswarehouse.setProperty("/records/", []);
            //realiza la llamada ajax
            $.ajax(settings);
        },

        /**
		 * Navega a la vista para crear un nuevo registro
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        onNewRecord: function(oEvent) {
            this.getRouter().navTo("oswarehouse_Create", {}, true);
        },

        /**
		 * Coincidencia de ruta para acceder a la creación de un registro
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        _onCreateMatched: function(oEvent) {
            //resetea y habilita los campos del formulario para su edición
            this._resetRecordValues();
            this._editRecordValues(true);
        },

        /**
		 * Resetea todos los valores existentes en el formulario del registro
		 */
        _resetRecordValues: function() {
            /** @type {JSONModel} WAREHOUSE Referencia al modelo "WAREHOUSE" */
            var oswarehouse = this.getView().getModel("OSWAREHOUSE");

            oswarehouse.setProperty("/code/value", "");
            oswarehouse.setProperty("/code/state", "None");
            oswarehouse.setProperty("/code/stateText", "");

            oswarehouse.setProperty("/name/value", "");
            oswarehouse.setProperty("/name/state", "None");
            oswarehouse.setProperty("/name/stateText", "");
        },

        /**
		 * Habilita/deshabilita la edición de los datos de un registro
		 * @param  {Boolean} edit "true" si habilita la edición, "false" si la deshabilita
		 */
        _editRecordValues: function(edit) {
            /** @type {JSONModel} WAREHOUSE Referencia al modelo "WAREHOUSE" */
            var oswarehouse = this.getView().getModel("OSWAREHOUSE");

            oswarehouse.setProperty("/code/editable", edit);
            oswarehouse.setProperty("/code/required", edit);

            oswarehouse.setProperty("/name/editable", edit);
            oswarehouse.setProperty("/name/required", edit);
        },

        /**
		 * Valida la correctitud de los datos existentes en el formulario del registro
		 * @return {Boolean} Devuelve "true" si los datos son correctos, y "false" si son incorrectos
		 */
        _validRecord: function() {
            /**
			 * @type {JSONModel} WAREHOUSE Referencia al modelo "WAREHOUSE"
			 * @type {Boolean} flag             "true" si los datos son válidos, "false" si no lo son
			 */
            var oswarehouse = this.getView().getModel("OSWAREHOUSE"),
                flag = true;

            if (oswarehouse.getProperty("/code/value") === "") {
                flag = false;
                oswarehouse.setProperty("/code/state", "Error");
                oswarehouse.setProperty("/code/stateText", this.getI18n().getText("enter.FIELD"));
            } else {
                oswarehouse.setProperty("/code/state", "None");
                oswarehouse.setProperty("/code/stateText", "");
            }

            if (oswarehouse.getProperty("/name/value") === "") {
                flag = false;
                oswarehouse.setProperty("/name/state", "Error");
                oswarehouse.setProperty("/name/stateText", this.getI18n().getText("enter.FIELD"));
            } else {
                oswarehouse.setProperty("/name/state", "None");
                oswarehouse.setProperty("/name/stateText", "");
            }

            return flag;
        },

        /**
		 * Solicita al servicio correspondiente crear un registro WAREHOUSE,
		 * dado un cliente, una sociedad y una granja
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        onCreate: function(oEvent) {
            //Si el registro que se desea crear es válido
            if (this._validRecord()) {
                /**
				 * @type {JSONModel} WAREHOUSE Referencia al modelo "WAREHOUSE"
				 * @type {JSONModel} dummy    Referencia al modelo "dummy"
				 * @type {Controller} that    Referencia a este controlador
				 * @type {Object} json        Objeto a enviar en la solicitud
				 * @type {Object} settings    Opciones de la llamada a la función ajax
				 */
                var ospartnership = this.getView().getModel("OSPARTNERSHIP"),
                    osfarm = this.getView().getModel("OSFARM"),
                    oswarehouse = this.getView().getModel("OSWAREHOUSE"),
                    util = this.getView().getModel("util"),
                    serviceUrl = util.getProperty("/serviceUrl"),
                    that = this,
                    json = {
                        "partnership_id": ospartnership.getProperty(ospartnership.getProperty("/selectedRecordPath/") + "/partnership_id"),
                        "farm_id": osfarm.getProperty(osfarm.getProperty("/selectedRecordPath/") + "/farm_id"),
                        "name": oswarehouse.getProperty("/name/value"),
                        "code": oswarehouse.getProperty("/code/value")
                    },
                    settings = {
                        async: true,
                        url: serviceUrl + "/warehouse",
                        method: "POST",
                        data: JSON.stringify(json),
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function(res) {
                            util.setProperty("/busy/", false);
                            that._resetRecordValues();
                            that.onToast(that.getI18n().getText("OS.recordCreated"));
                            that.getRouter().navTo("oswarehouse", {}, true);
                        },
                        error: function(err) {
                            console.log(err);
                            util.setProperty("/error/status", err.status);
                            util.setProperty("/error/statusText", err.statusText);
                        }
                    };
                util.setProperty("/busy/", true);
                //realiza la llamada ajax
                $.ajax(settings);
            }
        },

        /**
		 * Coincidencia de ruta para acceder a los detalles de un registro
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        _onRecordMatched: function(oEvent) {
            //Establece las opciones disponibles al visualizar el registro
            this._viewOptions();
        },

        /**
		 * Cambia las opciones de visualización disponibles en la vista de detalles de un registro
		 */
        _viewOptions: function() {
            /**
			 * @type {JSONModel} OS       Referencia al modelo "OS"
			 * @type {JSONModel} WAREHOUSE Referencia al modelo "WAREHOUSE"
			 */
            var util = this.getView().getModel("util"),
                oswarehouse = this.getView().getModel("OSWAREHOUSE");

            if (util.getProperty("/selectedEntity/") === "oswarehouse") {
                oswarehouse.setProperty("/modify/", true);
                oswarehouse.setProperty("/delete/", true);
            } else {
                oswarehouse.setProperty("/modify/", false);
                oswarehouse.setProperty("/delete/", false);
            }

            oswarehouse.setProperty("/save/", false);
            oswarehouse.setProperty("/cancel/", false);

            this._editRecordValues(false);
        },

        /**
		 * Ajusta la vista para visualizar los datos de un registro
		 */
        onView: function() {
            this._viewOptions();
        },

        /**
		 * Cambia las opciones de edición disponibles en la vista de detalles de un registro
		 */
        _editOptions: function() {
            /** @type {JSONModel} WAREHOUSE Referencia al modelo WAREHOUSE */
            var oswarehouse = this.getView().getModel("OSWAREHOUSE");

            oswarehouse.setProperty("/modify/", false);
            oswarehouse.setProperty("/delete/", false);
            oswarehouse.setProperty("/save/", true);
            oswarehouse.setProperty("/cancel/", true);

            this._editRecordValues(true);
        },

        /**
		 * Ajusta la vista para editar los datos de un registro
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        onEdit: function(oEvent) {
            this._editOptions();
        },

        /**
		 * Verifica si el registro seleccionado tiene algún cambio con respecto a sus valores originales
		 * @return {Boolean} Devuelve "true" el registro cambió, y "false" si no cambió
		 */
        _recordChanged: function() {
            /**
			 * @type {JSONModel} WAREHOUSE Referencia al modelo "WAREHOUSE"
			 * @type {Boolean} flag       "true" si el registro cambió, "false" si no cambió
			 */
            var oswarehouse = this.getView().getModel("OSWAREHOUSE"),
                flag = false;

            if (oswarehouse.getProperty("/order_/value") !==
			oswarehouse.getProperty(oswarehouse.getProperty("/selectedRecordPath/") + "/order_")) {
                flag = true;
            }

            if (oswarehouse.getProperty("/name/value") !==
			oswarehouse.getProperty(oswarehouse.getProperty("/selectedRecordPath/") + "/name")) {
                flag = true;
            }

            if (!flag) this.onToast("No se detectaron cambios");

            return flag;
        },

        /**
		 * Solicita al servicio correspondiente actualizar un registro WAREHOUSE
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        onUpdate: function(oEvent) {
            /**
			 * Si el registro que se quiere actualizar es válido y hubo algún cambio
			 * con respecto a sus datos originales
			 */

 			if (this._validRecord() && this._recordChanged()) {

                /**
				 * @type {JSONModel} WAREHOUSE Referencia al modelo "WAREHOUSE"
				 * @type {JSONModel} dummy    Referencia al modelo "dummy"
				 * @type {Controller} that    Referencia a este controlador
				 */
                var oswarehouse = this.getView().getModel("OSWAREHOUSE"),
                    ospartnership = this.getView().getModel("OSPARTNERSHIP"),
                    util = this.getView().getModel("util"),
                    that = this,
                    osfarm = this.getView().getModel("OSFARM"),
                    serviceUrl= util.getProperty("/serviceUrl");

                $.ajax({
                    type: "PUT",
                    contentType: "application/json",
                    data: JSON.stringify({
                        "partnership_id": ospartnership.getProperty(ospartnership.getProperty("/selectedRecordPath/") + "/partnership_id"),
                        "farm_id": osfarm.getProperty(osfarm.getProperty("/selectedRecordPath/") + "/farm_id"),
                        "warehouse_id": oswarehouse.getProperty(oswarehouse.getProperty("/selectedRecordPath/") + "/warehouse_id"),
                        "name": oswarehouse.getProperty("/name/value"),
                        "code": oswarehouse.getProperty("/code/value")
                    }),
                    url: serviceUrl+"/warehouse/",
                    dataType: "json",
                    async: true,
                    success: function(data) {

                        util.setProperty("/busy/", false);
                        that._resetRecordValues();
                        that._viewOptions();
                        that.onToast(that.getI18n().getText("OS.recordUpdated"));
                        that.getRouter().navTo("oswarehouse", {}, true /*no history*/ );

                    },
                    error: function(request, status, error) {
                        that.onToast("Error de comunicación");
                        console.log("Read failed");
                    }
                });
            }
        },

        /**
		 * Cancela la edición de un registro WAREHOUSE
		 * @param  {Event} oEvent Evento que llamó esta función
		 */
        onCancelEdit: function(oEvent) {
            /** @type {JSONModel} WAREHOUSE Referencia al modelo WAREHOUSE */
            this.onView();
        },

        /**
		 * Solicita al servicio correspondiente eliminar un galpon
		 * @param  {Controller} that         Referencia al controlador que llama esta función
		 * @param  {JSONModel} dummy         Referencia al modelo "dummy"
		 * @param  {JSONModel} PARTNERSHIP Referencia al modelo "PARTNERSHIP"
		 */
        onDelete: function(that, util, oswarehouse) {

            /**
			 * @type {Object} json     Objeto a enviar en la solicitud
			 * @type {Object} settings Opciones de la llamada a la función ajax
			 */

            var ospartnership = that.getView().getModel("OSPARTNERSHIP"),
                osfarm = that.getView().getModel("OSFARM"),
                serviceUrl = util.getProperty("/serviceUrl"),
                json = {
                    "client_id": 1,
                    "partnership_id": ospartnership.getProperty(ospartnership.getProperty("/selectedRecordPath/") + "/partnership_id"),
                    "farm_id": osfarm.getProperty(osfarm.getProperty("/selectedRecordPath/") + "/farm_id"),
                    "warehouse_id": oswarehouse.getProperty(oswarehouse.getProperty("/selectedRecordPath/") + "/warehouse_id")
                },
                settings = {
                    async: true,
                    url: serviceUrl + "/warehouse",
                    method: "DELETE",
                    data: JSON.stringify(json),
                    dataType: "json",
                    contentType: "application/json",
                    success: function(res) {
                        util.setProperty("/busy/", false);
                        that.onToast(that.getI18n().getText("El almacén se ha eliminado"));
                        that.getRouter().navTo("oswarehouse", {}, true);
                    },
                    error: function(err) {
                        that.onToast("Error de comunicación");
                        console.log("Read failed");
                    }
                };
            util.setProperty("/busy/", true);
            //Realiza la llamada ajax
            $.ajax(settings);
        }
    });
});
