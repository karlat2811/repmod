sap.ui.define([
    "technicalConfiguration/controller/BaseController",
    "sap/ui/model/json/JSONModel",
    "sap/ui/model/Sorter",
    "sap/ui/model/Filter",
    "sap/m/Dialog",
    "sap/m/Button"
], function(BaseController, JSONModel,Sorter,Filter, Dialog, Button) {
    "use strict";
    const that2 = this;
    return BaseController.extend("technicalConfiguration.controller.txposturecurve", {

        onInit: function() {
            //ruta para la vista principal
            this.getOwnerComponent().getRouter().getRoute("txposturecurve").attachPatternMatched(this._onRouteMatched, this);
            //ruta para la vista de detalles de un registro
            //this.getOwnerComponent().getRouter().getRoute("mdprocess_Record").attachPatternMatched(this._onRecordMatched, this);
            //ruta para la vista de creación de un registro
            this.getOwnerComponent().getRouter().getRoute("txposturecurve_Create").attachPatternMatched(this._onCreateMatched, this);
        },

        /**
         * Coincidencia de ruta para acceder a la vista principal
         * @param  {Event} oEvent Evento que llamó esta función
         */
        _onRouteMatched: function(oEvent) {
            var that = this,
                util = this.getView().getModel("util"),
                txposturecurve = this.getView().getModel("TXPOSTURECURVE"),
                mdbreed = this.getView().getModel("MDBREED");


            mdbreed.setProperty("/selectedRecord/", "");

            mdbreed.attachRequestCompleted(function(){
                //establece el tab de la tabla Registro como el tab seleccionado
                that.getView().byId("tabBar").setSelectedKey("kregisterCurveFilter");

                //Borra cualquier seleccion que se haya hecho en la tabla CurveTable
                that.getView().byId("curveTable").removeSelections(true);

            });

            txposturecurve.setProperty("/settings/enabledTab", false);

            //dependiendo del dispositivo, establece la propiedad "phone"
            this.getView().getModel("util").setProperty("/phone/",
                this.getOwnerComponent().getContentDensityClass() === "sapUiSizeCozy");

            //establece process como la entidad seleccionada
            util.setProperty("/selectedEntity/", "txposturecurve");
            txposturecurve.setProperty("/postureRecords", []);
            txposturecurve.setProperty("/newRecords", []);
            txposturecurve.setProperty("/curves", []);
            txposturecurve.setProperty("/breedRecords", []);

            txposturecurve.setProperty("/create", true);
            txposturecurve.setProperty("/delete", false);
            txposturecurve.setProperty("/save", false);




            //obtiene los registros de MDPROCESS
            this.onRead(that, util, txposturecurve);
            util.setProperty("/busy/", true);

        },

        validateFloatInput: function (o) {
            let input= o.getSource();
            let floatLength=10,
                intLength = 10;
            console.log("entro en la funcion v");
            let value = input.getValue();
            let regex = new RegExp(`/^([0-9]{1,${intLength}})([.][0-9]{0,${floatLength}})?$/`);
            if (regex.test(value)) {
                input.setValueState("None");
                input.setValueStateText("");
                return true;
            }
            else {
                let pNumber = 0;
                let aux = value
                    .split("")
                    .filter(char => {
                        if (/^[0-9.]$/.test(char)) {
                            if (char !== ".") {
                                return true;
                            }
                            else {
                                if (pNumber === 0) {
                                    pNumber++;
                                    return true;
                                }
                            }
                        }
                    })
                    .join("")
                    .split(".");
                value = aux[0].substring(0, intLength);

                if (aux[1] !== undefined) {
                    value += "." + aux[1].substring(0, floatLength);
                }
                input.setValue(value);
                return false;
            }
        },



        validateIntInput: function (o) {
            let input= o.getSource();
            let length = 10;
            let value = input.getValue();
            let regex = new RegExp(`/^[0-9]{1,${length}}$/`);

            if (regex.test(value)) {
                return true;
            }
            else {
                let aux = value
                    .split("")
                    .filter(char => {
                        if (/^[0-9]$/.test(char)) {
                            if (char !== ".") {
                                return true;
                            }
                        }
                    })
                    .join("");
                value = aux.substring(0, length);
                input.setValue(value);
                return false;
            }
        },


        onEdit: function() {

          

            this.oTable = this.getView().byId("idProductsTable");
            this.oReadOnlyTemplate = this.byId("idProductsTable").removeItem(0);
            this.rebindTable(this.oReadOnlyTemplate, "Navigation");
            // var that2 = this;

            let oEditableTemplate = new sap.m.ColumnListItem({
                cells: [
                    new sap.m.ObjectIdentifier({
                        title: "{TXPOSTURECURVE>week}"
                    }), new sap.m.Input({
                        value: "{TXPOSTURECURVE>theorical_performance}",
                        description: "Huevos",
                        liveChange: this.validateFloatInput
                    })
                ]
            });

            // this.aProductCollection = jQuery.extend(true, [], this.oModel.getProperty("/ProductCollection"));
            this.txposturecurveI = JSON.parse(JSON.stringify(this.getView().getModel("TXPOSTURECURVE").getProperty("/postureRecords")));
            console.log("el modelo curva de postura");
            console.log(this.txposturecurveI);
            this.byId("editButton").setVisible(false);
            this.byId("saveButton").setVisible(true);
            this.byId("cancelButton").setVisible(true);
            this.rebindTable(oEditableTemplate, "Edit");
        },

        rebindTable: function(oTemplate, sKeyboardMode) {
            this.oTable.bindItems({
                path: "TXPOSTURECURVE>/postureRecords",
                template: oTemplate
            }).setKeyboardMode(sKeyboardMode);
        },

        onSave: function() {
            console.log("El original");
            console.log(this.txposturecurveI);
            this.byId("saveButton").setVisible(false);
            this.byId("cancelButton").setVisible(false);
            this.byId("editButton").setVisible(true);
            console.log("El nuevo");
            // console.log(this.oReadOnlyTemplate);
            let arra = this.oReadOnlyTemplate.oBindingContexts.TXPOSTURECURVE.oModel.oData.postureRecords;
            console.log("posicion 0");
            // console.log(parseFloat(arra[0].theorical_performance));
            let arra2 = [];
          
            var i = 0;

            arra.forEach(item => {
                if((item.theorical_performance) != this.txposturecurveI[i].theorical_performance) 
                {
                    arra2.push(item);
                }
                i++;
            });



            console.log("le array");
            console.log(arra2);
            const serverName = "/posture_curve";
          
            fetch(serverName, {
                method: "PUT",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify({arra2})


                // JSON.stringify({
                //   scenario_id: scenario_id,
                //   _date: scheduled_date,
                //   partnership_id: partnership_id,
                //   breed_id: breed_id
                // })
            })
                .then(
                    function(response) {
                        if (response.status !== 200) {
                            console.log("Looks like there was a problem. Status Code: " +
                    response.status);
                            return;
                        }

                        response.json().then(function(res) {
                            console.log("ressssssss:::::::");
                            console.log(res);
                            // mdprojected.setProperty("/records", res.data);
                            // mdprogrammed.setProperty("/product/records", res.product);
                            // mdprogrammed.setProperty("/slaughterhouse", res.slaughterhouse);
                            // console.log("aqui deberia salir slaughterhouse");
                            // console.log(mdprogrammed);
                            // mdprojected.refresh();
                            // mdprogrammed.refresh();
                            // console.log("modelo uwejyguaguybubfre");
                            // console.log(mdprogrammed);

                        });
                    }
                )
                .catch(function(err) {
                    console.log("Fetch Error :-S", err);
                });








            console.log("salio 0");
            console.log(arra);
            console.log("salio 1");
            console.log(arra2);
            console.log("la longitud");
            console.log(arra.length);
            this.rebindTable(this.oReadOnlyTemplate, "Navigation");

        },

        onCancel: function() {
            this.byId("cancelButton").setVisible(false);
            this.byId("saveButton").setVisible(false);
            this.byId("editButton").setVisible(true);

            this.getView().getModel("TXPOSTURECURVE").setProperty("/postureRecords", this.txposturecurveI);
            this.rebindTable(this.oReadOnlyTemplate, "Navigation");
        },





        onRead: function(that, util, txposturecurve) {
            console.log("Llegue a read");
            var serviceUrl= util.getProperty("/serviceUrl"),
                that = this;
            var settings = {
                url: serviceUrl+"/posture_curve",
                method: "GET",
                success: function(res) {
                    util.setProperty("/busy/", false);
                    txposturecurve.setProperty("/curves/", res.data);
                    //console.log(mdprocess);
                },
                error: function(err) {
                    util.setProperty("/error/status", err.status);
                    util.setProperty("/error/statusText", err.statusText);
                    //that.onConnectionError();
                }
            };

            util.setProperty("/busy/", true);
            //borra los registros OSPARTNERSHIP que estén almacenados actualmente
            txposturecurve.setProperty("/curves/", []);
            //realiza la llamada ajax
            $.ajax(settings);
        },
        onTabSelection: function(ev) {
            var selectedKey = ev.getSource().getSelectedKey();
            var viewId = this.getView().getId() + "--";
            var txposturecurve = this.getModel("TXPOSTURECURVE");

            console.log(selectedKey, viewId, "registerCurveFilter");
            if (selectedKey === "kregisterCurveFilter") {
                txposturecurve.setProperty("/create", true);
                txposturecurve.setProperty("/delete", false);
                txposturecurve.setProperty("/save", false);
            }

            if(selectedKey === "kpostureCurveFilter"){
                txposturecurve.setProperty("/create", false);
                txposturecurve.setProperty("/delete", true);
                txposturecurve.setProperty("/save", false);
            }

        },
        onCurveSearch: function(oEvent) {
            var aFilters = [],
                sQuery = oEvent.getSource().getValue(),
                binding = this.getView().byId("curveTable").getBinding("items");

            if (sQuery && sQuery.length > 0) {
            /** @type {Object} filter1 Primer filtro de la búsqueda */
                var filter1 = new sap.ui.model.Filter("name", sap.ui.model.FilterOperator.Contains, sQuery);
                aFilters = new sap.ui.model.Filter([filter1]);
            }

            //se actualiza el binding de la lista
            binding.filter(aFilters);

        },
        _onCreateMatched: function(oEvent) {
            this.loadBread();

        },
        loadBread: function(){

            var util = this.getModel("util"),
                service = util.getProperty("/serviceUrl"),
                txposturecurve = this.getModel("TXPOSTURECURVE");
            var settings = {
                url: service+"/breed/findBreedByCurve",
                method: "GET",
                success: function(res) {
                    console.log(res.data);
                    util.setProperty("/busy/", false);

                    if(res.data.length>0){
                        txposturecurve.setProperty("/breed_id/value", res.data[0].breed_id);
                    }
                    txposturecurve.setProperty("/breedRecords/", res.data);

                },
                error: function(err) {
                    util.setProperty("/error/status", err.status);
                    util.setProperty("/error/statusText", err.statusText);
                    //that.onConnectionError();
                }
            };
            console.log(util);
            util.setProperty("/busy/", true);
            txposturecurve.setProperty("/records/", []);
            $.ajax(settings);

        },
        onBreedChanged: function(){
            var txposturecurve = this.getView().getModel("TXPOSTURECURVE");
            console.log(this.getView().byId("selectBreed").getSelectedKey());
            txposturecurve.setProperty("/breed_id/value", this.getView().byId("selectBreed").getSelectedKey());

        },
        onSelectBreedRecord: function(oEvent) {
            console.log("Llegue");
            var that = this,
                util = this.getView().getModel("util"),
                mdbreed = this.getView().getModel("MDBREED"),
                txposturecurve = this.getView().getModel("TXPOSTURECURVE");

            //guarda la ruta del registro BROILERSFARM que fue seleccionado
            mdbreed.setProperty("/selectedRecordPath/", oEvent.getSource()["_aSelectedPaths"][0]);

            mdbreed.setProperty("/selectedRecord/", txposturecurve.getProperty(mdbreed.getProperty("/selectedRecordPath/")));
            console.log(mdbreed);
            //habilita el tab de la tabla de registros CENTER
            txposturecurve.setProperty("/settings/enabledTab", true);

            //habilita la opción
            txposturecurve.setProperty("/create", false);
            txposturecurve.setProperty("/delete", true);
            txposturecurve.setProperty("/save", false);

            //establece el tab de la tabla CENTER como el tab seleccionado
            this.getView().byId("tabBar").setSelectedKey("kpostureCurveFilter");

            //obtiene los registros de la curva de postura
            this.onPostureCurveRecords();
        },
        onPostureCurveRecords: function(){
            var util = this.getModel("util"),
                txposturecurve = this.getModel("TXPOSTURECURVE"),
                mdbreed = this.getModel("MDBREED");
            var serviceUrl= util.getProperty("/serviceUrl");
            var settings = {
                contentType: "application/json",
                dataType: "json",
                url: serviceUrl+"/posture_curve/findCurveByBreed",
                method: "POST",
                data: JSON.stringify({
                    "breed_id": mdbreed.getProperty("/selectedRecord/breed_id"),
                }),
                success: function(res) {
                    util.setProperty("/busy/", false);
                    txposturecurve.setProperty("/postureRecords/", res.data);
                    console.log(res.data);
                },
                error: function(err) {
                    util.setProperty("/error/status", err.status);
                    util.setProperty("/error/statusText", err.statusText);

                }
            };

            util.setProperty("/busy/", true);
            //borra los registros OSPARTNERSHIP que estén almacenados actualmente
            txposturecurve.setProperty("/postureRecords/", []);
            //realiza la llamada ajax
            $.ajax(settings);

        },
        onNewRecord: function(oEvent) {
    			this.getRouter().navTo("txposturecurve_Create", {}, true);
    		},
        saveRecuDates: function(oEvent) {
            var txposturecurve = this.getView().getModel("TXPOSTURECURVE");
            let numberOfActualRows = this.getView().byId("selectRecuDays").mProperties.selectedKey;
            let numberOfRows = this.getView().getModel("TXPOSTURECURVE").getProperty("/newRecords").length;
            let temp = this.getView().getModel("TXPOSTURECURVE").getProperty("/newRecords");
            console.log("nueva semanas" + numberOfActualRows);
            console.log("cantidad de semanas" + numberOfRows);
            if(numberOfActualRows > numberOfRows){
                if (temp != "") {
                    //todo hacer ciclo dependiendo de la cantidad a agregar
                    for(let i = 0; i < numberOfActualRows-numberOfRows; i++){
                        temp.push( {
                            "week": temp.length+1,
                            "breed_id": 0,
                            "theorical_performance": 0,
                            "historical_performance": 0,
                            "theorical_accum_mortality": 0,
                            "historical_accum_mortality": 0,
                            "theorical_uniformity": 0,
                            "historical_uniformity": 0,
                            "type_posture": "Joven"
                        });       
                    }
                    console.log(temp);
                }else {
                    temp = [{
                        "week": 1,
                        "breed_id": 0,
                        "theorical_performance": 0,
                        "historical_performance": 0,
                        "theorical_accum_mortality": 0,
                        "historical_accum_mortality": 0,
                        "theorical_uniformity": 0,
                        "historical_uniformity": 0,
                        "type_posture": "Joven"
                    }];       
                    for(let i = 0; i < numberOfActualRows-1; i++){
                        temp.push( {
                            "week": temp.length+1,
                            "breed_id": 0,
                            "theorical_performance": 0,
                            "historical_performance": 0,
                            "theorical_accum_mortality": 0,
                            "historical_accum_mortality": 0,
                            "theorical_uniformity": 0,
                            "historical_uniformity": 0,
                            "type_posture": "Joven"
                        });       
                    }
            
                    console.log(temp);
     
                }
            }else{
                if(numberOfActualRows < numberOfRows){
                    temp.splice(numberOfActualRows);
                    // this.getView().getModel("TXPOSTURECURVE").setProperty("/newRecords", temp);
                }
            }
    
            this.getView().getModel("TXPOSTURECURVE").setProperty("/newRecords", temp);
      
            // var txposturecurve = this.getView().getModel("TXPOSTURECURVE");
            // //console.log(txposturecurve);
            // if(this.byId("inputRecuDesc").getValue() !== "") {
            //     var day = parseInt(this.byId("selectRecuDays").getSelectedItem().getText(),10);

            //     var date1 = {}, date2 = {}, oDate, that = this, i, index = -1,
            //     size = txposturecurve.getProperty("/newRecords").length;
            //     this.byId("list").destroyItems();
            //     if(size !== 0) {
            //       i = 0;
            //       while(i < size && index === -1) {
            //         console.log(txposturecurve.getProperty("/newRecords")[i].week);
            //         date1.week = txposturecurve.getProperty("/newRecords")[i].week;
            //         oDate = txposturecurve.getProperty("/newRecords")[i + 1];
            //         //console.log(day, month, date1, oDate);
            //           console.log(day, date1.week);
            //           if(day <= date1.week )
            //           {
            //             index = i;
            //           }
            //           else if(oDate !== undefined) {
  				// 						date2.week = oDate.week;
  				// 						if( (day > date1.week) || (day > date1.week && day < date2.week)) {
  				// 								index = (i + 1);
  				// 						}

  				// 					}

            //         i++;
            //       }
            //     }else {
            //       index = 0;
            //       txposturecurve.setProperty("/newRecords", []);
            //       console.log("vacio");
            //     }
            //     txposturecurve.getProperty("/newRecords").splice(index, 0, {
            //       "week": parseInt(this.byId("selectRecuDays").getSelectedItem().getText(),10),
            //       "breed_id": 0,
            //       "theorical_performance": this.byId("inputRecuDesc").getValue(),
            //       "historical_performance": 0,
            //       "theorical_accum_mortality": 0,
            //       "historical_accum_mortality": 0,
            //       "theorical_uniformity": 0,
            //       "historical_uniformity": 0,
            //       "type_posture": 'Joven'
            //     });
            //     txposturecurve.refresh();
            //     this.byId("selectRecuDays").setSelectedKey('1');
            //     this.byId("inputRecuDesc").setValue("");

            //   }
            //   else {
            //     this.onToast("Debe ingresar un valor", null);
            //   }
        },
        onNavBack: function(oEvent) {
            /** @type {JSONModel} OS Referencia al modelo "OS" */
            var util = this.getView().getModel("util");
            this.getView().getModel("TXPOSTURECURVE").setProperty("/selectDay",1);
            this.getRouter().navTo(util.getProperty("/selectedEntity"), {}, true );

        },
        onCreate: function(oEvent) {

    				var txposturecurve = this.getView().getModel("TXPOSTURECURVE"),
    					util = this.getView().getModel("util"),
    					that = this,
    					json = {
    						"breed_id": txposturecurve.getProperty("/breed_id/value"),
    						"newRecords": txposturecurve.getProperty("/newRecords"),
    					},
    					serviceUrl = util.getProperty("/serviceUrl"),
    					settings = {
    						async: true,
    						url: serviceUrl + "/posture_curve",
    						method: "POST",
    						data: JSON.stringify(json),
    						dataType: "json",
    						contentType: "application/json; charset=utf-8",
    						success: function(res) {
    									that.onToast(that.getI18n().getText("Registro creado con exito"));
    									that.getRouter().navTo("txposturecurve", {}, true);
    						},
    						error: function(err) {
    							console.log(err);
    							util.setProperty("/error/status", err.status);
    							util.setProperty("/error/statusText", err.statusText);
    						}
    					};
    				util.setProperty("/busy/", true);
    				//realiza la llamada ajax
    				$.ajax(settings);

    		},
        handleDeleteRecu: function(oEvent) {
            var txposturecurve = this.getView().getModel("TXPOSTURECURVE"),
                oItem = oEvent.getParameter("listItem");
            console.log(oItem);
            console.log(oItem.oBindingContexts);
            var index = oItem.oBindingContexts.TXPOSTURECURVE.getPath().split("/")[2];

            txposturecurve.getProperty("/newRecords").splice(index, 1);
            txposturecurve.refresh();
        },
        onConfirmDelete: function(oEvent) {

            var oBundle = this.getView().getModel("i18n").getResourceBundle();
            var confirmation = oBundle.getText("confirmation");
            var util = this.getView().getModel("util"),
                mdbreed = this.getView().getModel("MDBREED"),
                txposturecurve = this.getModel("TXPOSTURECURVE"),
                serviceUrl = util.getProperty("/serviceUrl");

            var that = this;
            var dialog = new Dialog({
                title: confirmation,
                type: "Message",
                content: new sap.m.Text({
                    text: "Dese eliminar la curva de postura?"
                }),

                beginButton: new Button({
                    text: "Si",
                    press: function() {
                        dialog.close();
                        that.onDeleteRecord();
                    }
                }),
                endButton: new Button({
                    text: "No",
                    press: function() {
                        dialog.close();
                        dialog.destroy();
                    }
                })
            });

            dialog.open();

        },
        onDeleteRecord: function(){
            let util  = this.getModel("util"),
                that = this,
                txposturecurve = this.getModel("TXPOSTURECURVE"),
                serviceUrl = util.getProperty("/serviceUrl"),
                mdbreed = this.getView().getModel("MDBREED"),
                tabBar = this.getView().byId("tabBar");

            util.setProperty("/busy/", true);

            $.ajax({
                type: "DELETE",
                contentType: "application/json",
                data: JSON.stringify({
                    "breed_id": mdbreed.getProperty("/selectedRecord/breed_id")
                }),
                url: serviceUrl + "/posture_curve/",
                dataType: "json",
                async: true,
                success: function(data) {
                    util.setProperty("/busy/", false);
                    tabBar.setSelectedKey("kregisterCurveFilter");
                    txposturecurve.setProperty("/settings/enabledTab", false);
                    that._onRouteMatched();
                },
                error: function(request, status, error) {
                    that.onToast("Error de comunicación");
                    console.log("Read failed");
                }
            });

        }


    });
});
