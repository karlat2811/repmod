sap.ui.define([], function() {
    "use strict";

    return {

        recordsCount: function(records) {
            console.log("len: "+records.length);
            return records.length;
        },
        shedStatusText: function(status) {
            var result = "";

            switch (status) {
            case "1":
                result = "Disponible para alojamiento";
                break;

            case "2":
                result = "En recepción";
                break;

            case "3":
                result = "Alojado";
                break;

            case "4":
                result = "Lleno";
                break;

            case "5":
                result = "Disponible para desalojo";
                break;

            case "6":
                result = "En desalojo";
                break;

            case "7":
                result = "Desalojado";
                break;

            case "8":
                result = "En mantenimiento";
                break;

            case "9":
                result = "Descanso";
                break;

            case "10":
                result = "Desinfectado";
                break;

            case "11":
                result = "Deshabilitado";
                break;
            }
            return result;
        },
        onCapMinShed: function(capmin, width, lenght) {

            var result = 0;
            console.log(capmin+"*"+" ("+width+"*"+lenght+")");
            result = parseInt(capmin * (width*lenght));

            return result;
        },
        onCapMaxShed: function(capmax, width, lenght) {

            var result = 0;
            console.log(capmax+"*"+" ("+width+"*"+lenght+")");
            result = parseInt(capmax * (width*lenght));

            return result;
        },
        onkg:function(valorunid, abbreviation) {
            console.log(valorunid);
            console.log(abbreviation);
	     var result=0;
	     if(abbreviation ==="UN"){
	     	result="";
	     }else{
	     	result=valorunid;
	     }	
	     return result;	


        },

        onkg2:function(valorunid, abbreviation) {
            // console.log(valorunid);
            // console.log(abbreviation);
	     	var result=0;
	     	if(abbreviation ==="UN"){
                result=" ";
            }else{
                result="Kg";
            }	
            return result;	
        },

        enabledCreate: function(ok1, ok2){
            console.log("ok?: ", (ok1 && ok2));
            return (ok1 && ok2);
        }

    };
});
