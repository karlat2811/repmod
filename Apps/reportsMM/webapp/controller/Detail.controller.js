sap.ui.define([
    "MasterDetailSample/controller/BaseController",
    "jquery.sap.global",
    "sap/ui/model/json/JSONModel",
    "sap/ui/core/mvc/Controller",
    'sap/ui/model/Filter',
    'sap/ui/core/Fragment',
    "sap/m/MessageToast",
    "sap/m/Dialog",
    "sap/m/Button",
    "sap/m/Text",
    "sap/m/MessageBox"
], function (BaseController, jQuery, JSONModel, Controller,Filter,Fragment,MessageToast,Dialog,Button,Text,MessageBox) {
    "use strict"
    const incubatorStage = 2; /*Clase para levante y Cria*/
    const stateSucess= 'Success';
    const stateError= 'Error';
    const limitInt= 2147483640;
    const limitRows= 2000;
    const limitDiferencial= 359;

    return BaseController.extend("MasterDetailSample.controller.Detail", {

        /**
         * Called when a controller is instantiated and its View controls (if available) are already created.
         * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
         * @memberOf MasterDetail.view.Detail
         */


        onInit: function () {
          this.setFragments();
          this.getRouter().getRoute("detail").attachPatternMatched(this._onRouteMatched, this);
        },
        
                    
        /**
         * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
         * (NOT before the first rendering! onInit() is used for that one!).
         * @memberOf MasterDetail.view.Detail
         */
      
        _onRouteMatched: function (oEvent) {
            let oView= this.getView();
            var oArguments = oEvent.getParameter("arguments");
            let ospartnership = this.getModel("ospartnership");
          
                console.log("Say hi!",ospartnership)

   
              
            
            if(ospartnership.getProperty("/records").length>0){
                let partnership_id = ospartnership.getProperty("/selectedRecords/partnership_id");
              
                this.getModel("ospartnership").setProperty("/name","Epa")
                console.log("hola!!!")
                this.onRead(partnership_id);
            }
            else{
                this.reloadPartnership()
                    .then(data => {
                        if(data.length>0){
                            let obj= ospartnership.getProperty("/selectedRecords/");
                            if(obj){
                                this.onRead(obj.partnership_id);
                            }
                            else{
                                MessageToast.show("no existen empresas cargadas en el sistema", {
                                    duration: 3000,
                                    width: "20%"
                                });
                                console.log("err: ", data);
                            }
                        }
                        else{
                            MessageToast.show("ha ocurrido un error al cargar el inventario", {
                                duration: 3000,
                                width: "35%"
                            });
                            console.log("err: ", data);
                        }
                    });
            } 

            this.getView().byId("__header0").bindElement("ospartnership>/records/" + this.index + "/");
            this.onRead(this.index);
          },
          handleTitleSelectorPress: function(){
            console.log("En el handle")
          },
          generatedCSV: function()
          {
            let Estructs = this.getView().byId("filterOriginEntry").mProperties.selectedKey;
            let TypeEstructs = this.getView().byId("filterLotEntry1").mProperties.selectedKeys;
            let Progresive = this.getView().byId("filterLotEntry2").mProperties.selectedKey;
            let ProgresiveType1 = this.getView().byId("filterLotEntry3").mProperties.selectedKeys;
            let ProgresiveType2 = this.getView().byId("filterLotEntry4").mProperties.selectedKeys;
            var cont = []
            if(Estructs == "Estructuras"){

                if(TypeEstructs == "Núcleos"){
                    var mdreports = this.getModel("OSSHED").getProperty("/records")
                    ["Código", "Nombre", "Codígo Tipo del Núcleo"]
                    this.arrayObjToCsvCenter(mdreports);
                }
                else
                    if(TypeEstructs == "Galpones"){
                        var mdreports = this.getModel("OSSHED").getProperty("/records")
                       cont = ["Código", "Nombre", "Codígo Tipo del Núcleo"]

                        this.arrayObjToCsvCenter(mdreports, cont);
                    }
               
            }
            
            ;
              console.log(mdreports);
            
              // this.arrayObjToCsv();
          },
  
          arrayObjToCsvCenter: function (ar, cont) {
          //comprobamos compatibilidad
              if(window.Blob && (window.URL || window.webkitURL)){
                  var contenido = "",
                      d = new Date(),
                      blob,
                      reader,
                      save,
                      clicEvent;
                  //creamos contenido del archivo
                  var array = ["Código", "Nombre", "Codígo Tipo del Núcleo"];
                  console.log(array);
                  console.log("EL ARRAY");
                  console.log(ar);
                  for (var i = 0; i < ar.length; i++) {
               
  
                      console.log("se supone que las cabeceras");
                      console.log(Object.keys(ar[i]));
                      //construimos cabecera del csv
                      if (i == 0){
                          contenido += cont.join(";") + "\n";
                      //resto del contenido
                      }
                      contenido += Object.keys(ar[i]).map(function(key){
                          return ar[i][key];
                      }).join(";") + "\n";
                  }
                  console.log(contenido);
                  //creamos el blob
                  blob =  new Blob(["\ufeff", contenido], {type: "text/csv"});
                  //creamos el reader
                  var reader = new FileReader();
                  reader.onload = function (event) {
                  //escuchamos su evento load y creamos un enlace en dom
                      save = document.createElement("a");
                      save.href = event.target.result;
                      save.target = "_blank";
                      //aquí le damos nombre al archivo
                  
                      save.download = "salida.csv";
                  
  
                      try {
                      //creamos un evento click
                          clicEvent = new MouseEvent("click", {
                              "view": window,
                              "bubbles": true,
                              "cancelable": true
                          });
                      } catch (e) {
                      //si llega aquí es que probablemente implemente la forma antigua de crear un enlace
                          clicEvent = document.createEvent("MouseEvent");
                          clicEvent.initEvent("click", true, true);
                      }
                      //disparamos el evento
                      save.dispatchEvent(clicEvent);
                      //liberamos el objeto window.URL
                      (window.URL || window.webkitURL).revokeObjectURL(save.href);
                  };
                  //leemos como url
                  reader.readAsDataURL(blob);
              }else {
              //el navegador no admite esta opción
                  alert("Su navegador no permite esta acción");
              }
          },
     farms : function(){
        let selectObject = this.getView().getModel("ospartnership").oData.selectedRecord
         let osshed = this.getModel("OSSHED"),
         util = this.getModel("util");
     
      
         fetch("/farm/findFarmByPartnershipAll", {
             
             headers: {
                 'Content-Type': 'application/json'
             },
             method: 'POST',
             body: JSON.stringify({
                 "partnership_id": selectObject.partnership_id,
                
             })
         })
         .then(
             function(response) {
               if (response.status !== 200) {
                 console.log('Looks like there was a problem. Status Code: ' +
                   response.status);
             
                 return;
               }
     
               response.json().then(function(res) {
                 console.log("Estoy en y me retorna -> ",res);
                         util.setProperty("/busy/", false);
                          osshed.setProperty("/records",res.data);
               });
             }
           )
           .catch(function(err) {
             console.log('Fetch Error :-S', err);
           });
 
     
    
    
    }
    ,
          onRead: async function (index) {
              
            
            let ospartnership = this.getModel("ospartnership");
            let mdscenario = this.getModel("mdscenario");
        
            let activeS = await this.activeScenario();
            if(this.getModel("ospartnership").getProperty("/selectedRecord") == undefined){
                console.log("hola",this.getView().byId("__header0").mProperties.title =this.getModel("ospartnership").getProperty("/selectedRecords")[0] ) 
            
            }
            mdscenario.setProperty("/scenario_id", activeS.scenario_id);
            mdscenario.setProperty("/name", activeS.name);

                   
          },
       
    reloadPartnership: function(){
        let util = this.getModel("util");
            let that = this;
            let ospartnership = this.getModel("ospartnership");

            util.setProperty("/busy/", true);
            ospartnership.setProperty("/records", []);

            let url = util.getProperty("/serviceUrl") +util.getProperty("/" + util.getProperty("/service") + "/getPartnership");
            let method = "GET";
            let data = {};
            return new Promise((resolve, reject) => {
                function getPartnership(res) {
                    util.setProperty("/busy/", false);
                    ospartnership.setProperty("/records/", res.data);
                    if(res.data.length>0){
                        let obj= res.data[0];
                        obj.index= 0;
                        ospartnership.setProperty("/selectedRecords/", obj);
                        ospartnership.setProperty("/name", obj.name);
                        ospartnership.setProperty("/address", obj.address);
                        ospartnership.refresh(true)
                        // console.log("la data en el detail el mio: ",data)
                    }
                    resolve(res.data);
                }

                function error(err) {
                    console.log(err);
                    ospartnership.setProperty("/selectedRecords/", []);
                    util.setProperty("/error/status", err.status);
                    util.setProperty("/error/statusText", err.statusText);
                    reject(err);
                }

                /*Envía la solicitud*/
                // console.log("antes del send request, this:  ",this, "url:",url, "method: ",method, "data:",data, "getPartnership: ",getPartnership, "error: ",error, error)
                this.sendRequest.call(this, url, method, data, getPartnership, error, error);
            });
    },
        activeScenario: function () {
            let that= this;
            let util = this.getModel("util");
            const serverName = util.getProperty("/serviceUrl") + util.getProperty("/" + util.getProperty("/service") + "/activeScenario");
            return new Promise((resolve, reject) => {
              fetch(serverName)
                .then(
                  function (response) {
                    if (response.status !== 200) {
                        console.log('Looks like there was a problem. Status Code: ' +response.status);
                        that.openDialog(stateError, "Ha ocurrido un error.")
                        return;
                    }
                    response.json().then(function (res) {
                        resolve(res);
                    });
                  }
                )
                .catch(function (err) {
                  console.log('Fetch Error :-S', err);
                  that.openDialog(stateError, "Ha ocurrido un error.")
                });
    
            });
        },
        
    
    
        //AGREGADAS RECIENTEMENTE
        handleSelectionChange : function(oEvent){
          //let origin =  this.getView().byId("filterOriginEntry").getSelectedKey();
          console.log("El record!!!!",this.getModel("ospartnership").getProperty("/records"))
          let slot = this.getView().byId("filterOriginEntry").getSelectedKey()
          //let  mdinventory = this.getModel("mdinventory");
         this.setearFilters()
          this.getView().getModel("mdscenario").setProperty("/name",slot);
              if(slot == "Estructuras"){ 
                        this.setearTodos()
           this.getView().byId("filterLotEntry1").setSelectedKeys()
                        this.getView().byId("filterLotEntry1").setProperty("visible", true)
                      
              }else{
                    if(slot == "Progresiva"){
                        this.setearFilters()
                        this.setearTodos()
                        this.getView().getModel("OSSHED").setProperty("/records3",[])
                        this.getView().byId("filterLotEntry2").setProperty("visible", true)
                        this.getView().byId("filterLotEntry3").setProperty("visible", true)
                    }
              }
    
    
    
        },
        setearFilters: function(){
            this.getView().byId("filterLotEntry3").setProperty("visible", false);
            this.getView().byId("filterLotEntry4").setProperty("visible", false);
            this.getView().byId("filterLotEntry2").setProperty("visible", false);
            this.getView().byId("filterLotEntry1").setProperty("visible", false);
                this.getView().byId("filterLotEntry2").setSelectedKey("")
                this.getView().byId("filterLotEntry3").setSelectedKeys([]) 
                this.getView().byId("filterLotEntry4").setSelectedKeys([])
                

        },
        handleSelectionChange3: function(oEvent){
            console.log("HANDLESELECTION3")
            var oView = this.getView();
            var oDialog = oView.byId("filterLotEntry2").getSelectedKey();
            let prueba = this.getView().byId("filterLotEntry3")
             console.log("HANDLESELECTION 3",oDialog);
                    
           
        
            if(oDialog == "Cría y Levante" ||  oDialog == "Producción"  || oDialog == "Engorde"){
                  console.log("HII");
                    this.getView().byId("filterLotEntry4").setProperty("visible", false)
                    this.getView().byId("filterLotEntry3").setProperty("visible", true) 
                    this.getView().byId("filterLotEntry4").setSelectedKeys([])
    
            }else{
                    
                    this.getView().byId("filterLotEntry3").setProperty("visible", false)
                    this.getView().byId("filterLotEntry4").setProperty("visible", true)
                    this.getView().byId("filterLotEntry3").setSelectedKeys([])
                    this.getView().byId("filterLotEntry1").setSelectedKey("")
    
            }
            
        },
     //NUEVOOOO
     handleSelectionChange4: function(oEvent){
        console.log("En handleSelectionChange3")
     },
        habColumn: function(oEvent, select, select1){
     
          this.setearTodos();
            if(select =="Estructuras" && select1 != null){
                if(select1 == "Granjas" ||   select1=="Granjas,Galpón" ){
                    console.log(this.getView());
                    if(select1 == "Granjas"){
                        this.getView().byId("c1").setProperty("visible", true)
                        this.getView().byId("c2").setProperty("visible", true)
                        this.getView().byId("c3").setProperty("visible", true)

                    }
                 
                }else
                    if(select1 == "Núcleos" ){
                        this.getView().byId("c1").setProperty("visible", true)
                        this.getView().byId("c2").setProperty("visible", true)
    
                    }else
                        if(select1 == "Galpones"){
                            this.getView().byId("c1").setProperty("visible", true)
                            this.getView().byId("c4").setProperty("visible", true)
                            this.getView().byId("c5").setProperty("visible", true)
                            this.getView().byId("c6").setProperty("visible", true)
                            this.getView().byId("c7").setProperty("visible", true)
                            this.getView().byId("c8").setProperty("visible", true)
                        
                        }else
                            if(select1 == "Máquinas"){
                                this.getView().byId("c1").setProperty("visible", true)
                                this.getView().byId("c2").setProperty("visible", true)
                                this.getView().byId("c9").setProperty("visible", true)
                                this.getView().byId("c10").setProperty("visible", true)
                                this.getView().byId("c11").setProperty("visible", true)
                                this.getView().byId("t11").setProperty("visible", true)
                            }
            
            
            }
        },
        habColumn2: function(oEvent, select, select1){
            //AQUI SE HABILITARAN LAS COLUMNAS SEGUN LA SELECCION DE LOS CAMPOS Y EL PRIMER SELECT
            //Cuando se pulse el boton de busqueda se invoca!!
            //this.getView().byId("c1").setProperty("visible", true)
          
    
            this.setearTodos();
            if(this.maquinas(select1)==false){
                if(select1== "Granjas,Galpones" ||  select1 == "Galpones,Granjas" ){

                    MessageToast.show("Combinación de estructuras no permitida")   
                    console.log("No se porque caigo en este caso")
                }else{
                    if(select1 == "Granjas,Núcleos" ||select1 == "Núcleos,Granjas"){
                        this.getView().byId("c1").setProperty("visible", true)
                        this.getView().byId("c2").setProperty("visible", true)
                        this.getView().byId("c3").setProperty("visible", true)
                        this.getView().byId("c12").setProperty("visible", true)
                        this.getView().byId("t12").setProperty("visible", true)
                      
                    }else
                            if(select1 == "Núcleos,Galpones" || select1 == "Galpones,Núcleos"){
                                this.getView().byId("c1").setProperty("visible", true)
                                this.getView().byId("c2").setProperty("visible", true)
                                this.getView().byId("c13").setProperty("visible",true)
                                this.getView().byId("t13").setProperty("visible", true)
                              
                            }
                        }
            }else
                {
              
                        MessageToast.show("La opción máquinas no debe combinarse")
                 
                   
                }
        },
        habColumn3: function(oEvent, select, select1, select2,slect3,select4){
            //AQUI SE HABILITARAN LAS COLUMNAS SEGUN LA SELECCION DE LOS CAMPOS Y EL PRIMER SELECT
            //Cuando se pulse el boton de busqueda se invoca!!
            if(select =="Estructuras" && select1 != null){
                this.setearTodos()
                    this.getView().byId("c1").setProperty("visible", true)
                    this.getView().byId("c2").setProperty("visible", true)
                    this.getView().byId("c3").setProperty("visible", true)
                    this.getView().byId("c14").setProperty("visible", true)
                    this.getView().byId("t14").setProperty("visible", true)
            }
        },
        setear : function(){

            this.getView().byId("c1").setProperty("visible",true)
            this.getView().byId("c2").setProperty("visible",true)

        },
        setearTodos : function(){
           
            this.getView().byId("c1").setProperty("visible",false)
            this.getView().byId("c2").setProperty("visible",false)
            
            this.getView().byId("c3").setProperty("visible",false)
            this.getView().byId("c4").setProperty("visible",false)

            
            this.getView().byId("c5").setProperty("visible",false)
            this.getView().byId("c6").setProperty("visible",false)

            
            this.getView().byId("c7").setProperty("visible",false)
            this.getView().byId("c8").setProperty("visible",false)

            
            this.getView().byId("c9").setProperty("visible",false)
            this.getView().byId("c10").setProperty("visible",false)
            this.getView().byId("c11").setProperty("visible",false)
            this.getView().byId("t11").setProperty("visible", false)
            this.getView().byId("c12").setProperty("visible",false)
            this.getView().byId("t12").setProperty("visible", false)
            this.getView().byId("c13").setProperty("visible",false)
            this.getView().byId("t13").setProperty("visible", false)
            this.getView().byId("c14").setProperty("visible",false)
            this.getView().byId("t14").setProperty("visible", false)
 
          
            
        },
        //Función para el evento "Buscar de la consulta"
        searchEntry: async function (oEvent) {
    
          
           //Valor del primer filtro de Estructuras: "Estructuras" o "Progresiva"
           let Estructs = this.getView().byId("filterOriginEntry").mProperties.selectedKey;
       
  
           //Se setea el registro principal donde se cargan los JSON de las consultas 
           this.getView().getModel("OSSHED").setProperty("/records3",[])
         
           //Se setean las columnas de las tablas (se desactivan) y se reactivan las necesarias según el caso
            this.setearTodos();
    
            //Según la consulta se carga 
            if(Estructs == "Estructuras"){
            
                this.proccesRequestE(oEvent)
                    
            }  
            else
                {
                    this.proccesRequestP(oEvent)
                }
                
              

        },
        //Procesa las solicitudes de estructuras
        proccesRequestE: function(oEvent){
           let Estructs = this.getView().byId("filterOriginEntry").mProperties.selectedKey;
           let TypeEstructs = this.getView().byId("filterLotEntry1").mProperties.selectedKeys;
           let Progresive = this.getView().byId("filterLotEntry2").mProperties.selectedKey;
           let ProgresiveType1 = this.getView().byId("filterLotEntry3").mProperties.selectedKeys;
           let ProgresiveType2 = this.getView().byId("filterLotEntry4").mProperties.selectedKeys;
           
           //Se valida si la consulta especifia la Estructura: Granjas, Núcleos, Galpones, Máquinas
            if(TypeEstructs.length != 0){
                //Se activa la tabla correspondiente
                this.getView().getModel("mdscenario").setProperty("/name",Estructs+" > "+TypeEstructs+" ")
           
                this.getView().byId("Galpon").setProperty("visible", true)
                if(TypeEstructs.length == 1){
                    this.setearTodos();
                    this.habColumn(oEvent,Estructs,TypeEstructs)
                    this.searchEstructures(oEvent)
                    }else 
                    if(TypeEstructs.length == 2){
                        this.searchEstructures(oEvent)
                        this.habColumn2(oEvent,Estructs,TypeEstructs)  
                    }else
                        if(TypeEstructs.length == 3){
                                this.setearTodos();
                                console.log("Estoy en el caso 3")
                                if(this.maquinas(TypeEstructs)==true){
                                
                                    MessageToast.show("La opción Máquinas, no debe combinarse")
                                }
                                else
                                    {
                                        this.habColumn3(oEvent,Estructs,TypeEstructs,Progresive,ProgresiveType1,ProgresiveType2)
                                        this.getView().byId("Galpon").setProperty("visible", true)
                                        this.searchEstructures(oEvent)
                                    }
                        
            }else
                if(TypeEstructs.length == 4){
                    this.setearTodos();
                    MessageToast.show("La opción Máquinas, no debe combinarse")
                    
                }

            }else
                {
                    MessageToast.show("Consulta vacía!");
                }

        },
        proccesRequestP: function(oEvent){
            let Estructs = this.getView().byId("filterOriginEntry").mProperties.selectedKey;
            let TypeEstructs = this.getView().byId("filterLotEntry1").mProperties.selectedKeys;
            let Progresive = this.getView().byId("filterLotEntry2").mProperties.selectedKey;
            let ProgresiveType1 = this.getView().byId("filterLotEntry3").mProperties.selectedKeys;
           let ProgresiveType2 = this.getView().byId("filterLotEntry4").mProperties.selectedKeys;
          
            if(Estructs == "Progresiva" && ProgresiveType1!= null ){
                this.getView().getModel("mdscenario").setProperty("/name",Estructs+" > "+Progresive+" "+ProgresiveType1)
                if(ProgresiveType1.length){
                    console.log("Aquí se puede consultar")
                     this.getView().byId("Galpon").setProperty("visible", true)
                }else
                    {
                        MessageToast.show("Ups, rescue me2!");
                    }


            }
 
         },
        consult3 : function(oEvent){
            console.log("En consult2");
           
            
            console.log("EN PRUEBAAAAAAA!!!!")
            let selectObject   =this.getView().getModel("ospartnership").oData.selectedRecord;
            console.log("OBJETOS A CARGAR----",vconsult)
			let osshed = this.getModel("OSSHED"),
			util = this.getModel("util");
			console.log(util);
		
         
           
			fetch("/shed/findOshedByPartnerShip", {
				
				headers: {
					'Content-Type': 'application/json'
				},
				method: 'POST',
				body: JSON.stringify({
					"partnership_id":selectObject.partnership_id
				})
			})
			.then(
				function(response) {
				  if (response.status !== 200) {
					console.log('Looks like there was a problem. Status Code: ' +
					  response.status);
				
					return;
				  }
		
				  response.json().then(function(res) {
					console.log("Estoy en y me retorna -> ",res);
							util.setProperty("/busy/", false);
						
                        osshed.setProperty("/records3", res.data);
                        osshed.setProperty("/records5", res.data);
                        console.log("Banco de registros", osshed.getProperty("/records5"))
                   
				
				  });
				}
			  )
			  .catch(function(err) {
				console.log('Fetch Error :-S', err);
			  });

			

            
        },
        consult4 : function(oEvent){
            console.log("En consult2");
            
            
            console.log("EN PRUEBAAAAAAA!!!!")
            let selectObject =this.getView().getModel("ospartnership").oData.selectedRecord;
            let myarr = []
            let vconsult

           for(let i =0; i < selectObject.length; i++){
                myarr.push(selectObject[i].partnership_id)
                vconsult = selectObject[i].partnership_id
           }
            console.log("OBJETOS A CARGAR",vconsult)
			let osshed = this.getModel("OSSHED"),
			util = this.getModel("util");
			console.log(util);
		
         
			fetch("/incubator_plant/findIncubatorByPartnerships", {
				
				headers: {
					'Content-Type': 'application/json'
				},
				method: 'POST',
				body: JSON.stringify({
					"partnership_id": selectObject.partnership_id
				})
			})
			.then(
				function(response) {
				  if (response.status !== 200) {
					console.log('Looks like there was a problem. Status Code: ' +
					  response.status);
				
					return;
				  }
		
				  response.json().then(function(res) {
					console.log("Estoy en y me retorna -> ",res);
							util.setProperty("/busy/", false);
						
                        osshed.setProperty("/records3", res.data);
                        osshed.setProperty("/records6", res.data);
                        console.log("Banco de registros", osshed.getProperty("/records6"))
     
				
				  });
				}
			  )
			  .catch(function(err) {
                console.log('Fetch Error :-S', err);
                console.log("Say hi error!")
			  });

			

            
        },
        maquinas: function(search2){
            let flag = false;
            console.log(search2)
            for(let i = 0 ; i < search2.length; i++){
                if(search2[i] == "Máquinas"){
                    flag = true;

                    console.log(flag);
                  
                }

            }
            return flag;

        },
        //POPUPS
        setColumns : function(){

            this.getView().byId("t1").setProperty("visible",false)
            this.getView().byId("t2").setProperty("visible",false)
            this.getView().byId("t3").setProperty("visible",false)
            this.getView().byId("t4").setProperty("visible",false)

            this.getView().byId("t5").setProperty("visible",false)
            this.getView().byId("t6").setProperty("visible",false)
            this.getView().byId("t7").setProperty("visible",false)
            this.getView().byId("t8").setProperty("visible",false)
            this.getView().byId("t9").setProperty("visible",false)
            this.getView().byId("t10").setProperty("visible",false)
       


        },
		//ASIGNACIÓN 2 POPUP
		searchEstructures : function(oEvent){
           let selectObject = this.getView().getModel("ospartnership").oData.selectedRecord
           console.log("En la consulta....",selectObject)
           
           
           
           let search1 = this.getView().byId("filterOriginEntry").mProperties.selectedKey;
           let search2 = this.getView().byId("filterLotEntry1").mProperties.selectedKeys;
           let search3 = this.getView().byId("filterLotEntry2").mProperties.selectedKey;


            
			let osshed = this.getModel("OSSHED"),
			util = this.getModel("util");
		
         
			fetch("/farm/findFarmByPartnership", {
				
				headers: {
					'Content-Type': 'application/json'
				},
				method: 'POST',
				body: JSON.stringify({
                    "object": selectObject.partnership_id,
                    "search1" : search1,
                    "search2" : search2,
                    "search3" : search3
                   
				})
			})
			.then(
				function(response) {
				  if (response.status !== 200) {
					console.log('Looks like there was a problem. Status Code: ' +
					  response.status);
				
					return;
				  }
		
				  response.json().then(function(res) {
					console.log("Estoy en y me retorna -> ",res);
							util.setProperty("/busy/", false);
                             osshed.setProperty("/records3",res.data);
                             osshed.setProperty("/records",res.data);
                             //osshed.setProperty("/week",res.data);
                  });
				}
			  )
			  .catch(function(err) {
				console.log('Fetch Error :-S', err);
			  });
    
    
         },
         
		 prueba2 : function(oEvent){

		 

            let selectObject =this.getModel("ospartnership").getProperty("/records");
            let myarr = []
            let vconsult

           for(let i =0; i < selectObject.length; i++){
                myarr.push(selectObject[i].partnership_id)
                vconsult = selectObject[i].partnership_id
           }
          
			let osshed = this.getModel("OSSHED"),
			util = this.getModel("util");
        
			fetch("/center/findCenterByPartnerShip", {
				
				headers: {
					'Content-Type': 'application/json'
				},
				method: 'POST',
				body: JSON.stringify({
					"partnership_id": vconsult
				})
			})
			.then(
				function(response) {
				  if (response.status !== 200) {
					console.log('Looks like there was a problem. Status Code: ' +
					  response.status);
				
					return;
				  }
		
				  response.json().then(function(res) {
					
					util.setProperty("/busy/", false);
                    osshed.setProperty("/records3", res.data);
                    osshed.setProperty("/records4", res.data);
                    console.log("Banco 2",osshed.getProperty("/records4"))


				  });
				}
			  )
			  .catch(function(err) {
				console.log('Fetch Error :-S', err);
			  });

			

         },
         agrupar : function(oEvent){
            let prop = 'code_farm'
                        
                            let par  = res.data[0].code_farm
                            let par2  = res.data[0].code_center
                            console.log("par",par)
                            data.push(res.data[0])
                            for(let i = 0 ; i < res.data.length; i++){
                                
                                if(res.data[i].code_farm != par){
                                    data.push(res.data[i])
                                   
                                    par =res.data[i].code_farm
                                    if(res.data[i].code_center != par2){
                                        par2 =res.data[i].code_center
                                        data2.push(res.data[i])
                                   }
                                    console.log("Par2",par,res.data[i])
                               
                                }

                            }
            
         },
		 prueba3 : function(oEvent){
            console.log("EN PRUEBAAAAAAA!!!!")
            let selectObject =this.getModel("ospartnership").getProperty("/records");
            let myarr = []
            let vconsult

           for(let i =0; i < selectObject.length; i++){
                myarr.push(selectObject[i].partnership_id)
                vconsult = selectObject[i].partnership_id
           }
            console.log("OBJETOS A CARGAR",vconsult)
			let osshed = this.getModel("OSSHED"),
			util = this.getModel("util");
            console.log(util);
			fetch("/shed/findOshedByPartnerShip", {
				
				headers: {
					'Content-Type': 'application/json'
				},
				method: 'POST',
				body: JSON.stringify({
					"partnership_id":vconsult
				})
			})
			.then(
				function(response) {
				  if (response.status !== 200) {
					console.log('Looks like there was a problem. Status Code: ' +
					  response.status);
				
					return;
				  }
		
				  response.json().then(function(res) {
					console.log("Estoy en y me retorna -> ",res);
							util.setProperty("/busy/", false);
						
                        osshed.setProperty("/records3", res.data);
                        osshed.setProperty("/records5", res.data);
				
				  });
				}
			  )
			  .catch(function(err) {
				console.log('Fetch Error :-S', err);
			  });

			

 
		 },
       
        //TraerConsultas
        pruebaRecords : function(oEvent){

			console.log("Lanzando cotufas!",this.getModel("OSSHED"))
            
            //let selectObject = oEvent.getSource().getBindingContext("data").getObject();
			let osshed = this.getModel("OSSHED"),
			util = this.getModel("util");
			console.log(osshed);
	
         },
         
		//ASIGNACIÓN 1
        consult: function(oEvent){
            console.log("En consult");
            
            this.searchEstructures(oEvent);

			//var oDialog = oView.byId("searchEntry");
			var osshed = this.getView().getModel("OSSHED");
            let sheds = osshed.getProperty("/records3");
          
	
		
        },
        setColumn : function(){
            var oView = this.getView();
			//var oDialog = oView.byId("searchEntry");
			var osshed = this.getView().getModel("OSSHED");
            let sheds = osshed.getProperty("/records3");
          
			let aux = [];
			console.log("Say Hi!!!", sheds);
       
				for( let i = 0;i < sheds.length;i++){
                        aux.push(sheds[i]);
                        console.log("En consult--->",sheds[i])
					}
			osshed.setProperty("/records3",aux);
		
        },
       

          //FUNCION PARA EL PRIMER SELECT DEL FRONT REPORTES
          changeOrigin: function(oEvent){
           //modificar visibilidad del select 2
            //let origin =  this.getView().byId("filterOriginEntry").getSelectedKey();
            let slot = this.getView().byId("filterOriginEntry").getSelectedKey()
            //let  mdinventory = this.getModel("mdinventory");
            console.log("aqui ando 2!",this.getView().byId("filterLotEntry4").getProperty("visible"),this.getView().byId("filterLotEntry3").getProperty("visible"));
            console.log(slot);
                if(slot !== "Opciones:"){
    
                    if(slot == "Estructuras"){
                        if( this.getView().byId("filterLotEntry2").getProperty("visible")== true && this.getView().byId("filterLotEntry3").getProperty("visible")== false){
                            this.getView().byId("filterLotEntry2").setProperty("visible", false);
                            this.getView().byId("filterLotEntry3").setProperty("visible", false);
                            this.getView().byId("filterLotEntry4").setProperty("visible", false);
                            this.getView().byId("filterLotEntry1").setProperty("visible", true);
                        }else 
                            if(this.getView().byId("filterLotEntry3").getProperty("visible")== true){
                            console.log("caso 2, aquiii voyy");
                            this.getView().byId("filterLotEntry3").setProperty("visible", false);
                            this.getView().byId("filterLotEntry4").setProperty("visible", false);
                            this.getView().byId("filterLotEntry2").setProperty("visible", false);
                            this.getView().byId("filterLotEntry1").setProperty("visible", true);
                  
                            console.log("Aqui ando en caso 2222");
                            
                        }else{
                                this.getView().byId("filterLotEntry1").setProperty("visible", true);
                            }
                     }else if(slot == "Progresiva"){
                        if(this.getView().byId("filterLotEntry1").getProperty("visible")==true){ 
                        this.getView().byId("filterLotEntry1").setProperty("visible", false);
                        this.getView().byId("filterLotEntry2").setProperty("visible", true);
                        }else{
                            this.getView().byId("filterLotEntry2").setProperty("visible", true);
                        }
                    }
    
                }else{
    
                    if(this.getView().byId("filterLotEntry1").getProperty("visible")==true){
                        if(this.getView().byId("filterLotEntry3").getProperty("visible")==true){
                            this.getView().byId("filterLotEntry3").setProperty("visible", false);
                        }
                        this.getView().byId("filterLotEntry1").setProperty("visible", false);
                        MessageToast.show("Escoja una opción");
                    }else
                        if( this.getView().byId("filterLotEntry2").getProperty("visible")== true){
                            
                            if(this.getView().byId("filterLotEntry3").getProperty("visible")==true){
                                this.getView().byId("filterLotEntry3").setProperty("visible", false);
                            }
                            else 
                                if(this.getView().byId("filterLotEntry4").getProperty("visible")==true){
                                    this.getView().byId("filterLotEntry4").setProperty("visible", false);        
                                }
                            this.getView().byId("filterLotEntry2").setProperty("visible", false);
                            this.getView().byId("filterLotEntry3").setProperty("visible", false);
                            MessageToast.show("Introduzca un valor valido");  
                        }
    
                }
    
    
         
          },
          changeOrigin2: function(oEvent){
                console.log("Entro en la función")
                
            console.log("Prueba",slot);
            if(slot !== "Opciones:"){
              if(slot === "Estructuras"){
                  if(this.getView().byId("filterLotEntry2").getProperty("visible")==true){
                    this.getView().byId("filterLotEntry2").setProperty("visible", false);
                    let select = this.getView().byId("filterLotEntry3")
                    select.setProperty("visible", true);
                  }else{
                    this.getView().byId("filterLotEntry2").setProperty("visible",true);
                    let select = this.getView().byId("filterLotEntry2")
                  }
                console.log("ACTUALIZAR LA LISTA DE ITEMS EN EL SELECT")
               let select = this.getView().byId("filterLotEntry1")
                select.setProperty("visible", true);
               }else{
                console.log("ACTUALIZAR LA LISTA DE ITEMS EN EL SELECT2") 
                if(this.getView().byId("filterLotEntry1").getProperty("visible")==true)
                this.getView().byId("filterLotEntry1").setProperty("visible", false);
                let select = this.getView().byId("filterLotEntry2")
                select.setProperty("visible", true);
               }
            }else{
                MessageToast.show("Introduzca un valor valido");
            }
          },


          //POPUPS
          handlePopoverPress: function(oEvent){
			let _oPopover = this._getResponsivePopover();
            _oPopover.setModel(oEvent.getSource().getModel());
            const incubator = oEvent.getSource().getBindingContext("OSSHED").getObject()
            this.getModel("OSSHED").setProperty("/week", [incubator])
            this._oPopover.openBy(oEvent.getSource());
            
				//this.consult4(oEvent)		
			
		},
		_getResponsivePopover: function () {
		  if (!this._oPopover) {
			this._oPopover = sap.ui.xmlfragment("technicalConfiguration.view.shed.osshed_ShowPopover", this);
			this.getView().addDependent(this._oPopover);
		  }
		  return this._oPopover;
		},
		//POPUP 2
		handlePopoverPress2: function(oEvent){
			let _oPopover2 = this._getResponsivePopover2();
			_oPopover2.setModel(oEvent.getSource().getModel());
			this._oPopover2.openBy(oEvent.getSource());
				this.data2(oEvent)		
			
		},
		_getResponsivePopover2: function () {
		  if (!this._oPopover2) {
			this._oPopover2 = sap.ui.xmlfragment("technicalConfiguration.view.shed.osshed_ShowPopover1", this);
			this.getView().addDependent(this._oPopover2);
		  }
		  
		  return this._oPopover2;
		},
		//POPUP 3
		handlePopoverPress3: function(oEvent){
           
            let _oPopover3 = this._getResponsivePopover3();
			_oPopover3.setModel(oEvent.getSource().getModel());
			this._oPopover3.openBy(oEvent.getSource());
				this.data3(oEvent)
			
		},
		_getResponsivePopover3: function () {
		  if (!this._oPopover3) {
			this._oPopover3 = sap.ui.xmlfragment("technicalConfiguration.view.shed.osshed_ShowPopover2", this);
			this.getView().addDependent(this._oPopover3);
		  }
		  
          return this._oPopover3;
          
		}
         , handlePopoverPress4: function(oEvent){
            let _oPopover4 = this._getResponsivePopover4();
			_oPopover4.setModel(oEvent.getSource().getModel());
			this._oPopover4.openBy(oEvent.getSource());
				this.data2(oEvent)		
			
		},
		_getResponsivePopover4: function () {
		  if (!this._oPopover4) {
			this._oPopover4 = sap.ui.xmlfragment("technicalConfiguration.view.shed.osshed_ShowPopover1", this);
			this.getView().addDependent(this._oPopover4);
		  }
		  
		  return this._oPopover4;
		},
         data : function(oEvent){
            console.log("EN PRUEBAAAAAAA de DATA POPUP!!!!")
            let selectObject =this.getModel("OSSHED").getProperty("/records4");
           
          
            console.log("OBJETOS A CARGAR ---->",selectObject)
			let osshed = this.getModel("OSSHED"),
			util = this.getModel("util");
			console.log(util);
			/*fetch("/shed/findCenterByShed", {
				
				headers: {
					'Content-Type': 'application/json'
				},
				method: 'POST',
				body: JSON.stringify({
					"shed_id": myarr
				})
			})
			.then(
				function(response) {
				  if (response.status !== 200) {
					console.log('Looks like there was a problem. Status Code: ' +
					  response.status);
				
					return;
				  }
		
				  response.json().then(function(res) {
					console.log("Estoy en y me retorna -> .... ",res);
							util.setProperty("/busy/", false);
							osshed.setProperty("/records3", res.data);
						osshed.setProperty("/records8", res.data);

					//var firstItem = that.getView().byId("__list0").getItems()[0];
			
					var Oid = firstItem.getBindingContext("OSSHED").getPath().split('/');
					//var id = Oid[2];
				
				  });
				}
			  )
			  .catch(function(err) {
				console.log('Fetch Error :-S', err);
			  });

			

              */
         }, 
         data2 : function(oEvent){
             
            let selectObject =oEvent.getSource().getBindingContext("OSSHED").getObject();
            let osshed = this.getModel("OSSHED"),
			util = this.getModel("util");
            console.log("Llego al data fetch------> data2", selectObject)
            console.log("Objeto",selectObject.farm_id)
         
			fetch("/shed/findCenterByFarm", {
				
				headers: {
					'Content-Type': 'application/json'
				},
				method: 'POST',
				body: JSON.stringify({
					"farm_id": selectObject.farm_id
				})
			})
			.then(
				function(response) {
				  if (response.status !== 200) {
					console.log('Looks like there was a problem. Status Code: ' +
					  response.status);
				
					return;
				  }
		
				  response.json().then(function(res) {
					console.log("Estoy en y me retorna ->----- ",res);
							//util.setProperty("/busy/", false);
                        console.log("POPUP2",res.data);
                        osshed.setProperty("/records4", res.data);
				
		
				
				  });
				}
			  )
			  .catch(function(err) {
				console.log('Fetch Error :-S', err);
			  });

              

         },
         handleLinkPress : function(oEvent){

            this.handlePopoverPress3(oEvent)
            


         },
         data3: function(oEvent){
		     let selectObject =oEvent.getSource().getBindingContext("OSSHED").getObject()
             let v =  this.getView().byId("filterLotEntry1").mProperties.selectedKeys;
           
             let osshed = this.getModel("OSSHED")
            console.log("Estoy en el front del data 3, para la tercera consulta",selectObject)
			let util = this.getModel("util");
          
            
			fetch("/shed/findCenterByShed", {
				
				headers: {
					'Content-Type': 'application/json'
				},
				method: 'POST',
				body: JSON.stringify({
					"center_id": selectObject.center_id
				})
			})
			.then(
				function(response) {
				  if (response.status !== 200) {
					console.log('Looks like there was a problem. Status Code: ' +
					  response.status);
				
					return;
				  }
		
				  response.json().then(function(res) {
					console.log("Estoy en y me retorna -> ",res);
							util.setProperty("/busy/", false);
                            osshed.setProperty("/records7", res.data);
                  });
            
				}
			  )
			  .catch(function(err) {
				console.log('Fetch Error :-S', err);
			  });

		

 
         }
    
     
    });

});
