sap.ui.define([], function() {
	"use strict";

	return {
		calculateResidue: function(projected_quantity, residue) {
            return (projected_quantity-residue).toLocaleString();
        },
        calculateResidue2: function(projected_quantity, residue, partial_residue = 0) {
            console.log("FORMATTER ------------------------");
            console.log(projected_quantity, residue, partial_residue);
            return (projected_quantity - residue - partial_residue).toLocaleString();
        },
        sumResidues: function(records = []) {
            const sum = records.reduce((result, actual) => {
                return result + actual.projected_quantity - actual.residue;
            }, 0);
            return sum.toLocaleString();
        },

        selectableRow: function(projections = [], row) {
            if (projections.length === 0) {
                return "Active";
            }
		
            if (row.breed_name !== projections[0].breed_name) {
                return "Inactive";
            }

            else {
                return "Active";
            }
        },

        formatMiles: function(number){
            return (number !== "" && !isNaN(number)) ? number.toLocaleString() : number;

        },
        formatDate: function(date){
            let aDay=["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                aDate = date.split("/"),
                dt = new Date(aDate[2], aDate[1]-1, aDate[0]);
            return `${aDay[dt.getUTCDay()]} ${aDate[0]}`;
        },
        formatDate2: function(date){
            console.log("Date   ", date)
            if(date!== null){
                let aDay=["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"];
                date= new Date(date.toString());
                let c="/";
                date= aDay[date.getUTCDay()]+ "\n"+ date.getDate()+c+(date.getMonth()+1)+c+date.getFullYear();
            }
            return(date);
        },
        formatStatus:function(status){
			let ret;
			console.log("Say hi!!!")
            if(parseInt(status) < 0){
                ret = "Error";
				
            }else{
                if(parseInt(status) >= 0){
				  ret = "Success";
				  
                }else{
				  ret = "None";
                }
            }
            return ret;
        },
        formaticon: function(value){
			let ret;
			
            if (value === 1){
                ret = "sap-icon://accept";
            }else{
                ret = "sap-icon://decline";
            }

            return(ret);

        },
        formatcolor: function(value){
            let ret;
            if (value){
                ret = "green";
            }else{
                ret = "red";
            }

            return(ret);

        },
        formatVisible: function(value, value2){
            console.log(value, value2);
            console.log(!value||!value2);
            return !(value||value2);
        },
        formatAntiguedad: function(init_date){
            let diasdif;
            if(init_date!== null){
                let date = new Date();
                init_date = new Date(init_date.toString());
                console.log(date);
                console.log(init_date);

                diasdif= (date.getTime()-init_date.getTime());
                diasdif = Math.round(diasdif/(1000*60*60*24));

                console.log(diasdif);
                // date = new Date()

                // let fecha1 = new Date(init_date.toString());
                // let fecha2 = new Date(date.toString());
                //  console.log("las dos fechas")
                // console.log(fecha1)
                // console.log(fecha2)

                // console.log(fecha2.diff(fecha1, 'days'), ' dias de diferencia');
                
            }
            return(diasdif);
        },
        breed_name : function(breed_id){

            console.log(breed_id)
            let breed_names = ""
            if(breed_id == 1){
                breed_names = "Cobb"
            }else
                if(breed_id == 2){
                    breed_names = "H"
                }else
                    if(breed_id == 3){
                        breed_names = "Ross"
                    }

            return(breed_names)
        },
        calculateResidue: function(projected_quantity, residue) {
            return (projected_quantity-residue).toLocaleString();
        }
	};

});
