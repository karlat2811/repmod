sap.ui.define([
    "MasterDetailSample/controller/BaseController",
    "jquery.sap.global",
    "sap/ui/model/json/JSONModel",
    "sap/ui/core/mvc/Controller",
    'sap/ui/model/Filter',
    'sap/ui/core/Fragment',
    "sap/m/MessageToast",
    "sap/m/Dialog",
    "sap/m/Button",
    "sap/m/Text",
    "sap/m/MessageBox"

], function (BaseController, jQuery, JSONModel, Controller,Filter,Fragment,MessageToast,Dialog,Button,Text,MessageBox) {
    "use strict"
   

    return BaseController.extend("MasterDetailSample.controller.Detail", {

        /**
         * Called when a controller is instantiated and its View controls (if available) are already created.
         * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
         * @memberOf MasterDetail.view.Detail
         */


        onInit: function () {
          this.setFragments();
          this.getRouter().getRoute("detail").attachPatternMatched(this._onRouteMatched, this);
        
        },
        
		
                    
        /**
         * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
         * (NOT before the first rendering! onInit() is used for that one!).
         * @memberOf MasterDetail.view.Detail
         */
      
        _onRouteMatched: function (oEvent) {
            var oArguments = oEvent.getParameter("arguments");
        
            this.index = oArguments.id;


            let oView= this.getView();
            let ospartnership = this.getModel("ospartnership");

            
            if(ospartnership.getProperty("/records").length>0){
                let partnership_id = ospartnership.getProperty("/selectedRecords/partnership_id")
                this.onRead(partnership_id);
            }
            else{
                this.reloadPartnership()
                .then(data => {
                    if(data.length>0){
                        let obj= ospartnership.getProperty("/selectedRecords/");
                        if(obj){
                            this.onRead2(obj.index);
                        }
                        else{
                            MessageToast.show("no existen empresas cargadas en el sistema", {
                                duration: 3000,
                                width: "20%"
                            });
                            console.log("err: ", data)
                        }
                    }
                    else{
                        MessageToast.show("ha ocurrido un error al cargar el inventario", {
                            duration: 3000,
                            width: "35%"
                        });
                        console.log("err: ", data)
                    }
                });
    }

      this.getView().byId("__header0").bindElement("ospartnership>/records/" + this.index + "/");
      this.onRead(this.index);
          },
          generatedCSV: function()
          {
            let Estructs = this.getView().byId("filterOriginEntry").mProperties.selectedKey;
            let TypeEstructs = this.getView().byId("filterLotEntry1").mProperties.selectedKeys;
            let TypePogressive =  this.getView().byId("filterLotEntry2").mProperties.selectedKey;
            var cont = []
            var cad = ""
      
            
            if(Estructs == "Estructuras"){
                    console.log("changes")
                var mdreports = this.getModel("OSSHED").getProperty("/records")
                console.log("Longitud ",TypeEstructs.length)
               if(TypeEstructs.length == 1){
                if(TypeEstructs == "Granjas"){
                   cont = ["Código Granja", "Nombre Granja", "Tipo Granja"] 
                   cad = "Reporte Granjas"
                   mdreports = this.getModel("OSSHED").getProperty("/records10")
                   this.arrayObjToCsvCenter(mdreports,cont,cad)
                }
                if(TypeEstructs == "Núcleos"){
                    cont = ["Código Núcleo", "Nombre Núcleo"]
                    mdreports = this.getModel("OSSHED").getProperty("/centers")
                    cad = "Reporte Nucleos"
                    this.arrayObjToCsvCenter(mdreports,cont,cad)
                }
                else
                    if(TypeEstructs == "Galpones"){
                       cont = ["Código Galpón", "Ancho (Mts)", "Alto (Mts)", "Cap. Mín (UND)","Cap. Máx (UND)", "Días de Rotación"]
                       cad = "Reporte Galpones"
                       this.arrayObjToCsvCenter(mdreports,cont,cad)
                    }
                    else
                        if(TypeEstructs == "Máquinas"){
                          cont = ["Código Máquina", "Nombre Máquina", "Descripción Máquina", "Capacidad (UND)", "Domingo", "Lunes", "Martes", "Miércoles", "jueves", "Viernes", "Sábado"]
                          cad = "Reporte Máquinas"
                          this.arrayObjToCsvCenter(mdreports,cont,cad)
                        }
                    }else
                        if(TypeEstructs.length == 2){
                
                            if(TypeEstructs == "Granjas,Núcleos" ||  TypeEstructs == "Núcleos,Granjas"){
                                cont = ["Código Granja", "Nombre Graja", "Tipo Granja","Código Núcleo", "Nombre Núcleo"]
                                mdreports = this.getModel("OSSHED").getProperty("/records10")
                                cad = "Reporte Granjas_Núcleos"
                                this.arrayObjToCsvCenter(mdreports,cont,cad)
                                console.log("Hola",mdreports)
                           
                            }else
                                if(TypeEstructs == "Núcleos,Galpones" || TypeEstructs == "Galpones,Núcleos"){
                          
                                    mdreports = this.getModel("OSSHED").getProperty("/records9")
                                    cont = ["Código Núcleo", "Nombre Núcleo","Código Galpón", "Ancho (Mts)", "Alto (Mts)", "Cap. Mín (UND)","Cap. Máx (UND)", "Días de Rotación"]
                                    cad = "Reporte Nucleos_Galpones"
                                    this.arrayObjToCsvCenter(mdreports,cont,cad)
                                }else
                                    if(TypeEstructs == "Granjas,Galpones" || TypeEstructs == "Galpones,Granjas" || this.maquinas(TypeEstructs)==true){
                                        this.setearTodos()
                                        this.getModel("OSSHED").setProperty("/records3",[])
                                        this.getView().byId("btnExport").setProperty("visible", false)
                                        MessageToast.show("Combinación de estructuras ínvalida")
                                    }
                              
                        }else
                            if(TypeEstructs.length == 3&& this.maquinas(TypeEstructs)==false){
                                mdreports = this.getModel("OSSHED").getProperty("/records8")
                                console.log("Aqui llegó!")
                                     cont = ["Código Granja", "Nombre Granja","Tipo Granja","Código Núcleo","Nombre Núcleo","Código Galpón", "Ancho (Mts)", "Alto (Mts)", "Cap. Mín (UND)","Cap. Máx (UND)", "Días de Rotación"]
                                     cad = "Reporte Granjas_Nucleos_Galpones"
                                     this.arrayObjToCsvCenter(mdreports,cont,cad)

                            }else
                                {
                                    this.setearTodos()
                                    this.getModel("OSSHED").setProperty("/records3",[])
                                    MessageToast.show("Combinación de estructuras ínvalida")
                                }
                           
            }else
                if(Estructs == "Progresiva"){
                  let Progresive = this.getView().byId("filterLotEntry2").mProperties.selectedKey;
                  let ProgresiveType1 = this.getView().byId("filterLotEntry3").mProperties.selectedKeys;
                  let ProgresiveType2 = this.getView().byId("filterLotEntry4").mProperties.selectedKeys;

                    //this.getModel("OSSHED").setProperty("/exportable2")
                      if( TypePogressive == "Cría y Levante" || TypePogressive == "Producción"){

                        if(ProgresiveType1 == "Proyección")
                        {
                          mdreports = this.getModel("OSSHED").getProperty("/exportable")
                          console.log("Reportes Cría y Levante  ", mdreports)
                          cont=  ["Proyeccion Fecha", "Proyeccion Cantidad (UND)","Proyeccion Raza"]
                          if(Progresive == "Cría y Levante"){
                            cad = "Reporte Cria y Levante_Proyección"
                          }else
                              if(Progresive == "Producción"){
                                cad = "Reporte Produccion_Proyección"
                              }
                           
                           this.arrayObjToCsvCenter(mdreports,cont,cad)
                        }
                        else
                          if(ProgresiveType1.length == 2){
                            if(ProgresiveType1 == "Programación,Proyección" || ProgresiveType1 == "Proyección,Programación"){
                              mdreports = this.getModel("OSSHED").getProperty("/exportable2")
                             console.log("Reportes Cría y Levante  ", mdreports)
                          
                              cont=  ["Proyeccion Fecha", "Proyeccion Cantidad (UND)","Proyeccion Raza", "Programacion Fecha","Programacion Lote","Programacion Cantidad (UND)","Programacion Granja", "Programacion Nucleo", "Programacion Galpon"]
                              
                                if(Progresive == "Cría y Levante"){
                            cad = "Reporte Cria y Levante_Proyección_Programación"
                          }else
                              if(Progresive == "Producción"){
                                cad = "Reporte Produccion_Proyección_Programación"
                              }
                           

                              this.arrayObjToCsvCenter(mdreports,cont,cad)

  
                            }else
                                if(ProgresiveType1 == 'Proyección,Ejecución' || ProgresiveType1 == 'Ejecución,Proyección'){
                                        MessageToast.show("No ha seleccionadp la programación")
                                    /* mdreports = this.getModel("OSSHED").getProperty("/exportable2")
                                 
                                  console.log("Reportes Cría y Levante  ", mdreports)
                          
                              cont=  ["Proyeccion Fecha", "Proyeccion Cantidad (UND)","Proyeccion Raza", "Ejecucion Fecha","Ejecucion Lote","Ejecucion Cantidad (UND)","Ejecucion Granja", "Ejecucion Nucleo", "Ejecucion Galpon"]
                              
                                if(Progresive == "Cría y Levante"){
                            cad = "Reporte Cria y Levante_Proyección_Ejecución"
                          }else
                              if(Progresive == "Producción"){
                                cad = "Reporte Produccion_Proyección_Ejecución"
                              }


                              this.arrayObjToCsvCenter(mdreports,cont,cad)
  */
                                }
                             
                            
                             
                        }else
                              if(ProgresiveType1.length == 3){
                                  mdreports = this.getModel("OSSHED").getProperty("/exportable")
                             console.log("Reportes Cría y Levante  ", mdreports)
                          
                              cont=  ["Proyeccion Fecha", "Proyeccion Cantidad (UND)","Proyeccion Raza", "Programacion Fecha","Programacion Lote","Programacion Cantidad (UND)","Programacion Granja", "Programacion Nucleo", "Programacion Galpon", "Ejecucion Fecha","Ejecucion Lote","Ejecucion Cantidad (UND)","Ejecucion Granja", "Ejecucion Nucleo", "Ejecucion Galpon"]
                           
                              

                                 if(Progresive == "Cría y Levante"){
                            cad = "Reporte Cria y Levante_Proyección_Programacion_Ejecucion"  
                              }else
                              if(Progresive == "Producción"){
                                cad = "Reporte Produccion_Proyección_Programacion_Ejecucion"
                              }

                              this.arrayObjToCsvCenter(mdreports,cont,cad)
  
                              }


                      }
                }
            
            
              console.log(mdreports);
            
              // this.arrayObjToCsv();
          },
  
          arrayObjToCsvCenter: function (ar, cont,cad) {
          //comprobamos compatibilidad
              if(window.Blob && (window.URL || window.webkitURL)){
                  var contenido = "",
                      d = new Date(),
                      blob,
                      reader,
                      save,
                      clicEvent;
            
     
            
                  for (var i = 0; i < ar.length; i++) {
                      console.log(Object.keys(ar[i]));
                      //construimos cabecera del csv
                      if (i == 0){
                          contenido += cont.join(";") + "\n";
                      //resto del contenido
                      }
                      contenido += Object.keys(ar[i]).map(function(key){
                          return ar[i][key];
                      }).join(";") + "\n";
                  }
                  console.log("Holaaa", ar)
                  //creamos el blob
                  blob =  new Blob(["\ufeff", contenido], {type: "text/csv"});
                  //creamos el reader
                  var reader = new FileReader();
                  reader.onload = function (event) {
                  //escuchamos su evento load y creamos un enlace en dom
                      save = document.createElement("a");
                      save.href = event.target.result;
                      save.target = "_blank";
                      //aquí le damos nombre al archivo
                  console.log("Cadena", cad)
                      save.download = cad+".csv";
                  
  
                      try {
                      //creamos un evento click
                          clicEvent = new MouseEvent("click", {
                              "view": window,
                              "bubbles": true,
                              "cancelable": true
                          });
                      } catch (e) {
                      //si llega aquí es que probablemente implemente la forma antigua de crear un enlace
                          clicEvent = document.createEvent("MouseEvent");
                          clicEvent.initEvent("click", true, true);
                      }
                      //disparamos el evento
                      save.dispatchEvent(clicEvent);
                      //liberamos el objeto window.URL
                      (window.URL || window.webkitURL).revokeObjectURL(save.href);
                  };
                  //leemos como url
                  reader.readAsDataURL(blob);
              }else {
              //el navegador no admite esta opción
                  alert("Su navegador no permite esta acción");
              }
          },

          //Consultar las granjas 
     farms : function(){
        let selectObject = this.getView().getModel("ospartnership").oData.selectedRecord
         let osshed = this.getModel("OSSHED"),
         util = this.getModel("util");
     
      
         fetch("/farm/findFarmByPartnershipAll", {
             
             headers: {
                 'Content-Type': 'application/json'
             },
             method: 'POST',
             body: JSON.stringify({
                 "partnership_id": selectObject.partnership_id,
                
             })
         })
         .then(
             function(response) {
               if (response.status !== 200) {
                 console.log('Looks like there was a problem. Status Code: ' +
                   response.status);
             
                 return;
               }
     
               response.json().then(function(res) {
                         util.setProperty("/busy/", false);
                          osshed.setProperty("/records10",res.data);
               });
             }
           )
           .catch(function(err) {
             console.log('Fetch Error :-S', err);
           });
 
     
    
    
    },
    //Consultar granjas y centros
    farmsAndCenters : function(oEvent){
        let selectObject = this.getView().getModel("ospartnership").oData.selectedRecord
         let osshed = this.getModel("OSSHED"),
         util = this.getModel("util");
     
      
         fetch("/shed/findCenterByFarmAll", {
             
             headers: {
                 'Content-Type': 'application/json'
             },
             method: 'POST',
             body: JSON.stringify({
                "partnership_id": selectObject.partnership_id,
                 "farm_id": selectObject.farm_id
                
             })
         })
         .then(
             function(response) {
               if (response.status !== 200) {
                 console.log('Looks like there was a problem. Status Code: ' +
                   response.status);
             
                 return;
               }
     
               response.json().then(function(res) {
                 console.log("Estoy 2 en y me retorna -> ",res);
                         util.setProperty("/busy/", false);

                          osshed.setProperty("/records10",res.data);
                          console.log( osshed.getProperty("/records10"))
               });
             }
           )
           .catch(function(err) {
             console.log('Fetch Error :-S', err);
           });
 
     
    
    
    },
    //Consultar Centros y Galpones
    CentersAndSheds : function(oEvent){
        let selectObject = this.getView().getModel("ospartnership").oData.selectedRecord
         let osshed = this.getModel("OSSHED"),
         util = this.getModel("util");
     
      
         fetch("/center/findCenterByOshedAll", {
             
             headers: {
                 'Content-Type': 'application/json'
             },
             method: 'POST',
             body: JSON.stringify({
                "partnership_id": selectObject.partnership_id,
                 "center_id": selectObject.center_id
                
             })
         })
         .then(
             function(response) {
               if (response.status !== 200) {
                 console.log('Looks like there was a problem. Status Code: ' +
                   response.status);
             
                 return;
               }
     
               response.json().then(function(res) {
                 console.log("Estoy 2 en y me retorna -> ",res);
                         util.setProperty("/busy/", false);

                          osshed.setProperty("/records9",res.data);
                          console.log( osshed.getProperty("/records10"))
               });
             }
           )
           .catch(function(err) {
             console.log('Fetch Error :-S', err);
           });
 
     
    
    
    },
    //Consultar Centros
    Centers : function(oEvent){
        let selectObject = this.getView().getModel("ospartnership").oData.selectedRecord
         let osshed = this.getModel("OSSHED"),
         util = this.getModel("util");
     
      
         fetch("/center/findCenterSimpleInformation", {
             
             headers: {
                 'Content-Type': 'application/json'
             },
             method: 'POST',
             body: JSON.stringify({
                "partnership_id": selectObject.partnership_id
              
                
             })
         })
         .then(
             function(response) {
               if (response.status !== 200) {
                 console.log('Looks like there was a problem. Status Code: ' +
                   response.status);
             
                 return;
               }
     
               response.json().then(function(res) {
                 console.log("Estoy 2 en y me retorna -> ",res);
                         util.setProperty("/busy/", false);

                          osshed.setProperty("/centers",res.data);
                          console.log( osshed.getProperty("/centers"))
               });
             }
           )
           .catch(function(err) {
             console.log('Fetch Error :-S', err);
           });

    },
    //Consultar granjas,centros y galpones
    farmsAndCentersAndSheds : function(){
        let selectObject = this.getView().getModel("ospartnership").oData.selectedRecord
         let osshed = this.getModel("OSSHED"),
         util = this.getModel("util");
     
      
         fetch("/shed/findAllInformation", {
             
             headers: {
                 'Content-Type': 'application/json'
             },
             method: 'POST',
             body: JSON.stringify({
                "partnership_id": selectObject.partnership_id,
                 "farm_id" :selectObject.farm_id,
                 "center_id": selectObject.center_id
                
             })
         })
         .then(
             function(response) {
               if (response.status !== 200) {
                 console.log('Looks like there was a problem. Status Code: ' +
                   response.status);
             
                 return;
               }
     
               response.json().then(function(res) {
                 console.log("Estoy 2 en y me retorna -> ",res);
                         util.setProperty("/busy/", false);

                          osshed.setProperty("/records8",res.data);
                          console.log( osshed.getProperty("/records8"))
               });
             }
           )
           .catch(function(err) {
             console.log('Fetch Error :-S', err);
           });
 
     
    
    }
    ,   //Carga de los modelos y asignación de valor a las variables del Detail que dependen del Master
          onRead: async function (index) {
              
            let ospartnership = this.getModel("ospartnership"),
            mdscenario = this.getModel("mdscenario"),
            mdparameter_breed = this.getModel("mdparameter_breed"),
            oView = this.getView();
            let Estructs = this.getView().byId("filterOriginEntry").mProperties.selectedKey;
           
           //this.onSelection(oEvent)
           //this.loadBreed()
           this.loadLot()

          let partnership_id = ospartnership.getProperty("/records/" + this.index + "/partnership_id"),
            activeS = await this.activeScenario()
      
          mdscenario.setProperty("/scenario_id", activeS.scenario_id);
          mdscenario.setProperty("/name",Estructs);
    
          ospartnership.setProperty("/selectedRecordPath/", "/records/" + index);
          ospartnership.setProperty("/selectedRecord/", ospartnership.getProperty(ospartnership.getProperty("/selectedRecordPath/")));
          this.onIncubatorPlant()
                   
          },
          //Redireccionamiento de la ruta al hacer reload page
          onRead2: async function (index) {
              
            let ospartnership = this.getModel("ospartnership"),
            mdscenario = this.getModel("mdscenario"),
            mdparameter_breed = this.getModel("mdparameter_breed"),
            oView = this.getView();
            this.getRouter().navTo("detail", {
                partnership_id: 1,
                id: 0
            }, true /*create history*/ );
  
         
          let partnership_id = ospartnership.getProperty("/records/" + this.index + "/partnership_id"),
            activeS = await this.activeScenario()
                console.log("Se setea")
        
    
          ospartnership.setProperty("/selectedRecordPath/", "/records/" + index);
          ospartnership.setProperty("/selectedRecord/", ospartnership.getProperty(ospartnership.getProperty("/selectedRecordPath/")));
    
                   
          },

          //Reload del Detail
    reloadPartnership: function(){
        let util = this.getModel("util");
        console.log("Modelo Útil",util);
        let that = this;
        let ospartnership = this.getModel("ospartnership");
  
        util.setProperty("/busy/", true);
        ospartnership.setProperty("/records", []);
  
        let url = util.getProperty("/serviceUrl") +util.getProperty("/" + util.getProperty("/service") + "/getPartnership");
        console.log("URL",url)
        let method = "GET";
        let data = {};
        return new Promise((resolve, reject) => {
            function getPartnership(res) {
                util.setProperty("/busy/", false);
                ospartnership.setProperty("/records/", res.data);
                if(res.data.length>0){
                  console.log("Reload partnership")
                    let obj= res.data[0];
                    obj.index= 0;
                    ospartnership.setProperty("/selectedRecords/",obj);
                    ospartnership.setProperty("/name", obj.name);
                    ospartnership.setProperty("/address", "Bucaramanga");
                    ospartnership.setProperty("/selectedRecordPath/", "/records/"+0);
                    ospartnership.setProperty("/selectedRecord/",obj.index);
                  
              
                }
                resolve(res.data);
            };
  
            function error(err) {
                console.log(err);
                ospartnership.setProperty("/selectedRecords/", []);
                util.setProperty("/error/status", err.status);
                util.setProperty("/error/statusText", err.statusText);
                reject(err);
            };
  
            /*Envía la solicitud*/
            this.sendRequest.call(this, url, method, data, getPartnership, error, error);
        });
    },

    //Consulta el escenario activo
        activeScenario: function () {
            let that= this;
            let util = this.getModel("util");
            const serverName = util.getProperty("/serviceUrl") + util.getProperty("/" + util.getProperty("/service") + "/activeScenario");
            return new Promise((resolve, reject) => {
              fetch(serverName)
                .then(
                  function (response) {
                    if (response.status !== 200) {
                        console.log('Looks like there was a problem. Status Code: ' +response.status);
                        that.openDialog(stateError, "Ha ocurrido un error.")
                        return;
                    }
                    response.json().then(function (res) {
                        resolve(res);
                    });
                  }
                )
                .catch(function (err) {
                  console.log('Fetch Error :-S', err);
                  that.openDialog(stateError, "Ha ocurrido un error.")
                });
    
            });
        },
        
    
    //Filtros de búsqueda 
        //Filtro de Estructuras y Producción
        handleSelectionChange : function(oEvent){
          let slot = this.getView().byId("filterOriginEntry").getSelectedKey()
          let Progressive = this.getView().byId("filterLotEntry2").mProperties.selectedKey
          
         this.setearFilters()
          this.getView().getModel("mdscenario").setProperty("/name",slot);
          
              if(slot == "Estructuras"){ 
                        this.setearTodos()
                        this.getView().byId("filterLotEntry1").setSelectedKeys()
                        this.getView().byId("filterLotEntry1").setProperty("visible", true)
                        this.getView().byId("Progresive").setProperty("visible",false)
                        this.getView().byId("exportBtnP").setProperty("visible",false)
                        this.getView().byId("ProgresiveCurvIn").setProperty("visible", false)
                      
              }else{
                    if(slot == "Progresiva"){
                        this.setearFilters()
                        this.setearTodos()
                        this.getView().getModel("OSSHED").setProperty("/records3",[])
                        this.getView().getModel("OSSHED").setProperty("/progressive",[])
                        this.getView().byId("filterLotEntry2").setProperty("visible", true)
                        this.getView().byId("filterLotEntry3").setProperty("visible", true)
                        this.getView().byId("Galpon").setProperty("visible", false)
                        console.log("Vamos!!!!!")
                        if(slot == "Progresiva" && (Progressive== "Cría y Levante" || Progressive== "Producción" || Progressive== "Engorde")){
                            this.getView().byId("Progresive").setProperty("visible",true)  

                        }else
                            {
                                console.log("En la activación de tablas")
                                this.getView().byId("ProgresiveCurvIn").setProperty("visible",true)
                                this.getView().byId("filterBreed").setProperty("visible", true)

                            }
                        
                        this.getView().byId("exportBtn").setProperty("visible",false)
                        
                    }
              }
    
    
    
        },
        //Filtro de Tipos de Estructuras : Granjas, Núcleos, Galpones, Máquinas
        handleSelectionChange1 : function(oEvent){
            //let origin =  this.getView().byId("filterOriginEntry").getSelectedKey();
            let TypeEstructs = this.getView().byId("filterLotEntry1").mProperties.selectedKeys;
            let slot = this.getView().byId("filterOriginEntry").getSelectedKey()
            //let  mdinventory = this.getModel("mdinventory");
            //this.setearFilters()
            this.getView().getModel("mdscenario").setProperty("/name",slot);
           
           
            this.getView().getModel("OSSHED").setProperty("/records3",[])
            this.getView().byId("exportBtn").setProperty("visible",false)
            this.setearTodos()
            this.getView().byId("filterLotEntry3").setSelectedKeys([])
            this.getView().byId("filterLotEntry4").setSelectedKeys([])
            if(TypeEstructs.length > 1&& this.maquinas(TypeEstructs)==true){
                MessageToast.show("La combinación de estructuras es inválida")
                
            }else
                if(TypeEstructs == "Granjas,Galpones" || TypeEstructs == "Galpones,Granjas"){
                    MessageToast.show("La combinación de estructuras es inválida")
                }
                
                //this.getView().byId("filterLotEntry4").mAggregations.items[0].mProperties.text = 'jojojo'
      
          },
        setearFilters: function(){
            this.getView().byId("filterLotEntry3").setProperty("visible", false);
            this.getView().byId("filterLotEntry4").setProperty("visible", false);
            this.getView().byId("filterLotEntry2").setProperty("visible", false);
            this.getView().byId("filterLotEntry1").setProperty("visible", false);
            this.getView().byId("filterLotEntry2").setSelectedKey("")
            this.getView().byId("filterLotEntry3").setSelectedKeys([]) 
            this.getView().byId("filterLotEntry4").setSelectedKeys([])
                

        },
        handleSelectionChange3: function(oEvent){
            console.log("HANDLESELECTION3")
            var oView = this.getView();
            var oDialog = oView.byId("filterLotEntry2").getSelectedKey();
            let ProgProg =this.getView().byId("filterLotEntry2").getSelectedKey()
            let ProgY =  this.getView().byId("filterLotEntry3").getSelectedKeys()
            let ProgY2 =  this.getView().byId("filterLotEntry4").getSelectedKeys()
            let prueba = this.getView().byId("filterLotEntry3")
            let mdinventory = this.getModel("mdinventory")
            this.getView().byId("scheduled_dateBroilerI").setProperty("visible",false)
                       this.getView().byId("scheduled_dateBroilerS").setProperty("visible",false)
           
           
             this.desactiveProgessiveColumns();
             this.getView().byId("exportBtnP").setProperty("visible",false)
             this.getView().byId("exportBtnP").setProperty("visible",false)
             this.getView().byId("filterLotEntry3").setSelectedKeys([])
             this.getView().byId("filterLotEntry4").setSelectedKeys([])
              let optsOld = [{name: "Proyección"},{name : "Programación"},{name : "Ejecución"}]
                            mdinventory.setProperty("/opts3",optsOld)


            //this.getView().byId("/progressive",[])
            //this.getView().byId("/progressiveIncub",[])
            let osshed = this.getModel("OSSHED")
            osshed.setProperty("/progressive",[])
            osshed.setProperty("/progressiveCurvIn",[])
            this.getView().byId("scheduled_dateBroiler").setProperty("visible",false)
            this.getView().byId("inputLot").setProperty("visible",false)
            this.getView().byId("filterBreed").setProperty("visible", false)
            this.onBreedLoad(oEvent)
            if(oDialog == "Cría y Levante" ||  oDialog == "Producción"  || oDialog == "Engorde"){
                this.getView().byId("Progresive").setProperty("visible",true)                 
                    this.getView().byId("filterLotEntry4").setProperty("visible", false)
                    this.getView().byId("filterLotEntry3").setProperty("visible", true) 
                    this.getView().byId("filterLotEntry4").setSelectedKeys([])
                    this.getView().byId("filterBreed").setProperty("visible", false)
                    this.getView().byId("inputLot").setProperty("visible", false)
                    
                    if(oDialog == "Engorde"){
                        this.getView().byId("scheduled_dateBroiler").setProperty("visible",true)
                        this.getView().byId("filterBreed").setProperty("visible", true) 
                        let optsNew = [{name: "Proyectado"},{name : "Programación"},{name : "Ejecución"}]
                          mdinventory.setProperty("/opts3",optsNew)
                    }else
                        {
                            let optsOld = [{name: "Proyección"},{name : "Programación"},{name : "Ejecución"}]
                            mdinventory.setProperty("/opts3",optsOld)
                        }

                       
                    
            }else{
               
                this.getView().byId("filterBreed").setProperty("visible", false)
                    this.getView().byId("filterLotEntry3").setProperty("visible", false)
                    this.getView().byId("filterLotEntry4").setProperty("visible", true)
                    this.getView().byId("filterLotEntry3").setSelectedKeys([])
                    this.getView().byId("filterLotEntry1").setSelectedKeys([])
        
            }

            if(oDialog == "Curva de Postura"){
                this.getView().byId("filterBreed").setProperty("visible", true)
                this.getView().byId("Progresive").setProperty("visible",false)
                this.getView().byId("ProgresiveCurvIn").setProperty("visible", true)
                let optsOld = [{name: "Proyección"},{name : "Programación"},{name : "Ejecución"}]
                mdinventory.setProperty("/opts3",optsOld)
            }else
             if(oDialog == 'Incubadora'){
              this.getView().byId("ProgresiveCurvIn").setProperty("visible", true)
              this.getView().byId("Progresive").setProperty("visible",false)

                       this.getView().byId("scheduled_dateBroilerI").setProperty("visible",true)
                       this.getView().byId("scheduled_dateBroilerS").setProperty("visible",true)
                        this.getView().byId("filterBreed").setProperty("visible", true) 
                    }
                      
            
          
        },
     //Filtro de Producción
     handleSelectionChange4: function(oEvent){
        console.log("En handleSelectionChange4")
        this.desactiveProgessiveColumns();
        let ProgProg =this.getView().byId("filterLotEntry2").getSelectedKey()
        let ProgY =  this.getView().byId("filterLotEntry3").getSelectedKeys()
        let ProgY2 =  this.getView().byId("filterLotEntry4").getSelectedKeys()
        let osshed = this.getModel("OSSHED")
        this.getView().byId("filterLotEntry4").getSelectedKeys
       
        console.log("Hola babeeee")
        this.getView().byId("exportBtnP").setProperty("visible",false)
       
     },
     handleSelectionChange5: function(oEvent){
        console.log("En handleSelectionChange4")
        let ProgProg =this.getView().byId("filterLotEntry2").getSelectedKey()
        let ProgY =  this.getView().byId("filterLotEntry3").getSelectedKeys()
        let ProgY2 =  this.getView().byId("filterLotEntry4").getSelectedKeys()
        this.getView().byId("filterLotEntry4").getSelectedKeys
        //osshed.setProperty("/progressive",[])
        console.log("En los filtros", ProgProg,ProgY, ProgY2 )

     },

        //Habilitar columnas según la  Estructura seleccionada
        habColumn: function(oEvent, select, select1){
     
          this.setearTodos();
            if(select =="Estructuras" && select1 != null){
                if(select1 == "Granjas" ||   select1=="Granjas,Galpón" ){
                    console.log(this.getView());
                    if(select1 == "Granjas"){
                        this.getView().byId("c1").setProperty("visible", true)
                        this.getView().byId("c2").setProperty("visible", true)
                        this.getView().byId("c3").setProperty("visible", true)
                        this.farms(); 

                    }
                 
                }else
                    if(select1 == "Núcleos" ){
                        this.getView().byId("c1").setProperty("visible", true)
                        this.getView().byId("c2").setProperty("visible", true)
                        this.Centers();
    
                    }else
                        if(select1 == "Galpones"){
                            this.getView().byId("c1").setProperty("visible", true)
                            this.getView().byId("c4").setProperty("visible", true)
                            this.getView().byId("c5").setProperty("visible", true)
                            this.getView().byId("c6").setProperty("visible", true)
                            this.getView().byId("c7").setProperty("visible", true)
                            this.getView().byId("c8").setProperty("visible", true)
                        
                        }else
                            if(select1 == "Máquinas"){
                                this.getView().byId("c1").setProperty("visible", true)
                                this.getView().byId("c2").setProperty("visible", true)
                                this.getView().byId("c9").setProperty("visible", true)
                                this.getView().byId("c10").setProperty("visible", true)
                                this.getView().byId("c11").setProperty("visible", true)
                                this.getView().byId("t11").setProperty("visible", true)
                            }
            
            
            }
        },
         //AQUI SE HABILITARAN LAS COLUMNAS SEGUN LA SELECCION DE LOS CAMPOS Y EL PRIMER SELECT
        habColumn2: function(oEvent, select, select1){
            //Cuando se pulse el boton de busqueda se invoca!!
            this.setearTodos();
            if(this.maquinas(select1)==false&& !(select1== "Granjas,Galpones" ||  select1 == "Galpones,Granjas")){
                    if(select1 == "Granjas,Núcleos" ||select1 == "Núcleos,Granjas"){
                        this.getView().byId("c1").setProperty("visible", true)
                        this.getView().byId("c2").setProperty("visible", true)
                        this.getView().byId("c3").setProperty("visible", true)
                        this.getView().byId("c12").setProperty("visible", true)
                        this.getView().byId("t12").setProperty("visible", true)
                        this.farmsAndCenters(oEvent)
                      
                    }else
                            if(select1 == "Núcleos,Galpones" || select1 == "Galpones,Núcleos"){
                                this.getView().byId("c1").setProperty("visible", true)
                                this.getView().byId("c2").setProperty("visible", true)
                                this.getView().byId("c13").setProperty("visible",true)
                                this.getView().byId("t13").setProperty("visible", true)
                                this.CentersAndSheds()
                              
                            }
                        
            }else
                {
                    MessageToast.show("Combinación de estructuras no permitida") 
                }
        },
        habColumn3: function(oEvent, select, select1, select2,slect3,select4){
            //AQUI SE HABILITARAN LAS COLUMNAS SEGUN LA SELECCION DE LOS CAMPOS Y EL PRIMER SELECT
            //Cuando se pulse el boton de busqueda se invoca!!
            if(select =="Estructuras" && select1 != null){
                this.setearTodos()
                    this.getView().byId("c1").setProperty("visible", true)
                    this.getView().byId("c2").setProperty("visible", true)
                    this.getView().byId("c3").setProperty("visible", true)
                    this.getView().byId("c14").setProperty("visible", true)
                    this.getView().byId("t14").setProperty("visible", true)
                    
                    this.farmsAndCentersAndSheds()
            }
        },
        setear : function(){

            this.getView().byId("c1").setProperty("visible",true)
            this.getView().byId("c2").setProperty("visible",true)

        },
        setearTodos : function(){    
            this.getView().byId("c1").setProperty("visible",false)
            this.getView().byId("c2").setProperty("visible",false)
            
            this.getView().byId("c3").setProperty("visible",false)
            this.getView().byId("c4").setProperty("visible",false)

            
            this.getView().byId("c5").setProperty("visible",false)
            this.getView().byId("c6").setProperty("visible",false)

            
            this.getView().byId("c7").setProperty("visible",false)
            this.getView().byId("c8").setProperty("visible",false)

            
            this.getView().byId("c9").setProperty("visible",false)
            this.getView().byId("c10").setProperty("visible",false)
            this.getView().byId("c11").setProperty("visible",false)
            this.getView().byId("t11").setProperty("visible", false)
            this.getView().byId("c12").setProperty("visible",false)
            this.getView().byId("t12").setProperty("visible", false)
            this.getView().byId("c13").setProperty("visible",false)
            this.getView().byId("t13").setProperty("visible", false)
            this.getView().byId("c14").setProperty("visible",false)
            this.getView().byId("t14").setProperty("visible", false)
           
        },
        //Función para el evento "Buscar de la consulta"
        searchEntry: async function (oEvent) {
    
          
           //Valor del primer filtro de Estructuras: "Estructuras" o "Progresiva"
           let Estructs = this.getView().byId("filterOriginEntry").mProperties.selectedKey;
           let TypeEstructs = this.getView().byId("filterLotEntry1").mProperties.selectedKeys;
           let Progresive = this.getView().byId("filterLotEntry2").mProperties.selectedKey;
           let ProgresiveType1 = this.getView().byId("filterLotEntry3").mProperties.selectedKeys;
           let ProgresiveType2 = this.getView().byId("filterLotEntry4").mProperties.selectedKeys;
           let osshed = this.getModel("OSSHED")
           let mdinventory = this.getModel("mdinventory"),
           scheduled_date = this.getView().byId("scheduled_dateBroiler").mProperties.value,
           scheduled_dateI = this.getView().byId("scheduled_dateBroilerI").mProperties.value,
           scheduled_dateS = this.getView().byId("scheduled_dateBroilerS").mProperties.value
                                
           //Se setea el registro principal donde se cargan los JSON de las consultas 
           this.getView().getModel("OSSHED").setProperty("/records3",[])
            this.getView().getModel("OSSHED").setProperty("/progressive",[])
           //Se setean las columnas de las tablas (se desactivan) y se reactivan las necesarias según el caso
            this.setearTodos();
            this.desactiveProgessiveColumns();
          const link = oEvent.getSource()
            //Según la consulta se carga
          
                if(Estructs == "Estructuras"){
                
                    if(TypeEstructs.length!=0){
                    if(TypeEstructs.length >1 &&this.maquinas(TypeEstructs)==true ||( TypeEstructs=="Galpones,Granjas"||TypeEstructs=="Granjas,Galpones")){

                        
                        if(this.maquinas(TypeEstructs)){
                            MessageToast.show("La estructura máquinas no puede combinarse")
                            console.log("Aquí ando",this.getView().byId("filterLotEntry1"))
                        
                        }else{
         
                             MessageToast.show("Combinación de estructuras no permitida")
                        }
                         
                            this.getView().byId("filterLotEntry1").setSelectedKeys([])
                            this.getView().byId("exportBtn").setProperty("visible", false)
                    }else
                        {
                            this.proccesRequestE(oEvent)
                            
                        }
                }  
            else
                {
                    MessageToast.show("Consulta vacía")
                    this.getView().byId("exportBtn").setProperty("visible",false)
                    this.getView().byId("filterLotEntry1").setSelectedKeys([])
                  
                    }
                
            }
            else
                {
                    if(ProgresiveType1.length != 0   || ProgresiveType2.length != 0 ){
                        console.log("Hola mi pana")
                          this.proccesRequestP(oEvent)
                            this.searchProgressive(oEvent)
                        if(Progresive !== "Engorde"&& Progresive !== "Incubadora"){
                        
                        if(Progresive == "Cría y Levante" ){
                            mdinventory.setProperty("/stage",5)
                           
                        }else
                            if(Progresive == "Producción"){
                                mdinventory.setProperty("/stage",3)
                            }
                        
                      if(Progresive == 'Cría y Levante' || Progresive == 'Producción'){

                        
                        if(ProgresiveType1 == 'Programación,Proyección' ||ProgresiveType1 == 'Proyección,Programación'){
                            this.proccesRequestP(oEvent)
                            this.searchProgressive(oEvent)
                            const route = "/housingWayDetail/findHousingWayDetByHwSimple"
                        console.log("Caso 1")
                        this.reloadProgrammedByCodeProgramms(oEvent,route)  
                        
                        
                      }else
                          if(ProgresiveType1 == 'Proyección,Ejecución' || ProgresiveType1 == 'Ejecución,Proyección'){
                                MessageToast.show("No ha seleccionado la programación correspondiente")
                          }else 
                              if(ProgresiveType1.length == 3){
                                this.proccesRequestP(oEvent)
                                this.searchProgressive(oEvent)
                                    const route = "/housingWayDetail/findHousingWayDetByHwSimpleAllExecute"
                                    this.reloadProgrammedByCodeProgramms(oEvent,route)
                              }
                      
  
                      }

                      if(Progresive == "Curva de Postura"){
                          this.proccesRequestP(oEvent)
                                this.searchProgressive(oEvent)
                      }
                      
                   
                        
                        /*if(Progresive == "Producción"){
                         
                            console.log("DATOS" ,osshed.getProperty("/proyect3").length)
                        }*/
                    }else{
                        if(Progresive == "Engorde" ){


                          if(scheduled_date !== ""){
                            this.proccesRequestP(oEvent)
                            console.log("Aquí en engorde")
                            this.onPress(oEvent)
                        }else{
                            MessageToast.show("Ingrese fecha a consultar")
                        }

                      }else
                          if(Progresive == "Incubadora"){
                              console.log("Fechas -->",scheduled_dateI,scheduled_dateS)
                              
                           
                              
                           
                             if(scheduled_dateI !== "" && scheduled_dateS!== ""){
                            this.proccesRequestP(oEvent)
                           this.incubatorProjected() 
                        }else{
                            MessageToast.show("Ingrese fecha a consultar")
                        }
                        
                             
                              
                       
                      
                    }
                  }
                }else{
                    MessageToast.show("Consulta vacía")
                }
                
                }

                        
	
                
              
        },
        //Procesa las solicitudes de estructuras
        proccesRequestE: function(oEvent){
           let Estructs = this.getView().byId("filterOriginEntry").mProperties.selectedKey;
           let TypeEstructs = this.getView().byId("filterLotEntry1").mProperties.selectedKeys;
           let Progresive = this.getView().byId("filterLotEntry2").mProperties.selectedKey;
           let ProgresiveType1 = this.getView().byId("filterLotEntry3").mProperties.selectedKeys;
           let ProgresiveType2 = this.getView().byId("filterLotEntry4").mProperties.selectedKeys;
           
           //Se valida si la consulta especifia la Estructura: Granjas, Núcleos, Galpones, Máquinas
            if(TypeEstructs.length != 0){

                //Se activa la tabla correspondiente
                this.nameConsult(Estructs,TypeEstructs)
                this.getView().getModel("mdscenario").setProperty("/name",Estructs+" - "+this.nameConsult(Estructs,TypeEstructs))
               
                //Se hace visible la tabla de Estructuras
                this.getView().byId("Galpon").setProperty("visible", true)
                if(TypeEstructs.length == 1){
                    this.setearTodos();
                    this.habColumn(oEvent,Estructs,TypeEstructs)
                    this.searchEstructures(oEvent)
                    this.getView().byId("exportBtn").setProperty("visible", true)
                    }else 
                    if(TypeEstructs.length == 2){
                        this.searchEstructures(oEvent)
                        this.habColumn2(oEvent,Estructs,TypeEstructs)  
                        this.getView().byId("exportBtn").setProperty("visible", true)
                    }else
                        if(TypeEstructs.length == 3){
                                this.setearTodos();
                                if(this.maquinas(TypeEstructs)==true){
                                
                                    MessageToast.show("La opción Máquinas, no debe combinarse")
                                    this.getView().byId("exportBtn").setProperty("visible",false)
                                }
                                else
                                    {
                                        this.habColumn3(oEvent,Estructs,TypeEstructs,Progresive,ProgresiveType1,ProgresiveType2)
                                        this.searchEstructures(oEvent)
                                        this.getView().byId("Galpon").setProperty("visible", true)
                                        this.getView().byId("exportBtn").setProperty("visible", true)
                                    }
                        
            }else
                if(TypeEstructs.length == 4){
                    this.setearTodos();
                    MessageToast.show("La opción Máquinas, no debe combinarse")  
                }

            }else
                {
                    MessageToast.show("Consulta vacía!");
                }

        },
        proccesRequestP: function(oEvent){
            let Estructs = this.getView().byId("filterOriginEntry").mProperties.selectedKey;
            let TypeEstructs = this.getView().byId("filterLotEntry1").mProperties.selectedKeys;
            let Progresive = this.getView().byId("filterLotEntry2").mProperties.selectedKey;
            let ProgresiveType1 = this.getView().byId("filterLotEntry3").mProperties.selectedKeys;
            let ProgresiveType2 = this.getView().byId("filterLotEntry4").mProperties.selectedKeys;
            let osshed = this.getModel("OSSHED")
            this.getView().getModel("mdscenario").setProperty("/name",Estructs+" - "+this.nameConsult(Estructs,TypeEstructs))
            if((Estructs == "Progresiva" && ProgresiveType1!=  "") || (Estructs == "Progresiva" && ProgresiveType2!=  "")){
            this.getView().getModel("mdscenario").setProperty("/name",Estructs+" > "+Progresive+" "+ProgresiveType1)
    
                if(ProgresiveType1.length!=0 &&ProgresiveType2.length==0){
                    console.log("Caso Cría Levante, Producción y Engorde")
                    if(ProgresiveType1.length == 1 ){
                      
                        if( Progresive == "Cría y Levante" || Progresive == "Producción"){
                            if(ProgresiveType1 == "Proyección"){
                               
                               this.searchProgressiveSimple(oEvent)
                                this.activeColumnsSimple(Progresive,ProgresiveType1)
                              
                              
    
                            }else
                                {
                                    //Cuando la solicitud del Multibox de Progresiva es diferente a la "Proyección" que es obligatoria
                                    this.getView().byId("exportBtnP").setProperty("visible",false)
                                    MessageToast.show("Seleccione la proyección") 
                                    this.getView().byId("filterLotEntry3").setSelectedKeys([])
                                }
    
                        }else
                            {   //En el caso de Engorde
                                if(ProgresiveType1 == "Proyectado"){
                  
                                    this.activeColumnsSimple(Progresive,ProgresiveType1)
                                  
                                    this.getView().byId("exportBtnP").setProperty("visible",true)
        
                                }else
                                    {
                                        //Cuando la solicitud del Multibox de Progresiva es diferente a la "Proyección" que es obligatoria
                                        this.getView().byId("exportBtnP").setProperty("visible",false)
                                        MessageToast.show("Seleccione el proyectado") 
                 
                                        this.getView().byId("filterLotEntry3").setSelectedKeys([])
                                    }
                            }
                        
                    }else
                        if(ProgresiveType1.length == 2){
                            //Valida dos solicitudes donde al menos alguna sea "Proyección"
                          
                            if(Progresive == "Cría y Levante"  || Progresive == "Producción"){
                                if(this.findProgInArray(ProgresiveType1)){
                                    
                                    this.activeColumnsDoble(Progresive,ProgresiveType1)
                          
                                    this.getView().byId("exportBtnP").setProperty("visible",true)
                                    }
                                    else{
                                        MessageToast.show("Seleccione la Proyección")
                                        this.getView().byId("exportBtnP").setProperty("visible",false)
                                        this.getView().byId("filterLotEntry3").setSelectedKeys([])
                                    }

                            }else
                                {
                                    //Haciendo la validación para el caso de Engorde
                                    if(this.findProyInArray(ProgresiveType1)){
                                        this.activeColumnsDoble(Progresive,ProgresiveType1)
                                     
                                        
                                        }
                                        else{
                                            MessageToast.show("Seleccione el Proyectado")
                                            this.getView().byId("exportBtnP").setProperty("visible",false)
                                            this.getView().byId("filterLotEntry3").setSelectedKeys([])
                                        }
                                }
                            



                        }else
                            if(ProgresiveType1.length == 3){
                                console.log("Caso Interno  **ProgresiveType1.length** ",ProgresiveType1.length)
                                console.log("Consulta triple")
                                this.activeColumnsTriple(Progresive,ProgresiveType1)
                  
                                this.getView().byId("exportBtnP").setProperty("visible",true)   
                            }


                }else
                    if(ProgresiveType1.length==0 && ProgresiveType2.length!=0){
                  console.log("Aquí voy")
                  this.getView().byId("ProgresiveCurvIn").setProperty("visible", true)
                 
                        if(Progresive == "Curva de Postura"){
                           if(ProgresiveType2 == "Proyectado" && Progresive == "Curva de Postura"){
                            osshed.setProperty("/progressive",[])
                            this.loadListLot()
                            this.activeColumnsSimple(Progresive,ProgresiveType1)
                                //this.getView().byId("d14").setProperty("visible", true) 
                           
                           

                                        this.activeColumnsSimple(Progresive,ProgresiveType1)
                                     
                                        console.log("Segundo selector")
                                        this.getView().byId("exportBtnP").setProperty("visible",true)
                                    
                            }else
                            if( Progresive == "Curva de Postura" &&(ProgresiveType2 == "Proyectado,Ejecutado" || ProgresiveType2 == "Ejecutado,Proyectado") && Progresive == "Curva de Postura"){
                                osshed.setProperty("/progressive",[])
                                this.loadListLot()
                                this.activeColumnsSimple(Progresive,ProgresiveType1)
                                this.getView().byId("d14").setProperty("visible", true)   
                                console.log("Segundo selector")
                                this.getView().byId("exportBtnP").setProperty("visible",true)
                                }else
                                    { this.desactiveProgessiveColumns()
                                        MessageToast.show("Seleccione la proyección")
                                    }
                            }else
                                if(Progresive == "Incubadora" &&((ProgresiveType2 == "Proyectado")||ProgresiveType2 == "Proyectado,Ejecutado" || ProgresiveType2 == "Ejecutado,Proyectado")){
                                  
                                  if(ProgresiveType2 == "Proyectado"){
                                     this.activeColumnsSimple(Progresive,ProgresiveType1)
                                   }else
                                      if(ProgresiveType2 == "Proyectado,Ejecutado" || ProgresiveType2 == "Ejecutado,Proyectado"){
                                          console.log("Aquí voy")
                                           this.getView().byId("d30").setProperty("visible", true) 
                                          this.activeColumnsSimple(Progresive,ProgresiveType1)


                                         

                                          this.getView().byId("d28").setProperty("visible", true)

                                        }
                                 
                                }
                                else{
                                     MessageToast.show("Seleccione la proyección")
                                }                    
                               
                           
                     }

               

            }else
                {
                    MessageToast.show("Consulta vacía");
                    this.getView().byId("exportBtnP").setProperty("visible",false)
                }
               
             
         },
         findProgInArray : function(cad){
            let band = false;
            for(let i=0; i < cad.length; i++){
                if(cad[i] =="Proyección"){
                    band = true;
                }
            }
            return band;
         },
         findProyInArray : function(cad){
            let band = false;
            for(let i=0; i < cad.length; i++){
                if(cad[i] =="Proyectado"){
                    band = true;
                }
            }
            return band;
         },
         findStage: function(){
            let selectObject = this.getView().getModel("ospartnership").oData.selectedRecord
            console.log("En la consulta",this.getView().getModel("ospartnership"), selectObject)
            let search2 =this.getView().byId("filterLotEntry2").getSelectedKey()
            let search3 =  this.getView().byId("filterLotEntry3").getSelectedKeys()
            let search4 =  this.getView().byId("filterLotEntry4").getSelectedKeys()
            let scenario = this.getView().getModel("mdscenario").oData.scenario_id
           
        
             
             let osshed = this.getModel("OSSHED"),
             util = this.getModel("util");
         
         
             fetch("/stage/findStage", {
                 
                 headers: {
                     'Content-Type': 'application/json'
                 },
                 method: 'POST',
                 body: JSON.stringify({
                     "name" : search3
                 })
             })
             .then(
                 function(response) {
                   if (response.status !== 200) {
                     console.log('Looks like there was a problem. Status Code: ' +
                       response.status);
                 
                     return;
                   }
         
                   response.json().then(function(res) {
                             util.setProperty("/busy/", false);
                              osshed.setProperty("/progressive",res.data);
                              osshed.setProperty("/old",res.data);
                         
                   });
                 }
               )
               .catch(function(err) {
                 console.log('Fetch Error :-S', err);
               });
     
             
         
         },
         itsExecutionCurve: function(oEvent){
             
            let posturecurve=this.getView().getModel("OSSHED");
            //let modelo= this.getView().getModel("OSSHED");
            //let modeloo = this.getView().getModel("posturecurve");
            console.log(oEvent.getParameters());
            let lot= oEvent.getSource().getBindingContext("OSSHED").getObject().lot;
            console.log("Elementos Recibidos babe ",oEvent.getSource().getBindingContext("OSSHED").getObject().week);
            let num_week= oEvent.getSource().getBindingContext("OSSHED").getObject().num_week;
            let start_date = oEvent.getSource().getBindingContext("OSSHED").getObject().week;
            let year = 'Todos'
            let breed = 1
            const link = oEvent.getSource()
            // console.log({year});
            // console.log({breed});
            let week = start_date.split("/");
            var d=new Date(start_date.split("/").reverse().join("-"));
            var dd=d.getDate()+1;
            var mm=d.getMonth()+1;
            var yy=d.getFullYear();
            var newdate=yy+"/"+mm+"/"+dd;
            var end = new Date(newdate);
            end.setDate(end.getDate() + 6);
            // console.log({start_date});
            let band = true
      
            let jsonTemp = 
          { "end_date":end, 
              "init_date": start_date, 
              "lot":lot };

            posturecurve.setProperty("/savingthedate",jsonTemp);
            posturecurve.setProperty("/returnupdate",{
                breed_id: breed,
                year:year,
                lot: lot,
                num_week: num_week
        
            });

            start_date = start_date.split("/");
            start_date = start_date[2] + "-" + start_date[1] + "-" + start_date[0];
            let that = this
            let datos = []
            console.log(lot);
         
                  
                fetch("/eggs_storage/findEggsStorageDetailByYearWeekBreedLot", {
                method: "POST",
                headers: {
                    "Content-type": "application/json; charset=UTF-8"
                },

                body: JSON.stringify({
                    breed_id: breed,
                    year:year,
                    lot: lot,
                    num_week: num_week
          
                })
      
            
            })
                .then(
                    function(response) {
                        if (response.status !== 200) {
                            console.log("Looks like there was a problem. Status Code: " +
              response.status);
                            return;
                        }

                        response.json().then(function(res) {
                            res.data.forEach(item =>{

                                // let formatDate= `${(ddate.getDate() < 10 ? '0' : '') + ddate.getDate()}/${((ddate.getMonth() + 1) < 10 ? '0' : '') + (ddate.getMonth() + 1)}/${ddate.getFullYear()}`;
                                let formatDate= item.dia.split("-");
                                // console.log(formatDate);
                                let lol= formatDate[2]+"/"+formatDate[1]+"/"+formatDate[0];
                                posturecurve.setProperty("/formateDate", formatDate);

                                item.available= (item.eggs_executed==null || item.eggs_executed< 0)? true: false; 
                                item.dia= lol; 
                                
                                datos.push(item)
                                console.log("DATOOOS ", datos)
                            
                            });
                            if(res.data[0].eggs_executed == null){
                                MessageToast.show("No se encontraron ejecuciones asociadas")
                                posturecurve.setProperty("/recordsPosture",[])
                            }else
                                {
                                    posturecurve.setProperty("/recordsPosture",res.data)
                                    that._oPopoverPostureCurve.openBy(link)

                                }
                
                            
                        });

                    }
                );

                    if(band){
                    }
                
        
                
            //let oView = this.getView();
            //this.getView().getModel("posturecurve").setProperty("/executed/enabledTab");
            //oView.byId("tabBar").setSelectedKey("kTabProjected");


         },
        reloProgrammedad: function(){
            let housing_ways = [];
            let osshed = this.getModel("OSSHED")
            housing_ways.push(osshed.getProperty("/progressive")[0].housing_way_id)
            
            console.log(housing_ways);
            fetch("/housingWayDetail/findHousingWayDetByHw", {
                headers: {
                    "Content-Type": "application/json"
                },
                method: "POST",
                body: JSON.stringify({
                    records: housing_ways
                })
            })
                .then(
                    function(response) {
                        if (response.status !== 200) {
                            console.log("Looks like there was a problem. Status Code: " +
                  response.status);
                            return;
                        }
  
                        response.json().then(function(res) {
                            osshed.setProperty("/proyect",res.data);
                            osshed.setProperty("/proyect4",res.data);
                            osshed.setProperty("/proyect5",res.data);
                            osshed.setProperty("/proyect7",res.data);
                        });
                    }
                )
                .catch(function(err) {
                    console.log("Fetch Error :-S", err);
                });
            
        },
        reloadProgrammedByCode : function(oEvent){
            let housing_ways = [];
            let osshed = this.getModel("OSSHED")
            let aux = this.getModel("mdinventory")
            housing_ways.push(oEvent.getSource().getBindingContext("OSSHED").getObject().housing_way_id)
            console.log("Vector ", housing_ways)

           let arr = []
           const link = oEvent.getSource()
            
            let isRecords = new Promise((resolve, reject) =>{
            fetch("/housingWayDetail/findHousingWayDetByHw", {
                headers: {
                    "Content-Type": "application/json"
                },
                method: "POST",
                body: JSON.stringify({
                    records: housing_ways

                })
            })
                .then(
                    function(response) {
                        if (response.status !== 200) {
                            console.log("Looks like there was a problem. Status Code: " +
                         response.status);
                            return;
                        }
                        
                        response.json().then(function(res) {

                            resolve(res)
                            });
                        
                    

                    }
                )
                .catch(function(err) {
                    console.log("Fetch Error :-S", err);
                });
            });
            let that = false
            let vecEjec = []
            isRecords.then((res) => {
                if (res.data.length > 0) {
                    let records = res.data
                    records.forEach(element => {
                          
                        if(element.execution_date != null ){
                            vecEjec.push(element)
                           
                        }
                    });
                    console.log("Aquí entro", res.data)
                    osshed.setProperty("/CLE",vecEjec);
                    console.log("Datos de la programación ",osshed.getProperty("/proyect3"))
                    osshed.setProperty("/CLP", res.data);
                  
                    
                  
                    osshed.setProperty("/CLEP", res.data);
                    osshed.setProperty("/CLEPP", res.data);
                    osshed.setProperty("/proyect7", res.data);
                  
                    aux.setProperty("/prog", true)
                    that = true
              
               
                } else {
              
                    aux.setProperty("/prog",false)
                    that = false
                }

                if(that){
                    this.validatePopup(link)
                         
                }else{
                    
                     MessageToast.show("No se existen programaciones asociadas")
                }
            });
              
        
        },

            reloadProgrammedByCodeProgramms : function(oEvent, route){
            
                let selectObject = this.getView().getModel("ospartnership").oData.selectedRecords
       
                let scenario = this.getModel("mdscenario")
                let stage = this.getModel("mdinventory").getProperty("/stage")
                let that = this
                console.log("Registro, EScenario",scenario.oData.scenario_id,stage)
                console.log("Stage->>>",stage)
                 console.log("Stage->>>",stage)

               console.log("En el response---->")
                 let osshed = this.getModel("OSSHED"),
                 util = this.getModel("util");
             
            osshed.setProperty("/exportable2",[])
                 fetch(route, {
                     
                     headers: {
                         'Content-Type': 'application/json'
                     },
                     method: 'POST',
                     body: JSON.stringify({
                         "partnership_id": selectObject.partnership_id,
                         "stage_id" : scenario.oData.scenario_id,
                         "scenario_id": stage
                         
                     })
                 })
                 .then(
                     function(response) {
                       if (response.status !== 200) {
                         console.log('Looks like there was a problem. Status Code: ' +
                           response.status);
                     
                         return;
                       }
             
                       response.json().then(function(res) {
                     
                                  util.setProperty("/busy/", false);
                                  osshed.setProperty("/exportable2",res.data);
                                 console.log("En el response---->", res.data)
                                  //osshed.setProperty("/week",res.data);
                       });
                     }
                   )
                   .catch(function(err) {
                     console.log('Fetch Error :-S', err);
                   });
        
          
          
                    
              
        
        },
        validatePopup : function(link){
            
            let Estructs = this.getView().byId("filterOriginEntry").mProperties.selectedKey;
            let TypeEstructs = this.getView().byId("filterLotEntry1").mProperties.selectedKeys;
            let Progresive = this.getView().byId("filterLotEntry2").mProperties.selectedKey;
            let ProgresiveType1 = this.getView().byId("filterLotEntry3").mProperties.selectedKeys;
            let ProgresiveType2 = this.getView().byId("filterLotEntry4").mProperties.selectedKeys;
           let osshed = this.getModel("OSSHED") 
            
            if(ProgresiveType1 != ""){

                if(Progresive == "Cría y Levante" && ProgresiveType1.length == 2){
                    console.log("Aquí entro :)")
                    if(ProgresiveType1 == "Proyección,Programación"  || ProgresiveType1 == "Programación,Proyección"){
                        this._oPopoverProg.openBy(link);
                    }
                    else
                    if(ProgresiveType1 == "Proyección,Ejecución"  || ProgresiveType1 == "Ejecución,Proyección"){
                
                        this._oPopoverEjec.openBy(link)
                    
                        }
                    
                }else
                    if(Progresive == "Cría y Levante" && ProgresiveType1.length == 3){
                       
                        this._oPopoverMixPE.openBy(link)
                        
                }

                if(Progresive == "Producción" && ProgresiveType1.length == 2){
                    
                    if(ProgresiveType1 == "Proyección,Programación"  || ProgresiveType1 == "Programación,Proyección"){
                        this._oPopoverProyProg.openBy(link);
                    }
                    else
                    if(ProgresiveType1 == "Proyección,Ejecución"  || ProgresiveType1 == "Ejecución,Proyección"){
                        
                        this._oPopoverProyEjec.openBy(link)
                        //this._oPopoverProyEjec.openBy(link)
                        //this._oPopoverEjec.openBy(link)
                    
                        }
                }else
                    if(Progresive == "Producción" && ProgresiveType1.length == 3){
                        this._oPopoverProgMixExe.openBy(link)
                    }



            }




        },
         searchProgressive: function(){
          
                let selectObject = this.getView().getModel("ospartnership").oData.selectedRecords
       
                let scenario = this.getModel("mdscenario")
                let stage = this.getModel("mdinventory").getProperty("/stage")
                let that = this
                console.log("Registro, EScenario",selectObject,scenario)
                console.log("Stage->>>",stage)
                 console.log("Stage->>>",stage)

               
                 let osshed = this.getModel("OSSHED"),
                 util = this.getModel("util");
             
            
                 fetch("/housingWay/findHousingByStage", {
                     
                     headers: {
                         'Content-Type': 'application/json'
                     },
                     method: 'POST',
                     body: JSON.stringify({
                         "partnership_id": selectObject.partnership_id,
                         "scenario_id": scenario.oData.scenario_id,
                         "stage_id" : stage
                     })
                 })
                 .then(
                     function(response) {
                       if (response.status !== 200) {
                         console.log('Looks like there was a problem. Status Code: ' +
                           response.status);
                     
                         return;
                       }
             
                       response.json().then(function(res) {
                      that.getView().byId("exportBtnP").setProperty("visible", true)
                                  util.setProperty("/busy/", false);
                                  osshed.setProperty("/progressive",res.data);
                                 
                                  //osshed.setProperty("/week",res.data);
                       });
                     }
                   )
                   .catch(function(err) {
                     console.log('Fetch Error :-S', err);
                   });
        

         },
         searchProgressiveSimple: function(){
          
                let selectObject = this.getView().getModel("ospartnership").oData.selectedRecords
       
                let scenario = this.getModel("mdscenario")
                let stage = this.getModel("mdinventory").getProperty("/stage")
                let that = this
                console.log("Registro, EScenario",selectObject,scenario)
                console.log("Stage->>>",stage)
                 console.log("Stage->>>",stage)

               
                 let osshed = this.getModel("OSSHED"),
                 util = this.getModel("util");
             
            
                 fetch("/housingWay/findHousingByStageSimple", {
                     
                     headers: {
                         'Content-Type': 'application/json'
                     },
                     method: 'POST',
                     body: JSON.stringify({
                         "partnership_id": selectObject.partnership_id,
                         "scenario_id": scenario.oData.scenario_id,
                         "stage_id" : stage
                     })
                 })
                 .then(
                     function(response) {
                       if (response.status !== 200) {
                         console.log('Looks like there was a problem. Status Code: ' +
                           response.status);
                     
                         return;
                       }
             
                       response.json().then(function(res) {
                     
                                  util.setProperty("/busy/", false);
                                  osshed.setProperty("/exportable",res.data);
                                 
                                  //osshed.setProperty("/week",res.data);
                       });
                     }
                   )
                   .catch(function(err) {
                     console.log('Fetch Error :-S', err);
                   });
        

         },
         activeColumnsSimple : function(nameProg, TypeConsult){


            if(nameProg == "Cría y Levante" || nameProg == "Producción"){
                console.log("Cría y Levante")
                this.getView().byId("d1").setProperty("visible", true)
                this.getView().byId("d2").setProperty("visible", true)
                this.getView().byId("d3").setProperty("visible", true)
            }else
                    if(nameProg == "Engorde"){
                        this.getView().byId("d15").setProperty("visible", true)
                        this.getView().byId("d16").setProperty("visible", true)
                        this.getView().byId("d17").setProperty("visible", true)

                    }else
                        if(nameProg == "Curva de Postura"){
                            this.getView().byId("d8").setProperty("visible", true)
                            this.getView().byId("d9").setProperty("visible", true)
                            this.getView().byId("d10").setProperty("visible", true)
                            this.getView().byId("d11").setProperty("visible", true)
                            this.getView().byId("d12").setProperty("visible", true)
                            this.getView().byId("d13").setProperty("visible", true)

                        }else
                            if(nameProg == "Incubadora"){
                               
                                this.getView().byId("d23").setProperty("visible", true)
                                this.getView().byId("d24").setProperty("visible", true)
                                this.getView().byId("d25").setProperty("visible", true)
                                this.getView().byId("d26").setProperty("visible", true)
                                this.getView().byId("d27").setProperty("visible", true)
                            }
         },
         activeColumnsDoble : function(nameProg, TypeConsult){
             
            if(nameProg == "Cría y Levante" && (TypeConsult == "Proyección,Programación" || TypeConsult == "Programación,Proyección")){
                console.log("Cría y Levante")
                this.getView().byId("d1").setProperty("visible", true)
                this.getView().byId("d2").setProperty("visible", true)
                this.getView().byId("d3").setProperty("visible", true)
                this.getView().byId("d5").setProperty("visible", true)

            }else
                if(nameProg == "Producción" &&(TypeConsult == "Proyección,Programación" || TypeConsult == "Programación,Proyección")  ){
                    console.log("Producción")
                    this.getView().byId("d1").setProperty("visible", true)
                    this.getView().byId("d2").setProperty("visible", true)
                    this.getView().byId("d3").setProperty("visible", true)
                    this.getView().byId("d5").setProperty("visible", true)

                }else
                    if(nameProg == "Cría y Levante" &&(TypeConsult == "Proyección,Ejecución" || TypeConsult == "Ejecución,Proyección")  ){
                        this.getView().byId("d1").setProperty("visible", true)
                        this.getView().byId("d2").setProperty("visible", true)
                        this.getView().byId("d3").setProperty("visible", true)
                        this.getView().byId("d6").setProperty("visible", true)
                        //this.getView().byId("Progresive").setProperty("visible", true)
                    }
                    else
                    if(nameProg == "Producción" &&(TypeConsult == "Proyección,Ejecución" || TypeConsult == "Ejecución,Proyección")  ){
                        this.getView().byId("d1").setProperty("visible", true)
                        this.getView().byId("d2").setProperty("visible", true)
                        this.getView().byId("d3").setProperty("visible", true)
                        this.getView().byId("d6").setProperty("visible", true)
                        //this.getView().byId("Progresive").setProperty("visible", true)
                    }
                else
                    if(nameProg == "Engorde"  &&(TypeConsult == "Proyectado,Programación" || TypeConsult == "Programación,Proyectado") ){
                       this.getView().byId("d15").setProperty("visible", true)
                        this.getView().byId("d16").setProperty("visible", true)
                        this.getView().byId("d17").setProperty("visible", true)
                        this.getView().byId("d118").setProperty("visible", true)
                        //Programación

                    }else
                    if(nameProg == "Engorde"  &&(TypeConsult == "Proyectado,Ejecución" || TypeConsult == "Ejecución,Proyectado") ){
                        this.getView().byId("d15").setProperty("visible", true)
                        this.getView().byId("d16").setProperty("visible", true)
                        this.getView().byId("d17").setProperty("visible", true)
                        this.getView().byId("d119").setProperty("visible", true)
                        //Ejecución
                    }
                    else
                      if(nameProg == "Incubadora"  &&(TypeConsult == "Proyectado,Ejecutado" || TypeConsult == "Ejecutado,Proyectado")){
                        this.getView().byId("d23").setProperty("visible", true)
                                this.getView().byId("d24").setProperty("visible", true)
                                this.getView().byId("d25").setProperty("visible", true)
                                this.getView().byId("d26").setProperty("visible", true)
                                this.getView().byId("d27").setProperty("visible", true)
                                this.getView().byId("d30").setProperty("visible", true)
                      }

         },
         activeColumnsTriple: function(nameProg, TypeConsul){

            if( nameProg == "Cría y Levante"){
                this.getView().byId("d1").setProperty("visible", true)
                this.getView().byId("d2").setProperty("visible", true)
                this.getView().byId("d3").setProperty("visible", true)
                this.getView().byId("d5").setProperty("visible", true)
                
            }else
                if(nameProg == "Producción" ){
                    this.getView().byId("d1").setProperty("visible", true)
                    this.getView().byId("d2").setProperty("visible", true)
                    this.getView().byId("d3").setProperty("visible", true)
                    this.getView().byId("d7").setProperty("visible", true)
                }else
                    if(nameProg == "Engorde"){
                        this.getView().byId("d15").setProperty("visible", true)
                        this.getView().byId("d16").setProperty("visible", true)
                        this.getView().byId("d17").setProperty("visible", true)
                        this.getView().byId("d118").setProperty("visible", true)
                    }
 
         },
         desactiveProgessiveColumns: function(){
            this.getView().byId("d1").setProperty("visible", false)
            this.getView().byId("d2").setProperty("visible", false)
            this.getView().byId("d3").setProperty("visible", false)
            this.getView().byId("d4").setProperty("visible", false)
            this.getView().byId("d5").setProperty("visible", false)
            this.getView().byId("d6").setProperty("visible", false)
            this.getView().byId("d7").setProperty("visible", false)
            this.getView().byId("d8").setProperty("visible", false)
            this.getView().byId("d9").setProperty("visible", false)
            this.getView().byId("d10").setProperty("visible", false)
            this.getView().byId("d11").setProperty("visible", false)
            this.getView().byId("d12").setProperty("visible", false)
            this.getView().byId("d13").setProperty("visible", false)
            this.getView().byId("d14").setProperty("visible", false) 
            this.getView().byId("d15").setProperty("visible", false)
            this.getView().byId("d16").setProperty("visible", false)
            this.getView().byId("d17").setProperty("visible", false)
            this.getView().byId("d118").setProperty("visible", false)
            this.getView().byId("d119").setProperty("visible", false)
            this.getView().byId("ProgresiveCurvIn").setProperty("visible",false)

            /*this.getView().byId("d30").setProperty("visible",false)*/

            
            this.getView().byId("d23").setProperty("visible",false)
            this.getView().byId("d24").setProperty("visible",false)
            this.getView().byId("d25").setProperty("visible",false)
            this.getView().byId("d26").setProperty("visible",false)
            this.getView().byId("d27").setProperty("visible",false)
            this.getView().byId("d28").setProperty("visible",false)

            //this.getView().byId("Progresive").setProperty("visible",false) 
         },
         nameConsult : function(Estruct, TypeE){
            let str = ""
            
            if(TypeE == "Granjas"){
                str = "Granjas"
            }else
                if(TypeE == "Galpones"){
                    str = "Galpones"
                }else
                    if(TypeE == "Núcleos"){
                        str = "Núcleos"
                    }else
                        if(TypeE == "Máquinas"){
                            str = "Máquinas"
                        }
                        else
            if(TypeE == "Granjas,Núcleos"  ||TypeE == "Núcleos,Granjas" ){
                str = "Granjas > Núcleos"
            }
            else
                if(TypeE == "Núcleos,Galpones"  || TypeE == "Galpones,Núcleos" ){
                    str = "Núcleos > Galpones"
                }else
                    if( TypeE == "Granjas,Galpones,Núcleos" || TypeE == "Granjas,Núcleos,Galpones" || TypeE == "Núcleos,Galpones,Granjas"  || TypeE == "Núcleos,Granjas,Galpones"||
                    TypeE == "Galpones,Granjas,Núcleos" ||TypeE == "Galpones,Núcleos,Granjas"){
                        str = "Granjas > Núcleos > Galpones "
                    }
            
        

   return str;

         },
        consult3 : function(oEvent){
            let selectObject   =this.getView().getModel("ospartnership").oData.selectedRecord;    
			let osshed = this.getModel("OSSHED"),
			util = this.getModel("util");

			fetch("/shed/findOshedByPartnerShip", {
				
				headers: {
					'Content-Type': 'application/json'
				},
				method: 'POST',
				body: JSON.stringify({
					"partnership_id":selectObject.partnership_id
				})
			})
			.then(
				function(response) {
				  if (response.status !== 200) {
					console.log('Looks like there was a problem. Status Code: ' +
					  response.status);
				
					return;
				  }
		
				  response.json().then(function(res) {
					console.log("Estoy en y me retorna -> ",res);
							util.setProperty("/busy/", false);
						
                        osshed.setProperty("/records3", res.data);
                        osshed.setProperty("/records5", res.data);
                        console.log("Banco de registros", osshed.getProperty("/records5"))
                   
				
				  });
				}
			  )
			  .catch(function(err) {
				console.log('Fetch Error :-S', err);
			  });

			

            
        },
        consult4 : function(oEvent){
            let selectObject =this.getView().getModel("ospartnership").oData.selectedRecord;
            let myarr = []
            let vconsult

           for(let i =0; i < selectObject.length; i++){
                myarr.push(selectObject[i].partnership_id)
                vconsult = selectObject[i].partnership_id
           }
  
			let osshed = this.getModel("OSSHED"),
			util = this.getModel("util");
			fetch("/incubator_plant/findIncubatorByPartnerships", {
				
				headers: {
					'Content-Type': 'application/json'
				},
				method: 'POST',
				body: JSON.stringify({
					"partnership_id": selectObject.partnership_id
				})
			})
			.then(
				function(response) {
				  if (response.status !== 200) {
					console.log('Looks like there was a problem. Status Code: ' +
					  response.status);
				
					return;
				  }
		
				  response.json().then(function(res) {
					console.log("Estoy en y me retorna -> ",res);
							util.setProperty("/busy/", false);
						
                        osshed.setProperty("/records3", res.data);
                        osshed.setProperty("/records6", res.data);
                        console.log("Banco de registros", osshed.getProperty("/records6"))
     
				
				  });
				}
			  )
			  .catch(function(err) {
                console.log('Fetch Error :-S', err);
                console.log("Say hi error!")
			  });

			

            
        },
        maquinas: function(search2){
            let flag = false;

            for(let i = 0 ; i < search2.length; i++){
                if(search2[i] == "Máquinas"){
                    flag = true;

                    console.log(flag);
                  
                }

            }
            return flag;

        },
        //POPUPS
        setColumns : function(){

            this.getView().byId("t1").setProperty("visible",false)
            this.getView().byId("t2").setProperty("visible",false)
            this.getView().byId("t3").setProperty("visible",false)
            this.getView().byId("t4").setProperty("visible",false)

            this.getView().byId("t5").setProperty("visible",false)
            this.getView().byId("t6").setProperty("visible",false)
            this.getView().byId("t7").setProperty("visible",false)
            this.getView().byId("t8").setProperty("visible",false)
            this.getView().byId("t9").setProperty("visible",false)
            this.getView().byId("t10").setProperty("visible",false)
       


        },
        setTabla : function(){
            this.getView().byId("Galpon").setProperty("visible",false)
        },
		//ASIGNACIÓN 2 POPUP
		searchEstructures : function(oEvent){
           let selectObject = this.getView().getModel("ospartnership").oData.selectedRecord
           let search1 = this.getView().byId("filterOriginEntry").mProperties.selectedKey;
           let search2 = this.getView().byId("filterLotEntry1").mProperties.selectedKeys;
           let search3 = this.getView().byId("filterLotEntry2").mProperties.selectedKey;


            
			let osshed = this.getModel("OSSHED"),
			util = this.getModel("util");
		
        
			fetch("/farm/findFarmByPartnership", {
				
				headers: {
					'Content-Type': 'application/json'
				},
				method: 'POST',
				body: JSON.stringify({
                    "object": selectObject.partnership_id,
                    "search1" : search1,
                    "search2" : search2,
                    "search3" : search3
                   
				})
			})
			.then(
				function(response) {
				  if (response.status !== 200) {
					console.log('Looks like there was a problem. Status Code: ' +
					  response.status);
				
					return;
				  }
		
				  response.json().then(function(res) {
		
							util.setProperty("/busy/", false);
                             osshed.setProperty("/records3",res.data);
                             osshed.setProperty("/records",res.data);
                             console.log("Records----",osshed.getProperty("/records3"))
                             //osshed.setProperty("/week",res.data);
                  });
				}
			  )
			  .catch(function(err) {
				console.log('Fetch Error :-S', err);
			  });
    
            
         },
         
		 prueba2 : function(oEvent){

		 

            let selectObject =this.getModel("ospartnership").getProperty("/records");
            let myarr = []
            let vconsult

           for(let i =0; i < selectObject.length; i++){
                myarr.push(selectObject[i].partnership_id)
                vconsult = selectObject[i].partnership_id
           }
          
			let osshed = this.getModel("OSSHED"),
			util = this.getModel("util");
        
			fetch("/center/findCenterByPartnerShip", {
				
				headers: {
					'Content-Type': 'application/json'
				},
				method: 'POST',
				body: JSON.stringify({
					"partnership_id": vconsult
				})
			})
			.then(
				function(response) {
				  if (response.status !== 200) {
					console.log('Looks like there was a problem. Status Code: ' +
					  response.status);
				
					return;
				  }
		
				  response.json().then(function(res) {
					
					util.setProperty("/busy/", false);
                    osshed.setProperty("/records3", res.data);
                    osshed.setProperty("/records4", res.data);



				  });
				}
			  )
			  .catch(function(err) {
				console.log('Fetch Error :-S', err);
			  });

			

         },
         agrupar : function(oEvent){
            let prop = 'code_farm'
                        
                            let par  = res.data[0].code_farm
                            let par2  = res.data[0].code_center
         
                            data.push(res.data[0])
                            for(let i = 0 ; i < res.data.length; i++){
                                
                                if(res.data[i].code_farm != par){
                                    data.push(res.data[i])
                                   
                                    par =res.data[i].code_farm
                                    if(res.data[i].code_center != par2){
                                        par2 =res.data[i].code_center
                                        data2.push(res.data[i])
                                   }
                                    console.log("Par2",par,res.data[i])
                               
                                }

                            }
            
         },
		 prueba3 : function(oEvent){
            console.log("EN PRUEBAAAAAAA!!!!")
            let selectObject =this.getModel("ospartnership").getProperty("/records");
            let myarr = []
            let vconsult

           for(let i =0; i < selectObject.length; i++){
                myarr.push(selectObject[i].partnership_id)
                vconsult = selectObject[i].partnership_id
           }
            console.log("OBJETOS A CARGAR",vconsult)
			let osshed = this.getModel("OSSHED"),
			util = this.getModel("util");
            console.log(util);
			fetch("/shed/findOshedByPartnerShip", {
				
				headers: {
					'Content-Type': 'application/json'
				},
				method: 'POST',
				body: JSON.stringify({
					"partnership_id":vconsult
				})
			})
			.then(
				function(response) {
				  if (response.status !== 200) {
					console.log('Looks like there was a problem. Status Code: ' +
					  response.status);
				
					return;
				  }
		
				  response.json().then(function(res) {
					console.log("Estoy en y me retorna -> ",res);
							util.setProperty("/busy/", false);
						
                        osshed.setProperty("/records3", res.data);
                        osshed.setProperty("/records5", res.data);
				
				  });
				}
			  )
			  .catch(function(err) {
				console.log('Fetch Error :-S', err);
			  });

			

 
		 },
       
        //TraerConsultas
        pruebaRecords : function(oEvent){

			console.log("Lanzando cotufas!",this.getModel("OSSHED"))
            
            //let selectObject = oEvent.getSource().getBindingContext("data").getObject();
			let osshed = this.getModel("OSSHED"),
			util = this.getModel("util");
			console.log(osshed);
	
         },
         
		//ASIGNACIÓN 1
        consult: function(oEvent){
            console.log("En consult");
            
            this.searchEstructures(oEvent);

			//var oDialog = oView.byId("searchEntry");
			var osshed = this.getView().getModel("OSSHED");
            let sheds = osshed.getProperty("/records3");
          
	
		
        },
       

          //FUNCION PARA EL PRIMER SELECT DEL FRONT REPORTES
          changeOrigin: function(oEvent){
           //modificar visibilidad del select 2
            //let origin =  this.getView().byId("filterOriginEntry").getSelectedKey();
            let slot = this.getView().byId("filterOriginEntry").getSelectedKey()
            //let  mdinventory = this.getModel("mdinventory");
            console.log("aqui ando 2!",this.getView().byId("filterLotEntry4").getProperty("visible"),this.getView().byId("filterLotEntry3").getProperty("visible"));
            console.log(slot);
                if(slot !== "Opciones:"){
    
                    if(slot == "Estructuras"){
                        if( this.getView().byId("filterLotEntry2").getProperty("visible")== true && this.getView().byId("filterLotEntry3").getProperty("visible")== false){
                            this.getView().byId("filterLotEntry2").setProperty("visible", false);
                            this.getView().byId("filterLotEntry3").setProperty("visible", false);
                            this.getView().byId("filterLotEntry4").setProperty("visible", false);
                            this.getView().byId("filterLotEntry1").setProperty("visible", true);
                        }else 
                            if(this.getView().byId("filterLotEntry3").getProperty("visible")== true){
                            console.log("caso 2, aquiii voyy");
                            this.getView().byId("filterLotEntry3").setProperty("visible", false);
                            this.getView().byId("filterLotEntry4").setProperty("visible", false);
                            this.getView().byId("filterLotEntry2").setProperty("visible", false);
                            this.getView().byId("filterLotEntry1").setProperty("visible", true);
                  
                            console.log("Aqui ando en caso 2222");
                            
                        }else{
                                this.getView().byId("filterLotEntry1").setProperty("visible", true);
                            }
                     }else if(slot == "Progresiva"){
                        if(this.getView().byId("filterLotEntry1").getProperty("visible")==true){ 
                        this.getView().byId("filterLotEntry1").setProperty("visible", false);
                        this.getView().byId("filterLotEntry2").setProperty("visible", true);
                        }else{
                            this.getView().byId("filterLotEntry2").setProperty("visible", true);
                        }
                    }
    
                }else{
    
                    if(this.getView().byId("filterLotEntry1").getProperty("visible")==true){
                        if(this.getView().byId("filterLotEntry3").getProperty("visible")==true){
                            this.getView().byId("filterLotEntry3").setProperty("visible", false);
                        }
                        this.getView().byId("filterLotEntry1").setProperty("visible", false);
                        MessageToast.show("Escoja una opción");
                    }else
                        if( this.getView().byId("filterLotEntry2").getProperty("visible")== true){
                            
                            if(this.getView().byId("filterLotEntry3").getProperty("visible")==true){
                                this.getView().byId("filterLotEntry3").setProperty("visible", false);
                            }
                            else 
                                if(this.getView().byId("filterLotEntry4").getProperty("visible")==true){
                                    this.getView().byId("filterLotEntry4").setProperty("visible", false);        
                                }
                            this.getView().byId("filterLotEntry2").setProperty("visible", false);
                            this.getView().byId("filterLotEntry3").setProperty("visible", false);
                            MessageToast.show("Introduzca un valor valido");  
                                      this.getView().byId("filterLotEntry1").setValue("")
                        }
    
                }
    
    
         
          },
          changeOrigin2: function(oEvent){
                console.log("Entro en la función")
                
            console.log("Prueba",slot);
            if(slot !== "Opciones:"){
              if(slot === "Estructuras"){
                  if(this.getView().byId("filterLotEntry2").getProperty("visible")==true){
                    this.getView().byId("filterLotEntry2").setProperty("visible", false);
                    let select = this.getView().byId("filterLotEntry3")
                    select.setProperty("visible", true);
                  }else{
                    this.getView().byId("filterLotEntry2").setProperty("visible",true);
                    let select = this.getView().byId("filterLotEntry2")
                  }
                console.log("ACTUALIZAR LA LISTA DE ITEMS EN EL SELECT")
               let select = this.getView().byId("filterLotEntry1")
                select.setProperty("visible", true);
               }else{
                console.log("ACTUALIZAR LA LISTA DE ITEMS EN EL SELECT2") 
                if(this.getView().byId("filterLotEntry1").getProperty("visible")==true)
                this.getView().byId("filterLotEntry1").setProperty("visible", false);
                let select = this.getView().byId("filterLotEntry2")
                select.setProperty("visible", true);
               }
            }else{
                MessageToast.show("Introduzca un valor valido");
            }
          },


          //POPUPS
          handlePopoverPress: function(oEvent){
			let _oPopover = this._getResponsivePopover();
            _oPopover.setModel(oEvent.getSource().getModel());
            const incubator = oEvent.getSource().getBindingContext("OSSHED").getObject()
            this.getModel("OSSHED").setProperty("/week", [incubator])
            this._oPopover.openBy(oEvent.getSource());
			
		},
		_getResponsivePopover: function () {
		  if (!this._oPopover) {
			this._oPopover = sap.ui.xmlfragment("technicalConfiguration.view.shed.osshed_ShowPopover", this);
			this.getView().addDependent(this._oPopover);
		  }
		  return this._oPopover;
		},
		//POPUP 2
		handlePopoverPress2: function(oEvent){
			let _oPopover2 = this._getResponsivePopover2();
			_oPopover2.setModel(oEvent.getSource().getModel());
			this._oPopover2.openBy(oEvent.getSource());
				this.farmCenters(oEvent)		
			
		},
		_getResponsivePopover2: function () {
		  if (!this._oPopover2) {
			this._oPopover2 = sap.ui.xmlfragment("technicalConfiguration.view.shed.osshed_ShowPopover1", this);
			this.getView().addDependent(this._oPopover2);
		  }
		  
		  return this._oPopover2;
		},
		//POPUP 3
		handlePopoverPress3: function(oEvent){
           
            let _oPopover3 = this._getResponsivePopover3();
			_oPopover3.setModel(oEvent.getSource().getModel());
			this._oPopover3.openBy(oEvent.getSource());
				this.centerOsshed(oEvent)
			
		},
		_getResponsivePopover3: function () {
		  if (!this._oPopover3) {
			this._oPopover3 = sap.ui.xmlfragment("technicalConfiguration.view.shed.osshed_ShowPopover2", this);
			this.getView().addDependent(this._oPopover3);
		  }
		  
          return this._oPopover3;
          
		}
         , handlePopoverPress4: function(oEvent){
            let _oPopover4 = this._getResponsivePopover4();
			_oPopover4.setModel(oEvent.getSource().getModel());
			this._oPopover4.openBy(oEvent.getSource());
				this.farmCenters(oEvent)		
			
		},
		_getResponsivePopover4: function () {
		  if (!this._oPopover4) {
			this._oPopover4 = sap.ui.xmlfragment("technicalConfiguration.view.shed.osshed_ShowPopover1", this);
			this.getView().addDependent(this._oPopover4);
		  }
		  
		  return this._oPopover4;
        },

        itsProg : function(oEvent){

            let Progresive = this.getView().byId("filterLotEntry2").mProperties.selectedKey;
            let ProgresiveType1 = this.getView().byId("filterLotEntry3").mProperties.selectedKeys;
             let ProgresiveType2 = this.getView().byId("filterLotEntry4").mProperties.selectedKeys;
             this.reloadProgrammedByCode(oEvent)
            if(Progresive == "Cría y Levante" || Progresive == "Producción"){

                if(ProgresiveType1.length == 2){
                    if(Progresive == "Cría y Levante" && ProgresiveType1 == "Proyección,Programación"  || ProgresiveType1 == "Programación,Proyección" ){
                       console.log("Aquí vpy")
             
  
                      
                    }
                     /*if( Progresive == "Producción"&&ProgresiveType1 == "Proyección,Programación"  || ProgresiveType1 == "Programación,Proyección" ){
                           
                           let osshed =  this.getModel("OSSHED")
                
                       
                            //this.handlePopoverPressP(oEvent)
                         
                      
                              
                            
                            
                        }*/

                }
                else
                if(ProgresiveType1.length == 3){
                 
                   
                    if(Progresive == "Cría y Levante"&&(ProgresiveType1 == "Programación,Proyección,Ejecución"  || ProgresiveType1 == "Proyección,Programación,Ejecución"
                    || ProgresiveType1 == "Programación,Ejecución,Proyección"|| ProgresiveType1 == "Programación,Proyección,Ejecución"|| ProgresiveType1 == "Ejecución,Programación,Proyección"
                    || ProgresiveType1 == "Ejecución,Proyección,Programación" || ProgresiveType1 == "Proyección,Ejecución,Programación")){
                        const incubator = oEvent.getSource().getBindingContext("OSSHED").getObject()
                        console.log("Incubadora ", incubator)
                        this.reloadProgrammedByCode(oEvent)
                        //this.handlePopoverPressCLExtend(oEvent)

           
                    }else
                    if(Progresive == "Producción"&&(ProgresiveType1 == "Programación,Proyección,Ejecución"  || ProgresiveType1 == "Proyección,Programación,Ejecución"
                    || ProgresiveType1 == "Programación,Ejecución,Proyección"|| ProgresiveType1 == "Programación,Proyección,Ejecución"|| ProgresiveType1 == "Ejecución,Programación,Proyección"
                    || ProgresiveType1 == "Ejecución,Proyección,Programación" || ProgresiveType1 == "Proyección,Ejecución,Programación")){
               
                        //this.handlePopoverPressPExe(oEvent)
                
                    }

                 

                }





            }else
                if(Progresive == "Engorde"){

                }
         

        },
        itsExecution : function(oEvent){

    
            let Progresive = this.getView().byId("filterLotEntry2").mProperties.selectedKey;
            let ProgresiveType1 = this.getView().byId("filterLotEntry3").mProperties.selectedKeys;
             let ProgresiveType2 = this.getView().byId("filterLotEntry4").mProperties.selectedKeys;
           
          
               if(Progresive == "Cría y Levante" &&  ( ProgresiveType1 == "Proyección,Ejecución"  || ProgresiveType1 == "Ejecución,Proyección"))
            {
                console.log("Aquí voy!!!")
                this.reloadProgrammedByCode(oEvent)
            
            }
            else
                if(Progresive == "Cría y Levante"&&(ProgresiveType1 == "Programación,Proyección,Ejecución"  || ProgresiveType1 == "Proyección,Programación,Ejecución"
                || ProgresiveType1 == "Programación,Ejecución,Proyección"|| ProgresiveType1 == "Programación,Proyección,Ejecución"|| ProgresiveType1 == "Ejecución,Programación,Proyección"
                || ProgresiveType1 == "Ejecución,Proyección,Programación" || ProgresiveType1 == "Proyección,Ejecución,Programación")){
                    const incubator = oEvent.getSource().getBindingContext("OSSHED").getObject()
                    console.log("Incubadora ", incubator)
                    if(incubator.execution_date != null){
                         
                        //this.getModel("OSSHED").setProperty("/CLE", [incubator])
                        this.getModel("OSSHED").setProperty("/PLE", [incubator])
                        this._oPopoverPrEjec.openBy(oEvent.getSource())
                   
                    }else{
                        MessageToast.show("No existen ejecuciones asociadas")
                    }
                    
                }

                if(Progresive == "Producción" &&  ( ProgresiveType1 == "Proyección,Ejecución"  || ProgresiveType1 == "Ejecución,Proyección"))
            {
                console.log("Aquí voy!!!")
                this.reloadProgrammedByCode(oEvent)
            
            }
            else
                if(Progresive == "Producción"&&(ProgresiveType1 == "Programación,Proyección,Ejecución"  || ProgresiveType1 == "Proyección,Programación,Ejecución"
                || ProgresiveType1 == "Programación,Ejecución,Proyección"|| ProgresiveType1 == "Programación,Proyección,Ejecución"|| ProgresiveType1 == "Ejecución,Programación,Proyección"
                || ProgresiveType1 == "Ejecución,Proyección,Programación" || ProgresiveType1 == "Proyección,Ejecución,Programación")){
                    const incubator = oEvent.getSource().getBindingContext("OSSHED").getObject()
                    console.log("Incubadora ", incubator)
                    if(incubator.execution_date != null){
                        this.getModel("OSSHED").setProperty("/CLEP", [incubator])
                        this._oPopoverMixPExe.openBy(oEvent.getSource())
                   
                    }else{
                        MessageToast.show("No existen ejecuciones asociadas")
                    }
                    
                }

        },
        itsPoyected : function(oEvent){

        },
        itsExecuted : function(oEvent){

        }
        
        /* //Programados de Producción
         handlePopoverPressP: function(oEvent){    
            this.reloadProgrammedByCode(oEvent)
            let _oPopoverProyProg = this._getResponsivePopoverP();    
            _oPopoverProyProg.setModel(oEvent.getSource().getModel());
        
			
        },
		_getResponsivePopoverP: function () {
		  if (!this._oPopoverProyProg) {
			this._oPopoverProyProg = sap.ui.xmlfragment("MasterDetailSample.view.prog.produccion.osshed_ShowPopover7", this);
			this.getView().addDependent(_oPopoverProyProg);
		  }
		  return this._oPopoverProyProg;
        },
        //Ejecutados Programados
        handlePopoverPressPE: function(oEvent){
            this.reloadProgrammedByCode(oEvent)
			let _oPopoverProyEjec = this._getResponsivePopoverPE();
            _oPopoverProyEjec.setModel(oEvent.getSource().getModel());
            console.log("Parametros de la consulta", this.getModel("OSSHED").getProperty("/proyect7"))
            const incubator = oEvent.getSource().getBindingContext("OSSHED").getObject()
            this._oPopoverProyEjec.openBy(oEvent.getSource());
          
		},
		_getResponsivePopoverPE: function () {
		  if (!this._oPopoverProyEjec) {
			this._oPopoverEjec = sap.ui.xmlfragment("MasterDetailSample.view.prog.osshed_ShowPopover8", this);
			this.getView().addDependent(_oPopoverProyEjec);
		  }
		  return this._oPopoverProyEjec;
        },
        //Programacion y Ejecución de Cría y Levante
        
          handlePopoverPressCLExecutend: function(oEvent){
            this.reloadProgrammedByCode(oEvent)
            let _oPopoverMixPExe = this._getResponsivePopoverCLExecutend();
            _oPopoverMixPExe.setModel(oEvent.getSource().getModel());
            const incubator = oEvent.getSource().getBindingContext("OSSHED").getObject()
            this.getModel("OSSHED").setProperty("/proyect8", [incubator])
            this._oPopoverMixPExe.openBy(oEvent.getSource());
          
          },
          _getResponsivePopoverCLExecutend: function () {
            if (!this._oPopoverMixPExe) {
              this_oPopoverMixPExe = sap.ui.xmlfragment("MasterDetailSample.view.prog.osshed_ShowPopover10", this);
              this.getView().addDependent(_oPopoverMixPExe);
            }
            return this._oPopoverMixPExe;
        },
        handlePopoverPressPExe: function(oEvent){
            this.reloadProgrammedByCode(oEvent)
            let _oPopoverProgMixExe = this._getResponsivePopoverPExe();
            _oPopoverProgMixExe.setModel(oEvent.getSource().getModel());
            const incubator = oEvent.getSource().getBindingContext("OSSHED").getObject()
            this.getModel("OSSHED").setProperty("/proyect8", [incubator])
            this._oPopoverProgMixExe.openBy(oEvent.getSource());
        
          },
		_getResponsivePopoverPExe: function () {
		  if (!this._oPopoverProgMixExe) {
			this._oPopoverProgMixExe = sap.ui.xmlfragment("MasterDetailSample.view.prog.osshed_ShowPopover9", this);
			this.getView().addDependent(_oPopoverProgMixExe);
		  }
		  return this._oPopoverProgMixExe;
        },
        ,
        
    */
   ,
   handlePopoverPressIncub : function(oEvent){
    const incubator = oEvent.getSource().getBindingContext("OSSHED").getObject()
    this.getModel("OSSHED").setProperty("/IE", [incubator])
    this._oPopoverIncubEjec.openBy(oEvent.getSource())
},
        loadBreed : function(){
            let breed_id = this.getView().byId("filterBreed").mProperties.selectedKey
                let year = '2018'
                //let lot_id = 'H10'
                //let breedData = this.getModel("postureCurve")
         
                // posturecurve.setProperty("/egglots","Huevos Por Lote");
                // posturecurve.setProperty("/proportion","Proporción (%)");
                // posturecurve.setProperty("/table","SingleSelect");
               
                if(breed_id == 'Cobb'){
                    breed_id = 1
                }else
                    if(breed_id == "Ross"){
                        breed_id = 2

                    }else
                        {
                            breed_id == 3
                        }
            //let year = this.getView().byId("Selectyears").mProperties.selectedKey;
            let osshed = this.getModel("OSSHED")
            let ant = ""
            let nuevo = ""
            let data = new Array();
            fetch("/eggs_storage/findEggsStorageLots", {
                headers: {
                    "Content-Type": "application/json"
                },
                method: "POST",
                body: JSON.stringify({"breed_id" : breed_id})
            })
                .then(
                    function(response) {
                        if (response.status !== 200) {
                            console.log("Looks like there was a problem. Status Code: " +
                response.status);
                            return;
                        }

                        response.json().then(function(res) {
                            //util.setProperty("/busy/", false);
                            // console.log("yup");
                            let insert = new Array();
                            // console.log(res.data);
                            //insert.push("Todos");
             
                            res.data.forEach(element =>{
                                    
                                
                                  
                                if(element.lot.split("-")[0] !=  nuevo){
                                   nuevo = element.lot.split("-")[0] 
                                    insert.push(nuevo)
                                }
                                /*let ddate= new Date(element.week);
                                // let formatDate= `${(ddate.getDate() < 10 ? '0' : '') + ddate.getDate()}/${((ddate.getMonth() + 1) < 10 ? '0' : '') + (ddate.getMonth() + 1)}/${ddate.getFullYear()}`;
                                var jaj = new Array();
                                jaj = element.week.split("-");
                                jaj[2] = jaj[2][0] + jaj[2][1];
                                //let formatDate= `${(ddate.getDate() < 10 ? '0' : '') + ddate.getDate()}/${((ddate.getMonth() + 1) < 10 ? '0' : '') + (ddate.getMonth() + 1)}/${ddate.getFullYear()}`;
                                let formatDate = jaj[2] + "/" + jaj[1] + "/" + jaj[0];
                                   
    
                                    data.push(element);
                            

                                element.week= formatDate;
                                element.eggs=parseInt(element.eggs);
                                data.push(element)
                                //insert.push(element.lot);
                                */
                            });              
                            //  console.log(insert.length);
                            //   console.log(insert);
                            //posturecurve.setSizeLimit (insert.length+1);
                            //posturecurve.setProperty("/lot_init", insert);
                            // console.log(posturecurve.getProperty("/lot_init"))
              
                            // console.log(posturecurve.getProperty("/lot_init"))
                            // length
                            //cargando razas para el
                            osshed.setProperty("/raza",insert)
                            console.log("Data Raza --->",insert)
       
                        });
                    }
                )
                .catch(function(err) {
                    console.log("Fetch Error: ", err);
                });


        },
        loadLot: function(){
            
            

            let breed_id = this.getView().byId("filterBreed").mProperties.selectedKey;
            let year = '2019'
            let lot_id = this.getView().byId("inputLot").mProperties.selectedKey,
                //posturecurve = this.getView().getModel("posturecurve"),
                //week = this.getView().getModel("posturecurve").getProperty("/week"),
                //that = this;
                //let breedData = this.getModel("postureCurve")
                 osshed = this.getModel("mdbreed")
                // posturecurve.setProperty("/egglots","Huevos Por Lote");
                // posturecurve.setProperty("/proportion","Proporción (%)");
                // posturecurve.setProperty("/table","SingleSelect");
               
                if(breed_id == 'Cobb'){
                    breed_id = 1
                }else
                    if(breed_id = "Ross"){
                        breed_id = 2

                    }else
                        {
                            breed_id = 3
                        }
               // breed_id =
               console.log("BREED_ID ", breed_id)
                fetch("/eggs_storage/findAllEggsStorageView", {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify({
                        breed_id: breed_id,
                        lot: lot_id,
                        year : year
                       
                    })
       
        
                })
                    .then(
                        function(response) {
                            if (response.status !== 200) {
                                console.log("Looks like there was a problem. Status Code: " +
              response.status);
                                return;
                            }
                           
                       
                            response.json().then(function(res) {
                                
                                console.log("Data Lote --->", res.data)   
                                console.log("Consulta primaria")   
                                osshed.setProperty("/records", res.data)
                                 
                                //that.getView().getModel("posturecurve").setProperty("/weekReport",nMod);
               
                            });
                            
                        }
                    );
            
        
    

        },
        handleChangeBreed : function(){
             let osshed = this.getModel("OSSHED")
             let Progresive = this.getView().byId("filterLotEntry2").mProperties.selectedKey;
             osshed.setProperty("/progressiveCurvIn",[])
             osshed.setProperty("/progressive",[])
             this.loadBreed()
             if(Progresive == 'Curva de Postura'){
                this.byId("inputLot").setProperty("visible", true)
             }else{
                this.byId("inputLot").setProperty("visible", false)
             } 
                
            
        },
        loadListLot : function(){
            let breed_id = 1
            console.log("Raza ",breed_id)
            let lot_id = this.getView().byId("inputLot").mProperties.selectedKey

            let year = 'Todos'
            let that = this
            let osshed = this.getModel("OSSHED")
             breed_id = this.getView().byId("filterBreed").getSelectedKey();

              //this.getView().byId("Progresive").setProperty("busy",true)
            if(breed_id == 'Cobb'){
                breed_id = 1
            }else
                if(breed_id == 'Ross'){
                    breed_id = 3

                }else
                    if(breed_id == 'H'){
                        breed_id = 2
                    }
            console.log("Lotes--->", lot_id)
    sap.ui.core.BusyIndicator.show();

  //this.byId("Progresive").setBusy(true)

            fetch("/eggs_storage/findEggsStorageByYearBreedLot", {
                    method: "POST",

                    headers: {
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify({
                        breed_id: breed_id,
                        lot: lot_id,
                        year: year
                    })
           
            
                })
                    .then(
                        function(response) {
                            if (response.status !== 200) {
                                console.log("Looks like there was a problem. Status Code: " +
                  response.status);
                                return;
                            }
                            let nMod = new Array();
                            response.json().then(function(res) {
                                let i = 0;
                                let x;
                                let cad = ""
                             
                      
                      sap.ui.core.BusyIndicator.hide();
                      that.byId("Progresive").setBusy(false)
                                res.data.forEach(element => {
                                        if(element.eggs_executed==null){
                                        x=0;
                                    }else{
                                        x=element.eggs_executed;
                                    }
                                    var jaj = new Array();
                                    jaj = element.week.split("-");
                                    jaj[2] = jaj[2][0] + jaj[2][1];
                                    element.breed=breed_id;
                  
                                    let ddate= new Date(element.week2);
                                    //let formatDate= `${(ddate.getDate() < 10 ? '0' : '') + ddate.getDate()}/${((ddate.getMonth() + 1) < 10 ? '0' : '') + (ddate.getMonth() + 1)}/${ddate.getFullYear()}`;
                                    let formatDate = jaj[2] + "/" + jaj[1] + "/" + jaj[0];
                                    //posturecurve.setProperty("/formateDate", formatDate);
                                    osshed.setProperty("/formateDate", formatDate)
                                    element.formatDate = formatDate;

                                  
                                    let float = {
                                        week: element.formatDate,
                                        lot: element.lot,
                                        projected: element.eggs,
                                        eggs:parseFloat(element.week_eggs),
                                        executed: x,
                                        num_week: element.num_week,
                                        lot_eggs: parseInt(element.lot_eggs),
                                        percen: ((100*element.lot_eggs)/element.week_eggs).toFixed(2),
                                        breed:element.breed

                                    };
                                    
                                    nMod.push(float);
                                    osshed.setSizeLimit(nMod.length+1);
                                    osshed.setProperty("/progressiveCurvIn",nMod);
                                    i++;
                                
                                });                
                            });
                            //that.getView().byId("Progresive").setProperty("busy",false)
                        }
                    );
        },
        onPress: async function(){
            console.log("OnPress");
            //this.getView().byId("projectedTable").removeSelections();
            let that = this,
                mdprojected = this.getModel("OSSHED"),
                mdscenario =  this.getModel("mdscenario"),
                osshed= this.getModel("OSSHED"),
                partnership_id = this.getView().getModel("ospartnership").getProperty("/records/" + this.index + "/partnership_id"),
                util = this.getModel("util"),
                scenario_id = mdscenario.getProperty("/scenario_id"),
                scheduled_date = this.getView().byId("scheduled_dateBroiler").mProperties.value,
                breed_id = this.getView().byId("filterBreed").getSelectedKey();
                                
               
                console.log("Datos a enviar a la consulta ",scenario_id,scheduled_date,breed_id,partnership_id )
            const serverName = util.getProperty("/serviceUrl") + util.getProperty("/" + util.getProperty("/service") + "/findprojectedbroiler");
            console.log("/broiler/findprojectedbroiler  ", breed_id);
           //util.setProperty("/busy",true)
            if(breed_id == 'Cobb'){
                breed_id = 1
            }else
                if(breed_id == 'Ross'){
                    breed_id = 3

                }else
                    if(breed_id == 'H'){
                        breed_id = 2
                    }
                    console.log("Breed_id -->", breed_id)
           
            fetch("/broiler/findprojectedbroiler", {
                method: "POST",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify({
                    scenario_id: scenario_id,
                    _date: scheduled_date,
                    partnership_id: partnership_id,
                    breed_id: breed_id
                })
            })
                .then(
                    function(response) {
                        if (response.status !== 200) {
                            console.log("Looks like there was a problem. Status Code: " +
                    response.status);
                            return;
                        }
                        //util.setProperty("/busy",true)
                        response.json().then(function(res) {
                          that.getView().byId("exportBtnP").setProperty("visible",true)
                            console.log(res);
                            let records=res.data;
                            records.forEach(element => {
                                element.fProjected = that.getDia(element.projected_date);
                                element.projected_quantity = parseInt(element.projected_quantity)
                            });
                            console.log(records);
                            osshed.setProperty("/progressive", res.data);
                            osshed.setProperty("/product/records", res.product);
                            console.log("Lo que retorna-->",osshed);
                            mdprojected.refresh();
                            console.log("mdprojected en el boton buscar de proyectado" );
                            console.log(osshed);
                            console.log(osshed);
         

                        });
                    }
                )
                .catch(function(err) {
                    console.log("Fetch Error :-S", err);
                });
            
        },
         getDia: function(dia){
            let mdprogrammed = this.getView().getModel("mdprogrammed"),
                aDate = dia.split("/"),
                fecha = new Date(aDate[2], aDate[1] - 1, aDate[0]),
                dias= ["Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado"], 
                fullDate = dias[fecha.getUTCDay()] + " " +dia ; 
            console.log(fullDate);
            return fullDate;
            //mdprogrammed.setProperty("/selectedRecord/projected_date",fullDate); 

        },
               onIncubatorPlant: function () {

            
            let util = this.getModel("util"),
                partnership_id = this.getView().getModel("ospartnership").getProperty("/records/" + this.index + "/partnership_id"),
                that = this;
            let inreal = this.getView().getModel("incubatorRealNew");
            const serverName = util.getProperty("/serviceUrl") + util.getProperty("/" + util.getProperty("/service") + "/findIncPlantByPartnetship");

            return new Promise((resolve, reject) => {
                fetch("incubator_plant/findIncPlantByPartnetship", {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify({
                        partnership_id: partnership_id
                    })
                })
                    .then(
                        function (response) {
                            if (response.status !== 200) {
                                console.log("Looks like there was a problem. Status Code: " +
                    response.status);
                                return;
                            }

                            response.json().then(function (res) {
                                console.log("Buscando incubadora: ", res.data);
                                inreal.setProperty("/postureCurvIn", res.data);
                                resolve(res);
                            });
                        }
                    )
                    .catch(function (err) {
                        console.log("Fetch Error :-S", err);
                    });
            });

        }
        ,onIncubatorPlantExcecute: function () {

            
            let util = this.getModel("util"),
                partnership_id = this.getView().getModel("ospartnership").getProperty("/records/" + this.index + "/partnership_id"),
                that = this;
            let inreal = this.getView().getModel("incubatorRealNew");
            const serverName = util.getProperty("/serviceUrl") + util.getProperty("/" + util.getProperty("/service") + "/findIncPlantByPartnetship");

            return new Promise((resolve, reject) => {
                fetch("incubator_plant/findIncPlantByPartnetship", {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify({
                        partnership_id: partnership_id
                    })
                })
                    .then(
                        function (response) {
                            if (response.status !== 200) {
                                console.log("Looks like there was a problem. Status Code: " +
                    response.status);
                                return;
                            }

                            response.json().then(function (res) {
                                console.log("Buscando incubadora: ", res.data);
                                inreal.setProperty("/postureCurvIn", res.data);
                                resolve(res);
                            });
                        }
                    )
                    .catch(function (err) {
                        console.log("Fetch Error :-S", err);
                    });
            });

        },
        onBreedLoad: function () {
            const util = this.getModel("util"),
                serverName = util.getProperty("/serviceUrl") + util.getProperty("/" + util.getProperty("/service") + "/findBreed");
            let inreal = this.getView().getModel("mdinventory");
            let mdbreed = this.getModel("mdbreed"),
                that = this;
            mdbreed.setProperty("/records", []);

            let isRecords = new Promise((resolve, reject) => {
                fetch(serverName)
                    .then(
                        function (response) {
                            if (response.status !== 200) {

                                console.log("Looks like there was a problem. Status Code: " +
                      response.status);
                                return;
                            }
                            // Examine the text in the response
                            response.json().then(function (data) {
                                console.log(data.data);
                                // data.data.unshift({breed_id: 200, code: "Todas", name: "Todas"})
                                inreal.setProperty("/breed", data.data);
                                resolve(data);
                            });
                        }
                    )
                    .catch(function (err) {
                        console.log("Fetch Error :-S", err);
                    });
            });


            isRecords.then((res) => {
                if (res.data.length > 0) {
                    mdbreed.setProperty("/records", res.data);
                    mdbreed.setProperty("/value", mdbreed.getProperty("/records/0/breed_id"));
                    console.log(mdbreed);
                }
            });
        },
        itsProgEngorde : function(oEvent){
          let breed_id = []
          let mdprogrammed = this.getModel("OSSHED")
          let bandE = false
          let bandP = false
           const link = oEvent.getSource()
           let that = this
          breed_id.push(oEvent.getSource().getBindingContext("OSSHED").getObject().broiler_id)
        let Progresive = this.getView().byId("filterLotEntry2").mProperties.selectedKey;
           let ProgresiveType1 = this.getView().byId("filterLotEntry3").mProperties.selectedKeys;
           let ProgresiveType2 = this.getView().byId("filterLotEntry4").mProperties.selectedKeys;
        console.log("La consulta a los programados!!! ", breed_id)
            fetch("/broilerDetail/findbroilerdetail", {
                method: "POST",
                headers: {
                    "Content-type": "application/json"
                },
                body: JSON.stringify({
                    records: breed_id
                })
            })
                .then(response => {
                    if (response.status !== 200) {
                        console.log("Looks like there was a problem. Status Code: " +
            response.status);
                        return;
                    }
                    console.log("entre1");
                    response.json().then((res) => {
                        console.log(res);
                        let records = res.data;
                        records.forEach(element => {
                          console.log("Holaaaa", element.broiler_id)
                          if(element.broiler_id !== ""){
                            bandP= true
                            }else{
                              bandP= false
                            }

                            if(element.executedcenter_id && element.executedfarm_id && element.executedshed_id && element.execution_quantity && element.execution_date){
                                element.isexecuted = true;
                                bandE = true
                            }else{
                                element.isexecuted = false;
                                bandE = false
                            }
            
                        });
                        console.log(records);
                        
                        
                        console.log(records);

                        if (records.length > 0) {
                            console.log(records);
                            let residue_programmed = res.residue,
                                projected_quantity = mdprogrammed.getProperty("/selectedRecord/projected_quantity"),
                                total = projected_quantity - residue_programmed;
                                console.log(mdprogrammed);
                                if(ProgresiveType1 == 'Proyectado,Programación' || ProgresiveType1 == 'Programación,Proyectado'){
                                          if(bandP){
                                           mdprogrammed.setProperty("/BP", records);
                                           that._oPopoverBroilerP.openBy(link)  
                                        }else{
                                            MessageToast.show("No se encontraron programaciones asociadas")
                                        }
                                  }else
                                    if( ProgresiveType1 == 'Proyectado,Ejecución' || ProgresiveType1 == 'Proyectado,Programación'){
                                      if(bandE){
                                        mdprogrammed.setProperty("/BPE", records);
                                        that._oPopoverBroilerPE.openBy(link)
                                  
                                      }else{
                                          MessageToast.show("No se encontraron ejecuciones asociadas")
                                      }
                                }else
                                    if(ProgresiveType1 == 'Proyectado,Programación,Ejecución' || ProgresiveType1 == 'Proyectado,Ejecución,Programación'
                                      || ProgresiveType1 ==  'Programación,Proyectado,Ejecución' || ProgresiveType1 ==  'Programación,Ejecución,Proyectado'
                                      || ProgresiveType1 ==  'Ejecución,Programación,Proyectado' || ProgresiveType1 ==  'Ejecución,Proyectado,Programación'){
                                             mdprogrammed.setProperty("/BPEE", records);
                                             that._oPopoverBroilerBPE.openBy(link)
                                            console.log("Aquí entré!")

                                          
                                    }else
                                        {
                                          MessageToast.show("No se encontraron programaciones asociadas")
                                        }


                                
                        } else {
                            mdprogrammed.setProperty("/programmed_residue", mdprogrammed.getProperty("/selectedRecord/projected_quantity"));
                            mdprogrammed.setProperty("/executionSaveBtn", false);
                            MessageToast.show("No se encontraron programaciones asociadas")
                        }
                  
                    });
                })
                .catch(err => console.log);
        }

        ,
        itsExecuteEngorde : function(oEvent){
          let breed_id = []
          let mdprogrammed = this.getModel("OSSHED")
          let band = false
           const link = oEvent.getSource()
           let that = this
          breed_id.push(oEvent.getSource().getBindingContext("OSSHED").getObject().broiler_id)
        
        console.log("La consulta a los programados!!! ", breed_id)
            fetch("/broilerDetail/findbroilerdetail", {
                method: "POST",
                headers: {
                    "Content-type": "application/json"
                },
                body: JSON.stringify({
                    records: breed_id
                })
            })
                .then(response => {
                    if (response.status !== 200) {
                        console.log("Looks like there was a problem. Status Code: " +
            response.status);
                        return;
                    }
                    console.log("entre1");
                    response.json().then((res) => {
                        console.log(res);
                        let records = res.data;
                        records.forEach(element => {
                            if(element.executedcenter_id && element.executedfarm_id && element.executedshed_id && element.execution_quantity && element.execution_date){
                                element.isexecuted = true;
                                band = true
                            }else{
                                element.isexecuted = false;
                                band = false
                            }
            
                        });
                        console.log("Entreee2");
                        console.log(records);
                        mdprogrammed.setProperty("/BPE", records);
                        console.log(records);

                        if (band) {
                            console.log(records);
                            let residue_programmed = res.residue,
                                projected_quantity = mdprogrammed.getProperty("/selectedRecord/projected_quantity"),
                                total = projected_quantity - residue_programmed;
                            console.log(mdprogrammed);
                            that._oPopoverBroilerPE.openBy(link)
                        } else {
                            mdprogrammed.setProperty("/programmed_residue", mdprogrammed.getProperty("/selectedRecord/projected_quantity"));
                            mdprogrammed.setProperty("/executionSaveBtn", false);
                            MessageToast.show("No se encontraron ejecuciones asociadas")
                        }
                  
                    });


                })
                .catch(err => console.log);
             
              }
    ,
        handleLinkPressLot: function(oEvent){

            var mdprogrammed = this.getView().getModel("OSSHED");
            mdprogrammed.setProperty("/product/records", JSON.parse(JSON.stringify(oEvent.getSource().getBindingContext("OSSHED").getObject())));
            let selectObject = oEvent.getSource().getBindingContext("OSSHED").getObject();
            console.log("selectObject: ", selectObject);
            mdprogrammed.setProperty("/BPPL", selectObject.product);
            this._oPopoverBroilerPProduct.openBy(oEvent.getSource());
        },
        initExecutePopup: function(oEvent){

            var mdprogrammed = this.getView().getModel("OSSHED");
            let _oPopoverBroilerPE = this._getResponsivePopover();
            _oPopoverBroilerPE.setModel(oEvent.getSource().getModel());
            const selectObject = oEvent.getSource().getBindingContext("OSSHED").getObject();
            mdprogrammed.setProperty("/BPE", [selectObject]);
            console.log("Obtenidos",mdprogrammed.getProperty("/BPE"))
             this._oPopoverBroilerPE.openBy(oEvent.getSource());
        }
        ,
        _getResponsivePopoverLot: function () {
      if (!this._oPopoverBroilerPE) {
      this._oPopoverBroilerPE = sap.ui.xmlfragment("MasterDetailSample.view.prog.broiler.osshed_ShowBE", this);
      this.getView().addDependent(this._oPopoverBroilerPE);
      }
      return this._oPopoverBroilerPE;
    },

        showProgrammedLots: async function(oEvent) {
            const mdprogrammed = this.getView().getModel("OSSHED");
            const programming = oEvent.getSource().getBindingContext("OSSHED").getObject();
            console.log("Los programados---->", programming);

            const link = oEvent.getSource();
            console.log("programming.broiler_detail_id ", programming.broiler_detail_id)
            const response = await fetch("/broilerdetail/findIncubatorLotByBroilerLot", {
                headers: {
                    "Content-type": "application/json"
                },
                method: "POST",
                body: JSON.stringify({
                    broiler_detail_id: programming.broiler_detail_id
                })
            });

            if (!response.ok) {
                console.log("error");
                console.log(response);
            }
            else {
                const res = await response.json();
                mdprogrammed.setProperty("/BPL", res.data);
                console.log(mdprogrammed.getProperty("/BPL"));
               this._oPopoverBroilerPLot.openBy(link);
            }

            // programmed_eggs/findAllDateQuantityFarmProduct
        },
        onIncubatorPlant: function () {

            let util = this.getModel("util"),
                partnership_id = this.getModel("ospartnership").getProperty("/selectedRecords/partnership_id"),
                that = this;
            let inreal = this.getView().getModel("OSSHED");
            const serverName = util.getProperty("/serviceUrl") + util.getProperty("/" + util.getProperty("/service") + "/findIncPlantByPartnetship");
            console.log("serverName ")
            return new Promise((resolve, reject) => {
                fetch(serverName, {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify({
                        partnership_id: partnership_id
                    })
                })
                    .then(
                        function (response) {
                            if (response.status !== 200) {
                                console.log("Looks like there was a problem. Status Code: " +
                    response.status);
                                return;
                            }

                            response.json().then(function (res) {
                                console.log("Buscando incubadora: ", res.data);
                                inreal.setProperty("/plantaIncubadora", res.data);
                                resolve(res);
                            });
                        }
                    )
                    .catch(function (err) {
                        console.log("Fetch Error :-S", err);
                    });
            });

        },
        incubatorProjected : function(){
            let incubator_plant_id = this.getView().getModel("OSSHED").getProperty("/plantaIncubadora")[0].incubator_plant_id
                


                let scenario_id = this.getModel("mdscenario").getProperty("/scenario_id")
               let scheduled_date = this.getView().byId("scheduled_dateBroilerI").mProperties.value
               let scheduled_date2 = this.getView().byId("scheduled_dateBroilerS").mProperties.value
               let breed_id = this.getView().byId("filterBreed").getSelectedKey()
               let util = this.getModel("util"),
                partnership_id = this.getModel("ospartnership").getProperty("/selectedRecords/partnership_id"),
                plexus = 0
                let that = this
                let mdprojected = this.getModel("OSSHED")
                
           if(breed_id == 'Cobb'){
                    breed_id = 1
                }else
                    if(breed_id == "Ross"){
                        breed_id = 3

                    }else
                        if(breed_id == "H"){
                            breed_id == 2
                        }


console.log("Recibido",incubator_plant_id,scenario_id,scheduled_date,scheduled_date2, breed_id,partnership_id,plexus)
           const serverName = "/coldRoom/findProjectIncubator";
                    console.log(serverName);
                    console.log(scheduled_date);
                    console.log(scheduled_date2);
                    fetch(serverName, {
                        method: "POST",
                        headers: {
                            "Content-Type": "application/json"
                        },
                        body: JSON.stringify({
                            scenario_id: scenario_id,
                            init_date: scheduled_date,
                            end_date: scheduled_date2,
                            incubator_plant_id: incubator_plant_id,
                            partnership_id: partnership_id,
                            breed_id: breed_id,
                            plexus: plexus
                        })
                    })
                        .then(
                            function (response) {
                                if (response.status !== 200) {
                                    console.log("Looks like there was a problem. Status Code: " +
                            response.status);
                                    return;
                                }

                                response.json().then(function (res) {
                                    console.log("Buscando incubadora: ", res.data);
                                    mdprojected.setProperty("/progressiveCurvIn", res.data);
                                    mdprojected.setProperty("/raza", false);
                                    console.log(mdprojected);
                                    mdprojected.refresh();

                                });
                            }
                        )
                        .catch(function (err) {
                            console.log("Fetch Error :-S", err);
                        });
                

        
        },

         //Popup Granjas y Centros -Estructuras
         farmCenters : function(oEvent){
             
            let selectObject =oEvent.getSource().getBindingContext("OSSHED").getObject();
            let osshed = this.getModel("OSSHED"),
			util = this.getModel("util");
           
       
         
			fetch("/shed/findCenterByFarm", {
				
				headers: {
					'Content-Type': 'application/json'
				},
				method: 'POST',
				body: JSON.stringify({
					"farm_id": selectObject.farm_id
				})
			})
			.then(
				function(response) {
				  if (response.status !== 200) {
					console.log('Looks like there was a problem. Status Code: ' +
					  response.status);
				
					return;
				  }
		
				  response.json().then(function(res) {
							//util.setProperty("/busy/", false);
                        osshed.setProperty("/records4", res.data);
				  });
				}
			  )
			  .catch(function(err) {
				console.log('Fetch Error :-S', err);
			  });

              

         },
         handleLinkPress : function(oEvent){

            this.handlePopoverPress3(oEvent)
            


         },
         centerOsshed: function(oEvent){
		     let selectObject =oEvent.getSource().getBindingContext("OSSHED").getObject()
             let v =  this.getView().byId("filterLotEntry1").mProperties.selectedKeys;
           
             let osshed = this.getModel("OSSHED")
            console.log("Estoy en el front del data 3, para la tercera consulta",selectObject)
			let util = this.getModel("util");
          
            
			fetch("/shed/findCenterByShed", {
				
				headers: {
					'Content-Type': 'application/json'
				},
				method: 'POST',
				body: JSON.stringify({
					"center_id": selectObject.center_id
				})
			})
			.then(
				function(response) {
				  if (response.status !== 200) {
					console.log('Looks like there was a problem. Status Code: ' +
					  response.status);
				
					return;
				  }
		
				  response.json().then(function(res) {
					console.log("Estoy en y me retorna -> ",res);
							util.setProperty("/busy/", false);
                            osshed.setProperty("/records7", res.data);
                            
                  });
            
				}
			  )
			  .catch(function(err) {
				console.log('Fetch Error :-S', err);
			  });

		

 
         }
    
     
    });

});
