/*global history */
sap.ui.define([
  "sap/ui/core/mvc/Controller",
  "sap/ui/core/routing/History",
  "MasterDetailSample/model/formatter"

], function (Controller, History,formatter) {
  "use strict";
  var repDetailsDlg;
  return Controller.extend("MasterDetailSample.controller.BaseController", {
    formatter: formatter,
    
    /**
     * Convenience method for accessing the router in every controller of the application.
     * @public
     * @returns {sap.ui.core.routing.Router} the router for this component
     */
    setFragments : function () {

      //Estructuras
      this._oPopover = sap.ui.xmlfragment("MasterDetailSample.view.rep.osshed_ShowPopover", this);
      this._oPopover2 = sap.ui.xmlfragment("MasterDetailSample.view.rep.osshed_ShowPopover1", this);
      this._oPopover3 = sap.ui.xmlfragment("MasterDetailSample.view.rep.osshed_ShowPopover3", this);
      this._oPopover4 = sap.ui.xmlfragment("MasterDetailSample.view.rep.osshed_ShowPopover2", this);
      this.getView().addDependent(this._oPopover);
      this.getView().addDependent(this._oPopover2);
      this.getView().addDependent(this._oPopover3);
      this.getView().addDependent(this._oPopover4);
      
      //Progresiva
      //Cría y levante
      this._oPopoverProg = sap.ui.xmlfragment("MasterDetailSample.view.prog.criaYLevante.osshed_ShowCLP", this);
      this._oPopoverEjec = sap.ui.xmlfragment("MasterDetailSample.view.prog.criaYLevante.osshed_ShowCLE", this);
      this._oPopoverMixPE = sap.ui.xmlfragment("MasterDetailSample.view.prog.criaYLevante.osshed_ShowCLEPE", this);
      //
      //Producción
      this._oPopoverProyProg = sap.ui.xmlfragment("MasterDetailSample.view.prog.produccion.osshed_ShowPopover7", this);
      this._oPopoverProyEjec = sap.ui.xmlfragment("MasterDetailSample.view.prog.produccion.osshed_ShowPopover8", this);
      this._oPopoverPrEjec = sap.ui.xmlfragment("MasterDetailSample.view.prog.produccion.osshed_ShowPrP", this);
      //

     
      this._oPopoverMixPExe = sap.ui.xmlfragment("MasterDetailSample.view.prog.produccion.osshed_ShowPopover10", this);
      this._oPopoverProgMixExe = sap.ui.xmlfragment("MasterDetailSample.view.prog.produccion.osshed_ShowPopover11", this);
      this._oPopoverPrEjec = sap.ui.xmlfragment("MasterDetailSample.view.prog.produccion.osshed_ShowPrEjec", this);
      this._oPopoverPostureCurve = sap.ui.xmlfragment("MasterDetailSample.view.prog.curvePosture.osshed_ShowPostureCurve", this);

      this._oPopoverBroilerP= sap.ui.xmlfragment("MasterDetailSample.view.prog.broiler.osshed_ShowBP", this);
      this._oPopoverBroilerPProduct= sap.ui.xmlfragment("MasterDetailSample.view.prog.broiler.osshed_ShowBPProduct", this);
      this._oPopoverBroilerPLot= sap.ui.xmlfragment("MasterDetailSample.view.prog.broiler.osshed_ShowBPLot", this);
      this._oPopoverBroilerPE = sap.ui.xmlfragment("MasterDetailSample.view.prog.broiler.osshed_ShowBE", this);
      this._oPopoverBroilerBPE = sap.ui.xmlfragment("MasterDetailSample.view.prog.broiler.osshed_ShowBPE", this);
      this._oPopoverIncubEjec = sap.ui.xmlfragment("MasterDetailSample.view.prog.Incubadora.osshed_ShowIE", this);
      this.getView().addDependent(this._oPopoverProg);
      this.getView().addDependent(this._oPopoverEjec);
      this.getView().addDependent(this._oPopoverProyProg);
      this.getView().addDependent(this._oPopoverProyEjec);
      this.getView().addDependent(this._oPopoverMixPE);
      this.getView().addDependent(this._oPopoverMixPExe);
      this.getView().addDependent(this._oPopoverProgMixExe);
      this.getView().addDependent(this._oPopoverPrEjec);
      this.getView().addDependent(this._oPopoverPostureCurve);
      this.getView().addDependent(this._oPopoverBroilerP)
      this.getView().addDependent(this._oPopoverBroilerPProduct)
      this.getView().addDependent(this._oPopoverBroilerPLot)
      this.getView().addDependent(this._oPopoverBroilerPE)
      this.getView().addDependent(this._oPopoverBroilerBPE)
      this.getView().addDependent(this._oPopoverIncubEjec)
      console.log("Fragments")
    
  },
    getRouter: function () {
      return this.getOwnerComponent().getRouter();
    },
    
    getResourceBundle: function () {
      return this.getOwnerComponent().getModel("i18n").getResourceBundle();
    }
  ,
    /**
     * Convenience method for getting the view model by name in every controller of the application.
     * @public
     * @param {string} sName the model name
     * @returns {sap.ui.model.Model} the model instance
     */
    getModel: function (sName) {
      return this.getView().getModel(sName);
    },
    onSelection : function(oEvent){
      let ospartnership= this.getModel("ospartnership");
      let obj= oEvent.getSource().getBindingContext("ospartnership").getObject();//obtiene el objeto desde el modelo
      let index = oEvent.getSource().getBindingContext("ospartnership").getPath().split("/")[2];
      obj.index= index;
      ospartnership.setProperty("/selectedRecords/", obj);
      ospartnership.setProperty("/name", obj.name);
      ospartnership.setProperty("/address", obj.address);
      // console.log("obj", obj)
      ospartnership.setProperty("/selectedRecords/", obj);
      this.hola()
      this.getRouter().navTo("detail", {
          partnership_id: obj.partnership_id,
          id: obj.index
      }, false /*create history*/ );
    },
    hola : function(){

      this.createId.byId("Galpon").setProperty("visible", false)
    },
    /**
     * Convenience method for setting the view model in every controller of the application.
     * @public
     * @param {sap.ui.model.Model} oModel the model instance
     * @param {string} sName the model name
     * @returns {sap.ui.mvc.View} the view instance
     */
    setModel: function (oModel, sName) {
      return this.getView().setModel(oModel, sName);
    },
    sendRequest : function (url, method, data, successFunc, srvErrorFunc, connErrorFunc) {
        var util = this.getModel("util");
        //console.log("datos", data);
        var $settings = {
            url: url,
            method: method,
            data: JSON.stringify(data),
            contentType: "application/json",
            error: err => {
                //console.log("error", err);
                util.setProperty("/connectionError/status", err.status);
                util.setProperty("/connectionError/message", err.statusText);
                // console.log("entré a error porque:", err, method, url, data)
                this.onConnectionError();
                if(connErrorFunc) connErrorFunc(err);

            },
            success: res => {
                //console.log("respuesta", res);
                if(res.statusCode !== 200) {
                    util.setProperty("/serviceError/status", res.statusCode);
                    util.setProperty("/serviceError/message", res.msg);
                    this.onServiceError();
                    if(srvErrorFunc) srvErrorFunc(res);
                } else {
                    successFunc(res);
                }
            }
        };

        $.ajax($settings);
    }
    
     
    /**
     * Convenience method for getting the resource bundle.
     * @public
     * @returns {sap.ui.model.resource.ResourceModel} the resourceModel of the component
     */
    
  });

});