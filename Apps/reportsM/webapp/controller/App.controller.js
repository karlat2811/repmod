sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"MasterDetailSample/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/m/Dialog",
	'sap/m/Button',
	'sap/m/Text'
], function(App,BaseController,JSONModel,Dialog,Button,Text) {
	"use strict";

	return BaseController.extend("MasterDetailSample.controller.App", {

		onInit: function() {
			this.getView().addStyleClass(this.getOwnerComponent().getContentDensityClass());

			
			console.log("Hola babe")
			this.setModel(new JSONModel({
				"name":"",
				"address": "",
				"selectedRecords": {},
				"records": [],
				"title": this.getResourceBundle().getText("master.TitleCount", [0]),
				"noDataText": this.getResourceBundle().getText("master.masterListNoDataText"),
				"scenarioActivate": false
			}), "ospartnership");

			this.setModel(new JSONModel({
				"scenario_id": 0,
				"name": ""
			}), "mdscenario");

			this.setModel(new JSONModel({
				"records": [],
				"selectedKey": ""
			}), "mdincubatorplant");
			this.setModel(new JSONModel({
				"records": [],
				"selectedKey": ""
			}), "mdbreed");
			
			this.setModel(new JSONModel({
				"records": [],
				"selectRecords": {}
			}), "mdprojected");
			this.setModel(new JSONModel({
				"week": "",
				"date": "",
				"lot": "",
				"eggs_totaly": 0,
				"eggs_byLot": 0,
				"proportion": 0,
				"selectRecords": {}
			}), "postureCurve");
			

		}
		
	});

});