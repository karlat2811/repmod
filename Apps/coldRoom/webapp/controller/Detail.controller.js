sap.ui.define([
    "coldRoom/controller/BaseController",
    "jquery.sap.global",
    "sap/ui/model/Filter",
    "sap/ui/core/Fragment",
    "sap/ui/model/json/JSONModel",
    "sap/m/MessageToast",
    "sap/m/Dialog",
    "sap/m/Button",
    "sap/m/Text"
], function(BaseController, jQuery, Filter, Fragment, JSONModel, MessageToast, Dialog, Button, Text) {
    "use strict";
    const incubatorStage = 2; /*Clase para levante y Cria*/
    const stateSucess= "Success";
    const stateError= "Error";
    const limitInt= 2147483640;
    const limitRows= 2000;
    const limitDiferencial= 359;
    return BaseController.extend("coldRoom.controller.Detail", {

        onInit: function() {
            this.setFragments();
            this.getRouter().getRoute("detail").attachPatternMatched(this._onRouteMatched, this);
        },

        _onRouteMatched: function(oEvent) {
            let oView= this.getView();
            var oArguments = oEvent.getParameter("arguments");
            let ospartnership = this.getModel("ospartnership");
            let mdinventory = this.getModel("mdinventory");
            this.enabledTab({entry: false, egress: false});
            oView.byId("tabBar").setSelectedKey("projectTab");
            this.hideButtons(false, false, false, false);
            oView.byId("IngresoTable").addEventDelegate({
                onAfterRendering: oEvent=>{
                    console.log("victor te amo!");
                }
            });
            // mdinventory.setSizeLimit(limitRows)
            /*     this.reloadPartnership()
            .then(data => {
                if(data.length>0){
                    let obj= ospartnership.getProperty("/selectedRecords/");

                    if(obj){
                        this.onRead(obj.partnership_id);
                    }
                    else{
                        MessageToast.show("no existen empresas cargadas en el sistema", {
                            duration: 3000,
                            width: "20%"
                        });
                        console.log("err: ", data)
                    }
                }
                else{
                    MessageToast.show("ha ocurrido un error al cargar el inventario", {
                        duration: 3000,
                        width: "35%"
                    });
                    console.log("err: ", data)
                }
            }); */

            // debugger;
            if(ospartnership.getProperty("/records").length>0){
                let partnership_id = ospartnership.getProperty("/selectedRecords/partnership_id");
                this.onRead(partnership_id);
            }
            else{
                this.reloadPartnership()
                    .then(data => {
                        if(data.length>0){
                            let obj= ospartnership.getProperty("/selectedRecords/");
                            if(obj){
                                this.onRead(obj.partnership_id);
                            }
                            else{
                                MessageToast.show("no existen empresas cargadas en el sistema", {
                                    duration: 3000,
                                    width: "20%"
                                });
                                console.log("err: ", data);
                            }
                        }
                        else{
                            MessageToast.show("ha ocurrido un error al cargar el inventario", {
                                duration: 3000,
                                width: "35%"
                            });
                            console.log("err: ", data);
                        }
                    });
            } 

        },

        onRead: async function(partnership_id) {
        // debugger;
            console.log("Debbug",partnership_id)
            let ospartnership = this.getModel("ospartnership");
            let mdscenario = this.getModel("mdscenario");
        
            let activeS = await this.activeScenario();
            await this.loadInventory(partnership_id);

            mdscenario.setProperty("/scenario_id", activeS.scenario_id);
            mdscenario.setProperty("/name", activeS.name);


        },


        reloadPartnership: function(){
            let util = this.getModel("util");
            let that = this;
            let ospartnership = this.getModel("ospartnership");

            
            util.setProperty("/busy/", true);
            ospartnership.setProperty("/records", []);

            let url = util.getProperty("/serviceUrl") +util.getProperty("/" + util.getProperty("/service") + "/getPartnership");
            let method = "GET";
            let data = {};
            return new Promise((resolve, reject) => {
                function getPartnership(res) {
                    util.setProperty("/busy/", false);
                    ospartnership.setProperty("/records/", res.data);
                    if(res.data.length>0){
                        let obj= res.data[0];
                        obj.index= 0;
                        ospartnership.setProperty("/selectedRecords/", obj);
                        ospartnership.setProperty("/name", obj.name);
                        ospartnership.setProperty("/address", obj.address);
                    }
                    resolve(res.data);
                }

                function error(err) {
                    console.log(err);
                    ospartnership.setProperty("/selectedRecords/", []);
                    util.setProperty("/error/status", err.status);
                    util.setProperty("/error/statusText", err.statusText);
                    reject(err);
                }

                /*Envía la solicitud*/
                this.sendRequest.call(this, url, method, data, getPartnership, error, error);
            });
        },

        changeDate: function (oEvent) {
            var oDP = oEvent.getSource();
            var sValue = oEvent.getParameter("value");
            var bValid = oEvent.getParameter("valid");
            this._iEvent++;

            if (bValid) {
                oDP.setValueState(sap.ui.core.ValueState.None);
            } else {
                oDP.setValueState(sap.ui.core.ValueState.Error);
            }
        },

        formatDate: function(date){
            if(date!== null){
                let c= "/";//caracter separatos
                console.log(date);
                date= new Date(date.toString());
                // if((typeof date.getMonth === 'function'))
                if(!isNaN( date.getFullYear() )){
                    date= ( ((date.getDate()<10)? "0"+date.getDate(): date.getDate() )+c+
                        ((date.getMonth()+1<10)? "0"+(date.getMonth()+1): date.getMonth()+1 )+c+
                        date.getFullYear() );
                }
                else
                    date=null;
            }
            return (date);
        },

        constructDate: function(date){
            if(date!==null){
                date= new Date(date.toString());
                if(!(typeof date.getMonth === "function"))
                    date= null;
            }
            return(date);
        },

        constructDateWithFormat: function(date){
        // debugger;
            if(date!==null){
                date= new Date(date.toString());
                if(!(typeof date.getMonth === "function"))
                    date= null;
            }
            return(date);
        },

        validateIntInput: function (o) {
            let input= o.getSource();
            let length = 10;
            let value = input.getValue();
            let regex = new RegExp(`/^[0-9]{1,${length}}$/`);

            if (regex.test(value)) {
                return true;
            }
            else {
                let aux = value.split("").filter(char => {
                    if (/^[0-9]$/.test(char)) {
                        if (char !== ".") {
                            return true;
                        }
                    }
                }).join("");
            
                value = aux.substring(0, length);
                input.setValue(value);
                return false;
            }
        },
    

        onValidProgrammedQuantity: function(o){
            let input= o.getSource();
            let length = 10;
            let value = input.getValue();
            console.log("valor: " + value);
            let regex = new RegExp(`/^[0-9]{1,${length}}$/`);

            if (regex.test(value)) 
            {
                return true;
            }
            else 
            {
                let aux = value
                    .split("")
                    .filter(char => {
                        if (/^[0-9]$/.test(char)) 
                        {
                            if (char !== ".") {
                                return true;
                            }
                        }
                    })
                    .join("");
                value = aux.substring(0, length);
                input.setValue(value);

                console.log("el valor es: " + value);
    

                let mdprogrammed = this.getModel("mdprogrammed");
                console.log("el modelo mdprogrammed");
                console.log(mdprogrammed);

                let mdincubator = this.getModel("mdincubator"),
                    residue = mdincubator.getProperty("/residue");
                console.log("el modelo incubator");
                console.log(mdincubator);
                console.log("el residuo incubator");
                console.log(residue);

                let selectedMachine = sap.ui.getCore().byId("selectIncubator").getSelectedKey();
                // mdshed.setProperty("/selectedKey", selectedShed);
                console.log("el codigo de la maquina seleccionado");
                console.log(selectedMachine);

                let lot_records = mdprogrammed.getProperty("/lot_records");



                let asig = 0;
                console.log("el records de los lotes");
                console.log(lot_records);

                console.log("el for");
                lot_records.forEach(item=>{
                    // console.log("el item")
                    // console.log(item)
                    item.assigned.forEach(item2=>{
                        console.log("el item2");
                        console.log(item2);
                        if (item2.selected_incubator == selectedMachine) 
                        {
                            console.log("entro en el if");
                            console.log(item2.quantity_eggs);
                            asig += item2.quantity_eggs;
                        }
                    });
                });
                console.log("salio del for");
                console.log(asig);




                let array1 = mdincubator.getProperty("/list2");
        
                console.log("el array");
                console.log(array1);

                var found = array1.find(function(element) {
                    return element.incubator_id == selectedMachine;
                });

                console.log("el found");
                console.log(found.available);

                var available = 800;
                // if(parseInt(value) <= programmed_residue && parseInt(value) <= parseInt(found.capmax) ){
                if(parseInt(value) <= residue && parseInt(value) <= found.available){
                    mdprogrammed.setProperty("/name/state", "None");
                    mdprogrammed.setProperty("/name/stateText", "");
                    mdprogrammed.setProperty("/confirmBtn", true);

                }else{
                    console.log("el residuo en el else");
                    console.log(residue);
                    if (parseInt(value) > residue) {
                        console.log("el residuo en el if");
                        console.log(residue);
                        mdprogrammed.setProperty("/name/state", "Error");
                        mdprogrammed.setProperty("/name/stateText", "La cantidad programada supera al saldo");
                        mdprogrammed.setProperty("/confirmBtn", false);
                    }

                    if (parseInt(value) > found.available - asig) {
                        mdprogrammed.setProperty("/name/state", "Error");
                        mdprogrammed.setProperty("/name/stateText", "La cantidad programada supera la disponibilidad de la máquina");
                        mdprogrammed.setProperty("/confirmBtn", false);
                    }
            

                }
                return false;
            }
        },
        onValidAdjustedQuantity: function(o){
            let input= o.getSource();
            let length = 10;
            let value = input.getValue();
            console.log("valor: " + value);
            let regex = new RegExp(`/^[0-9]{1,${length}}$/`);

            if (regex.test(value)) 
            {
                return true;
            }
            else 
            {
                let aux = value
                    .split("")
                    .filter(char => {
                        if (/^[0-9]$/.test(char)) 
                        {
                            if (char !== ".") {
                                return true;
                            }
                        }
                    })
                    .join("");
                value = aux.substring(0, length);
                input.setValue(value);

                console.log("el valor es: " + value);
                let mdinventory= this.getModel("mdinventory"),
                    path = mdinventory.getProperty("/selectedRecordPath"),
                    obj = mdinventory.getProperty(path),
                    quantity = obj.eggs_executed - obj.quantity + limitDiferencial;
                console.log(path);
                console.log(mdinventory);
                console.log("el limite es: ",quantity);
                // if(parseInt(value) <= programmed_residue && parseInt(value) <= parseInt(found.capmax) ){
                if(parseInt(value) <= quantity){
                    mdinventory.setProperty("/name/state", "None");
                    mdinventory.setProperty("/name/stateText", "");
                    mdinventory.setProperty("/confirmBtn", true);

                }else{
                    console.log("el limite en el else");
                    console.log(quantity);
                    if (parseInt(value) > quantity) {
                        console.log("el limite en el if");
                        console.log(quantity);
                        mdinventory.setProperty("/name/state", "Error");
                        mdinventory.setProperty("/name/stateText", "La cantidad ingresada supera el límite");
                        mdinventory.setProperty("/confirmBtn", false);
                    }
            

                }
                return false;
            }
        },
        onValidEgressQuantity: function(o){
            let input= o.getSource();
            let length = 10;
            let value = input.getValue();
            console.log("valor: " + value);
            let regex = new RegExp(`/^[0-9]{1,${length}}$/`);

            if (regex.test(value)) 
            {
                return true;
            }
            else 
            {
                let aux = value
                    .split("")
                    .filter(char => {
                        if (/^[0-9]$/.test(char)) 
                        {
                            if (char !== ".") {
                                return true;
                            }
                        }
                    })
                    .join("");
                value = aux.substring(0, length);
                input.setValue(value);

                console.log("el valor es: " + value);
                let mdinventory= this.getModel("mdinventory"),
                    path = mdinventory.getProperty("/selectedRecordPath"),
                    lmt = mdinventory.getProperty("/egress/egressBalance");
                console.log(path);
                console.log(mdinventory);
                console.log("el limite es: ",lmt);
                // if(parseInt(value) <= programmed_residue && parseInt(value) <= parseInt(found.capmax) ){
                if(parseInt(value) <= lmt){
                    mdinventory.setProperty("/name/state", "None");
                    mdinventory.setProperty("/name/stateText", "");
                    mdinventory.setProperty("/confirmBtn", true);

                }else{
                    console.log("el limite en el else");
                    if (parseInt(value) > lmt) {
                        console.log("el limite en el if");
                        mdinventory.setProperty("/name/state", "Error");
                        mdinventory.setProperty("/name/stateText", "La cantidad ingresada supera el límite");
                        mdinventory.setProperty("/confirmBtn", false);
                    }
            

                }
                return false;
            }
        },


        activeScenario: function () {
            let that= this;
            let util = this.getModel("util");
            const serverName = util.getProperty("/serviceUrl") + util.getProperty("/" + util.getProperty("/service") + "/activeScenario");
            return new Promise((resolve, reject) => {
                fetch(serverName)
                    .then(
                        function (response) {
                            if (response.status !== 200) {
                                console.log("Looks like there was a problem. Status Code: " +response.status);
                                that.openDialog(stateError, "Ha ocurrido un error.");
                                return;
                            }

                            response.json().then(function (res) {
                                resolve(res);
                            });
                        }
                    )
                    .catch(function (err) {
                        console.log("Fetch Error :-S", err);
                        that.openDialog(stateError, "Ha ocurrido un error.");
                    });

            });
        },
    

        /**
     * Carga el inventario por incubadora
     */
        loadInventory: function (partnership_id) {
            console.log("partnership_id", partnership_id);
            let mdprojected = this.getModel("mdprojected");
            let util = this.getModel("util");
            // const serverName = util.getProperty("/serviceUrl") + util.getProperty("/" + util.getProperty("/service") + "/findInventoryByPartnership");
            const serverName = util.getProperty("/serviceUrl") + util.getProperty("/" + util.getProperty("/service") + "/findProjectEggs");
            console.log("si era esto", serverName);
            //esto se cambia despues de aclararlo
            let aclimatized= 3;
            let suitable1= 3;
            let suitable2= 100;
            let expired= 100;

            util.setProperty("/busy/", true);
            fetch(serverName, {
                method: "POST",
                headers: {"Content-type": "application/json; charset=UTF-8"},
                body: JSON.stringify({
                    partnership_id: partnership_id,
                    aclimatized: aclimatized,
                    suitable1: suitable1,
                    suitable2: suitable2,
                    rangeExpired: expired
                })
            }).then(
                function (response) {
                    util.setProperty("/busy/", false);
                    if (response.status !== 200) {
                        console.log("Looks like there was a problem. Status Code: ",response.status);
                        that.openDialog(stateError, "Ha ocurrido un error.");
                        return;
                    }
                    else{
                        response.json().then(function (res) {
                            if (res.data.length > 0) {
                                mdprojected.setProperty("/records", res.data);
                            } else {
                                mdprojected.setProperty("/records", []);
                            }
                        });
                    }
                }
            );
        },

        hideAllButtons: function(){
            let mdinventory= this.getModel("mdinventory");
            mdinventory.setProperty("/entry/viewEntrySave", false),
            mdinventory.setProperty("/entry/entryNew", false);
            mdinventory.setProperty("/entry/egressNew", false);

        },

        hideButtons: function (programmed, execution, real, reports) {
        // let mdprogrammed = this.getModel("mdprogrammed");
        // let mdreports = this.getModel("mdreports");
        // mdprogrammed.setProperty("/programmedNewBtn", programmed);
        // mdprogrammed.setProperty("/executionNewReal", real);
        // console.log("Entre y no cambio");
        // mdreports.setProperty("/reportsBtn", reports);
        },

        onTabSelection: function (ev) {
            var mdinventory = this.getModel("mdinventory");
            var ospartnership = this.getModel("ospartnership");
            var selectedKey = ev.getSource().getSelectedKey();
            let obj= ospartnership.getProperty("/selectedRecords/");
            console.log("Aja se seteaa!!!")
            this.hideAllButtons();
            if (selectedKey === "projectTab") {
                this.hideAllButtons();
                this.loadInventory(obj.partnership_id);
                this.getView().byId("projectTable").removeSelections();
                this.getView().byId("IngresoTable").removeSelections();
                this.enabledTab({entry: false, egress: false});
            }
            if (selectedKey === "tabIngreso") {
            // this.hideAllButtons();
                this.viewEntryBtns();
                // this.getModel("mdinventory").setProperty("/entry/entryNew", true);
                this.loadLots();
                this.getView().byId("IngresoTable").removeSelections();
                this.enabledTab({entry: true, egress: true});
            }
            if (selectedKey === "tabEgreso") {
            // this.hideAllButtons();
                this.loadLots();
                this.resetEgressFilters();
                mdinventory.setProperty("/entry/selectRecord", {});
                mdinventory.setProperty("/entry/entryNew", false);
                mdinventory.setProperty("/entry/egressNew", true);
                mdinventory.setProperty("/entry/viewEntrySave", false);
                mdinventory.setProperty("/egress/viewEgressDate", false);

                this.enabledTab({entry: true, egress: true});
            }else{
                mdinventory.setProperty("/egress/egressNew", false);
            }
        
        },

        enabledTab: function(obj) {
            this.getView().byId("tabIngreso").setEnabled(obj.entry);
            this.getView().byId("tabEgreso").setEnabled(obj.egress);
        },

    
        onSelectProject: function (oEvent) {
            let oView= this.getView();
            let mdinventory= this.getModel("mdinventory");
            let mdprojected= this.getModel("mdprojected");
            let objProjected= oEvent.getSource().getSelectedItem().getBindingContext("mdprojected").getObject();
        
            mdprojected.setProperty("/selectedRecords", objProjected);
            //limpiar entradas
            this.loadLots();
            mdinventory.setProperty("/entry/records", []);
            // mdinventory.setProperty("/entry/entryNew", true);
            oView.byId("entryDesde").setValue("");
            oView.byId("entryHasta").setValue("");
        
            // this.showEntryBtns(true)
        
            this.enabledTab({entry: true, egress: true});
            oView.byId("tabBar").setSelectedKey("tabIngreso");
        },

        onSelectEntry: function(oEvent){
            let oView= this.getView();
            let mdinventory= this.getModel("mdinventory");
            let objEntry= oEvent.getSource().getSelectedItem().getBindingContext("mdinventory").getObject();
        
            if(objEntry.available){
                MessageToast.show("No se puede consultar un ingreso no ejecutado", {
                    duration: 3000,
                    width: "20%"
                });
                this.getView().byId("IngresoTable").removeSelections();
            }
            else{
            //limpiar entradas
                this.resetEgressFilters();
                //cargar los primeros 10
                mdinventory.setProperty("/entry/selectRecord", objEntry);
                mdinventory.setProperty("/entry/viewEntrySave", false);
                mdinventory.setProperty("/egress/viewEgressDate", true);
                mdinventory.setProperty("/egress/egressNew", true);
                this.getMovementsByEntry(objEntry);
                this.enabledTab({entry: true, egress: true});
                oView.byId("tabBar").setSelectedKey("tabEgreso");
            }
        },
   
        searchEntry: async function (oEvent) {
            // let sId = oEvent.getSource().sId.substring(36, oEvent.getSource().sId.length);
            let that = this;
            let mdinventory= this.getModel("mdinventory");
            let mdprojected= this.getModel("mdprojected");
            let ospartnership= this.getModel("ospartnership");
            /* let selectPath = this.getView().byId("projectTable").getSelectedItem().getBindingContext('mdprojected').getPath();
        let selectObj= mdprojected.getProperty(selectPath); */
            let objProjected= mdprojected.getProperty("/selectedRecords");
            let init_date = this.getView().byId("entryDesde").mProperties.value;
            let end_date = this.getView().byId("entryHasta").mProperties.value;
            let slot = this.getView().byId("filterLotEntry").getSelectedKey();
            // debugger;

            //resetSearch
            mdinventory.setProperty("/entry/records", []);
            mdinventory.setProperty("/entry/objectSearch", {});
        
            if (init_date === null || init_date== "" || end_date === null || end_date== "" ){
            // console.log("fechas vacias")
                MessageToast.show("No se pueden consultar fechas vacías", {
                    duration: 3000,
                    width: "20%"
                });
            }
            else{
            // debugger;
            // console.log("fechas correctas")
                let descip = this.getView().byId("filterOriginEntry").getSelectedKey("");
                if(descip==="Compra"){
                    console.log(mdprojected);
                    console.log(objProjected.incubator_plant_id);
                    slot= (slot.toLowerCase()=="todos")? null: slot;
                    let objSearch={//objeto de busquedad
                        partnership_id: ospartnership.getProperty("/selectedRecords/partnership_id"),
                        incubator_plant_id: objProjected.incubator_plant_id,
                        init_date: init_date,
                        end_date: end_date,
                        slot: slot
                    };
                    fetch("/coldRoom/findEntryEggs2", {
                        method: "POST",
                        headers: {"Content-type": "application/json; charset=UTF-8"},
                        body: JSON.stringify({
                            partnership_id: ospartnership.getProperty("/selectedRecords/partnership_id"),
                            incubator_plant_id: objProjected.incubator_plant_id,
                            init_date: init_date,
                            end_date: end_date,
                            slot: slot
                        })
                    }).then(
                        function (response) {
                        // util.setProperty("/busy/", false);
                            if (response.status !== 200) {
                                console.log("Looks like there was a problem. Status Code: ",response.status);
                                that.openDialog(stateError, "Ha ocurrido un error.");
                                return;
                            }
                            else{
                                response.json().then(function (res) {
                                    console.log("data entry:: ", res.data);
                                    if (res.data.length > 0) {
                                    
                                        mdinventory.setProperty("/entry/records", res.data);
                                        mdinventory.setProperty("/entry/objectSearch", objSearch);
                                        mdinventory.setProperty("/compra",true);
                                        mdinventory.setProperty("/curva",false);
                                        let records= mdinventory.getProperty("/entry/records");
                                        records.forEach(item=>{
                                            item.stateQuantity= "None";
                                            item.stateTextQuantity= "";
                                            item.stateFecha_movements= "None";
                                            item.stateTextFecha_movements= "";
                                        
                                            item.fecha_movements= that.formatDate(item.fecha_movements);
    
                                            item.oldQuantity= item.quantity;
                                            item.oldFecha_movements= item.fecha_movements;
                                        });
                                        console.log("mdinventory", mdinventory);
    
                                        // if(res.data.length>mdinventory.iSizeLimit)
                                        //     mdinventory.setSizeLimit(res.data.length)
    
                                        mdinventory.refresh();
                                        that.viewEntryBtns();
                                    } else {
                                        mdinventory.setProperty("/entry/records", []);
                                        mdinventory.setProperty("/entry/objectSearch", {});
                                        that.viewEntryBtns();
                                    }
                                });
                            }
                        }
                    );
                }else{
                    init_date= new Date(init_date);
                    end_date= new Date(end_date);
                    slot= (slot.toLowerCase()=="todos")? null: slot;

                    let util = this.getModel("util");
                    let serverName = util.getProperty("/serviceUrl") + util.getProperty("/" + util.getProperty("/service") + "/findEntryEggs");
                    console.log(serverName);
                    // console.log("busy", util.getProperty("/busy"))
                    util.setProperty("/busy", true);
                    // console.log("busy", util.getProperty("/busy"))

                    let objSearch={//objeto de busquedad
                        partnership_id: ospartnership.getProperty("/selectedRecords/partnership_id"),
                        incubator_plant_id: objProjected.incubator_plant_id,
                        init_date: init_date,
                        end_date: end_date,
                        slot: slot
                    };
            
                    fetch(serverName, {
                        method: "POST",
                        headers: {"Content-type": "application/json; charset=UTF-8"},
                        body: JSON.stringify({
                            partnership_id: ospartnership.getProperty("/selectedRecords/partnership_id"),
                            incubator_plant_id: objProjected.incubator_plant_id,
                            init_date: init_date,
                            end_date: end_date,
                            slot: slot
                        })
                    }).then(
                        function (response) {
                            util.setProperty("/busy/", false);
                            if (response.status !== 200) {
                                console.log("Looks like there was a problem. Status Code: ",response.status);
                                that.openDialog(stateError, "Ha ocurrido un error.");
                                return;
                            }
                            else{
                                response.json().then(function (res) {
                                    console.log("data entry:: ", res.data);
                                    if (res.data.length > 0) {
                                        // res.data.forEach(element => {
                                        //     if (element.ajusted === true) {
                                        //         element.visibleAjus = true;    
                                        //         element.visibleAdd = false;
                                        //         element.icon = "sap-icon://add"
                                        //         element.handleLink = "onDialogAjuste"
                                        //     }else{
                                        //         element.visibleAjus = false;    
                                        //         element.visibleAdd = true;
                                        //         element.icon = "sap-icon://search"
                                        //         element.handleLink = "handleLink"
                                        //     }
                                    
                                        // });
                                        let ajusteButton= new Button ();
                                        ajusteButton = that.getView().byId("ajusteButton");
                                        console.log(ajusteButton);
                                        console.log("data entry:: ", res.data);
                                        mdinventory.setProperty("/entry/records", res.data);
                                        mdinventory.setProperty("/postureVisible", true);
                                        mdinventory.setProperty("/entry/objectSearch", objSearch);
                                        mdinventory.setProperty("/compra",false);
                                        mdinventory.setProperty("/curva",true);
                                        mdinventory.refresh(true);
                                        let records= mdinventory.getProperty("/entry/records");
                                        // ajusteButton.setIcon("sap-icon://search")
                                        // ajusteButton.attachPress(function(){alert("holas")})
                                        console.log(ajusteButton);
                                        records.forEach(item=>{
                                            item.stateQuantity= "None";
                                            item.stateTextQuantity= "";
                                            item.stateFecha_movements= "None";
                                            item.stateTextFecha_movements= "";
                                    
                                            item.fecha_movements= that.formatDate(item.fecha_movements);

                                            item.oldQuantity= item.quantity;
                                            item.oldFecha_movements= item.fecha_movements;
                                        });
                                        console.log("mdinventory", mdinventory);

                                        // if(res.data.length>mdinventory.iSizeLimit)
                                        //     mdinventory.setSizeLimit(res.data.length)

                                        mdinventory.refresh();
                                        that.viewEntryBtns();
                                    } else {
                                        mdinventory.setProperty("/entry/records", []);
                                        mdinventory.setProperty("/entry/objectSearch", {});
                                        that.viewEntryBtns();
                                    }
                                });
                            }
                        }
                    );
                }
            
            }
             
        },

        searchEgress: async function (oEvent) {
        // let sId = oEvent.getSource().sId.substring(36, oEvent.getSource().sId.length);
            let that = this;
            let mdinventory= this.getModel("mdinventory");
            let mdprojected= this.getModel("mdprojected");
            let init_date = this.getView().byId("egressDesde").mProperties.value;
            let end_date = this.getView().byId("egressHasta").mProperties.value;
            let slot = this.getView().byId("filterLotEgress").getSelectedKey();
        
            //resetSearch
            mdinventory.setProperty("/egress/objectSearch", {});
        
            if (init_date === null || init_date== "" || end_date === null || end_date== ""){
            // console.log("fechas vacias")
                MessageToast.show("No se pueden consultar fechas vacías", {
                    duration: 3000,
                    width: "20%"
                });
            }
            else{
            // debugger;
            // console.log("fechas correctas")
                init_date= new Date(init_date);
                end_date= new Date(end_date);
                slot= (slot.toLowerCase()=="todos")? null: slot;
            
                let util = this.getModel("util");
                let serverName = util.getProperty("/serviceUrl") + util.getProperty("/" + util.getProperty("/service") + "/getOutMovementsForDate");
                console.log(serverName);
                util.setProperty("/busy/", true);
            
                let name= mdprojected.getProperty("/selectedRecords/name");
                let objEntry= mdinventory.getProperty("/entry/selectRecord");
                let eggs_storage_id= (Object.keys(objEntry).length === 0)? null : objEntry.eggs_storage_id;
                // debugger;
                let objSearch={//objeto de busquedad
                    eggs_storage_id: eggs_storage_id,
                    init_date: init_date,
                    end_date: end_date,
                    type_movements: "egreso",
                    slot: slot
                };
                console.log(serverName);
                fetch(serverName, {
                    method: "POST",
                    headers: {"Content-type": "application/json; charset=UTF-8"},
                    body: JSON.stringify({
                        eggs_storage_id: objEntry.eggs_storage_id,
                        type_movements: "egreso",
                        init_date: init_date,
                        end_date: end_date,
                        slot: slot
                    })
                }).then(
                    function (response) {
                        util.setProperty("/busy/", false);
                        if (response.status !== 200) {
                            console.log("Looks like there was a problem. Status Code: ",response.status);
                            that.openDialog(stateError, "Ha ocurrido un error.");
                            return;
                        }
                        else{
                            response.json().then(function (res) {
                                console.log("data egress:: ", res.data);
                                if (res.data.length > 0) {
                                    res.data.forEach(item =>{
                                        item.name= name;
                                    // item.lot= objEntry.lot
                                    });
                                    mdinventory.setProperty("/egress/records", res.data);
                                    mdinventory.setProperty("/egress/objectSearch", objSearch);
                                    // mdinventory.setSizeLimit(res.data.length)
                                    mdinventory.refresh();
                                
                                } else {
                                    mdinventory.setProperty("/egress/records", []);
                                    mdinventory.setProperty("/egress/objectSearch", {});
                                }
                            });
                        }
                    }
                );
            }
              
        },

        onDialogPressEx: function(){
            let mdinventory= this.getModel("mdinventory");
            let entryList= mdinventory.getProperty("/entry/records");
            let newEntry=[];
            let flagError= false;
            entryList.forEach(function(item){
                if( (item.quantity!== null && item.quantity!="" )|| (item.fecha_movements!== null && item.fecha_movements!="" )){//almenos uno es distinto de null
                    if( (item.quantity!== null && item.quantity!="" )&& (item.fecha_movements!== null && item.fecha_movements!="" )){//ambos son distintos de null
                    //si se detectan cambios
                        if( (item.quantity!= item.oldQuantity)||(item.fecha_movements!= item.oldFecha_movements)){
                        // if(item.quantity> item.eggs_executed){
                            // debugger;
                            if((parseInt(item.quantity)> limitInt) || (parseInt(item.quantity)> (item.eggs_executed +limitDiferencial)) ) {
                                flagError=true;
                                item.stateQuantity= "Warning";
                                item.stateTextQuantity= "La cantidad ejecutada no debe superar la cantidad permitida ";
                            }
                            else{
                            // debugger;
                                item.stateQuantity= "None";
                                item.stateTextQuantity= "";
                                item.stateFecha_movements= "None";
                                item.stateTextFecha_movements= "";
                                let dateMov= item.fecha_movements;
                                newEntry.push({
                                    eggs_storage_id: item.eggs_storage_id,
                                    quantity: item.quantity,
                                    fecha_movements: new Date(item.fecha_movements),
                                    lot: item.lot,
                                    type_movements: "ingreso"
                                });
                            }
                        }
                    }else {//uno solo es distinto de null
                        flagError=true;
                        //si la cantidad ejecutada es el campo null
                        if( (item.quantity== null)){
                            item.stateQuantity= "Error";
                            item.stateTextQuantity= "Ambos campos de ejecucion deben ser llenados";
                        }
                        //si la fecha ejecutada es el campo null
                        if( (item.fecha_movements== null)){
                            item.stateFecha_movements= "Error";
                            item.stateTextFecha_movements= "Ambos campos de ejecucion deben ser llenados";
                        }
                    }
                }else{
                    item.stateQuantity= "None";
                    item.stateTextQuantity= "";
                    item.stateFecha_movements= "None";
                    item.stateTextFecha_movements= "";
                }
            });
            mdinventory.refresh();

            if(!flagError){
                if(newEntry.length>0){
                    // console.log('update', newEntry)
                
                    this.updateEntryColdRoom(newEntry);
                }else{
                    MessageToast.show("No se detectaron cambios", {
                        duration: 3000,
                        width: "20%"
                    });
                }
            }
            else{
                MessageToast.show("Se han ingresados valores incorrectos o incompletos", {
                    duration: 3000,
                    width: "20%"
                });
            }

        },

        updateEntryColdRoom: function(records){
            try{
                let that = this;
                let mdinventory= this.getModel("mdinventory");
                let util = this.getModel("util");
                let serverName = util.getProperty("/serviceUrl") + util.getProperty("/" + util.getProperty("/service") + "/addEntryEggs");
                console.log(serverName);

                util.setProperty("/busy/", true);
                console.log(serverName);
                fetch(serverName, {
                    method: "POST",
                    headers: {"Content-type": "application/json; charset=UTF-8"},
                    body: JSON.stringify({
                        records: records,
                        objSearch: mdinventory.getProperty("/entry/objectSearch")
                    })
                }).then(
                    function (response) {
                        util.setProperty("/busy/", false);
                        if (response.status !== 200) {
                            console.log("Looks like there was a problem. Status Code: ",response.status);
                            that.openDialog(stateError, "Ha ocurrido un error.");
                            return;
                        }
                        else{
                            response.json().then(function (res) {
                                console.log("data entry:: ", res.data);
                                if (res.data.length > 0) {
                                    mdinventory.setProperty("/entry/records", res.data);
                                    let records= mdinventory.getProperty("/entry/records");
                                    records.forEach(item=>{
                                        item.stateQuantity= "None";
                                        item.stateTextQuantity= "";
                                        item.stateFecha_movements= "None";
                                        item.stateTextFecha_movements= "";
                                        // debugger;
                                        item.fecha_movements= that.formatDate(item.fecha_movements);

                                        item.oldQuantity= item.quantity;
                                        item.oldFecha_movements= item.fecha_movements;
                                    });
                                    mdinventory.refresh();
                                    that.viewEntryBtns();
                                } else {
                                    mdinventory.setProperty("/entry/records", []);
                                    that.viewEntryBtns();
                                }
                            });
                            that.openDialog(stateSucess, "Ejecución guardada con exito.");
                       
                        }
                    }
                );
            }catch(err){
                console.log("err: ", err);
                this.openDialog(stateError, "Ha ocurrido un error.");
            }
        },



        getMovementsByEntry: function(objEntry){
            try{
                let that = this;
                let mdinventory= this.getModel("mdinventory");
                let util = this.getModel("util");
                let serverName = util.getProperty("/serviceUrl") + util.getProperty("/" + util.getProperty("/service") + "/getMovementsByEntry");
                console.log(serverName);

                //resetSearch
                mdinventory.setProperty("/egress/records", []);
                mdinventory.setProperty("/egress/objectSearch", {});

                util.setProperty("/busy/", true);
                console.log(serverName);
                fetch(serverName, {
                    method: "POST",
                    headers: {"Content-type": "application/json; charset=UTF-8"},
                    body: JSON.stringify({
                        eggs_storage_id: objEntry.eggs_storage_id,
                        type_movements: "egreso"
                    })
                }).then(
                    function (response) {
                        util.setProperty("/busy/", false);
                        if (response.status !== 200) {
                            console.log("Looks like there was a problem. Status Code: ",response.status);
                            that.openDialog(stateError, "Ha ocurrido un error.");
                            return;
                        }
                        else{
                            response.json().then(function (res) {
                            // debugger;
                                if (res.data.length > 0) {
                                    let sumEgress=0;
                                    res.data.forEach(item =>{
                                        item.name= objEntry.name;
                                        item.lot= objEntry.lot;
                                        sumEgress+= item.quantity;
                                    });
                                    mdinventory.setProperty("/egress/records", res.data);
                                    let ajus= objEntry.ajustes,
                                        sum = 0;
                                    ajus.forEach(element => {
                                        sum = parseInt(sum)+parseInt(element.quantity);
                                    });
                                    mdinventory.setProperty("/egress/egressBalance", parseInt (objEntry.quantity)+ parseInt (sum)- parseInt( sumEgress));
                                    mdinventory.refresh();
                                    console.log(mdinventory);

                                
                                
                                } else {
                                    mdinventory.setProperty("/egress/records", []);
                                    mdinventory.setProperty("/egress/egressBalance",objEntry.quantity);
                                }
                            });
                        }
                    }
                );
            }catch(err){
                console.log("err: ", err);
                this.openDialog(stateError, "Ha ocurrido un error.");
            }
        },

        viewEntryBtns: function(){
        // debugger;
            let mdinventory= this.getModel("mdinventory");
            mdinventory.setProperty("/entry/viewEntrySave", (mdinventory.getProperty("/entry/records").length>0));
        },

        openDialog: function(state, msj){

            let dialog = new Dialog({
                title: "Información",
                type: "Message",
                state: state,
                content: new Text({
                    text: msj
                }),
                beginButton: new Button({
                    text: "OK",
                    press: function() {
                        dialog.close();
                    }
                }),
                afterClose: function() {
                    dialog.destroy();
                }
            });
            dialog.open();
        },

        loadLots: function(){
            try{
                let that = this;
                let mdlot= this.getModel("mdlot");
                let mdprojected= this.getModel("mdprojected");
                let mdinventory= this.getModel("mdinventory");
                let util = this.getModel("util");
                let serverName = util.getProperty("/serviceUrl") + util.getProperty("/" + util.getProperty("/service") + "/getAllLots");
                let objProjected= mdprojected.getProperty("/selectedRecords");
                console.log(serverName);

                //resetRecords
                mdlot.setProperty("/records", []);
                mdlot.setProperty("/selectRecords/entry", {});

                util.setProperty("/busy/", true);
                fetch(serverName, {
                    method: "POST",
                    headers: {"Content-type": "application/json; charset=UTF-8"},
                    body: JSON.stringify({
                        incubator_plant_id: objProjected.incubator_plant_id,
                    })
                }).then(function (response) {
                // debugger;
                    util.setProperty("/busy/", false);
                    if (response.status !== 200) {
                        console.log("Looks like there was a problem. Status Code: ",response.status);
                        that.openDialog(stateError, "Ha ocurrido un error.");
                        return;
                    }
                    else{
                        response.json().then(function (res) {
                        // debugger;
                            if (res.data.length > 0) {
                                res.data.unshift({
                                    lot: "Todos",
                                    total: 0
                                });
                                mdlot.setProperty("/records", res.data);
                                //reset filter lot for entry
                                let slot= mdinventory.getProperty("/entry/objectSearch/slot");
                                slot= (slot=== undefined || slot== null || slot=="")? "Todos" : slot;
                                mdlot.setProperty("/selectRecords/entry", {lot: slot} );
                            
                                //reset filter lot for egress
                                slot= mdinventory.getProperty("/egress/objectSearch/slot");
                                slot= (slot=== undefined || slot== null || slot=="")? "Todos" : slot;
                                mdlot.setProperty("/selectRecords/egress", {lot: slot} );
                            } else {
                                mdlot.setProperty("/records", []);
                                mdlot.setProperty("/selectRecords/entry", {});
                            }
                        });
                    }
                });
            }catch(err){
                console.log("err: ", err);
                this.openDialog(stateError, "Ha ocurrido un error.");
            }
        },

        resetEgressFilters: function(){
            let mdinventory= this.getModel("mdinventory");
            let mdlot= this.getModel("mdlot");

            this.getView().byId("egressDesde").setValue("");
            this.getView().byId("egressHasta").setValue("");
            mdinventory.setProperty("/egress/records", []);
            mdlot.setProperty("/selectRecords/egress", {lot: null} );

        
        },

        onDialogAjuste: function(oEvent) {
            let id = oEvent.getSource().oPropagatedProperties.oBindingContexts.mdinventory.sPath,
                mdinventory = this.getModel("mdinventory"),
                selected = mdinventory.getProperty(id),
                obj = mdinventory.getProperty(id);

            mdinventory.setProperty("/selectedRecordPath",id);
            console.log(mdinventory.getProperty(id));
            mdinventory.setProperty("/entryDate",obj.oldFecha_movements);
            let selectedItem = {
                key: "Plexus",
                name: "Plexus"
            };
        
            this.formAjuste = sap.ui.xmlfragment(
                "coldRoom.view.DialogAjuste", this);
            var that = this;
            var dlg = sap.ui.getCore().byId("dialogAjuste");
            dlg.attachAfterClose(function () {
                that.formAjuste.destroy();
            });
            this.getView().addDependent(this.formAjuste);
            this.formAjuste.open();
        
        },
        onDialogAjusteEgress: function(oEvent) {

            let mdinventory = this.getModel("mdinventory"),
                obj = mdinventory.getProperty("/entry/selectRecord");
        
        
            console.log(obj);
            mdinventory.setProperty("/egressDate",obj.oldFecha_movements);
            this.formAjusteEgress = sap.ui.xmlfragment(
                "coldRoom.view.egress.DialogAjuste", this);
            var that = this;
            var dlg = sap.ui.getCore().byId("dialogAjusteEgress");
            dlg.attachAfterClose(function () {
                that.formAjusteEgress.destroy();
            });
            this.getView().addDependent(this.formAjusteEgress);
            this.formAjusteEgress.open();
        },
        onCloseDialogAjusteEgress:function(){
            this.formAjusteEgress.close();
            mdinventory.setProperty("/selectedRecordPath","");
            this.formAjusteEgress.destroy();
        },
        onSaveDialogAjusteEgress:function(){
            let fecha_movements = sap.ui.getCore().byId("date_ajusteE").getValue(), 
                quantity = sap.ui.getCore().byId("ajuste_quantityE").getValue(), 
                description = sap.ui.getCore().byId("descriptionSelectE").getSelectedKey(),
                mdinventory = this.getModel("mdinventory"),
                SelectedItem = mdinventory.getProperty("/entry/selectRecord"),
                newEntry = [],
                pDate = fecha_movements.split("-"),
                ndate = `${pDate[0]}/${pDate[1]}/${pDate[2]}`;
            console.log(fecha_movements);
            console.log(ndate);
            newEntry.push({
                eggs_storage_id: SelectedItem.eggs_storage_id,
                quantity: quantity,
                fecha_movements: fecha_movements,
                lot: SelectedItem.lot,
                type_movements: "egreso",
                description_adjustment: description
            });
            this.updateAdjustEgressColdRoom(newEntry);
            //Ya tengo lo necesario para hacer guardar la información
            //creo
            //... 

            //Al recibir la respuesta despues de guardar hacemos esto para cerrar el dialog
            this.formAjusteEgress.close();
            mdinventory.setProperty("/selectedRecordPath","");
            this.formAjusteEgress.destroy();
        },
        onCloseDialogAjuste:function(){
            this.formAjuste.close();
            // mdinventory.setProperty("/selectedRecordPath","");
            this.formAjuste.destroy();
        },
        onCloseDialogNew:function(){
            this.formNew.close();
            // mdinventory.setProperty("/selectedRecordPath","");
            this.formNew.destroy();
        },
        onSaveDialogAjuste:function(){

            let fecha_movements = sap.ui.getCore().byId("date_ajuste").getValue(), 
                quantity = sap.ui.getCore().byId("ajuste_quantity").getValue(), 
                description = sap.ui.getCore().byId("descriptionSelect").getSelectedKey(),
                mdinventory = this.getModel("mdinventory"),
                selected_path = mdinventory.getProperty("/selectedRecordPath"),
                SelectedItem = mdinventory.getProperty(selected_path),
                newEntry = [],
                pDate = fecha_movements.split("/"),
                ndate = this.formatDate(fecha_movements);
            console.log(pDate);
            console.log(SelectedItem);
            console.log(fecha_movements, quantity, description);
            console.log(ndate);
            console.log(new Date(fecha_movements));
            newEntry.push({
                eggs_storage_id: SelectedItem.eggs_storage_id,
                quantity: quantity,
                fecha_movements: ndate,
                lot: "X",
                type_movements: "ingreso",
                description_adjustment: description
            });
            this.updateAdjustEntryColdRoom(newEntry);
            //Ya tengo lo necesario para hacer guardar la información
            //creo
            //... 

            //Al recibir la respuesta despues de guardar hacemos esto para cerrar el dialog


            this.formAjuste.close();
            mdinventory.setProperty("/selectedRecordPath","");
            this.formAjuste.destroy();
        },
        updateAdjustEntryColdRoom: function(records){
            try{
                let that = this;
                let mdinventory= this.getModel("mdinventory");
                let util = this.getModel("util");
                let serverName = util.getProperty("/serviceUrl") + util.getProperty("/" + util.getProperty("/service") + "/addEntryEggs");
                console.log(serverName);

                util.setProperty("/busy/", true);
                console.log(serverName);
                fetch("/coldRoom/addAdjustEntryEggs", {
                    method: "POST",
                    headers: {"Content-type": "application/json; charset=UTF-8"},
                    body: JSON.stringify({
                        records: records,
                        objSearch: mdinventory.getProperty("/entry/objectSearch")
                    })
                }).then(
                    function (response) {
                        util.setProperty("/busy/", false);
                        if (response.status !== 200) {
                            console.log("Looks like there was a problem. Status Code: ",response.status);
                            that.openDialog(stateError, "Ha ocurrido un error.");
                            return;
                        }
                        else{
                            response.json().then(function (res) {
                                console.log("data entry:: ", res.data);
                                if (res.data.length > 0) {
                                    mdinventory.setProperty("/entry/records", res.data);
                                    let records= mdinventory.getProperty("/entry/records");
                                    records.forEach(item=>{
                                        item.stateQuantity= "None";
                                        item.stateTextQuantity= "";
                                        item.stateFecha_movements= "None";
                                        item.stateTextFecha_movements= "";
                                        // debugger;
                                        item.fecha_movements= that.formatDate(item.fecha_movements);

                                        item.oldQuantity= item.quantity;
                                        item.oldFecha_movements= item.fecha_movements;
                                    });
                                    mdinventory.refresh();
                                    that.viewEntryBtns();
                                } else {
                                    mdinventory.setProperty("/entry/records", []);
                                    that.viewEntryBtns();
                                }
                            });
                            that.openDialog(stateSucess, "Ejecución guardada con exito.");
                       
                        }
                    }
                );
            }catch(err){
                console.log("err: ", err);
                this.openDialog(stateError, "Ha ocurrido un error.");
            }
        },
        updateAdjustEgressColdRoom: function(records){
            try{
                let that = this;
                let mdinventory= this.getModel("mdinventory"),
                    obj = mdinventory.getProperty("/entry/selectRecord"),
                    name = mdinventory.getProperty("/entry/selectRecord/name");

                fetch("/coldRoom/addAdjustEgressEggs", {
                    method: "POST",
                    headers: {"Content-type": "application/json; charset=UTF-8"},
                    body: JSON.stringify({
                        records: records,
                        objSearch: mdinventory.getProperty("/entry/objectSearch")
                    })
                }).then(
                    function (response) {
                   
                        if (response.status !== 200) {
                            console.log("Looks like there was a problem. Status Code: ",response.status);
                            that.openDialog(stateError, "Ha ocurrido un error.");
                            return;
                        }
                        else{
                            response.json().then(function (res) {
                                console.log("data entry:: ", res);
                                if (res.data.length > 0) {
                                    mdinventory.setProperty("/egress/records", res.data);
                                    let records= mdinventory.getProperty("/egress/records");
                                    let sumEgress=0;
                                    records.forEach(item=>{
                                        sumEgress+= item.quantity;
                                        item.name= name;
                                        item.stateQuantity= "None";
                                        item.stateTextQuantity= "";
                                        item.stateFecha_movements= "None";
                                        item.stateTextFecha_movements= "";
                                        // debugger;
                                        console.log(that.formatDate(item.fecha_movements));
                                        // item.fecha_movements= that.formatDate(item.fecha_movements);
                                        item.oldQuantity= item.quantity;
                                        item.oldFecha_movements= item.fecha_movements;
                                    });
                                    let ajus= obj.ajustes,
                                        sum = 0;
                                    ajus.forEach(element => {
                                        sum = parseInt(sum)+parseInt(element.quantity);
                                    });
                                    mdinventory.setProperty("/egress/egressBalance", parseInt (obj.quantity)+ parseInt (sum)- parseInt( sumEgress));
                                    mdinventory.refresh();
                                    console.log(mdinventory);
                                // that.viewEntryBtns();
                                } else {
                                    mdinventory.setProperty("/egress/records", []);
                                    that.viewEntryBtns();
                                }
                            });
                            that.openDialog(stateSucess, "Ajuste guardado con exito.");
                       
                        }
                    }
                );
            }catch(err){
                console.log("err: ", err);
                this.openDialog(stateError, "Ha ocurrido un error.");
            }
        },
        onBreedLoad: function () {
            let util = this.getModel("util");
            let serverName = util.getProperty("/serviceUrl") + util.getProperty("/" + util.getProperty("/service") + "/findBreed")+"/findAllBreedWP";
            let inreal = this.getView().getModel("incubatorRealNew");
            let mdbreed = this.getModel("mdbreed");
            mdbreed.setProperty("/records", []);

            let isRecords = new Promise((resolve, reject) => {
                fetch(serverName).then(
                    function (response) {
                        if (response.status !== 200) {
                            console.log("Looks like there was a problem. Status Code: ",response.status);
                            return;
                        }
                        // Examine the text in the response
                        response.json().then(function (data) {
                            console.log(data);
                            // inreal.setProperty("/breed", data.data);
                            resolve(data);
                        });
                    }
                )
                    .catch(function (err) {
                        console.log("Fetch Error :-S", err);
                    });
            });
            console.log(isRecords);
            isRecords.then((res) => {
                if (res.data.length > 0) {
                    mdbreed.setProperty("/records", res.data);
                    mdbreed.setProperty("/selectedKey", mdbreed.getProperty("/records/0/breed_id"));
                // console.log(mdbreed);
                }
            });
        },
        onSavePress: async function(){
        // let id = oEvent.getSource().oPropagatedProperties.oBindingContexts.mdinventory.sPath,
            // mdinventory = this.getModel("mdinventory"),
            // selected = mdinventory.getProperty(id);

            // mdinventory.setProperty("/selectedRecordPath",id);
            // console.log(mdinventory.getProperty(id));
            await this.onBreedLoad();
            this.formNew = sap.ui.xmlfragment(
                "coldRoom.view.DialogNew", this);
            var that = this;
            var dlg = sap.ui.getCore().byId("dialogNew");
            dlg.attachAfterClose(function () {
                that.formNew.destroy();
            });
            this.getView().addDependent(this.formNew);
            this.formNew.open();
        },
        //   onCloseDialogAjusteNew:function(){
        //     this.formNew.close();
        //     // mdinventory.setProperty("/selectedRecordPath","");
        //     this.formNew.destroy();
        // },
        onSaveNewIngress:function(){

            let fecha_movements = sap.ui.getCore().byId("date_new").getValue(), 
                quantity = sap.ui.getCore().byId("new_quantity").getValue(), 
                description = sap.ui.getCore().byId("descriptionNewSelect").getSelectedKey(),
                mdinventory = this.getModel("mdinventory"),
                newEntry = [],
                pDate = fecha_movements.split("-"),
                mdincubator = this.getModel("mdincubator"),
                ospartnership = this.getModel("ospartnership"),
                mdprojected= this.getModel("mdprojected"),
                objProjected= mdprojected.getProperty("/selectedRecords"),
                partnership_id = ospartnership.getProperty("/selectedRecord/partnership_id"),
                incubator_plant_id = objProjected.incubator_plant_id,
                breed_id = this.getModel("mdbreed").getProperty("/selectedKey"),
                ndate = this.formatDate(fecha_movements),
                mdscenario = this.getModel("mdscenario"),
                scenario_id = mdscenario.getProperty("/scenario_id");
            // console.log(SelectedItem)
            // 'incubator_plant_id', 'scenario_id',  'breed_id' , 'init_date' , 'end_date', 'lot', 'eggs', 'eggs_executed', 'origin'
            console.log(fecha_movements, quantity, description);
            console.log(mdprojected);
            console.log(ndate);
            console.log(ospartnership);
            console.log(mdincubator);
            console.log(this.getModel("mdlot"));
            console.log(new Date(fecha_movements));
            newEntry.push({
                incubator_plant_id: incubator_plant_id,
                scenario_id: scenario_id,
                breed_id: breed_id,
                init_date: ndate,
                end_date: ndate,
                lot: "C",
                eggs: quantity,
                eggs_executed: quantity,
                origin: 1
            });
            console.log(newEntry);
            this.NewEntryColdRoom(newEntry);
            //Ya tengo lo necesario para hacer guardar la información
            //creo
            //... 

            //Al recibir la respuesta despues de guardar hacemos esto para cerrar el dialog


            this.formNew.close();
            // mdinventory.setProperty("/selectedRecordPath","");
            this.formNew.destroy();
        },
        changeOrigin: function(oEvent){
            let origin =  this.getView().byId("filterOriginEntry").getSelectedKey(),
                mdinventory = this.getModel("mdinventory");
            console.log(origin, oEvent);

            if(origin === "Compra"){
                mdinventory.setProperty("/entry/entryNew",true);
                mdinventory.setProperty("/entry/viewEntrySave",false);
            // mdinventory.setProperty("/curva",false);
            }else{
                mdinventory.setProperty("/entry/entryNew",false);
            // mdinventory.setProperty("/entry/viewEntrySave",true);
            // mdinventory.setProperty("/curva",true);
            }
        },
        NewEntryColdRoom: function(newEntry){
            try{
                let that = this;
                let mdinventory= this.getModel("mdinventory");
                let util = this.getModel("util");
                let serverName = util.getProperty("/serviceUrl") + util.getProperty("/" + util.getProperty("/service") + "/addEntryEggs");
                let ospartnership = this.getModel("ospartnership"),
                    partnership_id = ospartnership.getProperty("/selectedRecords/partnership_id");

                console.log(partnership_id);

                util.setProperty("/busy/", true);
                console.log(serverName);
                fetch("/coldRoom/addNewEntryEggs", {
                    method: "POST",
                    headers: {"Content-type": "application/json; charset=UTF-8"},
                    body: JSON.stringify({
                        records: newEntry,
                        partnership_id: partnership_id
                    })
                }).then(
                    function (response) {
                        util.setProperty("/busy/", false);
                        if (response.status !== 200) {
                            console.log("Looks like there was a problem. Status Code: ",response.status);
                            that.openDialog(stateError, "Ha ocurrido un error.");
                            return;
                        }
                        else{
                            response.json().then(function (res) {
                                console.log("data entry:: ", res.data);
                                if (res.data.length > 0) {
                                    mdinventory.setProperty("/entry/records", res.data);
                                    let records= mdinventory.getProperty("/entry/records");
                                    records.forEach(item=>{
                                        item.stateQuantity= "None";
                                        item.stateTextQuantity= "";
                                        item.stateFecha_movements= "None";
                                        item.stateTextFecha_movements= "";
                                        // debugger;
                                        item.fecha_movements= that.formatDate(item.fecha_movements);

                                        item.oldQuantity= item.quantity;
                                        item.oldFecha_movements= item.fecha_movements;
                                    });
                                    mdinventory.refresh();
                                    mdinventory.setProperty("/compra",true);
                                    mdinventory.setProperty("/curva",false);
                                // that.viewEntryBtns();
                                } else {
                                    mdinventory.setProperty("/entry/records", []);
                                    that.viewEntryBtns();
                                }
                            });
                            that.openDialog(stateSucess, "Ejecución guardada con exito.");
                       
                        }
                    }
                );
            }catch(err){
                console.log("err: ", err);
                this.openDialog(stateError, "Ha ocurrido un error.");
            }
        },
        onClick: function(oEvent){
            let ev = oEvent;
            console.log(ev);
            var mdinventory = this.getView().getModel("mdinventory");
            console.log(mdinventory);
            let selectObject = oEvent.getSource().getBindingContext("mdinventory").getObject();
            console.log(selectObject);
            if(selectObject.adjusted === true){
                this.handleLink(ev);
            }else{
                this.onDialogAjuste(ev);
            }
        
        }

    });
});
