sap.ui.define([], function() {
    "use strict";

    return {
        calculateResidue: function(projected_quantity, residue) {
            return (projected_quantity-residue).toLocaleString();
        },
        formatMiles: function(number){
            return (number !== "" && !isNaN(number)) ? number.toLocaleString() : number;
        },
        formatDays: function(date){
            let aDay=["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                aDate = date.split("-"),
                dt = new Date(aDate[2], aDate[1]-1, aDate[0]);
            return `${aDay[dt.getUTCDay()]} ${aDate[0]}/${aDate[1]}/${aDate[2]}`;
        },
        formatAjuste: function(ajus){

            if (ajus == "Ajuste") {
                ajus = "Ajuste Ingreso";
            }else{
                ajus = "Ajuste Egreso";
            }
            return ajus;
        },
        formatDate: function(date){

            if(date!== null){
                let c= "/";//caracter separatos
                date= new Date(date.toString());
                // if((typeof date.getMonth === 'function'))
                if(!isNaN( date.getFullYear() )){
                    date= ( ((date.getDate()<10)? "0"+date.getDate(): date.getDate() )+c+
							((date.getMonth()+1<10)? "0"+(date.getMonth()+1): date.getMonth()+1 )+c+
							date.getFullYear() );
                }
                else
                    date=null;
            }
            return (date);
        },
        showBtnForLength: function(records){
            return (records!== null && records.length>0);
        },
        formatIcon: function(value){
            if(value=== true){
                return("sap-icon://search");
            }else{
                return("sap-icon://add");
            }
        }
    };
});
