sap.ui.define([
    "annualPostureCurve/controller/BaseController",
    "jquery.sap.global",
    "sap/ui/model/Filter",
    "sap/ui/core/Fragment",
    "sap/ui/model/json/JSONModel",
    "sap/m/MessageToast",
    "sap/m/Dialog",
    "sap/m/Button",
    "sap/m/Text",
    "sap/ui/core/Item",
    "sap/m/IconTabFilter"
], function(BaseController, jQuery, Filter, Fragment, JSONModel, MessageToast, Dialog, Button, Text, item,IconTabFilter) {
    "use strict";
    const breedingStage = 3; /*Clase para Reproductora*/
    return BaseController.extend("annualPostureCurve.controller.Detail", {

        onInit: function() {
            // console.log("mas volumen puede ser")
            this.setFragments();
            this.getRouter().getRoute("detail").attachPatternMatched(this._onRouteMatched, this);


        },

        _onRouteMatched: async function(oEvent) {
            var oArguments = oEvent.getParameter("arguments");
            let mdscenario = this.getModel("mdscenario");
            let activeS = await this.activeScenario();
            mdscenario.setProperty("/scenario_id", activeS.scenario_id);
            mdscenario.setProperty("/name", activeS.name);

            this.index = oArguments.id;

            //   let oView= this.getView();
            //   let ospartnership = this.getModel("ospartnership");
            //   oView.byId("tabBar").setSelectedKey("tabParameter");
            //   oView.byId("idProductsTable").addEventDelegate({
            //     onAfterRendering: oEvent=>{
            //         console.log("victor te amo!")
            //     }
            // })







            // if(ospartnership.getProperty("/records").length>0){
            //     let partnership_id = ospartnership.getProperty("/selectedRecords/partnership_id")
            //     this.onRead(partnership_id);
            // }
            // else{
            //     this.reloadPartnership()
            //     .then(data => {
            //         if(data.length>0){
            //             let obj= ospartnership.getProperty("/selectedRecords/");
            //             if(obj){
            //                 this.onRead(obj.partnership_id);
            //             }
            //             else{
            //                 MessageToast.show("no existen empresas cargadas en el sistema", {
            //                     duration: 3000,
            //                     width: "20%"
            //                 });
            //                 console.log("err: ", data)
            //             }
            //         }
            //         else{
            //             MessageToast.show("ha ocurrido un error al cargar el inventario", {
            //                 duration: 3000,
            //                 width: "35%"
            //             });
            //             console.log("err: ", data)
            //         }
            //     });
            // }


            this.getView().byId("__header0").bindElement("ospartnership>/records/" + this.index + "/");

            this.getView().byId("Selectyears").setSelectedItem(new item ());
            this.getView().byId("breedSelect").setSelectedItem(new item ());
            this.getView().byId("selectBreed").setSelectedItem(new item ());

            // this.getView().byId("selectBreed").setSelectedItem(new item ());
            // this.getView().byId("selectLot").setSelectedItem(new item ());
            let emp = this.getView().getModel("posturecurve").getProperty("/id_empresa");
            this.onBreedLoad();
            fetch("/eggs_storage/getEggsStorageYears", {
                method: "GET",


            })
                .then(
                    (response) => {
                        if (response.status !== 200) {
                            console.log("Looks like there was a problem. Status Code: " +
              response.status);
                            return;
                        }

                        response.json().then((res)=> {
                            res.data.push({"year": "Todos"});
                            console.log("Años ", res.data)
                            this.getView().getModel("posturecurve").setProperty("/years",res.data);
                        });
                    }
                );

            // this.onRead(this.index);
        },

        reloadPartnership: function(){
            let util = this.getModel("util");
            let that = this;
            let ospartnership = this.getModel("ospartnership");

            util.setProperty("/busy/", true);
            ospartnership.setProperty("/records", []);

            let url = util.getProperty("/serviceUrl") +util.getProperty("/" + util.getProperty("/service") + "/getPartnership");
            let method = "GET";
            let data = {};
            return new Promise((resolve, reject) => {
                function getPartnership(res) {
                    util.setProperty("/busy/", false);
                    ospartnership.setProperty("/records/", res.data);
                    if(res.data.length>0){
                        let obj= res.data[0];
                        obj.index= 0;
                        ospartnership.setProperty("/selectedRecords/", obj);
                        ospartnership.setProperty("/name", obj.name);
                        ospartnership.setProperty("/address", obj.address);
                        // console.log("la data en el detail el mio: ",data)
                    }
                    resolve(res.data);
                }

                function error(err) {
                    console.log(err);
                    ospartnership.setProperty("/selectedRecords/", []);
                    util.setProperty("/error/status", err.status);
                    util.setProperty("/error/statusText", err.statusText);
                    reject(err);
                }

                /*Envía la solicitud*/
                // console.log("antes del send request, this:  ",this, "url:",url, "method: ",method, "data:",data, "getPartnership: ",getPartnership, "error: ",error, error)
                this.sendRequest.call(this, url, method, data, getPartnership, error, error);
            });
        },

        blockinput:function(){
            var aux= this.getView().getModel("posturecurve");
            aux.setProperty("/input",true);


            this.getView().byId("executionSaveBtn").setVisible(true);
            // this.getView().byId("ModifiyBtn").setEnabled(false)
            this.getView().byId("executionSaveBtn").setEnabled(true);
        },



        loadDays:function(oEvent){
            console.log("paso a circulo");
            //eliminado por FROM SAP
            // this.getView().byId("executionSaveBtn").setVisible(true);
            // this.getView().byId("ModifiyBtn").setVisible(true)
            // this.getView().byId("executionSaveBtn").setEnabled(false);
            // this.getView().byId("ModifiyBtn").setEnabled(true)
            let posturecurve=this.getView().getModel("posturecurve");
            let modelo= this.getView().getModel("posturecurve");
            let modeloo = this.getView().getModel("posturecurve");
            console.log(oEvent.getParameters());
            let lot= oEvent.getParameters().listItem.getBindingContext("posturecurve").getObject().lot;
            console.log(lot);
            let num_week= oEvent.getParameters().listItem.mAggregations.cells[0].mProperties.text;
            let start_date = oEvent.getParameters().listItem.mAggregations.cells[1].mProperties.text;
            let year = posturecurve.getProperty("/theyeartolook");
            let breed = posturecurve.getProperty("/thebreedtolook");
            // console.log({year});
            // console.log({breed});
            let week = start_date.split("/");
            var d=new Date(start_date.split("/").reverse().join("-"));
            var dd=d.getDate()+1;
            var mm=d.getMonth()+1;
            var yy=d.getFullYear();
            var newdate=yy+"/"+mm+"/"+dd;
            var end = new Date(newdate);
            end.setDate(end.getDate() + 6);
            // console.log({start_date});


            let jsonTemp =
          { "end_date":end,
              "init_date": start_date,
              "lot":lot };

            posturecurve.setProperty("/savingthedate",jsonTemp);
            posturecurve.setProperty("/returnupdate",{
                breed_id: breed,
                year:year,
                lot: lot,
                num_week: num_week

            });

            start_date = start_date.split("/");
            start_date = start_date[2] + "-" + start_date[1] + "-" + start_date[0];


            console.log(lot);
            fetch("/eggs_storage/findEggsStorageDetailByYearWeekBreedLot", {
                method: "POST",
                headers: {
                    "Content-type": "application/json; charset=UTF-8"
                },

                body: JSON.stringify({
                    breed_id: breed,
                    year:year,
                    lot: lot,
                    num_week: num_week

                })


            })
                .then(
                    function(response) {
                        if (response.status !== 200) {
                            console.log("Looks like there was a problem. Status Code: " +
              response.status);
                            return;
                        }

                        response.json().then(function(res) {
                            console.log("algo");
                            console.log(res.data);
                            console.log("aiudaaaaaaaaaaaaaaaaaa");
                            res.data.forEach(item =>{

                                // let formatDate= `${(ddate.getDate() < 10 ? '0' : '') + ddate.getDate()}/${((ddate.getMonth() + 1) < 10 ? '0' : '') + (ddate.getMonth() + 1)}/${ddate.getFullYear()}`;
                                let formatDate= item.dia.split("-");
                                // console.log(formatDate);
                                let lol= formatDate[2]+"/"+formatDate[1]+"/"+formatDate[0];
                                posturecurve.setProperty("/formateDate", formatDate);

                                item.available= (item.eggs_executed==null || item.eggs_executed< 0)? true: false;
                                item.dia= lol;
                            });

                            modelo.setProperty("/dayExecuted", res.data);
                            // modelo.setProperty("/dataOld", res.data);
                        });
                    }
                );

            let oView = this.getView();
            this.getView().getModel("posturecurve").setProperty("/executed/enabledTab");
            oView.byId("tabBar").setSelectedKey("kTabProjected");


        },
        formDate:function(date){
            var v = new Date(date);
            // console.log(date)
            if(date){
                var aux= date.split("/");
                var l =  `${aux[2]}-${aux[1]}-${aux[0]}`;
                // aux[0].toString()+"/"+aux[1].toString()+"/"+aux[2].toString()
                // console.log(v)
                v = new Date(l);}
            return(v);
        },
        onSelectLot:function(oEvent){

            let lot = oEvent.getParameters(),
                posturecurve=this.getView().getModel("posturecurve");
            posturecurve.setProperty("/input",true);
            // console.log(lot);
            let modelo= this.getView().getModel("posturecurve"),
                that = this;

            // let lote = oEvent.getParameters().selectedItem.mProperties.key;
            // var aux= this.getView().getModel("posturecurve").getProperty("/semanas")
            // var arr=new Array();
            // console.log(aux)
            // aux.forEach(element => {
            //   if(element.lot==lote){
            //     arr.push(element)
            //   }
            // });
            // let week_id=arr[0].week_id;
            // console.log(arr)
            // modelo.setProperty("/lotaux", lote);
            // console.log(this.getView().getModel("posturecurve"))
            let x =  (this.getView().getModel("posturecurve").getProperty("/fechainiciotemporal"));
            var end = new Date(x);
            end.setDate(end.getDate()-1);
            // console.log(x)
            // console.log(this.getView().getModel("posturecurve").getProperty("/fechafintemporal"))
            let y =  (this.getView().getModel("posturecurve").getProperty("/fechafintemporal"));
            // let x="2018-05-15]]T04:30:00.000Z"
            // let y="2018-05-22T04:30:00.000Z"
            let z=this.getView().byId("selecLot").setSelectedItem().mProperties.selectedKey;
            // console.log("hey")
            // console.log(z)
            // console.log(x)
            // console.log(y)



            fetch("/eggs_storage/findEggsStorageByLotAndDate", {
                method: "POST",
                headers: {
                    "Content-type": "application/json; charset=UTF-8"
                },

                body: JSON.stringify({
                    lot: z,
                    init_date_start:  end,
                    init_date_end: y

                })


            })
                .then(
                    function(response) {
                        if (response.status !== 200) {
                            console.log("Looks like there was a problem. Status Code: " +
              response.status);
                            return;
                        }

                        response.json().then(function(res) {
                            // console.log("algo")
                            // console.log(res.data);
                            // console.log("aiudaaaaaaaaaaaaaaaaaa");
                            res.data.forEach(item =>{
                                let ddate= new Date(item.init_date);
                                let formatDate= `${(ddate.getDate() < 10 ? "0" : "") + ddate.getDate()}/${((ddate.getMonth() + 1) < 10 ? "0" : "") + (ddate.getMonth() + 1)}/${ddate.getFullYear()}`;
                                posturecurve.setProperty("/formateDate", formatDate);

                                item.formatDate= formatDate;
                            });
                            modelo.setProperty("/dayExecuted", res.data);
                            //modelo.setProperty("/dataOld", res.data);
                            console.log("Aquiii voy ", res.data)
                        });
                    }
                );

            // console.log("lotsdddddddddddddddddddddde")
        },
        onRead: function (oEvent) {
            let breed_id = this.getView().byId("breedSelect").mProperties.selectedKey;
            let year = this.getView().byId("Selectyears").mProperties.selectedKey;
            fetch("/eggs_storage/findEggsStorageLots", {
                headers: {
                    "Content-Type": "application/json"
                },
                method: "POST",
                body: JSON.stringify({"breed_id" : breed_id})
            })
                .then(
                    function(response) {
                        if (response.status !== 200) {
                            console.log("Looks like there was a problem. Status Code: " +
                response.status);
                            return;
                        }

                        response.json().then(function(res) {
                            util.setProperty("/busy/", false);
                            // console.log("yup");
                            let insert = new Array();
                            // console.log(res.data);
                            insert.push("Todos");

                            res.data.forEach(element =>{
                                // console.log(element.lot)
                                insert.push(element.lot);
                            });
                            //  console.log(insert.length);
                            //   console.log(insert);
                            posturecurve.setSizeLimit (insert.length+1);
                            posturecurve.setProperty("/lot_init", insert);
                            console.log("Lotes! ", res.data)
                            // console.log(posturecurve.getProperty("/lot_init"))

                            // console.log(posturecurve.getProperty("/lot_init"))
                            // length
                        });
                    }
                )
                .catch(function(err) {
                    console.log("Fetch Error: ", err);
                });


            let el = oEvent.getParameters().selectedItem.mProperties.key,
                ep_id = this.getView().getModel("posturecurve").getProperty("/id_empresa"),
                util = this.getModel("util"),
                that=this,
                posturecurve = this.getView().getModel("posturecurve"),
                serviceUrl = util.getProperty("/serviceUrl") + util.getProperty("/" + util.getProperty("/service") + "/lotPostureCurve");
            // console.log(this.getView().getModel("posturecurve"));
            this.getView().getModel("posturecurve").setProperty("/idBreed_report",el);
            this.getView().getModel("posturecurve").setProperty("/idEmp_report",ep_id);
            // console.log("que"+serviceUrl)




            // console.log(el)
            // console.log("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
            serviceUrl = util.getProperty("/serviceUrl") + util.getProperty("/" + util.getProperty("/service") + "/lotPostureCurve");
            util.setProperty("/busy/", true);
            // console.log("Llegue");
            // console.log("breed_id: ", breed_id);
            let aux= this.getView().getModel("posturecurve").getProperty("/week");
            let params = {
                year: year
            };

            // posturecurve.setProperty("/week", aux);
        },


        onReadRe: function (oEvent) {
            let breed_id = this.getView().byId("selectBreed").mProperties.selectedKey,
                year = this.getView().byId("Selectyears2").mProperties.selectedKey,
                posturecurve = this.getView().getModel("posturecurve");


            // console.log(breed_id, year)


            fetch("/eggs_storage/findEggsStorageLots", {
                headers: {
                    "Content-Type": "application/json"
                },
                method: "POST",
                body: JSON.stringify({"breed_id" : breed_id})
            })
                .then(
                    function(response) {
                        if (response.status !== 200) {
                            console.log("Looks like there was a problem. Status Code: " +
                response.status);
                            return;
                        }

                        response.json().then(function(res) {
                            // console.log("yup");
                            let insert = new Array();
                            // console.log(res.data);
                            insert.push("Todos");

                            res.data.forEach(element =>{
                                // console.log(element.lot)
                                insert.push(element.lot);
                            });
                            //  console.log(insert.length);
                            //   console.log(insert);
                            posturecurve.setSizeLimit (insert.length+1);
                            posturecurve.setProperty("/lot_init", insert);
                            // console.log(posturecurve.getProperty("/lot_init"))

                            // console.log(posturecurve.getProperty("/lot_init"))
                            // length
                        });
                    }
                )
                .catch(function(err) {
                    console.log("Fetch Error: ", err);
                });
            // posturecurve.setProperty("/week", aux);
        },




        onRead2: function () {   //Esta funci�n la vamos a llevar completa a reproductora
            // console.log("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
            let posturecurve = this.getView().getModel("posturecurve"),
                util = this.getModel("util"),
                year = "2018",
                partnership_id=this.getView().getModel("posturecurve").getProperty("/id_empresa"),
                that=this,
                //serviceUrl = util.getProperty("/serviceUrl") + util.getProperty("/" + util.getProperty("/service") + "/lotPostureCurve");
                serviceUrl =  "/posturecurveWeekly/findAllPostureCurveByPartnershipAndBreed";
            // console.log(serviceUrl);
            util.setProperty("/busy/", true);
            // console.log("Llegue");
            let breed_id = this.getView().byId("breedSelect").mProperties.selectedKey;
            // console.log("breed_id: ", breed_id);
            let params = {
                year: year
            };

            fetch(serviceUrl, {
                method: "POST",
                body: "year="+year+"&breed_id="+breed_id+"&partnership_id="+partnership_id,
                headers: {
                    "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
                }
            })
                .then(
                    function(response) {
                        if (response.status !== 200) {
                            console.log("Looks like there was a problem. Status Code: " +
                            response.status);
                            return;
                        }

                        response.json().then(function(res) {
                            util.setProperty("/busy/", false);
                            // console.log(res.data);
                            //guardar res.data en la tabla posturecurve_weekly
                            /* guardamos cantidad proyectada para cada semana
                              guardar numero sem, raza, total eject:0, lote, idparnetship */
                            posturecurve.setProperty("/weekExecuted", res.data);
                            console.log("Data maestra", res.data)
                            // console.log(posturecurve.getProperty("/weekExecuted"));
                            // let j ={
                            //         "records":res.data,
                            //         "breed_id": breed_id,
                            //         "partnership_id":partnership_id
                            //       }
                            //       $.ajax({
                            //         type: "POST",
                            //         contentType: "application/json",
                            //         url: "/posturecurveWeekly",
                            //         dataType: "json",
                            //         data: JSON.stringify(j),
                            //         async: false,
                            //         success: function (data) {
                            //             oModel = new JSONModel(data.data);
                            //             stats = data.stats;
                            //         },
                            //         error: function (request) {
                            //             var msg = request.statusText;
                            //             that.onToast('Error: ' + 'El escenario no tiene datos cargados');
                            //             console.log("Read failed: ", request);
                            //         }
                            //     });
                        });
                    }
                )
                .catch(function(err) {
                    console.log("Fetch Error: ", err);
                });

        },




        // handleLinkPress: function(oEvent){
        //     var groupDetailsDlg = sap.ui.xmlfragment("annualPostureCurve.view.postureCurve.PostureCurveTable2", this);
        //     this.getView().addDependent(this.groupDetailsDlg);

        //     console.log("se prendio la que no se apaga");
        //     var posturecurve = this.getView().getModel("posturecurve");
        //     posturecurve.setProperty("/selectedRecordDialog", JSON.parse(JSON.stringify(oEvent.getSource().getBindingContext("posturecurve").getObject())));

        //     console.log(posturecurve.getProperty("/selectedRecordDialog"));
        //     //console.log(oEvent.getSource().getBindingContext("osfarm").getObject());
        //     let selectObject = oEvent.getSource().getBindingContext("posturecurve").getObject();
        //   console.log(selectObject);
        //     let lot=selectObject.lot;
        //     let eggs=selectObject.eggs;
        //     let eggsForDay=parseInt(eggs/7,10);


        //     let report = new Array();
        //     for (let i = 1; i <=7 ; i++) {
        //         report.push({
        //           "day":i,
        //           "lot":lot,
        //           "eggsForDay":eggsForDay
        //         })
        //       }

        //       posturecurve.setProperty("/datapopover", selectObject);
        //       posturecurve.refresh(true);

        //       console.log(this.getView().getModel("posturecurve").getProperty("/datapopover"));


        //       groupDetailsDlg.openBy(oEvent.getSource());

        //   },


        // onRead: async function(index) {
        //   console.log("mas volumen puede ser3")
        //   let ospartnership = this.getModel("ospartnership"),
        //       mdscenario = this.getModel("mdscenario"),
        //       oView = this.getView();

        //   oView.byId("tabBar").setSelectedKey("kTabParameter")

        //   let activeS = await this.activeScenario();
        //   mdscenario.setProperty("/scenario_id", activeS.scenario_id);
        //   mdscenario.setProperty("/name", activeS.name);

        //   ospartnership.setProperty("/selectedRecordPath/", "/records/" + index);
        //   ospartnership.setProperty("/selectedRecord/", ospartnership.getProperty(ospartnership.getProperty("/selectedRecordPath/")));

        //   // let isBreedLoad = await this.onBreedLoad();


        //   let util = this.getModel("util"),
        //     that = this,
        //     mdprojected = this.getModel("mdprojected"),
        //     mdprogrammed = this.getModel("mdprogrammed");
        //      console.log("mdprojected");
        //     console.log(mdprojected);
        //      console.log(mdprogrammed);
        //      console.log("asddddddddddddddddddddddddddddddddddddddddddddddddddd")


        //   // ospartnership.setProperty("/selectedPartnership/partnership_index", index);

        //   //  let process_info = await this.processInfo(),
        //   //     mdprocess = this.getModel("mdprocess");
        //   //     console.log(process_info.data);
        //   //     //console.log("process_info ",process_info.data[0].theoretical_duration);
        //   //     mdprocess.setProperty("/records", process_info.data);
        //   //     //mdprocess.setProperty("/duration", process_info.data[0].theoretical_duration );
        //   //     //mdprocess.setProperty("/decrease", process_info.data[0].theoretical_decrease );



        //   // let findScenario = await this.findProjected();
        //   // mdprojected.setProperty("/records", findScenario.data);
        //   // that.hideButtons(false, false,false, false);
        //   // this.onFarmLoad();
        //   // let isIncubatorPlant = await this.onIncubatorPlant(),
        //   //     mdincubatorplant = this.getModel("mdincubatorplant");

        //   // mdincubatorplant.setProperty("/records", isIncubatorPlant.data);
        //   // if(isIncubatorPlant.data.length>0){
        //   //   mdincubatorplant.setProperty("/selectedKey", isIncubatorPlant.data[0].incubator_plant_id);
        //   // }





        // },



        validateIntInput: function (o) {
            let input= o.getSource();
            let length = 10;
            let value = input.getValue();
            let regex = new RegExp(`/^[0-9]{1,${length}}$/`);

            if (regex.test(value)) {

                return true;
            }
            else {
                let aux = value
                    .split("")
                    .filter(char => {
                        if (/^[0-9]$/.test(char)) {
                            if (char !== ".") {
                                return true;
                            }
                        }
                    })
                    .join("");
                value = aux.substring(0, length);
                input.setValue(value);
                return false;
            }
        },



        reports: function()
        {
            var mdreports = this.getModel("mdreports");
            // console.log("presione el boton de reportes");
            let date1 = this.getView().byId("sd").mProperties.value,
                date2 = this.getView().byId("sd2").mProperties.value,
                breed_id = this.getView().byId("breedSelect").getSelectedKey();


            let aDate = date1.split("-"),
                init_date = `${aDate[0]}/${aDate[1]}/${aDate[2]}`;

            let aDate2 = date2.split("-"),
                end_date = `${aDate2[0]}/${aDate2[1]}/${aDate2[2]}`;


            // console.log("las fechas");
            // console.log(date1);
            // console.log(date2);
            // console.log(breed_id);
            // console.log("EL MODELO CON FECHAS");
            // console.log(mdreports)
            let serverName = "/reports/breeding";

            fetch(serverName, {
                headers: {
                    "Content-Type": "application/json"
                },
                method: "POST",
                body: JSON.stringify({
                    date1: date1,
                    date2: date2,
                    breed_id: breed_id
                })
            })
                .then(
                    function(response) {
                        if (response.status !== 200) {
                            console.log("Looks like there was a problem. Status Code: " +
                  response.status);
                            return;
                        }

                        response.json().then(function(res) {
                            // console.log("la respuesta despues de reportes")
                            // console.log(res);
                            mdreports.setProperty("/records", res.data);
                            // console.log("la longitud")
                            // console.log(res.data.length)
                            if (res.data.length > 0)
                            {
                                mdreports.setProperty("/reportsBtn", true);
                                mdreports.setProperty("/desde", init_date);
                                mdreports.setProperty("/hasta", end_date);
                                mdreports.setProperty("/visible", true);
                            }
                            else
                            {
                                mdreports.setProperty("/reportsBtn", false);
                                mdreports.setProperty("/visible", false);
                            }
                            resolve(res);
                        });
                    }
                )
                .catch(function(err) {
                    console.log("Fetch Error :-S", err);
                });
        },

        generatedCSV: function()
        {
            var mdreports = this.getModel("posturecurve").getProperty("/weekReport");
            // console.log(mdreports)
            this.arrayObjToCsv(mdreports);
            // this.arrayObjToCsv();
        },

        arrayObjToCsv: function (ar) {
        //comprobamos compatibilidad
            if(window.Blob && (window.URL || window.webkitURL)){
                var contenido = "",
                    d = new Date(),
                    blob,
                    reader,
                    save,
                    clicEvent;
                //creamos contenido del archivo
                var array = ["Fecha", "Lote", "Huevos Proyectados", "Huevos Ejecutados", "Proporcion (%)"];
                // console.log(array)
                // console.log("EL ARRAY")
                // console.log(ar)

                // {
                //   "formatDate": ar[0].formatDate,
                //   "lot": ar[0].lot,
                //   "projected": ar[0].projected,
                //   "executed": ar[0].executed,
                //   "percen": ar[0].percen
                // }



                for (var i = 0; i < ar.length; i++) {
                    let aja = new Array();

                    aja[i] = {
                        "formatDate": ar[i].formatDate,
                        "lot": ar[i].lot,
                        "projected": ar[i].projected,
                        "executed": ar[i].executed,
                        "percen": ar[i].percen
                    };


                    // console.log("se supone que las cabeceras")
                    // console.log(Object.keys(aja[i]))
                    //construimos cabecera del csv
                    if (i == 0)
                        contenido += array.join(";") + "\n";
                    //resto del contenido
                    contenido += Object.keys(aja[i]).map(function(key){
                        return aja[i][key];
                    }).join(";") + "\n";
                }
                // console.log(contenido)
                //creamos el blob
                blob =  new Blob(["\ufeff", contenido], {type: "text/csv"});
                //creamos el reader
                var reader = new FileReader();
                reader.onload = function (event) {
                //escuchamos su evento load y creamos un enlace en dom
                    save = document.createElement("a");
                    save.href = event.target.result;
                    save.target = "_blank";
                    //aqu� le damos nombre al archivo

                    save.download = "salida.csv";


                    try {
                    //creamos un evento click
                        clicEvent = new MouseEvent("click", {
                            "view": window,
                            "bubbles": true,
                            "cancelable": true
                        });
                    } catch (e) {
                    //si llega aqu� es que probablemente implemente la forma antigua de crear un enlace
                        clicEvent = document.createEvent("MouseEvent");
                        clicEvent.initEvent("click", true, true);
                    }
                    //disparamos el evento
                    save.dispatchEvent(clicEvent);
                    //liberamos el objeto window.URL
                    (window.URL || window.webkitURL).revokeObjectURL(save.href);
                };
                //leemos como url
                reader.readAsDataURL(blob);
            }else {
            //el navegador no admite esta opci�n
                alert("Su navegador no permite esta acci�n");
            }
        },
        selRazaReport: function(oEvent){
            let posturecurve = this.getModel("posturecurve");
            let breed_id = this.getView().byId("selectBreed").mProperties.selectedKey;+
            console.log("Raza ",breed_id)
            fetch("/eggs_storage/findEggsStorageLots", {
                headers: {
                    "Content-Type": "application/json"
                },
                method: "POST",
                body: JSON.stringify({"breed_id" : breed_id})
            })
                .then(
                    function(response) {
                        if (response.status !== 200) {
                            console.log("Looks like there was a problem. Status Code: " +
                response.status);
                            return;
                        }
                        response.json().then(function(res) {
                            res.data.unshift({"lot": "Todos"});
                            posturecurve.setSizeLimit (res.data.length+1);
                            // console.log(posturecurve.getProperty("/lot_rep"))
                            posturecurve.setProperty("/lot_rep", res.data.map(elem => elem.lot));
                            console.log("Empleando el map ", res.data.map(elem => elem.lot))
                        });
                    }
                )
                .catch(function(err) {
                    console.log("Fetch Error: ", err);
                });
            this.getView().byId("selectLot").setSelectedItem(new item());

        },
        LoadingDays: function(oEvent){

            let breed_id = this.getView().byId("breedSelect").mProperties.selectedKey;
            let year = this.getView().byId("Selectyears").mProperties.selectedKey;
            let lot = this.getView().byId("selectLote").mProperties.selectedKey;
            this.getView().byId("idProductsTable").removeSelections();
            this.getModel("posturecurve").setProperty("/dayExecuted","");

            // console.log(oTreeTable)
            // oTreeTable.removeSelections(true)
            let posturecurve=this.getModel("posturecurve");
            let yearr = this.getView().byId("Selectyears").mProperties.selectedKey;
            posturecurve.setProperty("/theyeartolook",yearr);
            let breedd = this.getView().byId("breedSelect").mProperties.selectedKey;
            posturecurve.setProperty("/thebreedtolook",breedd);
            if (lot=="Todos"){


                posturecurve.setProperty("/table","None");
                posturecurve.setProperty("/egglots","");
                posturecurve.setProperty("/proportion","");
                posturecurve.setProperty("/enabledLinkdate",false);
                posturecurve.setProperty("/weekenabled",true);
                posturecurve.setProperty("/enabledLink",true);
                posturecurve.setProperty("/lookenabled",false);
                fetch("/eggs_storage/findAllEggsStorageView", {
                    headers: {
                        "Content-Type": "application/json"
                    },
                    method: "POST",
                    body: JSON.stringify({"breed_id" : breed_id,
                        "year" : year})
                })
                    .then(
                        function(response) {
                            if (response.status !== 200) {
                                console.log("Looks like there was a problem. Status Code: " +
                        response.status);
                                return;
                            }

                            response.json().then(function(res) {
                                let info= JSON.parse(JSON.stringify(res.data));
                                posturecurve.setProperty("/daysInf",info);

                                res.data.forEach(element =>{
                                    let ddate= new Date(element.week);
                                    // let formatDate= `${(ddate.getDate() < 10 ? '0' : '') + ddate.getDate()}/${((ddate.getMonth() + 1) < 10 ? '0' : '') + (ddate.getMonth() + 1)}/${ddate.getFullYear()}`;
                                    var jaj = new Array();
                                    jaj = element.week.split("-");
                                    jaj[2] = jaj[2][0] + jaj[2][1];
                                    //let formatDate= `${(ddate.getDate() < 10 ? '0' : '') + ddate.getDate()}/${((ddate.getMonth() + 1) < 10 ? '0' : '') + (ddate.getMonth() + 1)}/${ddate.getFullYear()}`;
                                    let formatDate = jaj[2] + "/" + jaj[1] + "/" + jaj[0];
                                    posturecurve.setProperty("/formateDate", formatDate);

                                    element.week= formatDate;
                                    element.lot = "Ver";
                                    element.eggs=parseInt(element.eggs);
                                });
                                posturecurve.setProperty("/week", res.data);
                                console.log("Consulta---->", res.data)
                                console.log("Aquiiii estoyyy!!!")
                            });
                        }
                    )
                    .catch(function(err) {
                        console.log("Fetch Error: ", err);
                    });}
            else{



                let lot_id = this.getView().byId("selectLote").getSelectedKey(),
                    breed_id = this.getView().getModel("posturecurve").getProperty("/idBreed_report"),
                    posturecurve = this.getView().getModel("posturecurve"),
                    week = this.getView().getModel("posturecurve").getProperty("/week"),
                    that = this;
                posturecurve.setProperty("/enabledLinkdate",true);
                posturecurve.setProperty("/enabledemphasizedate",false);
                posturecurve.setProperty("/enabledLink",false);
                posturecurve.setProperty("/enabledemphasize",true);
                posturecurve.setProperty("/lookenabled",true);
                posturecurve.setProperty("/weekenabled",false);

                posturecurve.setProperty("/egglots","Huevos Por Lote");
                posturecurve.setProperty("/proportion","Proporción (%)");
                posturecurve.setProperty("/table","SingleSelect");
                fetch("/eggs_storage/findEggsStorageByYearBreedLot", {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify({
                        breed_id: breed_id,
                        lot: lot_id,
                        year: year
                    })


                })
                    .then(
                        function(response) {
                            if (response.status !== 200) {
                                console.log("Looks like there was a problem. Status Code: " +
                  response.status);
                                return;
                            }
                            let nMod = new Array();
                            response.json().then(function(res) {
                                let i = 0;
                                let x;
                                // console.log(res.data)

                                res.data.forEach(element => {
                                    if(element.eggs_executed==null){
                                        x=0;
                                    }else{
                                        x=element.eggs_executed;
                                    }
                                    var jaj = new Array();
                                    jaj = element.week.split("-");
                                    jaj[2] = jaj[2][0] + jaj[2][1];
                                    element.breed=breed_id;

                                    let ddate= new Date(element.week);
                                    //let formatDate= `${(ddate.getDate() < 10 ? '0' : '') + ddate.getDate()}/${((ddate.getMonth() + 1) < 10 ? '0' : '') + (ddate.getMonth() + 1)}/${ddate.getFullYear()}`;
                                    let formatDate = jaj[2] + "/" + jaj[1] + "/" + jaj[0];
                                    posturecurve.setProperty("/formateDate", formatDate);

                                    element.formatDate = formatDate;


                                    let float = {
                                        week: element.formatDate,
                                        lot: lot,
                                        projected: element.eggs,
                                        eggs:element.week_eggs,
                                        executed: x,
                                        num_week: element.num_week,
                                        lot_eggs: parseInt(element.lot_eggs),
                                        percen: ((100*element.lot_eggs)/element.week_eggs).toFixed(2),
                                        breed:element.breed

                                    };
                                    nMod.push(float);
                                    i++;
                                });
                                that.getView().getModel("posturecurve").setProperty("/week",nMod);
                                console.log("Respuesta ",that.getView().getModel("posturecurve").getProperty("/week"))
                            });
                        }
                    );
            }

        },
        selLotReport: function(oEvent){
            let ReportType = this.getView().byId("Reporttype").mProperties.selectedKey;
            // console.log(ReportType)

            if (ReportType == "Diario"){

                let lot_id = this.getView().byId("selectLot").mProperties.selectedKey,
                    breed_id = this.getView().byId("selectBreed").mProperties.selectedKey,
                    year = this.getView().byId("Selectyears2").mProperties.selectedKey,
                    ep_id = this.getView().getModel("posturecurve").getProperty("/idEmp_report"),
                    projected= this.getView().getModel("posturecurve").getProperty("/dWeek"),
                    posturecurve = this.getView().getModel("posturecurve"),
                    that = this;
                fetch("/eggs_storage/findEggsStorageDataReport", {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify({
                        breed_id: breed_id,
                        lot: lot_id,
                        year: year
                    })


                })
                    .then(
                        function(response) {
                            if (response.status !== 200) {
                                console.log("Looks like there was a problem. Status Code: " +
                  response.status);
                                return;
                            }
                            let nMod = new Array();
                            response.json().then(function(res) {
                                // console.log(res.data)
                                let i = 0;
                                let x;
                                res.data.forEach(item =>{

                                });
                                res.data.forEach(element => {
                                    if(element.eggs_executed==null){
                                        x=0;
                                    }else{
                                        x=element.eggs_executed;
                                    }
                                    let ddate= new Date(element.init_date);
                                    let formatDate= `${(ddate.getDate() < 10 ? "0" : "") + ddate.getDate()}/${((ddate.getMonth() + 1) < 10 ? "0" : "") + (ddate.getMonth() + 1)}/${ddate.getFullYear()}`;
                                    posturecurve.setProperty("/formateDate", formatDate);

                                    element.formatDate= formatDate;
                                    let float = {
                                        formatDate: element.formatDate,
                                        lot: element.lot,
                                        projected: element.eggs,
                                        executed: x,
                                        percen: ((x*100)/element.eggs).toFixed(2)
                                    };
                                    nMod.push(float);
                                    i++;
                                });

                                console.log("Consulta ...>", res.data)

                                that.getView().getModel("posturecurve").setProperty("/weekReport",nMod);
                                that.getView().byId("exportBtn").setEnabled(true);

                            });
                        }
                    );}
            else if (ReportType == "Semanal"){

                let breed_id = this.getView().byId("selectBreed").mProperties.selectedKey;
                let year = this.getView().byId("Selectyears2").mProperties.selectedKey;
                let lot = this.getView().byId("selectLot").mProperties.selectedKey;
                this.getView().byId("idProductsTable").removeSelections();

                // console.log(oTreeTable)
                // oTreeTable.removeSelections(true)
                let posturecurve=this.getModel("posturecurve");
                let yearr = this.getView().byId("Selectyears2").mProperties.selectedKey;
                let breedd = this.getView().byId("selectBreed").mProperties.selectedKey;
                if (lot=="Todos"){

                    fetch("/eggs_storage/findAllEggsStorageView", {
                        headers: {
                            "Content-Type": "application/json"
                        },
                        method: "POST",
                        body: JSON.stringify({"breed_id" : breed_id,
                            "year" : year})
                    })
                        .then(
                            function(response) {
                                if (response.status !== 200) {
                                    console.log("Looks like there was a problem. Status Code: " +
                        response.status);
                                    return;
                                }

                                response.json().then(function(res) {
                                    // console.log(res.data)
                                    let info= JSON.parse(JSON.stringify(res.data));
                                    posturecurve.setProperty("/daysInf",info);

                                    res.data.forEach(element =>{
                                        let ddate= new Date(element.week);
                                        // let formatDate= `${(ddate.getDate() < 10 ? '0' : '') + ddate.getDate()}/${((ddate.getMonth() + 1) < 10 ? '0' : '') + (ddate.getMonth() + 1)}/${ddate.getFullYear()}`;
                                        var jaj = new Array();
                                        jaj = element.week.split("-");
                                        jaj[2] = jaj[2][0] + jaj[2][1];
                                        //let formatDate= `${(ddate.getDate() < 10 ? '0' : '') + ddate.getDate()}/${((ddate.getMonth() + 1) < 10 ? '0' : '') + (ddate.getMonth() + 1)}/${ddate.getFullYear()}`;
                                        let formatDate = jaj[2] + "/" + jaj[1] + "/" + jaj[0];
                                        posturecurve.setProperty("/formateDate", formatDate);

                                        element.formatDate= formatDate;
                                        element.projected=element.eggs;
                                        element.executed=element.eggs_executed;
                                        element.percen= ((100* element.eggs_executed)/ element.projected).toFixed(2);

                                        element.lot="Todos";
                                        element.eggs=parseInt(element.eggs);
                                    });
                                    posturecurve.setProperty("/weekReport", res.data);

                                });
                            }
                        )
                        .catch(function(err) {
                            console.log("Fetch Error: ", err);
                        });}
                else{



                    let breed_id = this.getView().byId("selectBreed").mProperties.selectedKey;
                    let year = this.getView().byId("Selectyears2").mProperties.selectedKey;
                    let lot_id = this.getView().byId("selectLot").mProperties.selectedKey,
                        posturecurve = this.getView().getModel("posturecurve"),
                        week = this.getView().getModel("posturecurve").getProperty("/week"),
                        that = this;



                    // posturecurve.setProperty("/egglots","Huevos Por Lote");
                    // posturecurve.setProperty("/proportion","Proporción (%)");
                    // posturecurve.setProperty("/table","SingleSelect");
                    fetch("/eggs_storage/findEggsStorageByYearBreedLot", {
                        method: "POST",
                        headers: {
                            "Content-Type": "application/json"
                        },
                        body: JSON.stringify({
                            breed_id: breed_id,
                            lot: lot_id,
                            year: year
                        })


                    })
                        .then(
                            function(response) {
                                if (response.status !== 200) {
                                    console.log("Looks like there was a problem. Status Code: " +
                  response.status);
                                    return;
                                }
                                let nMod = new Array();
                                response.json().then(function(res) {
                                    let i = 0;
                                    let x;
                                    // console.log(res.data)

                                    res.data.forEach(element => {
                                        if(element.eggs_executed==null){
                                            x=0;
                                        }else{
                                            x=element.eggs_executed;
                                        }
                                        var jaj = new Array();
                                        jaj = element.week.split("-");
                                        jaj[2] = jaj[2][0] + jaj[2][1];
                                        element.breed=breed_id;

                                        let ddate= new Date(element.week);
                                        //let formatDate= `${(ddate.getDate() < 10 ? '0' : '') + ddate.getDate()}/${((ddate.getMonth() + 1) < 10 ? '0' : '') + (ddate.getMonth() + 1)}/${ddate.getFullYear()}`;
                                        let formatDate = jaj[2] + "/" + jaj[1] + "/" + jaj[0];
                                        posturecurve.setProperty("/formateDate", formatDate);

                                        element.formatDate = formatDate;

                                        let float = {
                                            formatDate: element.formatDate,
                                            lot: lot,
                                            projected: element.eggs,
                                            eggs:element.week_eggs,
                                            executed: x,
                                            num_week: element.num_week,
                                            projected: parseInt(element.lot_eggs),
                                            percen:((100* element.eggs_executed)/ element.lot_eggs).toFixed(2),
                                            breed:element.breed

                                        };
                                        nMod.push(float);
                                        i++;
                                    });
                                    console.log("Consulta primaria", res.data)
                                    that.getView().getModel("posturecurve").setProperty("/weekReport",nMod);

                                });
                            }
                        );
                }

            }
            else{
                let empty = new Array();
                this.getView().getModel("posturecurve").setProperty("/weekReport",empty);
            }

        },


        onTabSelection: function(oEvent){
            // this.getView().getModel("posturecurve").setProperty("/weekReport","")
            // this.getView().getModel("posturecurve").setProperty("/lot_rep","")
            // this.getView().getModel("posturecurve").setProperty("/dayExecuted",[]);
            // this.getView().byId("selecLot").setSelectedItem(new item())
            // this.getView().byId("breedSelect").setSelectedItem(new item())
            // this.getView().byId("selRazaReport").setSelectedItem(new item())
            // this.getView().byId("selLotReport").setSelectedItem(new item())
            // this.getView().byId("selectBreed").setSelectedItem(new item())

            var el=oEvent.getParameters().selectedItem.mProperties.text;
            let vacio = [];
            if(el!="Ejecutado"){

                this.getView().byId("executionSaveBtn").setEnabled(false);
                this.getView().byId("executionSaveBtn").setVisible(false);
                // this.getView().byId("ModifiyBtn").setEnabled(false)
                // this.getView().byId("ModifiyBtn").setVisible(false)


            }else{
                this.getView().byId("executionSaveBtn").setVisible(true);
                // this.getView().byId("ModifiyBtn").setVisible(true)
            }
            if(el=="Reportes"){
                this.onBreedLoad();
                this.getView().byId("exportBtn").setVisible(true);
                this.getView().byId("exportBtn").setEnabled(true);


            }else{
                this.getView().byId("exportBtn").setVisible(false);

            }
            // if(el!="Proyectado"&&el!="Ejecutado"){
            //   this.getView().getModel("posturecurve").setProperty("/week",[]);
            // }
        },
        onDialogPressEx: function(){
            var aux= this.getView().getModel("posturecurve");
            aux.setProperty("/input",false);
            // this.getView().byId("ModifiyBtn").setEnabled(true)
            this.getView().byId("executionSaveBtn").setVisible(true);
            this.getView().byId("executionSaveBtn").setEnabled(true);
            // debugger;

            let posturecurve= this.getModel("posturecurve");

            let jsonSelect= posturecurve.getProperty("/savingthedate");
            let updatedjson= posturecurve.getProperty("/returnupdate");
            // console.log({updatedjson})

            // var model= aux.dayExecuted
            let arr = aux.oData.dayExecuted;
            // console.log({arr})
            let arrayTemp = new Array();
            let update= [];
            arr.forEach(element => {
                if(element.eggs_executed!= null){
                    let jsonTemp =
            { "eggs_executed": element.eggs_executed,
                "init_date": element.dia,
                "lot": element.lot };
                    update.push(jsonTemp);
                }
            });
            // console.log(update)
            // console.log(jsonSelect)

            // "posturecurveday_id": element.posturecurveday_id,
            // "week_id": element.week_id,
            // "executed": parseInt(element.executed),
            // "day_of_week": element.day_of_week


            let formatDate = jsonSelect.init_date.split("/"),
                that = this;
            // console.log(formatDate)
            fetch("/eggs_storage/updateEggsExecuted", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    records: update,
                    init_date_start: formatDate[2] + "-" + formatDate[1] + "-" + formatDate[0],

                    breed_id: updatedjson.breed_id,
                    year:updatedjson.year,
                    lot: updatedjson.lot,
                    num_week: updatedjson.num_week


                })


            })
                .then(
                    function(response) {
                        if (response.status !== 200) {
                            console.log("Looks like there was a problem. Status Code: " +
                response.status);
                            return;
                        }

                        response.json().then(function(res) {
                            // console.log(res.data)
                            var dialog = new Dialog({
                                title: "Información",
                                type: "Message",
                                state: "Success",
                                content: new Text({
                                    text: "Información guardada con éxito."
                                }),
                                beginButton: new Button({
                                    text: "OK",
                                    press: function() {
                                        res.data.forEach(item=>{
                                            let formatDate= item.dia.split("-");

                                            posturecurve.setProperty("/formateDate", formatDate);
                                            item.available = (item.eggs_executed==null || item.eggs_executed< 0)? true: false;
                                            item.dia = formatDate[2]+"/"+formatDate[1]+"/"+formatDate[0];
                                        });
                                        // console.log(res.data)
                                        posturecurve.setProperty("/dayExecuted", res.data);
                                        that.getView().byId("executionSaveBtn").setEnabled(false);
                                        dialog.close();
                                    }
                                }),
                                afterClose: function() {
                                    dialog.destroy();
                                }
                            });

                            dialog.open();
                        });
                    });
        },
        onChange:function(value){
            // var modelo=this.getView().getModel("posturecurve");
            let input= value.getSource();
            let valuee = input.getValue();


            // let cant=value.getSource();
            // console.log(value)
            // console.log(valuee)
            if(valuee!==null &&valuee>=0&&(valuee%1===0)){
                this.getView().byId("executionSaveBtn").setVisible(true);
                this.getView().byId("executionSaveBtn").setEnabled(true);
            }else{
                this.getView().byId("executionSaveBtn").setEnabled(false);
            }



        },

        onViewCurve2: function(oEvent) {
            var aux= this.getView().getModel("posturecurve");
            var duck = oEvent.getParameter("listItem").getBindingContext("posturecurve").getPath();
            let week_id= aux.getProperty(duck).week_id;
            let lote= aux.getProperty(duck).lot;


            fetch("/posturecurveDaily/findDaysByWeek", {
                method: "POST",
                headers: {
                    "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
                },

                body: "week_id="+week_id
            })
                .then(
                    function(response) {
                        if (response.status !== 200) {
                            console.log("Looks like there was a problem. Status Code: " +
                response.status);
                            return;
                        }

                        response.json().then(function(res) {
                            // console.log(res.data);

                            aux.setProperty("/dayExecuted", res.data);
                            aux.setProperty("/dataOld", res.data);
                            // console.log(posturecurve.getProperty("/dayExecuted"));
                        });
                    }
                );






            // let lot=oEvent.getParameters().listItem.mAggregations.cells[1].mProperties.text;
            aux.setProperty("/lotaux", lote);

            //  console.log("sssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss");
            // console.log(week_id);
            // let eggs=oEvent.getParameters().listItem.mAggregations.cells[2].mProperties.number.replace('.', "");;
            // let eggsForDay=parseInt(eggs/7,10);


            //   let report = new Array();
            //  for (let i = 1; i <= 7; i++) {
            //   report.push({
            //     day:i,
            // 		lot:lot,
            // 		eggsForDay:eggsForDay
            //   })
            //   this.getView().getModel("posturecurve").setProperty("/day",report);
            //   }

            var oPage = this.getView().byId("tabProjected");
            var oFormFragment = sap.ui.xmlfragment("annualPostureCurve.view.postureCurve.PostureCurveTableReal2",this);
            // oPage.setVisible(false)
            this.dummy=oPage.getContent()[1];
            oPage.removeAllContent();
            oPage.insertContent(oFormFragment);
        } ,
        onBreedLoad: function() {
            const util = this.getModel("util"),
                serverName = "/breed/findAllBreedWP" ;
            // console.log(serverName);
            let mdbreed = this.getModel("mdbreed"),
                that = this;
            mdbreed.setProperty("/records", []);

            let isRecords = new Promise((resolve, reject) => {
                fetch(serverName)
                    .then(
                        function(response) {
                            if (response.status !== 200) {

                                console.log("Looks like there was a problem. Status Code: " +
                  response.status);
                                return;
                            }
                            // Examine the text in the response
                            response.json().then(function(res) {
                                resolve(res);
                            });
                        }


                    )

                    .catch(function(err) {
                        console.log("Fetch Error :-S", err);
                    });
            });


            isRecords.then((res) => {
                if (res.data.length > 0) {
                    mdbreed.setProperty("/records", res.data);
                    mdbreed.setProperty("/value", mdbreed.getProperty("/records/0/breed_id"));
                    that.getView().byId("breedSelect").setSelectedItem({});
                }
            });
        },


        onIncubatorPlant: function(){
            //  console.log("Entre");
            let util = this.getModel("util"),
                partnership_id = this.getView().getModel("ospartnership").getProperty("/records/" + this.index + "/partnership_id"),
                that = this;

            const serverName = util.getProperty("/serviceUrl") + util.getProperty("/" + util.getProperty("/service") + "/findIncPlantByPartnetship");

            return new Promise((resolve, reject)=>{
                fetch(serverName, {
                    method: "POST",
                    headers: { "Content-Type": "application/json" },
                    body: JSON.stringify({
                        partnership_id: partnership_id
                    })
                })
                    .then(
                        function(response) {
                            if (response.status !== 200) {
                                console.log("Looks like there was a problem. Status Code: " +
                 response.status);
                                return;
                            }

                            response.json().then(function(res) {
                                //  console.log("Buscando incubadora: ", res);

                                resolve(res);
                            });
                        }
                    )
                    .catch(function(err) {
                        console.log("Fetch Error :-S", err);
                    });
            });

        },


        activeScenario: function(){

            let util = this.getModel("util"),
                mdscenario = this.getModel("posturecurve"),
                that = this;
            const serverName = "/scenario/activeScenario";
            // const serverName = util.getProperty("/serviceUrl") + util.getProperty("/" + util.getProperty("/service") + "/activeScenario");

            return new Promise((resolve, reject)=>{
                fetch(serverName)
                    .then(
                        function(response) {
                            if (response.status !== 200) {
                                console.log("Looks like there was a problem. Status Code: " +
                response.status);
                                return;
                            }

                            response.json().then(function(res) {
                                resolve(res);
                            });
                        }
                    )
                    .catch(function(err) {
                        console.log("Fetch Error :-S", err);
                    });

            });
        }



    });
});
