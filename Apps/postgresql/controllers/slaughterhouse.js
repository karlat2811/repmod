const DBlayer = require("../models/slaughterhouse");

exports.findAllSlaughterhouse = function(req, res) {
    DBlayer.DBfindAllslaughterhouse()
        .then(function(data) {
            // console.log(data);
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
            res.status(500).send(err);
        });
};

exports.addSlaughterhouse = function(req, res) {
    DBlayer.DBaddslaughterhouse(req.body.name, req.body.description,req.body.address, req.body.code, req.body.capacity )
        .then(function(data) {
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
        //   console.log(err);
            res.status(500).send(err);
        });
};


exports.updateslaughterhouse = function(req, res) {
//   console.log(req.body);
    DBlayer.DBupdateslaughterhouse(req.body.name, req.body.description,req.body.address, req.body.slaughterhouse_id, req.body.code, req.body.capacity)
        .then(function(data) {
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
        //   console.log(err);
            res.status(500).send(err);
        });
};


exports.deleteslaughterhouse = function(req, res) {
    DBlayer.DBdeleteslaughterhouse(req.body.slaughterhouse_id)
        .then(function(data) {
            res.status(200).json({
                statusCode: 200,
                mgs: "Empresa Eliminada"
            });
        })
        .catch(function(err) {
        //   console.log(err);
            res.status(500).send(err);
        });
};
