const DBlayer = require("../models/broilerProduct");

exports.findAllBroilerProduct = async function(req, res) {

    try {
        let results = await DBlayer.DBfindAllBroilerProduct();
        res.status(200).json({
            statusCode: 200,
            data: results
        });

    } catch (err) {
        console.log(err);
        res.status(500).json({
            statusCode: 500,
            pgErrorCode: err
        });
    }
};

exports.addBroilerProduct = async function(req, res) {

    try {
        let name = req.body.name;
        let weight = req.body.weight;
        let day = req.body.days_eviction;
        let code = req.body.code;
        let results = await DBlayer.DBaddBroilerProduct(name, weight, day, code);

        res.status(200).json({
            statusCode: 200,
            data: results
        });

    } catch (err) {
        console.log(err);
        res.status(500).json({
            statusCode: 500,
            pgErrorCode: err
        });
    }
};


exports.updateBroilerProduct = async function(req, res) {

    try {
        let name = req.body.name,
            broiler_product_id = req.body.broiler_product_id;
        weight= req.body.weight;
        day= req.body.days_eviction;
        code = req.body.code;
        console.log(broiler_product_id, name , weight, day, code);
        let results = await DBlayer.DBupdateBroilerProduct(broiler_product_id, name, weight, day, code);

        res.status(200).json({
            statusCode: 200,
            data: results
        });

    } catch (err) {
        res.status(500).json({
            statusCode: 500,
            pgErrorCode: err
        });
    }
};


exports.deleteBroilerProduct = async function(req, res) {

    try {
        let broiler_product_id = req.body.broiler_product_id;

        let results = await DBlayer.DBdeleteBroilerProduct(broiler_product_id);

        res.status(200).json({
            statusCode: 200,
            data: results
        });

    } catch (err) {
        res.status(500).json({
            statusCode: 500,
            pgErrorCode: err
        });
    }
};

exports.isBeingUsed = function(req, res) {
    DBlayer.DBisBeingUsed(req.body.broiler_product_id)
        .then(function(data) {
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
            console.log(err);
            res.status(500).send(err);
        });
};
