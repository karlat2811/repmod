const DBlayer = require("../models/Parameter");
const DBprocess = require("../models/process");
const DBmeasure = require("../models/measure");
const utils = require("../../lib/utils");
/*
  Funcion POST : Se encarga de insertar un parametro a la DB
  Param: calendar_id Se envia en el cuerpo
*/
exports.findAllParameter = function(req, res) {

    DBlayer.DBfindAllParameter(req, res)
        .then(function(data) {
            res.status(200).json({
                status: "sucess",
                data: data
            });
        })
        .catch(function(err) {
            res.status(500).send(err);
        });
};

exports.findParameterByType = function(req, res) {
    DBlayer.DBfindParameterByType(req.params.type)
        .then(function(data) {
            res.status(200).json({
                status: "sucess",
                data: data
            });
        })
        .catch(function(err) {
            res.status(500).send(err);
        });
};

exports.findParameter = function(req, res) {

    DBlayer.DBfindParameter()
        .then(function(data) {
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
            res.status(500).send(err);
        });
};

exports.addParameter = function(req, res) {
    console.log("addParameter");

    DBlayer.DBaddParameter(req.body)
        .then(function(data) {
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
        //   console.log(err);
            res.status(500).send(err);
        });
};

exports.updateParameter = function(req, res) {

    DBlayer.DBupdateParameter(req.body)
        .then(function(data) {
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
            res.status(500).send(err);
        });
};

exports.deleteParameter = function(req, res) {
    DBlayer.DBdeleteParameter(req.body.parameter_id)
        .then(function(data) {
            res.status(200).json({
                statusCode: 200
            });
        })
        .catch(function(err) {
            res.status(500).send(err);
        });
};


exports.changeName = function(req, res) {

    let name = req.body.name;
    let excep = req.body.diff;
    DBlayer.DBcheckNameParameter(name,excep)
        .then(function(data){
            console.log("result: ", data);
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
            console.log(err);
            res.status(500).send(err);
        });
};

exports.bulkAddParameter = utils.wrap(async function(req, res) {
    const parameters = req.body.registers;
    const procesos = await DBprocess.DBKnowProcessid2(parameters);
});

exports.bulkAddParameter = function(req, res) {
    let J = 0;
    let band = false;
    DBprocess.DBKnowProcessid(req.body.registers).then(function (Processid) {
        if (Processid.length > 0) {
            for (let index = 0; index < req.body.registers.length; index++) {
                while ( J < Processid.length && !band) {
                    if ( req.body.registers[index].process_id == Processid[J].name) {
                        req.body.registers[index].process_id = Processid[J].process_id;
                        band = true;
                    }
                    J++; 
                }
                band = false;
                J = 0;
            }

            band = false;
            J = 0;
            index = 0;
            DBmeasure.DbKnowmeasure_id(req.body.registers).then(function (measureid) {
                for (let index = 0; index < req.body.registers.length; index++) {
                    while (J < measureid.length && !band) {
                        if (req.body.registers[index].measure_id == measureid[J].name) {
                            req.body.registers[index].measure_id = measureid[J].measure_id;
                            band = true;
                        }
                        J++;
                    }
                    band = false;
                    J = 0;
                }

                let parameter = req.body.registers;
                utils.cleanObjects(parameter);
                DBlayer.DBbulkAddparameter(parameter).then(function (data) {
                    res.status(200).json({
                        statusCode: 200,
                        data: data
                    });
                }).catch(function (err) {
                    console.log(err);
                    res.status(500).send(err);
                });
            });

        }else{
            res.status(401).json({
                statusCode: 401
            });
        }
    }).catch(function (err) {
        console.log(err);
        res.status(500).send(err);
    });
};
