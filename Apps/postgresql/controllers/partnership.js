const DBlayer = require("../models/partnership");
const utils = require("../../lib/utils");


/*
Funcion GET que devuelve todos las empresas
*/
exports.findAllPartnership = function(req, res) {
    DBlayer.DBfindAllPartnership()
        .then(function(data) {
            console.log("La data de partnerships: ",data);
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
            res.status(500).send(err);
        });
};
exports.findPartnershipByFarmType = function(req, res, next) {
 
    DBlayer.DBfindPartnershipByFarmType(req.body.farm_type)
        .then(function(data) {
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
            res.status(500).send(err);
        });
};

exports.bulkAddPartnership = function(req, res) {
    //console.log(req.body.registers);
    let partnerships = req.body.registers;
    utils.cleanObjects(partnerships);
    DBlayer.DBbulkAddPartnership(partnerships).then(function(data){
        res.status(200).json({
            statusCode: 200,
            data: data
        });
    }).catch(function(err){
        console.log(err);
        res.status(500).send(err);
    });
};

exports.bulkAddPartnership2 = utils.wrap(async function(req, res) {
    let partnerships = req.body.registers;
    const dbPartnerships = await DBlayer.DBfindAllPartnership()
    const errors = []

    utils.cleanObjects(partnerships);

    for (const partnership of partnerships) {
        const partnershipMatch = dbPartnerships.find(p => partnership.code === p.code)
        if (partnershipMatch) {
            errors.push({object: partnership, message: `El código de empresa ${partnership.code} ya existe`})
        }
    }
    
    if (errors.length > 0) {
        throw new Error(errors[0].message)
    }

    const result = await DBlayer.DBbulkAddPartnership(partnerships);
    
    res.status(200).json({
        statusCode: 200,
        data: result
    });

});

exports.addPartnership = function(req, res) {
    DBlayer.DBaddPartnership(req.body.name, req.body.description,req.body.address, req.body.code, req.body.os_disable  )
        .then(function(data) {
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
        //   console.log(err);
            res.status(500).send(err);
        });
};

exports.findFarmByPartnership = function(req, res) {
//   console.log(req.body.partnership_id);
    DBlayer.DBfindFarmByPartnership(req.body.partnership_id)
        .then(function(data) {
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
        //   console.log(err);
            res.status(500).send(err);
        });
};

exports.updatePartnership = function(req, res) {
//   console.log(req.body);
    DBlayer.DBupdatePartnership(req.body.name, req.body.description,req.body.address, req.body.partnership_id, req.body.code, req.body.os_disable)
        .then(function(data) {
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
        //   console.log(err);
            res.status(500).send(err);
        });
};


exports.deletePartnership = function(req, res) {
    DBlayer.DBdeletePartnership(req.body.partnership_id)
        .then(function(data) {
            res.status(200).json({
                statusCode: 200,
                mgs: "Empresa Eliminada"
            });
        })
        .catch(function(err) {
        //   console.log(err);
            res.status(500).send(err);
        });
};

exports.findIdByCode = function(req, res) {
    console.log(req.body.partnership_code);
    DBlayer.DBfindPartnershipByCode(req.body.partnership_code)
        .then(function(data) {
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
            console.log(err);
            res.status(500).send(err);
        });
};
exports.isBeingUsed = function(req, res) {
    console.log(req.body.partnership_code);
    DBlayer.DBisBeingUsed(req.body.partnership_id)
        .then(function(data) {
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
            console.log(err);
            res.status(500).send(err);
        });
};