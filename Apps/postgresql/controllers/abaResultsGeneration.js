/*const DBbroiler_product = require('../models/broilerProduct');
 const DBslaughterhouse = require('../models/slaughterhouse');*/
var axios = require("axios");
const puerto = 3001;//backend abaARP
const puerto2 = 3009;//backend ARP sin aba
const urlAbaElements = "abaElements";
const urlAbaElementsProperties = "abaElementsProperties";
const urlAbaFormulation = "abaFormulation";
const urlAbaConsumptionAndMortality = "abaConsumptionAndMortality";
const urlAbaElementsAndConcentrations = "abaElementsAndConcentrations";
const urlAbaResults = "abaResults";
const urlAbaBreedsAndStages = "abaBreedsAndStages";
const urlAbaConsumptionAndMortalityDetail = "abaConsumptionAndMortalityDetail";
const urlAbaStagesOfBreedsAndStages = "abaStagesOfBreedsAndStages";
const urlAbaAlimentsAndStages = "abaAlimentsAndStages";
const urlGoalsResults = "scenario_param/getGoalsResultsDemandSum";


exports.generateResults = async function (req, res) {

    try {
        //obtengo el scenario activo, por ahora hardcodeo
        let activeScenario = 135;
        //obtengo salida de la regresiva, por ahora un solo escenario
        allDemandData = await getGoalsResults();

        //aqui armo estructura similar a sincronizacion con gets en paralelo
        let allTheData = await Promise.all([getAbaElements(), getAbaElementsProperties(),
            getAbaFormulation(), getAbaConsumptionAndMortality(), getAbaElementsAndConcentrations(),
            getAbaBreedsAndStages(), getAbaConsumptionAndMortalityDetail(),
            // getAbaStagesOfBreedsAndStages(), getAbaAlimentsAndStages()]);
            getAbaStagesOfBreedsAndStages()]);

        var resultMap = new Map();

        for (let demandRow of allDemandData) {
            //encuentro el breedAndStage con ese producto
            let currentBreedAndStage = allTheData[5].filter(breedAndStage => breedAndStage.id_product == demandRow.product_id);
            //ahora busco los stages de ese BreedAndStages en StagesOfBreedsAndStages
            if (currentBreedAndStage.length > 0) {

                let stagesOfCurrentBreedAndStage = allTheData[7].filter(id_aba_breeds_and_stages => id_aba_breeds_and_stages.id_aba_breeds_and_stages == currentBreedAndStage[0].id);
                //aqui tomo la demanda, la demanda la tomo con el id de consumo y mortalidad que esta en breedsandstages
                let consAndMortalityOfCurrentBreedAndStage = allTheData[3].filter(consumptionAndMortality => consumptionAndMortality.id == currentBreedAndStage[0].id_aba_consumption_and_mortality);
                //ahora pido toda la demanda por unidad de tiempo con ese id
                let consAndMortalityWithTimeOfCurrentBreedAndStage = allTheData[6].filter(consumptionAndMortalityDetail => consumptionAndMortalityDetail.id_aba_consumption_and_mortality == consAndMortalityOfCurrentBreedAndStage[0].id);
                //ya tengo las fases, ahora itero sobre ellas
                for (let stage of stagesOfCurrentBreedAndStage) {
                    //obtengo los elementos en elementsAndConcentrations con el idFormulation del Stage
                    let currentFormulationElements = allTheData[4].filter(formulation => formulation.id_aba_formulation == stage.id_formulation);
                    //ahora itero sobre sus duraciones
                    //usar el consumo de ese breedandstage y el porcentaje en funcion al elemento
                    let sumOfTimes = 0;
                    for (i = 0; i < stage.duration; i++) {
                        //ahora sobre los elementos de la formula en esa fase
                        for (let elementAndProportions of currentFormulationElements){
                            if(!resultMap.has(elementAndProportions.id_aba_element)){
                                resultMap.set(elementAndProportions.id_aba_element, 0);
                            }
                            //consumo en ese momento * proporcion del elemento dentro de 1g de formula * demanda
                            //le tengo que pegar al consumptionAndMortalityDetail allTheData[6] a partir de breedsAndStages
                            //asumo que viene pre ordenado, todo llevar cuenta del dia actual a efectos del array o ir sumando
                            let temp = elementAndProportions.proportion * consAndMortalityWithTimeOfCurrentBreedAndStage[sumOfTimes+i].consumption
                            * demandRow.demand;
                            //todo agrego cantidad de ese tiempo al vector con productos
                            // resultMap.get(elementAndProportions.id_aba_element);
                            resultMap.set(elementAndProportions.id_aba_element, resultMap.get(elementAndProportions.id_aba_element) + temp);
                        }

                        //ya paso una unidad de tiempo asi que mato a los pollos
                        demandRow.demand = demandRow.demand - (demandRow.demand * consAndMortalityWithTimeOfCurrentBreedAndStage[sumOfTimes+i].mortality/100);
                    }
                    sumOfTimes += stage.duration;
                }
            }
        }

        console.log(resultMap);


        res.status(200).json({
            statusCode: 200
        });
    }
    catch
    (err) {
        console.log(err);
        res.status(500).json({
            statusCode: 500,
            pgErrorCode: err
        });
    }
}
;

async function getAbaElements() {
    let respuesta;
    var url = "http://localhost:" + puerto + "/" + urlAbaElements;
    try {
        respuesta = await axios.get(url);
        return respuesta.data.data;
    } catch (e) {
        console.log("Error");
    }
}

async function getAbaElementsProperties() {
    let respuesta;
    var url = "http://localhost:" + puerto + "/" + urlAbaElementsProperties;
    try {
        respuesta = await axios.get(url);
        return respuesta.data.data;
    } catch (e) {
        console.log("Error");
    }
}

async function getAbaFormulation() {
    let respuesta;
    var url = "http://localhost:" + puerto + "/" + urlAbaFormulation;
    try {
        respuesta = await axios.get(url);
        return respuesta.data.data;
    } catch (e) {
        console.log("Error");
    }
}

async function getAbaConsumptionAndMortality() {
    let respuesta;
    var url = "http://localhost:" + puerto + "/" + urlAbaConsumptionAndMortality;
    try {
        respuesta = await axios.get(url);
        return respuesta.data.data;
    } catch (e) {
        console.log("Error");
    }
}

async function getAbaElementsAndConcentrations() {
    let respuesta;
    var url = "http://localhost:" + puerto + "/" + urlAbaElementsAndConcentrations;
    try {
        respuesta = await axios.get(url);
        return respuesta.data.data;
    } catch (e) {
        console.log("Error");
    }
}

async function getAbaResults() {
    let respuesta;
    var url = "http://localhost:" + puerto + "/" + urlAbaResults;
    try {
        respuesta = await axios.get(url);
        return respuesta.data.data;
    } catch (e) {
        console.log("Error");
    }
}

async function getAbaBreedsAndStages() {
    let respuesta;
    var url = "http://localhost:" + puerto + "/" + urlAbaBreedsAndStages;
    try {
        respuesta = await axios.get(url);
        return respuesta.data.data;
    } catch (e) {
        console.log("Error");
    }
}

async function getAbaConsumptionAndMortalityDetail() {
    let respuesta;
    var url = "http://localhost:" + puerto + "/" + urlAbaConsumptionAndMortalityDetail;
    try {
        respuesta = await axios.get(url);
        return respuesta.data.data;
    } catch (e) {
        console.log("Error");
    }
}

async function getAbaStagesOfBreedsAndStages() {
    let respuesta;
    var url = "http://localhost:" + puerto + "/" + urlAbaStagesOfBreedsAndStages;
    try {
        respuesta = await axios.get(url);
        return respuesta.data.data;
    } catch (e) {
        console.log("Error");
    }
}

// async function getAbaAlimentsAndStages() {
//     let respuesta;
//     var url = 'http://localhost:' + puerto + '/' + urlAbaAlimentsAndStages;
//     try {
//         respuesta = await axios.get(url);
//         return respuesta.data.data;
//     } catch (e) {
//         console.log("Error")
//     }
// }

async function getGoalsResults() {
    let respuesta;
    var url = "http://localhost:" + puerto2 + "/" + urlGoalsResults;
    try {
        respuesta = await axios.get(url);
        return respuesta.data.data;
    } catch (e) {
        console.log("Error");
    }
}