const DBlayer = require("../models/shed");
const utils = require("../../lib/utils");
const DBpartnership = require("../models/partnership");
const DBfarm = require("../models/farm");
const DBcenter = require("../models/center");
const DBshedStatus = require("../models/shedStatus");
const DBfarmType = require("../models/farmType");
const status_disponible= 1;
const status_ocupado= 2;
const status_vacio= 3;
const status_inhabiliado= 4;

exports.findAllShed = function(req, res) {

    DBlayer.DBfindAllShed()
        .then(function(data) {
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
            res.status(500).send(err);
        });
};


exports.bulkAddShed2 = utils.wrap(async function(req, res) {
    const sheds = req.body.registers;
    const centers = await DBcenter.DbKnowCenterId2(sheds);
    const types = await DBfarmType.DbKnowFarmTypeShed2(sheds);
    const dbStatus = await DBshedStatus.DbKnowShedStatus2(sheds);
    const errors = [];

    for (const shed of sheds) {

        const centerMatch = centers.find(center => 
            shed.farmCode === center.farm_code 
      && shed.partnershipCode === center.partnership_code
      && shed.centerCode === center.center_code
        );
        if (centerMatch !== undefined) {
            shed.farm_id = centerMatch.farm_id;
            shed.partnership_id = centerMatch.partnership_id;
            shed.center_id = centerMatch.center_id;
        }
        else {
            errors.push({object: shed, message: `la combinacion de empresa: ${shed.partnershipCode}, granja : ${shed.farmCode} y centro : ${shed.centerCode} no existe`});
        }

        

        const typeMatch = types.find(type => type.name === shed.type);
        if (typeMatch !== undefined) {
            shed.type_id = typeMatch.type_id;
        }
        else if (shed.type){
            errors.push({object: shed, message: `el tipo de galpon : ${shed.type} no existe`});
        }

        const statusMatch = dbStatus.find(status => status.name === shed.statusShed)
        if (statusMatch !== undefined) {
        shed.statusshed_id = statusMatch.shed_status_id
        }
        else {
        errors.push({object: shed, message: `el estatus : ${shed.statusShed} no existe`})
        }

        const duplicatedShed = centers.find(c => 
            c.partnership_code === shed.partnershipCode 
            && c.farm_code === shed.farmCode
            && c.center_code === shed.centerCode
            && c.shed_code === shed.code
        )
        if (duplicatedShed !== undefined) {
            errors.push({object: shed, message: `La combinacion de empresa: ${shed.partnershipCode}, granja: ${shed.farmCode}, nucleo: ${shed.centerCode} y galpon: ${shed.code} ya existe`})
        }

    }
  
  if (errors.length > 0) {
     throw new Error(errors[0].message);
  }


  utils.cleanObjects(sheds);
  const result = await DBlayer.DBbulkAddShed(sheds)
  res.status(200).json({
    statusCode: 200,
    data: result
  })
})

exports.findCenterByFarm = function(req, res) {
    console.log("llegue",req.body.farm_id);
    DBlayer.DBfindCenterByFarm(req.body.farm_id)
    .then(function(data) {
      console.log("data sheds", data)
      res.status(200).json({
        statusCode: 200,
        data: data,
        asd: "asd"
      });
    })
    .catch(function(err) {
      console.log(err);
      res.status(500).send(err);
    });
 };
 


 
//copiar
 exports.findCenterByFarmAll= function(req, res) {
    console.log("llegue",req.body.farm_id);
    DBlayer.DBfindCenterByFarmAll(req.body.partnership_id,req.body.farm_id)
    .then(function(data) {
      console.log("data sheds", data)
      res.status(200).json({
        statusCode: 200,
        data: data,
        asd: "asd"
      });
    })
    .catch(function(err) {
      console.log(err);
      res.status(500).send(err);
    });
 };
 

exports.bulkAddShed = function (req, res) {
    let J = 0;
    let band = false;
    DBpartnership.DbKnowPartnership_id(req.body.registers).then(function (pa_id) {
        console.log("el body");
        for (let index = 0; index < req.body.registers.length; index++) {
            while (J < pa_id.length && !band) {
                if (req.body.registers[index].partnership_id == pa_id[J].code) {
                    req.body.registers[index].partnership_id = pa_id[J].partnership_id;
                    band = true;
                }
                J++;
            }
            band = false;
            J = 0;
        }
        band = false;
        J = 0;
        DBfarm.DbKnowFarmId(req.body.registers).then(function (farm_id) {
            for (let index = 0; index < req.body.registers.length; index++) {
                while (J < farm_id.length && !band) {
                    if (req.body.registers[index].farm_id == farm_id[J].code) {
                        req.body.registers[index].farm_id = farm_id[J].farm_id;
                        band = true;
                    }
                    J++;
                }
                band = false;
                J = 0;
            }
            band = false;
            J = 0;
            DBcenter.DbKnowCenterId(req.body.registers).then(function (center_id) {
                for (let index = 0; index < req.body.registers.length; index++) {
                    while (J < center_id.length && !band) {
                        if (req.body.registers[index].center_id == center_id[J].code 
              && req.body.registers[index].partnership_id  == center_id[J].partnership_id
               && req.body.registers[index].farm_id  == center_id[J].farm_id ) {
                            req.body.registers[index].center_id = center_id[J].center_id;
                            band = true;
                        }

                        J++;
                    }
                    if (!band) {

                        console.log("No hubo coincidencia");
                        console.log(`farm : ${req.body.registers[index].farm_id}
                           center : ${req.body.registers[index].center_id}
                           code : ${req.body.registers[index].code}`);
                        req.body.registers.splice(index, 1);
                    }

                    band = false;
                    J = 0;
                }
                band = false;
                J = 0;
                console.log("victor llego hasta aqui ");
                DBfarmType.DbKnowFarmTypeShed(req.body.registers).then(function (type_id) {
                    for (let index = 0; index < req.body.registers.length; index++) {
                        while (J < type_id.length && !band) {
                            if (req.body.registers[index].type_id == type_id[J].name) {
                                req.body.registers[index].type_id = type_id[J].farm_type_id;
                                band = true;
                            }
                            J++;
                        }
                        band = false;
                        J = 0;
                    }
                    band = false;
                    J = 0;
                    DBshedStatus.DbKnowShedStatus(req.body.registers).then(function (shedStatus) {
                        for (let index = 0; index < req.body.registers.length; index++) {
                            while (J < shedStatus.length && !band) {
                                if (req.body.registers[index].statusshed_id == shedStatus[J].name) {
                                    req.body.registers[index].statusshed_id = shedStatus[J].shed_status_id;
                                    band = true;
                                }
                                J++;
                            }
                            band = false;
                            J = 0;
                        }
                        let sheds = req.body.registers;
                        utils.cleanObjects(sheds);
                        console.log("adriana se quedo aqui");
                        DBlayer.DBbulkAddShed(sheds).then(function (data) {
                            res.status(200).json({
                                statusCode: 200,
                                data: data
                            });
                        }).catch(function (err) {
                            console.log(err);
                            res.status(500).send(err);
                        });
                    }).catch(function (err) {
                        console.log(err);
                        res.status(500).send(err);
                    });
                });
            }).catch(function (err) {
                console.log(err);
                res.status(500).send(err);
            });
        }).catch(function (err) {
            console.log(err);
            res.status(500).send(err);
        });
    });
};

exports.addShed = function(req, res) {

    DBlayer.DBaddShed(req.body.partnership_id, req.body.farm_id, req.body.center_id, req.body.code, req.body.status_id, req.body.stall_height, req.body.stall_width, req.body.capacity_min, req.body.capacity_max, req.body.rotation_days, req.body.breed_id, req.body.os_disable)

        .then(function(data) {
            console.log(data);
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
            // console.log(err);
            res.status(500).send(err);
        });
};


exports.findShedByCenter = function(req, res) {
    // console.log("llegue");
    DBlayer.DBfindShedByCenter(req.body.center_id)
        .then(function(data) {
            console.log("data sheds", data);
            res.status(200).json({
                statusCode: 200,
                data: data,
                asd: "asd"
            });
        })
        .catch(function(err) {
            console.log(err);
            res.status(500).send(err);
        });
};
exports.findShedByCenter2 = function(req, res) {
    // console.log("llegue");
    DBlayer.DBfindShedByCenter2(req.body.center_id)
        .then(function(data) {
            console.log("data sheds", data);
            res.status(200).json({
                statusCode: 200,
                data: data,
                asd: "asd"
            });
        })
        .catch(function(err) {
            console.log(err);
            res.status(500).send(err);
        });
};

exports.findShedByCenter3 = function(req, res) {
    // console.log("llegue");
    DBlayer.DBfindShedByCenter3(req.body.center_id)
        .then(function(data) {
            console.log("data sheds", data);
            res.status(200).json({
                statusCode: 200,
                data: data,
                asd: "asd"
            });
        })
        .catch(function(err) {
            console.log(err);
            res.status(500).send(err);
        });
};


exports.findShedsofFarms = function(req, res) {
    // console.log("llegue");
    DBlayer.DBfindShedsofFarms(req.body.partnership_id,req.body.farm_id)
        .then(function(data) {
            console.log("data sheds", data);
            res.status(200).json({
                statusCode: 200,
                data: data,
                asd: "asd"
            });
        })
        .catch(function(err) {
            console.log(err);
            res.status(500).send(err);
        });
};




exports.findShedsByPartnership = function(req, res) {
    // console.log("llegue");
    DBlayer.DBfindShedsByPartnership(req.body.partnership_id)
        .then(function(data) {
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
            // console.log(err);
            res.status(500).send(err);
        });
};
exports.findShedsByPartnership2 = function(req, res) {
    // console.log("llegue");
    DBlayer.DBfindShedsByPartnership2(req.body.partnership_id)
        .then(function(data) {
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
            // console.log(err);
            res.status(500).send(err);
        });
};


exports.updateShed = function(req, res) {

    DBlayer.DBupdateShed(req.body.shed_id, req.body.code, req.body.status_id, req.body.stall_height, req.body.stall_width, req.body.capacity_min, req.body.capacity_max, req.body.rotation_days, req.body.breed_id, req.body.os_disable)

        .then(function(data) {
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
            // console.log(err);
            res.status(500).send(err);
        });
};


exports.deleteShed = function(req, res) {
    DBlayer.DBdeleteShed(req.body.shed_id)
        .then(function(data) {
            res.status(200).json({
                statusCode: 200,
                mgs: "Galpon Eliminado"
            });
        })
        .catch(function(err) {
            // console.log(err);
            res.status(500).send(err);
        });
};

exports.updateRotationDays = function(req, res) {

    const records = req.body.data;
    const dataLength = records.length;

    try {
        for (let i = 0; i < dataLength; i++) {
            // console.log(records[i].shed_name);
            let uRD = DBlayer.DBupdateRotationDays(records[i].shed_id, records[i].rotation_days, records[i].capacity_theoretical);
        }
        res.status(200).json({
            statusCode: 200,
            mgs: "Galpon Actualizado"
        });

    } catch (err) {
    // console.log(err);
        res.status(500).send(err);
    }
};

exports.findShedsByFarms = async function(req, res) {
    try {
        let data  = await DBlayer.DBfindShedsByFarms(req.body.farms);
        res.status(200).json({
            statusCode: 200,
            data: data
        });
    } catch (err) {
        // console.log(err);
        res.status(500).send(err);
    }
};
exports.findShedsByFarms2 = async function(req, res) {
    try {
        let data  = await DBlayer.DBfindShedsByFarms2(req.body.farms);
        res.status(200).json({
            statusCode: 200,
            data: data
        });
    } catch (err) {
        // console.log(err);
        res.status(500).send(err);
    }
};

exports.findShedsByFarm = async function(req, res) {
    try {
        await DBlayer.DBcheckUpdateStatusShed();
        let data = await DBlayer.DBfindShedsByFarm(req.body.farm_id);
        res.status(200).json({
            statusCode: 200,
            data: data
        });
    } catch (err) {
        // console.log(err);
        res.status(500).send(err);
    }
};
exports.findShedsByFarm2 = async function(req, res) {
    try {
        await DBlayer.DBcheckUpdateStatusShed();
        let data = await DBlayer.DBfindShedsByFarm2(req.body.farm_id);
        res.status(200).json({
            statusCode: 200,
            data: data
        });
    } catch (err) {
        // console.log(err);
        res.status(500).send(err);
    }
};

exports.findShedsByFarmForReprod = async function(req, res) {
    try {
        // let data = await DBlayer.DBcheckUpdateStatusShedForReprod(req.body.farm_id);
        await DBlayer.DBcheckUpdateStatusShed();
        data = await DBlayer.DBfindShedsByFarm(req.body.farm_id);
        res.status(200).json({
            statusCode: 200,
            data: data
        });
    } catch (err) {
        // console.log(err);
        res.status(500).send(err);
    }
};
exports.findShedsByCenterForReprod = async function(req, res) {
    try {
        // let data = await DBlayer.DBcheckUpdateStatusShedForReprodByCenter(req.body.center_id);
        await DBlayer.DBcheckUpdateStatusShed();
        // data = await DBlayer.DBfindShedByCenter(req.body.center_id);
        data = await DBlayer.DBfindShedByCenter2(req.body.center_id);
        res.status(200).json({
            statusCode: 200,
            data: data
        });
    } catch (err) {
        // console.log(err);
        res.status(500).send(err);
    }
};

exports.updateShedOrder = function (req, res) {
    DBlayer.DBupdateShedOrder(req.body.data)
        .then(function (data) {
            res.status(200).json({
                statusCode: 200
            });
        })
        .catch(function (err) {
            console.log(err);
            res.status(500).send(err);
        });
};

exports.isBeingUsed = function(req, res) {
    DBlayer.DBisBeingUsed(req.body.shed_id)
        .then(function(data) {
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
            console.log(err);
            res.status(500).send(err);
        });
};


exports.findCenterByShed = function(req, res) {
    // console.log("llegue");
    DBlayer.DBfindCenterByShed(req.body.center_id)
      .then(function(data) {
        console.log("data sheds", data)
        res.status(200).json({
          statusCode: 200,
          data: data,
          asd: "asd"
        });
      })
      .catch(function(err) {
        console.log(err);
        res.status(500).send(err);
      });
  };

  
exports.findOshedByPartnerShip= function(req, res) {
    console.log("llegue");
    DBlayer.DBfindOshedByPartnerShip(req.body.partnership_id)
    .then(function(data) {
      console.log("data sheds", data)
      res.status(200).json({
        statusCode: 200,
        data: data,
        asd: "asd"
      });
    })
    .catch(function(err) {
      console.log(err);
      res.status(500).send(err);
    });
  };

 
  exports.findAllInformation = function(req, res) {
    console.log("llegue");
    DBlayer.DBfindAllInformation(req.body.partnership_id)
    .then(function(data) {
      console.log("data sheds", data)
      res.status(200).json({
        statusCode: 200,
        data: data,
        asd: "asd"
      });
    })
    .catch(function(err) {
      console.log(err);
      res.status(500).send(err);
    });
  };
  