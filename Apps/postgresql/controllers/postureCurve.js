const DBlayer = require("../models/postureCurve");
const DBbreed = require("../models/breed");
const utils = require("../../lib/utils");

exports.findAllPostureCurve = function(req, res) {
    DBlayer.DBfindAllPostureCurve()
        .then(function(data) {
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
            res.status(500).send(err);
        });
};



exports.bulkAddPostureCurve = function (req, res) {
    let J = 0;
    let band = false;
    DBbreed.DbKnowBreedid(req.body.registers).then(function (Breedid) {
        if (Breedid.length > 0) {
            for (let index = 0; index < req.body.registers.length; index++) {
                while ( J < Breedid.length && !band) {
                    if ( req.body.registers[index].breed_id == Breedid[J].name) {
                        req.body.registers[index].breed_id = Breedid[J].breed_id;
                        band = true;
                    }
                    J++; 
                }
                band = false;
                J = 0;
            }
            let postureCurves = req.body.registers;
            utils.cleanObjects(postureCurves);
            DBlayer.DBbulkAddPostureCurve(postureCurves).then(function (data) {
                res.status(200).json({
                    statusCode: 200,
                    data: data
                });
            }).catch(function (err) {
                console.log(err);
                res.status(500).send(err);
            });
        }else{
            res.status(401).json({
                statusCode: 401
            });
        }
    }).catch(function (err) {
        console.log(err);
        res.status(500).send(err);
    });
};

exports.bulkAddPostureCurve2 = utils.wrap(async function(req, res) {
    // throw new Error('holaa')
    const weeks = req.body.registers;
    const errors = [];
    utils.cleanObjects(weeks);

    const breeds = await DBbreed.DbKnowBreedid(weeks);
    console.log(breeds);
    for (const week of weeks) {

        const breedMatch = breeds.find(breed => breed.name === week.breedName);
        if (breedMatch !== undefined) {
            week.breed_id = breedMatch.breed_id;
        }
        else {
            errors.push({object: week, message: `La raza : ${week.breedName} no existe`});
        }
    }

    if (errors.length > 0) {
        throw new Error(errors[0].message);
    }

    const result = await DBlayer.DBbulkAddPostureCurve(weeks);
    res.status(200).json({
        statusCode: 200,
        data: result
    });
    
});

exports.findCurveByBreed = function(req, res) {
//   console.log(req.body.breed_id);
    DBlayer.DBfindCurveByBreed(req.body.breed_id)
        .then(function(data) {
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
            res.status(500).send(err);
        });
};


exports.addPostureCurve = async (req, res) => {
    try {

        let records = req.body.newRecords,
            breed_id = req.body.breed_id;

        records.forEach(item=>{
            item.breed_id = breed_id;
        });
        // console.log(records);
        let results = await DBlayer.DBaddPostureCurve(records);

        res.status(200).send({
            statusCode: 200,
            data: results
        });

    } catch (err) {
        // console.log(err);
        res.status(200).send({
            statusCode: 200,
            error: err.message,
            errorCode: err.code
        });
    }
};


exports.deletePostureCurveByBreed = function(req, res) {
    DBlayer.DBdeletePostureCurveByBreed(req.body.breed_id)
        .then(function(data) {
            res.status(200).json({
                statusCode: 200,
                mgs: "Curva Eliminada"
            });
        })
        .catch(function(err) {
        //   console.log(err);
            res.status(500).send(err);
        });
};



exports.updatePostureCurve = function(req, res) { 
    console.log("LLEGO AL CONTROLADOR RECIBIENDO"); 
    console.log(req.body); 
 
 
 
    DBlayer.DBupdatePostureCurve(req.body) 
        .then(function(data) { 
            res.status(200).json({ 
                statusCode: 200, 
                mgs: "Curva Actualizada" 
            }); 
        }) 
        .catch(function(err) { 
            console.log(err); 
            res.status(500).send(err); 
        }); 
};