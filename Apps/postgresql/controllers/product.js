const DBlayer = require("../models/product");


/*
Funcion GET que devuelve todos los productos
*/
exports.findAllProduct = function(req, res) {
    DBlayer.DBfindAllProduct()
        .then(function(data) {
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
            res.status(500).send(err);
        });
};

exports.addProduct = function(req, res) {
    DBlayer.DBaddProduct(req.body)
        .then(function(data) {
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
            // console.log(err);
            res.status(500).send(err);
        });
};

exports.bulkAddProduct = function(req, res) {
    //console.log(req.body.registers);
    let products = req.body.registers;
    //utils.cleanObjects(products);
    DBlayer.DBbulkAddProduct(products).then(function(data){
        res.status(200).json({
            statusCode: 200,
            data: data
        });
    }).catch(function(err){
        console.log(err);
        res.status(500).send(err);
    });
};

exports.updateProduct = function(req, res) {
    // console.log(req.body);
    DBlayer.DBupdateProduct(req.body)
        .then(function(data) {
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
            // console.log(err);
            res.status(500).send(err);
        });
};

exports.deleteProduct = function(req, res) {
    DBlayer.DBdeleteProduct(req.body.product_id)
        .then(function(data) {
            res.status(200).json({
                statusCode: 200
            });
        })
        .catch(function(err) {
            // console.log(err);
            res.status(500).send(err);
        });
};



exports.changeName = function(req, res) {
    let name = req.body.name;
    let excep = req.body.diff;
    DBlayer.DBcheckNameProduct(name,excep)
        .then(function(data){
            // console.log('result: ', data)
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
            res.status(500).send(err);
        });

        
};

exports.changeCode = function(req, res) {
    let name = req.body.name;
    let excep = req.body.diff;
    // console.log('in:: ',name, excep )
    DBlayer.DBcheckCodeProduct(name,excep)
        .then(function(data){
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
            res.status(500).send(err);
        });
   
};

exports.isBeingUsed = function(req, res) {
    console.log("controller");
    DBlayer.DBisBeingUsed(req.body.product_id)
        .then(function(data) {
            console.log("mi data: ", data);
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
            console.log(err);
            res.status(500).send(err);
        });
};