const config = require("../../config");
const assert = require("assert");
// Conexión a la BD:
const conn = require("../db");

function DBgetAllProcess() {
    return conn.db.any("SELECT * FROM public.mdprocess order by name ASC;");
}

function DBgetProcessById(process_id) {
    return conn.db.any("SELECT * FROM public.mdprocess WHERE process_id= $1;", [process_id]);
}


function DBgetAllProcessJ(){

    return conn.db.any("SELECT a.process_id, a.name as name, a.process_order, a.product_id, b.name as product_name, a.stage_id, c.name as stage_name, "+
                     "historical_decrease, theoretical_decrease, historical_weight, theoretical_weight, historical_duration, theoretical_duration, "+
                     "a.calendar_id, d.code, predecessor_id, visible,capacity,breed_id, a.biological_active, a.sync_considered, CASE WHEN predecessor_id=0 THEN 0 ELSE 1 END AS ispredecessor "+
                     "FROM mdprocess a "+
                     "LEFT JOIN mdproduct b on a.product_id = b.product_id "+
                     "LEFT JOIN mdstage  c on a.stage_id = c.stage_id "+
                     "LEFT JOIN txcalendar d on a.calendar_id = d.calendar_id order by breed_id,process_order ASC");
}

function DBaddProcess(process){
// console.log('add: ',process.biological_active);
    return conn.db.one(`INSERT INTO public.mdprocess (process_order, product_id, stage_id, historical_decrease,theoretical_decrease, historical_weight, theoretical_weight, 
    historical_duration,theoretical_duration, calendar_id,visible, name, 
    predecessor_id, capacity, breed_id, gender, fattening_goal, type_posture, biological_active, sync_considered) 
    VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19, $20) RETURNING process_id`,
    [process.process_order, process.product_id, process.stage_id, process.historical_decrease,process.theoretical_decrease, process.historical_weight,
        process.theoretical_weight, process.historical_duration, process.theoretical_duration,
        process.calendar_id, process.visible, process.name, process.predecessor_id, process.capacity, process.breed_id,
        process.gender, process.fattening_goal, process.type_posture, process.biological_active, process.sync_considered]);
}


function DBupdateProcess(process){
// console.log('update: ',process.biological_active);
    return conn.db.none(`UPDATE public.mdprocess SET process_order = $1, product_id = $2, stage_id = $3, historical_decrease = $4, theoretical_decrease = $5, 
                      historical_weight = $6, theoretical_weight = $7, historical_duration = $8 ,theoretical_duration = $9, 
                      calendar_id = $10, visible = $11, name = $12, predecessor_id= $13, capacity = $14, breed_id = $15, 
                      gender = $16, fattening_goal = $17, type_posture = $18, biological_active= $19, sync_considered = $21  
                      WHERE process_id = $20`,
    [process.process_order, process.product_id, process.stage_id, process.historical_decrease,process.theoretical_decrease, process.historical_weight,
        process.theoretical_weight, process.historical_duration, process.theoretical_duration,
        process.calendar_id, process.visible, process.name, process.predecessor_id, process.capacity, process.breed_id,
        process.gender, process.fattening_goal, process.type_posture, process.biological_active,
        process.process_id, process.sync_considered]);
}

function DBdeleteProcess(process_id) {
    return conn.db.none("DELETE FROM public.mdprocess WHERE process_id = $1", [process_id]);
}

function DBfindProcessByStage(stage_id) {
    // console.log("STAGE: ", stage_id);
    return conn.db.any("SELECT * FROM public.mdprocess WHERE stage_id= $1;", [stage_id]);
}

function DBfindProcessByStageAndBreed(stage_id, breed_id) {
    return conn.db.any("SELECT product_id, breed_id FROM public.mdprocess WHERE stage_id= $1 and breed_id =$2;",
        [stage_id, breed_id]);
}

function DBfindProcessBreedByStage(stage_id){
    return conn.db.any("SELECT b.name, theoretical_duration, historical_decrease as theoretical_decrease, calendar_id "+
                     "FROM public.mdprocess a "+
                     "LEFT JOIN public.mdbreed b on a.breed_id = b.breed_id "+
                     "WHERE stage_id= $1;", [stage_id]);
}



/*
* Retorna el detalle del housingWay solicitado para el proceso que se le indique
* Duracion y Merma los campos mas importante para avanzar a la siguiente etapa
*/
function DBfindProcessByStageBreed(stage_id, breed_id, scenario_id) {

    return conn.db.any("SELECT breed_id, stage_id, weight_goal, decrease_goal, duration_goal "+
                       "FROM public.mdprocess a "+
                       "LEFT JOIN public.txscenarioprocess b on a.process_id = b.process_id "+
                       "WHERE stage_id=$1 and breed_id=$2 and scenario_id = $3 ",
    [stage_id, breed_id, scenario_id]);
}

function DBfindProcessCalendarByStage(stage_id, breed_id) {

    return conn.db.any("SELECT breed_id, stage_id, weight_goal, decrease_goal, duration_goal "+
                       "FROM public.mdprocess a "+
                       "LEFT JOIN public.txscenarioprocess b on a.process_id = b.process_id "+
                       "WHERE stage_id=$1 and breed_id=$2 and scenario_id = $3 ",
    [stage_id, breed_id, scenario_id]);
}


function DBgetBiologicalActive() {

    return conn.db.any("SELECT * "+
                       "FROM public.mdprocess a "+
                       "WHERE biological_active=true ");
}

function DBfindProductByStage(stage_id) {

    return conn.db.any("SELECT * "+
                       "FROM public.mdprocess a "+
                       "WHERE stage_id=$1 ", [stage_id]);
}


function DBKnowProcessid(register) {
    let string = "SELECT name,process_id FROM public.mdprocess WHERE name = ";
    var index = 0;
    while(index < register.length){
        if (index == 0) {
            string = string +  "'"  +register[index].process_id +  "'" ;
        }else{
            string = string +" or " + "name = "+ "'" + register[index].process_id + "'";
        }  
        index++;
    }
    return conn.db.any(string);
}

function DBKnowProcessid(register) {
    let string = "SELECT name,process_id FROM public.mdprocess WHERE name = ";
    var index = 0;
    while(index < register.length){
        if (index == 0) {
            string = string +  "'"  +register[index].process +  "'" ;
        }else{
            string = string +" or " + "name = "+ "'" + register[index].process + "'";
        }  
        index++;
    }
    return conn.db.any(string);
}



module.exports = {
    DBgetAllProcess,
    DBgetProcessById,
    DBgetAllProcessJ,
    DBaddProcess,
    DBupdateProcess,
    DBdeleteProcess,
    DBfindProcessByStage,
    DBfindProcessBreedByStage,
    DBfindProcessByStageBreed,
    DBgetBiologicalActive,
    DBfindProductByStage,
    DBfindProcessByStageAndBreed,
    DBKnowProcessid
};
