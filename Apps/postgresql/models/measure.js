const config = require("../../config");
const conn = require("../db");

exports.DBfindAllMeasure = function() {
    return conn.db.any("SELECT * FROM public.mdmeasure order by name ASC");
};

exports.DBaddMeasure = function(name, abbreviation, originvalue, valuekg, is_unit) {
    return conn.db.one("INSERT INTO public.mdmeasure (name, abbreviation, originvalue, valuekg, is_unit ) VALUES ($1, $2, $3, $4, $5) RETURNING measure_id", [name.trim(), abbreviation.trim(), originvalue, valuekg, is_unit]);
};

exports.DBbulkAddMeasure = function(measures) {
    cs = conn.pgp.helpers.ColumnSet(["name", "abbreviation", "originvalue", "valuekg", "is_unit"], {table: "mdmeasure", schema: "public"});
    return conn.db.none(conn.pgp.helpers.insert(measures, cs));
};

exports.DBupdateMeasure = function(name, abbreviation, originvalue, valuekg, measure_id, is_unit) {
    return conn.db.none("UPDATE public.mdmeasure SET name = $1, abbreviation = $2, originvalue = $3, valuekg= $4, is_unit= $6 "+
                        "WHERE measure_id = $5", [name.trim(), abbreviation.trim(), originvalue, valuekg, measure_id, is_unit]);
};


exports.DBdeleteMeasure = function(measure_id) {
    return conn.db.none("DELETE FROM public.mdmeasure WHERE measure_id = $1",[measure_id]);
};


exports.DBchechNameMeasure = function(name, excep) {
    return conn.db.manyOrNone(	"SELECT name, abbreviation FROM public.mdmeasure "+
    							" WHERE mdmeasure.name = $1 and  mdmeasure.name <> $2 limit 1",
    							[name.trim(), excep.trim()]);
};

exports.DBchechAbrevMeasure = function(name, excep) {
    return conn.db.manyOrNone(	"SELECT name, abbreviation FROM public.mdmeasure "+
    							" WHERE mdmeasure.abbreviation = $1 and  mdmeasure.abbreviation <> $2 limit 1",
    							[name.trim(), excep.trim()]);
};

exports.DbKnowmeasure_id = function(register) {
    let string = "SELECT name,measure_id FROM public.mdmeasure WHERE name = ";
    var index = 0;
    while(index < register.length){
        if (index == 0) {
            string = string +  "'"  +register[index].measure_id +  "'" ;
        }else{
            string = string +" or " + "name = "+ "'" + register[index].measure_id + "'";
        }  
        index++;
    }
    return conn.db.any(string);
};
  

