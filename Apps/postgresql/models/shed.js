const config = require("../../config");
const conn = require("../db");

exports.DBfindAllShed = function() {
    return conn.db.any("SELECT * FROM public.osshed order by code ASC");
};


exports.DBaddShed = function(partnership_id, farm_id, center_id, code, status_id, stall_height, stall_width, capacity_min, capacity_max, rotation_days, breed_id, os_disable) {
    console.log(partnership_id, farm_id, center_id, code, status_id, stall_height, stall_width, capacity_min, capacity_max, rotation_days, breed_id, os_disable);
    return conn.db.one(`INSERT INTO public.osshed (partnership_id, farm_id, center_id, 
                        code, statusshed_id, stall_height, stall_width, capacity_min, capacity_max, rotation_days, breed_id, os_disable) 
                        VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12) RETURNING shed_id`, [partnership_id,
        farm_id, center_id, code, status_id, stall_height, stall_width, capacity_min,
        capacity_max,  rotation_days, breed_id, os_disable]);
};

exports.DBbulkAddShed = function(sheds){
    cs = conn.pgp.helpers.ColumnSet(["partnership_id", "farm_id", "center_id", 
        "code", "statusshed_id", "type_id", "building_date",
        "stall_width", "stall_height", "capacity_min","capacity_max", "environment_id", 
        "rotation_days", "nests_quantity", "cages_quantity",
        "birds_quantity", "capacity_theoretical"], {table: "osshed", schema: "public"});
    return conn.db.none(conn.pgp.helpers.insert(sheds, cs));
};


exports.DBfindShedByCenter = function(center_id) {
    console.log("I'm slim shady, yes i'm the real shady!");
    return conn.db.any(`SELECT a.shed_id, a.code, partnership_id, farm_id, 
    (a.stall_height * a.stall_width * capacity_min) as capmin, 
    (a.stall_height * a.stall_width * capacity_max) as capmax,a.stall_height, a.stall_width, a.environment_id, rotation_days, 
    statusshed_id, avaliable_date,a.order, false As availableOrder, os_disable, breed_id     
    FROM public.osshed a 
    WHERE center_id = $1 order by a.order ASC` , [center_id]);
};
exports.DBfindShedByCenter2 = function(center_id) {
    console.log("I'm slim shady, yes i'm the real shady!");
    return conn.db.any(`SELECT a.shed_id, a.code, partnership_id, farm_id, 
                        (a.stall_height * a.stall_width * capacity_min) as capmin, 
                        (a.stall_height * a.stall_width * capacity_max) as capmax,a.stall_height, a.stall_width, a.environment_id, rotation_days, 
                        statusshed_id, avaliable_date,a.order, false As availableOrder, os_disable, breed_id     
                        FROM public.osshed a 
                        WHERE center_id = $1 and a.os_disable is not true order by a.order ASC` , [center_id]);
};

exports.DBfindShedByCenter3 = function(center_id) {
    console.log("I'm slim shady, yes i'm the real shady!");
    return conn.db.any(`SELECT a.shed_id, a.code, partnership_id, farm_id, 
    (capacity_min) as capmin, 
    (capacity_max) as capmax,a.stall_height, a.stall_width, a.environment_id, rotation_days, 
    statusshed_id, avaliable_date,a.order, false As availableOrder, os_disable, breed_id     
    FROM public.osshed a 
    WHERE center_id = $1 order by a.order ASC` , [center_id]);
};



exports.DBupdateShed = function(shed_id, code, status_id, stall_height, stall_width, capacity_min, capacity_max, rotation_days, breed_id, os_disable) {
// console.log(shed_id+' '+code+' '+status_id+' '+stall_height+' '+stall_width+' '+capacity_min+' '+capacity_max+' '+environment_id);
    console.log(capacity_min);
    console.log(capacity_max);
    return conn.db.none(`UPDATE public.osshed SET code = $1, statusshed_id = $2, stall_height = $3, stall_width = $4, 
                        capacity_max = $5, capacity_min = $6, rotation_days = $7,
                        breed_id = $9, os_disable = $10
                        WHERE shed_id = $8`, [code,
        status_id, stall_height, stall_width, capacity_max,capacity_min, rotation_days, shed_id, breed_id, os_disable]);
};

exports.DBdeleteShed = function(shed_id) {
    return conn.db.none("DELETE FROM public.osshed WHERE shed_id = $1",[shed_id]);
};

exports.DBupdateRotationDays = function(shed_id, rotation_days, capacity_theoretical) {
//   console.log(shed_id, rotation_days, capacity_theoretical);
    return conn.db.none("UPDATE public.osshed SET rotation_days = $1, capacity_theoretical = $2 "+
                        "WHERE shed_id = $3", [rotation_days, capacity_theoretical, shed_id]);
};

//Buscar una granja especifica
exports.DBfindShedsByFarm = function(farm_id) {
    // console.log("- ", farm_id);
    return conn.db.any(`SELECT a.shed_id, a.code, partnership_id, farm_id, 
                        (a.stall_height * a.stall_width * capacity_min) as capmin, 
                        (a.stall_height * a.stall_width * capacity_max) as capmax, rotation_days, 
                        statusshed_id, avaliable_date,a.order, a.os_disable, 
                        CASE WHEN a.order is not null THEN false ELSE true END As availableOrder    
                        FROM public.osshed a 
                        WHERE a.farm_id = $1 order by a.order ASC`, [farm_id]);
};
exports.DBfindShedsByFarm2 = function(farm_id) {
    // console.log("- ", farm_id);
    return conn.db.any(`SELECT a.shed_id, a.code, partnership_id, farm_id, 
                        (a.stall_height * a.stall_width * capacity_min) as capmin, 
                        (a.stall_height * a.stall_width * capacity_max) as capmax, rotation_days, 
                        statusshed_id, avaliable_date,a.order, a.os_disable, 
                        CASE WHEN a.order is not null THEN false ELSE true END As availableOrder    
                        FROM public.osshed a 
                        WHERE a.farm_id = $1 and a.os_disable is not true order by a.order ASC`, [farm_id]);
};

//Buscar todas las granjas y determinar las capacidades
exports.DBfindShedsByFarms = function(farms) {
//   console.log("Sin formato: ", farms);
    farms= JSON.parse(farms);
    // console.log("- ", farms);
    return conn.db.any(`SELECT a.shed_id, a.code, partnership_id, farm_id, 
                        (a.stall_height * a.stall_width * capacity_min) as capmin, 
                        (a.stall_height * a.stall_width * capacity_max) as capmax, rotation_days, a.os_disable  
                        FROM public.osshed a 
                        WHERE a.farm_id in ($1:csv) order by farm_id, capmax DESC`, [farms]);
};
exports.DBfindShedsByFarms2 = function(farms) {
//   console.log("Sin formato: ", farms);
    farms= JSON.parse(farms);
    // console.log("- ", farms);
    return conn.db.any(`SELECT a.shed_id, a.code, partnership_id, farm_id, 
                        (a.stall_height * a.stall_width * capacity_min) as capmin, 
                        (a.stall_height * a.stall_width * capacity_max) as capmax, rotation_days, a.os_disable  
                        FROM public.osshed a 
                        WHERE a.farm_id in ($1:csv) and a.os_disable is not true order by farm_id, capmax DESC`, [farms]);
};

exports.DBfindSomething = function() {
    return conn.db.any("SELECT distinct 1"+
        "FROM public.osshed ");
};

exports.DBupdateStatusShed = function(shed_id, status) {
    console.log("status: ", status, shed_id);
    return conn.db.any("UPDATE public.osshed SET statusshed_id = $1 "+
                        "WHERE shed_id = $2 "+
                        " RETURNING shed_id, rotation_days, current_date as act_date ", [status, shed_id]);
};

exports.DBupdateAvaliableDateShed = function(shed_id, adate= null) {
    return conn.db.any( " UPDATE public.osshed SET avaliable_date =$1 "+
                            " WHERE shed_id = $2 "+
                            " RETURNING shed_id, avaliable_date, rotation_days ", [adate, shed_id]);
};


exports.DBcheckUpdateStatusShed = function() {
    return conn.db.any(`update public.osshed
                        set avaliable_date= null, statusshed_id= 1
                        where statusshed_id= 3
                        and avaliable_date <= current_date`);
};


exports.DBcheckUpdateStatusShedForReprod = function(farm_id) {
    return conn.db.any(`update public.osshed
                        set avaliable_date= null, statusshed_id= 1
                        where statusshed_id= 2 and farm_id = $1 
                        and avaliable_date <= current_date`, [farm_id]);
};
exports.DBcheckUpdateStatusShedForReprodByCenter = function(center_id) {
    return conn.db.any(`update public.osshed
                        set avaliable_date= null, statusshed_id= 1
                        where statusshed_id= 2 and center_id = $1 
                        and avaliable_date <= current_date`, [center_id]);
};

exports.DBfindCenterByFarm = function(farm_id) {
    return conn.db.any(`SELECT  b.code, b.center_id, b.name, a.farm_type_id, a.farm_id
	FROM public.osfarm as a
	LEFT JOIN public.oscenter as b ON a.farm_id = b.farm_id
	 WHERE a.farm_id = $1`, [farm_id]);
};


exports.DBfindShedById = function(id) {
    return conn.db.any("SELECT * FROM public.osshed WHERE shed_id = $1 ",[id]);
};

exports.DBforceFreeShedById = function(id) {
    return conn.db.any("update osshed set statusshed_id = 1, avaliable_date= null"+
                        " WHERE shed_id = $1 ",[id]);
};
exports.DBupdateShedOrder = function(records) {
    return conn.db.none(conn.pgp.helpers.update(records, ["?shed_id", {name: "order",cast: "integer"}], "osshed") + " WHERE v.shed_id = t.shed_id");
};

exports.DBisBeingUsed = function(shed_id) {
    return conn.db.one(`SELECT ((SELECT DISTINCT CASE WHEN b.housingway_detail_id IS NOT NULL THEN TRUE ELSE FALSE END
                            FROM public.osshed a
                            LEFT JOIN txhousingway_detail b on b.shed_id = a.shed_id or b.executionshed_id = a.shed_id
                            WHERE a.shed_id=$1)
                        OR (SELECT DISTINCT CASE WHEN b.broiler_detail_id IS NOT NULL THEN TRUE ELSE FALSE END
                            FROM public.osshed a
                            LEFT JOIN txbroiler_detail b on b.shed_id = a.shed_id or b.executionshed_id = a.shed_id
                        WHERE a.shed_id=$1)
                        OR (SELECT DISTINCT CASE WHEN b.broilereviction_detail_id IS NOT NULL THEN TRUE ELSE FALSE END
                            FROM public.osshed a
                            LEFT JOIN txbroilereviction_detail b on b.shed_id = a.shed_id 
                        WHERE a.shed_id=$1)) as used `,[shed_id]);
};
// exports.DBcheckFreeShed= function(){
//     return conn.db.any(`
//         update public.osshed as a
//         SET statusshed_id= 3
//         FROM (
//                 select t.shed_id
//                 from public.txbroilereviction_detail t
//                 where t.shed_id not in (
//                     select a.shed_id
//                     from public.txbroilereviction_detail a
//                     where a.execution_quantity = 0 or a.execution_quantity= null
//                     group by a.shed_id
//                 )
//                 group by t.shed_id
//             ) as t
//         WHERE a.shed_id = t.shed_id
//         Returning a.shed_id
//     `);
// }
exports.DBfindCenterByShed = function(center_id){
    return conn.db.any(`SELECT a.code ,stall_width,stall_height,capacity_min,capacity_max,rotation_days, b.center_id
	FROM public.osshed as a
    LEFT JOIN public.oscenter b ON a.center_id = b.center_id
	WHERE b.center_id =$1`, [center_id]);
};

exports.DBfindOshedByPartnerShip = function(partnership_id) {
    return conn.db.any(`SELECT code,stall_width,stall_height,capacity_min,capacity_max,rotation_days
	FROM public.osshed
	WHERE  partnership_id = 1
	GROUP BY code,stall_width,stall_height,capacity_min,capacity_max,rotation_days
	ORDER BY code`, [partnership_id]);
}



// DBFarmWithCenter
exports.DBfindShedsofFarms  = function(partnership_id,farm_id){
    console.log("HOLA KARLA")
    return conn.db.any(`SELECT  a.code, a.name, c.name, b.code as code_center, b.name as name_center
    FROM public.osfarm as a
    LEFT JOIN public.oscenter as b ON b.farm_id = a.farm_id
	LEFT JOIN public.mdfarmtype as c ON c.farm_type_id = a.farm_type_id
    WHERE a.partnership_id = $1
    ORDER BY a.code`,[partnership_id]);
}


exports.DBfindFarmByPartnership = function(partnership_id){
    return conn.db.any(`SELECT  code, name, farm_type_id
	    FROM public.osfarm
	    WHERE partnership_id = $1
	    GROUP BY code,name,farm_type_id
	    ORDER BY code`,[partnership_id]);
}
exports.DBfindCenterByFarmAll = function(partnership_id,farm_id) {
    return conn.db.any(`SELECT a.code as codeg, a.name as nameg, c.name as namec, b.code as codec, b.name as nameb
	FROM public.osfarm as a
	LEFT JOIN public.oscenter as b ON a.farm_id = b.farm_id
    LEFT JOIN public.mdfarmtype as c ON c.farm_type_id = a.farm_type_id
    WHERE a.partnership_id = $1
    ORDER BY a.code, b.code
   `, [partnership_id,farm_id]);
}


exports.DBfindAllInformation = function(partnership_id) {
    return conn.db.any(`SELECT a.code ascodeg, a.name as nameg, e.name as namef, b.code as coden, b.name as namen, c.code as codes,c.stall_width, c.stall_height,c.capacity_min, c.capacity_max,c.rotation_days
	FROM public.osfarm as a
	LEFT JOIN public.oscenter as b ON a.farm_id = b.farm_id
	LEFT JOIN public.osshed as c ON b.center_id = c.center_id
	LEFT JOIN public.ospartnership as d ON c.partnership_id = d.partnership_id
	LEFT JOIN public.mdfarmtype as e ON e.farm_type_id = a.farm_type_id
	WHERE a.partnership_id = $1
	ORDER BY a.code, b.code
   `, [partnership_id]);
}
