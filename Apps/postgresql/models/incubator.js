const config = require("../../config");
const conn = require("../db");

exports.DBfindIncubatorByPlant = function(incubator_plant_id) {
    return conn.db.any("SELECT * FROM public.osincubator "+
                       "WHERE incubator_plant_id=$1 "+
                       "order by name ASC", [incubator_plant_id]);
};

// exports.DBfindIncubatorByPlantAndDay = function(incubator_plant_id, day) {
//   console.log("recibido en el modelo")
//   console.log(incubator_plant_id)
//   console.log(day)
//     return conn.db.any("SELECT * FROM public.osincubator a "+
//                        "WHERE a.incubator_plant_id=$1 and a.$2 = true "+
//                        "order by name ASC", [incubator_plant_id, day]);
// };

exports.DBbulkAddIncubator = function(incubators){
    cs = conn.pgp.helpers.ColumnSet(["incubator_plant_id", "name", "code", "description", 
        "capacity", {prop: "capacity", name: "available"} ,"sunday", "monday", "tuesday", "wednesday", "thursday", "friday", 
        "saturday"], {table: "osincubator", schema: "public"});
    return conn.db.none(conn.pgp.helpers.insert(incubators, cs));
};

exports.DBaddIncubator = function(incubator_plant_id,name, code, description, capacity,
    sunday, monday, tuesday, wednesday, thursday,
    friday, saturday, available) {

    return conn.db.one("INSERT INTO public.osincubator (incubator_plant_id, name, code, description, capacity, "+
                        "sunday, monday, tuesday, wednesday, thursday, "+
                        "friday, saturday, available) "+
                        "VALUES ($1, $2 , $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13) RETURNING incubator_id", [
        incubator_plant_id,name, code, description, capacity,
        sunday, monday, tuesday, wednesday, thursday,
        friday, saturday, available]);
};

exports.DBupdateIncubator = function(incubator_id, name, code, description, capacity,
    sunday, monday, tuesday, wednesday, thursday,
    friday, saturday,available) {
    return conn.db.none("UPDATE public.osincubator SET name = $1, code = $2 , description= $3, "+
                        "capacity= $4, sunday=$5 , monday=$6, tuesday=$7 , wednesday=$8 , thursday=$9, "+
                        "friday=$10 , saturday=$11, available = $13 "+
                        "WHERE incubator_id = $12", [name, code, description, capacity,
        sunday, monday, tuesday, wednesday, thursday,
        friday, saturday, incubator_id,available]);
};


exports.DBupdateIncubator2 = function(date) {
    return conn.db.none(`UPDATE public.osincubator a 
      SET available = available + i.quantity 
      FROM ( SELECT a.incubator_id as id, sum(a.eggs) as quantity 
                       FROM txprogrammed_eggs a 
                       WHERE $1 >= a.use_date + 22 and a.released != true group by(a.incubator_id)) as i 
      WHERE i.id = a.incubator_id`, [date]);

   
    // 'UPDATE public.osincubator a '+
    //   'SET available = available + i.quantity '+
    //   'FROM ( SELECT a.programmed_eggs_id, a.incubator_id as id, a.eggs as quantity '+
    //                    'FROM txprogrammed_eggs a '+
    //                    'WHERE $1 >= a.use_date + 22 and a.released != true) as i '+
    //   'WHERE i.id = a.incubator_id RETURNING i.programmed_eggs_id', [date]);

};






exports.DBupdateAvailableIncubator = function(incubator_id, available) {
    console.log("llego al update available");
    console.log(incubator_id);
    console.log(available);
    return conn.db.none("UPDATE public.osincubator SET available = available - $2 "+
                        "WHERE incubator_id = $1", [incubator_id, available]);
};




exports.DBdeleteIncubator = function(incubator_id) {
    return conn.db.none("DELETE FROM public.osincubator WHERE incubator_id = $1",[incubator_id]);
};


exports.DBfindIncubatorByPartnership = function(partnership_id,incubator_plant_id) {
    return conn.db.any("select b.name, capacity, sunday, monday, tuesday, wednesday, thursday, "+
                       "friday, saturday, b.incubator_id, b.available  "+
                       "FROM osincubatorplant a "+
                       "LEFT JOIN osincubator b on b.incubator_plant_id = a.incubator_plant_id "+
                       "where partnership_id = $1 and a.incubator_plant_id = $2 ", [partnership_id,incubator_plant_id]);
};

exports.DBfindSomething = function() {
    return conn.db.any("SELECT distinct 1"+
        "FROM public.osincubator ");
};