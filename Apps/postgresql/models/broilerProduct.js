const config = require("../../config");
const conn = require("../db");

exports.DBfindAllBroilerProduct = function() {
    return conn.db.any("SELECT * FROM public.mdbroiler_product order by name ASC");
};

exports.DBaddBroilerProduct = function(name, weight, day, code){
    // console.log(name);
    return conn.db.one("INSERT INTO public.mdbroiler_product (name, weight, days_eviction, code) VALUES ($1, $2, $3, $4) RETURNING broiler_product_id", [name, weight, day, code]);
};

exports.DBupdateBroilerProduct = function(broiler_product_id, name, weight, day, code) {
    // console.log(broiler_product_id, name, weight, day);
    // console.log("UPDATE BD");
    return conn.db.none("UPDATE public.mdbroiler_product SET name = $1 , weight = $2, days_eviction = $3, code = $4"+
                        "WHERE broiler_product_id = $5", [name, weight, day, code, broiler_product_id]);
};

exports.DBdeleteBroilerProduct = function(broiler_product_id) {
    // console.log(broiler_product_id);
    return conn.db.none("DELETE FROM public.mdbroiler_product WHERE broiler_product_id = $1",
        [broiler_product_id]);
};

exports.DBisBeingUsed = function(broiler_product_id) {
    return conn.db.one(`SELECT ((SELECT DISTINCT CASE WHEN b.broiler_detail_id IS NOT NULL THEN TRUE ELSE FALSE END
                      FROM public.mdbroiler_product a
                          LEFT JOIN txbroiler_detail b on b.broiler_product_id = a.broiler_product_id
                      WHERE a.broiler_product_id = $1)
                      OR (SELECT DISTINCT CASE WHEN b.broilereviction_detail_id IS NOT NULL THEN TRUE ELSE FALSE END
                          FROM public.mdbroiler_product a
                          LEFT JOIN txbroilereviction_detail b on b.broiler_product_id = a.broiler_product_id 
                      WHERE a.broiler_product_id = $1)) as used `,[broiler_product_id]);
};
