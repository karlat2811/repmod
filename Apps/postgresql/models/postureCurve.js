const config = require("../../config");
const conn = require("../db");



exports.DBfindAllPostureCurve = function() {
    return conn.db.any("SELECT a.breed_id, " +
                       "(SELECT name FROM mdbreed where a.breed_id = breed_id) " +
                       "FROM txposturecurve a group by a.breed_id order by name");
};

exports.DBbulkAddPostureCurve = function(postureCurves){
    cs = conn.pgp.helpers.ColumnSet(["breed_id","week", "theorical_performance", 
        "historical_performance","theorical_accum_mortality", "historical_accum_mortality",
        "theorical_uniformity", "historical_uniformity", "type_posture"], {table: "txposturecurve", schema: "public"});
    return conn.db.none(conn.pgp.helpers.insert(postureCurves, cs));
};

exports.DBfindCurveByBreed = function(breed_id) {
    return conn.db.any("SELECT * FROM public.txposturecurve WHERE breed_id = $1 order by week ASC", [breed_id]);
};

function Inserts(template, data) {
    if (!(this instanceof Inserts)) {
        return new Inserts(template, data);
    }

    this._rawDBType = true;
    this.formatDBType = function() {
        //console.log(data.map(d => '(' + conn.pgp.as.format(template, d) + ')').join(','));
        return data.map(d => "(" + conn.pgp.as.format(template, d) + ")").join(",");
    };
}

exports.DBaddPostureCurve = function(records) {
    let promise = conn.db.none("INSERT INTO txposturecurve (week, breed_id, theorical_performance, "+
                             " historical_performance, theorical_accum_mortality, historical_accum_mortality, "+
                             " theorical_uniformity, historical_uniformity, type_posture) VALUES $1",
    Inserts("${week}, ${breed_id}, ${theorical_performance}, ${historical_performance}, ${theorical_accum_mortality}, "+
              "${historical_accum_mortality}, ${theorical_uniformity}, ${historical_uniformity}, ${type_posture} ", records));
    return promise;
};


exports.DBdeletePostureCurveByBreed = function(breed_id) {
    return conn.db.none("DELETE FROM public.txposturecurve WHERE breed_id = $1",[breed_id]);
};


exports.DBupdatePostureCurve = function(record){ 
    console.log("RECIBI EN EL MODELO"); 
    console.log(record); 
 
 
    cs = conn.pgp.helpers.ColumnSet(["posture_curve_id",          
        {name: "theorical_performance", 
            init: col => { 
            // set to 100, if the value is 0: 
                return parseFloat(col.value);} 
        }], {table: "txposturecurve", schema: "public"}); 
    return conn.db.none(conn.pgp.helpers.update(record.arra2, cs)+ "WHERE v.posture_curve_id = t.posture_curve_id"); 
 
};