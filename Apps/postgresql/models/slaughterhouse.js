const config = require("../../config");
const conn = require("../db");

exports.DBfindAllslaughterhouse = function() {
    return conn.db.any("SELECT * FROM public.osslaughterhouse order by name ASC");
};

exports.DBaddslaughterhouse = function(name, description, address, code, capacity) {
    return conn.db.one("INSERT INTO public.osslaughterhouse (name, description, address, code, capacity) VALUES ($1, $2, $3, $4, $5) RETURNING slaughterhouse_id",
        [name, description, address, code, capacity]);
};

exports.DBupdateslaughterhouse = function(name, description, address, slaughterhouse_id, code, capacity) {
    return conn.db.none("UPDATE public.osslaughterhouse SET name = $1, description = $2, address = $3, code = $4, capacity = $6 "+
                        "WHERE slaughterhouse_id = $5", [name, description, address, code, slaughterhouse_id, capacity]);
};

exports.DBdeleteslaughterhouse = function(slaughterhouse_id) {
    return conn.db.none("DELETE FROM public.osslaughterhouse WHERE slaughterhouse_id = $1",[slaughterhouse_id]);
};