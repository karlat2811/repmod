const config = require("../../config");
const conn = require("../db");


function Inserts(template, data) {
    if (!(this instanceof Inserts)) {
        return new Inserts(template, data);
    }

    this._rawDBType = true;
    this.formatDBType = function() {
	  return data.map(d => "(" + conn.pgp.as.format(template, d) + ")").join(",");
    };
}

exports.DBAddEgreso = function(fecha_movements, lot, quantity, eggs_storage_id) {

    return conn.db.none(`INSERT INTO public.txeggs_movements ( fecha_movements, lot, quantity, type_movements, eggs_storage_id) 
		values ($1, $2, $3, $5, $4) `, [fecha_movements, lot, quantity, eggs_storage_id, "egreso"] );



};


exports.DBfindProjectEggs = function(partnership_id) {

    return conn.db.any(`SELECT 	a.incubator_plant_id, a.name ,
						SUM(CASE WHEN c.type_movements = 'ingreso' and hey.eggs_storage_id = c.eggs_storage_id 
								and hey.eggs_storage_id = b.eggs_storage_id
								Then  (hey.ingreso + hey.egreso) Else 0 END) as total,
								
						SUM(CASE WHEN c.type_movements = 'ingreso' and hey.eggs_storage_id = c.eggs_storage_id 
							and hey.eggs_storage_id = b.eggs_storage_id and (EXTRACT(DAY FROM(NOW() - b.init_date )) <a.min_storage) 
							THEN (hey.ingreso + hey.egreso) ELSE 0 END ) as aclimatized,
							
						SUM(CASE WHEN c.type_movements = 'ingreso' and hey.eggs_storage_id = c.eggs_storage_id 
							and hey.eggs_storage_id = b.eggs_storage_id 
							and (EXTRACT(DAY FROM(NOW() - b.init_date )) >=a.min_storage and EXTRACT(DAY FROM(NOW() - b.init_date )) <=a.max_storage) 
							THEN (hey.ingreso+ hey.egreso) ELSE 0 END ) as suitable,
							
						SUM(CASE WHEN c.type_movements = 'ingreso' and hey.eggs_storage_id = c.eggs_storage_id 
							and hey.eggs_storage_id = b.eggs_storage_id and (EXTRACT(DAY FROM(NOW() - b.init_date )) >=a.max_storage) 
							THEN (hey.ingreso + hey.egreso) ELSE 0 END ) as expired,
							
						(
								SUM(CASE WHEN (c.type_movements = 'ingreso' and  hey.eggs_storage_id = c.eggs_storage_id and hey.eggs_storage_id = b.eggs_storage_id and a.acclimatized= true and EXTRACT(DAY FROM(NOW() - b.init_date )) <a.min_storage) then (hey.ingreso + hey.egreso) else 0 end )+
								SUM(CASE WHEN (c.type_movements = 'ingreso' and  hey.eggs_storage_id = c.eggs_storage_id and hey.eggs_storage_id = b.eggs_storage_id and a.suitable= true and EXTRACT(DAY FROM(NOW() - b.init_date )) >=a.min_storage and EXTRACT(DAY FROM(NOW() - b.init_date )) <a.max_storage) then (hey.ingreso + hey.egreso) else 0 end )+
								SUM(CASE WHEN (c.type_movements = 'ingreso' and  hey.eggs_storage_id = c.eggs_storage_id and hey.eggs_storage_id = b.eggs_storage_id and a.expired= true and EXTRACT(DAY FROM(NOW() - b.init_date )) >=a.max_storage) then (hey.ingreso + hey.egreso) else 0 end )
						) as available from 
						(   SELECT b.fecha_movements,b.eggs_storage_id,  
							SUM(CASE WHEN b.type_movements = 'ingreso' THEN  b.quantity ELSE 0 END) As ingreso ,
							SUM(CASE WHEN b.type_movements = 'egreso' THEN  (-1)*b.quantity ELSE 0 END) As egreso
							FROM txeggs_movements b  
							GROUP BY b.eggs_storage_id,b.fecha_movements
							Order by b.fecha_movements) AS hey ,osincubatorplant a 
						LEFT JOIN txeggs_storage b on a.incubator_plant_id = b.incubator_plant_id   
						LEFT JOIN txeggs_movements c on b.eggs_storage_id = c.eggs_storage_id
						WHERE   a.partnership_id = $1
						and  b.init_date <= NOW()
						and hey.eggs_storage_id = b.eggs_storage_id
						and hey.eggs_storage_id = c.eggs_storage_id
						and b.eggs_storage_id = c.eggs_storage_id
						and b.lot = c.lot 
						and (CASE WHEN (select count(*) from txeggs_movements) != 0 Then (hey.ingreso + hey.egreso) is not null 
								Else (hey.ingreso + hey.egreso) is null END)
						group by a.incubator_plant_id, a.name`,[partnership_id]);
    // return conn.db.any(`                   
    // 					SELECT 	a.incubator_plant_id, a.name ,
    // 					SUM(CASE WHEN c.type_movements = 'ingreso' and hey.eggs_storage_id = c.eggs_storage_id and hey.eggs_storage_id = b.eggs_storage_id
    // 							Then  (hey.ingreso + hey.AjusEgreso + hey.Original +hey.Suma + hey.egreso) Else 0 END) as total,
    // 					SUM(CASE WHEN c.type_movements = 'ingreso' and hey.eggs_storage_id = c.eggs_storage_id and hey.eggs_storage_id = b.eggs_storage_id and (EXTRACT(DAY FROM(NOW() - b.init_date )) <a.min_storage) THEN (hey.ingreso + hey.AjusEgreso + hey.Original +hey.Suma + hey.egreso) ELSE 0 END ) as aclimatized,
    // 					SUM(CASE WHEN c.type_movements = 'ingreso' and hey.eggs_storage_id = c.eggs_storage_id and hey.eggs_storage_id = b.eggs_storage_id and (EXTRACT(DAY FROM(NOW() - b.init_date )) >=a.min_storage and EXTRACT(DAY FROM(NOW() - b.init_date )) <a.max_storage) THEN (hey.ingreso + hey.AjusEgreso + hey.Original +hey.Suma + hey.egreso) ELSE 0 END ) as suitable,
    // 					SUM(CASE WHEN c.type_movements = 'ingreso' and hey.eggs_storage_id = c.eggs_storage_id and hey.eggs_storage_id = b.eggs_storage_id and (EXTRACT(DAY FROM(NOW() - b.init_date )) >=a.max_storage) THEN (hey.ingreso + hey.AjusEgreso + hey.Original +hey.Suma + hey.egreso) ELSE 0 END ) as expired,
    // 					(
    // 							SUM(CASE WHEN (c.type_movements = 'ingreso' and  hey.eggs_storage_id = c.eggs_storage_id and hey.eggs_storage_id = b.eggs_storage_id and a.acclimatized= true and EXTRACT(DAY FROM(NOW() - b.init_date )) <a.min_storage) then (hey.ingreso + hey.AjusEgreso + hey.Original +hey.Suma + hey.egreso) else 0 end )+
    // 							SUM(CASE WHEN (c.type_movements = 'ingreso' and  hey.eggs_storage_id = c.eggs_storage_id and hey.eggs_storage_id = b.eggs_storage_id and a.suitable= true and EXTRACT(DAY FROM(NOW() - b.init_date )) >=a.min_storage and EXTRACT(DAY FROM(NOW() - b.init_date )) <a.max_storage) then (hey.ingreso + hey.AjusEgreso + hey.Original +hey.Suma + hey.egreso) else 0 end )+
    // 							SUM(CASE WHEN (c.type_movements = 'ingreso' and  hey.eggs_storage_id = c.eggs_storage_id and hey.eggs_storage_id = b.eggs_storage_id and a.expired= true and EXTRACT(DAY FROM(NOW() - b.init_date )) >=a.max_storage) then (hey.ingreso + hey.AjusEgreso + hey.Original +hey.Suma + hey.egreso) else 0 end )
    // 					) as available from (  
    // 					SELECT b.fecha_movements,b.eggs_storage_id,  
    // 					SUM(CASE WHEN b.type_movements = 'ingreso' THEN  b.quantity ELSE 0 END) As ingreso ,
    // 					SUM(CASE WHEN b.type_movements = 'egreso' THEN  (-1)*b.quantity ELSE 0 END) As egreso ,  
    // 					SUM(CASE WHEN b.type_movements = 'Ajuste' and b.description_adjustment  != 'Compra de huevos' THEN (-1)*b.quantity ELSE 0 END) As AjusEgreso,  
    // 					SUM(CASE WHEN b.type_movements = 'Original' THEN b.quantity ELSE 0 END) As Original,  
    // 					SUM(CASE WHEN b.type_movements = 'Ajuste' and b.description_adjustment  = 'Compra de huevos' THEN b.quantity Else 0 End) as Suma  
    // 					FROM txeggs_movements b  
    // 					GROUP BY b.eggs_storage_id,b.fecha_movements) AS hey ,osincubatorplant a 
    // 					LEFT JOIN txeggs_storage b on a.incubator_plant_id = b.incubator_plant_id   
    // 					LEFT JOIN txeggs_movements c on b.eggs_storage_id = c.eggs_storage_id
    // 					WHERE   a.partnership_id = $1
    // 					and  b.init_date <= NOW()
    // 					and (CASE WHEN (select count(*) from txeggs_movements) != 0 Then (hey.ingreso + hey.AjusEgreso + hey.Original +hey.Suma + hey.egreso) is not null 
    // 							Else (hey.ingreso + hey.AjusEgreso + hey.Original +hey.Suma + hey.egreso) is null END)
    // 					group by a.incubator_plant_id`,[partnership_id]);
    /*   return conn.db.any(` SELECT a.incubator_plant_id, a.name ,
								SUM(b.eggs_executed) as total,
								SUM(CASE WHEN (EXTRACT(DAY FROM(NOW() - init_date )) <a.min_storage) THEN b.eggs_executed ELSE 0 END ) as aclimatized,
								SUM(CASE WHEN (EXTRACT(DAY FROM(NOW() - init_date )) >=a.min_storage and EXTRACT(DAY FROM(NOW() - init_date )) <a.max_storage) THEN b.eggs_executed ELSE 0 END ) as suitable,
								SUM(CASE WHEN (EXTRACT(DAY FROM(NOW() - init_date )) >=a.max_storage) THEN b.eggs_executed ELSE 0 END ) as expired,
								(
									SUM(CASE WHEN (a.acclimatized= true and EXTRACT(DAY FROM(NOW() - init_date )) <a.min_storage) then b.eggs_executed else 0 end )+
									SUM(CASE WHEN (a.suitable= true and EXTRACT(DAY FROM(NOW() - init_date )) >=a.min_storage and EXTRACT(DAY FROM(NOW() - init_date )) <a.max_storage) then b.eggs_executed else 0 end )+
									SUM(CASE WHEN (a.expired= true and EXTRACT(DAY FROM(NOW() - init_date )) >=a.max_storage) then b.eggs_executed else 0 end )
								) as available

						FROM    osincubatorplant a 
								LEFT JOIN txeggs_storage b on a.incubator_plant_id = b.incubator_plant_id

						WHERE   partnership_id = $1
								and init_date <= NOW() 
								And b.eggs_executed is not null 

						group by a.incubator_plant_id`,[partnership_id]); */
};

exports.DBgetForceProject= function(partnership_id){
    return conn.db.any(`
						SELECT 	a.incubator_plant_id, a.name, 0 as aclimatized
								, 0 as suitable, 0 as expired, 0 as total
								, 0 as available, 0 as weighted_days

						FROM    osincubatorplant a 
								LEFT JOIN txeggs_storage b on a.incubator_plant_id = b.incubator_plant_id

						WHERE   partnership_id = $1
								And b.eggs_executed is not null 

						group by a.incubator_plant_id, a.name`, [partnership_id]);
};

exports.DBgetWeightedDays= function(){
    return conn.db.any(`
								select 	sub.incubator_plant_id
										, sum(days) suma_dias
										, sum(diff) suma_diffs
										, sum(days * diff) as numerador
								from (
										SELECT 	b.incubator_plant_id
														,a.eggs_storage_id
														,b.init_date
														,(EXTRACT(DAY FROM(NOW() - b.init_date ))) as days
														,((SUM(CASE WHEN a.type_movements='ingreso' THEN a.quantity ELSE 0 END )) - (SUM(CASE WHEN a.type_movements='egreso' THEN a.quantity ELSE 0 END ))) as diff

										FROM txeggs_movements a
												LEFT JOIN txeggs_storage b on a.eggs_storage_id = b.eggs_storage_id  
										group by b.incubator_plant_id ,a.eggs_storage_id, b.init_date
								) as sub
								group by sub.incubator_plant_id`);
};
exports.DBgetMovementsByDescription= function(description){
    return conn.db.any(`SELECT eggs_movements_id, fecha_movements, lot, quantity, type_movements, eggs_storage_id, description_adjustment
							FROM public.txeggs_movements where description_adjustment = $1`, [description]);
};

exports.DBfindEntryEggs = function(partnership_id, incubator_plant_id, dateDesde, dateHasta) {

    return conn.db.any(`select DISTINCT b.eggs_storage_id, a.incubator_plant_id, a.name, b.lot, b.init_date, b.eggs_executed, c.type_movements, c.fecha_movements, c.quantity,
							(CASE WHEN c.fecha_movements IS NULL and c.quantity IS NULL THEN true ELSE false END) AS available
							from    osincubatorplant a 
									LEFT JOIN txeggs_storage b on a.incubator_plant_id = b.incubator_plant_id
									LEFT JOIN txeggs_movements c on b.eggs_storage_id = c.eggs_storage_id
							WHERE   a.partnership_id = $1
									and b.incubator_plant_id= $2
									and init_date >= $3
									and init_date <= $4
									And b.eggs_executed is not null
									and (c.type_movements= 'ingreso' or c.type_movements is null)
									and b.origin is null
									and c.description_adjustment is null
							ORDER BY b.eggs_storage_id ASC`,
    [partnership_id, incubator_plant_id, dateDesde, dateHasta]);
    // return conn.db.any(`    select  b.eggs_storage_id, a.incubator_plant_id, a.name, b.lot, b.init_date, b.eggs_executed, c.type_movements, c.fecha_movements, c.quantity,
    // 						(CASE WHEN c.fecha_movements IS NULL and c.quantity IS NULL THEN true ELSE false END) AS available
    // 						from    osincubatorplant a 
    // 								LEFT JOIN txeggs_storage b on a.incubator_plant_id = b.incubator_plant_id
    // 								LEFT JOIN txeggs_movements c on b.eggs_storage_id = c.eggs_storage_id
    // 						WHERE   a.partnership_id = $1
    // 								and b.incubator_plant_id= $2
    // 								and init_date >= $3
    // 								and init_date <= $4
    // 								And b.eggs_executed is not null
    // 								and (c.type_movements= 'ingreso' or c.type_movements is null)
    // 						ORDER BY b.eggs_storage_id ASC `,
    // 				[partnership_id, incubator_plant_id, dateDesde, dateHasta]);
};
exports.DBfindAjustes = function(eggs_storage_id) {

    return conn.db.any("SELECT description_adjustment, lot, TO_CHAR(fecha_movements, 'DD/MM/YYYY') as fecha_movements, quantity FROM txeggs_movements where eggs_storage_id = $1 and type_movements = 'ingreso' and description_adjustment is not null",
        [eggs_storage_id]);
    // return conn.db.any(`    select  b.eggs_storage_id, a.incubator_plant_id, a.name, b.lot, b.init_date, b.eggs_executed, c.type_movements, c.fecha_movements, c.quantity,
    // 						(CASE WHEN c.fecha_movements IS NULL and c.quantity IS NULL THEN true ELSE false END) AS available
    // 						from    osincubatorplant a 
    // 								LEFT JOIN txeggs_storage b on a.incubator_plant_id = b.incubator_plant_id
    // 								LEFT JOIN txeggs_movements c on b.eggs_storage_id = c.eggs_storage_id
    // 						WHERE   a.partnership_id = $1
    // 								and b.incubator_plant_id= $2
    // 								and init_date >= $3
    // 								and init_date <= $4
    // 								And b.eggs_executed is not null
    // 								and (c.type_movements= 'ingreso' or c.type_movements is null)
    // 						ORDER BY b.eggs_storage_id ASC `,
    // 				[partnership_id, incubator_plant_id, dateDesde, dateHasta]);
};

exports.DBfindEntryEggs2 = function(partnership_id, incubator_plant_id, dateDesde, dateHasta) {

    return conn.db.any(`select  b.eggs_storage_id, a.incubator_plant_id, a.name, b.lot, b.init_date, b.eggs_executed, c.type_movements, c.fecha_movements, c.quantity,
		(CASE WHEN c.fecha_movements IS NULL and c.quantity IS NULL THEN true ELSE false END) AS available
		from    osincubatorplant a 
				LEFT JOIN txeggs_storage b on a.incubator_plant_id = b.incubator_plant_id and origin = 1
				LEFT JOIN txeggs_movements c on b.eggs_storage_id = c.eggs_storage_id
		WHERE   a.partnership_id = $1
				and b.incubator_plant_id= $2
				and init_date >= $3
				and init_date <= $4
				And b.eggs_executed is not null
				and (c.type_movements= 'ingreso' or c.type_movements is null)
		ORDER BY b.eggs_storage_id ASC`,
    [partnership_id, incubator_plant_id, dateDesde, dateHasta]);
};

exports.DBfindEntryEggsWithLot = function(partnership_id, incubator_plant_id, dateDesde, dateHasta, lot) {

    return conn.db.any(`    select  b.eggs_storage_id, a.incubator_plant_id, a.name, b.lot, b.init_date, b.eggs_executed, c.type_movements, c.fecha_movements, c.quantity,
								(CASE WHEN c.fecha_movements IS NULL and c.quantity IS NULL THEN true ELSE false END) AS available
								from    osincubatorplant a 
										LEFT JOIN txeggs_storage b on a.incubator_plant_id = b.incubator_plant_id
										LEFT JOIN txeggs_movements c on b.eggs_storage_id = c.eggs_storage_id
								WHERE   a.partnership_id = $1
										and b.incubator_plant_id= $2
										and init_date >= $3
										and init_date <= $4
										And b.eggs_executed is not null
										and (c.type_movements= 'ingreso' or c.type_movements is null)
										and b.origin is null
										and c.description_adjustment is null
										and b.lot = $5
								ORDER BY b.eggs_storage_id ASC `,
    [partnership_id, incubator_plant_id, dateDesde, dateHasta, lot]);
};
exports.DBfindEntryEggsWithLot2 = function(partnership_id, incubator_plant_id, dateDesde, dateHasta, lot) {

    return conn.db.any(`    select  b.eggs_storage_id, a.incubator_plant_id, a.name, b.lot, b.init_date, b.eggs_executed, c.type_movements, c.fecha_movements, c.quantity,
								(CASE WHEN c.fecha_movements IS NULL and c.quantity IS NULL THEN true ELSE false END) AS available
								from    osincubatorplant a 
										LEFT JOIN txeggs_storage b on a.incubator_plant_id = b.incubator_plant_id and origin = 1 
										LEFT JOIN txeggs_movements c on b.eggs_storage_id = c.eggs_storage_id
								WHERE   a.partnership_id = $1
										and b.incubator_plant_id= $2
										and init_date >= $3
										and init_date <= $4
										And b.eggs_executed is not null
										and (c.type_movements= 'ingreso' or c.type_movements is null)
										and b.lot = $5
								ORDER BY b.eggs_storage_id ASC `,
    [partnership_id, incubator_plant_id, dateDesde, dateHasta, lot]);
};

exports.DBaddEntryEggs = function(records){

    cs = conn.pgp.helpers.ColumnSet(["eggs_storage_id", "fecha_movements",  "lot" , "quantity" , "type_movements"],
        {table: "txeggs_movements", schema: "public"});
    return conn.db.none(conn.pgp.helpers.insert(records, cs));
};
exports.DBfindMaxLotPlexus = function(plexus) {
    let promise = conn.db.one("select MAX(CAST((substring(lot, 2, 10)) AS INTEGER)) from txeggs_movements where description_adjustment = $1", [plexus]);
    return promise;
};
exports.DBaddAdjustEntryEggs = function(records){

    cs = conn.pgp.helpers.ColumnSet(["eggs_storage_id", "fecha_movements",  "lot" , "quantity" , "type_movements", "description_adjustment"],
        {table: "txeggs_movements", schema: "public"});
    return conn.db.none(conn.pgp.helpers.insert(records, cs));
};
exports.DBaddAdjustEgressEggs = function(records){

    cs = conn.pgp.helpers.ColumnSet(["eggs_storage_id", "fecha_movements",  "lot" , "quantity" , "type_movements", "description_adjustment"],
        {table: "txeggs_movements", schema: "public"});
    return conn.db.none(conn.pgp.helpers.insert(records, cs));
};
exports.DBaddNewEntryEggs = function(records){
    return conn.db.any(`INSERT INTO public.txeggs_storage(incubator_plant_id, scenario_id, breed_id, init_date, end_date, lot, 
						eggs, eggs_executed, origin) VALUES $1 RETURNING eggs_storage_id`,
    Inserts("${incubator_plant_id},${scenario_id},${breed_id}, ${init_date}, ${end_date}, ${lot},"+ 
		"${eggs}, ${eggs_executed}, ${origin}",records));
    // cs = conn.pgp.helpers.ColumnSet(['incubator_plant_id', 'scenario_id',  'breed_id' , 'init_date' , 'end_date', 'lot', 'eggs', 'eggs_executed', 'origin'],
    // 								{table: 'txeggs_storage', schema: 'public'});
    // return conn.db.none(conn.pgp.helpers.insert(records, cs));
};

exports.DBfindMaxLotComp = function() {
    let promise = conn.db.one("select MAX(CAST((substring(lot, 2, 10)) AS INTEGER)) from txeggs_storage where origin = 1");
    return promise;
};


exports.DBfindProjectIncubator = function(scenario_id, init_date, end_date, incubator_plant_id, breed_id) {

    console.log("llegpo al modelo");
    console.log(scenario_id);
    console.log(init_date);
    console.log(end_date);
    console.log(incubator_plant_id);
    console.log(breed_id);

    return conn.db.any(`select * ,
		(SELECT case when sum(quantity)> 0 then sum(quantity) else 0 end as residue FROM public.txincubator_lot
		WHERE eggs_movements_id = a.eggs_movements_id)
	from txeggs_movements a
	left join txeggs_storage b on a.eggs_storage_id = b.eggs_storage_id 
	where b.scenario_id = $1 and a.fecha_movements between $2 and $3 and b.incubator_plant_id = $4 and 
	b.breed_id = $5 and type_movements = 'ingreso' and (a.description_adjustment is null or a.description_adjustment = 'Compra')`, [scenario_id, init_date, end_date, incubator_plant_id, breed_id]);

/*
	return conn.db.any(" SELECT a.incubator_plant_id, a.name , sum(b.eggs_executed) as total, "+
						" SUM(CASE WHEN (EXTRACT(DAY FROM(NOW() - init_date )) <=$2) THEN b.eggs_executed ELSE 0 END ) as aclimatized, "+
						" SUM(CASE WHEN (EXTRACT(DAY FROM(NOW() - init_date )) >$3 and EXTRACT(DAY FROM(NOW() - init_date )) <$4) THEN b.eggs_executed ELSE 0 END ) as suitable, "+
						" SUM(CASE WHEN (EXTRACT(DAY FROM(NOW() - init_date )) >=$5) THEN b.eggs_executed ELSE 0 END ) as expired "+
						" FROM  osincubatorplant a  "+
						" LEFT JOIN txeggs_storage b on a.incubator_plant_id = b.incubator_plant_id "+
						" WHERE partnership_id = $1 "+
						" and init_date <= NOW()  "+
						" And b.eggs_executed is not null "+
						" group by a.incubator_plant_id ",
						[partnership_id, rangeAclimatized, range1Suitable, range2Suitable, rangeExpired]);*/
};
exports.DBfindProjectIncubatorAll = function(scenario_id, init_date, end_date, incubator_plant_id, breed_id) {

    console.log("llegpo al modelo");
    console.log(scenario_id);
    console.log(init_date);
    console.log(end_date);
    console.log(incubator_plant_id);
    console.log(breed_id);

    return conn.db.any(`SELECT * ,
						(SELECT case when sum(quantity)> 0 then sum(quantity) else 0 end as residue FROM public.txincubator_lot
		WHERE eggs_movements_id = a.eggs_movements_id), 
						w.name as name 
						FROM txeggs_movements a
						LEFT JOIN txeggs_storage b on a.eggs_storage_id = b.eggs_storage_id 
						LEFT JOIN mdbreed w on w.breed_id = b.breed_id 
						WHERE b.scenario_id = $1 AND a.fecha_movements between $2 AND $3 AND b.incubator_plant_id = $4 
						AND type_movements = 'ingreso'`, [scenario_id, init_date, end_date, incubator_plant_id]);

};

exports.DBfindProjectIncubatorPlexus = function(scenario_id, init_date, end_date, incubator_plant_id, plexus) {

    console.log("llegpo al modelo");
    console.log(scenario_id);
    console.log(init_date);
    console.log(end_date);
    console.log(incubator_plant_id);
    console.log(plexus);

    return conn.db.any(`select *, a.lot,
		(SELECT case when sum(execution_quantity)> 0 then sum(execution_quantity) else 0 end as residue FROM public.txprogrammed_eggs WHERE eggs_movements_id = a.eggs_movements_id)
	from txeggs_movements a
	left join txeggs_storage b on a.eggs_storage_id = b.eggs_storage_id
	where b.scenario_id = $1 and a.fecha_movements between $2 and $3 and b.incubator_plant_id = $4 and 
	a.description_adjustment = $5 and type_movements = 'ingreso' `, [scenario_id, init_date, end_date, incubator_plant_id, plexus]);
};


exports.DBgetMovementsByEntry= function(eggs_storage_id, type_movements) {

    return conn.db.any(`   select eggs_movements_id, fecha_movements, lot, quantity, 
								type_movements, eggs_storage_id, description_adjustment 
								from public.txeggs_movements
								where eggs_storage_id= $1
								and type_movements= $2
								ORDER BY eggs_movements_id ASC `,
    [eggs_storage_id, type_movements]);

	   /*  return conn.db.any(`   select eggs_movements_id, use_date as fecha_movements,
								lot_breed as lot, execution_quantity as quantity, 
								eggs_storage_id, 'egreso' as type_movements
								from public.txprogrammed_eggs
								where eggs_storage_id= $1
								ORDER BY eggs_movements_id ASC `,
						[eggs_storage_id, type_movements]); */
};

exports.DBgetOutMovementsByEntryForDate= function(eggs_storage_id, type_movements, init_date, end_date){
    return conn.db.any(`   select eggs_movements_id, fecha_movements, lot, quantity, type_movements, eggs_storage_id
								from public.txeggs_movements
								where eggs_storage_id= $1
								and type_movements= $2
								and fecha_movements >= $3
								and fecha_movements <= $4
								ORDER BY fecha_movements ASC`,
    [eggs_storage_id, type_movements, init_date, end_date]);
},

exports.DBgetOutMovementsForDate= function(type_movements, init_date, end_date){

    return conn.db.any(`   select eggs_movements_id, fecha_movements, lot, quantity, 
								eggs_storage_id, type_movements
								from public.txeggs_movements
								where type_movements= $1
								and fecha_movements >= $2
								and fecha_movements <= $3
								ORDER BY fecha_movements ASC`,
    [type_movements, init_date, end_date]);
};


exports.DBgetOutMovementsForDateWithLot= function(type_movements, init_date, end_date, slot){

    return conn.db.any(`   select eggs_movements_id, fecha_movements, lot, quantity, 
							eggs_storage_id, type_movements
							from public.txeggs_movements
							where type_movements= $1
							and fecha_movements >= $2
							and fecha_movements <= $3
							and lot= $4
							ORDER BY fecha_movements ASC`,
    [type_movements, init_date, end_date, slot]);
};

exports.DBgetAllLots= function(incubator_plant_id){

    return conn.db.any(`   select   lot, count(*) as total
							from    txeggs_storage
							where   eggs_executed is not null
									and incubator_plant_id= $1
							group by lot
							order by lot`,
    [incubator_plant_id]);
};