sap.ui.define([
    "broilersPlanningM/controller/BaseController",
    "jquery.sap.global",
    "sap/ui/model/Filter",
    "sap/ui/core/Fragment",
    "sap/ui/model/json/JSONModel",
    "sap/m/MessageToast",
    "sap/m/Dialog",
    "sap/m/Button",
    "sap/m/Text"
], function(BaseController, jQuery, Filter, Fragment, JSONModel, MessageToast, Dialog, Button, Text) {
    "use strict";
    const breedingStage = 1; /*Etapa para Engorde*/
    return BaseController.extend("broilersPlanningM.controller.Detail", {

        onInit: function() {
            this.setFragments();
            this.getRouter().getRoute("detail").attachPatternMatched(this._onRouteMatched, this);
        },

        _onRouteMatched: function(oEvent) {
            var oArguments = oEvent.getParameter("arguments");

            this.index = oArguments.id;
            let oView= this.getView();
            let ospartnership = this.getModel("ospartnership");
            oView.byId("tabBar").setSelectedKey("tabParameter");
            oView.byId("tableBreed").addEventDelegate({
                onAfterRendering: oEvent=>{
                    console.log("victor te amo!");
                }
            });
    
    
            this.programmedPopover = sap.ui.xmlfragment("broilersPlanningM.view.programmed.ProgrammedPopover", this);
            this.getView().addDependent(this.programmedPopover);
    
    
            if(ospartnership.getProperty("/records").length>0){
                let partnership_id = ospartnership.getProperty("/selectedRecords/partnership_id");
                this.onRead(partnership_id);
            }
            else{
                this.reloadPartnership()
                    .then(data => {
                        if(data.length>0){
                            let obj= ospartnership.getProperty("/selectedRecords/");
                            if(obj){
                                this.onRead(obj.partnership_id);
                            }
                            else{
                                MessageToast.show("no existen empresas cargadas en el sistema", {
                                    duration: 3000,
                                    width: "20%"
                                });
                                console.log("err: ", data);
                            }
                        }
                        else{
                            MessageToast.show("ha ocurrido un error al cargar el inventario", {
                                duration: 3000,
                                width: "35%"
                            });
                            console.log("err: ", data);
                        }
                    });
            } 



            this.getView().byId("__header0").bindElement("ospartnership>/records/" + this.index + "/");
            this.onRead(this.index);

        },

        showProgrammedLots: async function(oEvent) {
            const mdprogrammed = this.getView().getModel("mdprogrammed");
            const programming = oEvent.getSource().getBindingContext("mdprogrammed").getObject();
            console.log("Los programados---->", programming);

            const link = oEvent.getSource();
            const response = await fetch("/broilerdetail/findIncubatorLotByBroilerLot", {
                headers: {
                    "Content-type": "application/json"
                },
                method: "POST",
                body: JSON.stringify({
                    broiler_detail_id: programming.broiler_detail_id
                })
            });

            if (!response.ok) {
                console.log("error");
                console.log(response);
            }
            else {
                const res = await response.json();
                mdprogrammed.setProperty("/popover", res.data);
                console.log(mdprogrammed.getProperty("/popover"));
                this.programmedPopover.openBy(link);
            }

            // programmed_eggs/findAllDateQuantityFarmProduct
        },


        reloadPartnership: function(){
            let util = this.getModel("util");
            let that = this;
            let ospartnership = this.getModel("ospartnership");

            util.setProperty("/busy/", true);
            ospartnership.setProperty("/records", []);

            let url = util.getProperty("/serviceUrl") +util.getProperty("/" + util.getProperty("/service") + "/getPartnership");
            let method = "GET";
            let data = {};
            return new Promise((resolve, reject) => {
                function getPartnership(res) {
                    util.setProperty("/busy/", false);
                    ospartnership.setProperty("/records/", res.data);
                    if(res.data.length>0){
                        let obj= res.data[0];
                        obj.index= 0;
                        ospartnership.setProperty("/selectedRecords/", obj);
                        ospartnership.setProperty("/name", obj.name);
                        ospartnership.setProperty("/address", obj.address);
                    }
                    resolve(res.data);
                }

                function error(err) {
                    console.log(err);
                    ospartnership.setProperty("/selectedRecords/", []);
                    util.setProperty("/error/status", err.status);
                    util.setProperty("/error/statusText", err.statusText);
                    reject(err);
                }

                /*Envía la solicitud*/
                this.sendRequest.call(this, url, method, data, getPartnership, error, error);
            });
        },

        onRead: async function(index) {
            let ospartnership = this.getModel("ospartnership"),
                mdscenario = this.getModel("mdscenario"),
                oView = this.getView();

            oView.byId("tabBar").setSelectedKey("kTabParameter");

            let activeS = await this.activeScenario();
            mdscenario.setProperty("/scenario_id", activeS.scenario_id);
            mdscenario.setProperty("/name", activeS.name);

            ospartnership.setProperty("/selectedRecordPath/", "/records/" + index);
            ospartnership.setProperty("/selectedRecord/", ospartnership.getProperty(ospartnership.getProperty("/selectedRecordPath/")));

            this.onFarmLoad();
            this.onBreedLoad();

            let util = this.getModel("util"),
                that = this,
                mdprojected = this.getModel("mdprojected"),
                mdprogrammed = this.getModel("mdprogrammed");

            ospartnership.setProperty("/selectedPartnership/partnership_index", index);

            let process_info = await this.processInfo(),
                mdprocess = this.getModel("mdprocess");

            mdprocess.setProperty("/records", process_info.data);
            //mdprojected.setProperty("/records", findScenario.data);
            that.hideButtons(false, false, false, false);
            this.onFarmLoad();
        },


        validateIntInput: function (o) {
            let input= o.getSource();
            let length = 10;
            let value = input.getValue();
            let regex = new RegExp(`/^[0-9]{1,${length}}$/`);

            if (regex.test(value)) {
                console.log();
                return true;
            }
            else {
                let aux = value
                    .split("")
                    .filter(char => {
                        if (/^[0-9]$/.test(char)) {
                            if (char !== ".") {
                                return true;
                            }
                        }
                    })
                    .join("");
                value = aux.substring(0, length);
                input.setValue(value);
                return false;
            }
        },


        onValidProgrammedQuantity: function(o){
            let input= o.getSource();
            let length = 10;
            let value = input.getValue();
            console.log("valor: " + value);
            let regex = new RegExp(`/^[0-9]{1,${length}}$/`);

            if (regex.test(value)){
                console.log("entro if");
                return true;
            }
            else {
                console.log("entro else");
                let aux = value.split("").filter(char => {
                    if (/^[0-9]$/.test(char)){
                        if (char !== ".") {
                            return true;
                        }
                    }
                }).join("");
                value = aux.substring(0, length);
                input.setValue(value);
                this.validQuantityShed(value);
                return false;
            }
        },

        validQuantityShed: function(value){
            let mdshed = this.getModel("mdshed");
            let selectedShed = sap.ui.getCore().byId("selectShed").getSelectedKey();
            let array1 = mdshed.getProperty("/records");
            let mdprogrammed = this.getModel("mdprogrammed");
            let programmed_residue = mdprogrammed.getProperty("/programmed_residue");
            // console.log(programmed_residue)
        
            var found = array1.find(function(element) {
                return element.shed_id == selectedShed;
            });
            mdprogrammed.setProperty("/addBtn", true);
            // value= parseInt(value);
        
            // debugger;
            if(value === null || value ===""){//VALIDACION PARA ENTRADA NULA
                mdprogrammed.setProperty("/name/state", "None");
                mdprogrammed.setProperty("/name/stateText", "");
                mdprogrammed.setProperty("/addBtn", false);
            }
            else if(parseInt(value)===0){//VALIDACION PARA ENTRADA IGUAL A 0
                mdprogrammed.setProperty("/name/state", "Error");
                mdprogrammed.setProperty("/name/stateText", "La cantidad programada debe ser mayor a 0");
                mdprogrammed.setProperty("/addBtn", false);
            }
            else if(parseInt(value) > programmed_residue){//VALIDACION PARA ENTRADA MAYOR AL RESIDUO
                mdprogrammed.setProperty("/name/state", "Warning");
                mdprogrammed.setProperty("/name/stateText", "La cantidad programada supera al saldo");
            }
            else if(parseInt(value) > found.capmax) {//VALIDACION PARA ENTRADA MAYOR A CAPACID. MAX
                mdprogrammed.setProperty("/name/state", "Warning");
                mdprogrammed.setProperty("/name/stateText", "La cantidad programada supera la capacidad del galpon");
            }
            else if(parseInt(value)< parseInt(found.capmin)){//VALIDACION PARA ENTRADA MENOR A CAPAC. MIN
                mdprogrammed.setProperty("/name/state", "Warning");
                mdprogrammed.setProperty("/name/stateText", "La cantidad programada esta por debajo de la capacidad mínima del galpón");
            }
            else{
                mdprogrammed.setProperty("/name/state", "None");
                mdprogrammed.setProperty("/name/stateText", "");
            }

        },

        reports: function()
        {
            var mdreports = this.getModel("mdreports");
            console.log("presione el boton de reportes");
            let date1 = this.getView().byId("sd").mProperties.value,
                date2 = this.getView().byId("sd2").mProperties.value,
                breed_id = this.getView().byId("breedSelect2").getSelectedKey();

            let aDate = date1.split("-"),
                init_date = `${aDate[0]}/${aDate[1]}/${aDate[2]}`;

            let aDate2 = date2.split("-"),
                end_date = `${aDate2[0]}/${aDate2[1]}/${aDate2[2]}`;

      
            if (date1 === null || date1== "" || date2 === null || date2== "" ){
                // console.log("fechas vacias")
                MessageToast.show("No se pueden consultar fechas vacías", {
                    duration: 3000,
                    width: "20%"
                });
            }else{
                console.log("las fechas");
                console.log(date1);
                console.log(date2);
                console.log(breed_id);
                console.log("EL MODELO CON FECHAS");
                console.log(mdreports);
                let serverName = "/reports/broiler";

                fetch(serverName, {
                    headers: {
                        "Content-Type": "application/json"
                    },
                    method: "POST",
                    body: JSON.stringify({
                        date1: date1,
                        date2: date2,
                        breed_id: breed_id
                    })
                })
                    .then(
                        function(response) {
                            if (response.status !== 200) {
                                console.log("Looks like there was a problem. Status Code: " +
                  response.status);
                                return;
                            }

                            response.json().then(function(res) {
                                console.log("la respuesta despues de reportes");
                                console.log(res);
                                mdreports.setProperty("/records", res.data);
                                console.log("la longitud");
                                console.log(res.data.length);
                                mdreports.setProperty("/raza", res.raza);
                                if (res.data.length > 0) 
                                {
                                    mdreports.setProperty("/reportsBtn", true);
                                    mdreports.setProperty("/desde", init_date);
                                    mdreports.setProperty("/hasta", end_date);
                                    mdreports.setProperty("/visible", true);
                  
                                }
                                else
                                {
                                    mdreports.setProperty("/reportsBtn", false);
                                    mdreports.setProperty("/visible", false);
                                }
                                resolve(res);
                            });
                        }
                    )
                    .catch(function(err) {
                        console.log("Fetch Error :-S", err);
                    });
            }

      
        },

        generatedCSV: function()
        {
            var mdreports = this.getModel("mdreports").getProperty("/records");
            console.log(mdreports);
            this.arrayObjToCsv(mdreports);
            // this.arrayObjToCsv();
        },

        arrayObjToCsv: function (ar) {
        //comprobamos compatibilidad
            let breed_id = this.getView().byId("breedSelect").getSelectedKey();
            let array = [];
            if(window.Blob && (window.URL || window.webkitURL)){
                var contenido = "",
                    d = new Date(),
                    blob,
                    reader,
                    save,
                    clicEvent;
                //creamos contenido del archivo
                if(breed_id === "Todas"){
                    array = ["Fecha Programada",  "Cantidad Programada", "Fecha Ejecutada", "Cantidad Ejecutada", "Lote", "Raza", "Granja", "Núcleo", "Galpón", "Granja Ejecutada", "Núcleo Ejecutado", "Galpón Ejecutado","Variación Cantidad", "Variación Dias"];
                }else{
                    array = ["Fecha Programada",  "Cantidad Programada", "Fecha Ejecutada", "Cantidad Ejecutada", "Lote", "Granja", "Núcleo", "Galpón", "Granja Ejecutada", "Núcleo Ejecutado", "Galpón Ejecutado","Variación Cantidad", "Variación Dias"];
                }
                console.log(array);
                console.log("EL ARRAY");
                console.log(ar);
                for (var i = 0; i < ar.length; i++) {
             

                    console.log("se supone que las cabeceras");
                    console.log(Object.keys(ar[i]));
                    //construimos cabecera del csv
                    if (i == 0)
                        contenido += array.join(";") + "\n";
                    //resto del contenido
                    contenido += Object.keys(ar[i]).map(function(key){
                        return ar[i][key];
                    }).join(";") + "\n";
                }
                console.log(contenido);
                //creamos el blob
                blob =  new Blob(["\ufeff", contenido], {type: "text/csv"});
                //creamos el reader
                var reader = new FileReader();
                reader.onload = function (event) {
                //escuchamos su evento load y creamos un enlace en dom
                    save = document.createElement("a");
                    save.href = event.target.result;
                    save.target = "_blank";
                    //aquí le damos nombre al archivo
                
                    save.download = "salida.csv";
                

                    try {
                    //creamos un evento click
                        clicEvent = new MouseEvent("click", {
                            "view": window,
                            "bubbles": true,
                            "cancelable": true
                        });
                    } catch (e) {
                    //si llega aquí es que probablemente implemente la forma antigua de crear un enlace
                        clicEvent = document.createEvent("MouseEvent");
                        clicEvent.initEvent("click", true, true);
                    }
                    //disparamos el evento
                    save.dispatchEvent(clicEvent);
                    //liberamos el objeto window.URL
                    (window.URL || window.webkitURL).revokeObjectURL(save.href);
                };
                //leemos como url
                reader.readAsDataURL(blob);
            }else {
            //el navegador no admite esta opción
                alert("Su navegador no permite esta acción");
            }
        },
























        onPress: async function(){
            console.log("OnPress");
            this.getView().byId("projectedTable").removeSelections();
            let that = this,
                mdprojected = this.getModel("mdprojected"),
                mdscenario =  this.getModel("mdscenario"),
                mdprogrammed = this.getModel("mdprogrammed"),
                partnership_id = this.getView().getModel("ospartnership").getProperty("/records/" + this.index + "/partnership_id"),
                util = this.getModel("util"),
                scenario_id = mdscenario.getProperty("/scenario_id"),
                scheduled_date = this.getView().byId("scheduled_date").mProperties.value,
                breed_id = this.getView().byId("breedSelect").getSelectedKey();

            const serverName = util.getProperty("/serviceUrl") + util.getProperty("/" + util.getProperty("/service") + "/findprojectedbroiler");
            console.log(serverName);
            fetch(serverName, {
                method: "POST",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify({
                    scenario_id: scenario_id,
                    _date: scheduled_date,
                    partnership_id: partnership_id,
                    breed_id: breed_id
                })
            })
                .then(
                    function(response) {
                        if (response.status !== 200) {
                            console.log("Looks like there was a problem. Status Code: " +
                    response.status);
                            return;
                        }

                        response.json().then(function(res) {
                            console.log(res);
                            let records=res.data;
                            records.forEach(element => {
                                element.fProjected = that.getDia(element.projected_date);
                            });
                            console.log(records);
                            mdprojected.setProperty("/records", res.data);
                            mdprogrammed.setProperty("/product/records", res.product);
                            console.log(mdprogrammed);
                            mdprojected.refresh();
                            console.log("mdprojected en el boton buscar de proyectado" );
                            console.log(mdprojected);
                            console.log(mdprojected);
                            mdprogrammed.refresh();

                        });
                    }
                )
                .catch(function(err) {
                    console.log("Fetch Error :-S", err);
                });
        },
        processInfo: function() {

            let util = this.getModel("util"),
                mdprocess = this.getModel("mdprocess"),
                that = this;

            const serverName = util.getProperty("/serviceUrl") + util.getProperty("/" + util.getProperty("/service") + "/findProcessBreedByStage");
            return new Promise((resolve, reject) => {
                fetch(serverName, {
                    method: "POST",
                    headers: {
                        "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
                    },
                    body: "stage_id=" + breedingStage
                })
                    .then(
                        function(response) {
                            if (response.status !== 200) {
                                console.log("Looks like there was a problem. Status Code: " +
                  response.status);
                                return;
                            }

                            response.json().then(function(res) {
                                resolve(res);
                            });
                        }
                    )
                    .catch(function(err) {
                        console.log("Fetch Error :-S", err);
                    });
            });

        },
        findProjected: function() {
            console.log("llego a findPRojected");
            let util = this.getModel("util"),
                partnership_id = this.getView().getModel("ospartnership").getProperty("/records/" + this.index + "/partnership_id"),
                scenario_id = this.getModel("mdscenario").getProperty("/scenario_id");
            console.log(partnership_id);
            const serverName = util.getProperty("/serviceUrl") + util.getProperty("/" + util.getProperty("/service") + "/findHousingByStage");
            return new Promise((resolve, reject) => {
                fetch(serverName, {
                    method: "POST",
                    headers: {
                        "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
                    },
                    body: "stage_id=" + breedingStage + "&partnership_id=" + partnership_id + "&scenario_id=" + scenario_id
                })
                    .then(
                        function(response) {
                            if (response.status !== 200) {
                                console.log("Looks like there was a problem. Status Code: " +
                  response.status);
                                return;
                            }

                            response.json().then(function(res) {

                                console.log(res);
                                resolve(res);


                            });
                        }
                    )
                    .catch(function(err) {
                        console.log("Fetch Error :-S", err);
                    });
            });
        },
        activeScenario: function() {

            let util = this.getModel("util"),
                mdscenario = this.getModel("mdscenario"),
                that = this;
            const serverName = util.getProperty("/serviceUrl") + util.getProperty("/" + util.getProperty("/service") + "/activeScenario");

            return new Promise((resolve, reject) => {
                fetch(serverName)
                    .then(
                        function(response) {
                            if (response.status !== 200) {
                                console.log("Looks like there was a problem. Status Code: " +
                  response.status);
                                return;
                            }

                            response.json().then(function(res) {
                                resolve(res);
                            });
                        }
                    )
                    .catch(function(err) {
                        console.log("Fetch Error :-S", err);
                    });

            });
        },
        onTabSelection: async function(ev) {
            var mdprogrammed = this.getModel("mdprogrammed");
            var mdprojected = this.getModel("mdprojected");
            var mdexecuted = this.getModel("mdexecuted");
            var mdbreed = this.getModel("mdbreed");
            let recordsB = mdbreed.getProperty("/records"); 
            var mdfarms = this.getModel("mdfarms");
            let mdreports = this.getModel("mdreports");
            let util = this.getModel("util");
            console.log("llego ***");
            //osfarm.setProperty("/saveBtn", false);
            var selectedKey = ev.getSource().getSelectedKey();

            if (selectedKey === "kTabParameter") {
                console.log("entre parameters");
                this.getView().byId("projectedTable").removeSelections();
                // this.getView().getModel("mdprojected").setProperty("/records",[]);
                this.getView().getModel("mdprogrammed").setProperty("/rProgrammed/enabledTab",false);
                this.getView().getModel("mdexecuted").setProperty("/rExecuted/enabledTab",false);
                this.getView().byId("projectedTable").removeSelections();
                this.getView().byId("idtable2").removeSelections();
                this.getView().byId("idexecuted").removeSelections();
                this.hideButtons(false, false, false, false);
            }
            if (selectedKey === "kTabProjected") {
                console.log("entre projected");
                this.hideButtons(true, false, false, false);
                this.getView().byId("projectedTable").removeSelections();
                // this.getView().getModel("mdprojected").setProperty("/records",[]);
                this.getView().byId("idtable2").removeSelections();
                mdprogrammed.setProperty("/selectedRecords", []);
                this.getView().byId("bProj").firePress();
                // let findScenario = await this.findProjected();
                this.getView().getModel("mdprogrammed").setProperty("/records",[]);
        
                this.getView().getModel("mdprogrammed").setProperty("/rProgrammed/enabledTab",false);
                this.getView().getModel("mdexecuted").setProperty("/rExecuted/enabledTab",false);
        
                // mdprojected.setProperty("/records", findScenario.data);
        
                this.getView().byId("idtable2").removeSelections();
                this.getView().byId("idexecuted").removeSelections();

            }
            if (selectedKey === "ktabProgrammed") {
                this.hideButtons(false, true, false, false);
                util.setProperty("/busy", false);
                let records = mdprojected.getProperty("/records");
                this.getView().getModel("mdexecuted").setProperty("/records",[]);

                // this.getView().getModel("mdfarms").setProperty("/records",[]);
                this.getView().getModel("mdfarms").setProperty("/selectedKey","");

                // this.getView().getModel("mdcores").setProperty("/records",[]);
                this.getView().getModel("mdcores").setProperty("/selectedKey","");

                // this.getView().getModel("mdshed").setProperty("/records",[]);
                this.getView().getModel("mdshed").setProperty("/selectedKey","");

                this.getView().byId("idtable2").removeSelections();
                this.getView().byId("idexecuted").removeSelections();
                this.getView().getModel("mdexecuted").setProperty("/rExecuted/enabledTab",false);
        
        
                console.log(mdexecuted);
        
            }
            if (selectedKey === "ktabExecuted") {
                this.getView().byId("idexecuted").removeSelections();
                this.hideButtons(false,false, true, false);
                // let findExecuted = await this.findExecuted();
                // console.log(this.getView().getModel("mdfarms"))
                // console.log(this.getView().byId("selectFarm"));
                // console.log(mdexecuted)
                // let records = mdexecuted.getProperty("/records");
                // mdfarms.setProperty("/selectedKey",records[0].executedfarm_id)
                // this.onChangeFarmE()
                mdprogrammed.setProperty("/programmedsaveBtn", false);
                // if (records.length > 0) {
                //   mdexecuted.setProperty("/executionSaveBtn", true);
                // } else {
                //   mdexecuted.setProperty("/executionSaveBtn", false);
                // }
            }
            if (selectedKey === "ktabReports") {
                this.getView().byId("projectedTable").removeSelections();
                // this.getView().getModel("mdprojected").setProperty("/records",[]);
                this.getView().byId("idtable2").removeSelections();
                this.getView().getModel("mdprogrammed").setProperty("/records",[]);
                this.getView().byId("idexecuted").removeSelections();
                this.getView().getModel("mdexecuted").setProperty("/records",[]);
                this.getView().getModel("mdprogrammed").setProperty("/rProgrammed/enabledTab",false);
                this.getView().getModel("mdexecuted").setProperty("/rExecuted/enabledTab",false);
                recordsB.unshift({breed_id: "Todas", name: "Todas"});
                mdbreed.setProperty("/records",recordsB);
                mdbreed.setProperty("/value","Todas");
                var lo = mdreports.getProperty("/records");
                console.log("LA LONGITUD");
                console.log(lo.length);
                if (lo.length == 0) 
                {
                    this.hideButtons(false, false, false, false);
                }
                else
                {
                    this.hideButtons(false, false, false, true);
                }
        
            }
            if (selectedKey !== "ktabReports") {
                mdreports.setProperty("/records", []);
                this.getView().byId("sd").setValue("");
                this.getView().byId("sd2").setValue("");
                if (recordsB[0].breed_id==="Todas"){
                    recordsB.shift();
                    console.log(recordsB);
                    mdbreed.setProperty("/records",recordsB);
                }
        
            }
        },


        /*  /broilerdetail/findbroilerdetailById       */ 

        findExecuted: function(){
            let that= this,
                util = this.getModel("util"),
                mdprogrammed = this.getView().getModel("mdprogrammed"),
                mdprojected = this.getView().getModel("mdprojected"),
                mdexecuted = this.getView().getModel("mdexecuted"),
                mdfarms = this.getView().getModel("mdfarms"),
                mdcenter = this.getView().getModel("mdcenter"),
                mdshed = this.getView().getModel("mdshed"),
                hwid = mdexecuted.getProperty("/selectedRecord/housingway_detail_id");
            console.log(hwid);
            console.log("Aquì voyyyy")
            return new Promise((resolve, reject) => {
                fetch("/broilerdetail/findbroilerdetailById", {
                    method: "POST",
                    headers: {
                        "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
                    },
                    body: "broiler_detail_id=" + mdexecuted.getProperty("/selectedRecord/broiler_detail_id")
                })
                    .then(
                        function(response) {
                            if (response.status !== 200) {
                                console.log("Looks like there was a problem. Status Code: " +
                      response.status);
                                return;
                            }
      
                            response.json().then(function(res) {
                                console.log(res);
                                let records = res.data;
                                mdexecuted.setProperty("/records", records);
                                console.log(records[0].execution_quantity===null, records[0].execution_date);
                                if((records[0].execution_quantity === null ||records[0].execution_quantity === 0)&& records[0].execution_date === null){
                      
                                    mdexecuted.setProperty("/isnotexecuted",true);
                                    mdexecuted.setProperty("/isexecuted",false);
                                }else{
                                    mdexecuted.setProperty("/isnotexecuted",false);
                                    mdexecuted.setProperty("/isexecuted",true);
                                }
                                if(records[0].executedfarm_id!== null){
                                    console.log("entro farm",mdfarms);
                                    mdfarms.setProperty("/selectedKey",records[0].executedfarm_id);
                                }else{
                                    mdfarms.setProperty("/selectedKey",records[0].farm_id);
                                }
                                if(records[0].executedcenter_id!== null){
                                    console.log("entro center",mdcenter);
                                    mdcenter.setProperty("/selectedKey",records[0].executedcenter_id);
                                }else{
                                    mdcenter.setProperty("/selectedKey",records[0].center_id);
                                }
                                if(records[0].executedshed_id!== null){
                                    console.log(mdshed.getProperty("/selectedKey"));
                                    mdshed.setProperty("/selectedKey",records[0].executedshed_id);
                                    console.log("entro shed",mdshed,records[0].executedshed_id);
                                    mdshed.refresh(true);
                                }else{
                                    mdshed.setProperty("/selectedKey",records[0].shed_id);
                                }
                                console.log( records[0].execution_date);
                                if(records[0].execution_date === null){
                                    console.log("fecha o null o undefined");
                                    records[0].execution_date = records[0].scheduled_date;
                                    console.log( records[0].execution_date);
                                }
                                if(records[0].execution_quantity === null){
                                    console.log(records[0].scheduled_quantity,  records[0].execution_quantity);
                                    mdexecuted.setProperty("/execution_quantity",records[0].scheduled_quantity); 
                                    console.log(mdexecuted);
                                }else{
                                    console.log("entree");
                                    mdexecuted.setProperty("/execution_quantity",records[0].execution_quantity);
                                }
                                mdexecuted.setProperty("/saveBtn",true);
                                console.log(mdexecuted);
                                that.hideButtons(false, false, true, false);
      
                                console.log(records);
      
                                if (records.length > 0) {
                                    mdprogrammed.setProperty("/executionSaveBtn", true);
                                    console.log(records);
                                    let residue_programmed = res.residue,
                                        projected_quantity = mdprogrammed.getProperty("/selectedRecord/projected_quantity"),
                                        total = projected_quantity - residue_programmed;
      
                                    mdexecuted.setProperty("/programmed_residue", total);
                                    console.log(mdprogrammed);
                                } else {
                                    mdexecuted.setProperty("/programmed_residue", mdexecuted.getProperty("/selectedRecord/projected_quantity"));
                      
                                }
                                util.setProperty("/busy/", true);
                                resolve(res);
                            });
                        }
                    )
                    .catch(function(err) {
                        console.log("Fetch Error :-S", err);
                    });
            });
            
        },






        onSelectExecutedRecord: async function(oEvent) {

            console.log("pase al ejecutado");
            let that = this,
                util = this.getModel("util"),
                mdprogrammed = this.getView().getModel("mdprogrammed"),
                mdfarms = this.getView().getModel("mdfarms"),
                mdexecuted = this.getView().getModel("mdexecuted"),
                oView = this.getView(),
                scenario_id = this.getModel("mdscenario").getProperty("/scenario_id");
            console.log(mdprogrammed);
            //guarda la ruta del registro proyectado que fue seleccionado
            mdprogrammed.setProperty("/selectedRecordPath/", oEvent.getSource()["_aSelectedPaths"][0]);
            console.log(mdprogrammed.getProperty(mdprogrammed.getProperty("/selectedRecordPath/")));
            var selected = mdprogrammed.getProperty(mdprogrammed.getProperty("/selectedRecordPath/"));
            console.log(selected);
            mdexecuted.setProperty("/selectedRecord",mdprogrammed.getProperty(mdprogrammed.getProperty("/selectedRecordPath/")));
            console.log(mdexecuted);
            mdexecuted.setProperty("/rExecuted/enabledTab", true);

            oView.byId("tabBar").setSelectedKey("ktabExecuted");
            util.setProperty("/busy", false);
            let findExecuted = await this.findExecuted();
            console.log(this.getView().getModel("mdfarms"));
            console.log(this.getView().byId("selectFarm"));
            console.log(mdexecuted);
            mdprogrammed.setProperty("/programmedsaveBtn", false);
            let records = mdexecuted.getProperty("/records");
            if (records.length > 0) {
                console.log(">0");
                mdexecuted.setProperty("/executionSaveBtn", true);
            }
            if(records[0].executedfarm_id){
                console.log("executed farm id", records[0].executedfarm_id);
        
                mdfarms.setProperty("/selectedKey",records[0].executedfarm_id);
       
            }else{
                console.log("farm id", records[0].farm_id);
                mdfarms.setProperty("/selectedKey",records[0].farm_id);
            }
            // this.getView().byId("execution_quantityE").setValue('')
            // mdexecuted.setProperty("/execution_quantity",'');
            mdexecuted.setProperty("/name/state", "None");
            mdexecuted.setProperty("/name/stateText", "");
            mdexecuted.setProperty("/confirmBtn", false);
            mdexecuted.setProperty("/addBtn", false);
            console.log(mdfarms);
            console.log(records);
            this.onChangeFarmE();
            console.log("Cambio");

            //       mdprogrammed.setProperty("/selectedRecord/", mdprojected.getProperty(mdprogrammed.getProperty("/selectedRecordPath/")));
            //       console.log(mdprogrammed);
            //       let pDate = mdprogrammed.getProperty("/selectedRecord/projected_date"),
            //         aDate = pDate.split("/"),
            //         minDate = new Date(aDate[2], aDate[1] - 1, aDate[0]),
            //         date2 = new Date(aDate[2], aDate[1] - 1, aDate[0]),
            //         maxDate = this.addDays(date2, 7);

            //       mdprogrammed.setProperty("/selectedRecord/minDate/", minDate);
            //       mdprogrammed.setProperty("/selectedRecord/maxDate/", maxDate);

            //       //habilita el tab de la tabla de registros programado
            //       mdprogrammed.setProperty("/rProgrammed/enabledTab", true);

            //       oView.byId("tabBar").setSelectedKey("ktabProgrammed");


            //       console.log(mdprogrammed);
            //       //Buscar los registros de hausingway_detail
            //       const serverName = util.getProperty("/serviceUrl") + util.getProperty("/" + util.getProperty("/service") + "/findHousingWayDetByHw");
            // console.log(serverName)
      

        },




        onChangeCenterE: async function(){
            let mdcenter =  this.getView().getModel("mdcenter"),
                mdshed =  this.getView().getModel("mdshed"),
                mdexecuted = this.getModel("mdexecuted"),
                center_id = mdcenter.getProperty("/selectedKey"),
                executed_shed = mdexecuted.getProperty("/selectedRecord").executedshed_id;
            console.log(center_id);
            let findShed = await this.findShedByCenterForExecution(center_id);
            console.log(findShed);
            mdshed.setProperty("/records",findShed.data);
            // if(executed_shed!=null){
            //   mdshed.setProperty("/selectedKey",executed_shed)
            // }else{
            //   mdshed.setProperty("/selectedKey",mdexecuted.getProperty("/selectedRecord/shed_id"))
            // }
            console.log(mdshed);
            // this.getView().byId("execution_quantityE").setValue('')
            // mdexecuted.setProperty("/execution_quantity",'');
            mdexecuted.setProperty("/name/state", "None");
            mdexecuted.setProperty("/name/stateText", "");
            mdexecuted.setProperty("/confirmBtn", false);
            mdexecuted.setProperty("/addBtn", false);

            //Pendiente: Crear funcion para manejar el cambio del nucleo y de los galpones



            // console.log(sap.ui.getCore().byId("selectFarm"))
        },

        onValidExecutedQuantity: function(o)
        {
            let input= o.getSource();
            let length = 10;
            let value = input.getValue();
            console.log("valor: " + value);
            let regex = new RegExp(`/^[0-9]{1,${length}}$/`);

            if (regex.test(value)) 
            {
                console.log("entro if");
                return true;
            }
            else 
            {
                console.log("entro else");
                let aux = value
                    .split("")
                    .filter(char => {
                        if (/^[0-9]$/.test(char)) 
                        {
                            if (char !== ".") {
                                return true;
                            }
                        }
                    })
                    .join("");
                value = aux.substring(0, length);
                input.setValue(value);

                console.log("el valor es: " + value);
          

                let mdexecuted = this.getView().getModel("mdexecuted"),
                    programmedquantity = mdexecuted.getProperty("/selectedRecord").scheduled_quantity;

                console.log("el modelo shed en validacion");
                console.log(mdexecuted, programmedquantity);

                let mdshed= this.getView().getModel("mdshed");
                let records= mdshed.getProperty("/records"),
                    myshed= records.filter(function(item){
                        return mdshed.getProperty("/selectedKey")==item.shed_id;
                    });
                console.log(myshed);
                console.log(value,programmedquantity,value,myshed[0].capmax);

                if(parseInt(value) <= parseInt(myshed[0].capmax) ){
                    mdexecuted.setProperty("/name/state", "None");
                    console.log("entro");
                    mdexecuted.setProperty("/name/stateText", "");
                    mdexecuted.setProperty("/saveBtn", true);
                    console.log( mdexecuted.getProperty("/saveBtn"));

                }else{
                    // if (parseInt(value) > programmedquantity) {
                    //   mdexecuted.setProperty("/name/state", "Error");
                    //   mdexecuted.setProperty("/name/stateText", "La cantidad ejecutada supera la programada");
                    //   mdexecuted.setProperty("/saveBtn", false);
                    // }
                    console.log(value,myshed[0].capmax);
                    if (parseInt(value) > parseInt(myshed[0].capmax)) {
                        mdexecuted.setProperty("/name/state", "Warning");
                        mdexecuted.setProperty("/name/stateText", "La cantidad ejecutada supera la capacidad del galpon");
                        // mdexecuted.setProperty("/saveBtn", false);
                    }
                    if (value == "") {
                        mdexecuted.setProperty("/name/state", "Error");
                        mdexecuted.setProperty("/name/stateText", "La cantidad ejecutada no debe estar vacia");
                        mdexecuted.setProperty("/saveBtn", false);
                    }
                }
                return false;
            }
        },


        onChangeFarmE: async function(){
            let mdfarms = this.getView().getModel("mdfarms"),
                mdcenter =  this.getView().getModel("mdcenter"),
                mdexecuted =  this.getView().getModel("mdexecuted"),
                farm_id = mdfarms.getProperty("/selectedKey");
            console.log(mdfarms);
            let findCenter = await this.findCenterByFarm(farm_id);
            console.log(mdcenter);
            mdcenter.setProperty("/records",findCenter.data);
            // mdcenter.setProperty("/selectedKey",findCenter.data[0].center_id)
      
            console.log(mdcenter);
            this.onChangeCenterE();
            if(mdexecuted.getProperty("/records")[0].available){
                // console.log(this.getView().byId("execution_quantityE"))
                // this.getView().byId("execution_quantityE").setValue('')
                // mdexecuted.setProperty("/execution_quantity",'');
                mdexecuted.setProperty("/name/state", "None");
                mdexecuted.setProperty("/name/stateText", "");
                mdexecuted.setProperty("/confirmBtn", false);
                mdexecuted.setProperty("/addBtn", false);
            }
            //Pendiente: Crear funcion para manejar el cambio del nucleo y de los galpones



            // console.log(sap.ui.getCore().byId("selectFarm"))
        },

        getDia: function(dia){
            let mdprogrammed = this.getView().getModel("mdprogrammed"),
                aDate = dia.split("/"),
                fecha = new Date(aDate[2], aDate[1] - 1, aDate[0]),
                dias= ["Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado"], 
                fullDate = dias[fecha.getUTCDay()] + " " +dia ; 
            console.log(fullDate);
            return fullDate;
            //mdprogrammed.setProperty("/selectedRecord/projected_date",fullDate); 

        },





        hideButtons: function(projected, programmed, execution, reports) {
            let mdprojected = this.getModel("mdprojected");
            let mdprogrammed = this.getModel("mdprogrammed");
            let mdexecuted = this.getModel("mdexecuted");
            let mdreports = this.getModel("mdreports");
            mdprojected.setProperty("/projectedSaveBtn", projected);
            mdprogrammed.setProperty("/programmedsaveBtn", programmed);
            mdexecuted.setProperty("/executionSaveBtn", execution);
            mdreports.setProperty("/reportsBtn", reports);

        },
        _handleValueHelpSearch: function(evt) {
            var sValue = evt.getParameter("value");
            var oFilter = new Filter(
                "Name",
                sap.ui.model.FilterOperator.Contains, sValue
            );
            evt.getSource().getBinding("items").filter([oFilter]);
        },

        _handleValueHelpClose: function(evt) {
            var oSelectedItem = evt.getParameter("selectedItem");
            if (oSelectedItem) {
                var productInput = this.getView().byId(this.inputId);
                productInput.setValue(oSelectedItem.getTitle());
            }
            evt.getSource().getBinding("items").filter([]);
        },



















        onProjectedNext: function(oEvent) {
            const mdprogrammed = this.getView().getModel("mdprogrammed");
            const util = this.getModel("util");
            console.log(mdprogrammed.getProperty("/selectedRecords"));

            mdprogrammed.setProperty("/rProgrammed/enabledTab", true);

            this.getView().byId("tabBar").setSelectedKey("ktabProgrammed");
            console.log("entre0");
            const serverName = util.getProperty("/serviceUrl") + util.getProperty("/" + util.getProperty("/service") + "/findbroilerdetail");
        console.log("La consulta a los programados!!!",  mdprogrammed.getProperty("/selectedRecords").map(record => record.broiler_id))
            fetch(serverName, {
                method: "POST",
                headers: {
                    "Content-type": "application/json"
                },
                body: JSON.stringify({
                    records: mdprogrammed.getProperty("/selectedRecords").map(record => record.broiler_id)
                })
            })
                .then(response => {
                    if (response.status !== 200) {
                        console.log("Looks like there was a problem. Status Code: " +
            response.status);
                        return;
                    }
                    console.log("entre1");
                    response.json().then((res) => {
                        console.log(res);
                        let records = res.data;
                        records.forEach(element => {
                            if(element.executedcenter_id && element.executedfarm_id && element.executedshed_id && element.execution_quantity && element.execution_date){
                                element.isexecuted = true;
                            }else{
                                element.isexecuted = false;
                            }
            
                        });
                        console.log("Entreee2");
                        console.log(records);
                        mdprogrammed.setProperty("/records", records);
                        this.hideButtons(false, true, false, false);

                        console.log(records);

                        if (records.length > 0) {
                            mdprogrammed.setProperty("/executionSaveBtn", true);
                            console.log(records);
                            let residue_programmed = res.residue,
                                projected_quantity = mdprogrammed.getProperty("/selectedRecord/projected_quantity"),
                                total = projected_quantity - residue_programmed;

                            mdprogrammed.setProperty("/programmed_residue", total);
                            console.log(mdprogrammed);
                        } else {
                            mdprogrammed.setProperty("/programmed_residue", mdprogrammed.getProperty("/selectedRecord/projected_quantity"));
                            mdprogrammed.setProperty("/executionSaveBtn", false);
                        }
                        util.setProperty("/busy/", false);
                    });


                })
                .catch(err => console.log);
        },

        onSelectProgrammedRecords: function(oEvent) {
            const mdprogrammed = this.getView().getModel("mdprogrammed");
            const util = this.getView().getModel("util");
            const mdprojected = this.getView().getModel("mdprojected");

            const projectedTable = this.getView().byId("projectedTable");
            const projections = projectedTable.getSelectedItems().map(item => mdprojected.getProperty(item.getBindingContext("mdprojected").getPath()));
            const actualRecords = mdprogrammed.getProperty("/selectedRecords");
            // if (actualRecords.length > 0 && actualRecords[0].breed_name)
            mdprogrammed.setProperty("/selectedRecords", projections);
            console.log(projections);
            util.setProperty("/busy", false);
        },

        onSelectProgrammedRecord: async function(oEvent) {
            var tabla = this.getView().byId("idtable2");
            var itemsrows = tabla.mAggregations.items.length;

            let that = this,
                util = this.getModel("util"),
                mdprogrammed = this.getView().getModel("mdprogrammed"),
                mdprojected = this.getView().getModel("mdprojected"),
                oView = this.getView(),
                scenario_id = this.getModel("mdscenario").getProperty("/scenario_id"),
                partnership_id = this.getView().getModel("ospartnership").getProperty("/records/" + this.index + "/partnership_id"),
                mdrecords= mdprogrammed.getProperty("/records"),
                ospartnership = this.getView().getModel("ospartnership");

            util.setProperty("/busy", false);
            console.log("IDDDDDDDDDDDDDDD");
            console.log(partnership_id);
            console.log("record");
            console.log(mdrecords);
            ospartnership.setProperty("/selectedPartnership/partnership_index", this.index);
            console.log(itemsrows);

            this.hideButtons(false, true, false, false);


            console.log("avaliable, mdprogrammedmdprogrammedmdprogrammed aiuraaaa::::");
            console.log(mdprogrammed);

            //guarda la ruta del registro proyectado que fue seleccionado
            mdprogrammed.setProperty("/selectedRecordPath/", oEvent.getSource()["_aSelectedPaths"][0]);
            mdprogrammed.setProperty("/selectedRecord/", mdprojected.getProperty(mdprogrammed.getProperty("/selectedRecordPath/")));
            console.log("El proyectado que selecciono: ",mdprojected.getProperty(mdprogrammed.getProperty("/selectedRecordPath/")));
            console.log(mdprojected);
            console.log(mdprogrammed);
      
            let pDate = mdprogrammed.getProperty("/selectedRecord/projected_date"),
                aDate = pDate.split("/"),
                minDate = new Date(aDate[2], aDate[1] - 1, aDate[0]),
                date2 = new Date(aDate[2], aDate[1] - 1, aDate[0]),
                // dias= ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'], //Para lo del día
                maxDate = this.addDays(date2, 7);
            // console.log("Dia de la semana",dias[minDate.getUTCDay()])
            mdprogrammed.setProperty("/selectedRecord/minDate/", minDate);
            mdprogrammed.setProperty("/selectedRecord/maxDate/", maxDate);

            // mdprogrammed.setProperty("/SelectedRecord/fProjected_date", this.getDia(pDate));
            // console.log(mdprogrammed)
            // var fullDate = dias[minDate.getUTCDay()] + " " + mdprogrammed.getProperty("/selectedRecord/projected_date") ; //Para lo del día
            // console.log(fullDate)
            // mdprogrammed.setProperty("/selectedRecord/projected_date",fullDate); //Para lo del día
            //guarda la ruta del registro proyectado que fue seleccionado
            mdprogrammed.setProperty("/selectedRecordPath/", oEvent.getSource()["_aSelectedPaths"][0]);
            mdprogrammed.setProperty("/selectedRecord/", mdprojected.getProperty(mdprogrammed.getProperty("/selectedRecordPath/")));


            //habilita el tab de la tabla de registros programado
            mdprogrammed.setProperty("/rProgrammed/enabledTab", true);

            oView.byId("tabBar").setSelectedKey("ktabProgrammed");

            //  this.hideButtons(false, true, true);
            mdprojected.setProperty("/projectedSaveBtn", false);
            mdprogrammed.setProperty("/programmedsaveBtn", true);

            console.log("qloqqqqqqqqqqqqq");
            console.log(mdprogrammed);
            //Buscar los lotes que se pueden asiganr
            let lots = await this.loadLot(pDate),
                breed_id = this.getModel("mdprogrammed").getProperty("/selectedRecord/breed_id"),
                broiler_id = this.getModel("mdprogrammed").getProperty("/selectedRecord/broiler_id");


            //Buscar los registros de broiler_detail
            console.log(scenario_id, aDate[2]+"-"+aDate[1]+"-"+aDate[0], partnership_id, breed_id);
            const serverName = util.getProperty("/serviceUrl") + util.getProperty("/" + util.getProperty("/service") + "/findbroilerdetail");
            console.log(serverName);
            fetch(serverName, {
                method: "POST",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify({
                    scenario_id: scenario_id,
                    _date: aDate[2]+"-"+aDate[1]+"-"+aDate[0],
                    partnership_id: partnership_id,
                    breed_id: breed_id,
                    broiler_id:broiler_id
                })
            })
                .then(
                    function(response) {
                        if (response.status !== 200) {
                            console.log("Looks like there was a problem. Status Code: " +
                response.status);
                            return;
                        }

                        response.json().then(function(res) {
                            let records = res.data;
                            records.forEach(element => {
                                if(element.executedcenter_id && element.executedfarm_id && element.executedshed_id && element.execution_quantity && element.execution_date){
                                    element.isexecuted = true;
                                }else{
                                    element.isexecuted = false;
                                }
                
                            });
                            console.log("AQUIIII: ", records);
                            mdprogrammed.setProperty("/records", records);
                            console.log(mdprogrammed);
                            that.hideButtons(false, true, false, false);

                            if (records.length > 0) {
                                mdprogrammed.setProperty("/executionSaveBtn", true);
                                let residue_programmed = res.residue,
                                    projected_quantity = mdprogrammed.getProperty("/selectedRecord/projected_quantity"),
                                    total = projected_quantity - residue_programmed;
                                mdprogrammed.setProperty("/programmed_residue", total);
                                console.log(total);
                            } else {
                                mdprogrammed.setProperty("/programmed_residue", mdprogrammed.getProperty("/selectedRecord/projected_quantity"));
                                mdprogrammed.setProperty("/executionSaveBtn", false);
                            }
                            util.setProperty("/busy/", true);
                        });
                    }
                )
                .catch(function(err) {
                    console.log("Fetch Error :-S", err);
                });

        },
        loadLot: function(pdate){
            let util = this.getModel("util"),
                mdprogrammed = this.getModel("mdprogrammed"),
                scenario_id = this.getModel("mdscenario").getProperty("/scenario_id"),
                partnership_id = this.getView().getModel("ospartnership").getProperty("/records/" + this.index + "/partnership_id"),
                breed_id = this.getModel("mdprogrammed").getProperty("/selectedRecord/breed_id") ;

            mdprogrammed.setProperty("/selectedRecord/projected_quantity", parseInt(mdprogrammed.getProperty("/selectedRecord/projected_quantity")) );
            const serverName = util.getProperty("/serviceUrl") + util.getProperty("/" + util.getProperty("/service") + "/findBroilerLot");

            fetch(serverName, {
                method: "POST",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify({
                    scenario_id: scenario_id,
                    _date: pdate,
                    partnership_id: partnership_id,
                    breed_id: breed_id
                })
            })
                .then(
                    function(response) {
                        if (response.status !== 200) {
                            console.log("Looks like there was a problem. Status Code: " +
                response.status);
                            return;
                        }

                        response.json().then(function(res) {
                            let records = res.data;
                            mdprogrammed.setProperty("/selectedRecord/lot", records);
                            mdprogrammed.setProperty("/selectedRecord/lot_assigned", []);
                            mdprogrammed.setProperty("/selectedRecord/residue" , mdprogrammed.getProperty("/selectedRecord/projected_quantity" ) );
                            util.setProperty("/busy/", true);
                        });
                    }
                )
                .catch(function(err) {
                    console.log("Fetch Error :-S", err);
                });
        },

        onAddBroiler: function(){
            let selected_incubator = sap.ui.getCore().byId("selectLot").getSelectedKey(),
                quantity_chicken = parseInt(sap.ui.getCore().byId("assigned_quantity").mProperties.value, 10),
                mdprogrammed = this.getView().getModel("mdprogrammed"),
                mdshed = this.getView().getModel("mdshed"),
                records = mdprogrammed.getProperty("/selectedRecords");

            let lot = mdprogrammed.getProperty("/assigned"),
                iName = sap.ui.getCore().byId("selectLot").getSelectedItem(),
                name = iName.mProperties.text,
                selected_product_name = sap.ui.getCore().byId("selecProduct").getSelectedItem().mProperties.text,
                selected_product_id = sap.ui.getCore().byId("selecProduct").getSelectedKey();

            let flag= true;

            let dateInput = sap.ui.getCore().byId("programmed_date"),
                date = dateInput.getValue();

            if (date === undefined || date === "") {
                dateInput.setValueState("Error");
                return;
            }
            else {
                dateInput.setValueState("None");
            }

            const shedSelect = sap.ui.getCore().byId("selectShed");
            const shedKey = shedSelect.getSelectedKey();
            console.log("shedKey", shedKey);
            if (shedKey === undefined || shedKey === "") {
                shedSelect.setValueState("Error");
                return;
            }
            else {
                shedSelect.setValueState("None");
            }

            console.log("lote incubador: " + selected_incubator);
            console.log("Lot: ", lot, "name: ", name);

            if(lot === undefined){
                lot = [];
            }
            let list_name = name.split("-");

            let sum_chicken = parseInt(quantity_chicken, 10);
            console.log(lot);
            lot.forEach(item => {
            // sum_chicken += parseInt(item.quantity_chicken);
            //verifica que no este repetido el lote
                console.log("item", item);
                console.log("producto", selected_product_id);
                console.log("broiler id", selected_incubator);
                if (flag && selected_product_id== item.selected_product_id && selected_incubator == item.selected_lot){
                    flag= false;
                    item.quantity_chicken += parseInt(quantity_chicken);
                    const broiler_lot = records.find(record => record.broiler_id == selected_incubator);
                    broiler_lot.partial_residue += quantity_chicken;
                }
            
            });

            if(flag){
                let lotItem= {
                    selected_lot: parseInt(selected_incubator),
                    quantity_chicken: parseInt(quantity_chicken),
                    name: list_name[0].trim(),
                    description: selected_product_name,
                    selected_product_id: parseInt(selected_product_id)
                };
                lot.push(lotItem);
            
                const broiler_lot = records.find(record => record.broiler_id == selected_incubator);
                broiler_lot.partial_residue = quantity_chicken;

                sap.ui.getCore().byId("assigned_quantity").setValue("");
            }




            console.log("lot: ", lot);
            //console.log("listID: ", mdincubator.getProperty("/listID"));
            //mdprogrammed.setProperty("/lot_records/"+ mdincubator.getProperty("/listID") +"/assigned", lot );
            mdprogrammed.setProperty("/assigned", lot );
            if(lot.length>0){
                mdprogrammed.setProperty("/confirmBtn", true);
            }
            else{
                mdprogrammed.setProperty("/confirmBtn", false);
            }
            let residue = parseInt(mdprogrammed.getProperty("/selectedRecord/residue")) - sum_chicken;
            console.log("RESIDUOS::");
            console.log(parseInt(mdprogrammed.getProperty("/selectedRecord/residue")), sum_chicken, residue );
            //console.log("Brayan: ", mdprogrammed.getProperty("/lot_records/"+ mdincubator.getProperty("/listID") +"/eggs"), " ", sum_eggs);
            mdprogrammed.setProperty("/selectedRecord/residue" , residue );
            sap.ui.getCore().byId("assigned_quantity").setValue("");
            mdprogrammed.setProperty("/name/state", "None");
            mdprogrammed.setProperty("/name/stateText", "");
            mdprogrammed.setProperty("/addBtn", false);
            console.log("mdprogrammed luego del lote: ", mdprogrammed);

        },
        handleTitleSelectorPress: function(oEvent) {
            var _oPopover = this._getResponsivePopover();
            _oPopover.setModel(oEvent.getSource().getModel());
            _oPopover.openBy(oEvent.getParameter("domRef"));
            console.log("prueba");
        },
        _getResponsivePopover: function() {
            if (!this._oPopover) {

                this._oPopover = sap.ui.xmlfragment("broilersPlanningM.view.Popover", this);
                this.getView().addDependent(this._oPopover);
            }
            return this._oPopover;
        },
        addDays: function(ndate, ndays) {
            ndate.setDate(ndate.getDate() + ndays);
            return ndate;
        },

        onDialogPressPj: function(oEvent) {
            this.formProjected = sap.ui.xmlfragment(
                "broilersPlanningM.view.DialogProject", this);
            this.getView().addDependent(this.pressDialog);
            this.formProjected.open();
        },

        onProyectedCloseDialog: function(oEvent) {
            console.log("Entro");
            this.formProjected.close();
            this.formProjected.destroy();
        },

        onProjectedSaveDialog: function(oEvent) {

            let that = this,
                util = this.getModel("util"),
                mdprojected = this.getModel("mdprojected"),
                partnership_id = this.getView().getModel("ospartnership").getProperty("/records/" + this.index + "/partnership_id"),
                scenario_id = this.getModel("mdscenario").getProperty("/scenario_id"),
                breed_id = sap.ui.getCore().byId("breedSelect").getSelectedKey(),
                pDate = sap.ui.getCore().byId("projected_date").mProperties.dateValue,
                projected_quantity = sap.ui.getCore().byId("projected_quantity").mProperties.value,
                projected_date = `${pDate.getFullYear()}-${pDate.getMonth()+1}-${pDate.getDate()}`;
            console.log(projected_date);


            var dates = [];
            //this.byId("list").setBusy(true);
            const serverName = util.getProperty("/serviceUrl") + util.getProperty("/" + util.getProperty("/service") + "/housingway");

            fetch(serverName, {
                method: "POST",
                headers: {
                    "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
                },
                body: "stage_id=" + breedingStage + "&partnership_id=" + partnership_id + "&scenario_id=" + scenario_id + "&projected_quantity=" + projected_quantity +
            "&projected_date=" + projected_date + "&breed_id=" + breed_id + "&predecessor_id=0"
            })
                .then(
                    function(response) {
                        if (response.status !== 200) {
                            console.log("Looks like there was a problem. Status Code: " +
                response.status);
                            return;
                        }

                        response.json().then(function(res) {

                            that.formProjected.close();
                            that.formProjected.destroy();
                            var dialog = new Dialog({
                                title: "Información",
                                type: "Message",
                                state: "Success",
                                content: new Text({
                                    text: "Semana guardada con éxito."
                                }),
                                beginButton: new Button({
                                    text: "OK",
                                    press: function() {
                                        dialog.close();
                                        that.onProjectedSave();
                                    }
                                }),
                                afterClose: function() {
                                    dialog.destroy();
                                }
                            });

                            dialog.open();

                        });
                    }
                )
                .catch(function(err) {
                    console.log("Fetch Error :-S", err);
                });

        },
        onProjectedSave: async function() {

            let mdprojected = this.getModel("mdprojected"),
                mdprogrammed = this.getModel("mdprogrammed"),
                findScenario = await this.findProjected();

            mdprogrammed.setProperty("/rProgrammed/enabledTab", false);
            mdprogrammed.setProperty("/records", []);



            mdprojected.setProperty("/records", findScenario.data);
            mdprojected.attachRequestCompleted(function() {
                sap.ui.getCore().byId("projectedTable").removeSelections();
            });
            this.hideButtons(true, false, false, false);
        },
        onBreedLoad: function() {
            const util = this.getModel("util"),
                serverName = util.getProperty("/serviceUrl") + util.getProperty("/" + util.getProperty("/service") + "/findBreed");

            let mdbreed = this.getModel("mdbreed"),
                that = this;
            mdbreed.setProperty("/records", []);

            let isRecords = new Promise((resolve, reject) => {
                fetch(serverName)
                    .then(
                        function(response) {
                            if (response.status !== 200) {

                                console.log("Looks like there was a problem. Status Code: " +
                  response.status);
                                return;
                            }
                            // Examine the text in the response
                            response.json().then(function(data) {
                                //console.log(data);
                                resolve(data);
                            });
                        }
                    )
                    .catch(function(err) {
                        console.log("Fetch Error :-S", err);
                    });
            });


            isRecords.then((res) => {
                if (res.data.length > 0) {
                    mdbreed.setProperty("/records", res.data);
                    mdbreed.setProperty("/value", mdbreed.getProperty("/records/0/breed_id"));
                    console.log(mdbreed);
                }
            });
        },
        onProyectedSave: async function() {

            let mdprojected = this.getModel("mdprojected"),
                mdprogrammed = this.getModel("mdprogrammed"),
                findScenario = await this.findProjected();

            mdprogrammed.setProperty("/rProgrammed/enabledTab", false);
            mdprogrammed.setProperty("/records", []);
            this.getView().byId("projectedTable").removeSelections();
            mdprojected.setProperty("/records", findScenario.data);
            this.hideButtons(true, false, false, false);
        },
        onDialogPressPg: function(oEvent) {
            let mdprogrammed = this.getModel("mdprogrammed"),
                sRecord= mdprogrammed.getProperty("/selectedRecords")[0],
                lot = mdprogrammed.getProperty("/records");

            mdprogrammed.setProperty("/selectedRecord/lot_assigned", []);

            if(lot === undefined){
                lot = [];
            }
            let sum_chicken=0;
            lot.forEach(item=>{
                if (item.scheduled_quantity !== undefined && item.scheduled_quantity !== null)
                    sum_chicken += parseInt(item.scheduled_quantity);

            });

            // sRecord.residue= parseInt(sRecord.projected_quantity) -sum_chicken;

            this.formProgrammed = sap.ui.xmlfragment(
                "broilersPlanningM.view.DialogProgrammer", this);
            var that = this;
            var dlg = sap.ui.getCore().byId("dialogprogrammed");
            dlg.attachAfterClose(function () {
                that.formProgrammed.destroy();
            });
            this.getView().addDependent(this.formProgrammed);
            this.formProgrammed.open();
            this.onChangeFarm();
        },
        onChangeShed: async function() {
            let mdshed = this.getModel("mdshed"),
                mdprogrammed = this.getModel("mdprogrammed"),
                selectedShed = sap.ui.getCore().byId("selectShed").getSelectedKey();
            mdshed.setProperty("/selectedKey", selectedShed);
            console.log(selectedShed);
            sap.ui.getCore().byId("assigned_quantity").setValue();
            mdprogrammed.setProperty("/name/state", "None");
            mdprogrammed.setProperty("/name/stateText", "");
            mdprogrammed.setProperty("/confirmBtn", false);
            let array1 = mdshed.getProperty("/records");

            var found = array1.find(function(element) {
                return element.shed_id == selectedShed;
            });
            console.log("found",found);
            mdprogrammed.setProperty("/capmin2", parseInt(found.capmin));
            mdprogrammed.setProperty("/capmax2", parseInt(found.capmax));
            mdprogrammed.setProperty("/addBtn", false);

        },
        onChangeShedE: async function() {
            let mdshed = this.getModel("mdshed"),
                mdexecuted = this.getView().getModel("mdexecuted"),
                selectedShed = mdshed.getProperty("/selectedKey");
            console.log(selectedShed);
            // console.log(this.getView().byId("execution_quantityE"))
            // this.getView().byId("execution_quantityE").setValue('')
            // mdexecuted.setProperty("/execution_quantity",'');
            mdexecuted.setProperty("/name/state", "None");
            mdexecuted.setProperty("/name/stateText", "");
            mdexecuted.setProperty("/confirmBtn", false);
            mdexecuted.setProperty("/addBtn", false);
            console.log(mdexecuted);
            console.log(mdshed);
            mdexecuted.refresh();

        },
        onChangeCore: async function(){
            console.log(this.getView().getModel("mdprogrammed"));
            console.log(this.getView().getModel("mdprojected"));






            let mdcores = this.getModel("mdcores"),
                mdprogrammed= this.getModel("mdprogrammed"),
                selectedFarm = mdprogrammed.getProperty("/selectedFarm"),
                mdfarm = this.getModel("mdfarms"),
                selectedCore = sap.ui.getCore().byId("selectCore").getSelectedKey();
            console.log(selectedCore);
            mdcores.setProperty("/selectedKey", selectedCore);
            console.log( mdcores.getProperty("/selectedKey"));
            console.log(mdfarm.getProperty("/selectedKey"));
            /*Llamar a la funcion del back que se va a traer los galpones que pertenecen al nucleo seleccionado*/
            console.log("llego");
            let findShed = await this.findShedByFarm(selectedCore),
                mdshed = this.getModel("mdshed");

            mdshed.setProperty("/records", findShed.data);
            // sap.ui.getCore().byId("selectShed").setSelectedKey(findShed.data[0].shed_id)
            this.onChangeShed();
            sap.ui.getCore().byId("programmed_quantity").setValue();
            mdprogrammed.setProperty("/name/state", "None");
            mdprogrammed.setProperty("/name/stateText", "");
            mdprogrammed.setProperty("/confirmBtn", false);
            // sap.ui.getCore().byId("selectShed").setSelectedItem(new item ());

        },

        onChangeFarm: async function() {
            let mdfarm = this.getModel("mdfarms"),
                mdprogrammed = this.getModel("mdprogrammed"),
                selectedFarm = sap.ui.getCore().byId("selectFarm").getSelectedKey();
            mdfarm.setProperty("/selectedKey", selectedFarm);
            console.log(mdfarm);
            let findShed = await this.findCenterByFarm(selectedFarm),
                mdcores = this.getModel("mdcores");
            mdcores.setProperty("/records", findShed.data);
            var tmp = mdcores.getProperty("/records")[0].center_id;
            console.log(tmp);
            mdcores.setProperty("/selectedKey", tmp);
            sap.ui.getCore().byId("assigned_quantity").setValue();
            mdprogrammed.setProperty("/name/state", "None");
            mdprogrammed.setProperty("/name/stateText", "");
            mdprogrammed.setProperty("/confirmBtn", false);
            mdprogrammed.setProperty("/addBtn", false);
            console.log(mdprogrammed);
            this.onChangeCore();
        },
        findCenterByFarm: function(selectedFarm) {
            let util = this.getModel("util"),
                mdshed = this.getModel("mdshed"),
                partnership_id = this.getView().getModel("ospartnership").getProperty("/records/" + this.index + "/partnership_id");
            console.log(selectedFarm);
            return new Promise((resolve, reject) => {
                fetch("/center/findCenterByFarm2", {
                    method: "POST",
                    headers: {
                        "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
                    },

                    body: "farm_id=" + selectedFarm
                })
                    .then(
                        function(response) {
                            if (response.status !== 200) {
                                console.log("Looks like there was a problem. Status Code: " +
                  response.status);
                                return;
                            }

                            response.json().then(function(res) {
                                console.log("respuesta recibida en la busqueda de galpones");
                                console.log(res);
                              
                                console.log("sheds:: ", res.data);
                                resolve(res);
                            });
                        }
                    )
                    .catch(function(err) {
                        console.log("Fetch Error :-S", err);
                    });
            });
        },

        findShedByFarm: function(selectedFarm) {
            let util = this.getModel("util"),
                mdshed = this.getModel("mdshed"),
                partnership_id = this.getView().getModel("ospartnership").getProperty("/records/" + this.index + "/partnership_id");
            console.log(selectedFarm);
            const serverName = util.getProperty("/serviceUrl") + util.getProperty("/" + util.getProperty("/service") + "/findShedsByFarm");
            console.log(serverName);
            return new Promise((resolve, reject) => {
                fetch("/shed/findShedByCenter2", {
                    method: "POST",
                    headers: {
                        "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
                    },

                    body: "center_id=" + selectedFarm
                })
                    .then(
                        function(response) {
                            if (response.status !== 200) {
                                console.log("Looks like there was a problem. Status Code: " +
                  response.status);
                                return;
                            }

                            response.json().then(function(res) {
                                console.log(res);
                                res.data= res.data.filter(function(item){
                                    return item.statusshed_id==1;
                                });
                                resolve(res);
                            });
                        }
                    )
                    .catch(function(err) {
                        console.log("Fetch Error :-S", err);
                    });
            });
        },


        findShedByCenterForExecution: function(selectedFarm) { /* En caso que se pida mostrar todos los galpones en la pantalla de ejecucion */
            let util = this.getModel("util"),
                mdexecuted = this.getModel("mdexecuted"),
                partnership_id = this.getView().getModel("ospartnership").getProperty("/records/" + this.index + "/partnership_id");
            console.log(selectedFarm);
            const serverName = util.getProperty("/serviceUrl") + util.getProperty("/" + util.getProperty("/service") + "/findShedsByFarm");
            console.log(serverName);
            return new Promise((resolve, reject) => {
                fetch("/shed/findShedByCenter2", {
                    method: "POST",
                    headers: {
                        "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
                    },

                    body: "center_id=" + selectedFarm
                })
                    .then(
                        function(response) {
                            if (response.status !== 200) {
                                console.log("Looks like there was a problem. Status Code: " +
                  response.status);
                                return;
                            }

                            response.json().then(function(res) {
                                console.log(res);
                                res.data= res.data.filter(function(item){
                                    return item.statusshed_id===1||mdexecuted.getProperty("/selectedRecord").shed_id===item.shed_id||mdexecuted.getProperty("/selectedRecord").executedshed_id===item.shed_id;
                                });
                                resolve(res);
                            });
                        }
                    )
                    .catch(function(err) {
                        console.log("Fetch Error :-S", err);
                    });
            });
        },
        onProgrammedCloseDialog: function() {
            this.closeProgrammedDialog();
            this.formProgrammed.close();
            this.formProgrammed.destroy();
        },

        onProgrammedSaveDialog: function() {
            console.log("boton guardar");

            let that = this,
                util = this.getModel("util"),
                scenario_id = this.getModel("mdscenario").getProperty("/scenario_id"),
                mdprogrammed = this.getModel("mdprogrammed"),
                mdprocess = this.getModel("mdprocess"),
                pDate = sap.ui.getCore().byId("programmed_date").getValue(),
                partnership_id = this.getView().getModel("ospartnership").getProperty("/selectedRecord/").partnership_id,
                farm_id = sap.ui.getCore().byId("selectFarm").getSelectedKey(),
                shed_id = sap.ui.getCore().byId("selectShed").getSelectedKey(),

                center_id = sap.ui.getCore().byId("selectCore").getSelectedKey(),

                breed_id = mdprogrammed.getProperty("/selectedRecords/0/breed_id"),
                broiler_id = mdprogrammed.getProperty("/assigned/0/selected_lot");

            var dates = [];
            console.log("pDate: ", pDate);
            const serverName = util.getProperty("/serviceUrl") + util.getProperty("/" + util.getProperty("/service") + "/addbroilerdetail");
            console.log(mdprogrammed.getProperty("/selectedRecord/lot_assigned"));
            console.log(mdprogrammed.getProperty("/selectedRecord/lot_assigned"));
            mdprogrammed.setProperty("/confirmBtn",false);
      
            console.log(serverName);
            fetch(serverName, {
                method: "POST",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify({
                    scenario_id: scenario_id,
                    _date: pDate,
                    farm_id: farm_id,
                    shed_id: shed_id,
                    center_id: center_id,
                    partnership_id: partnership_id,
                    breed_id: breed_id,
                    broiler_id : broiler_id,
                    records: mdprogrammed.getProperty("/assigned"),
                    selected_product_id: mdprogrammed.getProperty("/assigned/0/selected_product_id")
                })
            })
                .then(
                    function(response) {
                        if (response.status !== 200) {
                            console.log(response);
                            console.log("Looks like there was a problem. Status Code: " +
                response.status);
                            return;
                        }

                        response.json().then(function(res) {
                            console.log("respuesta guardar");
                            that.formProgrammed.close();
                            that.formProgrammed.destroy();
                            var dialog = new Dialog({
                                title: "Información",
                                type: "Message",
                                state: "Success",
                                content: new Text({
                                    text: "Información guardada con éxito."
                                }),
                                beginButton: new Button({
                                    text: "OK",
                                    press: function() {
                                        dialog.close();
                                        console.log("LA DATA SETEADA");
                                        console.log(res.data);
                                        let record = res.data;
                                        record.forEach(element => {
                                            if(element.executedcenter_id && element.executedfarm_id && element.executedshed_id && element.execution_quantity && element.execution_date){
                                                element.isexecuted = true;
                                            }else{
                                                element.isexecuted = false;
                                            }
                          
                                        });
                                        mdprogrammed.setProperty("/records", record);
    
                                        console.log("modelo ok");
                                        console.log(mdprogrammed);
                                        //formateo los lotes mostrados luego de guardar
                                        mdprogrammed.setProperty("/selectedRecord/lot_assigned", []);
                                        mdprogrammed.setProperty("/assigned", []);
                                        that.hideButtons(false, true, false, false);
                                    }
                                }),
                                afterClose: function() {
                                    dialog.destroy();
                                }
                            });

                            dialog.open();

                        });
                    }
                )
                .catch(function(err) {
                    console.log("Fetch Error :-S", err);
                });
        },

        handleTitleSelectorPress: function(oEvent) {
            var _oPopover = this._getResponsivePopover();
            _oPopover.setModel(oEvent.getSource().getModel());
            _oPopover.openBy(oEvent.getParameter("domRef"));
            console.log("prueba");
        },
        _getResponsivePopover: function() {
            if (!this._oPopover) {

                this._oPopover = sap.ui.xmlfragment("broilersPlanningM.view.Popover", this);
                this.getView().addDependent(this._oPopover);
            }
            return this._oPopover;
        },
        onFarmLoad: function() {

            const util = this.getModel("util"),
                serverName = util.getProperty("/serviceUrl") + util.getProperty("/" + util.getProperty("/service") + "/findFarmByPartAndStatus"),
                partnership_id = this.getView().getModel("ospartnership").getProperty("/selectedRecord/").partnership_id;

            let osfarm = this.getModel("mdfarms"),
                that = this;

            osfarm.setProperty("/records", []);
            console.log(osfarm);
            console.log(serverName);
            let isRecords = new Promise((resolve, reject) => {
                fetch("/farm/findFarmByPartAndStatus2/", {
                    method: "POST",
                    headers: {
                        "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
                    },
                    body: "partnership_id=" + partnership_id + "&status_id=2"
                })
                    .then(
                        function(response) {
                            if (response.status !== 200) {

                                console.log("Looks like there was a problem. Status Code: " +
                  response.status);
                                return;
                            }
                            // Examine the text in the response
                            response.json().then(function(data) {
                                console.log(data);
                                resolve(data);
                            });
                        }
                    )
                    .catch(function(err) {
                        console.log("Fetch Error :-S", err);
                    });
            });


            isRecords.then((res) => {
                if (res.data.length > 0) {
                    osfarm.setProperty("/records", res.data);
                    console.log(osfarm);
                } else {
                    console.log("no tiene data ");
                }
            });
        },

        handleDelete: function(oEvent){
            let sId = oEvent.getParameters().listItem.sId,
                asId = sId.split("-"),
                idx = asId[asId.length-1],
                mdprogrammed = this.getModel("mdprogrammed"),
                that = this;

            //  let obj =  mdprogrammed.getProperty("/records/"+idx);
            let records =  mdprogrammed.getProperty("/selectedRecord/lot_assigned/");
            let assigneds = mdprogrammed.getProperty("/assigned");
            console.log(assigneds[idx]);
       
            var dialog = new Dialog({
                title: "Confirmación",
                type: "Message",
                content: new Text({
                    text: "Se procedera a eliminar el lote: " + assigneds[idx].name

                }),
                beginButton: new Button({
                    text: "Continuar",
                    press: function () {
                        // debugger;
                        // that.deleteProgrammed(obj.lot);
                        let residue= parseInt(mdprogrammed.getProperty("/selectedRecord/residue"));
                        let quantity= parseInt(assigneds[idx].quantity_chicken);
                        mdprogrammed.setProperty("/selectedRecord/residue" , + residue+ quantity);
                        const records = mdprogrammed.getProperty("/selectedRecords");
                        const record = records.find(record => record.broiler_id == assigneds[idx].selected_lot);
                        record.partial_residue -= assigneds[idx].quantity_chicken;
                        assigneds.splice(idx,1);
                        if(assigneds.length>0){
                            mdprogrammed.setProperty("/confirmBtn", true);
                        }
                        else{
                            mdprogrammed.setProperty("/confirmBtn", false);
                        }
                        mdprogrammed.refresh();
                        dialog.close();
                    }
                }),
                endButton: new Button({
                    text: "Cancelar",
                    press: function () {
                        dialog.close();
                    }
                }),
                afterClose: function() {
                    dialog.destroy();
                }
            });

            dialog.open();

        },
        deleteProgrammed: function(lot) {
            let that = this,
                util = this.getModel("util"),
                mdprogrammed = this.getModel("mdprogrammed"),
                serverName = util.getProperty("/serviceUrl") + util.getProperty("/" + util.getProperty("/service") + "/deleteBroilerDetail"),
                scenario_id = this.getModel("mdscenario").getProperty("/scenario_id"),
                partnership_id = this.getView().getModel("ospartnership").getProperty("/records/" + this.index + "/partnership_id"),
                breed_id = this.getModel("mdprogrammed").getProperty("/selectedRecord/breed_id"),
                _date =   mdprogrammed.getProperty("/selectedRecord/updateDate/");


            fetch(serverName, {
                method: "DELETE",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify({
                    lot: lot,
                    scenario_id: scenario_id,
                    _date: _date,
                    partnership_id: partnership_id,
                    breed_id: breed_id
                })
            })
                .then(
                    function(response) {
                        if (response.status !== 200) {
                            console.log("Looks like there was a problem. Status Code: " +
                  response.status);
                            response.json().then(
                                function(resp){
                                    MessageToast.show(resp.msg);
                                }
                            );

                            return;
                        }
                        // Examine the text in the response
                        response.json().then(function(res) {
                            let records = res.data;
                            mdprogrammed.setProperty("/records", records);
                            console.log(mdprogrammed);
                            that.hideButtons(false, true, false, false);

                            if (records.length > 0) {
                                mdprogrammed.setProperty("/executionSaveBtn", true);
                                let residue_programmed = res.residue,
                                    projected_quantity = mdprogrammed.getProperty("/selectedRecord/projected_quantity"),
                                    total = projected_quantity - residue_programmed;
                                mdprogrammed.setProperty("/programmed_residue", total);
                            } else {
                                mdprogrammed.setProperty("/programmed_residue", mdprogrammed.getProperty("/selectedRecord/projected_quantity"));
                                mdprogrammed.setProperty("/executionSaveBtn", false);
                            }
                            util.setProperty("/busy/", true);
                        });
                    }
                )
                .catch(function(err) {
                    console.log("Fetch Error :-S", err);
                });
        },



        onDialogPressEx: function() {

            let that = this,
                util = this.getModel("util"),
                mdprogrammed = this.getModel("mdprogrammed"),
                mdexecuted = this.getModel("mdexecuted"),
                aRecords = mdexecuted.getProperty("/records"),
                sRecords = mdprogrammed.getProperty("/selectedRecord"),
                execution_quantity = mdexecuted.getProperty("/execution_quantity"),
                scenario_id = this.getModel("mdscenario").getProperty("/scenario_id"),
                mdscenario = this.getModel("mdscenario"),
                partnership_id = this.getView().getModel("ospartnership").getProperty("/records/" + this.index + "/partnership_id"),
                farm_id = this.getView().getModel("mdfarms").getProperty("/selectedKey"),
                center_id = this.getView().getModel("mdcenter").getProperty("/selectedKey"),
                mdshed = this.getModel("mdshed"),
                shed_id = mdshed.getProperty("/selectedKey");
            console.log(mdprogrammed.getProperty("/selectedRecord/projected_date"));
            console.log(aRecords[0].execution_date);
            let pDate = aRecords[0].execution_date,
                aDate = pDate.split("/"),
                minDate = new Date(aDate[2], aDate[1] - 1, aDate[0]),
                date2 = new Date(aDate[2], aDate[1] - 1, aDate[0]),
                maxDate = this.addDays(date2, 7),
                breed_id = this.getModel("mdprogrammed").getProperty("/selectedRecords/0/breed_id"),
                broiler_id = this.getModel("mdprogrammed").getProperty("/selectedRecords/0/broiler_id");
            console.log(broiler_id);
            console.log(breed_id);
            console.log(mdexecuted);
            console.log(mdprogrammed);
            mdprogrammed.setProperty("/selectedRecord/minDate/", minDate);
            mdprogrammed.setProperty("/selectedRecord/maxDate/", maxDate);
        

            console.log(broiler_id);
            // ospartnership = this.getView().getModel("ospartnership");
            // ospartnership.setProperty("/selectedPartnership/partnership_index", this.index);


            console.log(mdscenario);

            let housing_way_id = mdprogrammed.getProperty("/selectedRecord/broilereviction_id");
            let records_programmed = [],
                isValidRecord = true;

            mdshed = this.getModel("mdshed");

            console.log("el modelo shed en validacion");
            console.log(mdshed);

            console.log("el modelo mdprogrammed en validacion");
            console.log(mdprogrammed);
            console.log("execution_quantiy",execution_quantity);

            aRecords.forEach(item => {
                console.log("el item");
                console.log(item);
                // if ((item.available!== undefined && item.available!== null && item.available)) {
                // console.log("entro en el primer if")
                //   item.execution_date = sRecords.projected_date;
                //console.log("si entro en el if" + item.execution_date);
                if (item.execution_date && (parseInt(execution_quantity))) {
                    item.executedfarm_id = farm_id;
                    item.executedcenter_id = center_id;
                    item.executedshed_id = shed_id;
                    item.execution_quantity = execution_quantity;
                    console.log("No es null los valores son: ", item.execution_date, item.execution_quantity);
                    records_programmed.push(item);
                }



                if ((!item.execution_date) && parseInt(execution_quantity)) {
                    console.log("entro en el primer if 2");
                    console.log("execution_date null");
                    item.state_date = "Error";
                    item.state_text_date = "El campo fecha no puede estar en blanco";
                    isValidRecord = false;
                } else {
                    item.state_date = "None";
                    item.state_text_date = "";
                }

                if ((item.execution_date) && (!parseInt(execution_quantity))) {
                    console.log("entro en el primer if 3");
                    console.log("execution_date null");
                    item.state_quantity = "Error";
                    item.state_text_quantity = "El campo cantidad no puede estar en blanco";
                    isValidRecord = false;
                } else {
                    item.state_quantity = "None";
                    item.state_text_quantity = "";
                }

                // if ((item.execution_date) && (parseInt(execution_quantity) > item.capacity_shed)) {
                //   console.log("entro en el primer if 4")
                //   console.log("execution_date null");
                //   item.state_quantity = 'Error';
                //   item.state_text_quantity = 'El campo cantidad supera la capacidad del galpon';
                //   isValidRecord = false;
                // } else {
                //     item.state_quantity = 'None';
                //     item.state_text_quantity = '';
                // }

                /*
                if ((!item.execution_date)) {
                    console.log("execution_date null");
                    item.state_date = 'None';
                    item.state_text_date = 'Recuerde asignar la cantidad ejecutada';
                    isValidRecord = false;
                } else {
                    item.state_date = 'None';
                    item.state_text_date = '';
                }

                if ((!item.execution_quantity)) {
                    console.log("execution_date quantity");
                    item.state_quantity = 'None';
                    item.state_text_quantity = 'Recuerde asignar la cantidad ejecutada';
                    isValidRecord = false;
                } else {
                    item.state_quantity = 'None';
                    item.state_text_quantity = '';
                }*/
            // }
            });

            mdprogrammed.refresh(true);
        
            console.log("inserta esto:");
            console.log(records_programmed);
            // debugger;
            const serverName = util.getProperty("/serviceUrl") + util.getProperty("/" + util.getProperty("/service") + "/addbroilerdetail");
            console.log(serverName);

            // if (records_programmed.length > 0 && isValidRecord) {
            if (records_programmed.length > 0 ) {
                console.log("Se va a realizar la consulta al backend: ");

                var dialogC = new Dialog({
                    title: "Aviso",
                    type: "Message",
                    content: new Text({
                        text: "¿Desea guardar los cambios?"
                    }),
                    beginButton: new Button({
                        text: "Aceptar",
                        press: function() {
              
                            fetch(serverName, {
                                method: "PUT",
                                headers: {
                                    "Content-Type": "application/json"
                                },
                                body: JSON.stringify({
                                    records: records_programmed,
                                    scenario_id: scenario_id,
                                    _date: aDate[2]+"-"+aDate[1]+"-"+aDate[0],
                                    partnership_id: partnership_id,
                                    breed_id: breed_id,
                                    broiler_id: broiler_id
                                })
                            })
                                .then(
                                    function(response) {
                                        if (response.status !== 200) {
                                            console.log("Looks like there was a problem. Status Code: " +
                  response.status);
                                            return;
                                        }
                                        console.log("Esperando respuesta... ");
                                        response.json().then(function(res) {
                                            console.log("se recibe la repsuesta");
                                            console.log(res.data);

                                            mdexecuted.setProperty("/isnotexecuted",false);
                                            mdexecuted.setProperty("/isexecuted",true);
                                            mdprogrammed.setProperty("/records", res.data);
                                            mdexecuted.setProperty("/name/state", "None");
                                            mdexecuted.setProperty("/name/stateText", "");
                                            mdexecuted.setProperty("/saveBtn", false);
                                            that.findExecuted();


                                            var dialog = new Dialog({
                                                title: "Información",
                                                state: "Success",
                                                type: "Message",
                                                content: new Text({
                                                    text: "Información guardada con éxito."
                                                }),
                                                beginButton: new Button({
                                                    text: "OK",
                                                    press: function() {
                                                        dialog.close();
                                                    }
                                                }),
                                                afterClose: function() {
                                                    dialog.destroy();
                                                }
                                            });

                                            dialog.attachBeforeClose(function(){
                                                dialogC.close();
                                            });
                                            dialog.open();

                                        });
                                    }
                                )
                                .catch(function(err) {
                                    console.log("Fetch Error :-S", err);
                                });
              
                        }
                    }),
                    endButton: new Button({
                        text: "Cancelar",
                        press: function() {
                            dialogC.close();
                        }
                    }),
                    afterClose: function() {
                        dialogC.destroy();
                    }
                });
                console.log("Aqui está");
                dialogC.open();
        
            } else if (!isValidRecord) {

                this.onToast("Faltan campos");
            } else {
                //No se detectaron cambios
                this.onToast("No de detectaron cambios");
            }
        },









    

        onDialogPressExBrayan: function() {

            let that = this,
                util = this.getModel("util"),
                mdprogrammed = this.getModel("mdprogrammed"),
                aRecords = mdprogrammed.getProperty("/records"),
                mdscenario = this.getModel("mdscenario");
            console.log(mdscenario);

            let housing_way_id = mdprogrammed.getProperty("/selectedRecord/housing_way_id");
            console.log("housing_way_id: ", housing_way_id);
            let records_programmed = [],
                isValidRecord = true;
            aRecords.forEach(item => {
                if ((item.available == true)) {
                    if ((item.execution_date) && (item.execution_quantity)) {
                        console.log("No es null los valores son: ", item.execution_date, item.execution_quantity);
                        records_programmed.push(item);
                    }
                    if ((!item.execution_date) && (item.execution_quantity)) {
                        console.log("execution_date null");
                        item.state_date = "Error";
                        item.state_text_date = "El campo no puede estar en blanco";
                        isValidRecord = false;
                    } else {
                        item.state_date = "None";
                        item.state_text_date = "";
                    }

                    if ((item.execution_date) && (!item.execution_quantity)) {
                        console.log("execution_date quantity");
                        item.state_quantity = "Error";
                        item.state_text_quantity = "El campo no puede estar en blanco";
                        isValidRecord = false;
                    } else {
                        item.state_quantity = "None";
                        item.state_text_quantity = "";
                    }
                }
            });
            mdprogrammed.refresh(true);
            console.log(aRecords);
            console.log(records_programmed);
            const serverName = util.getProperty("/serviceUrl") + util.getProperty("/" + util.getProperty("/service") + "/housingwaydetail");
            if (records_programmed.length > 0 && isValidRecord) {
                fetch(serverName, {
                    method: "PUT",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify({
                        records: records_programmed,
                        stage_id: breedingStage,
                        housing_way_id: housing_way_id,
                        scenario_id: mdscenario.getProperty("/scenario_id")
                    })
                })
                    .then(
                        function(response) {
                            if (response.status !== 200) {
                                console.log("Looks like there was a problem. Status Code: " +
                  response.status);
                                return;
                            }

                            response.json().then(function(res) {
                                var dialog = new Dialog({
                                    title: "Información",
                                    type: "Message",
                                    state: "Success",
                                    content: new Text({
                                        text: "Información guardada con éxito."
                                    }),
                                    beginButton: new Button({
                                        text: "OK",
                                        press: function() {
                                            dialog.close();
                                            mdprogrammed.setProperty("/records", res.data);
                                        }
                                    }),
                                    afterClose: function() {
                                        dialog.destroy();
                                    }
                                });

                                dialog.open();

                            });
                        }
                    )
                    .catch(function(err) {
                        console.log("Fetch Error :-S", err);
                    });
            } else if (!isValidRecord) {

                this.onToast("Faltan campos");
            } else {
                //No se detectaron cambios
                this.onToast("No de detectaron cambios");
            }
        },


        reloadProgrammed: function(broiler_detail, mdprogrammed){
            let broilers = [];
  
            // housingway_detail.forEach(element => {
            //   housing_ways.push(element.housing_way_id)
            // });
            broilers = broiler_detail.map(record => record.broiler_id);
            console.log(broilers);
            fetch("/broilerdetail/findbroilerdetail", {
                headers: {
                    "Content-Type": "application/json"
                },
                method: "POST",
                body: JSON.stringify({
                    records: broilers
                })
            })
                .then(
                    function(response) {
                        if (response.status !== 200) {
                            console.log("Looks like there was a problem. Status Code: " +
                  response.status);
                            return;
                        }
  
                        response.json().then(function(res) {
                            console.log(res.data);
                            mdprogrammed.setProperty("/records",res.data);
                        });
                    }
                )
                .catch(function(err) {
                    console.log("Fetch Error :-S", err);
                });
  
        },



        toSap: function () {
            let util = this.getModel("util"),
                mdprogrammed = this.getView().getModel("mdprogrammed"),
                ids = mdprogrammed.getProperty("/selectedRecords"),
                that = this;

          
        
            var dialogToSap = new Dialog({
                title: "Confirmación",
                type: "Message",
                content: new Text({
                    text: "Se procedera a sincronizar: "
                }),
                beginButton: new Button({
                    text: "Continuar",
                    press: function () {
                    
                        dialogToSap.close();
                        dialogToSap.destroy();
                        util.setProperty("/busy", true);

                        fetch("/synchronization/syncEngorde", {
                            method: "GET"
                        })
                            .then(
                                function(response) {
                                    if (response.status !== 200) {
                                        util.setProperty("/busy", false);
                                        console.log("Looks like there was a problem. Status Code: " + response.status);
                                        var dialog = new Dialog({
                                            title: "Información",
                                            type: "Message",
                                            state: "Error",
                                            content: new Text({
                                                text: "Error de sincronización."
                                            }),
                                            beginButton: new Button({
                                                text: "OK",
                                                press: function() {
                                                    dialog.close();
                                                    dialogToSap.close();
                                                    dialogToSap.destroy();
                                                }
                                            }),
                                            afterClose: function() {
                                                dialog.destroy();
                                            }
                                        });
                                        dialog.open();
                                        return;
                                    }else{
                                        util.setProperty("/busy", false);
                                        response.json().then(function(res) {
                                            console.log("la respuesta despues de sincronizar");
                                            console.log(res);

                                            let texto = "";
                                            if (res.resp.length > 0) 
                                            {
                                                console.log("si hubo");
                                                texto = "Sincronización realizada con éxito.\n"+ res.resp[0].satisfactorios + " registro(s) guardados\n"+res.resp[0].error+" registro(s) erroneos";

                                            }
                                            else{
                                                console.log("no hubo");
                                                texto = "Todos los registros ya han sido sincronizados";
                                            }
                            
                                            that.reloadProgrammed(ids, mdprogrammed);
                            
                                            var dialog = new Dialog({
                                                title: "Información",
                                                type: "Message",
                                                state: "Success",
                                                content: new Text({
                                                    text: texto
                                                }),
                                                beginButton: new Button({
                                                    text: "OK",
                                                    press: function() {
                                                        dialog.close();
                                                        dialogToSap.close();
                                                        dialogToSap.destroy();
                                                    }
                                                }),
                                                afterClose: function() {
                                                    dialog.destroy();
                                                }
                                            });
                                            dialog.open();
                                        });
                                    }
                                }
                            )
                            .catch(function(err) {
                                console.log("Fetch Error :-S", err);
                                util.setProperty("/busy", false);
                                var dialog = new Dialog({
                                    title: "Información",
                                    type: "Message",
                                    state: "Error",
                                    content: new Text({
                                        text: "Error de sincronización."
                                    }),
                                    beginButton: new Button({
                                        text: "OK",
                                        press: function() {
                                            dialog.close();
                                            dialogToSap.close();
                                            dialogToSap.destroy();
                                        }
                                    }),
                                    afterClose: function() {
                                        dialog.destroy();
                                    }
                                });
                                dialog.open();
                            });
                    }
                }),
                endButton: new Button({
                    text: "Cancelar",
                    press: function () {
                        dialogToSap.close();
                      
                    }
                }),
                afterClose: function() {
                    dialogToSap.destroy();
                }
            });
            dialogToSap.open();
        },
    
        onPressDetProg: function(oEvent){
            let that = this,
                path = oEvent.getSource().oPropagatedProperties.oBindingContexts.mdprogrammed.sPath;
            console.log("path: ",path);
            var dialog = new Dialog({
                title: "Información",
                type: "Message",
                state: "Warning",
                content: new Text({
                    text: "¿Desea eliminar la programación seleccionada?."
                }),
                beginButton: new Button({
                    text: "Aceptar",
                    press: function() {
                        that.onUpdateDisabled(path);
                        dialog.close();
                    }
                }),
                endButton: new Button({
                    text: "Cancelar",
                    press: function() {
                        dialog.close();
                    }
                }),
                afterClose: function() {
                    dialog.destroy();
                }
            });
  
            dialog.open();
        },
        closeProgrammedDialog: function(oEvent) {
            const mdprogrammed = this.getView().getModel("mdprogrammed");
            mdprogrammed.getProperty("/selectedRecords").forEach(record => record.partial_residue = 0);
            mdprogrammed.setProperty("/assigned", []);
        },
  
        onUpdateDisabled: function(path){
            let mdprogrammed = this.getView().getModel("mdprogrammed"),
                selectedItem = mdprogrammed.getProperty(path),
                id = selectedItem.broiler_detail_id,
                broiler_id = selectedItem.broiler_id,
                scenario_id = this.getModel("mdscenario").getProperty("/scenario_id"),
                partnership_id = this.getView().getModel("ospartnership").getProperty("/records/" + this.index + "/partnership_id"),
                breed_id = this.getModel("mdprogrammed").getProperty("/selectedRecords/0/breed_id"),
                date = this.getModel("mdprogrammed").getProperty("/selectedRecords/0/projected_date"),
                shed_id = selectedItem.shed_id;
            console.log(this.getModel("mdprogrammed").getProperty("/selectedRecords"));
            console.log(selectedItem);
            console.log(mdprogrammed);
  
  
            fetch("/broilerDetail/updateDisabledbroilerdetail", {
                headers: {
                    "Content-Type": "application/json"
                },
                method: "PUT",
                body: JSON.stringify({
                    broiler_id : broiler_id,
                    broiler_detail_id: id,
                    execution_date: date,
                    scenario_id: scenario_id,
                    partnership_id: partnership_id,
                    breed_id: breed_id,
                    shed_id : shed_id
                })
            })
                .then(
                    function(response) {
                        if (response.status !== 200 && response.status !== 409) {
                            console.log("Looks like there was a problem. Status Code: " +
                    response.status);
                            return;
                        }
  
                        if(response.status === 409){
                            var dialog = new Dialog({
                                title: "Información",
                                type: "Message",
                                state: "Error",
                                content: new Text({
                                    text: "No se puede eliminar la programación, porque ya ha sido ejecutada."
                                }),
                                beginButton: new Button({
                                    text: "OK",
                                    press: function() {
                                        dialog.close();
                                    }
                                }),
                                afterClose: function() {
                                    dialog.destroy();
                                }
                            });
    
                            dialog.open();
                        }
                
                        if(response.status === 200){
                            response.json().then(function(res) {
                                var dialog = new Dialog({
                                    title: "Información",
                                    type: "Message",
                                    state: "Success",
                                    content: new Text({
                                        text: "Programación eliminada con éxito."
                                    }),
                                    beginButton: new Button({
                                        text: "OK",
                                        press: function() {
                                            dialog.close();
                                            let records = res.data;
                                            // records.forEach(element => {
                                            //   if( element.executionslaughterhouse_id && element.execution_quantity && element.execution_date){
                                            //     element.isexecuted = true;
                                            //   }else{
                                            //     element.isexecuted = false;
                                            //   }
                            
                                            // });
                                            mdprogrammed.setProperty("/records", records);
                                            mdprogrammed.refresh(true);
                          
                                        }
                                    }),
                                    afterClose: function() {
                                        dialog.destroy();
                                    }
                                });
      
                                dialog.open();
                                console.log(res);
                  
                            });
                        }
                
                    }
                )
                .catch(function(err) {
                    console.log("Fetch Error :-S", err);
                });
  
        }


    });
});
