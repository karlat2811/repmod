sap.ui.define([], function() {
    "use strict";

    return {
        calculateResidue: function(projected_quantity, residue) {
            return (projected_quantity-residue).toLocaleString();
        },
        calculateResidue2: function(projected_quantity, residue, partial_residue = 0) {
            console.log("FORMATTER ------------------------");
            console.log(projected_quantity, residue, partial_residue);
            return (projected_quantity - residue - partial_residue).toLocaleString();
        },
        sumResidues: function(records = []) {
            const sum = records.reduce((result, actual) => {
                return result + actual.projected_quantity - actual.residue;
            }, 0);
            return sum.toLocaleString();
        },

        formatMiles: function(number){
            return (number !== "" && !isNaN(number)) ? parseInt(number).toLocaleString() : number;

        },
        formatStatus:function(status){
            let ret;
            if(parseInt(status) < 0){
                ret = "Error";
				
            }else{
                if(parseInt(status) >= 0){
				  ret = "Success";
				  
                }else{
				  ret = "None";
                }
            }
            return ret;
        },

        enabledSaveBtn: function(records){
            console.log("enabled?: ", (records.length>0));
            return(records.length>0);
        },
        formaticon: function(value){
            let ret;
            if (value=== true){
                ret = "sap-icon://accept";
            }else{
                ret = "sap-icon://decline";
            }

            return(ret);

        },
        formatcolor: function(value){
            let ret;
            if (value=== true){
                ret = "green";
            }else{
                ret = "red";
            }

            return(ret);

        },
        formatVisible: function(value, value2){
            console.log(value, value2);
            console.log(!value||!value2);
            return !(value||value2);
        }
        // formatVisible: function(value){
        // 	return !value
        // }
    };
});
