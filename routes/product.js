const express = require("express");
const config = require("../config");

const productCtrl = config.driver === "postgres" ? require("../postgresql/controllers/product") : require("./hcp/controllers/product");

const api_product = express.Router();



api_product.get("/", productCtrl.findAllProduct);
api_product.post("/", productCtrl.addProduct);
api_product.post("/changeName", productCtrl.changeName);
api_product.post("/changeCode", productCtrl.changeCode);
api_product.post("/isBeingUsed", productCtrl.isBeingUsed);
api_product.put("/", productCtrl.updateProduct);
api_product.delete("/", productCtrl.deleteProduct);



module.exports = api_product;
