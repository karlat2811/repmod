const express = require("express");
const config = require("../config");

const breedCtrl = config.driver === "postgres" ? require("../postgresql/controllers/breed") : require("./hcp/controllers/Calendar");

const api_breed = express.Router();


api_breed.get("/findAllBreedWP", breedCtrl.findAllBreedWP);
api_breed.get("/", breedCtrl.findAllBreed);
api_breed.post("/", breedCtrl.addBreed);
api_breed.post("/changeName", breedCtrl.changeName);
api_breed.post("/changeCode", breedCtrl.changeCode);
api_breed.post("/isBeingUsed", breedCtrl.isBeingUsed);
api_breed.put("/", breedCtrl.updateBreed);
api_breed.delete("/", breedCtrl.deleteBreed);
api_breed.get("/findBreedByCode/:code", breedCtrl.findBreedByCode);
api_breed.get("/findBreedByCurve", breedCtrl.findBreedByCurve);


module.exports = api_breed;
