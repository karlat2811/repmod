PGDMP     ,    8        
    	    v            planning    10.3    10.3 �   �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            �           1262    110369    planning    DATABASE     �   CREATE DATABASE planning WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Spanish_Venezuela.1252' LC_CTYPE = 'Spanish_Venezuela.1252';
    DROP DATABASE planning;
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            �           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3                        3079    12924    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            �           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1259    110370    abaTimeUnit_id_seq    SEQUENCE     �   CREATE SEQUENCE public."abaTimeUnit_id_seq"
    START WITH 2
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 +   DROP SEQUENCE public."abaTimeUnit_id_seq";
       public       postgres    false    3            �            1259    110372    aba_breeds_and_stages_id_seq    SEQUENCE     �   CREATE SEQUENCE public.aba_breeds_and_stages_id_seq
    START WITH 8
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 3   DROP SEQUENCE public.aba_breeds_and_stages_id_seq;
       public       postgres    false    3            �            1259    110374    aba_breeds_and_stages    TABLE       CREATE TABLE public.aba_breeds_and_stages (
    id integer DEFAULT nextval('public.aba_breeds_and_stages_id_seq'::regclass) NOT NULL,
    code character varying(100),
    name character varying(100),
    id_aba_consumption_and_mortality integer,
    id_process integer
);
 )   DROP TABLE public.aba_breeds_and_stages;
       public         postgres    false    197    3            �           0    0    TABLE aba_breeds_and_stages    COMMENT     o   COMMENT ON TABLE public.aba_breeds_and_stages IS 'Relaciona los procesos de ARP con el consumo y mortalidad ';
            public       postgres    false    198            �           0    0    COLUMN aba_breeds_and_stages.id    COMMENT     o   COMMENT ON COLUMN public.aba_breeds_and_stages.id IS 'Id de la relacion entre proceso y consumo y mortalidad';
            public       postgres    false    198            �           0    0 !   COLUMN aba_breeds_and_stages.code    COMMENT     u   COMMENT ON COLUMN public.aba_breeds_and_stages.code IS 'Codigo de la relacion entre proceso y consumo y mortalidad';
            public       postgres    false    198            �           0    0 !   COLUMN aba_breeds_and_stages.name    COMMENT     u   COMMENT ON COLUMN public.aba_breeds_and_stages.name IS 'Nombre de la relacion entre proceso y consumo y mortalidad';
            public       postgres    false    198            �           0    0 =   COLUMN aba_breeds_and_stages.id_aba_consumption_and_mortality    COMMENT     �   COMMENT ON COLUMN public.aba_breeds_and_stages.id_aba_consumption_and_mortality IS 'Id de tabla aba_consumption_and_mortality (FK)';
            public       postgres    false    198            �           0    0 '   COLUMN aba_breeds_and_stages.id_process    COMMENT     Y   COMMENT ON COLUMN public.aba_breeds_and_stages.id_process IS 'Id de la tabla mdprocess';
            public       postgres    false    198            �            1259    110378 $   aba_consumption_and_mortality_id_seq    SEQUENCE     �   CREATE SEQUENCE public.aba_consumption_and_mortality_id_seq
    START WITH 8
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 ;   DROP SEQUENCE public.aba_consumption_and_mortality_id_seq;
       public       postgres    false    3            �            1259    110380    aba_consumption_and_mortality    TABLE     $  CREATE TABLE public.aba_consumption_and_mortality (
    id integer DEFAULT nextval('public.aba_consumption_and_mortality_id_seq'::regclass) NOT NULL,
    code character varying(100),
    name character varying(100),
    id_breed integer,
    id_stage integer,
    id_aba_time_unit integer
);
 1   DROP TABLE public.aba_consumption_and_mortality;
       public         postgres    false    199    3            �           0    0 #   TABLE aba_consumption_and_mortality    COMMENT     �   COMMENT ON TABLE public.aba_consumption_and_mortality IS 'Almacena la información del consumo y mortalidad asociados a la combinacion de raza y etapa';
            public       postgres    false    200            �           0    0 '   COLUMN aba_consumption_and_mortality.id    COMMENT     �   COMMENT ON COLUMN public.aba_consumption_and_mortality.id IS 'Id de los datos de consumo y mortalidad asociados a una raza y una etapa';
            public       postgres    false    200            �           0    0 )   COLUMN aba_consumption_and_mortality.code    COMMENT     �   COMMENT ON COLUMN public.aba_consumption_and_mortality.code IS 'Codigo de los datos de consumo y mortalidad asociados a una raza y una etapa ';
            public       postgres    false    200            �           0    0 )   COLUMN aba_consumption_and_mortality.name    COMMENT     �   COMMENT ON COLUMN public.aba_consumption_and_mortality.name IS 'Nombre de los datos de consumo y mortalidad asociados a una raza y una etapa';
            public       postgres    false    200            �           0    0 -   COLUMN aba_consumption_and_mortality.id_breed    COMMENT     �   COMMENT ON COLUMN public.aba_consumption_and_mortality.id_breed IS 'Id de la raza asociada a los datos de consumo y mortalidad';
            public       postgres    false    200            �           0    0 -   COLUMN aba_consumption_and_mortality.id_stage    COMMENT     �   COMMENT ON COLUMN public.aba_consumption_and_mortality.id_stage IS 'id de la etapa en la que se encuentran los datos de consumo y mortalidad ';
            public       postgres    false    200            �           0    0 5   COLUMN aba_consumption_and_mortality.id_aba_time_unit    COMMENT     �   COMMENT ON COLUMN public.aba_consumption_and_mortality.id_aba_time_unit IS 'Id de la unidad de tiempo utilizada en los datos cargados en consumo y mortalidad (dias o semanas)';
            public       postgres    false    200            �            1259    110384 +   aba_consumption_and_mortality_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.aba_consumption_and_mortality_detail_id_seq
    START WITH 203
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 B   DROP SEQUENCE public.aba_consumption_and_mortality_detail_id_seq;
       public       postgres    false    3            �            1259    110386 $   aba_consumption_and_mortality_detail    TABLE     =  CREATE TABLE public.aba_consumption_and_mortality_detail (
    id integer DEFAULT nextval('public.aba_consumption_and_mortality_detail_id_seq'::regclass) NOT NULL,
    id_aba_consumption_and_mortality integer NOT NULL,
    time_unit_number integer,
    consumption double precision,
    mortality double precision
);
 8   DROP TABLE public.aba_consumption_and_mortality_detail;
       public         postgres    false    201    3            �           0    0 *   TABLE aba_consumption_and_mortality_detail    COMMENT     �   COMMENT ON TABLE public.aba_consumption_and_mortality_detail IS 'Almacena los detalles para la unidad de tiempo asociada a una determinada agrupación de consumo y mortalidad ';
            public       postgres    false    202            �           0    0 .   COLUMN aba_consumption_and_mortality_detail.id    COMMENT     �   COMMENT ON COLUMN public.aba_consumption_and_mortality_detail.id IS 'Id de los detalles para la unidad de tiempo asociada a una determinada agrupación de consumo y mortalidad ';
            public       postgres    false    202            �           0    0 L   COLUMN aba_consumption_and_mortality_detail.id_aba_consumption_and_mortality    COMMENT     �   COMMENT ON COLUMN public.aba_consumption_and_mortality_detail.id_aba_consumption_and_mortality IS 'Id de la agrupación de consumo y mortalidad asociada';
            public       postgres    false    202            �           0    0 <   COLUMN aba_consumption_and_mortality_detail.time_unit_number    COMMENT     �   COMMENT ON COLUMN public.aba_consumption_and_mortality_detail.time_unit_number IS 'Indica la unidad de tiempo asociada a la agrupacion de consumo y mortalidad';
            public       postgres    false    202            �           0    0 7   COLUMN aba_consumption_and_mortality_detail.consumption    COMMENT     �   COMMENT ON COLUMN public.aba_consumption_and_mortality_detail.consumption IS 'Consumo asociado a una determinada agrupación de consumo y mortalidad ';
            public       postgres    false    202            �           0    0 5   COLUMN aba_consumption_and_mortality_detail.mortality    COMMENT     �   COMMENT ON COLUMN public.aba_consumption_and_mortality_detail.mortality IS 'Mortalidad asociada a una determinada agrupación de consumo y mortalidad ';
            public       postgres    false    202            �            1259    110390    aba_elements_id_seq    SEQUENCE     �   CREATE SEQUENCE public.aba_elements_id_seq
    START WITH 22
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 *   DROP SEQUENCE public.aba_elements_id_seq;
       public       postgres    false    3            �            1259    110392    aba_elements    TABLE       CREATE TABLE public.aba_elements (
    id integer DEFAULT nextval('public.aba_elements_id_seq'::regclass) NOT NULL,
    code character varying(100),
    name character varying(100),
    id_aba_element_property integer,
    equivalent_quantity double precision
);
     DROP TABLE public.aba_elements;
       public         postgres    false    203    3            �           0    0    TABLE aba_elements    COMMENT     T   COMMENT ON TABLE public.aba_elements IS 'Almacena los datos de los macroelementos';
            public       postgres    false    204            �           0    0    COLUMN aba_elements.id    COMMENT     D   COMMENT ON COLUMN public.aba_elements.id IS 'Id del macroelemento';
            public       postgres    false    204            �           0    0    COLUMN aba_elements.code    COMMENT     J   COMMENT ON COLUMN public.aba_elements.code IS 'Codigo del macroelemento';
            public       postgres    false    204            �           0    0    COLUMN aba_elements.name    COMMENT     J   COMMENT ON COLUMN public.aba_elements.name IS 'Nombre del macroelemento';
            public       postgres    false    204            �           0    0 +   COLUMN aba_elements.id_aba_element_property    COMMENT     q   COMMENT ON COLUMN public.aba_elements.id_aba_element_property IS 'Id de la propiedad asociada al macroelemento';
            public       postgres    false    204            �           0    0 '   COLUMN aba_elements.equivalent_quantity    COMMENT     �   COMMENT ON COLUMN public.aba_elements.equivalent_quantity IS 'Cantidad de la propiedad asociada al macroelemento con el fin de realizar equivalencias';
            public       postgres    false    204            �            1259    110396 &   aba_elements_and_concentrations_id_seq    SEQUENCE     �   CREATE SEQUENCE public.aba_elements_and_concentrations_id_seq
    START WITH 105
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 =   DROP SEQUENCE public.aba_elements_and_concentrations_id_seq;
       public       postgres    false    3            �            1259    110398    aba_elements_and_concentrations    TABLE     k  CREATE TABLE public.aba_elements_and_concentrations (
    id integer DEFAULT nextval('public.aba_elements_and_concentrations_id_seq'::regclass) NOT NULL,
    id_aba_element integer,
    id_aba_formulation integer,
    proportion double precision,
    id_element_equivalent integer,
    id_aba_element_property integer,
    equivalent_quantity double precision
);
 3   DROP TABLE public.aba_elements_and_concentrations;
       public         postgres    false    205    3            �           0    0 %   TABLE aba_elements_and_concentrations    COMMENT     x   COMMENT ON TABLE public.aba_elements_and_concentrations IS 'Asocia una formula con los macroelementos que la componen';
            public       postgres    false    206            �           0    0 )   COLUMN aba_elements_and_concentrations.id    COMMENT     �   COMMENT ON COLUMN public.aba_elements_and_concentrations.id IS 'Id de la asociación entre una formula con los macroelementos que la componen';
            public       postgres    false    206            �           0    0 5   COLUMN aba_elements_and_concentrations.id_aba_element    COMMENT     g   COMMENT ON COLUMN public.aba_elements_and_concentrations.id_aba_element IS 'Id del elemento asociado';
            public       postgres    false    206            �           0    0 9   COLUMN aba_elements_and_concentrations.id_aba_formulation    COMMENT     l   COMMENT ON COLUMN public.aba_elements_and_concentrations.id_aba_formulation IS 'Id de la formula asociado';
            public       postgres    false    206            �           0    0 1   COLUMN aba_elements_and_concentrations.proportion    COMMENT     x   COMMENT ON COLUMN public.aba_elements_and_concentrations.proportion IS 'Proporción del elemento dentro de la formula';
            public       postgres    false    206            �            1259    110402    aba_elements_properties_id_seq    SEQUENCE     �   CREATE SEQUENCE public.aba_elements_properties_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 5   DROP SEQUENCE public.aba_elements_properties_id_seq;
       public       postgres    false    3            �            1259    110404    aba_elements_properties    TABLE     �   CREATE TABLE public.aba_elements_properties (
    id integer DEFAULT nextval('public.aba_elements_properties_id_seq'::regclass) NOT NULL,
    code character varying(100),
    name character varying(100)
);
 +   DROP TABLE public.aba_elements_properties;
       public         postgres    false    207    3            �           0    0    TABLE aba_elements_properties    COMMENT     �   COMMENT ON TABLE public.aba_elements_properties IS 'Almacena las propiedades que pueden llegar a tener los macroelementos para realizar la equivalencia';
            public       postgres    false    208            �           0    0 !   COLUMN aba_elements_properties.id    COMMENT     Z   COMMENT ON COLUMN public.aba_elements_properties.id IS 'Id de la propiedad del elemento';
            public       postgres    false    208            �           0    0 #   COLUMN aba_elements_properties.code    COMMENT     _   COMMENT ON COLUMN public.aba_elements_properties.code IS 'Codigode la propiedad del elemento';
            public       postgres    false    208            �           0    0 #   COLUMN aba_elements_properties.name    COMMENT     `   COMMENT ON COLUMN public.aba_elements_properties.name IS 'Nombre de la propiedad del elemento';
            public       postgres    false    208            �            1259    110408    aba_formulation_id_seq    SEQUENCE     �   CREATE SEQUENCE public.aba_formulation_id_seq
    START WITH 68
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 -   DROP SEQUENCE public.aba_formulation_id_seq;
       public       postgres    false    3            �            1259    110410    aba_formulation    TABLE     �   CREATE TABLE public.aba_formulation (
    id integer DEFAULT nextval('public.aba_formulation_id_seq'::regclass) NOT NULL,
    code character varying(100),
    name character varying(100)
);
 #   DROP TABLE public.aba_formulation;
       public         postgres    false    209    3            �           0    0    TABLE aba_formulation    COMMENT     g   COMMENT ON TABLE public.aba_formulation IS 'Almacena los datos del alimento balanceado para animales';
            public       postgres    false    210            �           0    0    COLUMN aba_formulation.id    COMMENT     [   COMMENT ON COLUMN public.aba_formulation.id IS 'Id del alimento balanceado para animales';
            public       postgres    false    210            �           0    0    COLUMN aba_formulation.code    COMMENT     a   COMMENT ON COLUMN public.aba_formulation.code IS 'Codigo del alimento balanceado para animales';
            public       postgres    false    210            �           0    0    COLUMN aba_formulation.name    COMMENT     a   COMMENT ON COLUMN public.aba_formulation.name IS 'Nombre del alimento balanceado para animales';
            public       postgres    false    210            �            1259    110414    aba_results_id_seq    SEQUENCE     �   CREATE SEQUENCE public.aba_results_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 )   DROP SEQUENCE public.aba_results_id_seq;
       public       postgres    false    3            �            1259    110416    aba_results    TABLE     �   CREATE TABLE public.aba_results (
    id integer DEFAULT nextval('public.aba_results_id_seq'::regclass) NOT NULL,
    id_aba_element integer,
    quantity double precision
);
    DROP TABLE public.aba_results;
       public         postgres    false    211    3            �            1259    110420 &   aba_stages_of_breeds_and_stages_id_seq    SEQUENCE     �   CREATE SEQUENCE public.aba_stages_of_breeds_and_stages_id_seq
    START WITH 24
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 =   DROP SEQUENCE public.aba_stages_of_breeds_and_stages_id_seq;
       public       postgres    false    3            �            1259    110422    aba_stages_of_breeds_and_stages    TABLE       CREATE TABLE public.aba_stages_of_breeds_and_stages (
    id integer DEFAULT nextval('public.aba_stages_of_breeds_and_stages_id_seq'::regclass) NOT NULL,
    id_aba_breeds_and_stages integer,
    id_formulation integer,
    name character varying(100),
    duration integer
);
 3   DROP TABLE public.aba_stages_of_breeds_and_stages;
       public         postgres    false    213    3            �           0    0 %   TABLE aba_stages_of_breeds_and_stages    COMMENT     �   COMMENT ON TABLE public.aba_stages_of_breeds_and_stages IS 'Almacena las fases asociadas a los animales considerados en la tabla de consumo y mortalidad y asocia el alimento a ser proporcionado en dicha fase';
            public       postgres    false    214            �           0    0 )   COLUMN aba_stages_of_breeds_and_stages.id    COMMENT     �   COMMENT ON COLUMN public.aba_stages_of_breeds_and_stages.id IS 'Id de la fase asociadas a los animales considerados en la tabla de consumo y mortalidad ';
            public       postgres    false    214            �           0    0 ?   COLUMN aba_stages_of_breeds_and_stages.id_aba_breeds_and_stages    COMMENT     �   COMMENT ON COLUMN public.aba_stages_of_breeds_and_stages.id_aba_breeds_and_stages IS 'Id de la tabla que almacena la relacion entre proceso y consumo y mortalidad';
            public       postgres    false    214            �           0    0 5   COLUMN aba_stages_of_breeds_and_stages.id_formulation    COMMENT     �   COMMENT ON COLUMN public.aba_stages_of_breeds_and_stages.id_formulation IS 'Id del alimento balanceado para animales asociado a la fase';
            public       postgres    false    214            �           0    0 +   COLUMN aba_stages_of_breeds_and_stages.name    COMMENT     �   COMMENT ON COLUMN public.aba_stages_of_breeds_and_stages.name IS 'Nombre de la fase asociadas a los animales considerados en la tabla de consumo y mortalidad ';
            public       postgres    false    214            �           0    0 /   COLUMN aba_stages_of_breeds_and_stages.duration    COMMENT     �   COMMENT ON COLUMN public.aba_stages_of_breeds_and_stages.duration IS 'Duracion de la fase asociadas a los animales considerados en la tabla de consumo y mortalidad ';
            public       postgres    false    214            �            1259    110426    aba_time_unit    TABLE     �   CREATE TABLE public.aba_time_unit (
    id integer DEFAULT nextval('public."abaTimeUnit_id_seq"'::regclass) NOT NULL,
    singular_name character varying(100),
    plural_name character varying(100)
);
 !   DROP TABLE public.aba_time_unit;
       public         postgres    false    196    3            �           0    0    TABLE aba_time_unit    COMMENT     L   COMMENT ON TABLE public.aba_time_unit IS 'Almacena las unidades de tiempo';
            public       postgres    false    215            �           0    0    COLUMN aba_time_unit.id    COMMENT     K   COMMENT ON COLUMN public.aba_time_unit.id IS 'Id de la unidad de tiempo
';
            public       postgres    false    215            �           0    0 "   COLUMN aba_time_unit.singular_name    COMMENT     e   COMMENT ON COLUMN public.aba_time_unit.singular_name IS 'Nombre en singular de la unidad de tiempo';
            public       postgres    false    215            �           0    0     COLUMN aba_time_unit.plural_name    COMMENT     a   COMMENT ON COLUMN public.aba_time_unit.plural_name IS 'Nombre en plural de la unidad de tiempo';
            public       postgres    false    215            �            1259    110430    availability_shed_id_seq    SEQUENCE     �   CREATE SEQUENCE public.availability_shed_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.availability_shed_id_seq;
       public       postgres    false    3            �            1259    110432    base_day_id_seq    SEQUENCE     x   CREATE SEQUENCE public.base_day_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.base_day_id_seq;
       public       postgres    false    3            �            1259    110434    breed_id_seq    SEQUENCE     u   CREATE SEQUENCE public.breed_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.breed_id_seq;
       public       postgres    false    3            �            1259    110436    broiler_detail_id_seq    SEQUENCE     ~   CREATE SEQUENCE public.broiler_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.broiler_detail_id_seq;
       public       postgres    false    3            �            1259    110438    broiler_id_seq    SEQUENCE     w   CREATE SEQUENCE public.broiler_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.broiler_id_seq;
       public       postgres    false    3            �            1259    110440    broiler_product_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.broiler_product_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public.broiler_product_detail_id_seq;
       public       postgres    false    3            �            1259    110442    broiler_product_id_seq    SEQUENCE        CREATE SEQUENCE public.broiler_product_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.broiler_product_id_seq;
       public       postgres    false    3            �            1259    110444    broilereviction_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.broilereviction_detail_id_seq
    START WITH 124
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public.broilereviction_detail_id_seq;
       public       postgres    false    3            �            1259    110446    broilereviction_id_seq    SEQUENCE     �   CREATE SEQUENCE public.broilereviction_id_seq
    START WITH 70
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.broilereviction_id_seq;
       public       postgres    false    3            �            1259    110448    brooder_id_seq    SEQUENCE     w   CREATE SEQUENCE public.brooder_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.brooder_id_seq;
       public       postgres    false    3            �            1259    110450    brooder_machines_id_seq    SEQUENCE     �   CREATE SEQUENCE public.brooder_machines_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.brooder_machines_id_seq;
       public       postgres    false    3            �            1259    110452    calendar_day_id_seq    SEQUENCE     |   CREATE SEQUENCE public.calendar_day_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.calendar_day_id_seq;
       public       postgres    false    3            �            1259    110454    calendar_id_seq    SEQUENCE     x   CREATE SEQUENCE public.calendar_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.calendar_id_seq;
       public       postgres    false    3            �            1259    110456    center_id_seq    SEQUENCE     v   CREATE SEQUENCE public.center_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.center_id_seq;
       public       postgres    false    3            �            1259    110458    egg_planning_id_seq    SEQUENCE     |   CREATE SEQUENCE public.egg_planning_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.egg_planning_id_seq;
       public       postgres    false    3            �            1259    110460    egg_required_id_seq    SEQUENCE     |   CREATE SEQUENCE public.egg_required_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.egg_required_id_seq;
       public       postgres    false    3            �            1259    110462    eggs_storage_id_seq    SEQUENCE     |   CREATE SEQUENCE public.eggs_storage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.eggs_storage_id_seq;
       public       postgres    false    3            �            1259    110464    farm_id_seq    SEQUENCE     t   CREATE SEQUENCE public.farm_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE public.farm_id_seq;
       public       postgres    false    3            �            1259    110466    farm_type_id_seq    SEQUENCE     y   CREATE SEQUENCE public.farm_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.farm_type_id_seq;
       public       postgres    false    3            �            1259    110468    holiday_id_seq    SEQUENCE     w   CREATE SEQUENCE public.holiday_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.holiday_id_seq;
       public       postgres    false    3            �            1259    110470    housing_way_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.housing_way_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.housing_way_detail_id_seq;
       public       postgres    false    3            �            1259    110472    housing_way_id_seq    SEQUENCE     {   CREATE SEQUENCE public.housing_way_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.housing_way_id_seq;
       public       postgres    false    3            �            1259    110474    incubator_id_seq    SEQUENCE     y   CREATE SEQUENCE public.incubator_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.incubator_id_seq;
       public       postgres    false    3            �            1259    110476    incubator_plant_id_seq    SEQUENCE        CREATE SEQUENCE public.incubator_plant_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.incubator_plant_id_seq;
       public       postgres    false    3            �            1259    110478    industry_id_seq    SEQUENCE     x   CREATE SEQUENCE public.industry_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.industry_id_seq;
       public       postgres    false    3            �            1259    110480    line_id_seq    SEQUENCE     t   CREATE SEQUENCE public.line_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE public.line_id_seq;
       public       postgres    false    3            �            1259    110482    lot_eggs_id_seq    SEQUENCE     x   CREATE SEQUENCE public.lot_eggs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.lot_eggs_id_seq;
       public       postgres    false    3            �            1259    110484    lot_fattening_id_seq    SEQUENCE     }   CREATE SEQUENCE public.lot_fattening_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.lot_fattening_id_seq;
       public       postgres    false    3            �            1259    110486 
   lot_id_seq    SEQUENCE     s   CREATE SEQUENCE public.lot_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 !   DROP SEQUENCE public.lot_id_seq;
       public       postgres    false    3            �            1259    110488    lot_liftbreeding_id_seq    SEQUENCE     �   CREATE SEQUENCE public.lot_liftbreeding_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.lot_liftbreeding_id_seq;
       public       postgres    false    3            �            1259    110490     mdapplication_application_id_seq    SEQUENCE     �   CREATE SEQUENCE public.mdapplication_application_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999999999999
    CACHE 1;
 7   DROP SEQUENCE public.mdapplication_application_id_seq;
       public       postgres    false    3            �            1259    110492    mdapplication    TABLE     �   CREATE TABLE public.mdapplication (
    application_id integer DEFAULT nextval('public.mdapplication_application_id_seq'::regclass) NOT NULL,
    application_name character varying(30) NOT NULL,
    type character varying(20)
);
 !   DROP TABLE public.mdapplication;
       public         postgres    false    246    3            �           0    0    TABLE mdapplication    COMMENT     X   COMMENT ON TABLE public.mdapplication IS 'Almacena la informacion de las aplicaciones';
            public       postgres    false    247            �           0    0 #   COLUMN mdapplication.application_id    COMMENT     P   COMMENT ON COLUMN public.mdapplication.application_id IS 'Id de la aplicació';
            public       postgres    false    247            �           0    0 %   COLUMN mdapplication.application_name    COMMENT     W   COMMENT ON COLUMN public.mdapplication.application_name IS 'Nombre de la aplicación';
            public       postgres    false    247            �           0    0    COLUMN mdapplication.type    COMMENT     W   COMMENT ON COLUMN public.mdapplication.type IS 'A qué tipo pertenece la aplicación';
            public       postgres    false    247            �            1259    110496    mdapplication_rol_id_seq    SEQUENCE     �   CREATE SEQUENCE public.mdapplication_rol_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9999999999999
    CACHE 1;
 /   DROP SEQUENCE public.mdapplication_rol_id_seq;
       public       postgres    false    3            �            1259    110498    mdapplication_rol    TABLE     �   CREATE TABLE public.mdapplication_rol (
    id integer DEFAULT nextval('public.mdapplication_rol_id_seq'::regclass) NOT NULL,
    application_id integer NOT NULL,
    rol_id integer NOT NULL
);
 %   DROP TABLE public.mdapplication_rol;
       public         postgres    false    248    3            �           0    0    TABLE mdapplication_rol    COMMENT     _   COMMENT ON TABLE public.mdapplication_rol IS 'Contiene los id de aplicación y los id de rol';
            public       postgres    false    249            �           0    0    COLUMN mdapplication_rol.id    COMMENT     \   COMMENT ON COLUMN public.mdapplication_rol.id IS 'Id la combinacion de aplicación y rol ';
            public       postgres    false    249            �           0    0 '   COLUMN mdapplication_rol.application_id    COMMENT     `   COMMENT ON COLUMN public.mdapplication_rol.application_id IS 'Identificador de la aplicación';
            public       postgres    false    249            �           0    0    COLUMN mdapplication_rol.rol_id    COMMENT     N   COMMENT ON COLUMN public.mdapplication_rol.rol_id IS 'Identificador del rol';
            public       postgres    false    249            �            1259    110502    mdbreed    TABLE     �   CREATE TABLE public.mdbreed (
    breed_id integer DEFAULT nextval('public.breed_id_seq'::regclass) NOT NULL,
    code character varying(20) NOT NULL,
    name character varying(45) NOT NULL
);
    DROP TABLE public.mdbreed;
       public         postgres    false    218    3            �           0    0    TABLE mdbreed    COMMENT     U   COMMENT ON TABLE public.mdbreed IS 'Tabla donde se almacenan las razas de las aves';
            public       postgres    false    250            �           0    0    COLUMN mdbreed.breed_id    COMMENT     >   COMMENT ON COLUMN public.mdbreed.breed_id IS 'Id de la raza';
            public       postgres    false    250            �           0    0    COLUMN mdbreed.code    COMMENT     >   COMMENT ON COLUMN public.mdbreed.code IS 'Codigo de la raza';
            public       postgres    false    250            �           0    0    COLUMN mdbreed.name    COMMENT     >   COMMENT ON COLUMN public.mdbreed.name IS 'Nombre de la Raza';
            public       postgres    false    250            �            1259    110506    mdbroiler_product    TABLE     �   CREATE TABLE public.mdbroiler_product (
    broiler_product_id integer DEFAULT nextval('public.broiler_product_id_seq'::regclass) NOT NULL,
    name character varying(45) NOT NULL,
    days_eviction integer,
    weight double precision
);
 %   DROP TABLE public.mdbroiler_product;
       public         postgres    false    222    3            �           0    0    TABLE mdbroiler_product    COMMENT     w   COMMENT ON TABLE public.mdbroiler_product IS 'Almacena los productos de salida de la etapa de engorda hacia desalojo';
            public       postgres    false    251            �           0    0 +   COLUMN mdbroiler_product.broiler_product_id    COMMENT     ^   COMMENT ON COLUMN public.mdbroiler_product.broiler_product_id IS 'Id de producto de engorde';
            public       postgres    false    251            �           0    0    COLUMN mdbroiler_product.name    COMMENT     T   COMMENT ON COLUMN public.mdbroiler_product.name IS 'Nombre de producto de engorde';
            public       postgres    false    251            �           0    0 &   COLUMN mdbroiler_product.days_eviction    COMMENT     y   COMMENT ON COLUMN public.mdbroiler_product.days_eviction IS 'Días necesarios para el desalojo del producto de engorde';
            public       postgres    false    251            �           0    0    COLUMN mdbroiler_product.weight    COMMENT     b   COMMENT ON COLUMN public.mdbroiler_product.weight IS 'Peso estimado del producto para su salida';
            public       postgres    false    251            �            1259    110510 
   mdfarmtype    TABLE     �   CREATE TABLE public.mdfarmtype (
    farm_type_id integer DEFAULT nextval('public.farm_type_id_seq'::regclass) NOT NULL,
    name character varying(45) NOT NULL
);
    DROP TABLE public.mdfarmtype;
       public         postgres    false    234    3            �           0    0    TABLE mdfarmtype    COMMENT     D   COMMENT ON TABLE public.mdfarmtype IS 'Define los tipos de granja';
            public       postgres    false    252            �           0    0    COLUMN mdfarmtype.farm_type_id    COMMENT     L   COMMENT ON COLUMN public.mdfarmtype.farm_type_id IS 'Id de tipo de granja';
            public       postgres    false    252            �           0    0    COLUMN mdfarmtype.name    COMMENT     O   COMMENT ON COLUMN public.mdfarmtype.name IS 'Nombre de la etapa de la granja';
            public       postgres    false    252            �            1259    110514    measure_id_seq    SEQUENCE     w   CREATE SEQUENCE public.measure_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.measure_id_seq;
       public       postgres    false    3            �            1259    110516 	   mdmeasure    TABLE     $  CREATE TABLE public.mdmeasure (
    measure_id integer DEFAULT nextval('public.measure_id_seq'::regclass) NOT NULL,
    name character varying(10) NOT NULL,
    abbreviation character varying(5) NOT NULL,
    originvalue double precision,
    valuekg double precision,
    is_unit boolean
);
    DROP TABLE public.mdmeasure;
       public         postgres    false    253    3            �           0    0    TABLE mdmeasure    COMMENT     _   COMMENT ON TABLE public.mdmeasure IS 'Almacena las medidas a utilizar en las planificaciones';
            public       postgres    false    254            �           0    0    COLUMN mdmeasure.measure_id    COMMENT     D   COMMENT ON COLUMN public.mdmeasure.measure_id IS 'Id de la medida';
            public       postgres    false    254            �           0    0    COLUMN mdmeasure.name    COMMENT     B   COMMENT ON COLUMN public.mdmeasure.name IS 'Nombre de la medida';
            public       postgres    false    254            �           0    0    COLUMN mdmeasure.abbreviation    COMMENT     O   COMMENT ON COLUMN public.mdmeasure.abbreviation IS 'Abreviatura de la medida';
            public       postgres    false    254            �           0    0    COLUMN mdmeasure.originvalue    COMMENT     Q   COMMENT ON COLUMN public.mdmeasure.originvalue IS 'Valor original de la medida';
            public       postgres    false    254            �           0    0    COLUMN mdmeasure.valuekg    COMMENT     R   COMMENT ON COLUMN public.mdmeasure.valuekg IS 'Valor en Kilogramos de la medida';
            public       postgres    false    254            �            1259    110520    parameter_id_seq    SEQUENCE     y   CREATE SEQUENCE public.parameter_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.parameter_id_seq;
       public       postgres    false    3                        1259    110522    mdparameter    TABLE     '  CREATE TABLE public.mdparameter (
    parameter_id integer DEFAULT nextval('public.parameter_id_seq'::regclass) NOT NULL,
    description character varying(250) NOT NULL,
    type character varying(10),
    measure_id integer,
    process_id integer,
    name character varying(250) NOT NULL
);
    DROP TABLE public.mdparameter;
       public         postgres    false    255    3            �           0    0    TABLE mdparameter    COMMENT     �   COMMENT ON TABLE public.mdparameter IS 'Almacena la definición de los parámetros a utilizar en la planificación regresiva junto a sus respectivas características';
            public       postgres    false    256            �           0    0    COLUMN mdparameter.parameter_id    COMMENT     N   COMMENT ON COLUMN public.mdparameter.parameter_id IS 'Id de los parámetros';
            public       postgres    false    256            �           0    0    COLUMN mdparameter.description    COMMENT     W   COMMENT ON COLUMN public.mdparameter.description IS 'Descripción de los parámetros';
            public       postgres    false    256            �           0    0    COLUMN mdparameter.type    COMMENT     D   COMMENT ON COLUMN public.mdparameter.type IS 'Tipo de parámetros';
            public       postgres    false    256            �           0    0    COLUMN mdparameter.measure_id    COMMENT     F   COMMENT ON COLUMN public.mdparameter.measure_id IS 'Id de la medida';
            public       postgres    false    256            �           0    0    COLUMN mdparameter.process_id    COMMENT     E   COMMENT ON COLUMN public.mdparameter.process_id IS 'Id del proceso';
            public       postgres    false    256            �           0    0    COLUMN mdparameter.name    COMMENT     F   COMMENT ON COLUMN public.mdparameter.name IS 'Nombre del parámetro';
            public       postgres    false    256                       1259    110529    process_id_seq    SEQUENCE     w   CREATE SEQUENCE public.process_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.process_id_seq;
       public       postgres    false    3                       1259    110531 	   mdprocess    TABLE     B  CREATE TABLE public.mdprocess (
    process_id integer DEFAULT nextval('public.process_id_seq'::regclass) NOT NULL,
    process_order integer NOT NULL,
    product_id integer NOT NULL,
    stage_id integer NOT NULL,
    historical_decrease double precision NOT NULL,
    theoretical_decrease double precision NOT NULL,
    historical_weight double precision NOT NULL,
    theoretical_weight double precision NOT NULL,
    historical_duration integer NOT NULL,
    theoretical_duration integer NOT NULL,
    calendar_id integer NOT NULL,
    visible boolean,
    name character varying(250) NOT NULL,
    predecessor_id integer,
    capacity integer NOT NULL,
    breed_id integer NOT NULL,
    gender character varying(30),
    fattening_goal double precision,
    type_posture character varying(30),
    biological_active boolean
);
    DROP TABLE public.mdprocess;
       public         postgres    false    257    3            �           0    0    TABLE mdprocess    COMMENT     �   COMMENT ON TABLE public.mdprocess IS 'Almacena los procesos definidos para la planificación progresiva junto a sus respectivas características';
            public       postgres    false    258            �           0    0    COLUMN mdprocess.process_id    COMMENT     G   COMMENT ON COLUMN public.mdprocess.process_id IS 'Id de los procesos';
            public       postgres    false    258            �           0    0    COLUMN mdprocess.process_order    COMMENT     M   COMMENT ON COLUMN public.mdprocess.process_order IS 'Orden de los procesos';
            public       postgres    false    258            �           0    0    COLUMN mdprocess.product_id    COMMENT     D   COMMENT ON COLUMN public.mdprocess.product_id IS 'Id del producto';
            public       postgres    false    258            �           0    0    COLUMN mdprocess.stage_id    COMMENT     >   COMMENT ON COLUMN public.mdprocess.stage_id IS 'Id de etapa';
            public       postgres    false    258            �           0    0 $   COLUMN mdprocess.historical_decrease    COMMENT     Y   COMMENT ON COLUMN public.mdprocess.historical_decrease IS 'Merma historica del proceso';
            public       postgres    false    258            �           0    0 %   COLUMN mdprocess.theoretical_decrease    COMMENT     Y   COMMENT ON COLUMN public.mdprocess.theoretical_decrease IS 'Merma teórica del proceso';
            public       postgres    false    258            �           0    0 "   COLUMN mdprocess.historical_weight    COMMENT     V   COMMENT ON COLUMN public.mdprocess.historical_weight IS 'Peso historico del proceso';
            public       postgres    false    258            �           0    0 #   COLUMN mdprocess.theoretical_weight    COMMENT     V   COMMENT ON COLUMN public.mdprocess.theoretical_weight IS 'Peso teórico del proceso';
            public       postgres    false    258            �           0    0 $   COLUMN mdprocess.historical_duration    COMMENT     ^   COMMENT ON COLUMN public.mdprocess.historical_duration IS 'Duración histórica del proceso';
            public       postgres    false    258            �           0    0 %   COLUMN mdprocess.theoretical_duration    COMMENT     ]   COMMENT ON COLUMN public.mdprocess.theoretical_duration IS 'Duración teórica del proceso';
            public       postgres    false    258            �           0    0    COLUMN mdprocess.calendar_id    COMMENT     G   COMMENT ON COLUMN public.mdprocess.calendar_id IS 'Id del calendario';
            public       postgres    false    258            �           0    0    COLUMN mdprocess.visible    COMMENT     I   COMMENT ON COLUMN public.mdprocess.visible IS 'Visibilidad del proceso';
            public       postgres    false    258            �           0    0    COLUMN mdprocess.name    COMMENT     A   COMMENT ON COLUMN public.mdprocess.name IS 'Nombre del proceso';
            public       postgres    false    258            �           0    0    COLUMN mdprocess.predecessor_id    COMMENT     J   COMMENT ON COLUMN public.mdprocess.predecessor_id IS 'Id del predecesor';
            public       postgres    false    258            �           0    0    COLUMN mdprocess.capacity    COMMENT     X   COMMENT ON COLUMN public.mdprocess.capacity IS 'Capacidad semanal asociada al proceso';
            public       postgres    false    258            �           0    0    COLUMN mdprocess.breed_id    COMMENT     @   COMMENT ON COLUMN public.mdprocess.breed_id IS 'Id de la raza';
            public       postgres    false    258            �           0    0    COLUMN mdprocess.gender    COMMENT     N   COMMENT ON COLUMN public.mdprocess.gender IS 'Genero del producto de salida';
            public       postgres    false    258            �           0    0    COLUMN mdprocess.fattening_goal    COMMENT     H   COMMENT ON COLUMN public.mdprocess.fattening_goal IS 'Meta de engorde';
            public       postgres    false    258            �           0    0    COLUMN mdprocess.type_posture    COMMENT     s   COMMENT ON COLUMN public.mdprocess.type_posture IS 'Define el tipo de postura de acuerdo a la edad de la gallina';
            public       postgres    false    258            �           0    0 "   COLUMN mdprocess.biological_active    COMMENT     h   COMMENT ON COLUMN public.mdprocess.biological_active IS 'Define si el proceso es un activo biológico';
            public       postgres    false    258                       1259    110535    product_id_seq    SEQUENCE     w   CREATE SEQUENCE public.product_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.product_id_seq;
       public       postgres    false    3                       1259    110537 	   mdproduct    TABLE     �   CREATE TABLE public.mdproduct (
    product_id integer DEFAULT nextval('public.product_id_seq'::regclass) NOT NULL,
    code character varying(20) NOT NULL,
    name character varying(45) NOT NULL
);
    DROP TABLE public.mdproduct;
       public         postgres    false    259    3            �           0    0    TABLE mdproduct    COMMENT     Z   COMMENT ON TABLE public.mdproduct IS 'Almacena los productos utilizados en los procesos';
            public       postgres    false    260            �           0    0    COLUMN mdproduct.product_id    COMMENT     D   COMMENT ON COLUMN public.mdproduct.product_id IS 'Id del producto';
            public       postgres    false    260            �           0    0    COLUMN mdproduct.code    COMMENT     B   COMMENT ON COLUMN public.mdproduct.code IS 'Codigo del producto';
            public       postgres    false    260            �           0    0    COLUMN mdproduct.name    COMMENT     B   COMMENT ON COLUMN public.mdproduct.name IS 'Nombre del producto';
            public       postgres    false    260                       1259    110541    mdrol_rol_id_seq    SEQUENCE        CREATE SEQUENCE public.mdrol_rol_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 10000000
    CACHE 1;
 '   DROP SEQUENCE public.mdrol_rol_id_seq;
       public       postgres    false    3                       1259    110543    mdrol    TABLE     	  CREATE TABLE public.mdrol (
    rol_id integer DEFAULT nextval('public.mdrol_rol_id_seq'::regclass) NOT NULL,
    rol_name character varying(80) NOT NULL,
    admin_user_creator character varying(80) NOT NULL,
    creation_date timestamp with time zone NOT NULL
);
    DROP TABLE public.mdrol;
       public         postgres    false    261    3            �           0    0    TABLE mdrol    COMMENT     O   COMMENT ON TABLE public.mdrol IS 'Almacena los datos de los diferentes roles';
            public       postgres    false    262            �           0    0    COLUMN mdrol.rol_id    COMMENT     7   COMMENT ON COLUMN public.mdrol.rol_id IS 'Id del rol';
            public       postgres    false    262            �           0    0    COLUMN mdrol.rol_name    COMMENT     =   COMMENT ON COLUMN public.mdrol.rol_name IS 'Nombre del rol';
            public       postgres    false    262            �           0    0    COLUMN mdrol.admin_user_creator    COMMENT     [   COMMENT ON COLUMN public.mdrol.admin_user_creator IS 'Especifica que usuario creo el rol';
            public       postgres    false    262                        0    0    COLUMN mdrol.creation_date    COMMENT     N   COMMENT ON COLUMN public.mdrol.creation_date IS 'Fecha de creación del rol';
            public       postgres    false    262                       1259    110547    scenario_id_seq    SEQUENCE     x   CREATE SEQUENCE public.scenario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.scenario_id_seq;
       public       postgres    false    3                       1259    110549 
   mdscenario    TABLE     d  CREATE TABLE public.mdscenario (
    scenario_id integer DEFAULT nextval('public.scenario_id_seq'::regclass) NOT NULL,
    description character varying(250) NOT NULL,
    date_start timestamp with time zone,
    date_end timestamp with time zone,
    name character varying(250) NOT NULL,
    status integer DEFAULT 0,
    calendar_id integer NOT NULL
);
    DROP TABLE public.mdscenario;
       public         postgres    false    263    3                       0    0    TABLE mdscenario    COMMENT     [   COMMENT ON TABLE public.mdscenario IS 'Almacena información de los distintos escenarios';
            public       postgres    false    264                       0    0    COLUMN mdscenario.scenario_id    COMMENT     G   COMMENT ON COLUMN public.mdscenario.scenario_id IS 'Id del escenario';
            public       postgres    false    264                       0    0    COLUMN mdscenario.description    COMMENT     P   COMMENT ON COLUMN public.mdscenario.description IS 'Descripcion del escenario';
            public       postgres    false    264                       0    0    COLUMN mdscenario.date_start    COMMENT     S   COMMENT ON COLUMN public.mdscenario.date_start IS 'Fecha de inicio del escenario';
            public       postgres    false    264                       0    0    COLUMN mdscenario.date_end    COMMENT     N   COMMENT ON COLUMN public.mdscenario.date_end IS 'Fecha de fin del escenario';
            public       postgres    false    264                       0    0    COLUMN mdscenario.name    COMMENT     D   COMMENT ON COLUMN public.mdscenario.name IS 'Nombre del escenario';
            public       postgres    false    264                       0    0    COLUMN mdscenario.status    COMMENT     F   COMMENT ON COLUMN public.mdscenario.status IS 'Estado del escenario';
            public       postgres    false    264            	           1259    110557    status_shed_id_seq    SEQUENCE     {   CREATE SEQUENCE public.status_shed_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.status_shed_id_seq;
       public       postgres    false    3            
           1259    110559    mdshedstatus    TABLE     �   CREATE TABLE public.mdshedstatus (
    shed_status_id integer DEFAULT nextval('public.status_shed_id_seq'::regclass) NOT NULL,
    name character varying(50) NOT NULL,
    description character varying(250) NOT NULL
);
     DROP TABLE public.mdshedstatus;
       public         postgres    false    265    3                       0    0    TABLE mdshedstatus    COMMENT     b   COMMENT ON TABLE public.mdshedstatus IS 'Almaceno los estatus de disponibilidad de los galpones';
            public       postgres    false    266            	           0    0 "   COLUMN mdshedstatus.shed_status_id    COMMENT     T   COMMENT ON COLUMN public.mdshedstatus.shed_status_id IS 'Id del estado del galpon';
            public       postgres    false    266            
           0    0    COLUMN mdshedstatus.name    COMMENT     a   COMMENT ON COLUMN public.mdshedstatus.name IS 'Nombre del estado en que se encuentra el galpon';
            public       postgres    false    266                       0    0    COLUMN mdshedstatus.description    COMMENT     [   COMMENT ON COLUMN public.mdshedstatus.description IS 'Descripcion del estado del galpon
';
            public       postgres    false    266                       1259    110563    stage_id_seq    SEQUENCE     u   CREATE SEQUENCE public.stage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.stage_id_seq;
       public       postgres    false    3                       1259    110565    mdstage    TABLE     �   CREATE TABLE public.mdstage (
    stage_id integer DEFAULT nextval('public.stage_id_seq'::regclass) NOT NULL,
    order_ integer,
    name character varying(250) NOT NULL
);
    DROP TABLE public.mdstage;
       public         postgres    false    267    3                       0    0    TABLE mdstage    COMMENT     d   COMMENT ON TABLE public.mdstage IS 'Almacena las etapas a utilizar en el proceso de planificacion';
            public       postgres    false    268                       0    0    COLUMN mdstage.stage_id    COMMENT     ?   COMMENT ON COLUMN public.mdstage.stage_id IS 'Id de la etapa';
            public       postgres    false    268                       0    0    COLUMN mdstage.order_    COMMENT     U   COMMENT ON COLUMN public.mdstage.order_ IS 'Orden en el que se muestras las etapas';
            public       postgres    false    268                       0    0    COLUMN mdstage.name    COMMENT     ?   COMMENT ON COLUMN public.mdstage.name IS 'Nombre de la etapa';
            public       postgres    false    268                       1259    110569    mduser_user_id_seq    SEQUENCE     �   CREATE SEQUENCE public.mduser_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9999999999
    CACHE 1;
 )   DROP SEQUENCE public.mduser_user_id_seq;
       public       postgres    false    3                       1259    110571    mduser    TABLE     �  CREATE TABLE public.mduser (
    user_id integer DEFAULT nextval('public.mduser_user_id_seq'::regclass) NOT NULL,
    username character varying(80) NOT NULL,
    password character varying(80) NOT NULL,
    name character varying(80) NOT NULL,
    lastname character varying(80) NOT NULL,
    active boolean NOT NULL,
    admi_user_creator character varying(80) NOT NULL,
    rol_id integer NOT NULL,
    creation_date timestamp with time zone NOT NULL
);
    DROP TABLE public.mduser;
       public         postgres    false    269    3                       1259    110575    oscenter    TABLE       CREATE TABLE public.oscenter (
    center_id integer DEFAULT nextval('public.center_id_seq'::regclass) NOT NULL,
    partnership_id integer NOT NULL,
    farm_id integer NOT NULL,
    name character varying(45) NOT NULL,
    code character varying(20) NOT NULL
);
    DROP TABLE public.oscenter;
       public         postgres    false    229    3                       0    0    TABLE oscenter    COMMENT     S   COMMENT ON TABLE public.oscenter IS 'Almacena los datos referentes a los nucleos';
            public       postgres    false    271                       0    0    COLUMN oscenter.center_id    COMMENT     @   COMMENT ON COLUMN public.oscenter.center_id IS 'Id del nucleo';
            public       postgres    false    271                       0    0    COLUMN oscenter.partnership_id    COMMENT     H   COMMENT ON COLUMN public.oscenter.partnership_id IS 'Id de la empresa';
            public       postgres    false    271                       0    0    COLUMN oscenter.farm_id    COMMENT     @   COMMENT ON COLUMN public.oscenter.farm_id IS 'Id de la granja';
            public       postgres    false    271                       0    0    COLUMN oscenter.name    COMMENT     @   COMMENT ON COLUMN public.oscenter.name IS 'Nombre del nucleo
';
            public       postgres    false    271                       0    0    COLUMN oscenter.code    COMMENT     ?   COMMENT ON COLUMN public.oscenter.code IS 'Codigo del nucleo';
            public       postgres    false    271                       1259    110579    oscenter_oswarehouse    TABLE     �   CREATE TABLE public.oscenter_oswarehouse (
    client_id integer NOT NULL,
    partnership_id integer NOT NULL,
    farm_id integer NOT NULL,
    center_id integer NOT NULL,
    warehouse_id integer NOT NULL,
    delete_mark integer
);
 (   DROP TABLE public.oscenter_oswarehouse;
       public         postgres    false    3                       0    0    TABLE oscenter_oswarehouse    COMMENT     p   COMMENT ON TABLE public.oscenter_oswarehouse IS 'Relación que une los núcleos con sus respectivos almacenes';
            public       postgres    false    272                       0    0 %   COLUMN oscenter_oswarehouse.client_id    COMMENT     M   COMMENT ON COLUMN public.oscenter_oswarehouse.client_id IS 'Id del cliente';
            public       postgres    false    272                       0    0 *   COLUMN oscenter_oswarehouse.partnership_id    COMMENT     T   COMMENT ON COLUMN public.oscenter_oswarehouse.partnership_id IS 'Id de la empresa';
            public       postgres    false    272                       0    0 #   COLUMN oscenter_oswarehouse.farm_id    COMMENT     L   COMMENT ON COLUMN public.oscenter_oswarehouse.farm_id IS 'Id de la granja';
            public       postgres    false    272                       0    0 %   COLUMN oscenter_oswarehouse.center_id    COMMENT     L   COMMENT ON COLUMN public.oscenter_oswarehouse.center_id IS 'Id del nucleo';
            public       postgres    false    272                       0    0 (   COLUMN oscenter_oswarehouse.warehouse_id    COMMENT     P   COMMENT ON COLUMN public.oscenter_oswarehouse.warehouse_id IS 'Id del almacen';
            public       postgres    false    272                       0    0 '   COLUMN oscenter_oswarehouse.delete_mark    COMMENT     Q   COMMENT ON COLUMN public.oscenter_oswarehouse.delete_mark IS 'Marca de borrado';
            public       postgres    false    272                       1259    110582    osfarm    TABLE     �   CREATE TABLE public.osfarm (
    farm_id integer DEFAULT nextval('public.farm_id_seq'::regclass) NOT NULL,
    partnership_id integer,
    code character varying(20) NOT NULL,
    name character varying(45) NOT NULL,
    farm_type_id integer NOT NULL
);
    DROP TABLE public.osfarm;
       public         postgres    false    233    3                       0    0    TABLE osfarm    COMMENT     p   COMMENT ON TABLE public.osfarm IS 'Almacena la información de la granja con sus respectivas características';
            public       postgres    false    273                       0    0    COLUMN osfarm.farm_id    COMMENT     >   COMMENT ON COLUMN public.osfarm.farm_id IS 'Id de la granja';
            public       postgres    false    273                       0    0    COLUMN osfarm.partnership_id    COMMENT     F   COMMENT ON COLUMN public.osfarm.partnership_id IS 'Id de la empresa';
            public       postgres    false    273                        0    0    COLUMN osfarm.code    COMMENT     ?   COMMENT ON COLUMN public.osfarm.code IS 'Codigo de la granja';
            public       postgres    false    273            !           0    0    COLUMN osfarm.name    COMMENT     ?   COMMENT ON COLUMN public.osfarm.name IS 'Nombre de la granja';
            public       postgres    false    273            "           0    0    COLUMN osfarm.farm_type_id    COMMENT     I   COMMENT ON COLUMN public.osfarm.farm_type_id IS 'Id del tipo de granja';
            public       postgres    false    273                       1259    110586    osincubator    TABLE     �  CREATE TABLE public.osincubator (
    incubator_id integer DEFAULT nextval('public.incubator_id_seq'::regclass) NOT NULL,
    incubator_plant_id integer,
    name character varying(45) NOT NULL,
    code character varying(20) NOT NULL,
    description character varying(250) NOT NULL,
    capacity integer,
    sunday integer,
    monday integer,
    tuesday integer,
    wednesday integer,
    thursday integer,
    friday integer,
    saturday integer,
    available integer
);
    DROP TABLE public.osincubator;
       public         postgres    false    238    3            #           0    0    TABLE osincubator    COMMENT     y   COMMENT ON TABLE public.osincubator IS 'Almacena las máquinas de incubación pertenecientes a cada una de las plantas';
            public       postgres    false    274            $           0    0    COLUMN osincubator.incubator_id    COMMENT     L   COMMENT ON COLUMN public.osincubator.incubator_id IS 'Id de la incubadora';
            public       postgres    false    274            %           0    0 %   COLUMN osincubator.incubator_plant_id    COMMENT     Y   COMMENT ON COLUMN public.osincubator.incubator_plant_id IS 'Id de la planta incubadora';
            public       postgres    false    274            &           0    0    COLUMN osincubator.name    COMMENT     H   COMMENT ON COLUMN public.osincubator.name IS 'Nombre de la incubadora';
            public       postgres    false    274            '           0    0    COLUMN osincubator.code    COMMENT     H   COMMENT ON COLUMN public.osincubator.code IS 'Codigo de la incubadora';
            public       postgres    false    274            (           0    0    COLUMN osincubator.description    COMMENT     T   COMMENT ON COLUMN public.osincubator.description IS 'Descripcion de la incubadora';
            public       postgres    false    274            )           0    0    COLUMN osincubator.capacity    COMMENT     O   COMMENT ON COLUMN public.osincubator.capacity IS 'Capacidad de la incubadora';
            public       postgres    false    274            *           0    0    COLUMN osincubator.sunday    COMMENT     ]   COMMENT ON COLUMN public.osincubator.sunday IS 'Marca los dias de trabajo de la incubadora';
            public       postgres    false    274            +           0    0    COLUMN osincubator.monday    COMMENT     ^   COMMENT ON COLUMN public.osincubator.monday IS 'Marca los días de trabajo de la incubadora';
            public       postgres    false    274            ,           0    0    COLUMN osincubator.tuesday    COMMENT     _   COMMENT ON COLUMN public.osincubator.tuesday IS 'Marca los días de trabajo de la incubadora';
            public       postgres    false    274            -           0    0    COLUMN osincubator.wednesday    COMMENT     a   COMMENT ON COLUMN public.osincubator.wednesday IS 'Marca los días de trabajo de la incubadora';
            public       postgres    false    274            .           0    0    COLUMN osincubator.thursday    COMMENT     `   COMMENT ON COLUMN public.osincubator.thursday IS 'Marca los días de trabajo de la incubadora';
            public       postgres    false    274            /           0    0    COLUMN osincubator.friday    COMMENT     ^   COMMENT ON COLUMN public.osincubator.friday IS 'Marca los días de trabajo de la incubadora';
            public       postgres    false    274            0           0    0    COLUMN osincubator.saturday    COMMENT     `   COMMENT ON COLUMN public.osincubator.saturday IS 'Marca los días de trabajo de la incubadora';
            public       postgres    false    274                       1259    110590    osincubatorplant    TABLE     U  CREATE TABLE public.osincubatorplant (
    incubator_plant_id integer DEFAULT nextval('public.incubator_plant_id_seq'::regclass) NOT NULL,
    name character varying(45) NOT NULL,
    code character varying(20) NOT NULL,
    description character varying(250),
    partnership_id integer,
    max_storage integer,
    min_storage integer
);
 $   DROP TABLE public.osincubatorplant;
       public         postgres    false    239    3            1           0    0    TABLE osincubatorplant    COMMENT     }   COMMENT ON TABLE public.osincubatorplant IS 'Almacena la información de la planta incubadora perteneciente a cada empresa';
            public       postgres    false    275            2           0    0 *   COLUMN osincubatorplant.incubator_plant_id    COMMENT     ^   COMMENT ON COLUMN public.osincubatorplant.incubator_plant_id IS 'Id de la planta incubadora';
            public       postgres    false    275            3           0    0    COLUMN osincubatorplant.name    COMMENT     T   COMMENT ON COLUMN public.osincubatorplant.name IS 'Nombre de la planta incubadora';
            public       postgres    false    275            4           0    0    COLUMN osincubatorplant.code    COMMENT     T   COMMENT ON COLUMN public.osincubatorplant.code IS 'Codigo de la planta incubadora';
            public       postgres    false    275            5           0    0 #   COLUMN osincubatorplant.description    COMMENT     a   COMMENT ON COLUMN public.osincubatorplant.description IS 'Descripción de la planta incubadora';
            public       postgres    false    275            6           0    0 &   COLUMN osincubatorplant.partnership_id    COMMENT     P   COMMENT ON COLUMN public.osincubatorplant.partnership_id IS 'Id de la empresa';
            public       postgres    false    275            7           0    0 #   COLUMN osincubatorplant.max_storage    COMMENT     ]   COMMENT ON COLUMN public.osincubatorplant.max_storage IS 'Numero máximo de almacenamiento';
            public       postgres    false    275            8           0    0 #   COLUMN osincubatorplant.min_storage    COMMENT     \   COMMENT ON COLUMN public.osincubatorplant.min_storage IS 'Numero minimo de almacenamiento';
            public       postgres    false    275                       1259    110594    partnership_id_seq    SEQUENCE     {   CREATE SEQUENCE public.partnership_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.partnership_id_seq;
       public       postgres    false    3                       1259    110596    ospartnership    TABLE     2  CREATE TABLE public.ospartnership (
    partnership_id integer DEFAULT nextval('public.partnership_id_seq'::regclass) NOT NULL,
    name character varying(45) NOT NULL,
    address character varying(250) NOT NULL,
    description character varying(250) NOT NULL,
    code character varying(20) NOT NULL
);
 !   DROP TABLE public.ospartnership;
       public         postgres    false    276    3            9           0    0    TABLE ospartnership    COMMENT     j   COMMENT ON TABLE public.ospartnership IS 'Almacena la información referente a las empresas registradas';
            public       postgres    false    277            :           0    0 #   COLUMN ospartnership.partnership_id    COMMENT     M   COMMENT ON COLUMN public.ospartnership.partnership_id IS 'Id de la empresa';
            public       postgres    false    277            ;           0    0    COLUMN ospartnership.name    COMMENT     G   COMMENT ON COLUMN public.ospartnership.name IS 'Nombre de la empresa';
            public       postgres    false    277            <           0    0    COLUMN ospartnership.address    COMMENT     M   COMMENT ON COLUMN public.ospartnership.address IS 'Direccion de la empresa';
            public       postgres    false    277            =           0    0     COLUMN ospartnership.description    COMMENT     T   COMMENT ON COLUMN public.ospartnership.description IS 'Descripción de la empresa';
            public       postgres    false    277            >           0    0    COLUMN ospartnership.code    COMMENT     G   COMMENT ON COLUMN public.ospartnership.code IS 'Codigo de la empresa';
            public       postgres    false    277                       1259    110603    shed_id_seq    SEQUENCE     t   CREATE SEQUENCE public.shed_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE public.shed_id_seq;
       public       postgres    false    3                       1259    110605    osshed    TABLE     �  CREATE TABLE public.osshed (
    shed_id integer DEFAULT nextval('public.shed_id_seq'::regclass) NOT NULL,
    partnership_id integer NOT NULL,
    farm_id integer NOT NULL,
    center_id integer NOT NULL,
    code character varying(20) NOT NULL,
    statusshed_id integer NOT NULL,
    type_id integer,
    building_date date,
    stall_width double precision NOT NULL,
    stall_height double precision NOT NULL,
    capacity_min double precision NOT NULL,
    capacity_max double precision NOT NULL,
    environment_id integer,
    rotation_days integer DEFAULT 0 NOT NULL,
    nests_quantity integer DEFAULT 0,
    cages_quantity integer DEFAULT 0,
    birds_quantity integer DEFAULT 0,
    capacity_theoretical integer DEFAULT 0,
    avaliable_date date
);
    DROP TABLE public.osshed;
       public         postgres    false    278    3            ?           0    0    TABLE osshed    COMMENT     d   COMMENT ON TABLE public.osshed IS 'Almacena la informacion de los galpones asociados a la empresa';
            public       postgres    false    279            @           0    0    COLUMN osshed.shed_id    COMMENT     <   COMMENT ON COLUMN public.osshed.shed_id IS 'Id del galpon';
            public       postgres    false    279            A           0    0    COLUMN osshed.partnership_id    COMMENT     F   COMMENT ON COLUMN public.osshed.partnership_id IS 'Id de la empresa';
            public       postgres    false    279            B           0    0    COLUMN osshed.farm_id    COMMENT     >   COMMENT ON COLUMN public.osshed.farm_id IS 'Id de la granja';
            public       postgres    false    279            C           0    0    COLUMN osshed.center_id    COMMENT     >   COMMENT ON COLUMN public.osshed.center_id IS 'Id del nucleo';
            public       postgres    false    279            D           0    0    COLUMN osshed.code    COMMENT     =   COMMENT ON COLUMN public.osshed.code IS 'Codigo del galpon';
            public       postgres    false    279            E           0    0    COLUMN osshed.statusshed_id    COMMENT     _   COMMENT ON COLUMN public.osshed.statusshed_id IS 'Identificador del estado actual del galpon';
            public       postgres    false    279            F           0    0    COLUMN osshed.type_id    COMMENT     D   COMMENT ON COLUMN public.osshed.type_id IS 'Id del tipo de galpon';
            public       postgres    false    279            G           0    0    COLUMN osshed.building_date    COMMENT     c   COMMENT ON COLUMN public.osshed.building_date IS 'Almacena la fecha de construccion del edificio';
            public       postgres    false    279            H           0    0    COLUMN osshed.stall_width    COMMENT     M   COMMENT ON COLUMN public.osshed.stall_width IS 'Indica el ancho del galpon';
            public       postgres    false    279            I           0    0    COLUMN osshed.stall_height    COMMENT     M   COMMENT ON COLUMN public.osshed.stall_height IS 'Indica el alto del galpon';
            public       postgres    false    279            J           0    0    COLUMN osshed.capacity_min    COMMENT     D   COMMENT ON COLUMN public.osshed.capacity_min IS 'Capacidad minima';
            public       postgres    false    279            K           0    0    COLUMN osshed.capacity_max    COMMENT     F   COMMENT ON COLUMN public.osshed.capacity_max IS 'Capacidad máxima ';
            public       postgres    false    279            L           0    0    COLUMN osshed.environment_id    COMMENT     E   COMMENT ON COLUMN public.osshed.environment_id IS 'Id del ambiente';
            public       postgres    false    279            M           0    0    COLUMN osshed.rotation_days    COMMENT     H   COMMENT ON COLUMN public.osshed.rotation_days IS 'Días de rotación
';
            public       postgres    false    279            N           0    0    COLUMN osshed.nests_quantity    COMMENT     I   COMMENT ON COLUMN public.osshed.nests_quantity IS 'Cantidad de nidales';
            public       postgres    false    279            O           0    0    COLUMN osshed.cages_quantity    COMMENT     H   COMMENT ON COLUMN public.osshed.cages_quantity IS 'Cantidad de jaulas';
            public       postgres    false    279            P           0    0    COLUMN osshed.birds_quantity    COMMENT     F   COMMENT ON COLUMN public.osshed.birds_quantity IS 'Cantidad de aves';
            public       postgres    false    279            Q           0    0 "   COLUMN osshed.capacity_theoretical    COMMENT     O   COMMENT ON COLUMN public.osshed.capacity_theoretical IS '	Capacidad teórica';
            public       postgres    false    279                       1259    110614    silo_id_seq    SEQUENCE     t   CREATE SEQUENCE public.silo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE public.silo_id_seq;
       public       postgres    false    3                       1259    110616    ossilo    TABLE     �  CREATE TABLE public.ossilo (
    silo_id integer DEFAULT nextval('public.silo_id_seq'::regclass) NOT NULL,
    client_id integer,
    partnership_id integer NOT NULL,
    farm_id integer NOT NULL,
    center_id integer NOT NULL,
    name character varying(45) NOT NULL,
    rings_height double precision,
    rings_height_id integer,
    height double precision NOT NULL,
    height_unit_id integer,
    diameter double precision NOT NULL,
    diameter_unit_id integer,
    total_rings_quantity integer,
    measuring_mechanism integer,
    cone_degrees double precision,
    total_capacity_1 double precision,
    total_capacity_2 double precision,
    capacity_unit_id_1 integer,
    capacity_unit_id_2 integer,
    central character varying(20)
);
    DROP TABLE public.ossilo;
       public         postgres    false    280    3            R           0    0    TABLE ossilo    COMMENT     E   COMMENT ON TABLE public.ossilo IS 'Almacena los datos de los silos';
            public       postgres    false    281            S           0    0    COLUMN ossilo.silo_id    COMMENT     :   COMMENT ON COLUMN public.ossilo.silo_id IS 'Id del silo';
            public       postgres    false    281            T           0    0    COLUMN ossilo.client_id    COMMENT     ?   COMMENT ON COLUMN public.ossilo.client_id IS 'Id del cliente';
            public       postgres    false    281            U           0    0    COLUMN ossilo.partnership_id    COMMENT     F   COMMENT ON COLUMN public.ossilo.partnership_id IS 'Id de la empresa';
            public       postgres    false    281            V           0    0    COLUMN ossilo.farm_id    COMMENT     >   COMMENT ON COLUMN public.ossilo.farm_id IS 'Id de la granja';
            public       postgres    false    281            W           0    0    COLUMN ossilo.center_id    COMMENT     >   COMMENT ON COLUMN public.ossilo.center_id IS 'Id del nucleo';
            public       postgres    false    281            X           0    0    COLUMN ossilo.name    COMMENT     ;   COMMENT ON COLUMN public.ossilo.name IS 'Nombre del silo';
            public       postgres    false    281            Y           0    0    COLUMN ossilo.rings_height    COMMENT     E   COMMENT ON COLUMN public.ossilo.rings_height IS 'Numero de anillos';
            public       postgres    false    281            Z           0    0    COLUMN ossilo.rings_height_id    COMMENT     R   COMMENT ON COLUMN public.ossilo.rings_height_id IS 'Unidad de medida del anillo';
            public       postgres    false    281            [           0    0    COLUMN ossilo.height    COMMENT     =   COMMENT ON COLUMN public.ossilo.height IS 'Altura del silo';
            public       postgres    false    281            \           0    0    COLUMN ossilo.height_unit_id    COMMENT     R   COMMENT ON COLUMN public.ossilo.height_unit_id IS 'Unidad de media de la altura';
            public       postgres    false    281            ]           0    0    COLUMN ossilo.diameter    COMMENT     A   COMMENT ON COLUMN public.ossilo.diameter IS 'Diametro del silo';
            public       postgres    false    281            ^           0    0    COLUMN ossilo.diameter_unit_id    COMMENT     T   COMMENT ON COLUMN public.ossilo.diameter_unit_id IS 'Unidad de media del diametro';
            public       postgres    false    281            _           0    0 "   COLUMN ossilo.total_rings_quantity    COMMENT     U   COMMENT ON COLUMN public.ossilo.total_rings_quantity IS 'Total de anillos del silo';
            public       postgres    false    281            `           0    0 !   COLUMN ossilo.measuring_mechanism    COMMENT     Y   COMMENT ON COLUMN public.ossilo.measuring_mechanism IS 'Mecanismo de medidad del silo
';
            public       postgres    false    281            a           0    0    COLUMN ossilo.cone_degrees    COMMENT     C   COMMENT ON COLUMN public.ossilo.cone_degrees IS 'Grados del cono';
            public       postgres    false    281            b           0    0    COLUMN ossilo.total_capacity_1    COMMENT     L   COMMENT ON COLUMN public.ossilo.total_capacity_1 IS 'Total de capacidad 1';
            public       postgres    false    281            c           0    0    COLUMN ossilo.total_capacity_2    COMMENT     L   COMMENT ON COLUMN public.ossilo.total_capacity_2 IS 'Total de capacidad 2';
            public       postgres    false    281            d           0    0     COLUMN ossilo.capacity_unit_id_1    COMMENT     X   COMMENT ON COLUMN public.ossilo.capacity_unit_id_1 IS 'Id de Capacidad de la unidad 1';
            public       postgres    false    281            e           0    0     COLUMN ossilo.capacity_unit_id_2    COMMENT     X   COMMENT ON COLUMN public.ossilo.capacity_unit_id_2 IS 'Id de Capacidad de la unidad 2';
            public       postgres    false    281            f           0    0    COLUMN ossilo.central    COMMENT     6   COMMENT ON COLUMN public.ossilo.central IS 'Central';
            public       postgres    false    281                       1259    110620    ossilo_osshed    TABLE     �   CREATE TABLE public.ossilo_osshed (
    silo_id integer NOT NULL,
    shed_id integer NOT NULL,
    center_id integer NOT NULL,
    farm_id integer NOT NULL,
    partnership_id integer NOT NULL,
    client_id integer NOT NULL,
    deleted_mark integer
);
 !   DROP TABLE public.ossilo_osshed;
       public         postgres    false    3            g           0    0    TABLE ossilo_osshed    COMMENT     R   COMMENT ON TABLE public.ossilo_osshed IS 'Tabla union de las tablas silo y shed';
            public       postgres    false    282            h           0    0    COLUMN ossilo_osshed.silo_id    COMMENT     A   COMMENT ON COLUMN public.ossilo_osshed.silo_id IS 'Id del silo';
            public       postgres    false    282            i           0    0    COLUMN ossilo_osshed.shed_id    COMMENT     C   COMMENT ON COLUMN public.ossilo_osshed.shed_id IS 'Id del galpon';
            public       postgres    false    282            j           0    0    COLUMN ossilo_osshed.center_id    COMMENT     E   COMMENT ON COLUMN public.ossilo_osshed.center_id IS 'Id del nucleo';
            public       postgres    false    282            k           0    0    COLUMN ossilo_osshed.farm_id    COMMENT     E   COMMENT ON COLUMN public.ossilo_osshed.farm_id IS 'Id de la granja';
            public       postgres    false    282            l           0    0 #   COLUMN ossilo_osshed.partnership_id    COMMENT     M   COMMENT ON COLUMN public.ossilo_osshed.partnership_id IS 'Id de la empresa';
            public       postgres    false    282            m           0    0    COLUMN ossilo_osshed.client_id    COMMENT     F   COMMENT ON COLUMN public.ossilo_osshed.client_id IS 'Id del cliente';
            public       postgres    false    282            n           0    0 !   COLUMN ossilo_osshed.deleted_mark    COMMENT     K   COMMENT ON COLUMN public.ossilo_osshed.deleted_mark IS 'Marca de borrado';
            public       postgres    false    282                       1259    110623    slaughterhouse_id_seq    SEQUENCE        CREATE SEQUENCE public.slaughterhouse_id_seq
    START WITH 33
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.slaughterhouse_id_seq;
       public       postgres    false    3                       1259    110625    osslaughterhouse    TABLE     Z  CREATE TABLE public.osslaughterhouse (
    slaughterhouse_id integer DEFAULT nextval('public.slaughterhouse_id_seq'::regclass) NOT NULL,
    name character varying(45) NOT NULL,
    address character varying(250) NOT NULL,
    description character varying(250) NOT NULL,
    code character varying(20) NOT NULL,
    capacity double precision
);
 $   DROP TABLE public.osslaughterhouse;
       public         postgres    false    283    3                       1259    110632    warehouse_id_seq    SEQUENCE     y   CREATE SEQUENCE public.warehouse_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.warehouse_id_seq;
       public       postgres    false    3                       1259    110634    oswarehouse    TABLE       CREATE TABLE public.oswarehouse (
    warehouse_id integer DEFAULT nextval('public.warehouse_id_seq'::regclass) NOT NULL,
    partnership_id integer NOT NULL,
    farm_id integer NOT NULL,
    name character varying(45) NOT NULL,
    code character varying(20) NOT NULL
);
    DROP TABLE public.oswarehouse;
       public         postgres    false    285    3            o           0    0    TABLE oswarehouse    COMMENT     \   COMMENT ON TABLE public.oswarehouse IS 'Almacena la informacion referente a los almacenes';
            public       postgres    false    286            p           0    0    COLUMN oswarehouse.warehouse_id    COMMENT     G   COMMENT ON COLUMN public.oswarehouse.warehouse_id IS 'Id del almacen';
            public       postgres    false    286            q           0    0 !   COLUMN oswarehouse.partnership_id    COMMENT     ^   COMMENT ON COLUMN public.oswarehouse.partnership_id IS 'Id de la empresa dueña del almacen';
            public       postgres    false    286            r           0    0    COLUMN oswarehouse.farm_id    COMMENT     C   COMMENT ON COLUMN public.oswarehouse.farm_id IS 'Id de la granja';
            public       postgres    false    286            s           0    0    COLUMN oswarehouse.name    COMMENT     C   COMMENT ON COLUMN public.oswarehouse.name IS 'Nombre del almacen';
            public       postgres    false    286            t           0    0    COLUMN oswarehouse.code    COMMENT     C   COMMENT ON COLUMN public.oswarehouse.code IS 'Codigo del almacen';
            public       postgres    false    286                       1259    110638    posture_curve_id_seq    SEQUENCE     }   CREATE SEQUENCE public.posture_curve_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.posture_curve_id_seq;
       public       postgres    false    3                        1259    110640    predecessor_id_seq    SEQUENCE     {   CREATE SEQUENCE public.predecessor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.predecessor_id_seq;
       public       postgres    false    3            !           1259    110642    process_class_id_seq    SEQUENCE     }   CREATE SEQUENCE public.process_class_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.process_class_id_seq;
       public       postgres    false    3            "           1259    110644    programmed_eggs_id_seq    SEQUENCE        CREATE SEQUENCE public.programmed_eggs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.programmed_eggs_id_seq;
       public       postgres    false    3            #           1259    110646    raspberry_id_seq    SEQUENCE     y   CREATE SEQUENCE public.raspberry_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.raspberry_id_seq;
       public       postgres    false    3            $           1259    110648    scenario_formula_id_seq    SEQUENCE     �   CREATE SEQUENCE public.scenario_formula_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.scenario_formula_id_seq;
       public       postgres    false    3            %           1259    110650    scenario_parameter_day_seq    SEQUENCE     �   CREATE SEQUENCE public.scenario_parameter_day_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.scenario_parameter_day_seq;
       public       postgres    false    3            &           1259    110652    scenario_parameter_id_seq    SEQUENCE     �   CREATE SEQUENCE public.scenario_parameter_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.scenario_parameter_id_seq;
       public       postgres    false    3            '           1259    110654    scenario_posture_id_seq    SEQUENCE     �   CREATE SEQUENCE public.scenario_posture_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.scenario_posture_id_seq;
       public       postgres    false    3            (           1259    110656    scenario_process_id_seq    SEQUENCE     �   CREATE SEQUENCE public.scenario_process_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.scenario_process_id_seq;
       public       postgres    false    3            )           1259    110658    txavailabilitysheds    TABLE       CREATE TABLE public.txavailabilitysheds (
    availability_shed_id integer DEFAULT nextval('public.availability_shed_id_seq'::regclass) NOT NULL,
    shed_id integer NOT NULL,
    init_date date,
    end_date date,
    lot_code character varying(20) NOT NULL
);
 '   DROP TABLE public.txavailabilitysheds;
       public         postgres    false    216    3            u           0    0    TABLE txavailabilitysheds    COMMENT     �   COMMENT ON TABLE public.txavailabilitysheds IS 'Almacena la disponibilidad en fechas de los galpones de acuerdo a la programación establecida';
            public       postgres    false    297            v           0    0 /   COLUMN txavailabilitysheds.availability_shed_id    COMMENT     �   COMMENT ON COLUMN public.txavailabilitysheds.availability_shed_id IS 'Id de la disponibilidad del almacen, indicando si este esta disponible';
            public       postgres    false    297            w           0    0 "   COLUMN txavailabilitysheds.shed_id    COMMENT     I   COMMENT ON COLUMN public.txavailabilitysheds.shed_id IS 'Id del galpon';
            public       postgres    false    297            x           0    0 $   COLUMN txavailabilitysheds.init_date    COMMENT     r   COMMENT ON COLUMN public.txavailabilitysheds.init_date IS 'Fecha de inicio de la programacion de uso del galpon';
            public       postgres    false    297            y           0    0 #   COLUMN txavailabilitysheds.end_date    COMMENT     r   COMMENT ON COLUMN public.txavailabilitysheds.end_date IS 'Fecha de cerrado de la programacion de uso del galpon';
            public       postgres    false    297            z           0    0 #   COLUMN txavailabilitysheds.lot_code    COMMENT     W   COMMENT ON COLUMN public.txavailabilitysheds.lot_code IS 'codigo del lote del galpon';
            public       postgres    false    297            *           1259    110662 	   txbroiler    TABLE     L  CREATE TABLE public.txbroiler (
    broiler_id integer DEFAULT nextval('public.broiler_id_seq'::regclass) NOT NULL,
    projected_date date,
    projected_quantity integer,
    partnership_id integer,
    scenario_id integer,
    breed_id integer,
    lot_incubator character varying(45) NOT NULL,
    programmed_eggs_id integer
);
    DROP TABLE public.txbroiler;
       public         postgres    false    220    3            {           0    0    TABLE txbroiler    COMMENT     c   COMMENT ON TABLE public.txbroiler IS 'Almacena la proyeccion realizada para el modulo de engorde';
            public       postgres    false    298            |           0    0    COLUMN txbroiler.broiler_id    COMMENT     U   COMMENT ON COLUMN public.txbroiler.broiler_id IS 'Id de la programacion de engorde';
            public       postgres    false    298            }           0    0    COLUMN txbroiler.projected_date    COMMENT     X   COMMENT ON COLUMN public.txbroiler.projected_date IS 'Fecha de proyección de engorde';
            public       postgres    false    298            ~           0    0 #   COLUMN txbroiler.projected_quantity    COMMENT     `   COMMENT ON COLUMN public.txbroiler.projected_quantity IS 'Cantidad proyectada para el engorde';
            public       postgres    false    298                       0    0    COLUMN txbroiler.partnership_id    COMMENT     I   COMMENT ON COLUMN public.txbroiler.partnership_id IS 'Id de la empresa';
            public       postgres    false    298            �           0    0    COLUMN txbroiler.scenario_id    COMMENT     G   COMMENT ON COLUMN public.txbroiler.scenario_id IS 'Id edl escenario ';
            public       postgres    false    298            �           0    0    COLUMN txbroiler.breed_id    COMMENT     K   COMMENT ON COLUMN public.txbroiler.breed_id IS 'Id de la raza a engordar';
            public       postgres    false    298            �           0    0    COLUMN txbroiler.lot_incubator    COMMENT     u   COMMENT ON COLUMN public.txbroiler.lot_incubator IS 'Lote de incubación de donde provienen los huevos proyectados';
            public       postgres    false    298            �           0    0 #   COLUMN txbroiler.programmed_eggs_id    COMMENT     Y   COMMENT ON COLUMN public.txbroiler.programmed_eggs_id IS 'Id de los huevos programados';
            public       postgres    false    298            +           1259    110666    txbroiler_detail    TABLE     �  CREATE TABLE public.txbroiler_detail (
    broiler_detail_id integer DEFAULT nextval('public.broiler_detail_id_seq'::regclass) NOT NULL,
    broiler_id integer NOT NULL,
    scheduled_date date,
    scheduled_quantity integer,
    farm_id integer NOT NULL,
    shed_id integer NOT NULL,
    confirm integer,
    execution_date date,
    execution_quantity integer,
    lot integer NOT NULL,
    broiler_product_id integer
);
 $   DROP TABLE public.txbroiler_detail;
       public         postgres    false    219    3            �           0    0    TABLE txbroiler_detail    COMMENT     l   COMMENT ON TABLE public.txbroiler_detail IS 'Almacena la programacion y ejecuccion del proceso de engorde';
            public       postgres    false    299            �           0    0 )   COLUMN txbroiler_detail.broiler_detail_id    COMMENT     `   COMMENT ON COLUMN public.txbroiler_detail.broiler_detail_id IS 'Id de los detalles de engorde';
            public       postgres    false    299            �           0    0 "   COLUMN txbroiler_detail.broiler_id    COMMENT     \   COMMENT ON COLUMN public.txbroiler_detail.broiler_id IS 'Id de la programacion de engorde';
            public       postgres    false    299            �           0    0 &   COLUMN txbroiler_detail.scheduled_date    COMMENT     k   COMMENT ON COLUMN public.txbroiler_detail.scheduled_date IS 'Fecha programada para el proceso de engorde';
            public       postgres    false    299            �           0    0 *   COLUMN txbroiler_detail.scheduled_quantity    COMMENT     r   COMMENT ON COLUMN public.txbroiler_detail.scheduled_quantity IS 'Cantidad programada para el proceso de engorde';
            public       postgres    false    299            �           0    0    COLUMN txbroiler_detail.farm_id    COMMENT     H   COMMENT ON COLUMN public.txbroiler_detail.farm_id IS 'Id de la granja';
            public       postgres    false    299            �           0    0    COLUMN txbroiler_detail.shed_id    COMMENT     F   COMMENT ON COLUMN public.txbroiler_detail.shed_id IS 'Id del galpon';
            public       postgres    false    299            �           0    0    COLUMN txbroiler_detail.confirm    COMMENT     E   COMMENT ON COLUMN public.txbroiler_detail.confirm IS 'Confirmacion';
            public       postgres    false    299            �           0    0 &   COLUMN txbroiler_detail.execution_date    COMMENT     p   COMMENT ON COLUMN public.txbroiler_detail.execution_date IS 'Fecha de ejeccion de la planificacion de engorde';
            public       postgres    false    299            �           0    0 *   COLUMN txbroiler_detail.execution_quantity    COMMENT     u   COMMENT ON COLUMN public.txbroiler_detail.execution_quantity IS 'Cantidad ejecutada de la programación de engorde';
            public       postgres    false    299            �           0    0    COLUMN txbroiler_detail.lot    COMMENT     D   COMMENT ON COLUMN public.txbroiler_detail.lot IS 'Lote de engorde';
            public       postgres    false    299            �           0    0 *   COLUMN txbroiler_detail.broiler_product_id    COMMENT     ^   COMMENT ON COLUMN public.txbroiler_detail.broiler_product_id IS 'Id del producto de engorde';
            public       postgres    false    299            ,           1259    110670    txbroilereviction    TABLE     �  CREATE TABLE public.txbroilereviction (
    broilereviction_id integer DEFAULT nextval('public.broilereviction_id_seq'::regclass) NOT NULL,
    projected_date date,
    projected_quantity integer,
    partnership_id integer NOT NULL,
    scenario_id integer NOT NULL,
    breed_id integer NOT NULL,
    lot_incubator character varying(45) NOT NULL,
    broiler_detail_id integer NOT NULL
);
 %   DROP TABLE public.txbroilereviction;
       public         postgres    false    224    3            �           0    0    TABLE txbroilereviction    COMMENT     _   COMMENT ON TABLE public.txbroilereviction IS 'Almacena las proyeccion del modula de desalojo';
            public       postgres    false    300            �           0    0 +   COLUMN txbroilereviction.broilereviction_id    COMMENT     ^   COMMENT ON COLUMN public.txbroilereviction.broilereviction_id IS 'Id del modulo de desalojo';
            public       postgres    false    300            �           0    0 '   COLUMN txbroilereviction.projected_date    COMMENT     b   COMMENT ON COLUMN public.txbroilereviction.projected_date IS 'Fecha proyectada para el desalojo';
            public       postgres    false    300            �           0    0 +   COLUMN txbroilereviction.projected_quantity    COMMENT     i   COMMENT ON COLUMN public.txbroilereviction.projected_quantity IS 'Cantidad proyectada para el desalojo';
            public       postgres    false    300            �           0    0 '   COLUMN txbroilereviction.partnership_id    COMMENT     Q   COMMENT ON COLUMN public.txbroilereviction.partnership_id IS 'Id de la empresa';
            public       postgres    false    300            �           0    0 $   COLUMN txbroilereviction.scenario_id    COMMENT     N   COMMENT ON COLUMN public.txbroilereviction.scenario_id IS 'Id del escenario';
            public       postgres    false    300            �           0    0 !   COLUMN txbroilereviction.breed_id    COMMENT     H   COMMENT ON COLUMN public.txbroilereviction.breed_id IS 'Id de la raza';
            public       postgres    false    300            �           0    0 &   COLUMN txbroilereviction.lot_incubator    COMMENT     R   COMMENT ON COLUMN public.txbroilereviction.lot_incubator IS 'Lote de incubacion';
            public       postgres    false    300            -           1259    110674    txbroilereviction_detail    TABLE     �  CREATE TABLE public.txbroilereviction_detail (
    broilereviction_detail_id integer DEFAULT nextval('public.broilereviction_detail_id_seq'::regclass) NOT NULL,
    broilereviction_id integer NOT NULL,
    scheduled_date date,
    scheduled_quantity integer,
    farm_id integer NOT NULL,
    shed_id integer NOT NULL,
    confirm integer,
    execution_date date,
    execution_quantity integer,
    lot integer NOT NULL,
    broiler_product_id integer NOT NULL,
    slaughterhouse_id integer NOT NULL
);
 ,   DROP TABLE public.txbroilereviction_detail;
       public         postgres    false    223    3            �           0    0    TABLE txbroilereviction_detail    COMMENT     v   COMMENT ON TABLE public.txbroilereviction_detail IS 'Almacena la programación y ejecución del módulo de desalojo';
            public       postgres    false    301            �           0    0 9   COLUMN txbroilereviction_detail.broilereviction_detail_id    COMMENT     ~   COMMENT ON COLUMN public.txbroilereviction_detail.broilereviction_detail_id IS 'Id de los detalles del modulo de desarrollo';
            public       postgres    false    301            �           0    0 2   COLUMN txbroilereviction_detail.broilereviction_id    COMMENT     e   COMMENT ON COLUMN public.txbroilereviction_detail.broilereviction_id IS 'Id del modulo de desalojo';
            public       postgres    false    301            �           0    0 .   COLUMN txbroilereviction_detail.scheduled_date    COMMENT     i   COMMENT ON COLUMN public.txbroilereviction_detail.scheduled_date IS 'Fecha programada para el desalojo';
            public       postgres    false    301            �           0    0 2   COLUMN txbroilereviction_detail.scheduled_quantity    COMMENT     p   COMMENT ON COLUMN public.txbroilereviction_detail.scheduled_quantity IS 'Cantidad programada para el desalojo';
            public       postgres    false    301            �           0    0 '   COLUMN txbroilereviction_detail.farm_id    COMMENT     P   COMMENT ON COLUMN public.txbroilereviction_detail.farm_id IS 'Id de la granja';
            public       postgres    false    301            �           0    0 '   COLUMN txbroilereviction_detail.shed_id    COMMENT     N   COMMENT ON COLUMN public.txbroilereviction_detail.shed_id IS 'Id del galpon';
            public       postgres    false    301            �           0    0 '   COLUMN txbroilereviction_detail.confirm    COMMENT     M   COMMENT ON COLUMN public.txbroilereviction_detail.confirm IS 'Confirmacion';
            public       postgres    false    301            �           0    0 .   COLUMN txbroilereviction_detail.execution_date    COMMENT     \   COMMENT ON COLUMN public.txbroilereviction_detail.execution_date IS 'Fecha de ejecución ';
            public       postgres    false    301            �           0    0 2   COLUMN txbroilereviction_detail.execution_quantity    COMMENT     c   COMMENT ON COLUMN public.txbroilereviction_detail.execution_quantity IS 'Cantidad de ejecución ';
            public       postgres    false    301            �           0    0 #   COLUMN txbroilereviction_detail.lot    COMMENT     X   COMMENT ON COLUMN public.txbroilereviction_detail.lot IS 'Lote del modulo de desalojo';
            public       postgres    false    301            �           0    0 2   COLUMN txbroilereviction_detail.broiler_product_id    COMMENT     f   COMMENT ON COLUMN public.txbroilereviction_detail.broiler_product_id IS 'Id del producto de engorde';
            public       postgres    false    301            �           0    0 1   COLUMN txbroilereviction_detail.slaughterhouse_id    COMMENT     g   COMMENT ON COLUMN public.txbroilereviction_detail.slaughterhouse_id IS 'Id de la planta de beneficio';
            public       postgres    false    301            .           1259    110678    txbroilerproduct_detail    TABLE     �   CREATE TABLE public.txbroilerproduct_detail (
    broilerproduct_detail_id integer DEFAULT nextval('public.broiler_product_detail_id_seq'::regclass) NOT NULL,
    broiler_detail integer NOT NULL,
    broiler_product_id integer,
    quantity integer
);
 +   DROP TABLE public.txbroilerproduct_detail;
       public         postgres    false    221    3            �           0    0    TABLE txbroilerproduct_detail    COMMENT     h   COMMENT ON TABLE public.txbroilerproduct_detail IS 'Almacena los detalles de la produccion de engorde';
            public       postgres    false    302            �           0    0 7   COLUMN txbroilerproduct_detail.broilerproduct_detail_id    COMMENT     |   COMMENT ON COLUMN public.txbroilerproduct_detail.broilerproduct_detail_id IS 'Id de los detalles de produccion de engorde';
            public       postgres    false    302            �           0    0 -   COLUMN txbroilerproduct_detail.broiler_detail    COMMENT     Z   COMMENT ON COLUMN public.txbroilerproduct_detail.broiler_detail IS 'Detalles de engorde';
            public       postgres    false    302            �           0    0 1   COLUMN txbroilerproduct_detail.broiler_product_id    COMMENT     e   COMMENT ON COLUMN public.txbroilerproduct_detail.broiler_product_id IS 'Id del producto de engorde';
            public       postgres    false    302            �           0    0 '   COLUMN txbroilerproduct_detail.quantity    COMMENT     `   COMMENT ON COLUMN public.txbroilerproduct_detail.quantity IS 'Cantidad de producto de engorde';
            public       postgres    false    302            /           1259    110682    txbroodermachine    TABLE     �  CREATE TABLE public.txbroodermachine (
    brooder_machine_id_seq integer DEFAULT nextval('public.brooder_machines_id_seq'::regclass) NOT NULL,
    partnership_id integer NOT NULL,
    farm_id integer NOT NULL,
    capacity integer,
    sunday integer,
    monday integer,
    tuesday integer,
    wednesday integer,
    thursday integer,
    friday integer,
    saturday integer,
    name character varying(250)
);
 $   DROP TABLE public.txbroodermachine;
       public         postgres    false    226    3            �           0    0    TABLE txbroodermachine    COMMENT     ]   COMMENT ON TABLE public.txbroodermachine IS 'Almacena los datos de las maquinas de engorde';
            public       postgres    false    303            �           0    0 .   COLUMN txbroodermachine.brooder_machine_id_seq    COMMENT     c   COMMENT ON COLUMN public.txbroodermachine.brooder_machine_id_seq IS 'Id de la maquina de engorde';
            public       postgres    false    303            �           0    0 &   COLUMN txbroodermachine.partnership_id    COMMENT     P   COMMENT ON COLUMN public.txbroodermachine.partnership_id IS 'Id de la empresa';
            public       postgres    false    303            �           0    0    COLUMN txbroodermachine.farm_id    COMMENT     H   COMMENT ON COLUMN public.txbroodermachine.farm_id IS 'Id de la granja';
            public       postgres    false    303            �           0    0     COLUMN txbroodermachine.capacity    COMMENT     Q   COMMENT ON COLUMN public.txbroodermachine.capacity IS 'Capacidad de la maquina';
            public       postgres    false    303            �           0    0    COLUMN txbroodermachine.sunday    COMMENT     ?   COMMENT ON COLUMN public.txbroodermachine.sunday IS 'Domingo';
            public       postgres    false    303            �           0    0    COLUMN txbroodermachine.monday    COMMENT     =   COMMENT ON COLUMN public.txbroodermachine.monday IS 'Lunes';
            public       postgres    false    303            �           0    0    COLUMN txbroodermachine.tuesday    COMMENT     ?   COMMENT ON COLUMN public.txbroodermachine.tuesday IS 'Martes';
            public       postgres    false    303            �           0    0 !   COLUMN txbroodermachine.wednesday    COMMENT     D   COMMENT ON COLUMN public.txbroodermachine.wednesday IS 'Miercoles';
            public       postgres    false    303            �           0    0     COLUMN txbroodermachine.thursday    COMMENT     @   COMMENT ON COLUMN public.txbroodermachine.thursday IS 'Jueves';
            public       postgres    false    303            �           0    0    COLUMN txbroodermachine.friday    COMMENT     ?   COMMENT ON COLUMN public.txbroodermachine.friday IS 'Viernes';
            public       postgres    false    303            �           0    0     COLUMN txbroodermachine.saturday    COMMENT     @   COMMENT ON COLUMN public.txbroodermachine.saturday IS 'Sabado';
            public       postgres    false    303            �           0    0    COLUMN txbroodermachine.name    COMMENT     J   COMMENT ON COLUMN public.txbroodermachine.name IS 'Nombre de la maquina';
            public       postgres    false    303            0           1259    110686 
   txcalendar    TABLE     |  CREATE TABLE public.txcalendar (
    calendar_id integer DEFAULT nextval('public.calendar_id_seq'::regclass) NOT NULL,
    description character varying(250) NOT NULL,
    saturday character varying(15),
    sunday character varying(15),
    week_start character varying(15),
    code character(20) NOT NULL,
    year_start integer,
    year_end integer,
    generated integer
);
    DROP TABLE public.txcalendar;
       public         postgres    false    228    3            �           0    0    TABLE txcalendar    COMMENT     n   COMMENT ON TABLE public.txcalendar IS 'Almacena la informacion del calendario con la que trabaja el sistema';
            public       postgres    false    304            �           0    0    COLUMN txcalendar.calendar_id    COMMENT     H   COMMENT ON COLUMN public.txcalendar.calendar_id IS 'Id del calendario';
            public       postgres    false    304            �           0    0    COLUMN txcalendar.description    COMMENT     S   COMMENT ON COLUMN public.txcalendar.description IS 'Descripción del calendario
';
            public       postgres    false    304            �           0    0    COLUMN txcalendar.saturday    COMMENT     Z   COMMENT ON COLUMN public.txcalendar.saturday IS 'Indica si el día sábado es laborable';
            public       postgres    false    304            �           0    0    COLUMN txcalendar.sunday    COMMENT     X   COMMENT ON COLUMN public.txcalendar.sunday IS 'Indica si el día Domingo es laborable';
            public       postgres    false    304            �           0    0    COLUMN txcalendar.week_start    COMMENT     [   COMMENT ON COLUMN public.txcalendar.week_start IS 'Semana en la que inicia el calendario';
            public       postgres    false    304            �           0    0    COLUMN txcalendar.code    COMMENT     E   COMMENT ON COLUMN public.txcalendar.code IS 'Codigo del calendario';
            public       postgres    false    304            �           0    0    COLUMN txcalendar.year_start    COMMENT     Y   COMMENT ON COLUMN public.txcalendar.year_start IS 'Año en el que inicia el calendario';
            public       postgres    false    304            �           0    0    COLUMN txcalendar.year_end    COMMENT     Y   COMMENT ON COLUMN public.txcalendar.year_end IS 'Año en el que finaliza el calendario';
            public       postgres    false    304            �           0    0    COLUMN txcalendar.generated    COMMENT     u   COMMENT ON COLUMN public.txcalendar.generated IS 'Indica si el calendario fue generado a partir de otro calendario';
            public       postgres    false    304            1           1259    110690    txcalendarday    TABLE     �  CREATE TABLE public.txcalendarday (
    calendar_day_id integer DEFAULT nextval('public.calendar_day_id_seq'::regclass) NOT NULL,
    calendar_id integer NOT NULL,
    use_date timestamp with time zone NOT NULL,
    use_year integer NOT NULL,
    use_month integer NOT NULL,
    use_day integer NOT NULL,
    use_week timestamp with time zone NOT NULL,
    week_day integer NOT NULL,
    sequence integer NOT NULL,
    working_day integer NOT NULL
);
 !   DROP TABLE public.txcalendarday;
       public         postgres    false    227    3            �           0    0    TABLE txcalendarday    COMMENT     _   COMMENT ON TABLE public.txcalendarday IS 'Almacena los datos de los dias que son laborables ';
            public       postgres    false    305            �           0    0 $   COLUMN txcalendarday.calendar_day_id    COMMENT     W   COMMENT ON COLUMN public.txcalendarday.calendar_day_id IS 'Id del dia del calendario';
            public       postgres    false    305            �           0    0     COLUMN txcalendarday.calendar_id    COMMENT     K   COMMENT ON COLUMN public.txcalendarday.calendar_id IS 'Id del calendario';
            public       postgres    false    305            �           0    0    COLUMN txcalendarday.use_date    COMMENT     d   COMMENT ON COLUMN public.txcalendarday.use_date IS 'Fecha en el que se encuentra el dia laborable';
            public       postgres    false    305            �           0    0    COLUMN txcalendarday.use_year    COMMENT     c   COMMENT ON COLUMN public.txcalendarday.use_year IS 'Año en el que se encuentra el dia laborable';
            public       postgres    false    305            �           0    0    COLUMN txcalendarday.use_month    COMMENT     c   COMMENT ON COLUMN public.txcalendarday.use_month IS 'Mes en el que se encuentra el dia laborable';
            public       postgres    false    305            �           0    0    COLUMN txcalendarday.use_day    COMMENT     a   COMMENT ON COLUMN public.txcalendarday.use_day IS 'Dia en el que se encuentra el dia laborable';
            public       postgres    false    305            �           0    0    COLUMN txcalendarday.use_week    COMMENT     e   COMMENT ON COLUMN public.txcalendarday.use_week IS 'Semana en el que se encuentra el dia laborable';
            public       postgres    false    305            �           0    0    COLUMN txcalendarday.week_day    COMMENT     l   COMMENT ON COLUMN public.txcalendarday.week_day IS 'Dia de semana en el que se encuentra el dia laborable';
            public       postgres    false    305            �           0    0     COLUMN txcalendarday.working_day    COMMENT     Z   COMMENT ON COLUMN public.txcalendarday.working_day IS 'Indica si el dia es laboral o no';
            public       postgres    false    305            2           1259    110694    txeggs_planning    TABLE       CREATE TABLE public.txeggs_planning (
    egg_planning_id integer DEFAULT nextval('public.egg_planning_id_seq'::regclass) NOT NULL,
    month_planning integer,
    year_planning integer,
    scenario_id integer,
    planned double precision,
    breed_id integer NOT NULL
);
 #   DROP TABLE public.txeggs_planning;
       public         postgres    false    230    3            �           0    0    TABLE txeggs_planning    COMMENT     g   COMMENT ON TABLE public.txeggs_planning IS 'Almacena los detalles de la planificación de los huevos';
            public       postgres    false    306            �           0    0 &   COLUMN txeggs_planning.egg_planning_id    COMMENT     [   COMMENT ON COLUMN public.txeggs_planning.egg_planning_id IS 'Id de planeación de huevos';
            public       postgres    false    306            �           0    0 %   COLUMN txeggs_planning.month_planning    COMMENT     c   COMMENT ON COLUMN public.txeggs_planning.month_planning IS 'Mes de planificación de los huevos
';
            public       postgres    false    306            �           0    0 $   COLUMN txeggs_planning.year_planning    COMMENT     b   COMMENT ON COLUMN public.txeggs_planning.year_planning IS 'Año de planificación de los huevos';
            public       postgres    false    306            �           0    0 "   COLUMN txeggs_planning.scenario_id    COMMENT     p   COMMENT ON COLUMN public.txeggs_planning.scenario_id IS 'Escenario al cual pertenecen los huevos planificados';
            public       postgres    false    306            �           0    0    COLUMN txeggs_planning.planned    COMMENT     X   COMMENT ON COLUMN public.txeggs_planning.planned IS 'Cantidad de huevos planificados
';
            public       postgres    false    306            �           0    0    COLUMN txeggs_planning.breed_id    COMMENT     T   COMMENT ON COLUMN public.txeggs_planning.breed_id IS 'Id de la raza de los huevos';
            public       postgres    false    306            3           1259    110698    txeggs_required    TABLE     
  CREATE TABLE public.txeggs_required (
    egg_required_id integer DEFAULT nextval('public.egg_required_id_seq'::regclass) NOT NULL,
    use_month integer,
    use_year integer,
    scenario_id integer NOT NULL,
    required double precision,
    breed_id integer
);
 #   DROP TABLE public.txeggs_required;
       public         postgres    false    231    3            �           0    0    TABLE txeggs_required    COMMENT     V   COMMENT ON TABLE public.txeggs_required IS 'Almacena los datos de huevos requeridos';
            public       postgres    false    307            �           0    0 &   COLUMN txeggs_required.egg_required_id    COMMENT     [   COMMENT ON COLUMN public.txeggs_required.egg_required_id IS 'Id de los huevos requeridos';
            public       postgres    false    307            �           0    0     COLUMN txeggs_required.use_month    COMMENT     =   COMMENT ON COLUMN public.txeggs_required.use_month IS 'Mes';
            public       postgres    false    307            �           0    0    COLUMN txeggs_required.use_year    COMMENT     =   COMMENT ON COLUMN public.txeggs_required.use_year IS 'Año';
            public       postgres    false    307            �           0    0 "   COLUMN txeggs_required.scenario_id    COMMENT     L   COMMENT ON COLUMN public.txeggs_required.scenario_id IS 'Id del escenario';
            public       postgres    false    307            �           0    0    COLUMN txeggs_required.required    COMMENT     K   COMMENT ON COLUMN public.txeggs_required.required IS 'Cantidad requerida';
            public       postgres    false    307            �           0    0    COLUMN txeggs_required.breed_id    COMMENT     F   COMMENT ON COLUMN public.txeggs_required.breed_id IS 'Id de la raza';
            public       postgres    false    307            4           1259    110702    txeggs_storage    TABLE     G  CREATE TABLE public.txeggs_storage (
    eggs_storage_id integer DEFAULT nextval('public.eggs_storage_id_seq'::regclass) NOT NULL,
    incubator_plant_id integer NOT NULL,
    scenario_id integer NOT NULL,
    breed_id integer NOT NULL,
    init_date date,
    end_date date,
    lot character varying(45),
    eggs integer
);
 "   DROP TABLE public.txeggs_storage;
       public         postgres    false    232    3            �           0    0    TABLE txeggs_storage    COMMENT     ~   COMMENT ON TABLE public.txeggs_storage IS 'Guarda la informacion de almacenamiento de los huevos en las plantas incubadoras';
            public       postgres    false    308            �           0    0 %   COLUMN txeggs_storage.eggs_storage_id    COMMENT     W   COMMENT ON COLUMN public.txeggs_storage.eggs_storage_id IS 'Id del almacen de huevos';
            public       postgres    false    308            �           0    0 (   COLUMN txeggs_storage.incubator_plant_id    COMMENT     Y   COMMENT ON COLUMN public.txeggs_storage.incubator_plant_id IS 'Id de planta incubadora';
            public       postgres    false    308            �           0    0 !   COLUMN txeggs_storage.scenario_id    COMMENT     K   COMMENT ON COLUMN public.txeggs_storage.scenario_id IS 'Id del escenario';
            public       postgres    false    308            �           0    0    COLUMN txeggs_storage.breed_id    COMMENT     E   COMMENT ON COLUMN public.txeggs_storage.breed_id IS 'Id de la raza';
            public       postgres    false    308            �           0    0    COLUMN txeggs_storage.init_date    COMMENT     H   COMMENT ON COLUMN public.txeggs_storage.init_date IS 'Fecha de inicio';
            public       postgres    false    308            �           0    0    COLUMN txeggs_storage.end_date    COMMENT     J   COMMENT ON COLUMN public.txeggs_storage.end_date IS 'Fecha de terminado';
            public       postgres    false    308            �           0    0    COLUMN txeggs_storage.lot    COMMENT     7   COMMENT ON COLUMN public.txeggs_storage.lot IS 'Lote';
            public       postgres    false    308            �           0    0    COLUMN txeggs_storage.eggs    COMMENT     F   COMMENT ON COLUMN public.txeggs_storage.eggs IS 'Cantidad de huevos';
            public       postgres    false    308            5           1259    110706    txgoals_erp    TABLE     �   CREATE TABLE public.txgoals_erp (
    goals_erp_id bigint NOT NULL,
    use_week date,
    use_value integer,
    product_id integer NOT NULL,
    code character varying(10),
    scenario_id integer NOT NULL
);
    DROP TABLE public.txgoals_erp;
       public         postgres    false    3            �           0    0    TABLE txgoals_erp    COMMENT     �   COMMENT ON TABLE public.txgoals_erp IS 'Almacena los datos generados de las metas de producción de la planificación regresiva para ser enviados al ERP';
            public       postgres    false    309            �           0    0    COLUMN txgoals_erp.goals_erp_id    COMMENT     N   COMMENT ON COLUMN public.txgoals_erp.goals_erp_id IS 'Id de la meta del ERP';
            public       postgres    false    309            �           0    0    COLUMN txgoals_erp.use_week    COMMENT     ;   COMMENT ON COLUMN public.txgoals_erp.use_week IS 'Semana';
            public       postgres    false    309            �           0    0    COLUMN txgoals_erp.use_value    COMMENT     D   COMMENT ON COLUMN public.txgoals_erp.use_value IS 'Valor objetivo';
            public       postgres    false    309            �           0    0    COLUMN txgoals_erp.product_id    COMMENT     F   COMMENT ON COLUMN public.txgoals_erp.product_id IS 'Id del producto';
            public       postgres    false    309            �           0    0    COLUMN txgoals_erp.code    COMMENT     D   COMMENT ON COLUMN public.txgoals_erp.code IS 'Codigo del producto';
            public       postgres    false    309            �           0    0    COLUMN txgoals_erp.scenario_id    COMMENT     H   COMMENT ON COLUMN public.txgoals_erp.scenario_id IS 'Id del escenario';
            public       postgres    false    309            6           1259    110709    txgoals_erp_goals_erp_id_seq    SEQUENCE     �   CREATE SEQUENCE public.txgoals_erp_goals_erp_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.txgoals_erp_goals_erp_id_seq;
       public       postgres    false    3    309            �           0    0    txgoals_erp_goals_erp_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.txgoals_erp_goals_erp_id_seq OWNED BY public.txgoals_erp.goals_erp_id;
            public       postgres    false    310            7           1259    110711    txhousingway    TABLE     d  CREATE TABLE public.txhousingway (
    housing_way_id integer DEFAULT nextval('public.housing_way_id_seq'::regclass) NOT NULL,
    projected_quantity integer,
    projected_date date,
    stage_id integer NOT NULL,
    partnership_id integer NOT NULL,
    scenario_id integer NOT NULL,
    breed_id integer NOT NULL,
    predecessor_id integer NOT NULL
);
     DROP TABLE public.txhousingway;
       public         postgres    false    237    3            �           0    0    TABLE txhousingway    COMMENT     t   COMMENT ON TABLE public.txhousingway IS 'Almacena la proyección de los módulos de levante, cría y reproductora';
            public       postgres    false    311            �           0    0 "   COLUMN txhousingway.housing_way_id    COMMENT     �   COMMENT ON COLUMN public.txhousingway.housing_way_id IS 'Id de las proyecciones  de los módulos de levante, cría y reproductora';
            public       postgres    false    311            �           0    0 &   COLUMN txhousingway.projected_quantity    COMMENT     S   COMMENT ON COLUMN public.txhousingway.projected_quantity IS 'Cantidad proyectada';
            public       postgres    false    311            �           0    0 "   COLUMN txhousingway.projected_date    COMMENT     L   COMMENT ON COLUMN public.txhousingway.projected_date IS 'Fecha proyectada';
            public       postgres    false    311            �           0    0    COLUMN txhousingway.stage_id    COMMENT     D   COMMENT ON COLUMN public.txhousingway.stage_id IS 'Id de la etapa';
            public       postgres    false    311            �           0    0 "   COLUMN txhousingway.partnership_id    COMMENT     L   COMMENT ON COLUMN public.txhousingway.partnership_id IS 'Id de la empresa';
            public       postgres    false    311            �           0    0    COLUMN txhousingway.breed_id    COMMENT     C   COMMENT ON COLUMN public.txhousingway.breed_id IS 'Id de la raza';
            public       postgres    false    311            �           0    0 "   COLUMN txhousingway.predecessor_id    COMMENT     N   COMMENT ON COLUMN public.txhousingway.predecessor_id IS 'Id del predecesor ';
            public       postgres    false    311            8           1259    110715    txhousingway_detail    TABLE     �  CREATE TABLE public.txhousingway_detail (
    housingway_detail_id integer DEFAULT nextval('public.housing_way_detail_id_seq'::regclass) NOT NULL,
    housing_way_id integer NOT NULL,
    scheduled_date date,
    scheduled_quantity integer,
    farm_id integer NOT NULL,
    shed_id integer NOT NULL,
    confirm integer,
    execution_date date,
    execution_quantity integer,
    lot character varying(45),
    incubator_plant_id integer
);
 '   DROP TABLE public.txhousingway_detail;
       public         postgres    false    236    3            �           0    0    TABLE txhousingway_detail    COMMENT     �   COMMENT ON TABLE public.txhousingway_detail IS 'Almacena la programación y la ejecución de los módulos de levante y cría y reproductora';
            public       postgres    false    312            �           0    0 /   COLUMN txhousingway_detail.housingway_detail_id    COMMENT     �   COMMENT ON COLUMN public.txhousingway_detail.housingway_detail_id IS 'Id de la programación y ejecución de los modelos de levante y cría y reproductora';
            public       postgres    false    312            �           0    0 )   COLUMN txhousingway_detail.housing_way_id    COMMENT     �   COMMENT ON COLUMN public.txhousingway_detail.housing_way_id IS 'Id de las proyecciones  de los módulos de levante, cría y reproductora';
            public       postgres    false    312            �           0    0 )   COLUMN txhousingway_detail.scheduled_date    COMMENT     S   COMMENT ON COLUMN public.txhousingway_detail.scheduled_date IS 'Fecha programada';
            public       postgres    false    312            �           0    0 -   COLUMN txhousingway_detail.scheduled_quantity    COMMENT     Z   COMMENT ON COLUMN public.txhousingway_detail.scheduled_quantity IS 'Cantidad programada';
            public       postgres    false    312            �           0    0 "   COLUMN txhousingway_detail.farm_id    COMMENT     K   COMMENT ON COLUMN public.txhousingway_detail.farm_id IS 'Id de la granja';
            public       postgres    false    312            �           0    0 "   COLUMN txhousingway_detail.shed_id    COMMENT     S   COMMENT ON COLUMN public.txhousingway_detail.shed_id IS 'Id del galpon utilizado';
            public       postgres    false    312            �           0    0 "   COLUMN txhousingway_detail.confirm    COMMENT     [   COMMENT ON COLUMN public.txhousingway_detail.confirm IS 'Confirmacion de sincronizacion ';
            public       postgres    false    312            �           0    0 )   COLUMN txhousingway_detail.execution_date    COMMENT     V   COMMENT ON COLUMN public.txhousingway_detail.execution_date IS 'Fecha de ejecución';
            public       postgres    false    312            �           0    0 -   COLUMN txhousingway_detail.execution_quantity    COMMENT     Z   COMMENT ON COLUMN public.txhousingway_detail.execution_quantity IS 'Cantidad a ejecutar';
            public       postgres    false    312            �           0    0    COLUMN txhousingway_detail.lot    COMMENT     I   COMMENT ON COLUMN public.txhousingway_detail.lot IS 'Lote seleccionado';
            public       postgres    false    312            �           0    0 -   COLUMN txhousingway_detail.incubator_plant_id    COMMENT     a   COMMENT ON COLUMN public.txhousingway_detail.incubator_plant_id IS 'Id de la planta incubadora';
            public       postgres    false    312            9           1259    110719    txlot    TABLE     n  CREATE TABLE public.txlot (
    lot_id integer DEFAULT nextval('public.lot_id_seq'::regclass) NOT NULL,
    lot_code character varying(20) NOT NULL,
    lot_origin character varying(150),
    status integer,
    proyected_date date,
    sheduled_date date,
    proyected_quantity integer,
    sheduled_quantity integer,
    released_quantity integer,
    product_id integer NOT NULL,
    breed_id integer NOT NULL,
    gender character varying(30),
    type_posture character varying(30),
    shed_id integer NOT NULL,
    origin character varying(30),
    farm_id integer NOT NULL,
    housing_way_id integer NOT NULL
);
    DROP TABLE public.txlot;
       public         postgres    false    244    3            �           0    0    TABLE txlot    COMMENT     T   COMMENT ON TABLE public.txlot IS 'Almacena la informacion de los diferentes lotes';
            public       postgres    false    313            �           0    0    COLUMN txlot.lot_id    COMMENT     8   COMMENT ON COLUMN public.txlot.lot_id IS 'Id del lote';
            public       postgres    false    313                        0    0    COLUMN txlot.lot_code    COMMENT     >   COMMENT ON COLUMN public.txlot.lot_code IS 'Codigo del lote';
            public       postgres    false    313                       0    0    COLUMN txlot.lot_origin    COMMENT     @   COMMENT ON COLUMN public.txlot.lot_origin IS 'Origen del lote';
            public       postgres    false    313                       0    0    COLUMN txlot.status    COMMENT     <   COMMENT ON COLUMN public.txlot.status IS 'Estado del lote';
            public       postgres    false    313                       0    0    COLUMN txlot.proyected_date    COMMENT     E   COMMENT ON COLUMN public.txlot.proyected_date IS 'Fecha proyectada';
            public       postgres    false    313                       0    0    COLUMN txlot.sheduled_date    COMMENT     D   COMMENT ON COLUMN public.txlot.sheduled_date IS 'Fecha programada';
            public       postgres    false    313                       0    0    COLUMN txlot.proyected_quantity    COMMENT     L   COMMENT ON COLUMN public.txlot.proyected_quantity IS 'Cantidad proyectada';
            public       postgres    false    313                       0    0    COLUMN txlot.sheduled_quantity    COMMENT     K   COMMENT ON COLUMN public.txlot.sheduled_quantity IS 'Cantidad programada';
            public       postgres    false    313                       0    0    COLUMN txlot.released_quantity    COMMENT     I   COMMENT ON COLUMN public.txlot.released_quantity IS 'Cantidad liberada';
            public       postgres    false    313                       0    0    COLUMN txlot.product_id    COMMENT     @   COMMENT ON COLUMN public.txlot.product_id IS 'Id del producto';
            public       postgres    false    313            	           0    0    COLUMN txlot.breed_id    COMMENT     <   COMMENT ON COLUMN public.txlot.breed_id IS 'Id de la raza';
            public       postgres    false    313            
           0    0    COLUMN txlot.gender    COMMENT     <   COMMENT ON COLUMN public.txlot.gender IS 'Genero del lote';
            public       postgres    false    313                       0    0    COLUMN txlot.type_posture    COMMENT     B   COMMENT ON COLUMN public.txlot.type_posture IS 'Tipo de postura';
            public       postgres    false    313                       0    0    COLUMN txlot.shed_id    COMMENT     ;   COMMENT ON COLUMN public.txlot.shed_id IS 'Id del galpon';
            public       postgres    false    313                       0    0    COLUMN txlot.origin    COMMENT     3   COMMENT ON COLUMN public.txlot.origin IS 'Origen';
            public       postgres    false    313                       0    0    COLUMN txlot.farm_id    COMMENT     =   COMMENT ON COLUMN public.txlot.farm_id IS 'Id de la granja';
            public       postgres    false    313                       0    0    COLUMN txlot.housing_way_id    COMMENT     ~   COMMENT ON COLUMN public.txlot.housing_way_id IS 'Id del almacenamientos de la proyecciones de levante, cria y reproductora';
            public       postgres    false    313            :           1259    110723 
   txlot_eggs    TABLE     �   CREATE TABLE public.txlot_eggs (
    lot_eggs_id integer DEFAULT nextval('public.lot_eggs_id_seq'::regclass) NOT NULL,
    theorical_performance double precision,
    week_date date,
    week integer
);
    DROP TABLE public.txlot_eggs;
       public         postgres    false    242    3                       0    0    TABLE txlot_eggs    COMMENT     S   COMMENT ON TABLE public.txlot_eggs IS 'Almacena los datos de los lotes de huevos';
            public       postgres    false    314                       0    0    COLUMN txlot_eggs.lot_eggs_id    COMMENT     L   COMMENT ON COLUMN public.txlot_eggs.lot_eggs_id IS 'Id del lote de huevos';
            public       postgres    false    314                       0    0 '   COLUMN txlot_eggs.theorical_performance    COMMENT     T   COMMENT ON COLUMN public.txlot_eggs.theorical_performance IS 'Rendimiento teorico';
            public       postgres    false    314                       0    0    COLUMN txlot_eggs.week_date    COMMENT     G   COMMENT ON COLUMN public.txlot_eggs.week_date IS 'Fecha de la semana';
            public       postgres    false    314                       0    0    COLUMN txlot_eggs.week    COMMENT     6   COMMENT ON COLUMN public.txlot_eggs.week IS 'Semana';
            public       postgres    false    314            ;           1259    110727    txposturecurve    TABLE     �  CREATE TABLE public.txposturecurve (
    posture_curve_id integer DEFAULT nextval('public.posture_curve_id_seq'::regclass) NOT NULL,
    week integer NOT NULL,
    breed_id integer NOT NULL,
    theorical_performance double precision NOT NULL,
    historical_performance double precision,
    theorical_accum_mortality integer,
    historical_accum_mortality integer,
    theorical_uniformity double precision,
    historical_uniformity double precision,
    type_posture character varying(30) NOT NULL
);
 "   DROP TABLE public.txposturecurve;
       public         postgres    false    287    3                       0    0    TABLE txposturecurve    COMMENT        COMMENT ON TABLE public.txposturecurve IS 'Almacena la información de la curva de postura por cada raza separada por semana';
            public       postgres    false    315                       0    0 &   COLUMN txposturecurve.posture_curve_id    COMMENT     Y   COMMENT ON COLUMN public.txposturecurve.posture_curve_id IS 'Id de la curva de postura';
            public       postgres    false    315                       0    0    COLUMN txposturecurve.week    COMMENT     _   COMMENT ON COLUMN public.txposturecurve.week IS 'Semana en la que inicia la curva de postura';
            public       postgres    false    315                       0    0    COLUMN txposturecurve.breed_id    COMMENT     P   COMMENT ON COLUMN public.txposturecurve.breed_id IS 'Identificador de la raza';
            public       postgres    false    315                       0    0 +   COLUMN txposturecurve.theorical_performance    COMMENT     X   COMMENT ON COLUMN public.txposturecurve.theorical_performance IS 'Desempeño teórico';
            public       postgres    false    315                       0    0 ,   COLUMN txposturecurve.historical_performance    COMMENT     [   COMMENT ON COLUMN public.txposturecurve.historical_performance IS 'Desempeño histórico';
            public       postgres    false    315                       0    0 /   COLUMN txposturecurve.theorical_accum_mortality    COMMENT     h   COMMENT ON COLUMN public.txposturecurve.theorical_accum_mortality IS 'Acumulado de mortalidad teorico';
            public       postgres    false    315                       0    0 0   COLUMN txposturecurve.historical_accum_mortality    COMMENT     k   COMMENT ON COLUMN public.txposturecurve.historical_accum_mortality IS 'Acumulado de mortalidad historico';
            public       postgres    false    315                       0    0 *   COLUMN txposturecurve.theorical_uniformity    COMMENT     W   COMMENT ON COLUMN public.txposturecurve.theorical_uniformity IS 'Uniformidad teorica';
            public       postgres    false    315                       0    0 +   COLUMN txposturecurve.historical_uniformity    COMMENT     Z   COMMENT ON COLUMN public.txposturecurve.historical_uniformity IS 'Uniformidad historica';
            public       postgres    false    315                       0    0 "   COLUMN txposturecurve.type_posture    COMMENT     K   COMMENT ON COLUMN public.txposturecurve.type_posture IS 'Tipo de postura';
            public       postgres    false    315            <           1259    110731    txprogrammed_eggs    TABLE     �  CREATE TABLE public.txprogrammed_eggs (
    programmed_eggs_id integer DEFAULT nextval('public.programmed_eggs_id_seq'::regclass) NOT NULL,
    incubator_id integer NOT NULL,
    lot_breed character varying(45),
    lot_incubator character varying(45),
    use_date date,
    eggs integer,
    breed_id integer NOT NULL,
    execution_quantity integer,
    eggs_storage_id integer NOT NULL,
    confirm integer,
    released boolean
);
 %   DROP TABLE public.txprogrammed_eggs;
       public         postgres    false    290    3                        0    0    TABLE txprogrammed_eggs    COMMENT        COMMENT ON TABLE public.txprogrammed_eggs IS 'Almacena la proyección, programación y ejecución del módulo de incubadoras';
            public       postgres    false    316            !           0    0 +   COLUMN txprogrammed_eggs.programmed_eggs_id    COMMENT     j   COMMENT ON COLUMN public.txprogrammed_eggs.programmed_eggs_id IS 'Id de las programacion de incubadoras';
            public       postgres    false    316            "           0    0 %   COLUMN txprogrammed_eggs.incubator_id    COMMENT     O   COMMENT ON COLUMN public.txprogrammed_eggs.incubator_id IS 'Id de incubadora';
            public       postgres    false    316            #           0    0 "   COLUMN txprogrammed_eggs.lot_breed    COMMENT     I   COMMENT ON COLUMN public.txprogrammed_eggs.lot_breed IS 'Lote por raza';
            public       postgres    false    316            $           0    0 &   COLUMN txprogrammed_eggs.lot_incubator    COMMENT     S   COMMENT ON COLUMN public.txprogrammed_eggs.lot_incubator IS 'Lote de incubadoras';
            public       postgres    false    316            %           0    0    COLUMN txprogrammed_eggs.eggs    COMMENT     I   COMMENT ON COLUMN public.txprogrammed_eggs.eggs IS 'Cantidad de huevos';
            public       postgres    false    316            &           0    0 !   COLUMN txprogrammed_eggs.breed_id    COMMENT     E   COMMENT ON COLUMN public.txprogrammed_eggs.breed_id IS 'Id de raza';
            public       postgres    false    316            '           0    0 +   COLUMN txprogrammed_eggs.execution_quantity    COMMENT     [   COMMENT ON COLUMN public.txprogrammed_eggs.execution_quantity IS 'Cantidad de ejecución';
            public       postgres    false    316            =           1259    110735    txscenarioformula    TABLE     �  CREATE TABLE public.txscenarioformula (
    scenario_formula_id integer DEFAULT nextval('public.scenario_formula_id_seq'::regclass) NOT NULL,
    process_id integer NOT NULL,
    predecessor_id integer NOT NULL,
    parameter_id integer NOT NULL,
    sign integer,
    divider double precision,
    duration integer,
    scenario_id integer NOT NULL,
    measure_id integer NOT NULL
);
 %   DROP TABLE public.txscenarioformula;
       public         postgres    false    292    3            (           0    0    TABLE txscenarioformula    COMMENT     �   COMMENT ON TABLE public.txscenarioformula IS 'Almacena los datos para la formulación de salida de la planificación regresiva';
            public       postgres    false    317            )           0    0 ,   COLUMN txscenarioformula.scenario_formula_id    COMMENT     d   COMMENT ON COLUMN public.txscenarioformula.scenario_formula_id IS 'Id de la formula del escenario';
            public       postgres    false    317            *           0    0 #   COLUMN txscenarioformula.process_id    COMMENT     K   COMMENT ON COLUMN public.txscenarioformula.process_id IS 'Id del proceso';
            public       postgres    false    317            +           0    0 '   COLUMN txscenarioformula.predecessor_id    COMMENT     R   COMMENT ON COLUMN public.txscenarioformula.predecessor_id IS 'Id del predecesor';
            public       postgres    false    317            ,           0    0 %   COLUMN txscenarioformula.parameter_id    COMMENT     O   COMMENT ON COLUMN public.txscenarioformula.parameter_id IS 'Id del parametro';
            public       postgres    false    317            -           0    0    COLUMN txscenarioformula.sign    COMMENT     E   COMMENT ON COLUMN public.txscenarioformula.sign IS 'Firma de datos';
            public       postgres    false    317            .           0    0     COLUMN txscenarioformula.divider    COMMENT     J   COMMENT ON COLUMN public.txscenarioformula.divider IS 'divisor de datos';
            public       postgres    false    317            /           0    0 !   COLUMN txscenarioformula.duration    COMMENT     Q   COMMENT ON COLUMN public.txscenarioformula.duration IS 'Duracion de la formula';
            public       postgres    false    317            0           0    0 $   COLUMN txscenarioformula.scenario_id    COMMENT     N   COMMENT ON COLUMN public.txscenarioformula.scenario_id IS 'Id del escenario';
            public       postgres    false    317            1           0    0 #   COLUMN txscenarioformula.measure_id    COMMENT     M   COMMENT ON COLUMN public.txscenarioformula.measure_id IS 'Id de la medida
';
            public       postgres    false    317            >           1259    110739    txscenarioparameter    TABLE     c  CREATE TABLE public.txscenarioparameter (
    scenario_parameter_id integer DEFAULT nextval('public.scenario_parameter_id_seq'::regclass) NOT NULL,
    process_id integer NOT NULL,
    parameter_id integer NOT NULL,
    use_year integer,
    use_month integer,
    use_value integer,
    scenario_id integer NOT NULL,
    value_units integer DEFAULT 0
);
 '   DROP TABLE public.txscenarioparameter;
       public         postgres    false    294    3            2           0    0    TABLE txscenarioparameter    COMMENT     s   COMMENT ON TABLE public.txscenarioparameter IS 'Almacena las metas de producción ingresadas para los escenarios';
            public       postgres    false    318            3           0    0 0   COLUMN txscenarioparameter.scenario_parameter_id    COMMENT     l   COMMENT ON COLUMN public.txscenarioparameter.scenario_parameter_id IS 'Id de los parametros del escenario';
            public       postgres    false    318            4           0    0 %   COLUMN txscenarioparameter.process_id    COMMENT     M   COMMENT ON COLUMN public.txscenarioparameter.process_id IS 'Id del proceso';
            public       postgres    false    318            5           0    0 '   COLUMN txscenarioparameter.parameter_id    COMMENT     Q   COMMENT ON COLUMN public.txscenarioparameter.parameter_id IS 'Id del parametro';
            public       postgres    false    318            6           0    0 #   COLUMN txscenarioparameter.use_year    COMMENT     O   COMMENT ON COLUMN public.txscenarioparameter.use_year IS 'Año del parametro';
            public       postgres    false    318            7           0    0 $   COLUMN txscenarioparameter.use_month    COMMENT     O   COMMENT ON COLUMN public.txscenarioparameter.use_month IS 'Mes del parametro';
            public       postgres    false    318            8           0    0 $   COLUMN txscenarioparameter.use_value    COMMENT     Q   COMMENT ON COLUMN public.txscenarioparameter.use_value IS 'Valor del parametro';
            public       postgres    false    318            9           0    0 &   COLUMN txscenarioparameter.scenario_id    COMMENT     P   COMMENT ON COLUMN public.txscenarioparameter.scenario_id IS 'Id del escenario';
            public       postgres    false    318            :           0    0 &   COLUMN txscenarioparameter.value_units    COMMENT     U   COMMENT ON COLUMN public.txscenarioparameter.value_units IS 'Valor de las unidades';
            public       postgres    false    318            ?           1259    110744    txscenarioparameterday    TABLE     {  CREATE TABLE public.txscenarioparameterday (
    scenario_parameter_day_id integer DEFAULT nextval('public.scenario_parameter_day_seq'::regclass) NOT NULL,
    use_day integer,
    parameter_id integer NOT NULL,
    units_day integer,
    scenario_id integer NOT NULL,
    sequence integer,
    use_month integer,
    use_year integer,
    week_day integer,
    use_week date
);
 *   DROP TABLE public.txscenarioparameterday;
       public         postgres    false    293    3            ;           0    0    TABLE txscenarioparameterday    COMMENT     V   COMMENT ON TABLE public.txscenarioparameterday IS 'Almcacena los parametros por dia';
            public       postgres    false    319            <           0    0 7   COLUMN txscenarioparameterday.scenario_parameter_day_id    COMMENT     m   COMMENT ON COLUMN public.txscenarioparameterday.scenario_parameter_day_id IS 'Id de los parametros del dia';
            public       postgres    false    319            =           0    0 %   COLUMN txscenarioparameterday.use_day    COMMENT     B   COMMENT ON COLUMN public.txscenarioparameterday.use_day IS 'Dia';
            public       postgres    false    319            >           0    0 *   COLUMN txscenarioparameterday.parameter_id    COMMENT     c   COMMENT ON COLUMN public.txscenarioparameterday.parameter_id IS 'Id de los parametros necesarios';
            public       postgres    false    319            ?           0    0 '   COLUMN txscenarioparameterday.units_day    COMMENT     U   COMMENT ON COLUMN public.txscenarioparameterday.units_day IS 'Cantidad de material';
            public       postgres    false    319            @           0    0 )   COLUMN txscenarioparameterday.scenario_id    COMMENT     u   COMMENT ON COLUMN public.txscenarioparameterday.scenario_id IS 'Escenario al cual pertenece el scanrioparameterday';
            public       postgres    false    319            A           0    0 &   COLUMN txscenarioparameterday.sequence    COMMENT     R   COMMENT ON COLUMN public.txscenarioparameterday.sequence IS 'Secuencia del dia
';
            public       postgres    false    319            B           0    0 '   COLUMN txscenarioparameterday.use_month    COMMENT     ]   COMMENT ON COLUMN public.txscenarioparameterday.use_month IS 'Mes en que se ubica el día ';
            public       postgres    false    319            C           0    0 &   COLUMN txscenarioparameterday.use_year    COMMENT     ]   COMMENT ON COLUMN public.txscenarioparameterday.use_year IS 'Año en que se ubica el día ';
            public       postgres    false    319            D           0    0 &   COLUMN txscenarioparameterday.week_day    COMMENT     P   COMMENT ON COLUMN public.txscenarioparameterday.week_day IS 'Dia de la semana';
            public       postgres    false    319            E           0    0 &   COLUMN txscenarioparameterday.use_week    COMMENT     F   COMMENT ON COLUMN public.txscenarioparameterday.use_week IS 'Semana';
            public       postgres    false    319            @           1259    110748    txscenarioposturecurve    TABLE     3  CREATE TABLE public.txscenarioposturecurve (
    scenario_posture_id integer DEFAULT nextval('public.scenario_posture_id_seq'::regclass) NOT NULL,
    posture_date date,
    eggs double precision,
    scenario_id integer NOT NULL,
    housingway_detail_id integer NOT NULL,
    breed_id integer NOT NULL
);
 *   DROP TABLE public.txscenarioposturecurve;
       public         postgres    false    295    3            F           0    0    TABLE txscenarioposturecurve    COMMENT     o   COMMENT ON TABLE public.txscenarioposturecurve IS 'Almacena los datos que se utilizan en la curva de postura';
            public       postgres    false    320            G           0    0 1   COLUMN txscenarioposturecurve.scenario_posture_id    COMMENT     i   COMMENT ON COLUMN public.txscenarioposturecurve.scenario_posture_id IS 'Id de la postura del escenario';
            public       postgres    false    320            H           0    0 *   COLUMN txscenarioposturecurve.posture_date    COMMENT     W   COMMENT ON COLUMN public.txscenarioposturecurve.posture_date IS 'Fecha de la postura';
            public       postgres    false    320            I           0    0 "   COLUMN txscenarioposturecurve.eggs    COMMENT     N   COMMENT ON COLUMN public.txscenarioposturecurve.eggs IS 'Cantidad de huevos';
            public       postgres    false    320            J           0    0 )   COLUMN txscenarioposturecurve.scenario_id    COMMENT     R   COMMENT ON COLUMN public.txscenarioposturecurve.scenario_id IS 'Id del scenario';
            public       postgres    false    320            K           0    0 2   COLUMN txscenarioposturecurve.housingway_detail_id    COMMENT     �   COMMENT ON COLUMN public.txscenarioposturecurve.housingway_detail_id IS 'Id de la programación y ejecución de los modelos de levante y cría y reproductora';
            public       postgres    false    320            L           0    0 &   COLUMN txscenarioposturecurve.breed_id    COMMENT     M   COMMENT ON COLUMN public.txscenarioposturecurve.breed_id IS 'Id de la raza';
            public       postgres    false    320            A           1259    110752    txscenarioprocess    TABLE     4  CREATE TABLE public.txscenarioprocess (
    scenario_process_id integer DEFAULT nextval('public.scenario_process_id_seq'::regclass) NOT NULL,
    process_id integer NOT NULL,
    decrease_goal double precision,
    weight_goal double precision,
    duration_goal integer,
    scenario_id integer NOT NULL
);
 %   DROP TABLE public.txscenarioprocess;
       public         postgres    false    296    3            M           0    0    TABLE txscenarioprocess    COMMENT     m   COMMENT ON TABLE public.txscenarioprocess IS 'Almacena los procesos asociados a cada uno de los escenarios';
            public       postgres    false    321            N           0    0 ,   COLUMN txscenarioprocess.scenario_process_id    COMMENT     a   COMMENT ON COLUMN public.txscenarioprocess.scenario_process_id IS 'Id del proceso de escenario';
            public       postgres    false    321            O           0    0 #   COLUMN txscenarioprocess.process_id    COMMENT     V   COMMENT ON COLUMN public.txscenarioprocess.process_id IS 'Id del proceso a utilizar';
            public       postgres    false    321            P           0    0 &   COLUMN txscenarioprocess.decrease_goal    COMMENT     v   COMMENT ON COLUMN public.txscenarioprocess.decrease_goal IS 'Guarda los datos de la merma historia en dicho proceso';
            public       postgres    false    321            Q           0    0 $   COLUMN txscenarioprocess.weight_goal    COMMENT     q   COMMENT ON COLUMN public.txscenarioprocess.weight_goal IS 'Guarda los datos del peso historio en dicho proceso';
            public       postgres    false    321            R           0    0 &   COLUMN txscenarioprocess.duration_goal    COMMENT     y   COMMENT ON COLUMN public.txscenarioprocess.duration_goal IS 'Guarda los datos de la duracion historia en dicho proceso';
            public       postgres    false    321            S           0    0 $   COLUMN txscenarioprocess.scenario_id    COMMENT     X   COMMENT ON COLUMN public.txscenarioprocess.scenario_id IS 'Id del escenario utilizado';
            public       postgres    false    321            B           1259    110756 #   user_application_application_id_seq    SEQUENCE     �   CREATE SEQUENCE public.user_application_application_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 :   DROP SEQUENCE public.user_application_application_id_seq;
       public       postgres    false    3            C           1259    110758     user_application_user_app_id_seq    SEQUENCE     �   CREATE SEQUENCE public.user_application_user_app_id_seq
    START WITH 215
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 7   DROP SEQUENCE public.user_application_user_app_id_seq;
       public       postgres    false    3            D           1259    110760    user_application_user_id_seq    SEQUENCE     �   CREATE SEQUENCE public.user_application_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 3   DROP SEQUENCE public.user_application_user_id_seq;
       public       postgres    false    3                       2604    110762    txgoals_erp goals_erp_id    DEFAULT     �   ALTER TABLE ONLY public.txgoals_erp ALTER COLUMN goals_erp_id SET DEFAULT nextval('public.txgoals_erp_goals_erp_id_seq'::regclass);
 G   ALTER TABLE public.txgoals_erp ALTER COLUMN goals_erp_id DROP DEFAULT;
       public       postgres    false    310    309                      0    110374    aba_breeds_and_stages 
   TABLE DATA               m   COPY public.aba_breeds_and_stages (id, code, name, id_aba_consumption_and_mortality, id_process) FROM stdin;
    public       postgres    false    198   ��                0    110380    aba_consumption_and_mortality 
   TABLE DATA               m   COPY public.aba_consumption_and_mortality (id, code, name, id_breed, id_stage, id_aba_time_unit) FROM stdin;
    public       postgres    false    200   �                0    110386 $   aba_consumption_and_mortality_detail 
   TABLE DATA               �   COPY public.aba_consumption_and_mortality_detail (id, id_aba_consumption_and_mortality, time_unit_number, consumption, mortality) FROM stdin;
    public       postgres    false    202   6�                0    110392    aba_elements 
   TABLE DATA               d   COPY public.aba_elements (id, code, name, id_aba_element_property, equivalent_quantity) FROM stdin;
    public       postgres    false    204   S�                0    110398    aba_elements_and_concentrations 
   TABLE DATA               �   COPY public.aba_elements_and_concentrations (id, id_aba_element, id_aba_formulation, proportion, id_element_equivalent, id_aba_element_property, equivalent_quantity) FROM stdin;
    public       postgres    false    206   p�                0    110404    aba_elements_properties 
   TABLE DATA               A   COPY public.aba_elements_properties (id, code, name) FROM stdin;
    public       postgres    false    208   ��                0    110410    aba_formulation 
   TABLE DATA               9   COPY public.aba_formulation (id, code, name) FROM stdin;
    public       postgres    false    210   ѧ                0    110416    aba_results 
   TABLE DATA               C   COPY public.aba_results (id, id_aba_element, quantity) FROM stdin;
    public       postgres    false    212   �                0    110422    aba_stages_of_breeds_and_stages 
   TABLE DATA               w   COPY public.aba_stages_of_breeds_and_stages (id, id_aba_breeds_and_stages, id_formulation, name, duration) FROM stdin;
    public       postgres    false    214   �                0    110426    aba_time_unit 
   TABLE DATA               G   COPY public.aba_time_unit (id, singular_name, plural_name) FROM stdin;
    public       postgres    false    215   (�      <          0    110492    mdapplication 
   TABLE DATA               O   COPY public.mdapplication (application_id, application_name, type) FROM stdin;
    public       postgres    false    247   Z�      >          0    110498    mdapplication_rol 
   TABLE DATA               G   COPY public.mdapplication_rol (id, application_id, rol_id) FROM stdin;
    public       postgres    false    249   =�      ?          0    110502    mdbreed 
   TABLE DATA               7   COPY public.mdbreed (breed_id, code, name) FROM stdin;
    public       postgres    false    250   ��      @          0    110506    mdbroiler_product 
   TABLE DATA               \   COPY public.mdbroiler_product (broiler_product_id, name, days_eviction, weight) FROM stdin;
    public       postgres    false    251   ©      A          0    110510 
   mdfarmtype 
   TABLE DATA               8   COPY public.mdfarmtype (farm_type_id, name) FROM stdin;
    public       postgres    false    252   ��      C          0    110516 	   mdmeasure 
   TABLE DATA               b   COPY public.mdmeasure (measure_id, name, abbreviation, originvalue, valuekg, is_unit) FROM stdin;
    public       postgres    false    254   F�      E          0    110522    mdparameter 
   TABLE DATA               d   COPY public.mdparameter (parameter_id, description, type, measure_id, process_id, name) FROM stdin;
    public       postgres    false    256   ��      G          0    110531 	   mdprocess 
   TABLE DATA               J  COPY public.mdprocess (process_id, process_order, product_id, stage_id, historical_decrease, theoretical_decrease, historical_weight, theoretical_weight, historical_duration, theoretical_duration, calendar_id, visible, name, predecessor_id, capacity, breed_id, gender, fattening_goal, type_posture, biological_active) FROM stdin;
    public       postgres    false    258   �      I          0    110537 	   mdproduct 
   TABLE DATA               ;   COPY public.mdproduct (product_id, code, name) FROM stdin;
    public       postgres    false    260   �      K          0    110543    mdrol 
   TABLE DATA               T   COPY public.mdrol (rol_id, rol_name, admin_user_creator, creation_date) FROM stdin;
    public       postgres    false    262   j�      M          0    110549 
   mdscenario 
   TABLE DATA               o   COPY public.mdscenario (scenario_id, description, date_start, date_end, name, status, calendar_id) FROM stdin;
    public       postgres    false    264   �      O          0    110559    mdshedstatus 
   TABLE DATA               I   COPY public.mdshedstatus (shed_status_id, name, description) FROM stdin;
    public       postgres    false    266   l�      Q          0    110565    mdstage 
   TABLE DATA               9   COPY public.mdstage (stage_id, order_, name) FROM stdin;
    public       postgres    false    268   ϭ      S          0    110571    mduser 
   TABLE DATA                  COPY public.mduser (user_id, username, password, name, lastname, active, admi_user_creator, rol_id, creation_date) FROM stdin;
    public       postgres    false    270   -�      T          0    110575    oscenter 
   TABLE DATA               R   COPY public.oscenter (center_id, partnership_id, farm_id, name, code) FROM stdin;
    public       postgres    false    271   ��      U          0    110579    oscenter_oswarehouse 
   TABLE DATA               x   COPY public.oscenter_oswarehouse (client_id, partnership_id, farm_id, center_id, warehouse_id, delete_mark) FROM stdin;
    public       postgres    false    272   ��      V          0    110582    osfarm 
   TABLE DATA               S   COPY public.osfarm (farm_id, partnership_id, code, name, farm_type_id) FROM stdin;
    public       postgres    false    273   ��      W          0    110586    osincubator 
   TABLE DATA               �   COPY public.osincubator (incubator_id, incubator_plant_id, name, code, description, capacity, sunday, monday, tuesday, wednesday, thursday, friday, saturday, available) FROM stdin;
    public       postgres    false    274   e�      X          0    110590    osincubatorplant 
   TABLE DATA               �   COPY public.osincubatorplant (incubator_plant_id, name, code, description, partnership_id, max_storage, min_storage) FROM stdin;
    public       postgres    false    275   ް      Z          0    110596    ospartnership 
   TABLE DATA               Y   COPY public.ospartnership (partnership_id, name, address, description, code) FROM stdin;
    public       postgres    false    277   "�      \          0    110605    osshed 
   TABLE DATA               &  COPY public.osshed (shed_id, partnership_id, farm_id, center_id, code, statusshed_id, type_id, building_date, stall_width, stall_height, capacity_min, capacity_max, environment_id, rotation_days, nests_quantity, cages_quantity, birds_quantity, capacity_theoretical, avaliable_date) FROM stdin;
    public       postgres    false    279   ϱ      ^          0    110616    ossilo 
   TABLE DATA               ?  COPY public.ossilo (silo_id, client_id, partnership_id, farm_id, center_id, name, rings_height, rings_height_id, height, height_unit_id, diameter, diameter_unit_id, total_rings_quantity, measuring_mechanism, cone_degrees, total_capacity_1, total_capacity_2, capacity_unit_id_1, capacity_unit_id_2, central) FROM stdin;
    public       postgres    false    281   `�      _          0    110620    ossilo_osshed 
   TABLE DATA               v   COPY public.ossilo_osshed (silo_id, shed_id, center_id, farm_id, partnership_id, client_id, deleted_mark) FROM stdin;
    public       postgres    false    282   }�      a          0    110625    osslaughterhouse 
   TABLE DATA               i   COPY public.osslaughterhouse (slaughterhouse_id, name, address, description, code, capacity) FROM stdin;
    public       postgres    false    284   ��      c          0    110634    oswarehouse 
   TABLE DATA               X   COPY public.oswarehouse (warehouse_id, partnership_id, farm_id, name, code) FROM stdin;
    public       postgres    false    286   �      n          0    110658    txavailabilitysheds 
   TABLE DATA               k   COPY public.txavailabilitysheds (availability_shed_id, shed_id, init_date, end_date, lot_code) FROM stdin;
    public       postgres    false    297   z�      o          0    110662 	   txbroiler 
   TABLE DATA               �   COPY public.txbroiler (broiler_id, projected_date, projected_quantity, partnership_id, scenario_id, breed_id, lot_incubator, programmed_eggs_id) FROM stdin;
    public       postgres    false    298   ��      p          0    110666    txbroiler_detail 
   TABLE DATA               �   COPY public.txbroiler_detail (broiler_detail_id, broiler_id, scheduled_date, scheduled_quantity, farm_id, shed_id, confirm, execution_date, execution_quantity, lot, broiler_product_id) FROM stdin;
    public       postgres    false    299   �      q          0    110670    txbroilereviction 
   TABLE DATA               �   COPY public.txbroilereviction (broilereviction_id, projected_date, projected_quantity, partnership_id, scenario_id, breed_id, lot_incubator, broiler_detail_id) FROM stdin;
    public       postgres    false    300   K�      r          0    110674    txbroilereviction_detail 
   TABLE DATA               �   COPY public.txbroilereviction_detail (broilereviction_detail_id, broilereviction_id, scheduled_date, scheduled_quantity, farm_id, shed_id, confirm, execution_date, execution_quantity, lot, broiler_product_id, slaughterhouse_id) FROM stdin;
    public       postgres    false    301   ��      s          0    110678    txbroilerproduct_detail 
   TABLE DATA               y   COPY public.txbroilerproduct_detail (broilerproduct_detail_id, broiler_detail, broiler_product_id, quantity) FROM stdin;
    public       postgres    false    302   
�      t          0    110682    txbroodermachine 
   TABLE DATA               �   COPY public.txbroodermachine (brooder_machine_id_seq, partnership_id, farm_id, capacity, sunday, monday, tuesday, wednesday, thursday, friday, saturday, name) FROM stdin;
    public       postgres    false    303   '�      u          0    110686 
   txcalendar 
   TABLE DATA               �   COPY public.txcalendar (calendar_id, description, saturday, sunday, week_start, code, year_start, year_end, generated) FROM stdin;
    public       postgres    false    304   D�      v          0    110690    txcalendarday 
   TABLE DATA               �   COPY public.txcalendarday (calendar_day_id, calendar_id, use_date, use_year, use_month, use_day, use_week, week_day, sequence, working_day) FROM stdin;
    public       postgres    false    305   ��      w          0    110694    txeggs_planning 
   TABLE DATA               y   COPY public.txeggs_planning (egg_planning_id, month_planning, year_planning, scenario_id, planned, breed_id) FROM stdin;
    public       postgres    false    306   �x      x          0    110698    txeggs_required 
   TABLE DATA               p   COPY public.txeggs_required (egg_required_id, use_month, use_year, scenario_id, required, breed_id) FROM stdin;
    public       postgres    false    307   y      y          0    110702    txeggs_storage 
   TABLE DATA               �   COPY public.txeggs_storage (eggs_storage_id, incubator_plant_id, scenario_id, breed_id, init_date, end_date, lot, eggs) FROM stdin;
    public       postgres    false    308   0z      z          0    110706    txgoals_erp 
   TABLE DATA               g   COPY public.txgoals_erp (goals_erp_id, use_week, use_value, product_id, code, scenario_id) FROM stdin;
    public       postgres    false    309   �      |          0    110711    txhousingway 
   TABLE DATA               �   COPY public.txhousingway (housing_way_id, projected_quantity, projected_date, stage_id, partnership_id, scenario_id, breed_id, predecessor_id) FROM stdin;
    public       postgres    false    311   �      }          0    110715    txhousingway_detail 
   TABLE DATA               �   COPY public.txhousingway_detail (housingway_detail_id, housing_way_id, scheduled_date, scheduled_quantity, farm_id, shed_id, confirm, execution_date, execution_quantity, lot, incubator_plant_id) FROM stdin;
    public       postgres    false    312   a�      ~          0    110719    txlot 
   TABLE DATA               �   COPY public.txlot (lot_id, lot_code, lot_origin, status, proyected_date, sheduled_date, proyected_quantity, sheduled_quantity, released_quantity, product_id, breed_id, gender, type_posture, shed_id, origin, farm_id, housing_way_id) FROM stdin;
    public       postgres    false    313   ��                0    110723 
   txlot_eggs 
   TABLE DATA               Y   COPY public.txlot_eggs (lot_eggs_id, theorical_performance, week_date, week) FROM stdin;
    public       postgres    false    314   ؃      �          0    110727    txposturecurve 
   TABLE DATA               �   COPY public.txposturecurve (posture_curve_id, week, breed_id, theorical_performance, historical_performance, theorical_accum_mortality, historical_accum_mortality, theorical_uniformity, historical_uniformity, type_posture) FROM stdin;
    public       postgres    false    315   ��      �          0    110731    txprogrammed_eggs 
   TABLE DATA               �   COPY public.txprogrammed_eggs (programmed_eggs_id, incubator_id, lot_breed, lot_incubator, use_date, eggs, breed_id, execution_quantity, eggs_storage_id, confirm, released) FROM stdin;
    public       postgres    false    316   L�      �          0    110735    txscenarioformula 
   TABLE DATA               �   COPY public.txscenarioformula (scenario_formula_id, process_id, predecessor_id, parameter_id, sign, divider, duration, scenario_id, measure_id) FROM stdin;
    public       postgres    false    317   ��      �          0    110739    txscenarioparameter 
   TABLE DATA               �   COPY public.txscenarioparameter (scenario_parameter_id, process_id, parameter_id, use_year, use_month, use_value, scenario_id, value_units) FROM stdin;
    public       postgres    false    318   E�      �          0    110744    txscenarioparameterday 
   TABLE DATA               �   COPY public.txscenarioparameterday (scenario_parameter_day_id, use_day, parameter_id, units_day, scenario_id, sequence, use_month, use_year, week_day, use_week) FROM stdin;
    public       postgres    false    319   O�      �          0    110748    txscenarioposturecurve 
   TABLE DATA               �   COPY public.txscenarioposturecurve (scenario_posture_id, posture_date, eggs, scenario_id, housingway_detail_id, breed_id) FROM stdin;
    public       postgres    false    320   �      �          0    110752    txscenarioprocess 
   TABLE DATA               �   COPY public.txscenarioprocess (scenario_process_id, process_id, decrease_goal, weight_goal, duration_goal, scenario_id) FROM stdin;
    public       postgres    false    321   $�      T           0    0    abaTimeUnit_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public."abaTimeUnit_id_seq"', 2, false);
            public       postgres    false    196            U           0    0    aba_breeds_and_stages_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.aba_breeds_and_stages_id_seq', 8, false);
            public       postgres    false    197            V           0    0 +   aba_consumption_and_mortality_detail_id_seq    SEQUENCE SET     \   SELECT pg_catalog.setval('public.aba_consumption_and_mortality_detail_id_seq', 203, false);
            public       postgres    false    201            W           0    0 $   aba_consumption_and_mortality_id_seq    SEQUENCE SET     S   SELECT pg_catalog.setval('public.aba_consumption_and_mortality_id_seq', 8, false);
            public       postgres    false    199            X           0    0 &   aba_elements_and_concentrations_id_seq    SEQUENCE SET     V   SELECT pg_catalog.setval('public.aba_elements_and_concentrations_id_seq', 105, true);
            public       postgres    false    205            Y           0    0    aba_elements_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.aba_elements_id_seq', 22, true);
            public       postgres    false    203            Z           0    0    aba_elements_properties_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.aba_elements_properties_id_seq', 1, false);
            public       postgres    false    207            [           0    0    aba_formulation_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.aba_formulation_id_seq', 68, true);
            public       postgres    false    209            \           0    0    aba_results_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.aba_results_id_seq', 1, false);
            public       postgres    false    211            ]           0    0 &   aba_stages_of_breeds_and_stages_id_seq    SEQUENCE SET     V   SELECT pg_catalog.setval('public.aba_stages_of_breeds_and_stages_id_seq', 24, false);
            public       postgres    false    213            ^           0    0    availability_shed_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.availability_shed_id_seq', 301, true);
            public       postgres    false    216            _           0    0    base_day_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.base_day_id_seq', 3, true);
            public       postgres    false    217            `           0    0    breed_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.breed_id_seq', 18, true);
            public       postgres    false    218            a           0    0    broiler_detail_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.broiler_detail_id_seq', 89, true);
            public       postgres    false    219            b           0    0    broiler_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.broiler_id_seq', 109, true);
            public       postgres    false    220            c           0    0    broiler_product_detail_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.broiler_product_detail_id_seq', 2, true);
            public       postgres    false    221            d           0    0    broiler_product_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.broiler_product_id_seq', 22, true);
            public       postgres    false    222            e           0    0    broilereviction_detail_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.broilereviction_detail_id_seq', 198, true);
            public       postgres    false    223            f           0    0    broilereviction_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.broilereviction_id_seq', 147, true);
            public       postgres    false    224            g           0    0    brooder_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.brooder_id_seq', 52, true);
            public       postgres    false    225            h           0    0    brooder_machines_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.brooder_machines_id_seq', 7, true);
            public       postgres    false    226            i           0    0    calendar_day_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.calendar_day_id_seq', 25565, true);
            public       postgres    false    227            j           0    0    calendar_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.calendar_id_seq', 19, true);
            public       postgres    false    228            k           0    0    center_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.center_id_seq', 170, true);
            public       postgres    false    229            l           0    0    egg_planning_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.egg_planning_id_seq', 3152, true);
            public       postgres    false    230            m           0    0    egg_required_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.egg_required_id_seq', 3429, true);
            public       postgres    false    231            n           0    0    eggs_storage_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.eggs_storage_id_seq', 34567, true);
            public       postgres    false    232            o           0    0    farm_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.farm_id_seq', 176, true);
            public       postgres    false    233            p           0    0    farm_type_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.farm_type_id_seq', 3, true);
            public       postgres    false    234            q           0    0    holiday_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.holiday_id_seq', 297, true);
            public       postgres    false    235            r           0    0    housing_way_detail_id_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.housing_way_detail_id_seq', 641, true);
            public       postgres    false    236            s           0    0    housing_way_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.housing_way_id_seq', 843, true);
            public       postgres    false    237            t           0    0    incubator_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.incubator_id_seq', 29, true);
            public       postgres    false    238            u           0    0    incubator_plant_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.incubator_plant_id_seq', 20, true);
            public       postgres    false    239            v           0    0    industry_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.industry_id_seq', 1, true);
            public       postgres    false    240            w           0    0    line_id_seq    SEQUENCE SET     9   SELECT pg_catalog.setval('public.line_id_seq', 1, true);
            public       postgres    false    241            x           0    0    lot_eggs_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.lot_eggs_id_seq', 108, true);
            public       postgres    false    242            y           0    0    lot_fattening_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.lot_fattening_id_seq', 1, false);
            public       postgres    false    243            z           0    0 
   lot_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.lot_id_seq', 316, true);
            public       postgres    false    244            {           0    0    lot_liftbreeding_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.lot_liftbreeding_id_seq', 4, true);
            public       postgres    false    245            |           0    0     mdapplication_application_id_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('public.mdapplication_application_id_seq', 1, false);
            public       postgres    false    246            }           0    0    mdapplication_rol_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.mdapplication_rol_id_seq', 17, true);
            public       postgres    false    248            ~           0    0    mdrol_rol_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.mdrol_rol_id_seq', 3, true);
            public       postgres    false    261                       0    0    mduser_user_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.mduser_user_id_seq', 1, false);
            public       postgres    false    269            �           0    0    measure_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.measure_id_seq', 19, true);
            public       postgres    false    253            �           0    0    parameter_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.parameter_id_seq', 58, true);
            public       postgres    false    255            �           0    0    partnership_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.partnership_id_seq', 33, true);
            public       postgres    false    276            �           0    0    posture_curve_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.posture_curve_id_seq', 365, true);
            public       postgres    false    287            �           0    0    predecessor_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.predecessor_id_seq', 13, true);
            public       postgres    false    288            �           0    0    process_class_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.process_class_id_seq', 5, true);
            public       postgres    false    289            �           0    0    process_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.process_id_seq', 94, true);
            public       postgres    false    257            �           0    0    product_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.product_id_seq', 99, true);
            public       postgres    false    259            �           0    0    programmed_eggs_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.programmed_eggs_id_seq', 348, true);
            public       postgres    false    290            �           0    0    raspberry_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.raspberry_id_seq', 5, true);
            public       postgres    false    291            �           0    0    scenario_formula_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.scenario_formula_id_seq', 1302, true);
            public       postgres    false    292            �           0    0    scenario_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.scenario_id_seq', 172, true);
            public       postgres    false    263            �           0    0    scenario_parameter_day_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.scenario_parameter_day_seq', 57418, true);
            public       postgres    false    293            �           0    0    scenario_parameter_id_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('public.scenario_parameter_id_seq', 6485, true);
            public       postgres    false    294            �           0    0    scenario_posture_id_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.scenario_posture_id_seq', 59674, true);
            public       postgres    false    295            �           0    0    scenario_process_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.scenario_process_id_seq', 843, true);
            public       postgres    false    296            �           0    0    shed_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.shed_id_seq', 370, true);
            public       postgres    false    278            �           0    0    silo_id_seq    SEQUENCE SET     9   SELECT pg_catalog.setval('public.silo_id_seq', 4, true);
            public       postgres    false    280            �           0    0    slaughterhouse_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.slaughterhouse_id_seq', 37, true);
            public       postgres    false    283            �           0    0    stage_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.stage_id_seq', 27, true);
            public       postgres    false    267            �           0    0    status_shed_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.status_shed_id_seq', 10, true);
            public       postgres    false    265            �           0    0    txgoals_erp_goals_erp_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.txgoals_erp_goals_erp_id_seq', 1920, true);
            public       postgres    false    310            �           0    0 #   user_application_application_id_seq    SEQUENCE SET     R   SELECT pg_catalog.setval('public.user_application_application_id_seq', 1, false);
            public       postgres    false    322            �           0    0     user_application_user_app_id_seq    SEQUENCE SET     P   SELECT pg_catalog.setval('public.user_application_user_app_id_seq', 215, true);
            public       postgres    false    323            �           0    0    user_application_user_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.user_application_user_id_seq', 1, false);
            public       postgres    false    324            �           0    0    warehouse_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.warehouse_id_seq', 135, true);
            public       postgres    false    285            C           2606    110764    aba_time_unit abaTimeUnit_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.aba_time_unit
    ADD CONSTRAINT "abaTimeUnit_pkey" PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.aba_time_unit DROP CONSTRAINT "abaTimeUnit_pkey";
       public         postgres    false    215            %           2606    110766 0   aba_breeds_and_stages aba_breeds_and_stages_pkey 
   CONSTRAINT     n   ALTER TABLE ONLY public.aba_breeds_and_stages
    ADD CONSTRAINT aba_breeds_and_stages_pkey PRIMARY KEY (id);
 Z   ALTER TABLE ONLY public.aba_breeds_and_stages DROP CONSTRAINT aba_breeds_and_stages_pkey;
       public         postgres    false    198            .           2606    110768 N   aba_consumption_and_mortality_detail aba_consumption_and_mortality_detail_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.aba_consumption_and_mortality_detail
    ADD CONSTRAINT aba_consumption_and_mortality_detail_pkey PRIMARY KEY (id);
 x   ALTER TABLE ONLY public.aba_consumption_and_mortality_detail DROP CONSTRAINT aba_consumption_and_mortality_detail_pkey;
       public         postgres    false    202            )           2606    110770 @   aba_consumption_and_mortality aba_consumption_and_mortality_pkey 
   CONSTRAINT     ~   ALTER TABLE ONLY public.aba_consumption_and_mortality
    ADD CONSTRAINT aba_consumption_and_mortality_pkey PRIMARY KEY (id);
 j   ALTER TABLE ONLY public.aba_consumption_and_mortality DROP CONSTRAINT aba_consumption_and_mortality_pkey;
       public         postgres    false    200            3           2606    110772 D   aba_elements_and_concentrations aba_elements_and_concentrations_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.aba_elements_and_concentrations
    ADD CONSTRAINT aba_elements_and_concentrations_pkey PRIMARY KEY (id);
 n   ALTER TABLE ONLY public.aba_elements_and_concentrations DROP CONSTRAINT aba_elements_and_concentrations_pkey;
       public         postgres    false    206            1           2606    110774    aba_elements aba_elements_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.aba_elements
    ADD CONSTRAINT aba_elements_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.aba_elements DROP CONSTRAINT aba_elements_pkey;
       public         postgres    false    204            8           2606    110776 4   aba_elements_properties aba_elements_properties_pkey 
   CONSTRAINT     r   ALTER TABLE ONLY public.aba_elements_properties
    ADD CONSTRAINT aba_elements_properties_pkey PRIMARY KEY (id);
 ^   ALTER TABLE ONLY public.aba_elements_properties DROP CONSTRAINT aba_elements_properties_pkey;
       public         postgres    false    208            :           2606    110778 $   aba_formulation aba_formulation_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.aba_formulation
    ADD CONSTRAINT aba_formulation_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.aba_formulation DROP CONSTRAINT aba_formulation_pkey;
       public         postgres    false    210            <           2606    110780    aba_results aba_results_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.aba_results
    ADD CONSTRAINT aba_results_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.aba_results DROP CONSTRAINT aba_results_pkey;
       public         postgres    false    212            ?           2606    110782 D   aba_stages_of_breeds_and_stages aba_stages_of_breeds_and_stages_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.aba_stages_of_breeds_and_stages
    ADD CONSTRAINT aba_stages_of_breeds_and_stages_pkey PRIMARY KEY (id);
 n   ALTER TABLE ONLY public.aba_stages_of_breeds_and_stages DROP CONSTRAINT aba_stages_of_breeds_and_stages_pkey;
       public         postgres    false    214            E           2606    110784    mdapplication application_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public.mdapplication
    ADD CONSTRAINT application_pkey PRIMARY KEY (application_id);
 H   ALTER TABLE ONLY public.mdapplication DROP CONSTRAINT application_pkey;
       public         postgres    false    247            I           2606    110786 (   mdapplication_rol mdapplication_rol_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.mdapplication_rol
    ADD CONSTRAINT mdapplication_rol_pkey PRIMARY KEY (id);
 R   ALTER TABLE ONLY public.mdapplication_rol DROP CONSTRAINT mdapplication_rol_pkey;
       public         postgres    false    249            K           2606    110788    mdbreed mdbreed_code_key 
   CONSTRAINT     S   ALTER TABLE ONLY public.mdbreed
    ADD CONSTRAINT mdbreed_code_key UNIQUE (code);
 B   ALTER TABLE ONLY public.mdbreed DROP CONSTRAINT mdbreed_code_key;
       public         postgres    false    250            M           2606    110790    mdbreed mdbreed_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.mdbreed
    ADD CONSTRAINT mdbreed_pkey PRIMARY KEY (breed_id);
 >   ALTER TABLE ONLY public.mdbreed DROP CONSTRAINT mdbreed_pkey;
       public         postgres    false    250            O           2606    110792 (   mdbroiler_product mdbroiler_product_pkey 
   CONSTRAINT     v   ALTER TABLE ONLY public.mdbroiler_product
    ADD CONSTRAINT mdbroiler_product_pkey PRIMARY KEY (broiler_product_id);
 R   ALTER TABLE ONLY public.mdbroiler_product DROP CONSTRAINT mdbroiler_product_pkey;
       public         postgres    false    251            Q           2606    110794    mdfarmtype mdfarmtype_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.mdfarmtype
    ADD CONSTRAINT mdfarmtype_pkey PRIMARY KEY (farm_type_id);
 D   ALTER TABLE ONLY public.mdfarmtype DROP CONSTRAINT mdfarmtype_pkey;
       public         postgres    false    252            S           2606    110796    mdmeasure mdmeasure_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.mdmeasure
    ADD CONSTRAINT mdmeasure_pkey PRIMARY KEY (measure_id);
 B   ALTER TABLE ONLY public.mdmeasure DROP CONSTRAINT mdmeasure_pkey;
       public         postgres    false    254            W           2606    110798    mdparameter mdparameter_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.mdparameter
    ADD CONSTRAINT mdparameter_pkey PRIMARY KEY (parameter_id);
 F   ALTER TABLE ONLY public.mdparameter DROP CONSTRAINT mdparameter_pkey;
       public         postgres    false    256            ]           2606    110800    mdprocess mdprocess_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.mdprocess
    ADD CONSTRAINT mdprocess_pkey PRIMARY KEY (process_id);
 B   ALTER TABLE ONLY public.mdprocess DROP CONSTRAINT mdprocess_pkey;
       public         postgres    false    258            _           2606    110802    mdproduct mdproduct_code_unique 
   CONSTRAINT     Z   ALTER TABLE ONLY public.mdproduct
    ADD CONSTRAINT mdproduct_code_unique UNIQUE (code);
 I   ALTER TABLE ONLY public.mdproduct DROP CONSTRAINT mdproduct_code_unique;
       public         postgres    false    260            a           2606    110804    mdproduct mdproduct_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.mdproduct
    ADD CONSTRAINT mdproduct_pkey PRIMARY KEY (product_id);
 B   ALTER TABLE ONLY public.mdproduct DROP CONSTRAINT mdproduct_pkey;
       public         postgres    false    260            h           2606    110806 !   mdscenario mdscenario_name_unique 
   CONSTRAINT     \   ALTER TABLE ONLY public.mdscenario
    ADD CONSTRAINT mdscenario_name_unique UNIQUE (name);
 K   ALTER TABLE ONLY public.mdscenario DROP CONSTRAINT mdscenario_name_unique;
       public         postgres    false    264            j           2606    110808    mdscenario mdscenario_pkey 
   CONSTRAINT     a   ALTER TABLE ONLY public.mdscenario
    ADD CONSTRAINT mdscenario_pkey PRIMARY KEY (scenario_id);
 D   ALTER TABLE ONLY public.mdscenario DROP CONSTRAINT mdscenario_pkey;
       public         postgres    false    264            l           2606    110810    mdshedstatus mdshedstatus_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public.mdshedstatus
    ADD CONSTRAINT mdshedstatus_pkey PRIMARY KEY (shed_status_id);
 H   ALTER TABLE ONLY public.mdshedstatus DROP CONSTRAINT mdshedstatus_pkey;
       public         postgres    false    266            n           2606    110812    mdstage mdstage_name_unique 
   CONSTRAINT     V   ALTER TABLE ONLY public.mdstage
    ADD CONSTRAINT mdstage_name_unique UNIQUE (name);
 E   ALTER TABLE ONLY public.mdstage DROP CONSTRAINT mdstage_name_unique;
       public         postgres    false    268            p           2606    110814    mdstage mdstage_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.mdstage
    ADD CONSTRAINT mdstage_pkey PRIMARY KEY (stage_id);
 >   ALTER TABLE ONLY public.mdstage DROP CONSTRAINT mdstage_pkey;
       public         postgres    false    268            s           2606    110816    mduser mduser_user_id_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY public.mduser
    ADD CONSTRAINT mduser_user_id_pkey PRIMARY KEY (user_id);
 D   ALTER TABLE ONLY public.mduser DROP CONSTRAINT mduser_user_id_pkey;
       public         postgres    false    270            u           2606    110818    mduser mduser_username_unique 
   CONSTRAINT     \   ALTER TABLE ONLY public.mduser
    ADD CONSTRAINT mduser_username_unique UNIQUE (username);
 G   ALTER TABLE ONLY public.mduser DROP CONSTRAINT mduser_username_unique;
       public         postgres    false    270            �           2606    110820 .   oscenter_oswarehouse oscenter_oswarehouse_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.oscenter_oswarehouse
    ADD CONSTRAINT oscenter_oswarehouse_pkey PRIMARY KEY (client_id, partnership_id, farm_id, center_id, warehouse_id);
 X   ALTER TABLE ONLY public.oscenter_oswarehouse DROP CONSTRAINT oscenter_oswarehouse_pkey;
       public         postgres    false    272    272    272    272    272            y           2606    110822 .   oscenter oscenter_partnership_farm_code_unique 
   CONSTRAINT     �   ALTER TABLE ONLY public.oscenter
    ADD CONSTRAINT oscenter_partnership_farm_code_unique UNIQUE (partnership_id, farm_id, code);
 X   ALTER TABLE ONLY public.oscenter DROP CONSTRAINT oscenter_partnership_farm_code_unique;
       public         postgres    false    271    271    271            {           2606    110824    oscenter oscenter_pkey 
   CONSTRAINT     [   ALTER TABLE ONLY public.oscenter
    ADD CONSTRAINT oscenter_pkey PRIMARY KEY (center_id);
 @   ALTER TABLE ONLY public.oscenter DROP CONSTRAINT oscenter_pkey;
       public         postgres    false    271            �           2606    110826 %   osfarm osfarm_partnership_code_unique 
   CONSTRAINT     p   ALTER TABLE ONLY public.osfarm
    ADD CONSTRAINT osfarm_partnership_code_unique UNIQUE (partnership_id, code);
 O   ALTER TABLE ONLY public.osfarm DROP CONSTRAINT osfarm_partnership_code_unique;
       public         postgres    false    273    273            �           2606    110828    osfarm osfarm_pkey 
   CONSTRAINT     U   ALTER TABLE ONLY public.osfarm
    ADD CONSTRAINT osfarm_pkey PRIMARY KEY (farm_id);
 <   ALTER TABLE ONLY public.osfarm DROP CONSTRAINT osfarm_pkey;
       public         postgres    false    273            �           2606    110830    osshed oshed_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.osshed
    ADD CONSTRAINT oshed_pkey PRIMARY KEY (shed_id);
 ;   ALTER TABLE ONLY public.osshed DROP CONSTRAINT oshed_pkey;
       public         postgres    false    279            �           2606    110832 2   osincubator osincubator_incubatorplant_code_unique 
   CONSTRAINT     �   ALTER TABLE ONLY public.osincubator
    ADD CONSTRAINT osincubator_incubatorplant_code_unique UNIQUE (incubator_plant_id, code);
 \   ALTER TABLE ONLY public.osincubator DROP CONSTRAINT osincubator_incubatorplant_code_unique;
       public         postgres    false    274    274            �           2606    110834    osincubator osincubator_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.osincubator
    ADD CONSTRAINT osincubator_pkey PRIMARY KEY (incubator_id);
 F   ALTER TABLE ONLY public.osincubator DROP CONSTRAINT osincubator_pkey;
       public         postgres    false    274            �           2606    110836 9   osincubatorplant osincubatorplant_partnership_code_unique 
   CONSTRAINT     �   ALTER TABLE ONLY public.osincubatorplant
    ADD CONSTRAINT osincubatorplant_partnership_code_unique UNIQUE (partnership_id, code);
 c   ALTER TABLE ONLY public.osincubatorplant DROP CONSTRAINT osincubatorplant_partnership_code_unique;
       public         postgres    false    275    275            �           2606    110838 &   osincubatorplant osincubatorplant_pkey 
   CONSTRAINT     t   ALTER TABLE ONLY public.osincubatorplant
    ADD CONSTRAINT osincubatorplant_pkey PRIMARY KEY (incubator_plant_id);
 P   ALTER TABLE ONLY public.osincubatorplant DROP CONSTRAINT osincubatorplant_pkey;
       public         postgres    false    275            �           2606    110840 '   ospartnership ospartnership_code_unique 
   CONSTRAINT     b   ALTER TABLE ONLY public.ospartnership
    ADD CONSTRAINT ospartnership_code_unique UNIQUE (code);
 Q   ALTER TABLE ONLY public.ospartnership DROP CONSTRAINT ospartnership_code_unique;
       public         postgres    false    277            �           2606    110842     ospartnership ospartnership_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY public.ospartnership
    ADD CONSTRAINT ospartnership_pkey PRIMARY KEY (partnership_id);
 J   ALTER TABLE ONLY public.ospartnership DROP CONSTRAINT ospartnership_pkey;
       public         postgres    false    277            �           2606    110844 1   osshed osshed_partnership_farm_center_code_unique 
   CONSTRAINT     �   ALTER TABLE ONLY public.osshed
    ADD CONSTRAINT osshed_partnership_farm_center_code_unique UNIQUE (partnership_id, farm_id, center_id, code);
 [   ALTER TABLE ONLY public.osshed DROP CONSTRAINT osshed_partnership_farm_center_code_unique;
       public         postgres    false    279    279    279    279            �           2606    110846     ossilo_osshed ossilo_osshed_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.ossilo_osshed
    ADD CONSTRAINT ossilo_osshed_pkey PRIMARY KEY (silo_id, shed_id);
 J   ALTER TABLE ONLY public.ossilo_osshed DROP CONSTRAINT ossilo_osshed_pkey;
       public         postgres    false    282    282            �           2606    110848    ossilo ossilo_pkey 
   CONSTRAINT     U   ALTER TABLE ONLY public.ossilo
    ADD CONSTRAINT ossilo_pkey PRIMARY KEY (silo_id);
 <   ALTER TABLE ONLY public.ossilo DROP CONSTRAINT ossilo_pkey;
       public         postgres    false    281            �           2606    110850 &   osslaughterhouse osslaughterhouse_pkey 
   CONSTRAINT     s   ALTER TABLE ONLY public.osslaughterhouse
    ADD CONSTRAINT osslaughterhouse_pkey PRIMARY KEY (slaughterhouse_id);
 P   ALTER TABLE ONLY public.osslaughterhouse DROP CONSTRAINT osslaughterhouse_pkey;
       public         postgres    false    284            �           2606    110852 4   oswarehouse oswarehouse_partnership_farm_code_unique 
   CONSTRAINT     �   ALTER TABLE ONLY public.oswarehouse
    ADD CONSTRAINT oswarehouse_partnership_farm_code_unique UNIQUE (partnership_id, farm_id, code);
 ^   ALTER TABLE ONLY public.oswarehouse DROP CONSTRAINT oswarehouse_partnership_farm_code_unique;
       public         postgres    false    286    286    286            �           2606    110854    oswarehouse oswarehouse_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.oswarehouse
    ADD CONSTRAINT oswarehouse_pkey PRIMARY KEY (warehouse_id);
 F   ALTER TABLE ONLY public.oswarehouse DROP CONSTRAINT oswarehouse_pkey;
       public         postgres    false    286            c           2606    110856    mdrol rol_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.mdrol
    ADD CONSTRAINT rol_pkey PRIMARY KEY (rol_id);
 8   ALTER TABLE ONLY public.mdrol DROP CONSTRAINT rol_pkey;
       public         postgres    false    262            �           2606    110858 ,   txavailabilitysheds txavailabilitysheds_pkey 
   CONSTRAINT     |   ALTER TABLE ONLY public.txavailabilitysheds
    ADD CONSTRAINT txavailabilitysheds_pkey PRIMARY KEY (availability_shed_id);
 V   ALTER TABLE ONLY public.txavailabilitysheds DROP CONSTRAINT txavailabilitysheds_pkey;
       public         postgres    false    297            �           2606    110860 &   txbroiler_detail txbroiler_detail_pkey 
   CONSTRAINT     s   ALTER TABLE ONLY public.txbroiler_detail
    ADD CONSTRAINT txbroiler_detail_pkey PRIMARY KEY (broiler_detail_id);
 P   ALTER TABLE ONLY public.txbroiler_detail DROP CONSTRAINT txbroiler_detail_pkey;
       public         postgres    false    299            �           2606    110862    txbroiler txbroiler_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.txbroiler
    ADD CONSTRAINT txbroiler_pkey PRIMARY KEY (broiler_id);
 B   ALTER TABLE ONLY public.txbroiler DROP CONSTRAINT txbroiler_pkey;
       public         postgres    false    298            �           2606    110864 6   txbroilereviction_detail txbroilereviction_detail_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.txbroilereviction_detail
    ADD CONSTRAINT txbroilereviction_detail_pkey PRIMARY KEY (broilereviction_detail_id);
 `   ALTER TABLE ONLY public.txbroilereviction_detail DROP CONSTRAINT txbroilereviction_detail_pkey;
       public         postgres    false    301            �           2606    110866 (   txbroilereviction txbroilereviction_pkey 
   CONSTRAINT     v   ALTER TABLE ONLY public.txbroilereviction
    ADD CONSTRAINT txbroilereviction_pkey PRIMARY KEY (broilereviction_id);
 R   ALTER TABLE ONLY public.txbroilereviction DROP CONSTRAINT txbroilereviction_pkey;
       public         postgres    false    300            �           2606    110868 4   txbroilerproduct_detail txbroilerproduct_detail_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.txbroilerproduct_detail
    ADD CONSTRAINT txbroilerproduct_detail_pkey PRIMARY KEY (broilerproduct_detail_id);
 ^   ALTER TABLE ONLY public.txbroilerproduct_detail DROP CONSTRAINT txbroilerproduct_detail_pkey;
       public         postgres    false    302            �           2606    110870 &   txbroodermachine txbroodermachine_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.txbroodermachine
    ADD CONSTRAINT txbroodermachine_pkey PRIMARY KEY (brooder_machine_id_seq);
 P   ALTER TABLE ONLY public.txbroodermachine DROP CONSTRAINT txbroodermachine_pkey;
       public         postgres    false    303            �           2606    110872    txcalendar txcalendar_pkey 
   CONSTRAINT     a   ALTER TABLE ONLY public.txcalendar
    ADD CONSTRAINT txcalendar_pkey PRIMARY KEY (calendar_id);
 D   ALTER TABLE ONLY public.txcalendar DROP CONSTRAINT txcalendar_pkey;
       public         postgres    false    304            �           2606    110874     txcalendarday txcalendarday_pkey 
   CONSTRAINT     k   ALTER TABLE ONLY public.txcalendarday
    ADD CONSTRAINT txcalendarday_pkey PRIMARY KEY (calendar_day_id);
 J   ALTER TABLE ONLY public.txcalendarday DROP CONSTRAINT txcalendarday_pkey;
       public         postgres    false    305            �           2606    110876 $   txeggs_planning txeggs_planning_pkey 
   CONSTRAINT     o   ALTER TABLE ONLY public.txeggs_planning
    ADD CONSTRAINT txeggs_planning_pkey PRIMARY KEY (egg_planning_id);
 N   ALTER TABLE ONLY public.txeggs_planning DROP CONSTRAINT txeggs_planning_pkey;
       public         postgres    false    306            �           2606    110878 $   txeggs_required txeggs_required_pkey 
   CONSTRAINT     o   ALTER TABLE ONLY public.txeggs_required
    ADD CONSTRAINT txeggs_required_pkey PRIMARY KEY (egg_required_id);
 N   ALTER TABLE ONLY public.txeggs_required DROP CONSTRAINT txeggs_required_pkey;
       public         postgres    false    307            �           2606    110880 "   txeggs_storage txeggs_storage_pkey 
   CONSTRAINT     m   ALTER TABLE ONLY public.txeggs_storage
    ADD CONSTRAINT txeggs_storage_pkey PRIMARY KEY (eggs_storage_id);
 L   ALTER TABLE ONLY public.txeggs_storage DROP CONSTRAINT txeggs_storage_pkey;
       public         postgres    false    308            �           2606    110882    txgoals_erp txgoals_erp_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.txgoals_erp
    ADD CONSTRAINT txgoals_erp_pkey PRIMARY KEY (goals_erp_id);
 F   ALTER TABLE ONLY public.txgoals_erp DROP CONSTRAINT txgoals_erp_pkey;
       public         postgres    false    309            �           2606    110884 ,   txhousingway_detail txhousingway_detail_pkey 
   CONSTRAINT     |   ALTER TABLE ONLY public.txhousingway_detail
    ADD CONSTRAINT txhousingway_detail_pkey PRIMARY KEY (housingway_detail_id);
 V   ALTER TABLE ONLY public.txhousingway_detail DROP CONSTRAINT txhousingway_detail_pkey;
       public         postgres    false    312            �           2606    110886    txhousingway txhousingway_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public.txhousingway
    ADD CONSTRAINT txhousingway_pkey PRIMARY KEY (housing_way_id);
 H   ALTER TABLE ONLY public.txhousingway DROP CONSTRAINT txhousingway_pkey;
       public         postgres    false    311                       2606    110888    txlot_eggs txlot_eggs_pkey 
   CONSTRAINT     a   ALTER TABLE ONLY public.txlot_eggs
    ADD CONSTRAINT txlot_eggs_pkey PRIMARY KEY (lot_eggs_id);
 D   ALTER TABLE ONLY public.txlot_eggs DROP CONSTRAINT txlot_eggs_pkey;
       public         postgres    false    314            �           2606    110890    txlot txlot_lot_code_key 
   CONSTRAINT     W   ALTER TABLE ONLY public.txlot
    ADD CONSTRAINT txlot_lot_code_key UNIQUE (lot_code);
 B   ALTER TABLE ONLY public.txlot DROP CONSTRAINT txlot_lot_code_key;
       public         postgres    false    313                       2606    110892    txlot txlot_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.txlot
    ADD CONSTRAINT txlot_pkey PRIMARY KEY (lot_id);
 :   ALTER TABLE ONLY public.txlot DROP CONSTRAINT txlot_pkey;
       public         postgres    false    313                       2606    110894 "   txposturecurve txposturecurve_pkey 
   CONSTRAINT     n   ALTER TABLE ONLY public.txposturecurve
    ADD CONSTRAINT txposturecurve_pkey PRIMARY KEY (posture_curve_id);
 L   ALTER TABLE ONLY public.txposturecurve DROP CONSTRAINT txposturecurve_pkey;
       public         postgres    false    315                       2606    110896 (   txprogrammed_eggs txprogrammed_eggs_pkey 
   CONSTRAINT     v   ALTER TABLE ONLY public.txprogrammed_eggs
    ADD CONSTRAINT txprogrammed_eggs_pkey PRIMARY KEY (programmed_eggs_id);
 R   ALTER TABLE ONLY public.txprogrammed_eggs DROP CONSTRAINT txprogrammed_eggs_pkey;
       public         postgres    false    316                       2606    110898 (   txscenarioformula txscenarioformula_pkey 
   CONSTRAINT     w   ALTER TABLE ONLY public.txscenarioformula
    ADD CONSTRAINT txscenarioformula_pkey PRIMARY KEY (scenario_formula_id);
 R   ALTER TABLE ONLY public.txscenarioformula DROP CONSTRAINT txscenarioformula_pkey;
       public         postgres    false    317                       2606    110900 ,   txscenarioparameter txscenarioparameter_pkey 
   CONSTRAINT     }   ALTER TABLE ONLY public.txscenarioparameter
    ADD CONSTRAINT txscenarioparameter_pkey PRIMARY KEY (scenario_parameter_id);
 V   ALTER TABLE ONLY public.txscenarioparameter DROP CONSTRAINT txscenarioparameter_pkey;
       public         postgres    false    318                       2606    110902 2   txscenarioparameterday txscenarioparameterday_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioparameterday
    ADD CONSTRAINT txscenarioparameterday_pkey PRIMARY KEY (scenario_parameter_day_id);
 \   ALTER TABLE ONLY public.txscenarioparameterday DROP CONSTRAINT txscenarioparameterday_pkey;
       public         postgres    false    319                        2606    110904 2   txscenarioposturecurve txscenarioposturecurve_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioposturecurve
    ADD CONSTRAINT txscenarioposturecurve_pkey PRIMARY KEY (scenario_posture_id);
 \   ALTER TABLE ONLY public.txscenarioposturecurve DROP CONSTRAINT txscenarioposturecurve_pkey;
       public         postgres    false    320            $           2606    110906 (   txscenarioprocess txscenarioprocess_pkey 
   CONSTRAINT     w   ALTER TABLE ONLY public.txscenarioprocess
    ADD CONSTRAINT txscenarioprocess_pkey PRIMARY KEY (scenario_process_id);
 R   ALTER TABLE ONLY public.txscenarioprocess DROP CONSTRAINT txscenarioprocess_pkey;
       public         postgres    false    321            e           2606    110908    mdrol uniqueRolName 
   CONSTRAINT     T   ALTER TABLE ONLY public.mdrol
    ADD CONSTRAINT "uniqueRolName" UNIQUE (rol_name);
 ?   ALTER TABLE ONLY public.mdrol DROP CONSTRAINT "uniqueRolName";
       public         postgres    false    262            �           1259    110909    calendar_index    INDEX     N   CREATE INDEX calendar_index ON public.txcalendarday USING hash (calendar_id);
 "   DROP INDEX public.calendar_index;
       public         postgres    false    305            X           1259    110910    calendarid_index    INDEX     L   CREATE INDEX calendarid_index ON public.mdprocess USING hash (calendar_id);
 $   DROP INDEX public.calendarid_index;
       public         postgres    false    258            �           1259    110911 
   code_index    INDEX     H   CREATE UNIQUE INDEX code_index ON public.txcalendar USING btree (code);
    DROP INDEX public.code_index;
       public         postgres    false    304            �           1259    110912 
   date_index    INDEX     G   CREATE INDEX date_index ON public.txcalendarday USING hash (use_date);
    DROP INDEX public.date_index;
       public         postgres    false    305            *           1259    110913    fki_FK_ id_aba_time_unit    INDEX     p   CREATE INDEX "fki_FK_ id_aba_time_unit" ON public.aba_consumption_and_mortality USING btree (id_aba_time_unit);
 .   DROP INDEX public."fki_FK_ id_aba_time_unit";
       public         postgres    false    200            @           1259    110914    fki_FK_id_aba_breeds_and_stages    INDEX     �   CREATE INDEX "fki_FK_id_aba_breeds_and_stages" ON public.aba_stages_of_breeds_and_stages USING btree (id_aba_breeds_and_stages);
 5   DROP INDEX public."fki_FK_id_aba_breeds_and_stages";
       public         postgres    false    214            &           1259    110915 '   fki_FK_id_aba_consumption_and_mortality    INDEX     �   CREATE INDEX "fki_FK_id_aba_consumption_and_mortality" ON public.aba_breeds_and_stages USING btree (id_aba_consumption_and_mortality);
 =   DROP INDEX public."fki_FK_id_aba_consumption_and_mortality";
       public         postgres    false    198            /           1259    110916 (   fki_FK_id_aba_consumption_and_mortality2    INDEX     �   CREATE INDEX "fki_FK_id_aba_consumption_and_mortality2" ON public.aba_consumption_and_mortality_detail USING btree (id_aba_consumption_and_mortality);
 >   DROP INDEX public."fki_FK_id_aba_consumption_and_mortality2";
       public         postgres    false    202            4           1259    110917    fki_FK_id_aba_element    INDEX     m   CREATE INDEX "fki_FK_id_aba_element" ON public.aba_elements_and_concentrations USING btree (id_aba_element);
 +   DROP INDEX public."fki_FK_id_aba_element";
       public         postgres    false    206            =           1259    110918    fki_FK_id_aba_element2    INDEX     Z   CREATE INDEX "fki_FK_id_aba_element2" ON public.aba_results USING btree (id_aba_element);
 ,   DROP INDEX public."fki_FK_id_aba_element2";
       public         postgres    false    212            5           1259    110919    fki_FK_id_aba_element_property    INDEX        CREATE INDEX "fki_FK_id_aba_element_property" ON public.aba_elements_and_concentrations USING btree (id_aba_element_property);
 4   DROP INDEX public."fki_FK_id_aba_element_property";
       public         postgres    false    206            6           1259    110920    fki_FK_id_aba_formulation    INDEX     u   CREATE INDEX "fki_FK_id_aba_formulation" ON public.aba_elements_and_concentrations USING btree (id_aba_formulation);
 /   DROP INDEX public."fki_FK_id_aba_formulation";
       public         postgres    false    206            +           1259    110921    fki_FK_id_breed    INDEX     _   CREATE INDEX "fki_FK_id_breed" ON public.aba_consumption_and_mortality USING btree (id_breed);
 %   DROP INDEX public."fki_FK_id_breed";
       public         postgres    false    200            A           1259    110922    fki_FK_id_formulation    INDEX     m   CREATE INDEX "fki_FK_id_formulation" ON public.aba_stages_of_breeds_and_stages USING btree (id_formulation);
 +   DROP INDEX public."fki_FK_id_formulation";
       public         postgres    false    214            '           1259    110923    fki_FK_id_process    INDEX     [   CREATE INDEX "fki_FK_id_process" ON public.aba_breeds_and_stages USING btree (id_process);
 '   DROP INDEX public."fki_FK_id_process";
       public         postgres    false    198            ,           1259    110924    fki_FK_id_stage    INDEX     _   CREATE INDEX "fki_FK_id_stage" ON public.aba_consumption_and_mortality USING btree (id_stage);
 %   DROP INDEX public."fki_FK_id_stage";
       public         postgres    false    200            F           1259    110925 )   fki_mdapplication_rol_application_id_fkey    INDEX     q   CREATE INDEX fki_mdapplication_rol_application_id_fkey ON public.mdapplication_rol USING btree (application_id);
 =   DROP INDEX public.fki_mdapplication_rol_application_id_fkey;
       public         postgres    false    249            G           1259    110926 !   fki_mdapplication_rol_rol_id_fkey    INDEX     a   CREATE INDEX fki_mdapplication_rol_rol_id_fkey ON public.mdapplication_rol USING btree (rol_id);
 5   DROP INDEX public.fki_mdapplication_rol_rol_id_fkey;
       public         postgres    false    249            T           1259    110927    fki_mdparameter_measure_id_fkey    INDEX     ]   CREATE INDEX fki_mdparameter_measure_id_fkey ON public.mdparameter USING btree (measure_id);
 3   DROP INDEX public.fki_mdparameter_measure_id_fkey;
       public         postgres    false    256            U           1259    110928    fki_mdparameter_process_id_fkey    INDEX     ]   CREATE INDEX fki_mdparameter_process_id_fkey ON public.mdparameter USING btree (process_id);
 3   DROP INDEX public.fki_mdparameter_process_id_fkey;
       public         postgres    false    256            Y           1259    110929    fki_mdprocess_breed_id_fkey    INDEX     U   CREATE INDEX fki_mdprocess_breed_id_fkey ON public.mdprocess USING btree (breed_id);
 /   DROP INDEX public.fki_mdprocess_breed_id_fkey;
       public         postgres    false    258            f           1259    110930    fki_mdscenario_calendar_id_fkey    INDEX     ]   CREATE INDEX fki_mdscenario_calendar_id_fkey ON public.mdscenario USING btree (calendar_id);
 3   DROP INDEX public.fki_mdscenario_calendar_id_fkey;
       public         postgres    false    264            q           1259    110931    fki_mduser_rol_id_fkey    INDEX     K   CREATE INDEX fki_mduser_rol_id_fkey ON public.mduser USING btree (rol_id);
 *   DROP INDEX public.fki_mduser_rol_id_fkey;
       public         postgres    false    270            v           1259    110932    fki_oscenter_farm_id_fkey    INDEX     Q   CREATE INDEX fki_oscenter_farm_id_fkey ON public.oscenter USING btree (farm_id);
 -   DROP INDEX public.fki_oscenter_farm_id_fkey;
       public         postgres    false    271            |           1259    110933 '   fki_oscenter_oswarehouse_center_id_fkey    INDEX     m   CREATE INDEX fki_oscenter_oswarehouse_center_id_fkey ON public.oscenter_oswarehouse USING btree (center_id);
 ;   DROP INDEX public.fki_oscenter_oswarehouse_center_id_fkey;
       public         postgres    false    272            }           1259    110934 %   fki_oscenter_oswarehouse_farm_id_fkey    INDEX     i   CREATE INDEX fki_oscenter_oswarehouse_farm_id_fkey ON public.oscenter_oswarehouse USING btree (farm_id);
 9   DROP INDEX public.fki_oscenter_oswarehouse_farm_id_fkey;
       public         postgres    false    272            ~           1259    110935 ,   fki_oscenter_oswarehouse_partnership_id_fkey    INDEX     w   CREATE INDEX fki_oscenter_oswarehouse_partnership_id_fkey ON public.oscenter_oswarehouse USING btree (partnership_id);
 @   DROP INDEX public.fki_oscenter_oswarehouse_partnership_id_fkey;
       public         postgres    false    272                       1259    110936 *   fki_oscenter_oswarehouse_warehouse_id_fkey    INDEX     s   CREATE INDEX fki_oscenter_oswarehouse_warehouse_id_fkey ON public.oscenter_oswarehouse USING btree (warehouse_id);
 >   DROP INDEX public.fki_oscenter_oswarehouse_warehouse_id_fkey;
       public         postgres    false    272            w           1259    110937     fki_oscenter_partnership_id_fkey    INDEX     _   CREATE INDEX fki_oscenter_partnership_id_fkey ON public.oscenter USING btree (partnership_id);
 4   DROP INDEX public.fki_oscenter_partnership_id_fkey;
       public         postgres    false    271            �           1259    110938    fki_osfarm_farm_type_id_fkey    INDEX     W   CREATE INDEX fki_osfarm_farm_type_id_fkey ON public.osfarm USING btree (farm_type_id);
 0   DROP INDEX public.fki_osfarm_farm_type_id_fkey;
       public         postgres    false    273            �           1259    110939    fki_osfarm_partnership_id_fkey    INDEX     [   CREATE INDEX fki_osfarm_partnership_id_fkey ON public.osfarm USING btree (partnership_id);
 2   DROP INDEX public.fki_osfarm_partnership_id_fkey;
       public         postgres    false    273            �           1259    110940 '   fki_osincubator_incubator_plant_id_fkey    INDEX     m   CREATE INDEX fki_osincubator_incubator_plant_id_fkey ON public.osincubator USING btree (incubator_plant_id);
 ;   DROP INDEX public.fki_osincubator_incubator_plant_id_fkey;
       public         postgres    false    274            �           1259    110941 (   fki_osincubatorplant_partnership_id_fkey    INDEX     o   CREATE INDEX fki_osincubatorplant_partnership_id_fkey ON public.osincubatorplant USING btree (partnership_id);
 <   DROP INDEX public.fki_osincubatorplant_partnership_id_fkey;
       public         postgres    false    275            �           1259    110942    fki_osshed_center_id_fkey    INDEX     Q   CREATE INDEX fki_osshed_center_id_fkey ON public.osshed USING btree (center_id);
 -   DROP INDEX public.fki_osshed_center_id_fkey;
       public         postgres    false    279            �           1259    110943    fki_osshed_farm_id_fkey    INDEX     M   CREATE INDEX fki_osshed_farm_id_fkey ON public.osshed USING btree (farm_id);
 +   DROP INDEX public.fki_osshed_farm_id_fkey;
       public         postgres    false    279            �           1259    110944    fki_osshed_partnership_id_fkey    INDEX     [   CREATE INDEX fki_osshed_partnership_id_fkey ON public.osshed USING btree (partnership_id);
 2   DROP INDEX public.fki_osshed_partnership_id_fkey;
       public         postgres    false    279            �           1259    110945    fki_osshed_statusshed_id_fkey    INDEX     Y   CREATE INDEX fki_osshed_statusshed_id_fkey ON public.osshed USING btree (statusshed_id);
 1   DROP INDEX public.fki_osshed_statusshed_id_fkey;
       public         postgres    false    279            �           1259    110946    fki_ossilo_center_id_fkey    INDEX     Q   CREATE INDEX fki_ossilo_center_id_fkey ON public.ossilo USING btree (center_id);
 -   DROP INDEX public.fki_ossilo_center_id_fkey;
       public         postgres    false    281            �           1259    110947    fki_ossilo_farm_id_fkey    INDEX     M   CREATE INDEX fki_ossilo_farm_id_fkey ON public.ossilo USING btree (farm_id);
 +   DROP INDEX public.fki_ossilo_farm_id_fkey;
       public         postgres    false    281            �           1259    110948     fki_ossilo_osshed_center_id_fkey    INDEX     _   CREATE INDEX fki_ossilo_osshed_center_id_fkey ON public.ossilo_osshed USING btree (center_id);
 4   DROP INDEX public.fki_ossilo_osshed_center_id_fkey;
       public         postgres    false    282            �           1259    110949    fki_ossilo_osshed_farm_id_fkey    INDEX     [   CREATE INDEX fki_ossilo_osshed_farm_id_fkey ON public.ossilo_osshed USING btree (farm_id);
 2   DROP INDEX public.fki_ossilo_osshed_farm_id_fkey;
       public         postgres    false    282            �           1259    110950 %   fki_ossilo_osshed_partnership_id_fkey    INDEX     i   CREATE INDEX fki_ossilo_osshed_partnership_id_fkey ON public.ossilo_osshed USING btree (partnership_id);
 9   DROP INDEX public.fki_ossilo_osshed_partnership_id_fkey;
       public         postgres    false    282            �           1259    110951    fki_ossilo_osshed_shed_id_fkey    INDEX     [   CREATE INDEX fki_ossilo_osshed_shed_id_fkey ON public.ossilo_osshed USING btree (shed_id);
 2   DROP INDEX public.fki_ossilo_osshed_shed_id_fkey;
       public         postgres    false    282            �           1259    110952    fki_ossilo_osshed_silo_id_fkey    INDEX     [   CREATE INDEX fki_ossilo_osshed_silo_id_fkey ON public.ossilo_osshed USING btree (silo_id);
 2   DROP INDEX public.fki_ossilo_osshed_silo_id_fkey;
       public         postgres    false    282            �           1259    110953    fki_ossilo_partnership_id_fkey    INDEX     [   CREATE INDEX fki_ossilo_partnership_id_fkey ON public.ossilo USING btree (partnership_id);
 2   DROP INDEX public.fki_ossilo_partnership_id_fkey;
       public         postgres    false    281            �           1259    110954    fki_oswarehouse_farm_id_fkey    INDEX     W   CREATE INDEX fki_oswarehouse_farm_id_fkey ON public.oswarehouse USING btree (farm_id);
 0   DROP INDEX public.fki_oswarehouse_farm_id_fkey;
       public         postgres    false    286            �           1259    110955 #   fki_oswarehouse_partnership_id_fkey    INDEX     e   CREATE INDEX fki_oswarehouse_partnership_id_fkey ON public.oswarehouse USING btree (partnership_id);
 7   DROP INDEX public.fki_oswarehouse_partnership_id_fkey;
       public         postgres    false    286            Z           1259    110956    fki_process_product_id_fkey    INDEX     W   CREATE INDEX fki_process_product_id_fkey ON public.mdprocess USING btree (product_id);
 /   DROP INDEX public.fki_process_product_id_fkey;
       public         postgres    false    258            [           1259    110957    fki_process_stage_id_fkey    INDEX     S   CREATE INDEX fki_process_stage_id_fkey ON public.mdprocess USING btree (stage_id);
 -   DROP INDEX public.fki_process_stage_id_fkey;
       public         postgres    false    258            �           1259    110958 %   fki_txavailabilitysheds_lot_code_fkey    INDEX     i   CREATE INDEX fki_txavailabilitysheds_lot_code_fkey ON public.txavailabilitysheds USING btree (lot_code);
 9   DROP INDEX public.fki_txavailabilitysheds_lot_code_fkey;
       public         postgres    false    297            �           1259    110959 $   fki_txavailabilitysheds_shed_id_fkey    INDEX     g   CREATE INDEX fki_txavailabilitysheds_shed_id_fkey ON public.txavailabilitysheds USING btree (shed_id);
 8   DROP INDEX public.fki_txavailabilitysheds_shed_id_fkey;
       public         postgres    false    297            �           1259    110960 $   fki_txbroiler_detail_broiler_id_fkey    INDEX     g   CREATE INDEX fki_txbroiler_detail_broiler_id_fkey ON public.txbroiler_detail USING btree (broiler_id);
 8   DROP INDEX public.fki_txbroiler_detail_broiler_id_fkey;
       public         postgres    false    299            �           1259    110961 !   fki_txbroiler_detail_farm_id_fkey    INDEX     a   CREATE INDEX fki_txbroiler_detail_farm_id_fkey ON public.txbroiler_detail USING btree (farm_id);
 5   DROP INDEX public.fki_txbroiler_detail_farm_id_fkey;
       public         postgres    false    299            �           1259    110962 !   fki_txbroiler_detail_shed_id_fkey    INDEX     a   CREATE INDEX fki_txbroiler_detail_shed_id_fkey ON public.txbroiler_detail USING btree (shed_id);
 5   DROP INDEX public.fki_txbroiler_detail_shed_id_fkey;
       public         postgres    false    299            �           1259    110963 %   fki_txbroiler_programmed_eggs_id_fkey    INDEX     i   CREATE INDEX fki_txbroiler_programmed_eggs_id_fkey ON public.txbroiler USING btree (programmed_eggs_id);
 9   DROP INDEX public.fki_txbroiler_programmed_eggs_id_fkey;
       public         postgres    false    298            �           1259    110964 #   fki_txbroilereviction_breed_id_fkey    INDEX     e   CREATE INDEX fki_txbroilereviction_breed_id_fkey ON public.txbroilereviction USING btree (breed_id);
 7   DROP INDEX public.fki_txbroilereviction_breed_id_fkey;
       public         postgres    false    300            �           1259    110965 ,   fki_txbroilereviction_detail_broiler_id_fkey    INDEX        CREATE INDEX fki_txbroilereviction_detail_broiler_id_fkey ON public.txbroilereviction_detail USING btree (broilereviction_id);
 @   DROP INDEX public.fki_txbroilereviction_detail_broiler_id_fkey;
       public         postgres    false    301            �           1259    110966 4   fki_txbroilereviction_detail_broiler_product_id_fkey    INDEX     �   CREATE INDEX fki_txbroilereviction_detail_broiler_product_id_fkey ON public.txbroilereviction_detail USING btree (broiler_product_id);
 H   DROP INDEX public.fki_txbroilereviction_detail_broiler_product_id_fkey;
       public         postgres    false    301            �           1259    110967 )   fki_txbroilereviction_detail_farm_id_fkey    INDEX     q   CREATE INDEX fki_txbroilereviction_detail_farm_id_fkey ON public.txbroilereviction_detail USING btree (farm_id);
 =   DROP INDEX public.fki_txbroilereviction_detail_farm_id_fkey;
       public         postgres    false    301            �           1259    110968 )   fki_txbroilereviction_detail_shed_id_fkey    INDEX     q   CREATE INDEX fki_txbroilereviction_detail_shed_id_fkey ON public.txbroilereviction_detail USING btree (shed_id);
 =   DROP INDEX public.fki_txbroilereviction_detail_shed_id_fkey;
       public         postgres    false    301            �           1259    110969 3   fki_txbroilereviction_detail_slaughterhouse_id_fkey    INDEX     �   CREATE INDEX fki_txbroilereviction_detail_slaughterhouse_id_fkey ON public.txbroilereviction_detail USING btree (slaughterhouse_id);
 G   DROP INDEX public.fki_txbroilereviction_detail_slaughterhouse_id_fkey;
       public         postgres    false    301            �           1259    110970 )   fki_txbroilereviction_partnership_id_fkey    INDEX     q   CREATE INDEX fki_txbroilereviction_partnership_id_fkey ON public.txbroilereviction USING btree (partnership_id);
 =   DROP INDEX public.fki_txbroilereviction_partnership_id_fkey;
       public         postgres    false    300            �           1259    110971 &   fki_txbroilereviction_scenario_id_fkey    INDEX     k   CREATE INDEX fki_txbroilereviction_scenario_id_fkey ON public.txbroilereviction USING btree (scenario_id);
 :   DROP INDEX public.fki_txbroilereviction_scenario_id_fkey;
       public         postgres    false    300            �           1259    110972 /   fki_txbroilerproduct_detail_broiler_detail_fkey    INDEX     }   CREATE INDEX fki_txbroilerproduct_detail_broiler_detail_fkey ON public.txbroilerproduct_detail USING btree (broiler_detail);
 C   DROP INDEX public.fki_txbroilerproduct_detail_broiler_detail_fkey;
       public         postgres    false    302            �           1259    110973 "   fki_txbroodermachines_farm_id_fkey    INDEX     b   CREATE INDEX fki_txbroodermachines_farm_id_fkey ON public.txbroodermachine USING btree (farm_id);
 6   DROP INDEX public.fki_txbroodermachines_farm_id_fkey;
       public         postgres    false    303            �           1259    110974 )   fki_txbroodermachines_partnership_id_fkey    INDEX     p   CREATE INDEX fki_txbroodermachines_partnership_id_fkey ON public.txbroodermachine USING btree (partnership_id);
 =   DROP INDEX public.fki_txbroodermachines_partnership_id_fkey;
       public         postgres    false    303            �           1259    110975 !   fki_txeggs_planning_breed_id_fkey    INDEX     a   CREATE INDEX fki_txeggs_planning_breed_id_fkey ON public.txeggs_planning USING btree (breed_id);
 5   DROP INDEX public.fki_txeggs_planning_breed_id_fkey;
       public         postgres    false    306            �           1259    110976 $   fki_txeggs_planning_scenario_id_fkey    INDEX     g   CREATE INDEX fki_txeggs_planning_scenario_id_fkey ON public.txeggs_planning USING btree (scenario_id);
 8   DROP INDEX public.fki_txeggs_planning_scenario_id_fkey;
       public         postgres    false    306            �           1259    110977 !   fki_txeggs_required_breed_id_fkey    INDEX     a   CREATE INDEX fki_txeggs_required_breed_id_fkey ON public.txeggs_required USING btree (breed_id);
 5   DROP INDEX public.fki_txeggs_required_breed_id_fkey;
       public         postgres    false    307            �           1259    110978 $   fki_txeggs_required_scenario_id_fkey    INDEX     g   CREATE INDEX fki_txeggs_required_scenario_id_fkey ON public.txeggs_required USING btree (scenario_id);
 8   DROP INDEX public.fki_txeggs_required_scenario_id_fkey;
       public         postgres    false    307            �           1259    110979     fki_txeggs_storage_breed_id_fkey    INDEX     _   CREATE INDEX fki_txeggs_storage_breed_id_fkey ON public.txeggs_storage USING btree (breed_id);
 4   DROP INDEX public.fki_txeggs_storage_breed_id_fkey;
       public         postgres    false    308            �           1259    110980 *   fki_txeggs_storage_incubator_plant_id_fkey    INDEX     s   CREATE INDEX fki_txeggs_storage_incubator_plant_id_fkey ON public.txeggs_storage USING btree (incubator_plant_id);
 >   DROP INDEX public.fki_txeggs_storage_incubator_plant_id_fkey;
       public         postgres    false    308            �           1259    110981 #   fki_txeggs_storage_scenario_id_fkey    INDEX     e   CREATE INDEX fki_txeggs_storage_scenario_id_fkey ON public.txeggs_storage USING btree (scenario_id);
 7   DROP INDEX public.fki_txeggs_storage_scenario_id_fkey;
       public         postgres    false    308            �           1259    110982    fki_txfattening_breed_id_fkey    INDEX     W   CREATE INDEX fki_txfattening_breed_id_fkey ON public.txbroiler USING btree (breed_id);
 1   DROP INDEX public.fki_txfattening_breed_id_fkey;
       public         postgres    false    298            �           1259    110983 #   fki_txfattening_partnership_id_fkey    INDEX     c   CREATE INDEX fki_txfattening_partnership_id_fkey ON public.txbroiler USING btree (partnership_id);
 7   DROP INDEX public.fki_txfattening_partnership_id_fkey;
       public         postgres    false    298            �           1259    110984     fki_txfattening_scenario_id_fkey    INDEX     ]   CREATE INDEX fki_txfattening_scenario_id_fkey ON public.txbroiler USING btree (scenario_id);
 4   DROP INDEX public.fki_txfattening_scenario_id_fkey;
       public         postgres    false    298            �           1259    110985    fki_txgoals_erp_product_id_fkey    INDEX     ]   CREATE INDEX fki_txgoals_erp_product_id_fkey ON public.txgoals_erp USING btree (product_id);
 3   DROP INDEX public.fki_txgoals_erp_product_id_fkey;
       public         postgres    false    309            �           1259    110986     fki_txgoals_erp_scenario_id_fkey    INDEX     _   CREATE INDEX fki_txgoals_erp_scenario_id_fkey ON public.txgoals_erp USING btree (scenario_id);
 4   DROP INDEX public.fki_txgoals_erp_scenario_id_fkey;
       public         postgres    false    309            �           1259    110987    fki_txhousingway_breed_id_fkey    INDEX     [   CREATE INDEX fki_txhousingway_breed_id_fkey ON public.txhousingway USING btree (breed_id);
 2   DROP INDEX public.fki_txhousingway_breed_id_fkey;
       public         postgres    false    311            �           1259    110988 $   fki_txhousingway_detail_farm_id_fkey    INDEX     g   CREATE INDEX fki_txhousingway_detail_farm_id_fkey ON public.txhousingway_detail USING btree (farm_id);
 8   DROP INDEX public.fki_txhousingway_detail_farm_id_fkey;
       public         postgres    false    312            �           1259    110989 +   fki_txhousingway_detail_housing_way_id_fkey    INDEX     u   CREATE INDEX fki_txhousingway_detail_housing_way_id_fkey ON public.txhousingway_detail USING btree (housing_way_id);
 ?   DROP INDEX public.fki_txhousingway_detail_housing_way_id_fkey;
       public         postgres    false    312            �           1259    110990 /   fki_txhousingway_detail_incubator_plant_id_fkey    INDEX     }   CREATE INDEX fki_txhousingway_detail_incubator_plant_id_fkey ON public.txhousingway_detail USING btree (incubator_plant_id);
 C   DROP INDEX public.fki_txhousingway_detail_incubator_plant_id_fkey;
       public         postgres    false    312            �           1259    110991 $   fki_txhousingway_detail_shed_id_fkey    INDEX     g   CREATE INDEX fki_txhousingway_detail_shed_id_fkey ON public.txhousingway_detail USING btree (shed_id);
 8   DROP INDEX public.fki_txhousingway_detail_shed_id_fkey;
       public         postgres    false    312            �           1259    110992 $   fki_txhousingway_partnership_id_fkey    INDEX     g   CREATE INDEX fki_txhousingway_partnership_id_fkey ON public.txhousingway USING btree (partnership_id);
 8   DROP INDEX public.fki_txhousingway_partnership_id_fkey;
       public         postgres    false    311            �           1259    110993 !   fki_txhousingway_scenario_id_fkey    INDEX     a   CREATE INDEX fki_txhousingway_scenario_id_fkey ON public.txhousingway USING btree (scenario_id);
 5   DROP INDEX public.fki_txhousingway_scenario_id_fkey;
       public         postgres    false    311            �           1259    110994    fki_txhousingway_stage_id_fkey    INDEX     [   CREATE INDEX fki_txhousingway_stage_id_fkey ON public.txhousingway USING btree (stage_id);
 2   DROP INDEX public.fki_txhousingway_stage_id_fkey;
       public         postgres    false    311            �           1259    110995    fki_txlot_breed_id_fkey    INDEX     M   CREATE INDEX fki_txlot_breed_id_fkey ON public.txlot USING btree (breed_id);
 +   DROP INDEX public.fki_txlot_breed_id_fkey;
       public         postgres    false    313            �           1259    110996    fki_txlot_farm_id_fkey    INDEX     K   CREATE INDEX fki_txlot_farm_id_fkey ON public.txlot USING btree (farm_id);
 *   DROP INDEX public.fki_txlot_farm_id_fkey;
       public         postgres    false    313            �           1259    110997    fki_txlot_housin_way_id_fkey    INDEX     X   CREATE INDEX fki_txlot_housin_way_id_fkey ON public.txlot USING btree (housing_way_id);
 0   DROP INDEX public.fki_txlot_housin_way_id_fkey;
       public         postgres    false    313            �           1259    110998    fki_txlot_product_id_fkey    INDEX     Q   CREATE INDEX fki_txlot_product_id_fkey ON public.txlot USING btree (product_id);
 -   DROP INDEX public.fki_txlot_product_id_fkey;
       public         postgres    false    313            �           1259    110999    fki_txlot_shed_id_fkey    INDEX     K   CREATE INDEX fki_txlot_shed_id_fkey ON public.txlot USING btree (shed_id);
 *   DROP INDEX public.fki_txlot_shed_id_fkey;
       public         postgres    false    313                       1259    111000     fki_txposturecurve_breed_id_fkey    INDEX     _   CREATE INDEX fki_txposturecurve_breed_id_fkey ON public.txposturecurve USING btree (breed_id);
 4   DROP INDEX public.fki_txposturecurve_breed_id_fkey;
       public         postgres    false    315                       1259    111001 #   fki_txprogrammed_eggs_breed_id_fkey    INDEX     e   CREATE INDEX fki_txprogrammed_eggs_breed_id_fkey ON public.txprogrammed_eggs USING btree (breed_id);
 7   DROP INDEX public.fki_txprogrammed_eggs_breed_id_fkey;
       public         postgres    false    316                       1259    111002 *   fki_txprogrammed_eggs_eggs_storage_id_fkey    INDEX     s   CREATE INDEX fki_txprogrammed_eggs_eggs_storage_id_fkey ON public.txprogrammed_eggs USING btree (eggs_storage_id);
 >   DROP INDEX public.fki_txprogrammed_eggs_eggs_storage_id_fkey;
       public         postgres    false    316            	           1259    111003 '   fki_txprogrammed_eggs_incubator_id_fkey    INDEX     m   CREATE INDEX fki_txprogrammed_eggs_incubator_id_fkey ON public.txprogrammed_eggs USING btree (incubator_id);
 ;   DROP INDEX public.fki_txprogrammed_eggs_incubator_id_fkey;
       public         postgres    false    316                       1259    111004 %   fki_txscenarioformula_measure_id_fkey    INDEX     i   CREATE INDEX fki_txscenarioformula_measure_id_fkey ON public.txscenarioformula USING btree (measure_id);
 9   DROP INDEX public.fki_txscenarioformula_measure_id_fkey;
       public         postgres    false    317                       1259    111005 '   fki_txscenarioformula_parameter_id_fkey    INDEX     m   CREATE INDEX fki_txscenarioformula_parameter_id_fkey ON public.txscenarioformula USING btree (parameter_id);
 ;   DROP INDEX public.fki_txscenarioformula_parameter_id_fkey;
       public         postgres    false    317                       1259    111006 %   fki_txscenarioformula_process_id_fkey    INDEX     i   CREATE INDEX fki_txscenarioformula_process_id_fkey ON public.txscenarioformula USING btree (process_id);
 9   DROP INDEX public.fki_txscenarioformula_process_id_fkey;
       public         postgres    false    317                       1259    111007 &   fki_txscenarioformula_scenario_id_fkey    INDEX     k   CREATE INDEX fki_txscenarioformula_scenario_id_fkey ON public.txscenarioformula USING btree (scenario_id);
 :   DROP INDEX public.fki_txscenarioformula_scenario_id_fkey;
       public         postgres    false    317                       1259    111008 )   fki_txscenarioparameter_parameter_id_fkey    INDEX     q   CREATE INDEX fki_txscenarioparameter_parameter_id_fkey ON public.txscenarioparameter USING btree (parameter_id);
 =   DROP INDEX public.fki_txscenarioparameter_parameter_id_fkey;
       public         postgres    false    318                       1259    111009 '   fki_txscenarioparameter_process_id_fkey    INDEX     m   CREATE INDEX fki_txscenarioparameter_process_id_fkey ON public.txscenarioparameter USING btree (process_id);
 ;   DROP INDEX public.fki_txscenarioparameter_process_id_fkey;
       public         postgres    false    318                       1259    111010 (   fki_txscenarioparameter_scenario_id_fkey    INDEX     o   CREATE INDEX fki_txscenarioparameter_scenario_id_fkey ON public.txscenarioparameter USING btree (scenario_id);
 <   DROP INDEX public.fki_txscenarioparameter_scenario_id_fkey;
       public         postgres    false    318                       1259    111011 ,   fki_txscenarioparameterday_parameter_id_fkey    INDEX     w   CREATE INDEX fki_txscenarioparameterday_parameter_id_fkey ON public.txscenarioparameterday USING btree (parameter_id);
 @   DROP INDEX public.fki_txscenarioparameterday_parameter_id_fkey;
       public         postgres    false    319                       1259    111012 +   fki_txscenarioparameterday_scenario_id_fkey    INDEX     u   CREATE INDEX fki_txscenarioparameterday_scenario_id_fkey ON public.txscenarioparameterday USING btree (scenario_id);
 ?   DROP INDEX public.fki_txscenarioparameterday_scenario_id_fkey;
       public         postgres    false    319                       1259    111013 (   fki_txscenarioposturecurve_breed_id_fkey    INDEX     o   CREATE INDEX fki_txscenarioposturecurve_breed_id_fkey ON public.txscenarioposturecurve USING btree (breed_id);
 <   DROP INDEX public.fki_txscenarioposturecurve_breed_id_fkey;
       public         postgres    false    320                       1259    111014 4   fki_txscenarioposturecurve_housingway_detail_id_fkey    INDEX     �   CREATE INDEX fki_txscenarioposturecurve_housingway_detail_id_fkey ON public.txscenarioposturecurve USING btree (housingway_detail_id);
 H   DROP INDEX public.fki_txscenarioposturecurve_housingway_detail_id_fkey;
       public         postgres    false    320                       1259    111015 +   fki_txscenarioposturecurve_scenario_id_fkey    INDEX     u   CREATE INDEX fki_txscenarioposturecurve_scenario_id_fkey ON public.txscenarioposturecurve USING btree (scenario_id);
 ?   DROP INDEX public.fki_txscenarioposturecurve_scenario_id_fkey;
       public         postgres    false    320            !           1259    111016 %   fki_txscenarioprocess_process_id_fkey    INDEX     i   CREATE INDEX fki_txscenarioprocess_process_id_fkey ON public.txscenarioprocess USING btree (process_id);
 9   DROP INDEX public.fki_txscenarioprocess_process_id_fkey;
       public         postgres    false    321            "           1259    111017 &   fki_txscenarioprocess_scenario_id_fkey    INDEX     k   CREATE INDEX fki_txscenarioprocess_scenario_id_fkey ON public.txscenarioprocess USING btree (scenario_id);
 :   DROP INDEX public.fki_txscenarioprocess_scenario_id_fkey;
       public         postgres    false    321                       1259    111018    posturedate_index    INDEX     [   CREATE INDEX posturedate_index ON public.txscenarioposturecurve USING hash (posture_date);
 %   DROP INDEX public.posturedate_index;
       public         postgres    false    320            �           1259    111019    sequence_index    INDEX     L   CREATE INDEX sequence_index ON public.txcalendarday USING btree (sequence);
 "   DROP INDEX public.sequence_index;
       public         postgres    false    305            /           2606    111020 ;   aba_stages_of_breeds_and_stages FK_id_aba_breeds_and_stages    FK CONSTRAINT     �   ALTER TABLE ONLY public.aba_stages_of_breeds_and_stages
    ADD CONSTRAINT "FK_id_aba_breeds_and_stages" FOREIGN KEY (id_aba_breeds_and_stages) REFERENCES public.aba_breeds_and_stages(id) ON DELETE CASCADE;
 g   ALTER TABLE ONLY public.aba_stages_of_breeds_and_stages DROP CONSTRAINT "FK_id_aba_breeds_and_stages";
       public       postgres    false    198    214    3109            %           2606    111025 9   aba_breeds_and_stages FK_id_aba_consumption_and_mortality    FK CONSTRAINT     �   ALTER TABLE ONLY public.aba_breeds_and_stages
    ADD CONSTRAINT "FK_id_aba_consumption_and_mortality" FOREIGN KEY (id_aba_consumption_and_mortality) REFERENCES public.aba_consumption_and_mortality(id);
 e   ALTER TABLE ONLY public.aba_breeds_and_stages DROP CONSTRAINT "FK_id_aba_consumption_and_mortality";
       public       postgres    false    198    200    3113            *           2606    111030 I   aba_consumption_and_mortality_detail FK_id_aba_consumption_and_mortality2    FK CONSTRAINT     �   ALTER TABLE ONLY public.aba_consumption_and_mortality_detail
    ADD CONSTRAINT "FK_id_aba_consumption_and_mortality2" FOREIGN KEY (id_aba_consumption_and_mortality) REFERENCES public.aba_consumption_and_mortality(id) ON DELETE CASCADE;
 u   ALTER TABLE ONLY public.aba_consumption_and_mortality_detail DROP CONSTRAINT "FK_id_aba_consumption_and_mortality2";
       public       postgres    false    3113    202    200            +           2606    111035 1   aba_elements_and_concentrations FK_id_aba_element    FK CONSTRAINT     �   ALTER TABLE ONLY public.aba_elements_and_concentrations
    ADD CONSTRAINT "FK_id_aba_element" FOREIGN KEY (id_aba_element) REFERENCES public.aba_elements(id);
 ]   ALTER TABLE ONLY public.aba_elements_and_concentrations DROP CONSTRAINT "FK_id_aba_element";
       public       postgres    false    206    204    3121            .           2606    111040    aba_results FK_id_aba_element2    FK CONSTRAINT     �   ALTER TABLE ONLY public.aba_results
    ADD CONSTRAINT "FK_id_aba_element2" FOREIGN KEY (id_aba_element) REFERENCES public.aba_elements(id);
 J   ALTER TABLE ONLY public.aba_results DROP CONSTRAINT "FK_id_aba_element2";
       public       postgres    false    3121    212    204            ,           2606    111045 :   aba_elements_and_concentrations FK_id_aba_element_property    FK CONSTRAINT     �   ALTER TABLE ONLY public.aba_elements_and_concentrations
    ADD CONSTRAINT "FK_id_aba_element_property" FOREIGN KEY (id_aba_element_property) REFERENCES public.aba_elements_properties(id);
 f   ALTER TABLE ONLY public.aba_elements_and_concentrations DROP CONSTRAINT "FK_id_aba_element_property";
       public       postgres    false    3128    208    206            -           2606    111050 5   aba_elements_and_concentrations FK_id_aba_formulation    FK CONSTRAINT     �   ALTER TABLE ONLY public.aba_elements_and_concentrations
    ADD CONSTRAINT "FK_id_aba_formulation" FOREIGN KEY (id_aba_formulation) REFERENCES public.aba_formulation(id) ON DELETE CASCADE;
 a   ALTER TABLE ONLY public.aba_elements_and_concentrations DROP CONSTRAINT "FK_id_aba_formulation";
       public       postgres    false    210    3130    206            '           2606    111055 1   aba_consumption_and_mortality FK_id_aba_time_unit    FK CONSTRAINT     �   ALTER TABLE ONLY public.aba_consumption_and_mortality
    ADD CONSTRAINT "FK_id_aba_time_unit" FOREIGN KEY (id_aba_time_unit) REFERENCES public.aba_time_unit(id);
 ]   ALTER TABLE ONLY public.aba_consumption_and_mortality DROP CONSTRAINT "FK_id_aba_time_unit";
       public       postgres    false    215    200    3139            (           2606    111060 )   aba_consumption_and_mortality FK_id_breed    FK CONSTRAINT     �   ALTER TABLE ONLY public.aba_consumption_and_mortality
    ADD CONSTRAINT "FK_id_breed" FOREIGN KEY (id_breed) REFERENCES public.mdbreed(breed_id);
 U   ALTER TABLE ONLY public.aba_consumption_and_mortality DROP CONSTRAINT "FK_id_breed";
       public       postgres    false    250    200    3149            0           2606    111065 1   aba_stages_of_breeds_and_stages FK_id_formulation    FK CONSTRAINT     �   ALTER TABLE ONLY public.aba_stages_of_breeds_and_stages
    ADD CONSTRAINT "FK_id_formulation" FOREIGN KEY (id_formulation) REFERENCES public.aba_formulation(id);
 ]   ALTER TABLE ONLY public.aba_stages_of_breeds_and_stages DROP CONSTRAINT "FK_id_formulation";
       public       postgres    false    214    210    3130            &           2606    111070 #   aba_breeds_and_stages FK_id_process    FK CONSTRAINT     �   ALTER TABLE ONLY public.aba_breeds_and_stages
    ADD CONSTRAINT "FK_id_process" FOREIGN KEY (id_process) REFERENCES public.mdprocess(process_id);
 O   ALTER TABLE ONLY public.aba_breeds_and_stages DROP CONSTRAINT "FK_id_process";
       public       postgres    false    258    3165    198            )           2606    111075 )   aba_consumption_and_mortality FK_id_stage    FK CONSTRAINT     �   ALTER TABLE ONLY public.aba_consumption_and_mortality
    ADD CONSTRAINT "FK_id_stage" FOREIGN KEY (id_stage) REFERENCES public.mdstage(stage_id);
 U   ALTER TABLE ONLY public.aba_consumption_and_mortality DROP CONSTRAINT "FK_id_stage";
       public       postgres    false    3184    268    200            1           2606    111080 7   mdapplication_rol mdapplication_rol_application_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.mdapplication_rol
    ADD CONSTRAINT mdapplication_rol_application_id_fkey FOREIGN KEY (application_id) REFERENCES public.mdapplication(application_id);
 a   ALTER TABLE ONLY public.mdapplication_rol DROP CONSTRAINT mdapplication_rol_application_id_fkey;
       public       postgres    false    247    3141    249            2           2606    111085 /   mdapplication_rol mdapplication_rol_rol_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.mdapplication_rol
    ADD CONSTRAINT mdapplication_rol_rol_id_fkey FOREIGN KEY (rol_id) REFERENCES public.mdrol(rol_id);
 Y   ALTER TABLE ONLY public.mdapplication_rol DROP CONSTRAINT mdapplication_rol_rol_id_fkey;
       public       postgres    false    262    3171    249            3           2606    111090 '   mdparameter mdparameter_measure_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.mdparameter
    ADD CONSTRAINT mdparameter_measure_id_fkey FOREIGN KEY (measure_id) REFERENCES public.mdmeasure(measure_id) ON UPDATE CASCADE ON DELETE CASCADE;
 Q   ALTER TABLE ONLY public.mdparameter DROP CONSTRAINT mdparameter_measure_id_fkey;
       public       postgres    false    256    254    3155            4           2606    111095 '   mdparameter mdparameter_process_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.mdparameter
    ADD CONSTRAINT mdparameter_process_id_fkey FOREIGN KEY (process_id) REFERENCES public.mdprocess(process_id) ON UPDATE CASCADE ON DELETE CASCADE;
 Q   ALTER TABLE ONLY public.mdparameter DROP CONSTRAINT mdparameter_process_id_fkey;
       public       postgres    false    258    3165    256            5           2606    111100 !   mdprocess mdprocess_breed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.mdprocess
    ADD CONSTRAINT mdprocess_breed_id_fkey FOREIGN KEY (breed_id) REFERENCES public.mdbreed(breed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 K   ALTER TABLE ONLY public.mdprocess DROP CONSTRAINT mdprocess_breed_id_fkey;
       public       postgres    false    250    258    3149            6           2606    111105 $   mdprocess mdprocess_calendar_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.mdprocess
    ADD CONSTRAINT mdprocess_calendar_id_fkey FOREIGN KEY (calendar_id) REFERENCES public.txcalendar(calendar_id) ON UPDATE CASCADE ON DELETE CASCADE;
 N   ALTER TABLE ONLY public.mdprocess DROP CONSTRAINT mdprocess_calendar_id_fkey;
       public       postgres    false    3286    304    258            7           2606    111110 #   mdprocess mdprocess_product_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.mdprocess
    ADD CONSTRAINT mdprocess_product_id_fkey FOREIGN KEY (product_id) REFERENCES public.mdproduct(product_id) ON UPDATE CASCADE ON DELETE CASCADE;
 M   ALTER TABLE ONLY public.mdprocess DROP CONSTRAINT mdprocess_product_id_fkey;
       public       postgres    false    3169    258    260            8           2606    111115 !   mdprocess mdprocess_stage_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.mdprocess
    ADD CONSTRAINT mdprocess_stage_id_fkey FOREIGN KEY (stage_id) REFERENCES public.mdstage(stage_id) ON UPDATE CASCADE ON DELETE CASCADE;
 K   ALTER TABLE ONLY public.mdprocess DROP CONSTRAINT mdprocess_stage_id_fkey;
       public       postgres    false    268    258    3184            9           2606    111120 &   mdscenario mdscenario_calendar_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.mdscenario
    ADD CONSTRAINT mdscenario_calendar_id_fkey FOREIGN KEY (calendar_id) REFERENCES public.txcalendar(calendar_id);
 P   ALTER TABLE ONLY public.mdscenario DROP CONSTRAINT mdscenario_calendar_id_fkey;
       public       postgres    false    264    3286    304            :           2606    111125    mduser mduser_rol_id_fkey    FK CONSTRAINT     {   ALTER TABLE ONLY public.mduser
    ADD CONSTRAINT mduser_rol_id_fkey FOREIGN KEY (rol_id) REFERENCES public.mdrol(rol_id);
 C   ALTER TABLE ONLY public.mduser DROP CONSTRAINT mduser_rol_id_fkey;
       public       postgres    false    270    3171    262            ;           2606    111130    oscenter oscenter_farm_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.oscenter
    ADD CONSTRAINT oscenter_farm_id_fkey FOREIGN KEY (farm_id) REFERENCES public.osfarm(farm_id) ON UPDATE CASCADE ON DELETE CASCADE;
 H   ALTER TABLE ONLY public.oscenter DROP CONSTRAINT oscenter_farm_id_fkey;
       public       postgres    false    3207    271    273            =           2606    111135 8   oscenter_oswarehouse oscenter_oswarehouse_center_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.oscenter_oswarehouse
    ADD CONSTRAINT oscenter_oswarehouse_center_id_fkey FOREIGN KEY (center_id) REFERENCES public.oscenter(center_id) ON UPDATE CASCADE ON DELETE CASCADE;
 b   ALTER TABLE ONLY public.oscenter_oswarehouse DROP CONSTRAINT oscenter_oswarehouse_center_id_fkey;
       public       postgres    false    271    3195    272            >           2606    111140 6   oscenter_oswarehouse oscenter_oswarehouse_farm_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.oscenter_oswarehouse
    ADD CONSTRAINT oscenter_oswarehouse_farm_id_fkey FOREIGN KEY (farm_id) REFERENCES public.osfarm(farm_id) ON UPDATE CASCADE ON DELETE CASCADE;
 `   ALTER TABLE ONLY public.oscenter_oswarehouse DROP CONSTRAINT oscenter_oswarehouse_farm_id_fkey;
       public       postgres    false    272    3207    273            ?           2606    111145 =   oscenter_oswarehouse oscenter_oswarehouse_partnership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.oscenter_oswarehouse
    ADD CONSTRAINT oscenter_oswarehouse_partnership_id_fkey FOREIGN KEY (partnership_id) REFERENCES public.ospartnership(partnership_id) ON UPDATE CASCADE ON DELETE CASCADE;
 g   ALTER TABLE ONLY public.oscenter_oswarehouse DROP CONSTRAINT oscenter_oswarehouse_partnership_id_fkey;
       public       postgres    false    277    3221    272            @           2606    111150 ;   oscenter_oswarehouse oscenter_oswarehouse_warehouse_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.oscenter_oswarehouse
    ADD CONSTRAINT oscenter_oswarehouse_warehouse_id_fkey FOREIGN KEY (warehouse_id) REFERENCES public.oswarehouse(warehouse_id) ON UPDATE CASCADE ON DELETE CASCADE;
 e   ALTER TABLE ONLY public.oscenter_oswarehouse DROP CONSTRAINT oscenter_oswarehouse_warehouse_id_fkey;
       public       postgres    false    286    272    3249            <           2606    111155 %   oscenter oscenter_partnership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.oscenter
    ADD CONSTRAINT oscenter_partnership_id_fkey FOREIGN KEY (partnership_id) REFERENCES public.ospartnership(partnership_id) ON UPDATE CASCADE ON DELETE CASCADE;
 O   ALTER TABLE ONLY public.oscenter DROP CONSTRAINT oscenter_partnership_id_fkey;
       public       postgres    false    277    271    3221            A           2606    111160    osfarm osfarm_farm_type_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.osfarm
    ADD CONSTRAINT osfarm_farm_type_id_fkey FOREIGN KEY (farm_type_id) REFERENCES public.mdfarmtype(farm_type_id) ON UPDATE CASCADE ON DELETE CASCADE;
 I   ALTER TABLE ONLY public.osfarm DROP CONSTRAINT osfarm_farm_type_id_fkey;
       public       postgres    false    252    273    3153            B           2606    111165 !   osfarm osfarm_partnership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.osfarm
    ADD CONSTRAINT osfarm_partnership_id_fkey FOREIGN KEY (partnership_id) REFERENCES public.ospartnership(partnership_id) ON UPDATE CASCADE ON DELETE CASCADE;
 K   ALTER TABLE ONLY public.osfarm DROP CONSTRAINT osfarm_partnership_id_fkey;
       public       postgres    false    277    3221    273            C           2606    111170 /   osincubator osincubator_incubator_plant_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.osincubator
    ADD CONSTRAINT osincubator_incubator_plant_id_fkey FOREIGN KEY (incubator_plant_id) REFERENCES public.osincubatorplant(incubator_plant_id) ON UPDATE CASCADE ON DELETE CASCADE;
 Y   ALTER TABLE ONLY public.osincubator DROP CONSTRAINT osincubator_incubator_plant_id_fkey;
       public       postgres    false    274    275    3217            D           2606    111175 5   osincubatorplant osincubatorplant_partnership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.osincubatorplant
    ADD CONSTRAINT osincubatorplant_partnership_id_fkey FOREIGN KEY (partnership_id) REFERENCES public.ospartnership(partnership_id) ON UPDATE CASCADE ON DELETE CASCADE;
 _   ALTER TABLE ONLY public.osincubatorplant DROP CONSTRAINT osincubatorplant_partnership_id_fkey;
       public       postgres    false    275    277    3221            E           2606    111180    osshed osshed_center_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.osshed
    ADD CONSTRAINT osshed_center_id_fkey FOREIGN KEY (center_id) REFERENCES public.oscenter(center_id) ON UPDATE CASCADE ON DELETE CASCADE;
 F   ALTER TABLE ONLY public.osshed DROP CONSTRAINT osshed_center_id_fkey;
       public       postgres    false    279    3195    271            F           2606    111185    osshed osshed_farm_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.osshed
    ADD CONSTRAINT osshed_farm_id_fkey FOREIGN KEY (farm_id) REFERENCES public.osfarm(farm_id) ON UPDATE CASCADE ON DELETE CASCADE;
 D   ALTER TABLE ONLY public.osshed DROP CONSTRAINT osshed_farm_id_fkey;
       public       postgres    false    3207    279    273            G           2606    111190 !   osshed osshed_partnership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.osshed
    ADD CONSTRAINT osshed_partnership_id_fkey FOREIGN KEY (partnership_id) REFERENCES public.ospartnership(partnership_id) ON UPDATE CASCADE ON DELETE CASCADE;
 K   ALTER TABLE ONLY public.osshed DROP CONSTRAINT osshed_partnership_id_fkey;
       public       postgres    false    277    279    3221            H           2606    111195     osshed osshed_statusshed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.osshed
    ADD CONSTRAINT osshed_statusshed_id_fkey FOREIGN KEY (statusshed_id) REFERENCES public.mdshedstatus(shed_status_id) ON UPDATE CASCADE ON DELETE CASCADE;
 J   ALTER TABLE ONLY public.osshed DROP CONSTRAINT osshed_statusshed_id_fkey;
       public       postgres    false    266    279    3180            I           2606    111200    ossilo ossilo_center_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ossilo
    ADD CONSTRAINT ossilo_center_id_fkey FOREIGN KEY (center_id) REFERENCES public.oscenter(center_id) ON UPDATE CASCADE ON DELETE CASCADE;
 F   ALTER TABLE ONLY public.ossilo DROP CONSTRAINT ossilo_center_id_fkey;
       public       postgres    false    271    281    3195            J           2606    111205    ossilo ossilo_farm_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ossilo
    ADD CONSTRAINT ossilo_farm_id_fkey FOREIGN KEY (farm_id) REFERENCES public.osfarm(farm_id) ON UPDATE CASCADE ON DELETE CASCADE;
 D   ALTER TABLE ONLY public.ossilo DROP CONSTRAINT ossilo_farm_id_fkey;
       public       postgres    false    3207    281    273            L           2606    111210 *   ossilo_osshed ossilo_osshed_center_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ossilo_osshed
    ADD CONSTRAINT ossilo_osshed_center_id_fkey FOREIGN KEY (center_id) REFERENCES public.oscenter(center_id) ON UPDATE CASCADE ON DELETE CASCADE;
 T   ALTER TABLE ONLY public.ossilo_osshed DROP CONSTRAINT ossilo_osshed_center_id_fkey;
       public       postgres    false    282    3195    271            M           2606    111215 (   ossilo_osshed ossilo_osshed_farm_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ossilo_osshed
    ADD CONSTRAINT ossilo_osshed_farm_id_fkey FOREIGN KEY (farm_id) REFERENCES public.osfarm(farm_id) ON UPDATE CASCADE ON DELETE CASCADE;
 R   ALTER TABLE ONLY public.ossilo_osshed DROP CONSTRAINT ossilo_osshed_farm_id_fkey;
       public       postgres    false    273    3207    282            N           2606    111220 /   ossilo_osshed ossilo_osshed_partnership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ossilo_osshed
    ADD CONSTRAINT ossilo_osshed_partnership_id_fkey FOREIGN KEY (partnership_id) REFERENCES public.ospartnership(partnership_id) ON UPDATE CASCADE ON DELETE CASCADE;
 Y   ALTER TABLE ONLY public.ossilo_osshed DROP CONSTRAINT ossilo_osshed_partnership_id_fkey;
       public       postgres    false    282    3221    277            O           2606    111225 (   ossilo_osshed ossilo_osshed_shed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ossilo_osshed
    ADD CONSTRAINT ossilo_osshed_shed_id_fkey FOREIGN KEY (shed_id) REFERENCES public.osshed(shed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 R   ALTER TABLE ONLY public.ossilo_osshed DROP CONSTRAINT ossilo_osshed_shed_id_fkey;
       public       postgres    false    3227    279    282            P           2606    111230 (   ossilo_osshed ossilo_osshed_silo_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ossilo_osshed
    ADD CONSTRAINT ossilo_osshed_silo_id_fkey FOREIGN KEY (silo_id) REFERENCES public.ossilo(silo_id) ON UPDATE CASCADE ON DELETE CASCADE;
 R   ALTER TABLE ONLY public.ossilo_osshed DROP CONSTRAINT ossilo_osshed_silo_id_fkey;
       public       postgres    false    281    282    3234            K           2606    111235 !   ossilo ossilo_partnership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ossilo
    ADD CONSTRAINT ossilo_partnership_id_fkey FOREIGN KEY (partnership_id) REFERENCES public.ospartnership(partnership_id) ON UPDATE CASCADE ON DELETE CASCADE;
 K   ALTER TABLE ONLY public.ossilo DROP CONSTRAINT ossilo_partnership_id_fkey;
       public       postgres    false    3221    277    281            Q           2606    111240 $   oswarehouse oswarehouse_farm_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.oswarehouse
    ADD CONSTRAINT oswarehouse_farm_id_fkey FOREIGN KEY (farm_id) REFERENCES public.osfarm(farm_id) ON UPDATE CASCADE ON DELETE CASCADE;
 N   ALTER TABLE ONLY public.oswarehouse DROP CONSTRAINT oswarehouse_farm_id_fkey;
       public       postgres    false    273    3207    286            R           2606    111245 +   oswarehouse oswarehouse_partnership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.oswarehouse
    ADD CONSTRAINT oswarehouse_partnership_id_fkey FOREIGN KEY (partnership_id) REFERENCES public.ospartnership(partnership_id) ON UPDATE CASCADE ON DELETE CASCADE;
 U   ALTER TABLE ONLY public.oswarehouse DROP CONSTRAINT oswarehouse_partnership_id_fkey;
       public       postgres    false    3221    277    286            S           2606    111250 5   txavailabilitysheds txavailabilitysheds_lot_code_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txavailabilitysheds
    ADD CONSTRAINT txavailabilitysheds_lot_code_fkey FOREIGN KEY (lot_code) REFERENCES public.txlot(lot_code) ON UPDATE CASCADE ON DELETE CASCADE;
 _   ALTER TABLE ONLY public.txavailabilitysheds DROP CONSTRAINT txavailabilitysheds_lot_code_fkey;
       public       postgres    false    313    297    3327            T           2606    111255 4   txavailabilitysheds txavailabilitysheds_shed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txavailabilitysheds
    ADD CONSTRAINT txavailabilitysheds_shed_id_fkey FOREIGN KEY (shed_id) REFERENCES public.osshed(shed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 ^   ALTER TABLE ONLY public.txavailabilitysheds DROP CONSTRAINT txavailabilitysheds_shed_id_fkey;
       public       postgres    false    3227    297    279            U           2606    111260 !   txbroiler txbroiler_breed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroiler
    ADD CONSTRAINT txbroiler_breed_id_fkey FOREIGN KEY (breed_id) REFERENCES public.mdbreed(breed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 K   ALTER TABLE ONLY public.txbroiler DROP CONSTRAINT txbroiler_breed_id_fkey;
       public       postgres    false    3149    250    298            Y           2606    111265 1   txbroiler_detail txbroiler_detail_broiler_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroiler_detail
    ADD CONSTRAINT txbroiler_detail_broiler_id_fkey FOREIGN KEY (broiler_id) REFERENCES public.txbroiler(broiler_id) ON UPDATE CASCADE ON DELETE CASCADE;
 [   ALTER TABLE ONLY public.txbroiler_detail DROP CONSTRAINT txbroiler_detail_broiler_id_fkey;
       public       postgres    false    298    299    3259            Z           2606    111270 .   txbroiler_detail txbroiler_detail_farm_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroiler_detail
    ADD CONSTRAINT txbroiler_detail_farm_id_fkey FOREIGN KEY (farm_id) REFERENCES public.osfarm(farm_id) ON UPDATE CASCADE ON DELETE CASCADE;
 X   ALTER TABLE ONLY public.txbroiler_detail DROP CONSTRAINT txbroiler_detail_farm_id_fkey;
       public       postgres    false    299    273    3207            [           2606    111275 .   txbroiler_detail txbroiler_detail_shed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroiler_detail
    ADD CONSTRAINT txbroiler_detail_shed_id_fkey FOREIGN KEY (shed_id) REFERENCES public.osshed(shed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 X   ALTER TABLE ONLY public.txbroiler_detail DROP CONSTRAINT txbroiler_detail_shed_id_fkey;
       public       postgres    false    3227    299    279            V           2606    111280 '   txbroiler txbroiler_partnership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroiler
    ADD CONSTRAINT txbroiler_partnership_id_fkey FOREIGN KEY (partnership_id) REFERENCES public.ospartnership(partnership_id) ON UPDATE CASCADE ON DELETE CASCADE;
 Q   ALTER TABLE ONLY public.txbroiler DROP CONSTRAINT txbroiler_partnership_id_fkey;
       public       postgres    false    277    3221    298            W           2606    111285 +   txbroiler txbroiler_programmed_eggs_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroiler
    ADD CONSTRAINT txbroiler_programmed_eggs_id_fkey FOREIGN KEY (programmed_eggs_id) REFERENCES public.txprogrammed_eggs(programmed_eggs_id) ON UPDATE CASCADE ON DELETE CASCADE;
 U   ALTER TABLE ONLY public.txbroiler DROP CONSTRAINT txbroiler_programmed_eggs_id_fkey;
       public       postgres    false    298    316    3339            X           2606    111290 $   txbroiler txbroiler_scenario_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroiler
    ADD CONSTRAINT txbroiler_scenario_id_fkey FOREIGN KEY (scenario_id) REFERENCES public.mdscenario(scenario_id) ON UPDATE CASCADE ON DELETE CASCADE;
 N   ALTER TABLE ONLY public.txbroiler DROP CONSTRAINT txbroiler_scenario_id_fkey;
       public       postgres    false    3178    264    298            \           2606    111295 1   txbroilereviction txbroilereviction_breed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroilereviction
    ADD CONSTRAINT txbroilereviction_breed_id_fkey FOREIGN KEY (breed_id) REFERENCES public.mdbreed(breed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 [   ALTER TABLE ONLY public.txbroilereviction DROP CONSTRAINT txbroilereviction_breed_id_fkey;
       public       postgres    false    300    250    3149            _           2606    122114 :   txbroilereviction txbroilereviction_broiler_detail_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroilereviction
    ADD CONSTRAINT txbroilereviction_broiler_detail_id_fkey FOREIGN KEY (broiler_detail_id) REFERENCES public.txbroiler_detail(broiler_detail_id);
 d   ALTER TABLE ONLY public.txbroilereviction DROP CONSTRAINT txbroilereviction_broiler_detail_id_fkey;
       public       postgres    false    3264    300    299            `           2606    111300 A   txbroilereviction_detail txbroilereviction_detail_broiler_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroilereviction_detail
    ADD CONSTRAINT txbroilereviction_detail_broiler_id_fkey FOREIGN KEY (broilereviction_id) REFERENCES public.txbroilereviction(broilereviction_id) ON UPDATE CASCADE ON DELETE CASCADE;
 k   ALTER TABLE ONLY public.txbroilereviction_detail DROP CONSTRAINT txbroilereviction_detail_broiler_id_fkey;
       public       postgres    false    3269    301    300            a           2606    111305 I   txbroilereviction_detail txbroilereviction_detail_broiler_product_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroilereviction_detail
    ADD CONSTRAINT txbroilereviction_detail_broiler_product_id_fkey FOREIGN KEY (broiler_product_id) REFERENCES public.mdbroiler_product(broiler_product_id) ON UPDATE CASCADE ON DELETE CASCADE;
 s   ALTER TABLE ONLY public.txbroilereviction_detail DROP CONSTRAINT txbroilereviction_detail_broiler_product_id_fkey;
       public       postgres    false    251    3151    301            b           2606    111310 >   txbroilereviction_detail txbroilereviction_detail_farm_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroilereviction_detail
    ADD CONSTRAINT txbroilereviction_detail_farm_id_fkey FOREIGN KEY (farm_id) REFERENCES public.osfarm(farm_id) ON UPDATE CASCADE ON DELETE CASCADE;
 h   ALTER TABLE ONLY public.txbroilereviction_detail DROP CONSTRAINT txbroilereviction_detail_farm_id_fkey;
       public       postgres    false    3207    301    273            c           2606    111315 >   txbroilereviction_detail txbroilereviction_detail_shed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroilereviction_detail
    ADD CONSTRAINT txbroilereviction_detail_shed_id_fkey FOREIGN KEY (shed_id) REFERENCES public.osshed(shed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 h   ALTER TABLE ONLY public.txbroilereviction_detail DROP CONSTRAINT txbroilereviction_detail_shed_id_fkey;
       public       postgres    false    3227    279    301            d           2606    111320 H   txbroilereviction_detail txbroilereviction_detail_slaughterhouse_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroilereviction_detail
    ADD CONSTRAINT txbroilereviction_detail_slaughterhouse_id_fkey FOREIGN KEY (slaughterhouse_id) REFERENCES public.osslaughterhouse(slaughterhouse_id) ON UPDATE CASCADE ON DELETE CASCADE;
 r   ALTER TABLE ONLY public.txbroilereviction_detail DROP CONSTRAINT txbroilereviction_detail_slaughterhouse_id_fkey;
       public       postgres    false    284    301    3243            ]           2606    111325 7   txbroilereviction txbroilereviction_partnership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroilereviction
    ADD CONSTRAINT txbroilereviction_partnership_id_fkey FOREIGN KEY (partnership_id) REFERENCES public.ospartnership(partnership_id) ON UPDATE CASCADE ON DELETE CASCADE;
 a   ALTER TABLE ONLY public.txbroilereviction DROP CONSTRAINT txbroilereviction_partnership_id_fkey;
       public       postgres    false    3221    277    300            ^           2606    111330 4   txbroilereviction txbroilereviction_scenario_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroilereviction
    ADD CONSTRAINT txbroilereviction_scenario_id_fkey FOREIGN KEY (scenario_id) REFERENCES public.mdscenario(scenario_id) ON UPDATE CASCADE ON DELETE CASCADE;
 ^   ALTER TABLE ONLY public.txbroilereviction DROP CONSTRAINT txbroilereviction_scenario_id_fkey;
       public       postgres    false    300    264    3178            e           2606    111335 C   txbroilerproduct_detail txbroilerproduct_detail_broiler_detail_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroilerproduct_detail
    ADD CONSTRAINT txbroilerproduct_detail_broiler_detail_fkey FOREIGN KEY (broiler_detail) REFERENCES public.txbroiler_detail(broiler_detail_id) ON UPDATE CASCADE ON DELETE CASCADE;
 m   ALTER TABLE ONLY public.txbroilerproduct_detail DROP CONSTRAINT txbroilerproduct_detail_broiler_detail_fkey;
       public       postgres    false    302    3264    299            f           2606    111340 .   txbroodermachine txbroodermachine_farm_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroodermachine
    ADD CONSTRAINT txbroodermachine_farm_id_fkey FOREIGN KEY (farm_id) REFERENCES public.osfarm(farm_id) ON UPDATE CASCADE ON DELETE CASCADE;
 X   ALTER TABLE ONLY public.txbroodermachine DROP CONSTRAINT txbroodermachine_farm_id_fkey;
       public       postgres    false    303    3207    273            g           2606    111345 5   txbroodermachine txbroodermachine_partnership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroodermachine
    ADD CONSTRAINT txbroodermachine_partnership_id_fkey FOREIGN KEY (partnership_id) REFERENCES public.ospartnership(partnership_id) ON UPDATE CASCADE ON DELETE CASCADE;
 _   ALTER TABLE ONLY public.txbroodermachine DROP CONSTRAINT txbroodermachine_partnership_id_fkey;
       public       postgres    false    3221    277    303            h           2606    111350 ,   txcalendarday txcalendarday_calendar_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txcalendarday
    ADD CONSTRAINT txcalendarday_calendar_id_fkey FOREIGN KEY (calendar_id) REFERENCES public.txcalendar(calendar_id) ON UPDATE CASCADE ON DELETE CASCADE;
 V   ALTER TABLE ONLY public.txcalendarday DROP CONSTRAINT txcalendarday_calendar_id_fkey;
       public       postgres    false    304    3286    305            i           2606    111355 -   txeggs_planning txeggs_planning_breed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txeggs_planning
    ADD CONSTRAINT txeggs_planning_breed_id_fkey FOREIGN KEY (breed_id) REFERENCES public.mdbreed(breed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 W   ALTER TABLE ONLY public.txeggs_planning DROP CONSTRAINT txeggs_planning_breed_id_fkey;
       public       postgres    false    3149    250    306            j           2606    111360 0   txeggs_planning txeggs_planning_scenario_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txeggs_planning
    ADD CONSTRAINT txeggs_planning_scenario_id_fkey FOREIGN KEY (scenario_id) REFERENCES public.mdscenario(scenario_id);
 Z   ALTER TABLE ONLY public.txeggs_planning DROP CONSTRAINT txeggs_planning_scenario_id_fkey;
       public       postgres    false    3178    264    306            k           2606    111365 -   txeggs_required txeggs_required_breed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txeggs_required
    ADD CONSTRAINT txeggs_required_breed_id_fkey FOREIGN KEY (breed_id) REFERENCES public.mdbreed(breed_id);
 W   ALTER TABLE ONLY public.txeggs_required DROP CONSTRAINT txeggs_required_breed_id_fkey;
       public       postgres    false    307    3149    250            l           2606    111370 0   txeggs_required txeggs_required_scenario_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txeggs_required
    ADD CONSTRAINT txeggs_required_scenario_id_fkey FOREIGN KEY (scenario_id) REFERENCES public.mdscenario(scenario_id);
 Z   ALTER TABLE ONLY public.txeggs_required DROP CONSTRAINT txeggs_required_scenario_id_fkey;
       public       postgres    false    264    3178    307            m           2606    111375 +   txeggs_storage txeggs_storage_breed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txeggs_storage
    ADD CONSTRAINT txeggs_storage_breed_id_fkey FOREIGN KEY (breed_id) REFERENCES public.mdbreed(breed_id);
 U   ALTER TABLE ONLY public.txeggs_storage DROP CONSTRAINT txeggs_storage_breed_id_fkey;
       public       postgres    false    250    308    3149            n           2606    111380 5   txeggs_storage txeggs_storage_incubator_plant_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txeggs_storage
    ADD CONSTRAINT txeggs_storage_incubator_plant_id_fkey FOREIGN KEY (incubator_plant_id) REFERENCES public.osincubatorplant(incubator_plant_id);
 _   ALTER TABLE ONLY public.txeggs_storage DROP CONSTRAINT txeggs_storage_incubator_plant_id_fkey;
       public       postgres    false    275    3217    308            o           2606    111385 .   txeggs_storage txeggs_storage_scenario_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txeggs_storage
    ADD CONSTRAINT txeggs_storage_scenario_id_fkey FOREIGN KEY (scenario_id) REFERENCES public.mdscenario(scenario_id);
 X   ALTER TABLE ONLY public.txeggs_storage DROP CONSTRAINT txeggs_storage_scenario_id_fkey;
       public       postgres    false    264    3178    308            p           2606    111390 '   txgoals_erp txgoals_erp_product_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txgoals_erp
    ADD CONSTRAINT txgoals_erp_product_id_fkey FOREIGN KEY (product_id) REFERENCES public.mdproduct(product_id);
 Q   ALTER TABLE ONLY public.txgoals_erp DROP CONSTRAINT txgoals_erp_product_id_fkey;
       public       postgres    false    3169    260    309            q           2606    111395 (   txgoals_erp txgoals_erp_scenario_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txgoals_erp
    ADD CONSTRAINT txgoals_erp_scenario_id_fkey FOREIGN KEY (scenario_id) REFERENCES public.mdscenario(scenario_id);
 R   ALTER TABLE ONLY public.txgoals_erp DROP CONSTRAINT txgoals_erp_scenario_id_fkey;
       public       postgres    false    309    3178    264            r           2606    111400 '   txhousingway txhousingway_breed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txhousingway
    ADD CONSTRAINT txhousingway_breed_id_fkey FOREIGN KEY (breed_id) REFERENCES public.mdbreed(breed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 Q   ALTER TABLE ONLY public.txhousingway DROP CONSTRAINT txhousingway_breed_id_fkey;
       public       postgres    false    250    3149    311            v           2606    111405 4   txhousingway_detail txhousingway_detail_farm_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txhousingway_detail
    ADD CONSTRAINT txhousingway_detail_farm_id_fkey FOREIGN KEY (farm_id) REFERENCES public.osfarm(farm_id) ON UPDATE CASCADE ON DELETE CASCADE;
 ^   ALTER TABLE ONLY public.txhousingway_detail DROP CONSTRAINT txhousingway_detail_farm_id_fkey;
       public       postgres    false    273    3207    312            w           2606    111410 ;   txhousingway_detail txhousingway_detail_housing_way_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txhousingway_detail
    ADD CONSTRAINT txhousingway_detail_housing_way_id_fkey FOREIGN KEY (housing_way_id) REFERENCES public.txhousingway(housing_way_id) ON UPDATE CASCADE ON DELETE CASCADE;
 e   ALTER TABLE ONLY public.txhousingway_detail DROP CONSTRAINT txhousingway_detail_housing_way_id_fkey;
       public       postgres    false    3314    312    311            x           2606    111415 4   txhousingway_detail txhousingway_detail_shed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txhousingway_detail
    ADD CONSTRAINT txhousingway_detail_shed_id_fkey FOREIGN KEY (shed_id) REFERENCES public.osshed(shed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 ^   ALTER TABLE ONLY public.txhousingway_detail DROP CONSTRAINT txhousingway_detail_shed_id_fkey;
       public       postgres    false    312    3227    279            s           2606    111420 -   txhousingway txhousingway_partnership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txhousingway
    ADD CONSTRAINT txhousingway_partnership_id_fkey FOREIGN KEY (partnership_id) REFERENCES public.ospartnership(partnership_id) ON UPDATE CASCADE ON DELETE CASCADE;
 W   ALTER TABLE ONLY public.txhousingway DROP CONSTRAINT txhousingway_partnership_id_fkey;
       public       postgres    false    311    3221    277            t           2606    111425 *   txhousingway txhousingway_scenario_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txhousingway
    ADD CONSTRAINT txhousingway_scenario_id_fkey FOREIGN KEY (scenario_id) REFERENCES public.mdscenario(scenario_id) ON UPDATE CASCADE ON DELETE CASCADE;
 T   ALTER TABLE ONLY public.txhousingway DROP CONSTRAINT txhousingway_scenario_id_fkey;
       public       postgres    false    311    3178    264            u           2606    111430 '   txhousingway txhousingway_stage_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txhousingway
    ADD CONSTRAINT txhousingway_stage_id_fkey FOREIGN KEY (stage_id) REFERENCES public.mdstage(stage_id);
 Q   ALTER TABLE ONLY public.txhousingway DROP CONSTRAINT txhousingway_stage_id_fkey;
       public       postgres    false    268    311    3184            y           2606    111435    txlot txlot_breed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txlot
    ADD CONSTRAINT txlot_breed_id_fkey FOREIGN KEY (breed_id) REFERENCES public.mdbreed(breed_id);
 C   ALTER TABLE ONLY public.txlot DROP CONSTRAINT txlot_breed_id_fkey;
       public       postgres    false    3149    313    250            z           2606    111440    txlot txlot_farm_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txlot
    ADD CONSTRAINT txlot_farm_id_fkey FOREIGN KEY (farm_id) REFERENCES public.osfarm(farm_id) ON UPDATE CASCADE ON DELETE CASCADE;
 B   ALTER TABLE ONLY public.txlot DROP CONSTRAINT txlot_farm_id_fkey;
       public       postgres    false    3207    313    273            {           2606    111445    txlot txlot_housing_way_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txlot
    ADD CONSTRAINT txlot_housing_way_id_fkey FOREIGN KEY (housing_way_id) REFERENCES public.txhousingway(housing_way_id) ON UPDATE CASCADE ON DELETE CASCADE;
 I   ALTER TABLE ONLY public.txlot DROP CONSTRAINT txlot_housing_way_id_fkey;
       public       postgres    false    311    3314    313            |           2606    111450    txlot txlot_product_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txlot
    ADD CONSTRAINT txlot_product_id_fkey FOREIGN KEY (product_id) REFERENCES public.mdproduct(product_id);
 E   ALTER TABLE ONLY public.txlot DROP CONSTRAINT txlot_product_id_fkey;
       public       postgres    false    3169    260    313            }           2606    111455    txlot txlot_shed_id_fkey    FK CONSTRAINT     }   ALTER TABLE ONLY public.txlot
    ADD CONSTRAINT txlot_shed_id_fkey FOREIGN KEY (shed_id) REFERENCES public.osshed(shed_id);
 B   ALTER TABLE ONLY public.txlot DROP CONSTRAINT txlot_shed_id_fkey;
       public       postgres    false    279    313    3227            ~           2606    111460 +   txposturecurve txposturecurve_breed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txposturecurve
    ADD CONSTRAINT txposturecurve_breed_id_fkey FOREIGN KEY (breed_id) REFERENCES public.mdbreed(breed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 U   ALTER TABLE ONLY public.txposturecurve DROP CONSTRAINT txposturecurve_breed_id_fkey;
       public       postgres    false    315    250    3149                       2606    111465 1   txprogrammed_eggs txprogrammed_eggs_breed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txprogrammed_eggs
    ADD CONSTRAINT txprogrammed_eggs_breed_id_fkey FOREIGN KEY (breed_id) REFERENCES public.mdbreed(breed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 [   ALTER TABLE ONLY public.txprogrammed_eggs DROP CONSTRAINT txprogrammed_eggs_breed_id_fkey;
       public       postgres    false    316    3149    250            �           2606    111470 8   txprogrammed_eggs txprogrammed_eggs_eggs_storage_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txprogrammed_eggs
    ADD CONSTRAINT txprogrammed_eggs_eggs_storage_id_fkey FOREIGN KEY (eggs_storage_id) REFERENCES public.txeggs_storage(eggs_storage_id) ON UPDATE CASCADE ON DELETE CASCADE;
 b   ALTER TABLE ONLY public.txprogrammed_eggs DROP CONSTRAINT txprogrammed_eggs_eggs_storage_id_fkey;
       public       postgres    false    308    3304    316            �           2606    111475 5   txprogrammed_eggs txprogrammed_eggs_incubator_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txprogrammed_eggs
    ADD CONSTRAINT txprogrammed_eggs_incubator_id_fkey FOREIGN KEY (incubator_id) REFERENCES public.osincubator(incubator_id) ON UPDATE CASCADE ON DELETE CASCADE;
 _   ALTER TABLE ONLY public.txprogrammed_eggs DROP CONSTRAINT txprogrammed_eggs_incubator_id_fkey;
       public       postgres    false    316    274    3212            �           2606    111480 3   txscenarioformula txscenarioformula_measure_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioformula
    ADD CONSTRAINT txscenarioformula_measure_id_fkey FOREIGN KEY (measure_id) REFERENCES public.mdmeasure(measure_id) ON UPDATE CASCADE ON DELETE CASCADE;
 ]   ALTER TABLE ONLY public.txscenarioformula DROP CONSTRAINT txscenarioformula_measure_id_fkey;
       public       postgres    false    3155    317    254            �           2606    111485 5   txscenarioformula txscenarioformula_parameter_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioformula
    ADD CONSTRAINT txscenarioformula_parameter_id_fkey FOREIGN KEY (parameter_id) REFERENCES public.mdparameter(parameter_id) ON UPDATE CASCADE ON DELETE CASCADE;
 _   ALTER TABLE ONLY public.txscenarioformula DROP CONSTRAINT txscenarioformula_parameter_id_fkey;
       public       postgres    false    256    317    3159            �           2606    111490 3   txscenarioformula txscenarioformula_process_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioformula
    ADD CONSTRAINT txscenarioformula_process_id_fkey FOREIGN KEY (process_id) REFERENCES public.mdprocess(process_id) ON UPDATE CASCADE;
 ]   ALTER TABLE ONLY public.txscenarioformula DROP CONSTRAINT txscenarioformula_process_id_fkey;
       public       postgres    false    258    317    3165            �           2606    111495 4   txscenarioformula txscenarioformula_scenario_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioformula
    ADD CONSTRAINT txscenarioformula_scenario_id_fkey FOREIGN KEY (scenario_id) REFERENCES public.mdscenario(scenario_id) ON UPDATE CASCADE ON DELETE CASCADE;
 ^   ALTER TABLE ONLY public.txscenarioformula DROP CONSTRAINT txscenarioformula_scenario_id_fkey;
       public       postgres    false    264    317    3178            �           2606    111500 9   txscenarioparameter txscenarioparameter_parameter_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioparameter
    ADD CONSTRAINT txscenarioparameter_parameter_id_fkey FOREIGN KEY (parameter_id) REFERENCES public.mdparameter(parameter_id) ON UPDATE CASCADE ON DELETE CASCADE;
 c   ALTER TABLE ONLY public.txscenarioparameter DROP CONSTRAINT txscenarioparameter_parameter_id_fkey;
       public       postgres    false    256    318    3159            �           2606    111505 7   txscenarioparameter txscenarioparameter_process_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioparameter
    ADD CONSTRAINT txscenarioparameter_process_id_fkey FOREIGN KEY (process_id) REFERENCES public.mdprocess(process_id) ON UPDATE CASCADE ON DELETE CASCADE;
 a   ALTER TABLE ONLY public.txscenarioparameter DROP CONSTRAINT txscenarioparameter_process_id_fkey;
       public       postgres    false    258    318    3165            �           2606    111510 8   txscenarioparameter txscenarioparameter_scenario_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioparameter
    ADD CONSTRAINT txscenarioparameter_scenario_id_fkey FOREIGN KEY (scenario_id) REFERENCES public.mdscenario(scenario_id) ON UPDATE CASCADE ON DELETE CASCADE;
 b   ALTER TABLE ONLY public.txscenarioparameter DROP CONSTRAINT txscenarioparameter_scenario_id_fkey;
       public       postgres    false    264    318    3178            �           2606    111515 ?   txscenarioparameterday txscenarioparameterday_parameter_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioparameterday
    ADD CONSTRAINT txscenarioparameterday_parameter_id_fkey FOREIGN KEY (parameter_id) REFERENCES public.mdparameter(parameter_id);
 i   ALTER TABLE ONLY public.txscenarioparameterday DROP CONSTRAINT txscenarioparameterday_parameter_id_fkey;
       public       postgres    false    256    319    3159            �           2606    111520 >   txscenarioparameterday txscenarioparameterday_scenario_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioparameterday
    ADD CONSTRAINT txscenarioparameterday_scenario_id_fkey FOREIGN KEY (scenario_id) REFERENCES public.mdscenario(scenario_id);
 h   ALTER TABLE ONLY public.txscenarioparameterday DROP CONSTRAINT txscenarioparameterday_scenario_id_fkey;
       public       postgres    false    264    319    3178            �           2606    111525 ;   txscenarioposturecurve txscenarioposturecurve_breed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioposturecurve
    ADD CONSTRAINT txscenarioposturecurve_breed_id_fkey FOREIGN KEY (breed_id) REFERENCES public.mdbreed(breed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 e   ALTER TABLE ONLY public.txscenarioposturecurve DROP CONSTRAINT txscenarioposturecurve_breed_id_fkey;
       public       postgres    false    3149    320    250            �           2606    111530 G   txscenarioposturecurve txscenarioposturecurve_housingway_detail_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioposturecurve
    ADD CONSTRAINT txscenarioposturecurve_housingway_detail_id_fkey FOREIGN KEY (housingway_detail_id) REFERENCES public.txhousingway_detail(housingway_detail_id) ON UPDATE CASCADE ON DELETE CASCADE;
 q   ALTER TABLE ONLY public.txscenarioposturecurve DROP CONSTRAINT txscenarioposturecurve_housingway_detail_id_fkey;
       public       postgres    false    320    312    3320            �           2606    111535 >   txscenarioposturecurve txscenarioposturecurve_scenario_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioposturecurve
    ADD CONSTRAINT txscenarioposturecurve_scenario_id_fkey FOREIGN KEY (scenario_id) REFERENCES public.mdscenario(scenario_id) ON UPDATE CASCADE ON DELETE CASCADE;
 h   ALTER TABLE ONLY public.txscenarioposturecurve DROP CONSTRAINT txscenarioposturecurve_scenario_id_fkey;
       public       postgres    false    3178    320    264            �           2606    111540 3   txscenarioprocess txscenarioprocess_process_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioprocess
    ADD CONSTRAINT txscenarioprocess_process_id_fkey FOREIGN KEY (process_id) REFERENCES public.mdprocess(process_id) ON UPDATE CASCADE ON DELETE CASCADE;
 ]   ALTER TABLE ONLY public.txscenarioprocess DROP CONSTRAINT txscenarioprocess_process_id_fkey;
       public       postgres    false    258    321    3165            �           2606    111545 4   txscenarioprocess txscenarioprocess_scenario_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioprocess
    ADD CONSTRAINT txscenarioprocess_scenario_id_fkey FOREIGN KEY (scenario_id) REFERENCES public.mdscenario(scenario_id) ON UPDATE CASCADE ON DELETE CASCADE;
 ^   ALTER TABLE ONLY public.txscenarioprocess DROP CONSTRAINT txscenarioprocess_scenario_id_fkey;
       public       postgres    false    264    321    3178                  x������ � �            x������ � �            x������ � �            x������ � �            x������ � �         4   x�3�4�TUHIU((�/I��K�2�4��NN��2�4�H%&�f��r��qqq '[�            x������ � �            x������ � �            x������ � �         "   x�3�N�M�K�R�\��.��&��b�=... ��
�      <   �   x�}��j�@�gݻ��i��kC��<@�8��tw���ƄL>}�%5�ċ��/D�3��0�JhV�!�[봨�մ7�<bh֎
��ZYj1�th�`4)+(/��S�������ˈ:���P�k;_ÀRHx`��/�#�Z�Ž���Qs�F�j#͇l��,c�X9��x,�����B�d-
�4���#?���#��f��k�Ɖ�      >   K   x���	 1��d��mmw���X�'� ������NL��l��ƶ��E�r��zP�g�I����w�{H�*�r      ?      x�3��200�t�wr�����  �'      @   +   x�32�����WHIUH�K�/JI�41�4�33@�\1z\\\ 0
�      A   9   x�3�J-(�O)MN�<�9�ˈ�5/=�(%�˘�'�,1�$U�R�����D�=... �6>      C   :   x�3����LIL���440�4�0����2���/rIMKM.�ʹpa	W� `�_      E      x�M�1�0�99�O���*�+�ob���'��IE]<|?w��j�J 3E�� O�!k4��#��"�qp��w��,�Z��.�,U-R�B�0��jMEKdk?*[`Ӳ�O�����S79�      G   �   x�}PA�0<OC�PK�l<h�ų�
hHL1��O�cn����a��t�f�@�C�L���F�u[���РT@{/�kqwͣ
�ǻlE��ԉ�xT��խmʾ(��+�	���<+��E�����4���y38tnCџ��(Iˍ���a��'p�G�9c9�o¥iK��	/���r�X�      I   s   x�U�K�0��)|�����˔��bd��z������}�y��\s��US�Y�d5ې�h���<r�?O�&Eiܿ�cB��ur�eRUz^НP�Ģ�|�0�j,�7��sC�#�"�      K   n   x�u�1�0F�99Hd��S���D�R�"��%#�o��8��{Y��M�gө@l�,q��Tg�(ڪ$*Q���ᒴX���eE}t�N��11/��H��c�F�+A      M   t   x�347�tvv�s��W
uurT0Tpuqtv�4204�54�50T02�2 !]������������s~QJ~R�����9���%���!g@Qi*H�aFX�9��dP� pD#3      O   S   x�3�t�,.���L�IU(H,JTp���J��L�+��'�eę�\Z�����L83�2�2s2K@��.cβ���k�W� 
�,{      Q   N   x�3�4�t�K�/JI�2�4���K.MJL����2�4�J-(�O)M.�/J,�2�4�t.:�6Q�R�'�,1�$�+F��� ۏ�      S   {   x�3�LL����,.)JL�/�T1JR14R	1�,t�p��J3Kr�ӏ�L��N7I��2-.N5N-NN	.L�
J���,+�t!���Y`�id`h�k D�
��V�V�&�&\1z\\\ �+�      T   �   x�%�A�0���p
N`h��p%�c\�� �&�5�o�<s��~:/U6�A�������e����g�|����PA�&��D�ٍx���Wc�b��ޅIs�B���i@O^�[q�a��Z.�-�b�>m���LI��C��k��Eв_I�p��0m����_���p��F~E_�m�$�?O      U      x������ � �      V   �   x�-�1�0��9EO���Ma����
!��b�D
	
e�6����Ƭ���4�)a}��?���e�{7�BK&�йDCLEW����^�,5��K�$[����0��#{��A�vt}�z�� �y�9� l(��|IԜ�U���5�݆5W9�|��2��C�prv���/!�q�9�      W   i   x�32�42��J�M-.O�T00��b��K3�2�K�S�`j8ML8A��-,���P�2�eL�Y�&� ��Q3f��0���Y Ÿb���� ��>3      X   4   x�32�H,J�,��424rr�J<�K�S�9��9�9��b���� 8�*      Z   �   x�U�Q� D��{ �`�X�ZI�����Ky&/&�U&�13o�lpUI9ֽh�"�"�f{�F�aȕESD�p�ϻR7�2��P.k%URq0g��{oC0�2���<p�1Ћ�}ܰ0�q�e���F���[^�R.pr�6�k��~�7�      \   �   x�u���0��g�T`�'�-x�9�-���"�āg@ݡ
i�'.�
��!�Qa��b|޸I�y9��p��u_}���@�x�ؠy�����i���p����g�|��+�%�mK?��h����8�      ^      x������ � �      _      x������ � �      a   E   x�36��I�+ITHIUpJ�KM�L��W00��M��L�G�M��V��E�ŉ)@N@Ŧ@����� OU�      c   {   x�u�=�0���9ENl��0�]�2 :nO�R�O���{-�(*I.�Z�6o��7���l����Dy@�h��
�2j@`b��"���p�Z�!Ȗ��l��ʟ��b�D�?$# ���� ��^a      n      x������ � �      o   B   x�U���@���f8�J���C_&�{�AP�l�F%�	����9�j��BX:o�����0�m_      p   R   x�m��	�0Cѳ�K��Ʊ�K���	�BCoB��e¨y0-4�;��ʼ�L2�����
�cLV;ӥ�_5�R���SD?��      q   H   x�M̱�0���Ł)K�K��#����3>�1k�`�XA�xT��?r"j7Ty�tQu��Ḡ����}Y��      r   W   x�����0C�s�KQ�I���A/�8���'ctA�!��bܥ���s���÷YO��b2�a_��m�d_�m3O���CU/0�-~      s      x������ � �      t      x������ � �      u   D   x�3��tN�I�KI,����IL�/JL�IEb��%+9�}\�\�<�������H�rr��qqq ȋ�      v      x�|][��:��v��'��=���h�E*,/�\��Sɢ�d�Ȕ�W�W
���_H���������x��ף��OZ(�@�%�������0��׿%(���/�����1���R׿�(���/���=(�@e|����A��d(���<(â��Pb�|���<`"��_0Y��ƙ��F�1}�y��p�H8�/�*���.�����=�É#u�X�p��ǃs�q�n�ׁ�=�Ñ#u��?�H���9\9RW��'�{<8�3G���x��xpwNԝS�¹������?���#������į����=���OԟS�:p�ǃs�s������{<8�?'�ϩ~�dy����D�9�/�"������?���S�=�ß��4�p��ǃs�s���>h��xp�ԟs�:p�ǃs�s������{�8��sZ'=N�>a����H&J��Y@���H&L���W�3�|��/E2q�L�2|9��|'�	T~`���ʱ0�+�v��^_O��C�i�*�m�G���!_��c�8�8��|���G/ �q��E+I��S�7N��p%i��p�U����c�$WN�
9����;��hxr:<��+I��FW�+�ÕY��4\q8�*��t�2W��+g��3�ÙY��4\q8�Û���,\I�8������pg���@�s�?�ßY��4\qV�·?�p%i������|�3W��+'_���?��+I�����9�����Yi�9�����iW�?�ßY��4\q8����|�3W��+��z���|�3W��+���
�Ɵ3�Y�W��Y���C�'_�f��\�C�'_��^�\�C�'_�f%����;��_0�jp��:y�������ɥ0�s��Y�os58r�q���������r���^F��na��z֟u���8ܸn�~ֳ��;�������c����g����Ñ����g=�Ϻ;�O��'����?����W��+����?�g9|���~ֳ��;�e!8s=����g�Yw8����zx3�Y����p�����pg����x��ß����g=�Ϻ;��;����~ֳ���7�:����~ֳ��;�u������g=�Ϻ�)׀?�ß��z֟u�S�n�?����?��]�܎��~ֳ��;�~�s;����g�Yw�5����g����gݿ�5���ع�ҲH���0s��Z���m�5�Ͷ�_��S��)�?�O
0�-6��(��(��ʲ-���VA}^E�W�uT�6?�.�<W���6t����$�8���2��8��yL]lE�RSW5�; ���i������I��1u��`���j��{G-3#,6Ei4�l;Ƭ��7��aj:7��߰O��q]���M��o0!��0�:��h���ܯ�������a��Y�����?Lm��NMݷ��2/�����CLݷ���g}�S�lq������^ b��O�L������#0��;5uWS��Dn���U�&`c'`��o�W��<ʠ����{](�������\�F�h�
�����q��<���s�e�A��֏߫j���hM��Y)|h)|����#(�U[L�4��;�����8��-rL�mL�6�Kk�_[��{�0xj��[��p��lj|���ak��LJ_L�/��S:�6u������/�RW�s�`�ڂ��0'{���QEa�[N�&�7�-H�Z$Jʅmo9�X�	{���u��Eaܟ.	7���b�]x���0��3@[�k�hW�]'?8k%��c�WV��0~��J�F�#��"+~�|J�E�L�+��j�h��=��>����2��f8�ȹ���?�| ,@3��ȫ�Q��I(-��M#�G-GW�_N��i3E=AbOjpW�]N��^�E�����۝��] E���!�wЌ�A3H��� TC�������c�&�+#Mo;}��`�v`9 �Ƕ������p��?2�֍�Q;^�v�|=}��m��=��҈�P�B���cZ���oB�� ۬��Gc/�0Ey=��!@�66#0�p��=S�r�x������H!{��_#c�R�ʜ�B�8 �E�A���*H!{��_"cW�)߭����x /ʸB���!{6�.�
)�
)��� ��%[v��B|�*�!{��B�^!��z��!{��]!E���>r���D���WH�5Ido�֕ں[�^��$��lq���ں�f6�����Dl]���7�Y_B$�}T�l]�־��`� b몶vV8�,�Oc�F�붯k���c��C�h�2���~I�����u�~/�zU$�#VĮ��u�C��~L=R<�ȥ���n�Q�z�H"G2�v���oS���s>p������z��!G�������0�0u��������!G�1uWSw��W�C�jL=H9k\�J�5�U�����|��5��ߴ�"�mZ_��X��o�W�?�^-�/g=���z�Hǈ���Y�_���ԓ���T"�Q�H��y������oHǜǗ��ϓ���o�g��������5$�3F{Η�(U=��5�3���\AL-��ׁ��[ϔ b�Կ���G�8Ӵ@���"�&{i���@"����ω߀�!m�%�s���_��o���v�vp�,���븹|p��p�,v�o�H��k����°։���@�&(b��a�cߑ#Ñ��K��`�cߑ##�q!��VZ�"Ǿ#GF�	��A�K�ȱ��##��_���b{�Y��w���9a�&H�r [W�����w��T�ٺ���Q�D ��غ����.��m�c�c#G��I[ۆ49�7rd윰|0�m�b�cߑ#c���aj���"Ǿ#G��I�S�9�9R�n�Զ��E�����ʁ��8p�����>���ԽXfꮦv�X��F9�92v�~ ������;'G`j�c�c#G��I�L=���c���[8�Z�8�J��R�� ՂbD�92�e::�U�ȱ�ȑ�sB�u�j��,r�o��ȹ���	_m�6��92rNH>��v�ȱ�ȑ�s�~SۦV9��O�nY��=�,r�;rd�僩M7 ����|u`kӦF#Ǿ#GF��uc��)9v�9'�Q�v�	��5�[Az#5{�*�¶�'�$���ao��
�y&��u��n$c�1�V3�����FN?䶪��H���f���C�JPOέ� R�f$x�C��EO�	��A�%�D�PU�yrn=<(Ě�/E�Ɋ[���9a�&(�b���U1D�+CZӌ��fi|�X8D͈"YDUYx$��`p�*��Q�'5�#��[��N��E���3rNH>�{w�K[��ȹ���b�Z8�Xv�,�#�Dk��qp%��J^��- {'��3{�>���uck&�����[�bqXe=ieݱs�*H!�)C%���o�J�B��,��'��;vNh�R-@�䗴��ع|�=[3�xU ��[8�Z�8�[���`��T����y�rp��=5�o��{�;'4l��� ���4��!{6�.�)�q윰|�u)��!E��έ����p �;���0WA�k�@�)z��B���-@�)z�8����D�fl]������P$����l]�֎�[Id� "��jk��-($�}4�l]�֎����� "��jk_��*��>��kv�}_�ׅ�`Պ�a�u�����"��+@�Rkz_;z�]Y������}��9��:X�t �[��}���4r$c�Nmݷ�='4l���a��jk_��*��Q�Dl�����z�H#Gi�ٺ���}�uM�uW[;~n=}���[J�C�:��"��8�6.��o�W�C�,�B�R���U,LݛbT�P*ԗaL=�D.��T�;�RB$�cSOJ��M�:��`�Y���B?׮��q�`q:�
u�\����bT���@N���q�t ���T*��s+�C�8����>�0=��&e�4A���5�����(�a�|rf��1lk{����9��,��}��{�����,�@b��a���s�    ^��������s�B�P�b=�7zd�\��%��Ԅ�}���[6�jVjǾ�Gv@ZP!
�R<�<2 ���	�Z<�<2�N�>�z�؄��A'�� �Z<�<2�n��M�X�	�A'�n�z�ք�}���[�B�z�ք�}��0}����౿�#���Z<�<2�N�A��jMX��w��:!Jak�5a�c��#c���[[�6�C'Tlme�,x�o�H��v��FiB�Ǿ�Gv@�[�	������AY��w��(:y���d�c��#���R���E<�7xd�:�N�b��౿�#��DEM�HǂǾ�G��- Hz�4<�<�RK���Lh��w�Ȁ�냢��b�c�GFѭg�>����s����[�C�P�CyX��w��(�����ÂǾ�GF�Ɉ ���3a�c��#���k0��A�Ǿ�Gv@F��f��|al3���]�GF���	k��4x�<2�N~�`n3���]�GF�I�{���
	��'C'�%�mFďA�{R0���I5#
<k�?Q�:!S!�13
�ǈ���{���f�ď�:��3t��A�c�D>N2�y����	���C�F���z�n=:�L��S�$����<A�\ *�fD>#.t�c�3d&͌*�ً���N�{��ޔ�NJA;��P�4�4a�x�i3�ny �&�
M=��gF�-�ФY�	#�ǞD���� 4iVh���G�2�N�>�*M�<��V��]9d�J&�`Q�z�
R�f�&L:��K�bU���
M�0t�q����񙰵�0]��sA'LT=Vi�؍w����?���*M�x��Wc�\�
2�n�&l��ؓ�(�����B6�j\����#��Vh�f%�=+�	�S���2�9�����T���JM
�BʥZ5'�/� �U�z��B?'CFak�4)�
)z�8~n=}d��*M\�@�
�5�[[���� ��ׁ���Bv�5�����ک�2�n�&�ں��?�>2�n�&�ں��?W���[�I���jk��	�[[�I���jk���"��Vl�*7P�T���)�Zǚ'ƯqA�c�&�^�M�kԯ�r��>�.p_׎����5 *L��owE9��DJ(T�f�j�E9��D�T�U��8��r��/|�@�VsE9�xǚ(�B���v>A���jk�̇r����m�٧�q�_�B;�`P.t(����A;�`P.t(���r���r�C�PG�	�[������6��k��P���h^���y#}v>��\�>ߧ/��x�I�Щ\�#�d�.�jv<��\�T.�t��A�f�LʅN�BA�>R���R��r5�V�M�R�i�������- ��'���׉���9N3�@�\���P}0�Q H\�T���w��՛������t�B�P�z>�7|d]��(��ڄE�}G������ЫY�	���)�����j=�=���
*��MX��w�Ȁ�Ҟp֘��z5��G4
+7a�c��##��kh*Vn¢Ǿ�GF�	�e�������t2u��7a�c��#e�lm',z�o��^�O��՛����	[[�	���A'�lm�O����t¸��v,2���A'Llm���豿�##�/�6j=�=Ro�ǰ���������ڮ�c�c��#�w���~8=�=2�N.��n.c�c�GF�ɐY({�F-=�=2�nA�P���X��w��:����[�X��w�H)�� �1*=�=�B�A�c׷�豿�#�Y��Bծa�c�G��-ӡO��u,z�;zd�P}0�����Ǿ�G�Э#��vS ���C'TLmF�����Rx�M`k3[�F�}G���a��̀=v��p}��Q@�Ǯ�#c����͔=v�C'\�m�ď���0�~uD&��)�K8�N��I�
{�9�cE��$ޕ�2ifP�g����N�+Cf�̨���VO��y$�� �1�
"����6�е+Ch�̬���ŽI�3t+x�Ф�Y�︊���3t������ߨ�u��g�@j�̸���E]D8�+ClҬ؄1�7����l���ք���T�p��ԤY�	��^���9!�`l+5a��N���ԤY�	c��^=��9!�`�~��Q��ϭ���Y�	S��wc�ﮂ�Y�	���M����R�{�Zi!2�f�&L:��-��	�]O0�f��ƻ��s�*� �����E��[����	d�J�Qg�:�����4a�^�^�B	�� ��Vi�v��������R�Bo�r}�s��#��ViR�R.�9QA�حФ�����9��`j+4)�9�z��E�حФ�����9��`j�4qu��nS��U�?v�4q�p�����#}�VhR�����	�S[�I��>y@7��c�B�JM]�Ԏ�������e]��v�܂B�8�Ԥ�˺��ڱs����4qU�D�UsS;�>�4q��4�ī"}Fi"U�ܗ����4Y�ڔ	u��z���Q�H����T�����q��+�V-�V�1���02�Z�mk�έ���q�����mk_��*��a8�@9|�M���P0(:����q���2�C�P��	�[���2�C�P�έ���q���2�C�P��	�[��2�C�P���!wv@��{�@s3���r5����'p�pb�: 4�Sv>��L�T&��/!�i�LʄNeB='<�jv@��L�T&��s�j��P �
�V+z�7�k��P �
.V�*Vs����i&���FR���א7N3��sf/RJ_'�都M�(�:�5�GZ.��j?�7~d�\��)��ք���=W/�	e�4a�c��#��d�%�jVi¢�~����?�O([�	���)�.O�Vj¢��F��u銅��JMX������'D=Vi¢Ǿ�G��-�	�4a�c��O�R��6�b�&,z�;zd��2ڄJ:L�8窜�c�d)*lm�&,z�o�Hٻ�[[�	�����[[�	�����|����ʢǾ�G��	[�e�,z�;zd�𶰵݂ɢ��F������[0Y��w���91lm�`�����w�?���&L=�=�⧰�݄ɢǾ�G$c4ak�	�E���;'�O!걋0Y��w���9�VM�݄ɢǾ�G��	�I�]�ɢǾ�G���FHz�dL�P&��w�B�cWa�豿�#{]���݄ɢǾ�G�l�.�jWa�����s����v&���=��>z��݅ɢǾ�GFϭ#��T��F�}G����m�0�مI�Ǯ�#%��ǅ�̀=v��	a�`n3��F�]�G�$�=�m���k���9��ap3�@X3�WS>�Mq�ИT3�@��\����ehL�S��, EU��9."�f���Ձ��ޕ!3ifR�'�6R�:�,�I3�
"۬�(���֯�&͌*�l��d���2�&͌*�l���ϩZͱ�J�ffD�\���6����FHM��V�v�I������4+6a���>��n|'�&�l�沉��z���4+5a��M��/ !�`l+5a�����Z͍rɐ�4+5a����@7�%#�lVk�ġ7��8�E���Y�	ӆ
��ռ�d�J&�?��9M2�f�&Lz}�WA٬҄	Coz�~�D)d�R�L{����t!��Vi�it���n?2�n�&�v@gm7w���|�X�I��Y��=�>2�n�&����2���C٭���f7P������#��VhR�R��p֣G٭Τ�����9��`j�3)�)z�8zn=|$���L
�@�^ ��[��@�v�T��՘ډ��nu&��������c�2G�g�����2�JM]���_�c�2�JM]�Ԏ�kWE�حΤѻ�]��\�*��au&����R����W����L����Վ���+3qU�蛾�*���=���w�;���q�5����oS;���>��SS�K�j�߯��]�٩���ڑs����vf��>��nD�8�    l�NM��Ԏ�[�㨇��ܕt����"{v8���n�q���׳G�8�l�A�СD�����	%B����[O�㰳	%B�����WC�8�p�A�СD�#��gB�8�p�I��y�R�5�7���&pE�l"��_���q���2{��ׁx5d��'���J�:�~5$��N'���J�:r�]I�4�	����j*Ts����i�H��c��Yc���5$�ӌ'$.T�*Ts����i���ǋ�T��5$��$�r�+���H�U��豿�##���F�lu&,x�o��ȹv�M([�	���9'�ҠT�*;�;2rNX>��ʄŎ}ǎ����֐�X�	��;R�n]�P�X�	��;�ׅ䃠ǊLX��w�Ȁ��+2a�c߱##�$j��ǊLX��w���9��0�U��ر��#��և@�P�*;�7vdܜ�0���ر�ؑ�wWD�P�";���q@H>�ڈLh��w�ȀD2S�	��;2jN�D��.�d�ccGF�	�S�%�,v�;vdԜX��k0Y�د~p�>z��݃�bǾcG�ݭ`�y��Q�])gw@�Ȱ�݃�b��ƎG(>�y�L;�;2jNf�B�c�`�ر�ؑQs��A�c�`�ر�ؑQs��U��Ŏ}ǎ����$���=�,v�o�H���[�5�,v�;vdD� [�=�,v�;vd@⪰�]��bǾcGFͭ����ja�ر�ؑQs�xP�T��Ǝ}ǎ����6�0i��5vd����!-�f4��Ǝ��[H0�N@cǮ�#;!��`o3��Ǝ]cG�$�?naƅje�.�j_�ujQujn�K����	��ujQujn�K��CH�Ш\�#8�!1iv1`�\hT.ԳwW�Ȥ�)�1K`���!���1S
"۬�Pj�i� T&͌)�l�փ�B5��g�L��S�n��R3����FM��T�r�I����ϐ�4+5a�������N(M�U�0�Yp�ɻ+ChҬЄQ�79W��c[�	c�oN-~���I�JF<�@�tS\22�f�&LzSO��[��H!�8�.V[O���ȹ�d�B�
�9����d�B&
}6�*�t!��Vh�4�7P�:�nb���*M2�F�z*_8B�A�c�&�6@g�6w�\�
R�n�&��?g�6w��|�X�I���Y��;��!��Vh�i�s�ns��-�!��Vi�
7P��V�Q��!��8�8���g��[�I�7H��	�S[�I�7H�ıs��#��VgR�R�q��z��!��Tj�jL��9d�:�JM]/��o�W�Bv+3qu�D�����)d�2Ǚ(ů�H!���Tjꪦv@��H!�<8*vY�}Y;vNX>Hz�̤�˺��ڱs��� �U�4zY7��;'4=Ve��e���v�\�*2�a�`�����@�߯� �]��X��_��ua�`j��SS�mj��=�a�`vjꮦv��|0�LЩ���ڱs��#�v2A���jj��-($�Î&��'9',Lm'��pT��F�Wd��&p���y��a�`j;�`P"��wZ�㰃	%B����kWC�8�ajF�%B9',lm'LJ��Kuj���!qv0��D�T"ԑs��A7�Duj����!m�v2��� ��Pw ^Y㴣	&%B��HX>���h�zp�����ؾ��q��R��*��*57��!i�f6� q��knOέO��q��R����ޞ�[��q���W=^���@7��!m���J�8����|&�����l���z����_�T��Rŏ�
�_��Xo�f�i����~]����_��j�v���`A7,�U%�1�P5MCU�ذ�+� ��訚��*�.q�p�}���0�騊|�K�!.�������iZ��v�i�߫�?MU9���ȧ�D��Oī?mU�[$j�wH��O_U��j�%��4�Q���n�O[U�*�/��{ ����/Sڪr�mU��n��~[�z��w_�}Η~�=�O�V�z��wc�d�A d���z����5�C�eH��z��.���¼�*$Z��ֵ@��v5=��~��[�@��0��9�$���l���l7����K}����:��D/6}����OغO�����I�6 � [�z ��,�a�5l=���=R�|�l%�ֳ[ �̽�u�U%+�|�r��lE�|W�>��6���*�0-�C��!�]�\���1ȗ�ְ�ָ�b<>�/!�]�a@�ָբ��X�f�j�s��;p��d>��W����F6���z����d���Q�yJ�8>ٚ[��,S?���@䖭��2u�7�l��dkn��$�7�����a��b�I�B˯l��'gb���՟�c�1`�-����En�z���� "wQӻ�Ϛ�Ƅ�G�@�.jz��������]䀤�����L�>_��$�7횈�R��ٺ��]a�^�Z
� "��j��3��H-�K�����O�6_�Z����-V�l��5q�%{�z�}����ǝ�l����s���z��Ԓ�ԘX}n��$�7��#RK6Rcb���ꓔ�dj
lmC5&V�[������Ւ՘X}�b�I�����H-�H�����O6k���Ԓ�ԘX}n�:;��>"�d#5&V�[�΀��G��l����s��'齑5وՒ�ըZ}n��$�!�E��L�F��S���{��Z�kɄkT�>U�>�N�{C�c�l6*X�*X�l�L�)��1�V��ҟ��-Gcu�50�d�!I����s����80I�x�����Eb���?'~����l�X,�U듔v�*����r��I�V\��������֭O6xZ�`�$f�t}G�0z��=���x����t��؋�Y%p�x�ix[�Hx�E¬�rM���t���E´Vx5���t��b���Q05�-]$<t�0CZ��i�[�Hx�E¬(%E�ې�t��؋�Y!p�. o�!o�"ᡋ�Y!p�u`o�ao�"�q��_��bG��8G���qR*����.��޴ԝ���NHIq��W�ד���.�8�����fp'_$<�"aVlW�� �ˢ��+JE���䋄�^$�
�돫0��p'_$<�"aV��"l]���"��.��«7غ��礁�;�~��k;��J���IE�n���9i��
���غ[�E��]$��RP��{9p�_�w���ן6a�,�sd�s�:�ʽL=�D~&�ʥ2� S�dϱ:G�:�;�a�y���9��9�'�w��l�|7��:`��C����aWH�+���5
7�x| 2�Q|����b:>���(���5.�h/�B��9���bG��|"����.���5*>Pn�@�ű:���yJ�8��en]-^��[@�����uq�፵@�Ὼ8�L���15���W�ʀ˭L�ځC����"W�r"Lݓ�aWQӫȕ׳�0u���O��1a�Q�9v5��|������L7_]{}�t����!����G��Żdq�����U�5��0 b�L��{6�%{�1]�ܺ8V,�ĕ��ajv�w��]0_WZ�W���WG���č�l��tqs��؁���%�1]�ܺ8$�D�چiL7�.�U��G��l��tqs��Xp=}�i��i��s��
(�D�چiL7�.�U��G��l��tqs��h��Ӓ�Ә.nn];��>�d5���[ǀ�v�H-�H�
��Ʊ*�]O��M�F�qS�q�
(U$k�kT7UǪ�wAq�tW,m���
(U$Dl�DlT7UG�R���2Vg긹�q��]O��%�Dk�Qk�J\��[P�b�q�ګ+�EX=Ek�Qk�.�_��z	ː��^]aG|����X����ݛ�ˀwAV��"Q�kw
)J�V��bV���.)�c���;:.rϋ�\��QDӌ"��
�^W���0�h��6ǻ�ގp_�W�0�i��u�C��J��0�h�iDt]��u��D��M3���+{]!Z��M3���+{]!���㈦GD�]W����H4�@"��p�BV��"���D"��p�BV�Wl��sO]WH�WC��w]!{}�&��+�{]!Z�L0]W8��~    ���%���+�{]!����%���,p�8�ky�q�^��^����+dU���A�C��u�s�+�e«�w	�w��
�^W�H9�n���9i�����u3�f�
绮�U�ß0u�m���;'�����*G�*����;�ԣ[ V�x��*��a�yTY�#k�×	�a�iL����w�{}��%4��z��N���ӯ�����a7H���52����X�\4EqE�e��������ES_%��h�]��r��KV��ר/��2��z��{K�]��va�˹��v�,�n�h�]��rn�%-�]�.�v�0��܊Kv`y#�]�mwa���*.��Ft�D�����U\�ڝ� aj����s+.Y�n={4�D����s+.Y��\�.��%3��܊KV����}�Lq9_�%��]}.q�0f�~��ץ��"ە��s+.P�&b�dc+���[q�jw�������rn�%�ݭ�2n�do"����⒕�ֳ�E�lp���U\�ҝ� aj[1��܊KZۻ&b�dc+�������z��������rn�%Z�U���Y�74�s�;�[��j�ho^�.�vpN�V��VLq9�⒕���Gl�ll��s+.Y�n=}W�WLq9�������*��).�V\���#�J&���˹%��.���h���a�ۗ��+���.��.Y�N*?��	���r�꒕��8+�8��.��.Y�NJ?��r4V�В�K{�\_C�r)[$Z0U9�?!�P�+g�CO��2%� �D6����(SI�/݉/@6��l"~h�TT�Kw�Nd#����$�n_������QN��S/Ҷ�+݉+l����F'@�P��֮������c�r�z����f��NΡ���޶>�L3_�6rm�d�6�ܡ*i�>Ρ}��"w5L���r��NW�:�RwL�f� �����I��6h��&NVo[�;fL3c���~��ۤr�f�p4R{'���\���G�G�v����m��`;�a;����������f �u��Cs�b����A�ͽ~�I��6<]?4��!Vo[�,G(G�v��Yn[G0]?4��C��&e;��t����X�m={���t�м�wr��S�bϱ`7i����?`�~4���D���z������=n�uW��0�H�&�&\�me�����D�+�I���� ��I�	]����t	GG�@���r��H�t	���e��u)ۡ�(&��.���ZyFS�	�~�y�����@MH�;Ͻߙ���_�0���X�hG�; �{�D����H��s��)�ܘ�2���v_��]���X�2�m��6]�n�Yn�GE�E�UK��p�l�,7֣$�l]�֮඾Hsc;
e��n[��
n���,7��N���z7w`=|d��u2FF7�%u@��#ˍ�(I������r'��a�q�$�ߴ��U�����Q)c���֮rW��7ΣP�l��֮�~�ަpʘ����U�V|��6ţ&�l�����J,�ڦx%�������zB:Je�'v\�P��K-��R�~b�e^w���iS>
e�'v�O�+ݭ���6��P�~b��ĺ��z��gS9���'v�O������t6գR�~b畾^_�s R=
e�'v^�g=z�H�����ة?��r��=� �~��X�>5Fw����1 ��&�b��1��ܭ��9 iU2�ck70ީٍt�hB.����(LH�є(l{�RC�0��M�¶���[� r8*�4-
��r'?V��Q$�=�ҦB�ŀ� ��v��kdZ"s�Q	��Q"�}���m�	�
��Q"���	�S�4�0I{?��ßW W��Q#�F��GO Y�YJ�s+Y�Nli6l)�8N�8���x�ەf�!��˱�/Yݮ_���هL�/�6_�T єf"��ˡ���pW�����lD���C[/Y�n}*�D�f%2���y�
wy!��f)2m���V������lE�}�C�.�	)��f-2m��vɐ�ձy���t7����r׮��0���t7����rWZxlH�1e�J�o��~����N��0؀�c`��wuD��F��4 s��GHlH�1]@0�?B�`CB�:���r�>"�`#B�:���r'@����t���r��4D��F��t���r�>B�`CB� :�nZ�[@0�	�n���&`��g��0؈��&�{7� lm'C��s�&`��v��N�b�	��M�Jw�����v̽�����BoGC����M�Jw�c8�R�)�y�	��x� �\��w7��`:j��U.J今���@"D+���ep~�Q_�Cʼ��7�B�t�A�h�\�zcUot��e"(��Ur�]� J��̍`��+Z%������-7�rE+��pW�����PrE��j����>�)S[!W��gM�\�l=|��r5�{�����ɤ�S[�}��gM�\�LZ�`j��w�o�nj7�?��Vp�F�Ǽ��ş��phA�����X�	��d��NM���H�[��٫�SSw5�+�I�s�*4t��е�N�D�ҙ���R)�S�L��ɲ���CCW&[�lf�l�����a*uZ�	:3Y:�W� ԾH�Lm��I3�y}6��g:3Y:s�|v^�u����,�9i6;�_ue����f&�fNZ@8;���=Й�ҙn�"��
A��	>3>��"�m�;�LcB�T��Կܭr0�a4�r�U�1AIE�f6��@q�=�箁�̆֔2��+�ݽr$���T�v���Iu�f���3��*P'ى!C��E�m�:&���I�V7�@�B��؉����a t#�ԍ ���<�`�J V(_�D�l&�Н s�`���UV7#A�R��KX�L\��fV�^Ǳ{ف�%�M8�nB��8�Ց!I��df9!�t���
e�j�N8�ѽ��ࡍ��P&��Nf��>ǡ}��P&%7������q�6GV([976N���v9�r�������d�ËԿN��7gG�5xR�;$�����P�x�^2'\���`;B>���0�/��K3�K3|��5�s�2�����ܙ�C��I���-���ѼG%����R��q_;<�C��I�; %7��NO�S8�N���>��`�'��{���I������N��+e��cvRhG��,�f�R&�`j;<��n�@ھ��:�';=�-׼��-e~��';<��}xpb�: %7��Oʴ�'k�Z�7�'�yt�&��M<�R&�`k;=)�&��M<�R&%7$��((�+��:�k,�����B���W�+�I�ŉx ��K����[y��(ڻ���h��Je�D�,w|"'ռ���O�����վ^��@��Q�b1RU}��=(������ҪeIWs�.7xQ=��,*�Z�t5�ekd���Df몶v57�r���Q�bwH����B��5����w��^�2�؏�C��!�t����&�;����5���q����qE7�r���Q�b��ָ�r��+����l��֮�&{PG��ٺ��]�L��P�G9�ٺ��}��5a�x�������.7��Q�b��0�������R>�\�z_����a���Q�b!�Аϕݤ��.G=��|CC>Wv[OV*GA��|CC>W.�67غe.���T���G���Q�b!߼>_�50u;�\,��9 is���QOd!�Ԑϕ���G���QPd!�Ԑϕݤ��G��R���ڭ��!V�G�����2~Y�c�y�(�tƽ�_(�Z�Q�����d_�r8ʊ���X�Aݍn��ģ�E[㥺Y��ǀI�9.��ER��v�`@}+�-��2)_���`�|ԷhK�
�|�L\,g�GU���T���	�^�5�n���@tfCtҹ[s��bH�
�0�t����[��v7���厖9���9�wiA��"���USv������f��0/[�vP�U3��q�z�v�x(�u��(�jr�j�A�١����*Z/���J�o6/��5���N����%�ïh�Z�L�7(����B�1L����ng@h;���ˬ-=�v|�����2�    ���g�x%���i�J�jz(q���N��I��J��F��N9���;!΀X!���)ɓ���o&P����!:JO~��O q40�I@�_�(}�#@1���@�_�(�n ?�� ����������74����_@����1F ����@��u�E0�(��"8��e����	��m���߯�4����yV~Si����@�z ���J�t����I���=�v9������@8�÷]��AJ_H�iclT�s;��A�_n�w�û]�� �Ϸ$��8���R�B�� �=��_��P���h1�~Q������A�_�N(`�`,N��ЖK��ѡ�Qb:��W%��
�#
%Ğ\�'GF�ƈ
FI�"� ��ۂ"�u{5��ÁDn&�����#�%{U}
P��u�a-��QJ:�ȍ�����P�(eX ��Y��!���Qj�H̿����@��p ���ߎ�@<^��7�֧ i��w#����D��l�v�n���sI��7��o?�R�`o{��������q��$��E��!��[�2�4�&�hR�* �Y	���D�]��][��(c�0,�wU{;~���5���]�ގ���b�$b���{��5{�}4)`}]\ 5���D�rm�LH< ���b��M���Lq0j)��?�M��	�Z�=�r�)U=��b%0j5oI�Ҥ��4 rC�����ػo{����}m�1{w��; .���d��y�>�J| �N�8YLs#m{�1�����8��r�)GEr�22"�:���j�����T<�c��a�X�;4�u��l����[(�㰜��9�i��`�\��FJ�����n�Y$���H9|!���m�%c�I��g?��i�3��[�e*S3�m,> ^��r �/�|J�8���x+�"��Ս��p��j��v �o�|J(8����x�jp��&�wG= N r��a�hn��=�x�����R��m�j��^�z�P���@9isF᪙*
�Q(s�e�u�fR{��N�UI�ۖR��7�i2���&�d �@��&�d:i@���#��&�dB�*�/(���d��d�)�5����n�M&�N�J?H�V�ov�o2�􃴭����#��&�dbi@5=��Ă��������mu_�_PH9�I9Y�P����ξ��ȋ��G
$�����3�zX|�a�'��s��sF^���:I�K� 9g�9g�ՙ��3�� 7@��w�yu&��L$�G��t�n�ͪ3��D6q!��}@��[�a�� ��X �M�]�aH"AC��g�H>���:Iߩ��s��$����:�p����sco��N�i��RŊ�s�a���_�4�\�X�t�X$��A;�.U�H:G
�_)�O��5B�9R;��7A=M��	�u�lN��)B> �Wd�#��\(�2��T�"��X v��P�BbyI��"����#� ��H�B)z���� ���؛lI�F����:G�w�>�H< I���1{W���}� �#X$f��vE�s�v {W���B-�{Oco�r$E�y��h�ؐt�9 �k���z�=C�@�oz��4"�1X$v�7����C�9c;����w�o���s&co�w$E����Րs�4 b���:Q��r�\,�wW{w/�)�,�"1{w����,$�����ػ������[�&���|�� ���7 ��ܳ}��$·��o�A_��� ����)㠣i4��yO�	w�i#��6����es��q#�7����N�H�I1b�H	��GTF7���^���.@$�N��	}��s%NĈ��D�#ϕT-#��QdF|��;Wr<�H�")Ef�ǈ�s%{��2��e ��� ��%[ ��f� �̠�u0x�q���`�\��"1���I� ;�J�q��?q�ϕf�B�����07���| /�y��n�����Z�Z>\'AB�K�,�;�|�N����2�D�����`UW���,�ohɀn
q����w���6�����i������1�aYW��"1{W��c��?¶���D�]�ގ�������Yh��20fP�`� �k�iu��`�y��1��)���0˼�j������0�f^k<��הiu̟� ���j��B�����07�{�| {�mo������i������S����޶;���a���� ���c�eء%C� -s�vp��2��e ��M!��3@�+76��?!0Ҽ�i��:�iuğp�`�B=��wr�N:�1���h2�u(��A���{3�u(���� _g>)�:Ӛ]+A�˖�bL�T���c�r=��wn*��?q̱l%Z$ƴNeZ�'N�I��t�Ę֩L�#�n
�jp���a��p5a��N�D[9�6�?!^ QEk��b�J�6�P���o=Z(f�w�%���f�L�3�p�@�V����!���M}yج�\��X�d�l�غ�P�TX�j6�j��/��z��
r�nrM6�l]�:!���n�M6�P�ovs�t��:��Yju߷^�ov�o��/`뢾�K�nN�ET�.O,�8��8�(l�!�\Ͱ ��&�d�@}S��邏�q顴����	�9��9#�_���!�# ��;猼<wy�1��9��9#/��]�a̟�rξs���3q�g�ws�0x7g���g�'N����y �B}����*r�>�b����zw@��}F��
�I�I��}��T���ws����7�sSZU_w�bE�9´@�V��(��I I��@"f�P�O�#$�#E������Ԡ�u	H�@"ބi0���!�������9��$>��s�i�؍R�Fqԟ� ��Q�Dn��7����	�u�-�Q��(��'@�9j?�ȍR�Fq��M"�����a�I����d�"�mZ f�w��v�^$b�wH�H;ǈ�����'���1��D^�����YD|��a�7��\�[.��1�D�t�2��d�!휡Z v�7��}G��h����
w�i��@"�\�+�!�,� eg�M&2߬VV �wސv�4 b�~��]/AC�9s�@��]���?qd��D�����ގ�@�9K?������Wp	����d,�ʹ�����t�: ��ku@�H9g��q�C�V����zr+��H��Q�q��9)[A�G����YD(��"�#��Fa��Iԁ�b%1�yr�N��S��@���;������hN*V�['�s�#	���b1�sR'=�wӈ0�����I���8����İ�2��e ���"��Vâ˰��@�?y�I�JbXtvt;(�n%1,�;�dn��]�]2$q�'+�a�ex��@ȿ�E���&�E�aG��;y�5ړ��İ�2���9��	ړ��İ�2���9��	ړ��İ�2���9��	���Ű�2��%{��!B
�Ȏ�����H�O�Vâ˰��@�?!�ПT�&�E�aG���7���İ�2��2�O� JՊbXt��2���ap��a�ex����A��`o+�a�e��%; >���j%1,�;�dH7�{I�.Î.���РT�$�F��.��n�6�]�7���@R5r]�]B��@S�L]�]�
�m�\��2��� b�:�ѭcӭ��ao;u�E��.���:��١,�;���� :�f�.���ߦ;?� C������AR��&Ȑ�4;v�l�x��K
�Gv�Y�q���� �iv����|���$. L�C���p-%�f�.���W���f(`�����S$W'̐�43v���x�T`\�0C��������\�0C�����P�'n"67�����J2���3��]�Aٿ�H������㡴T`�Qa��5��Pv*��(
��f�M��P*���
��n�M���]�.K,H7�I7���*��7���I8�����j_HB!��&�d�?@٩��7d��d�    l| �T`�
R�nRN6��]��jX�sv�s��jTV]��/(�:���0�;b�g���ѭ$��qϜb��tv+�a��4��ar+�a��4��?@�٭&�h�.�0���ap+�a��h�'N���[M+��]�a���H:��İM��
n51�@w��� Ig��V���@Ðn"*�`�ɽ"����K+��aE1�\+7i�"��RŊ�sXQ�[��"���m�\H!YQL&��M�����W$�êb\�HYe�nA�B���,Ƒr�������:�U�z��R�� ��aU1�^)E�G��D"nU1�^)E�G�� �V�> �p�bE�9�.�R��KevnMA�H;G����}GH;�����b�:p�0�H�����PH;��5&s��
y��������neRl�;g���M�p��IQy��@�:��:;?��!�1Y$v�7��}	GX;��8���1��7��W2���}�� �9X f�w������@�B��B;�QАxN;y�� m����紣:5xW�;�ap;{�UN�C���K��iG/8Z@��� �v� ��P��!�T"�Z]�G&�Î =J��b>)mv�M%B�d�1y�6;P~ѤT�2�7Q�JO 
���b�1��R[(I *H"Yao��J@��(+����@�n*���@���� '@�R��_�7�� @�R��_�_B ��G�Xe�/Î/! �	УT�2�ŗaǗ�1���[i�/Î/�q4)��a�ex�K$>��b�1,�;�|��5E�R��_�_>\OAB�R��_�_>\OAB�R��_�_>\OAB�R��_�7�� �Z$��a�e��%; �24)U��a�e��%C	MJ�jcX|v|xS�#Ym�/Î/! �	ХT�8�ŗ�/! �ХT�6�ŗaǗ�0���R�V��˰���(B��`p��a�e��%;ps�0�����2���!��M�m�/�_� ��M�e�/Î/� �ЦT�0�ƗaǗ�P����T��ŗaǗ�P�7���	�,�;��#��X|����.D1��_`�ex�K$�$1͎_`�e��e ��P�4;~aP�u(��(��L���_�rJ�:
P�$b�1���r;�k��d�a���0)�:/��a5b�f�/8^@�ru���iv�¤�����t������0)�:�ru�� �0��_^���T���dha��� H\k�R;?�&C����hH��=(^ 1L3�J�R��R;?�&C����m��uBz�f�0�����B� ��f�0�ک��Ϭ)�4�lƏ�QyW� 
s�l��l��b�P愫��ݤ�d���J��"��|��|3�9��F�y
�nNOʽH��0HH8�I8���B������qv�q��_�2'�<K��ݤ�l���'�@K�=F�dwR���'�
�:�U�0�;��y(����[a��^CB��`s+�a��4��M%��V�
4qh����n�1�@w�����:�UưM|4� '@�٭0�h�.�0P$��
cX�&�# o*��V���@CB��`p��q�\G���H:�U�d:~,_�HH$��
c2?�u�����DB�9�0&�K�F2�HK�#��r�*n��*rΑ{�+�����~�AE�9�4�UM�Cl���O,��9�U�8R@1}@�9�2�����n&�ʘBo��7����	�s+�!�o�}�8�O� I����&�Tj��T䜣G�^����@�9z?���.��ί?��9�������a�s�1-3xU�;$ɶ�t�iN���Ė�~�AC�9C�@�
oz�;�OH$$�3��j��j������s�l����
w���HH:g�7&s���7d�3����7�U�^@�9s�@��]���u��$*��*��-Y��~���ˊ��Z"�vY� �.Ah�:���@6��l[���dS$��` +��.(��l��s�dG��Ծ�T"��Yi�G"u�Z$t)e+��ȣ�g��7�1R(�F�:���R6G�R���G��(=(�������6J����M%B�d�1��R�(I *��HVÛ(�����D�jcX|�����K�Xm�/�_����I�Xi�/Î/��n&���_�+�\K4)+�a�e��e ���$����7s��	B��`p+�a�ex�K��� ����ư�2���!�L"n�1,�;�|����&�b�1,�;�|����&�b�1,�;�|�\KAB�R��_�7���� MJՊcX|v|A(H#Yq�/Î/�)D�I�Zq�/Î/��$B�d�1,�;���'@�R��_�7����I�Zu�/Î/��n&��_�_����I�ڥ�,�;�� $�.%e�e��%; N�.�j����2��%@�R�;IY|v|�w3�0�]J��˰��@�?q(c��J��˰��@���I���VR_�_��ɯ�1�a`�ex����A��`p;��ŗaǗ���$B�dg0��2���!	�QL�3%\��� ��D(���A	ס��# �F�&�M��rTq����X���ivä�������ivä��T��3�B���v�c倴	Ww�ao;�aR�u*��� �if��r\og��ba������v�+f�a��� �W�E��s��=L3C�I�j��\��Z�!�if�@q�cR��[�!�if�g��x��B@��9��v��at�lƏm�QYWG 
s�l��l3��b��J��f7�@q�cR�]ribA�٭ؓ����=�4� ��&�|�|܋�	C(H�!���sr/�6�;!g7g�森��P7����3�U�Qw�{P\Ig��Fu�����H:�U�0�[`Tk\ٰ ��VÈ����k���[a+��+�hS��
cXy&��; ^�����ތ�ʎ8$q$��*cXy&��F��<"n�1�<wy��� ��V��3q�g�w�0�ư�L��F�	���sXe���$[���+r�a�1����z��!D���L'�e�@搄CB�9�.&�mO7
��n"B$���t�S�mO��@�9�0���\�쟸 R�au1�^(�R���XP�q+�q%�������q+�q�܋�y��a�zdJ��!� �VS��붷#���q������R��_�P�p���R{W��#���p���R{��A]�P�p���UL���i�+�1{����{��sؕ��^�Ͱ�~BC�9�F�F/��#��AB�9�J�F/��#�n�/��������?a��p�,c[�����
	�+I]�$=��
�|?AC�9�FR��(����8�]Iک���!�4"n�/tj�w�82�i0tj�w�82�i0ʶ��(��hS$���_�mʶ:�O| ���\��E�%�~�aC���,�#���? �:����|�P�-쐤��bu1��r�Pz�O*�hQ*V�[(w����AB�R���A�����YDȐ�*�7P��m��bU1�R�'I�)��me1,�op�^@�R��\�7�d@�hP*VÂ˰��@���E���(��a��p�hP*VÂ˰��@�?�4(��a�ex��@�?q4(+�a�ex���u?��I�jbXpvp��$"�m51,�W���'��VÂ˰���k&H�O*VÂ�����%H�O�VÂ����0��=�ZM.�.a�n$��a�e��?�Wړ�1�����T�}������d�vUn�!��?�ZU.�\����=�ZQ.�.a�n��\�\����=�ڥ�,�;���'@{R�KIYpvp5(H0�]Jʂ����u��'U����a���ap����a��0�X!��v+).�.a��	AL�[IYpvp�'w)1�N_`�ex��@�?� ��v�.�.�	$;|�    �a�쀔5!�iv� T�P��!	1L���Z�R����b�f�/��;�F��j��j2�0�_��j�J�:���ap;|aR�u*��?qa��0)�:�j�ܠv0������i|'������q�]�B'����¡q�]T��W���if��@q�]T���W�!�if���q�F�H�3�0�L`(.nL�n�k2�0͌`���Ε�0͌`�|܋�y�fat�k
�ev�~A	m�d��dS�8�m܄����8@��M���JՍɥ��f��C�i>&c���Ă����3�U�q�����"B�d��w��/���|cAA��M���*ů�
H9�I9#�6uݼ�W@�٭,���7��@~�%��nU1�� ��W5,�9��0��f��C-�sv+�aՙ��3��7@�٭(�Ug��PrP�`o��aՙ�Vg���#��Vê3��0��C���&�Ug��0�O���jbXu&^�8��h�9��İ�L��F�	}��sXQL����edv.U�H:��ĸ�	���A��Jb�X����s�i�ز��˞�H�9���d��)�'G�� �VS�}R���T��Jb
�Oʥ";?��"�VS�}R�>qԟ<���*b\����Td��T���*b!�"���H9���Tj�z�/ q���*b*�w�Tc��Td��n$���U���?qd�î$���U���?��q���R{W�����;@�9�NRW3I`�6�_Аq����q �_��"_v#i��w���!	}��sڕ�����\P?�	�+I����ߎ��	�L�ޝڻo{;�O\ 	�I;�w�Ta�[	��i��������Ώ�D�9�FR�ƽH���oN;w�S{w��C@�9�aoFmw���'>�|s���2��R��eؐmN;waP�u(��?�$���]�iʴ:�O� ��-\�>��c��H�pM��5���\��L Hm�߼� e%H�(���k��I�܏ν��m���M}tI4	OW	��k�G7�ѹ-0"'yz�R�H�$�J��P2	�i�*�6qMJ�O%�]�2M���\*/,���}(�|�LY����r��B���..9�2��m�O�T���qI��ta�C�#T�tr�j�ԅmu�F<��ꦕK�%��s��3���nZ��Ǿ���|��B��M/W�XXww�/�/X�ts��+�z���o�(X��sŏ����O<au���Ț��ս>�OX�4tE>�&���F�� ����ȇ�D�Z��@�>0�ꦥ+�5Q���.p�v>M]%�����
?P�+d���kA�VOl�J�,�)#?�;��x��J�M]�A_F����Ȭ��������t�ލԾ���z��J�M]�A_D�K*I�\��(����z�ܝw#=��X�����U3EƐ7��H�
��r 9' �w�{�`�|�!]o)�9�J���޵Y$_x�%���@��[:����F�_H��n��lunzW�&�F\`�޽X ���:7������G8���q�$Ҩ##��Y$���:7�R�����@"����}�:֌0�4g�sӻ:��.YF|~��(�Q��(I��G=TR�Q��(nH���Q��Eb7J�ŉ�%{�C%�t ����:u�4�x��d�V�wun"� ���b������1*���%H��U��4�4��^��I����5H��U�t���0x5g�sӻ:7���@��[9�ȗ�]�u�,�{�`����
w� �	{�f����
od�뜰�H��^�E����7[���չH�!�޳@���2��ֱR@֙C�@��]��$�1ڍ�,�wW{�F���c�ӁD���ޮ$�C֙��7[���չ�뿙�z�S9��W�s���G#��9X _;N{u.C�?g��"��qګs�->��3�t �;�]���B_| �f.��lunzW�&Ҧ#'�n�Z �X����= �fn��Zfګs+��k��$����f�����W�2$���i����s�^��Ȉny��4�(1��:7�����f��"1��չ���3�f��"&�����y	�d6߫s��,($�%F�UȤ�s�	�;A�YL�IW禽:�!�}�T��T���M�:7�!��S�\��\���M�:7�����b�M�:7���Du��m�m�չIW�>P��
V7�&]������
8		g1	']��tu.;!���������M�:�A�/ �,&夫s���MdL��r�brN�:7���DT���:��:3_���չ���' �,;��|unޫs�_�TJH:�N:3_������
8���3�չy��e�v����3�չy��eH��Yg���dS�]��ɌnY*����`�|6����LD��� 鬱H���LZu$!E�YS�H>��{u�s�W$��ΚƁ�xy��ͬ�#H0x6g���ڌ�.>����`�����j3�$>����v �?.�R�d��8VL����͸�M�J�����ڔ�*mUp|��,�7��],܌o���@�LЦ������7)]($XS����͸�Mv@n���������x�?�JH���k!u{��S�WG��� �����f��&��Nz��<�^�{:Pؤ��k�u�"�A*a��Ӂ�&7�]4܌nR�P8<���v�p3j��N��"�^��Y*a��J<��ꦽ���q���O谺i��f�p�с�
V7�]4܌n2:P|a��Ë��Q�MF��"�nz�h�5ܤ���x��i��f��&; {��M�7���JȄ ��./nF7(tB��M�7�������/[(j��Vw?�B*=�^�d�,�Lo���@�Q|:�Jܝ^���i���/�/H,�V��GxZ�R�H�-�ʖ8��X��w�W��f��&��*���Y���p�с�/���}����^�D���{��t�v�Ċ�I���0$����yI���8����187�n2 �	��j�X�w��@�	��x �?.����1>�Eb9l�)��of��"�6����G|��M��ݤK�Ba���j�ؕR�Jq�Ə���$r��R���Q-�n�ؕR�Jq|��,�x�@"WJ�+��b�GP�����&��n�e|��@��s�@��U��@�{*^�D^���0/�"1�W5�;p3�0x�1xU�;$����l�nz7�2>P|`��Z v�7��!(���{<��-��w��8���{�H�oz�;B�a�$r�5��=c�^0�0g�tӻI��.a�Y-3xW�;$qX$�9�����#�4�H�"1�w5�c�/� ��$b�w��A♣18ۤ��M����w�T ��y�7U\@�w��1�CXw@| Ig��"1v(�nn�.�@"��P�1��H9s9H1��N��:�T
�8s�q��X��	䛹E�ة�c��n��-c`�UE�M���D�sSXw@)�����t�nڛt��H6�#$�kq�
��\3�h����&]�
��\3�n����&]�
��\�����ui��,���.�<a��^����Nȍrn'<tMD	���R,�I���J�5�zM���IWR�N� �I��-h�r��$4�Fͦt��餫By%��4�6��n���+�&��Q�)�t{:�*B�K��Q�)�t{8�*>�+�Q�)�t{8�*>��Q�)�t{:�J��I<$�:N��?4&SO,jI��S:��t�U���sڹ%]r�=���z)���:�v�������tZ4�C;�p�U|��Jh:-�Ρ�tG8�*>p1�Hy4�C;�pҕ���x�x4���C�q�:���yP�+��%��@~SC�9[�@���I��/�h:g��H���I�
M�_�,��H���I�
IB�h:g��+���:�):�k M�s$~88�I��!��8P���p:S��sN��H�\��G-b��?Vw�wMEzˆ���?Vw�wME.bZ<^����k*:�ql?~x���k�����@g���������)�B�yo?Vw�wMu�qe�/��X�5[�5U�E,B�Dۏ�]��    ]SсN'az���&W������*���~,�-\�"RN���]��]Sс^^:h��k��kJ��C!�Y�5[�5�//�Y�5[�5U$/�.�Y�5[�5��E$�v ˻f˻�b�0�t�dy�ly�Tl�"�􋒮�-�J���q[$-�K^5�1�t�'G�I��rb�H�ӷPZ���s.�K�s�A��\lS���+B�tlI��wO��*vJ	L�e3?�,�*)d�3J`j���1f�TI!���f~LY&UR�B�� Sb�̏!ˤJʁE+"�,��1c�TI��e�	�Ʋu���]S��^`j��Qw�wME.Z	gՌ�k��k*2Ћ ��ƪu��q�Td��p�ͨ�f,t.�fٌ�k�����{	@5�X5��=�*�b�oVͨ�f���@a��f�f��nW]�.V�~�|��uD�Z�@����鬛Q���u�U\�����t��(W����,�H�)�lF���p�U��*d3�e3�U������XE�X7�\u{��*.�S�Lg�r���������鬛Q��=\u�XE$�u3�U��������鬛Q��=\u%Y葐p�(W�����3��3�U����*�� �3��3�U���������pF���p�U\��3��3�U���������pF���p�U\���t�HW����J���d3�d3�U����:�XE(�H5#]u{��H_p�6�J��	/\�z��T���U�����=��:�gP���u�U\�?@�9x;�r��᪫���*"ἝA���pՕl������3(W�������p^Ϡ\u{��H^�8�gP���u�Ud���h8�gP���u�Ud�b�o�Π\u{��*2Џ����A���p�Ud��o^Ϡ\u{��J�����A���nW]u�)4���3HW���*Ң�qZ� ]u{��*2�����3HWݞ���\�"�I�m����Mz%�MB�i,I,��7T{�J:��fӨٔ��=\u%[� O�fS���t�U'��mu��U����
�xE$��M���UW��^
�7��M���UW��^	h8�N���UW���VDҩᔮ�=]u蕀�Ө㔮�=]u%]衐tj9��nOW]u�KM�E�9���x]uU /4�M�Ю��u�UT�����9�v�᪫�@��sZ��C��p�UT�"���9�v�᪫�@'��s�c�w�^G��J��<��Jx�l�����!�k�)�'�Z"9���s�{�$ex-exB���sv�H�{�ɽ
9�_ӊ�J�2>����s�qn��/Ux�P��|[�@��2��Lr�N�!�vs��Ia|�,R��CV7�7ME�0!�!?�Y�4[�4��!?�Y�4�{�TT��؆��6du�l�MSQ�N!`��ې�M��MSQ��T�4��!��f����
���2��!��f���:�\�Z�,o�-n�*�7��[:h��i��i**�k cK�C�7͖7ME.V)�����򦩨@��-�Y�4[�4腀���v"˛f����
=�N;��M��MS�B���A;��M��MS�Z�"�N[��M��MSQ�^
[:h-��i��i**�KcK�EIW7�7MEz%`n��IK�R�W�n�-�őd�{&���
I��I��r©L.ϳ�Һ�Hz	�\F�f~X~~�N'at��^��|e�$�t4�K��2?�+�')D��Ob�̏���@]�ɥ�r�ÕɓT���;�{l��c~ɓ�^�]j��Q7���ļV���K��2z�JOؾ��RD�Y.�7������E �Lc��^��{V*�E �Lc���i�����@��e�e�M��7MI:}�|�ZF�4{�4ՁE)"߬�Q7�7M����je���bW�~�\��\FY���bW���B-�Y-�,v�k��x@oT ��,�Q�=,v�E��X,�,v�Ƕu*�2k��R��r�3�Lg������Ů
䙁Z��ZFY���bWр^�t�(����\�"��be��?�_��e:�e��n�]E�_j�>�?R}�g~�+O葐p��(���Z����B-�Y-�,v{X�H�PD�Y-�,v{X�*Ћ j�Nji���bWр�/�L'�����a��h@?�L'������Ů��`��餕��=,v%O� O"�����a���2�<��2(���*����8�eP�=,v�	D�9x-�����Ů����[��n�]E�����2(�����"@�9x+����a�+y¿�6�eP�=,v����m^ˠ,v�k���O�fs�Ve���bW�PD�y+����a��x@/����2(�����#�4�eP�=,v����4�e��=,v%Q��2Nk��nO�]ubQ�H9�e��=-vU(��iZ� -v{Z�*"�t�F�����i���@g��k�.���k#��$�fӨٔ�=,v�(E蒨ٔ�=-v%U���I�mJ�ݞ��?C�i�nJ�ݞ�*���M�~SZ����UD�WN��SZ����UD��tj8��nO�]Ez%��4�8��nO�]Ez)��4j9��nO�]Iz($=�N��uP\��{!��h:����Ů
�E�<zΡ-vGX�*"��$��=���#,v�lzN��sh����t:	M�<(�]���t:	M�<�-��ߥ�NH��s�68�"^{�B��;z�ٞ-� ^��Q�(��}�AE��$^��w4�sP����x-v�%��s�k$�w�����9�T?)#,v�%��s�Ñ�e�Ů$
�"]�c ���U��USh�BG"y�lq�T��C8 �#���j����x�E(b�)Id�U��US�N 4�I!#��-���\�"TIǦwT���k!�I���A{-�U��US2�N�A��2���⪩,J���9��x\5U(��,��Bi�ݛ��.R9ơT�㪩�@/�,vl��豧��\Z:�����-���	\�"�>�I��~S�N�!���H�~mI���TD�O�P�~mI��P^
�Y:��T^5[^5襀���|M�U��US1��SD���T^5[\5�<Ɩ�5�W͖WM�.RI'gSy�lyՔ\�3�P'����j��j�N&`r��\��te��%Т!Ob�̏��O�������b���I�&pq�P'�X��h姧�����\j,��1Y�DIa�
0��X-�����jJ��!�,�QW���+��W �ke􎕞�}��(E䛵2z��.��O��i���Vr�J%� �i,�QW��^5�E䛵2��߫�"� �i,�QW�WM�z$䛥2��㪩�R��R�!{ב#�%�_¡�i��1�=� ^�Eh�X*c�{b����2��2&�'�ߓB.J�$Vʘ��X~O
��2��2&�'�ߓ�:mKe
?�"��wy�E䛕2S�{~�_�� ��,��2�3�]��E)"�,��2�3�]�@�e:e����|"��P�tVʜ��}~~�QD�Y)s������:{�|�P��s��h�:�E䛅2��~���Gy@(�������3�߅�e:��^2�W�Ѐ�PD�������"߅�P�tv1�d���w��a�a[$�����@���
7�H��r��~s�-ߊ辒�.��aid���һT�o�~����ᖼ라k�����>�[�w��#�5�c�%�z'�Zh@/���2��Fj�/B?�j^�Pȹ�	޵<�Rt���1<�w}�w-�����>�G�O��"@�9x!�#y�'y�B���6s�F�G�O��#h3mdp�DK�RyW<zC�9h#C�K�Ho�z㡐rZ�P��u�:�}?�L���h�c�B9�6Ө��PZ���D]y�F�xM�_�F�k1F���Q��J#�Fj)�+����4j5[!-"TJ�5Bo�5�z͊����|����w�:5����P���RD֩�t�D���W$/��F��&Z{�R{W�N���Ci�]d��^�8�:Φ)Z:RT"�k-�Q�ٴ#EKK�J.NY��sФC�����S4�M�������("��s~�9����Dr:	=�E��e���'�V6��    �Ӣ��F�rǞzǲ�w4��|wI�v"^�����t���I�]��@=�lƁ�ړxJ���s�~p$E��Bu�?9�9g?�HR�ī�!�s��_�f�+:���t�qs e�8�R�0��RD�ͶH�t0,�5��s΃#)Kő���	�gX7����f�ۦ�
�	���42��ⶩ8�p@Gy�l�mSZ�"6��BF�6[�6�B�ܑ2��ⶩ�@'0�t�#)��I�0��$ah����ⶩ�@g�0�t�L��m��mJ��#A��GR�ۦ:�U����?[(��kׯP^Z:��P*�q�TL���k[(�y�t�.����a�uu�lq�TL����98��_[�B�R���1�-����ZM;����CI��%�ZNx-`j� GSy�ly�T��0�t����m��m**p���:Y���f�ۦ��J8�u25��͖�M�:��ѥ�lM�m��mSq�N'`v�����m��mJ��)<H�X2�c�2���|��P(�b��|�������:��/���R9���Ƃ�ӕɕ.�� �K�3?�+�+)\�W��Kf�m���M�.R�fŌ�m�O�X\�3x�7f􊕞�}9�% �Lc��ް�Vj$�ff�ܯR�@�(f+f�m���M�.R�f���m��m*2�K ��Ƃu��q�Td��5 �i,�Q���M�z$$�3��{#ݿ���LcŌ����Z�"J,�1�A�=��O��鬗1�A���2�S�Lg���������d@/�Y/c�b�A)d�g��΂�)�=#ߕ,�@�7�e
E�@?����Lg�̔������e:�e����|2�k r��r�)�=3߅���2��2����D�V��r��r�S~��O
���j��j�S~����W��)<��2��{���O1�2�MLO�?�^"�?�2���0��+�]���*"��az�|_��ߕ�~@)�������2߅\�N������.d�z��N�������l������2�nE��|-��m�d�%�z�Z����W2ܒ|��|-\���h7�d�%��ӎu�%���+nI��I�.Џ����[��w���\�"�;
f�"=A�V���Z��+
A�@A���wE�9x%�#��'��ɋ �����$_�$_�?6���KI�>I�.p��H8-ep�E��R|W,zC�9h)��Z|���r�N�����&*�w��7��F{*=��z�^Oxs�6Ө�t~Nk��+Ԣ!K�F��0�o��_���iu����L�]1F���Q�٪��*�w��7��F�f��K�����Z@�i�lV�$B�f=�N�f�羡��R@�i�o:`��s��
奀�Ө��PZ~Y/\��:N���i?�����Z@�i�rz(-z�t�L44�M�X�<��u�)zN��s��Z*����;���Gϩ�7�����9-z�/C���W$���sZ���HR��S�Xv���s��.��N�k�y�;��y<[ )���U���s�Ɂ�ړ|b���s��8�"_{��B��;z�ٯ-��<�*���霃�=�����_���t��p �8�O���^h:��-��?�Bz�霳q$�8�O���^h:g�y}J�4ֱ�H�wלi��0GZa����wis^��:��y]b�k�
�j���t&�:Ƽ&�@6�Og�OWx]b�k�d��t��t%��0`�k�d�V$:P����&/A�2�3^�C�=��:y�	���2�?8F�Nށ<e�g&�`A^���:y	r5g�F��O�ce`���%�S&|f���Za���%ȧ��{�o��k �]'�@>�o�3���Lw�����}���[6Dy
1�u��S��=�o��"�p��[�O����߾�"�l�9)�՝�#]��K�X��:σ��_��r�k �]�yn�D¯Lx��E�����.��+^�b�0�u�`�%~e�ˆ(/Lv�<�u���ם2�w�0�u�`�-�;�����*����纪7�7P\wn�[�0�u�\�r�᮫���.�R�#�uU$_ƅɮ�'����x�u�X��k�0�u�`�r�����}���\��s]�]w����:^��x�K��p���`�Ń]�]w����@A��]vIw���� �.��#�uU$����yr$�V刔���^�]g�Pr��9��}�(����B���Y/H������Ң��U*� I�u[$5���z�~����O�$M-Z,W�'|�/DE�ӷPj�����Ȋ�����"}-��n�S�o�������>�*��������"���B�:��wdBWt��#ɤ�F���!%�Ew�[(���DA�&�Ew���Lz'�ڢ{lI�f"=�Dj(/����v׵�]���n/h����v׵p�5!��B��莦Ӵ������I/4�w4���u-�uM@A^h:�h:M��Z��~#k��/J�4�u�UϽ�t��Ɂj7e᮫"y���E*o�������N����#�n��]ׄl�u�h:�ǶH��[��@�Nt��A	/ ̊4��ˤ��F��'R>��+䑾	Z�"�
Y�倯@����#�
Y�%�s�h:�nIU��
/N�^�:�A	W �� ��y�N4��89��p�
/�}�4���-��p�
/�:^h:�9��p�
7��h:�i[$Q�^x��|NJ�h�hT �4��yr ��	/F�^h:��o�D�g&����"@��\7GR	�����"@��ܶE	���2��K)�u>%\4�4��/��u>�Ɂ*^`Ш>���:�q�-��ʽ ����/q}��y7G�x�@c����H�7R�-��̽ ���>�m;��)�
���1���{p}��y����_��2��5`H��[$��+~	��k"���H*�W&��"�H��I$�ʄ�H^'>)�
���1a��5p!����� ���^�}vT�Ƅt�k�F�ϛ#U���11��5p#ߗm��W�h�J�x=��M�V �� �z�$�|��H�  ȫ�@��΁*\`ИpW��!��͑*Z`Иت8=�7���-�x�^�Ƅ�����m6gk�o	�X 4&�u|&��l���-��V�7�~�����^s��9��x 4�W��ל��J�< �� ���!�}��W���&V�w͹Q��ki�Y(@'1�5O�JMZ`ZX`
�<�R^~l����P�N&b�k����˂Hw(e�1�5y�q��)8�r�9�	���&9WKεDr�]���{ߊ4#�t
�]'o?�2�3^(@��a�����S&|f��tv���xʄ�Lx� �G�`����>3��
���
�^���ε<��T���Oɹ�ɹ�H^�:y�q�����<PP����� ��s=�s-�f�N^�|J��LεP�^�:'%��	�"���@�y6�~e��ԉ;$���H"�W&�Xd"��\�L��	/���N��*��7(S�y���nq�wr�"Pݬb�:y�떜띜k� �[�q��ǹ�����*2�s]<ΥluG��J�Й;C�g�$>swr��Û�x�K���VWr"	�\�s)[����*�K �\�s)[����\l"���\�Vw���� �0�u�<���a��8@��s]4�%muG��J��#!�4�%muG��^���ő�F�#R^C-:)?��+U��y� � $��g���#�^9@/(������͢�Z���D׽�i�x?����J��8�$^[��%�PH�38�$^�Ѣ�p� j��y�P�xMK���ȉ�7ټ���t	r��L�ʦ�.��:�9��.�$���l*h�q78�Lzn����$Ew�P*��F��N�!�)R�\?�,ٯ�^��!)2m�k���
�e E��"Ӷ����\\"2=�i[]�l窗��9��9M��Z��*Ы =�}n��ݫ���b ��t������ԃ����vH�uq EH�$�ʁ�%"���"	�'!U"9���������Ta �B@�y?��!||�ެr�=�s�-�(�^@'�r>�ŁT��,�B .*�m7�H���e��D����    �Y����r>}˷*�^"y��|%\�3��3� �@������,�B .*	��EnY�� �"@���ÑT�[x! ��r>sr$ŸZ2��!�HH�I	W�����z�5���9/�>3�%Ң��kl�D�g&��^h9���H*�3^@��r>��H*�3^@��s>%\�3��3� t-��\H1�g2��!�H7h��E_�3�r�Y�o����H�q=�q-��E����hs�$>sg2�� t��sΣS�<c/<�@��o����@*�W&��^����E	�2� �"�H�x8�J��	���vH��-�H��	/�N$|R�<c/<�y\H��8�b\�d\��D$�[$񕻓q-�����GR�띌ka �$��[$񙻓q-����ߔp���H�Љ;$���@�����JD������$�Z"9�Ԑ���H�q}�q-�s��Vs�cn��K�$�Z@����l-�-�xF1��]G�k�����C�\���4g�#��<#9B����R��:�u�Ns�:fȦu�X|a }v�J��1��׿��OЩD*MVǘ���,�b��cq��QaK��B ��v�۱8���[l�
:��T��%�Z��N "߬�)�\��Q��s'0�4Y3e�g�ZD"�H,�)���o�r���cN�L����8�dq̔��������P#�-ߊ��p��Y$�cN��TG�c[)����:�duL!�(�r�k ꘓ�1�$\�$\K��$"�����)���y@s���S�g����"�:�$u�_���.�vu�`@s�6�o}Rn	/��� �1'Ic�#"�ܮ�(cNR�TR�"���1')c����q���
�1')c�oE��p��T���t1~W�z� \��#d1'�b<����Z�?o��HǦoT�띄k���E�*�"U���a�+	B�� E"U������z��D�H#�tG��H^��\$��v�#�t��D$�1�Nw���" � ����"I�cO�c1v�E�e�pi�;�NW��JD�i���i�+)B��r�� �tG���LD�i���i��By!@s�*i�;�NW1�^	��\��A�鎰�U���u�� �tG��*��h��\��A�鎴�U�"�tZ� �tG��J��@(�h���i��N8 i�M����;]iq��"�r:��X`S@'� ��i���i���E&B�D���H;]� z)@s�Ji�;�NW1�^
���,�Q\��v��!�@�9�c�m���z�e q����t[��H�KD�Y��n;]� z��Y�xn;]� z��Y������*�k M����t��NW�^�9o��(�����d=����s�O���A��7kc����NWEr
M���!�{|���4:�t>�����'�vu��D���4���~E}�<�H,�)�����ڮ���9��(t�����v#�6F�3��3��b�o��(t�^tF�
@���2F�3�����|�2F�3����D���2F�3�����r>,�Q茽茢���|�4F�3��3�t��fe�Bg,�u`��7+c:c���� :·�1
��@g���:�����w������6F�3��3��s� �����X�3��[D"�H�m��֮�֮��1Ic$:c�Ψ~c�4� i�Dg,�i�#�4F�3��3����0� a�Dg,�E�-"	�E�Z��Z�:QpM$�W0(t��Q��������X�3��HH8�`P茽�z�5p!ἂA�3茊��D$�W0(t��Q������
��X�3���"x�p����tF�^�;:c/:��?�$�W0(t�����GB�y�Bg,�u����`P�:�"9�ء��
��@g���D��h�Dg,���9�8 ����Dgh
�n[(լ��J zrN[$:c��H��	@�?��)������u��[bPi�8�䤰��șD*M�Ɣ����(e�1�4YcrN�rY! �JĜ�di��1a��T۵RsJ��1&	WKµ�N!`Ni�6���|	)Rە�S�,�)�ݿ�/&R$V�L���.��E�2�deL���?[��*D0��9���|��w! �E�4�di�)	������4�di�)	�3���-"�feL�`�߈�vu�)�1'+c
)�F�y�k ʘ�7��p=�p-����9y�q���/���
cN�x ��K�](�Ŝ$��[���E���% U�I�?(�v|��bN��o��\�(�(漷|+��J��p� ���D1~�Z��W��#"�$���>
�����@s�"�#I�]K�]+8�As� Fz���U��y$��H#=tGx�*��9$b.�H��z�Jz�A�Dr�;�CW���"5���ᡫ"y`D��#)��I���^P�\ֶHR���@�r��2��Cw������ ��h���顫�?��a.�� =tGz�J��9;䜖0Hݑ��1�a��#=tU(�hb.�� =tGx�*�o��:�a��#=t��ԁ��"�;�CW�(Bs�&�;�CW��@s�.�;�CW2���A�D����]u`1�P"�2�;�CW��_d�Ƭ�1J��"� �;�17�c��#=t� q�M���H]E z-@s�<FQ��z�*p1�H:�c�m���$��C�Y��n]u�� ☛�1����U���s�,�Q4����" �
�tެ�Q:W{=t��D��1J�j���" ��sެ�Q*W]E :����fm��Zx�J��#!߬�QW]u�I4�7�c�����-&b$��Y��C��L����ai̐�=���]$z·�1�!�)������|X��tF�^h:��(t�^tF��fi�Bg�Eg�s� ��+c:c�ΨH^�9V�(t��Q�� z·�1
��@g�5���ai�Bg�Eg��·�1
���Q�W z·�1
��@g$C��}m�����|�^�9�(t��Q��A@���2F�3��3��[L"�����؋�(�����c:c��(�Ϝ����1
���F�U���0� a�Dg,�I:k%	c$:c/:��/"J$��Ht�^tF�
�*� U�Dg,�E�y	L��/(t�>�_
��|����X�3���8�o��HEp_Ip�o�H8/`P茽茤��C�y��Bg,�u�k�F�y��Bg,�ɋ�F�y��Bg,�E�y<H8/`P�:��E$"ἀA�3��3��3'�p޿��tF�N 5$��/(t�����G�Ԏ0(t��Q������X�3*�"!����@g��D�Ү=Ie<�E�9�hP7�
��X�3���:0�v0Ht��Q���|M 	��c9_o�N�Am�ژ�ʭH+��s�50�4YcrJ�>?���#Bk�ʘr�C�XAV�?'1�4YcrF�rY���B���de�I�Ւn-��S�S�,��2�3�]�?g0�4Y3e�燞�`4(��Mk��=3�倓HƜ,��2�3�]"9�a��r�{#���ʘ��1��[O�[+@0 �9YsJ����Ԯ���u1��[Ϥ[��% ]�ɺ�Sҭ秧Ԯ���6�Pѭgҭ��"�o�'�d���w	�% U����ڇ@��������I/��+�]�?/HbNv'�d���w�����9ٝ����2߅�[D"���[��O�ϹbN6'-�]��/�fo�[ҭwҭ%�P�\lNZ.}o$:P�ZBs�9�-��;����-
���[ѭwҭ��3'��@bs�GҭOЭ��[4"�G$��K���Z/���!�lNZ(�7����.6'}$��$�Z"-	gs�r�C����j�c��\dN�w>��K��QP"���~��2�7���*���J��Z��ZA�b��60��Q�ߤ���B�iCe��P��u�+����08#��v�W��#"봄�	�f����^8����0x(��ε�N@s��V���P���ϩ�bn��Ъa��N�hBs�*��,���B�    :[0!��iC%��H��r�	$�bn��Д��7��+�c.�ܴ��)W�/eG~�u�%�17�ch����N�Ֆ��,�QL����y�o
m���Et/�.�v�gBs�6F��+Rj��'΄6�fm�������HD�Y�X�/��+���Λ�1J��?Aҭ�g��Y�4����@�y�6FI\=P�Q��s=����p]�:�m�z��9o��(���U����fqLa�V�^�/b��8�*�^"��M��ژ!|d����4�kc�,�^���$B����X�3���"@���8F�3��3��[D"�����H~��?䛥1
���g��4�Kc<cϨH�ID�Y��xF�^�:��(x�^xF��HD�Y��{���y��|X��xF�^h:V�(x����GB�Y��xFp
]�����Ϩ@�HD�Y��{���9� a����X�3������=ŷ������b!�9؜T�3�����E�0�`sR���H~�A�D�	��Ϩ�^�lM��xFE���7/_P��<����$"߼}A�3�����N��/(x�^xF�^'���<c/<��?�����
���g$?����<c��?,"���
���gT$�����?R�w��[D"��<c/<��?g�$��/(x��Q��9i�����X�3��[D"�v�~A�3������Ҏ�/(x��Q���_���<�"9�k�F�$<c	�(�ϙD��6Z� �KxF��ID�i��g,�E�y!�Ϝ,��e9_�?��bLi�6�ąޯ��Kt�)M��R����w�S�,�19#l���Dr&cJ��1���)6���9�)M�Ƙ�[-����-&	gm̔	�����9��9��Ҙ)>3� t
Ҙ��1S&|f���#A��Ҙ��!R�8Li��Ҙ)>3�%Ң!FbmL5~>���k��1'KcNI��z^��Bs�2攄뙄k! ���9YsJ��Lµ�^PƜ���HR��k��z$��I/�"]����K ����I/������
�*�dk�����.�� D1'{�^2�W��^�Ĝ�Mz�|_��B z@s�7�-	�;�B :}p �lMzK����Ԯ���0';�V��p-�;��bk�[����"9w 5��֤����G��Ƞ��ؚ��������9�-�ZC���.�v�Ǡ��ؘ���듄k��� ��ؙ����.��M1�t�3i!�)�r`1��79��U���W$�(a.r&�ψ�:���D%�E��Χuvo�+��U )�E�<�����0-`��V7���z��j��ڒ�Xn��R����Y;d�v0TN���Z8m =�E;���:�`]K(� ��h	���:��:��3���h�SrZ��k���-b�;(Yז�k���Y;��hCS�f��J�]�.�P�ܴ��)G�/iG:������V1TJ.BݿNx-@s�2��<;���
��Dd��1��Y����y-@s�6Fq��3�ԮZ�LHcn��(�{�vd%X��Bs�4F1�+RJ��%΄4�fi�"����<P7[��Y�x���/t�7�c����\�ȋ M����q�@-�v�g��Y�$�+Rj��%Ή��fm�R�~��T�Hh:o�(����U����fu̐>��+A��H,�)�E��N#��|X3d����Ɂ\4��c���^�E%B����X 4� �"@���<F4�4� �@���:F4 �b ��u>��Q �@#)B��pV�(���Q����:F4 ���E���ay�h�h�5���au�h, Ez��|X� �FQ�^h;V�(���Q�"�pV�(�����3wH8�c@c/@��;����au�h�hT o�!�9X� �FQ��@s�8F4 �� �5ۓ*���Q�9s9R�|+��^�FQ��$As�;�h�h$E聐o6'U �@�x	L��/(���Q���|���X 4�����
���Q����|�����(
�+�B�y��h, Ez	��7�_P �@#9B��|���X 4����|���Ш@N#�7�_P �� �b �D:�o^�� �F1��m�7�_P �}���7�I�_P �@�@�;�v��A4 �����֎0H���Q�K�7i���hT(���6�� ���n,*����v0H���Q��I�s�AM���K�+�:n4��~�ևm}u��v7�j?l�÷�n���Į�F]}����U*�mǍ&������(��n�;n4��~8׷��+'�@���TW��-�+j$�O�;n4�մsE�VV�x!`�q�����+ZzWT'�������Fc]M{W��+�+�vU��I/���ga��[��p�#=-O�OՃٮ�]s�/��s����sG70��#�`��<�]S��:�]=F�����`E��@�4�v���Z+��<,D��pW��u�bk��M�	�� �]}P��ײ�|^� �]�:����SAA�0����"�7oE�~�"�pW�Ƒ*�vΌ_��0��OJ���^���{?���?�W��E�`��_�կ���<P�1f��uo��GeE�e���lW���oʚ�y�_k��ϖ��I�H��:�ŏ��P�M~Q,�(%�{`�k��/������80�5�{�$�(�_��jz�o�G3���(�_�2���w`�k�#�/��� A�F�F��W�4#�.T���H%|f�������{�$>3�%��D��8�J�̄7/�	�GR	���ʟ]H���W�\�t�'�̂��ꅄ���O�����y�H�yo��W��O�Y���F�/�H�~�'��"x����"���	�XE�uB~S�/���^���v �O�@*�W&�l�r���?�I$�ʄ�qp�H߄�����~e��_e��X;�H"�W&��~���]�%����Y���z�<�7��;�رjǯH^pױ~o��W�v<���ulG����g�3ҿ}�h�8͎-���������"@�iF	�����k��ٷ@��=�%�v��́*����u�k ݦ�Ƒ*���C�W$�4�v[$��=�2�0��@�iW��Q�o��H�v����o�D��"сĿ� 2~�Ie�_�'���B!�q(�r�Z���9��<�-�ȹ��~��+��^sR�ٔ��\��_�.��M��f�)W��������ٜ�l6e+�"5������I��m
s���;2ԿM��h7'��M�~C�y�߱���oN�7+j��#���HH:5�Mm^��_��pNj8�Z-�"E�K(�t��:Φv�ƚؿ����r6�\����N<����G-_˗F���a�c�ԓN�a�[QM�Ű±I-_��F���K$�|i,_�j:@�%��Xj��X�4
�t�[���|i$�顐t"��Kc�Ҩ��b�c#�Z�4/����Q$��j��X�4
������S-_˗F����`�c#�Z�4�/�B5[݃���S���~hJ�Y݃��R��򩘧��D� ���O9����\����%����v��=��/Шn		��7ʫ lu�� ���O�4��V�A	WC������ ��n�)�'Q0M���ݞ-��e�����#�H��I!=�r�� tu?)�C�*��+�� ��~^[ �Í�q�48�|_;�(~���+��% ��_GR��_�i�i:(���s�$�i�o�i:(��?��2R�"��'�����\[ �A������D�>(���q1���x�H⃲!������&T�J�4Y=:�{�|��wA4�@U�~m�D�g� �E���@*�3�]!O�!���l�D���8�o��|��w��5p"�s˷�����D�k�B�'%\�N��tZ!�E�������xA4�n$�|�H�w�� �^~M��>�g~�+�鑐�q$!�	!����@�oJ�2����
��Q$�H%�ʄD�!��    �?�I$�ʄD�+~vL��~e�����5��~e�����Q��|M�%��n���)�N���a��g�$�rwB�%�lL�� �;!�hz��4k[$�B.���M3J�2����B44�������$�\ M���6�-�x鞄�+��@$~N�� �'!�r�� ��]m�$޺'!�ɋ ��]�pi:=�tZA�E����T��tZa���Ӵ��B)L刜WL�11t��L�r��
�tL��N�B&���v��ל�kV����@r=���H��"��}~�Q���ٜ�l6���4g�(�YG�9��l�*�D�U��vsR�)��PW�(�YG�9��lj[�B�F(tIG�9�ᬐ��7��@$�Ng��H��r�+礎����_ ��
奀�sR�����o�+O����m���.�PJ��In��J�4/�����f�P�t{��i.t�y��J�4�/��4���mU���`�4}�dZM%_˗Fa�E�i7�|i,^	z:��_�P�Lz,�:���n*��X�4*�c"`j�6)��kF+��&����/��K�0��bh�g�~P^�#�:3��~lC��|��Ld�a{0��y��ᕐO=���X�f"%���ǯ�a�����+!�i���7�0�q��e��(��S�L�� �'G"�� �:0�a��7���Z`�<��f�W$:P�1��y�I�"/H3�P�ۃ	��Li\��g�z�%��~n3�������6�|����ќa�� M/�/�{�T��G�-H�Q�/u�_R6�3l4���Q��L~O, �i:(����l#��{b���,Kr�R�- �pF��I9��ƗF�&"����{R"9����K�Fs����48��1�_�2�3�] M/�/��MD�|�OD��80�4xzI�hΰ�T��� ���m�*�3�]1O"�o�^R6�3l4Ձ�"�<��l4�k��y	`|i�m R}��O�C1�80�4xzI�hΰ�T��� ��Ƶ�C���6WZ�$���Ҹ����}��� �E�izI�h��FS"��C"��6��}}�_�2�|�蒴ќa��"9 ��%;�qH����I����HF�hΰ�T��F1-ֶ|+��JȾ ��ap��6�����e/�_ ̊�HF�hΰє���H���!�|'�\x`h�X$�l4g�h�HE�Y%�l4g�h*@Ӌ ���6��'�h.d	g���ќa�� M�4�vn�
A~A.���Lc���ќa�)O����LF�hΰ�T��fڵ�C���6�*҂F�q��H͙6�
�tm�=�8�Jy�h*@�1��FBi�9�FS��h��~P�v:�oo�w��lޱDJ��0� o<F�Ӭ ���N8"�Vs�mR��-a��1���zͦ�_�0d�P�1�h6'5�Mٿ|C�j٦;:��9��H��Y/���F�u�7�Z ��!s ��%��@��zϬW��qHd�:Ί2��z�ub���:��M������Z@��hˆ|m,_	z:���<�F"�cRX&�^& ��m��2)=�j(GG$7Z�!_��Fa��Hn�jC�6����4}�dڵ!_��Fa�E֯m&Rf=��UL��Q�/�k��Y��i�t$Y�u�|m�	GE0��h�|m,_�q�05ڸ!_��F����ر�EJ����Q��0�ԏm*R�/z^���a�<���JЧ���a�����+A��z:��1b��w%�S,x��T�!b����S�P�!b����^�a�<ä�s~���^�a�<ä��s|���^�a�<ä��sz���E��m*R�2A�W�ӱH��܆"��④倗 f�:�0)s��`*�� f�:�0)s��`
�td3L�g��9�s0�j.|	��H�A1B�G���a��6�>(����|`�i�S����
{z$̌��2�a�86���#L�l�9����Q��m,R�{~r
�HJƁ�ѷ�H�����eZ����	&e6�L��^�`<����f��)T�k L�'��9�s0	{z$�{nS��~��<��Ә�P������W � 0`R�`3����%���L�l�9�B5�0�4h�I���0S���G��{�T��(�EI2ځ|��L����ɑ�ʒ4/^��`3���GE0�d��Q�`3��T$GE0�d��|_��j.|cm��T(���	j:4��%k�H�B��OND��[�an�hnI���0S��� Ɩ�U2�l~6 �l�SK�*e6�LX�(�m[��|'�\"y��4�F"��|r"�%�V�X%���f��)P�k ���JF���0S��Mc��2�a�@��"ᬓQ�`3��$��P$~m�r���9pm��NF���4S�A�i$���`3�����L#��4�i�@ͅ�b6��L��>ӻ]����ќ�6)���B��tL��NSz���n�����l��һ}�w�:ᵀ^sR�)��gz��PE֩ٔ��3���鵀vs�m$Rf=v�UP�K��~Sz���nW����I��n��ݮ@M�t��:N��>ӻ]
Y��Sz���nW'��t6�n����k�@M�ـ%7ro����k#QO51�7��H��F��぀���F"�D:A�P�H$������Q���/�L��|m���Qd�,��kc��(P��QL0�{���Y��j:&��vo3�2�=�^aO�����|m,_u�ӈ)�F6��|mT���bn���"%CHr�1��1���EJ&A��j:6�1�޶�Hɿ$�SPͅ�bl�ǘ~�^	�T�� sL��~p^	�T��#!�<Ȥ��c��>�"�S�m.R�1�_y	`���6�@��௨��G�oc��9�_QM��1u�c��9�_Qͅ�"�<ȤO��x�PM/�1�k��T��G�/T�ӱH$���"�������G�p�cR�'3OT$GF0��y�I9��p<Q���G�p�dR�'�u<Q��#d�6��(�_�k:0�A�ql���b�E)��H18ƃL��d���=���2)Ǔ�'��H1:ƓLS&|F�K �2��MF���Lx�5�0�4�6�>3��\)΃L��d���5�0�4x�I9��p<Q��BH�p�dR�'�u<����H���F�O�����K sL��G(�dd|�K���"�<ǤOf8�(\�k sL���*Ǔ�'
�\)���~���|O�����Ƴ�F�|_z^x����l��*�W��	sc��Q�'3O�G�0�d,�Q�'3OT���bn��2��d��'
�tt�Kַ�H�#ߟ�,��F����m0R��w���\)��J�x2��D�^�]���
G�G���GB�Y+�O��x��{	�ݴ�F*��\�y�ٴ��E*�I��� E�Y)�O�g�b[��ZMc��r<��x�`��"ߤ���'3O��5�N��m,R��:>9Yvn��F��m*R��:"���BH�r�HǓ�i��a����4��HǓ��'
�\)�-�r���}�5C�9���ü!�*��Z�ٶ�H	&�}C�5B��<�5�%�LKZ	|:��Hj6�%�LKZub!��:u�Ғv�%�
嵀~sR�)-igX�*X�K�m*Rf=��UXs!��:u�Ғv�%��5��rNj9�%�LKZk.�Y��SZ�δ����Ú��ȒV�6����5�� ��ȔV�6���5 ��ȔV�6���>��|�ܦ"�?��k�N,�S�dK+_��F�r���ɗV�6����5�+�Nδ�xm��-	F�9�����Q���"�aj�6)��3��\)�N޴�|m$��X$�N��|mԉe˃�1d��|�\M����1� ��)o�r���X�#%��O�5@��1�c�A{%�S`M�2ud��z%�S`�e˃��$���A~{z $��� ���˕	�m2R�9�_#-�	�A&=ƟS���"�$S�I&=şC��\�<H8�2��x�ߌX>�I�ΓLC�69�Pp�e˃�_�d��e<r���		�I����#��$�G���U<r��DZ�<H8�2���ɣ\�2e2�I�� �  �\s��`t��F#�'��Rpͅ�bt�G�L~R,?)��b�i�mU}R,?)�t8�c<�T��iF��s��2�e�2�3^"-[$|l��*�3^pͅ�"�<�4e�g&���^�e<�4e�g&���˗	�a�S~����\�k �L�g���2��,��q`�i��p�����/D���,�)��g~�K$/�2��{�o����l._$�w�^2�%�(J�!����	�2��\�<H��MG��_���l.��c,�)ˈ�	F�k�1�e�"ݿ,_��`�H�Hr	� ����2�D��=�{�0Yߦ#�|'�\�ͅ�"�c��TH�HrA6�0�d���%�|'�\��e˃|�`�X,�Hχ�#k	��4��T��o�����ʃ|�m8R!�O"�%��H��s�TH�?�?�J��mڹA�
I~I.��r�A�I/�p�e���eͦ�^�#����,��FG�i�6)7o�zR�O��?�h�\�uD��	��kIf��Lfj��`J�zMi�<ÀY���ٜ�lJ��
�\�<��k�d����á�#Ō^�&#%��_�^
h7'��Ҁy���>�Dҩߔ�3���4��Ni�<ÀYEZ�<H�m��2����"E��6)��#��7�h9'��Ҁy���5��s�sK���{b���RX]���������a��      w   n   x�M���0�3.fľ�������'O�RFJ� ���%�
1�*2�`���[��#��M�F�zD�#V;K��G25���B} ~�Q��2`"�/<����*      x     x�u�Kn�0��a
~�(�.��9�T���J���gT	�i��$~���*�Ʌ��(N)dfM�^ē螔ć4BXz�imdz�8$��ċ8	���`R.�eֈN1I�c�m��`��s�,��3a���o�oy�K��'��nߌ�S��Qw�����;@��|*^�ӕ{W���ʽ+BCu�����"�]����m��"�����+w�	�HuEXX}��"��}�hu}_ h]�L��+��k]�w�sG���Օ[WȮ�\]�����J�	      y   �  x�m�A��8���5���_���*��Hq���Ӯt*��n��_�c�w��������e�}���m?�T�$�_t�.A��tK�z��c��v�Ϣݮk�M�c�����'���#�]����G���x5�6�2^���J����n��� Ɩq%�(��+�F��d\Y5Z��qe�h�>WV�Vmd\Y5Z�m�}�-\Y5Z�+�����qe�h�=ᮬ:�z˸���wƕU�U2��:���:}=���:��'�ʪӪ��+�N�>3��:��+���W}�e\Y���xSV���n����6#�2^��z�ׁW���v���`��x�:�`^�{ ؁W���v�}.�Z�@��V�=l�w�:��^�{D��+�F�6pe�h����"��^%��U�U��j��`WV�Vl�ʪ�*�\Y��?ʪ�*�\YuZE�����:�"���U�U��괊`WV�Vl�ʪ�*�\YuZE��+�N���>WV�#�}�F�g����l��j{F��W����l���d�gx�:���ޖz�:��^�N{F��W����l���d�g���ju2�3��Z���`�V'�=#��z���"���U�U��j��`WV�Vl�ʪ�*�\Y5ZE��+�F���C~(�_#�\Y5ZE��+�N�6pe�i���:�"���U�U��괊`�s%u(�N�6pe�i���:�"���*�mW���`��qe���[ƋսA���Ϸ��{����{�{ƋսA����{�GƋսA�3���� #
���,�?VR��buo�g�X���2�������U�U��j��WV�VQ`�ʪ�*
�-\Y5ZE��+�F�(������ p˸�j��WV�VQ`�ʪ�*
\YuZE��y��:�����U�U��괊WV�VQ`�ʪ�*
|�z��7�2��F�-�mc��j�"���U�2���wqzt��όۂ�a�yw<�|��� ��p���ý���o�F��흧o��Ŋ����w֨o��t�S����z��U�xg���c<-NF���c<-NF���c<-NF���c<-NF����1��'���%�� #NF���c<-NF���c<������h��?��j���WV�Vq2^���z��7��g\Y5Z��\Y5Z��\Y5Z��x�z��7������5�d����d�օ�]/��1��+�N�8�+�N�8�+�N�8�+�N�8�+�N�8�+�N�8�ur�K��F�����8=�:��j��Gpe5NF��x��,�G��W��{x��,�G��k=��oo�'�ժ��^�:�Q���K��Fx��,�G��W��{x��,���>�e�]/��1�WV�VO�WV�VO�WV�VO�WV�VO�WV�VO���K���S`�<��oo�e\Y5Zūpe�i���U��S���U�U��WV�V�j
\YuZ�����\/��1��+�N�x?=x���`lWƕկ7˸���{��`���o�3����&oY�1��
w�O�?
o{Ƈ�o��!x��1���5Ę��%6�h�!xo��g��on� �l���Ę����a�����7lc���R�b�Vߗx� �l�}��bLV��%6�qf\Y5Z�+�����qe�h�=�ʪӪ��+�N�v_�r����d\YuZE��+�N�6pe�i�>���83��:�"���U�`�6��F��;pe5�o؁+��x��Z���W��`�v�ժ3��x��v�a��oK���82^�:�o؁W��`�v�ժ3��x��v�a^�:��7l�ʪ�*���{�ʪ�*�\Y5ZE��+�F����wK� Ƒqe�h����"���կ��)�U�U��괊`WV�Vl�ʪ�*�\YuZE��+�N����wK� �c���pe5�ݢ����v�FWV#�-	�Zmld�F�V٢�����F�h$�j���-�c%�}��b�V٢��-a�h$�j���-	�Zmld�F�V٢���U�U4��X��j��FWV�V�H�ʪ�*	\Y5ZE#�+�F�h���wK� #	\Y��H[��<�%l�g\YuZ�_���:�����U���y��ʪ�j�@vpe�i5~"ۿN�?:����������t�1N��������߿�vϲ�      z      x������ � �      |   E   x�]��� ��.Tq�Ұ��Ax UX~٧� ��2�����WOB%����V�=6��.�|Dd��      }   J   x�e���@D����n�^��l�`�o?/�(W��C���-��lA�6�n-��hW����rN� �PW      ~      x������ � �            x������ � �      �   G  x���=n�0���0��?KW��vm���JH�K��� �@��4�d���l4o�������y0] �Ck<���ф<��Խ���ܠ�������0������˭]���a��͂�J��5�2�p����M�2��F���׶�q`ݰ$x��mT�e�S��,�Jg�7��!+��x�	~�{b�e�n.oC�b��l�@ݱX�׷F�yT*ā˯D&(>�z���b��l�vP̬�3%�sf|ν�d�A��ee͗�������e|��q���NF�,q�U-k`ulTuil�59%p�o!��%L�1�.e[
������~?���K�?�      �   J   x�E˹�@�x݋��9�)���$A4�DN�D%q��h8����$l�� qId�c0�A���zƹ����      �   �   x���Q
!�o[̠f�{y�_�k���
�r����B�@� P�b���y��q�D����{i#�t�I�#e�A-��ر��T�S۾�S�mM���b.��L�W�_%ڮ�	�7�L\�d�EWɄ�d>�{J)��_�      �   �  x�u�Qr� C��a:`�����h6�m�V�ˌŋ��8ӧ��eD��v��#j��E��鳗��Xͪ���R8R���E�H��H��������)KR�g�,E!w���o��jyǯ��o��T�P�Sr:et�BP�U������E�)�))&�)]֥6�tA�)<�r��0��U�w�
Q�;�E9n`��!K<���GQ3�PC�q�q�}h�<��r�����P�[j�|E��h����[bu�#`�mׇnoP�n]PЈ�P8��P
�t*eBQ-iĮs�q����2�}�2��CJ�;e%����}y(�&��FR��Y��H!w��x6 �!{O���lF��@� o+#��M-���1Z�*�嬓1����Я�^HF�>j!y莀�ܺڏb��� �I�@2�A2��sA3kbrB��ٺD�b6m��*���2'�Tg9J������j� �Q
�F��zjpN�Ng=�]A1��dVG��ő�Gi�����8~ 5-�      �      x��}k�5)���[{ɱ���^f��h��:���t�T)o�8�#��{�����\s�5�%k�/������G��2��9 ]�N7�Mt��v��x�C�r1"�Nt��1N���{<���c���w��H!`�僒6�#���L�r���+S��m�c�郒>��ٮۆA�ٮ�A�
r��l�Ȕ� ����ZcJV���k��Jf����0��YA��SS���N��Z?�����/ũi��ی�`���f��%���#N0bL����<��Aߌ1�Ճ�X���9Ƙ������G\��`�#�7�Sc��3ckg��B֨�83�3#{�#��V��������c�1Z��>���I�����!�o�����y�o��F߫/��8���1>�v�Ӎ�I��F�َܾ�E��7�G��W�1�s�yY��+�rn6�a�di�f�@�J�H��<�LL���!���v�a�s�y�)9��87�ǰ#S�����l��A�r�P��<�{ ��>��A��s�x$q�]����A�a�_�%e8�����;"�㎱��ݘ�=�~n�Q�F�@�qn�u|0���/b����?��H��W�%������g�%���G�僑5�r~�1fd�j�a���9���aWJ���\�7}�Q���/)�T:�씢��қ�����R�<f ��N;H����O�7�-R���W�(YöD)Z �d�3�hy�8��d�-�Ĕ� S�32rN�UL�r��&!#�R�����1vJ�r����A)Z�탿���咙�d����X�������<��j1�/��T���H-���>�[1�h9w�o�b���!!W�S��"��c�-�ƌ��S���#3�qR�-��Y�G$;V\"g�~@�S�%���L�N�㫳�,tVι|P��4:+���=� 0����߼�y�9�WA�F�oK~�7�k WA�F��]~���_��WA���]}n�������&�,<��'8C��*<�,�� �Ux;0%{�WA���L��Ux����>�a_l�*<��A�
�*<����WA��nL��WA�dfJ�1�gA�1��kێ� ��1����q���_�� ��11��� �@w�o3R�pj�1!{��g�"/�����@�Ff�~hę��Y1���_;��c�Hv��9�1/ȵ�B֨�<�����.���0#;F;��G���1R�5�� ���֚Ƿ���+�u������^P忐������f���Ds���p�M�@���Zk����� #�Z��$%g��k�^&Jv�\k�)Q���Z����$�Zs�خ$�Zs7��
�k��~�� �Z��K�� �Z��ɔ� �Zs+��d�Zk��Ȋ1q��/"��͵V/����z���یp��k���b�j���w1B���#/�c�Z�߶+�̵V�� �6�k�^�'�#�j��U$�1S�5���i�ȵV�#"F������'��Y��Z��,#�p�)O���q����ٳ����A���_����0�Qm6^��7�=R"m6�j�b���e��Z�#�~�;S�q`�B��lL�� ��x��2%+�Jy�W���Yk}�b;�;��^�'Jְ���>������{!�)�A���x/�%+HK���eM�
r��0�/�2%{؅�x/�3%k�Fy��������{m�(�A6����9�#}é����b�/Aqjbc�#~A�S��s ^����o�X��~���1��g&&^";�By���d�h����	3�R)���bd��p��Bz}����k���k���^ݙy;h���k}��x�뷾ɮ;�o�����������0���}�w��2��^gFְ����B��a��
�lt�<�&�dy~Zþ��d�<�+�L��_��ǰ#S�����֘�5�9���)�����v�i_A�Ly��D0%+��y�%�di��[gJ��+��0%+I��x�5F��o??��#���H=�������8����MF�9���7��<@y|5�o2R�]90R?�;+Ȃy�_��gg���x�Qb�#���x+��f�q_��c���!��1�|�O�}�Lp��q��s~?�1����d���H�����l_�L�M�'�`��6�V��+F��f|���c��f�F��=�F��W&��:ѝ6�v�m��� �͐�3H����#S����\Q�1%+H�hd��d��>v�Y��,�ٴ��\Ao6=1%;�ʛM�L���fcG��d�i�i��}ۛv������с7�����7�d���� ���@�}P2����$+2r�d*�ٴ�kd�h�ٴ���+o6�0�h��{�o3�i��=�7���;}�}�?��O6^�����;���'/����������tDۉN7z$�����=7���'���Ư:}�ן6<���6��b�t��41�clx��Z-3���d�uLbd�8�a���]��� ����F^F'FV�#�������������J��1,�5M��Q�ȯ8��=�JE#/�%ݨhd�P���T4�1Q����F~�D����/,��Q���=;�v���a�� ���ϔ,t������d��Ѻ5�i�A��F�?+�JE#�laJְ���(���T4�=Q������K������X����m�~}�+�#m7��{��vx��D����s���5���xMЀ���Z&�~�p�#�0���w�e����>F�xY�����^v��zK��@ہ��9^+�
�L������9�)���:�F�%�^of^6<sVo���h���oux�w�Fy�_y|�"����} ���(���r"f�sjo僘���~#�`E�%�~]�Ǎ�_����1+Ҟ(���~"f=s~o�E�<���	�ߦ}|1V�F��5}�"���ϋ"fG�8Ƿ�A̎�s�o������e�־���ڌ�s��&#��׼�w#/��e�l�kG���/�W7<�~���,��k�7$/��_���0j�Q�o�Bg�R�_˅N�K�t��w���}�00�s,���u�!�(��r"��Ո�3�pu�?F?xY���-�o_�����{��e�h0J�k��rZ)��c��@'�-}��������w����僗h���H�#'��}��V��胤��Åt�B)���1k0���n��H� 5},�=��'��?~~�6xDZ��/+��g��������~�"M���k���$�uh)Ҏ���.�<��hG�u��{��	�7^S<���ܾ%~tPW'Ջ�N��*�D�B�������f�hܼ����8y������L�
4���,2�)L8�=��6��%��y�,��d~}��~�q������?h\����3��z�0/���=tz���1��6�� �M�-������@?B�5n�eZ�B=��rYC_i?��/{���oY������Y��K�Y�'bv�O~��`G�����`��5�N'�"� f�z���ዘtr�&fE:;������"m��_`���l��,%���|��C�5�O�`h6g���_h�����_h�f�5d������
�V���KB{���x�5d%���\�o�R^hu=�}92;��r�V�m�;,#)�Ц�{���B�>����B�<
fDj.4_�0#Rr�y�,9����	Ⓜ��.�Jo��K�wx����z���Bk����[hmup��SZ���Wx����[��*��j�z���B�{���Yh}5o��S2����.{��������j���������V��3�,Zo{#��S��s#��S�
ݓF�!&c��]8 X�+to�i�W���=d��Џ|n$7XI+tO��e�����$ �2V|����
}���JV�oU��(��Bߢ</�U�~��U�4z����T�3oRQ�{�	??)�н�C�)��[{���z���TS�>.�X�)tg~}RK�{,����B�O��s�    �z��li!�����V�(t/�$ +����N�U���ǧD�ʹ��)���CV
�7�JA�����)��$[Z?��ɖ�O�)a���zʘli�
&[Z;�����	=UH��rBO�--��S�dK�&�|`��ez�liՄ>)}��DzN�lḯ�3&[Z2��ɖVL��0�҂	=W8�j���.��CVr	=w<�j��^<�j��^k�VB/�}ޕ�_R	�\��,�z���X
%�r�o��I��:�_`)���yl��S*	�\��;>%���ul���4�]���$�]��;>����<��CV	ݮc�V�ݮc�=d%���:��`����:���)q�n���?����<���)i�nױ�+e�^�c��F��:��`���k����}��F�T�V�y��(Bߢ:�+M��5u^��$���y���R�y�X	"�-��+=���t^�)9���t^�)5�ް��z����B�j�Z
�7�yj%�ި橅zÚ��A�j�Z�7�yj�ީ�Ez����@�k�Z�w�yj�ީ�z����?�j�Z��w�yj��ޱ��z����>H�A5O)}0�T��L5O)|0�X��5O){0�T�L5O)z0�T��5O)y0�X��T�L5O�w0�T�r�`#�j�a#�b[p#�Zl��H�����H�����wxB�``;f�R� ��,\�p�,\�p�,\��Y�,46c.%��y�C
l��K�����K}�n��Ky��혅Ku�t�*`Y�p�,\jp�,\Jp�,\*l�,\
l�,\��a.e�b.U�A.E�C.5ұ%q^`!i0���p�h0��p)h0�	��g0��)g0���f0�F{�3�J{CZ� �R�1�J�(e�J�`�e�J�`�#e�J�`�Ϗ�=d�b0��p)b0������0`�,\Ip�,\)���,�c.��ae�J�`�e�J�`�#e�J�`�d�R�``Ϗ���.؂Y�T.`�,\	������(W��!���d�R�``f�R�`��f�J�`�e�J�`�3���`xJ�``��Sr\)<�V0�Ób�q#QZ�hn$J�``m$J�`�#m$J�`�n$R�`�3n$J�``m$J�`��6%R0��6�Q0�7)Q0�7�&
ұZ��`!P0��6�O0��6%O0�	7�N0�7%N0��6�M0�F��&���T&���&����%H�8p#Q�h#Q�i#Q��p#���q#��\�D"	��D"	��D�p���#��'�F��l�z�'�6��Djl���"��'�D0�O$R�`��s����s���V,�H�nXΑ"ܱ�#5R��s���,�H��XΑ���#�8c9G�p�r�TX�r�����=0��9Rz`�;�s��@
���؀��;0��9Rv`��s��� g,�Hс.��h́6�I��� W�J���@7�K���@w�L��@
��T\�p�\\�p�d\�����Uht�t\Jt�|\+�AB���bF�u�aJ�e�SN.UR($�Zd`�f�Zc`�#��Zb`���Za`�3%�R``�d�Z_`�Ss-/0��'��R�ɹ�Nٹ�H�Hϵ�� �ϵ��@GLе��@'�Х��@gJѥ��@Jb���@n4RT`�+�4RS`�m5RR`�;�5RQ �z�f#:`�.�R�su)'0���u'��[1�Lٺ�!�Qi	�a�.��b�.��Q�.��jɾ�JG �v�d�1*����K����K�N��K��Δ�K	��.��K��6�ܫ�J����F����NɻTH�[d`II���K����K送���� �r�@g��p�@J�n�@c�������Vhj�עM]�Z3 Ń��d�@L�b� c��`��zM��Z.`���^�45�k����n{�0��n������R@���P�@Sǽ�	hl��2�=�Z%`���^�4u�k�����{-0��w���@�@S��H1R뽖h��� !��� �0��� �1��� ]0��� m��ka��f	�����x�|�B>N����k �Nt�г;-���Ot�г�,������x��DGR�I�W2%��C탒G�)�F�j��da�������BM��� +
�L��#� �Ӥkj��Y�&�t>X�&7�o1�WK�;���-Frdm;>����D�4)��{0�O;����1^v(��c\}�otg�6z5��)��f��Pj�/B�1��S�zZ1�NS�Qgfd��v�z��`F֨o��ژ��&;���k�<b�����Kd�v(�5M�� ��hJ��ꬑT�)����XK���)���պWi덮ǉ�����mzv�Eg�7��]n���3����|�gM��W0�r�#;���c؁)��.�n�@W�d���m�N�� O7�ǰ/�����c ��������H����A.t�C�,�D��o�v����bܥ���3�bܥ�7�0�R��caF֊Z����#��c�C�����1���ufFv����?F}|02�K;o�1#+�U�y;᤟1����ya�wi�c����wi�=����H9��teFv�F)Z�o�op:�])E���o��9�F)�˨���J�;�h-�]�J��y:��N;�(Ekgt�78R���	��HO���(.��R4��e�V��R��m���k�6�R����b�Q���锢��/�7I��<�@���DJ�Z���v:"�h= !�'J�z��4cLG��Պ��c��5|���Q��BaF���yڕ�i��a7N�z�%���<��D�
2��+3%3�(E[z�@�
r>ńa'��5������Wg����24}u��Y�������Y�e���$�*����U�.�@�Ju5 EzU����`�C�R�W���"�*Օ��U�.�H�Ju EzU�� )V(:�*����,� )ҫR]H�^��@J�
β��*����U�>����J��?%zU�O�)ѫR}�O�^��J��T�S�W���2�*������?ezU���)',2�SZMOo�:��\��,��)U��?�Jfy�O�˨SrK���2���~*$�����~u��i?�/�n�:��e��:��e�=du�O����I?�/��!��~*(��������>�BRz�������!?�y�xY���|?�ƪ#~2���'�d$�����
Rz�|������uD�cy������<ܻ�0H�鳽󂔞>ڻr-�,O���
˃��I���՝A-T�]*�B���u��C+�S�����g�)\� ���.ɖ>ѻz5$[�@���_����*��'��Sv8Ax�4?uy;��a�Un!��gy�dK�S��}y�O����A~�*���s|�����N��?N���!�ӭ�>�w���q��t�����J���Ӎ�>�w�����N����>�v� �S�����̗Gw`�c�<��b3k��}���Z�ۧ�0��ձ��
������L���w�w&h��27ƈh!�_�>� �T��'��2��6,���_<�z`��/3_I��L�6`���/��"�J�x��m%�_|s
ȶR�/��������_C\SJ�L[=�I!�_���S���'�FI�߂N���/�IE\�J���o>�T����CF����I.�(t��gQ�Q	��3"ΣR�/�G\�J����b���0��gF�������_<U�83J��x2q�)���:}���`i�?�Rw���b)<X��b�<X��b)=X�Ab�=X��b)>X��b�>X��b)?X��b�?X���{�B�����o�P ,���{ؑ)YA�K�����!�"����qn�
aqy��1
�2����aq}}�K!�2���aq�}�K)��}��aq���1F^P+��b�FX\e    ?�R����>~��aq�}�%HXz��T$,.��b)IX\i?�R����>~��(�Mq�	�F�A����U +sq��Oj�2��z�#Z����]R�"0؋�P"0�k��"0�{[��E`���vHZ��\c�m����t�~[�ؑN��X�����Z	���E\PJ��\g/�R��J{H��$����d+Qs���d+U�Z{���,���^�%�t	����m%L`���� �	ʪ�8x���T���RA�.��
�w���C*h��B��������2�(���ױ��A{//�D�6_*^dy7���T�d�n�~�����a���Ȁl+��n��VL�͊cBt��d�����=��A�0�34���TZ,� �Sq��K�p"�%OK�Ǩ#�㎱A�h�NL�=��$�S�ŸH�Sq��1V��3���k��T�=1r���t'�_��c*nNqE)C��c�-R
0�;n�R����n�R����n�R����n�R����P��
s��B)d����)u����)����j�Pb.�0�l+)�2-��m��\�c( �J��L�!\RJ����P�5��K��ð�s�FCȶd.�4�m��\���)%�\�k(�R��e��r�̥OE\�Q�2w�Ӈ�e.n7����\�oOR����N�Rf.�8�3����;��Cj3�i9�3�ę�{��C�3�T~9��%`�D�1��c�D�1�+c�D2��c�D+2��GTI2��7Ti2�Q*>�(���0V>�*���0�>�,���0�.u�̅��s)�d�<����d�=��-�d.?����d��Dkq&;̨�ՙ�E�����e�����l
���M�Z��z�`EZգ�D��1��F��܌J��s�+�ƕ=�@7����N�qi�Y��a�O9dt�Ҹ���ȥq�9ЉJ��$s�3�ƥK�@*�K�́6*�+���TWF�ܸ4��2��w_Ze�V��K�́T�f��4��28Qi\�ep�Ҹ���B�4�h��A:fojéQ��ܨ4�<3�si\�f�f��5s�m�6s�#�Q*��N|G��3:Ӿ �3�ж �3�hW�ޙ��"�fPk����'�Q�/'RZk�i�w�[��c$�qxқ��h_�%�H�Q������5��F�e���(�I���`��m$]6+���Ƒȶ��(�:���c�L�Hd[�l���KJ�l�i�kJ�l��D���F����vF�$qG�Ne:H�RVeZH�R^�׆;�4�(�"�S��6��H�*�6��H�e���(�#I���p���$]6+Ǎ�g_?0",7J����(n%�;�4�(�%I���u���$]6Wa�a��PRG��@>ZH��B+>ZI��DK>ZJ��Gk>ZKӦ��������J5Ms/	�t�r��nt�,�4�%��Y
j�[J�IEM;z��g)�in*A7�RS��V���������=KUM;f[:=�r��t�,u5��%��Y
k��K���T�4���h)�i�2���H��6H��47��;h��in5A��R^��l�n�������Z
l�t�@֥¦M�	d]Jl�t���%56m�N��"�g���?��a.?��y�Q��)?Y�`U�0W�|���(C���o��B�kO�'����;>Q�0W��,J�ʓ��E?*���E?
溓0%��`�:	3��梓fDUl�N��M�IXr��`.9� >Uz�ޯ�~T�'a�T����$̞�;Xŏd.�o��x,p�od�˸�n�<�ٶ�'2�˹=\虀�z�2�˚�":�2�˜�nם	l����\�����>���%3Ț��k`J��}s�L��ѷ1�ĔltŜ4��v� ���샒d��4�Ȕ� �A	i��)Y�n����2S���[�t4����5��0�x�,FZ�d4[��o2�
墹&�o3b���ڙ��H�L4cB��&��"/�fk���% �ufFV����{fd��#&��ԏ�z�0͖>Y�������B�@s-�Ȏ�(�-0#���� ��K��o�rg��ae�8���ʟq�_=*�Ɓ�T�T�\�"�,ظ"�<�rER�4t�[G��8Н*�ʦю�����i��Ie�8Б+�ʩq�_8*�Ɓ�T�l«q�W$�Y�@W$�[�@W�H*�ƁnT��~�ݩ"��Xz�����"�,:RERz6t���4m�LI��8��*�ʶq��+�ʷq�+W$�q��^x+�F����[Y7��´kk��v�7��g!N�6g��Z3��l�W��օH��g07/�ȋ2h0w/������8�ڢ����_{4����&"��j�sC�Qi�`nd����i0�2��X5���v��,D���V"��j�sGCܑ�Y���!n�ڭ����dm�`nl��K�sgCܕ�a���!n�ڱ����em�`�o���l0w8D֥i�M�Cd]�6�[�ެmlz�����.��;k��kN�i��1��UI��9Љ��ʺs�3�$�w�@�R�l|�{�@W�	R����"H�wt�{ e�inA�.)I)�d�VJ����]�B���1X�p��O�w�L�pp���T�07�����0�������0�������0�����T�0w��� ���$ԕ�����d ������$ԕ�����$ԕ���M�I�?��a�4�+K�s�I�+K�s�I�+K�s�I�+K�s�I�+K��n���P�0����S��N�PW���F�fOIn�MFX�Jq��gfD	n��L(�s�� ��6�����6�e�"�8%�a��K+�6,}i�ԆE�/�Tڰh���B���Rg��x�S�̆M�	�NQ*6m'*9|�l�t�H ���p��6lzN��ذi9,+}���B^��p��R]�b�/��0���<��a�6�7xJZ��l��RY��k��RX��k��RW�b�/��հ��K+U5�}&�O�j��L��Vjj��L��VJj��L��(Es�	�RU���z��r���j��<nh1O3�+�4fʖ�`)�1�`���	��"����ԍ��F����јIt�+�H_���D4<�{����H������$4�8p,4�%u ����Td�5$�3��,")��@�T��w�H����= �R;c��E$�3���"����Ӏ���p� ,��a.;�eY���aɲ�|�HV\PX�E����o��	��C �eI`�*�2��'H��dA`eK�ʎ2�0O=�(�\5�7g�I)[��JR�W�|7��T��Y,�����pi`<�^�<�h����y
B��\��U6](w���87�^�Jhte��5���� �tD�͠6̟��}���0zN��c�JLt[�L6̅&p�ˆ�`�(�s��*P>v�D?�����~`����������
���;����`�=_]�Q+�������mm|{j���ꎴ��������~�ݢ���>�-5�=���4l~���Pi������!�4�������4��-w�C�i�ߎ?�=~4�=~�3px;z��g��v����4�oG��L�?��=~H3��=~(3����0�����]�G��L#AG��L|��ޫH�2����&������I�G��L|��ދH2��EO�1���������5�G�bL�<
�?��,�G-)�d���Α���#����	#���M��!@�0�{0fO�0ٴ`< >!�d��G-��dӀ1XH0��/�QK*0Y:�� �M�E`Y�/��/�QK�/�4_�U�ԗ̽�%ŗ̽�e��di]���Bz��x�ZRyɦ�",#%�dn�G-��d�v֧�]27]�Q�K斋P���K掋P���K憋P���K��?�O(.YZ�į�%K[��zKS��TRn��&~��Ԗ��._�[��]�����T�H-y>%�JKs+�h-�����Y�}�A�Rf�S���PY�i����Ȋ1^��;F����漇-%�r    <�7X*,�N��,�{7����W��cP�!��變l������񇸒k����m%����J���(+�9`;ǯ��
��򇮒?��+H-�d.�w�?��8��?��9���{������aݟ�`[��]�."է�)}R��*09����H�JsJ+���F+wJ��ņ�=��^�b�ҟ�<oE�aiPi�������$6,-*����"(6,M*�"�?�Ri.y�b�Ҧ�\<& ʧ�\�ņ�Q���
�8�gUq*} ��<�K"�a�Ui��b�Ҭ�\ ņ�[������*�[�k�Wi.f�b�Ұ�\�ņ�c��v�K�J�,�~hop��AJop��A*ol��A
ol�+t��5�S)�5���T��
	���F�JJsk�;�Rr�b:0[��[0Y��[�b��z[�b��r[�b��j[��Q�۲+u@(�-s�thRR[6�ҡ�D)m�4K�6%�e�,z͔Ζ�Y:��)�-s�t`Y�l���C�'E�l���Jc˦[:�"%�e�-��Rز�dyR`��-�<��e�.XV�Z6��a)u-s�t�5S�Z�r�����2�K�Q�Z6��aF�P�2wK�^3%�e�-����2wK�^3%�en�ӧT����a�����Y:�����M�t�=%�en���fU)j�~
�KhA-#��	���rP��!��{9�Lh5��5Bτ��%@%V.�=�͈���%QϙsN��K��3-���ur�(u�r9�B�aK�\*��i�\�~�h�r]��h�������	-W� �k�%r�h�ϴ���`����r�l?��Y.b��gZ=k����-ų�:���R;�%'�MKg�x��i�,��&4-��J-�����5����,W�>4���؈�E�\�;Ѵf�'��Ť,��3/�ܐ���yLh����Y�mH�z�}�6�m��΂>ʷ�VF��z�����s�y��]LҺ�<��.&�]o�`g�4��RO�G�½ޖ0	��}���v1I�zs��b����w�����`�,�ͅ��Ly؛���cgibo.c�o�����>u�6����2�|��*衳2�7o�w����\a�9K+{s�|�,��trߕf�V����@7{sl'�v��
�E'���et��Nڛ��v;߿�t]E�����Z�������ۉ�z�}k���K����(��/~��z��'��T`��d8���?�=�\\�C쮯��b��C�>a��f�҇�=����H���K1�ҁ�!2}3�z�T탾6��>�nm��-t������������bJ� Ja�Θ�I]�.��IY�6���*� WH��(��6���&� _/�!I@����O*p��O
p�#��؄'j)8��Rp����� ��� W8MK)��mp��J���,-� -���Rp����� G8HK��Mp��"���-5 x�ռF,$ ��- ��Z
 l�#���؎'h)�gq;������,��,�UԾ����,�)鿁5*�)��X�S�ܨF��pǋ%�g��Q��bD��p����7��:a��� g���i����J�o*1@MT����^�H�?�c�&����&�rSbi���`��7�4�/��\j/F�ҟ7`���s�`Y��M�`Y��M!��J�?W
��)��R��"5��0���$�\NfD*�Mi�)�7u^��-��\
���+�`c�T��l�=�?��<�������
��i?��a�(�����C��7 D��y�[�)���#L�U?��`����2L�M?�N�Y�!闗J�+H��ʧ��ˍ�����N�#?��r��j~�c��1�ܱ}䇖_��?�C�/wj ���;u���s� [���
�?d�\T6�*~.���?��������jy[J���	l�?�\�v�~.�[��>���=��|_���KP��}V�y�9J�����t��|�W�������ot�Ы�s$��f�]H��}27�.�*���G����bu\O���̊�q�N>��ߎ8�B~	�aJ��a_��teJ6��!��?��<��?��y�;��?���6xw3ڍN�����Α�[1�ǘ0���5�p*�>����5�K�1����B��@K��Ȏ���I�c�g�c�$d�F��Ff�2� #��9�%��Aߌ1�c�>�4�c�c��4�c<�`Iߑ9�x��>��#�5���nAL�u�������D�a��3��-��ģ�v���gM��½�nE2����Q���"Ab�Ϛ/�x��"�����K�t��{��\j�d�Y�%�P ��Ɵ5�#� ����� ��|g�P��]Y���D�ZR���/*[뵴�2�؆���8�:��.]S[<��B�㘫��5��]����mz��\�����d��u�Il��}�&#��6��2�w2�XlÝ��#�m4��ߎ��n�C�m4�|�w�b���{2!g����WL��3�|[=��5��?_�$bF�<^/O��H�����1���zy�@/���n�L���nmL���7�=�a�W���Qn�M�^A�����W�%�]|ÞA��w�~����o���Ӵ�a�o�;w�a�ٷ�Y�y�A����!YA�o�W�{��kA��6���__�u=9}�[<��F����ǻ�l�׭��8a�{>�љ����3��`_�u�DYOp�(��d��NV��r��:s���(7_v@��v��5�W���v���m��{��QZ�3�[3'3J����Uq���D�k�Ɯ�agR`��C��:�z�Qf�(,�'M�mp��{-Ӛڔ4ʊ�b��ޔtʊ�~��^�LQr�1��*��SE\���z��͛� [�X$3F���8���^1z�&��z�E�c,x�r;s���a���'}��R��]�y��Q������;UI����1n��g�H��~�O��<�_`%x�->�R�4[|��0��Z|ŧtO��א�v|�n�*�i���<}_`�	�m_�)I�����*E��d��<=_`�xZ,��Sr�7R�������'����+�5����0{R	���A�Z�'����:>qԅ��鉣�+�[���*+�[����B���Q�-�t�UM���Q��-�tYU�-�t�UA�w���z��+`U9�SP���j�gr�K��O����k	���/U���#ħ*	��/U��t���#TOS�*�u::p!��8�*���aBT�N7G�UB�n��TYA���V�* �c�����e=��/��K��	X�N��#�%�p?���_ҙC=D	.q��8�z����!3>$�@ʙC=З�-a���K����u��?m��p���uF�':]�yg8ef��ۉ�7z������':\�y�2Qx���8��g��#�p���B�,�MS�'� #V���'�o3b|��k W��������4���\1v�ij	9c��4�̌���2�G}|02R#�cFV�5�-L�L�v�[�~����.|-ڍ�F�S��� +�S�ј�$_�����A�Y�<�n��<��Q�[���>�}��"�܎�Ӿblg�1�³����������U�}��Ǵ�aW���1��7���L���˸a�o��KT�n�{�7���5k�-R)7�f��E�kd��$*�#�/�L� QC|:.|����G;���p�7ר8܃�ý*�O|��g�ۨ@?�-��p;��G�ዞn?ˁ�po�+��U��4Q~�s��#հ���zV�=Q{�탞n�T���/zV�/Ђ� ��5�B�����^��'�>�ڎ��>������t�6���u�;�
�3ڄ�?�����4��1�uh="]�����o|�k�1��1�u�=�J�_?�Ύ��U�X�k��Z�|{�����T�j�ȜVڎ��ec�6ߴvv���G������4[�����פֿ����E��R��s9Ц�r~��6�V���鵘_�NDw|�I$�;j�k)����^��dlp@mz-�����������zsv�"    r|{�鵊_�鵈__��!���ڲ�_�ʳ��k�M��z�M����J�W�������{�g�/FЦ��}����x_ߝT�+����^`%�ק��{ȪЧ���� �x�7t��������[�ߐE�zLykǞO�d����V%�:�`Q����NY ����Ny��s��������+N�uf�	���:s�`q�����Ny���jnzY���4�����?��A�=bq�`��7���?��p�S� �s�<�,9'�3=29'�#� |!.O���Wq��=2_��~u�xXC�_�2ި��~u�xx�*���፪<�W7��Q�~4*�|��>������+��y?f��kK)i��@5�OtEt��f/�Dۅ��J� Ϛ��.7z;&:i�z��|�W��46� �	�8��r%1%{��r�3%]Y���%+��r50%;�Nr9"#[�N5	 _S�����Ԕ���Sd9;>�[I,�a���Bg���[���g��3�/FOX���f������it����c��U��l��A�%9��s ɒ�y�+FG�Z-�c�3FW�ǵj�c���d�,��kd�X��s�%r�hT�%��AV* �R?�:+�F�n�c��H:��u&p�/�� `�;���W��?���@�<��>�� Pۅ���\w]�s��ߣ)uך�F���}�]k�~X䐇��=���a��6��_áO����	�$�
}<�_?.��h
>��]|��k4��L�h�|���Q�勜��3W!�|����W!k�Z8nU�Z��Y���$7v䕳VBaÁS�������;o��7�����F����n|��Ah�ύO%�fӵ����F�����Ɨ�:gk|n�}T�[��s��G��|n|��1��ύ��r������Z��>�\����s]��>'�[�L]��������Zυ�$"��zN	'����-8��W��K�qO>��v�S�CVoj}gă�|R�>$��ZO�� ��z6	�����&,s��� �e��ֻ�A�L?��� K����Z��!���zJ1�X����K���zH���Xő�h���8��?� �5���_L o��zw�ƪ7�ެ�;��ֻ��`�_��v*0ܣPh{�n�z?��A�����
(<�׳ދ5FY���o��d-�K��V��:7�
`Q���[B3�,DT�*�]�!�1�L��D��=�=dQ��s�l E��#4��D����e	��}���-��*k��*�\��U��a���>p�WY|�aJq������*K�TV8a�,<p�PYw����0����0�;xe�a��t�ʚ� w���%��+��8tV��v���V�哲�� ���s�ߣ�"���\�H�~m��=�g�=]�ѝji!}{��]�;c�6���£^1zލ��x�����.�b�C�Zx�R0�µ�>�}it��KN�o�r-<%&p�q-<��i��p֙O����ܘ}*!bsd�~�º�uI|l�Gn�\��I25H�OK���Ư�j����§�J5���5���	8�J����ߨ�u��=;�N=�#��E�7���],ѳ{BÑn�#?F�
G����{'�_�z6>Sg���=+�����=;\Þ�p�L�s-�F{|���iԗ����ڎ�Sg��6~���M��|�`E;�1��1�5��A���<�9|��Z+�k1��gzl0��k1,|�xDm�c1�p��üM�9��4�|F>��f粴|��ׇg�s�Z>�M_�a'_��O|�`uǬ�}$����}���K���+ߗ���	��/}O��\_z28hӗ�C��P�/����7i��"��'��7i�������?�����T���ʪ�X���|�&�^<���o��ŏ���MZ���ީJ�?��;Ui��|�&}^� ��ߤ͋���7���Ux�*M^�2N��ǋiᝪ�x�C�S�/~@�w���ů?�5��w��2[�����+P���.~d�"�6w�Ca��K=J�"��v�G�T�Q�.�(��0�إŨ�|]��X�Q�.uf�pu�G�𥕦.uf�P�P�.u�P�P�.�3<,�(G��a��K=f��;>��RCmi�R��<̞rs�~o�U
e�R�V��˥�.�=dQx��F��u^��(T١z�|�dաzS |�dѡ��=�8Us��TfycEɡ��wXp��P��kh��P��kh��P������P�F����P�}8�B�_�c�[��߄c�[�j�g��Y��Qi�
W,?
}u}?��0�/D�jh^���2x�{�E���x3��0�	/F�<���
À��Q`p�C�e}a�+^p�(/x�Ⱥ�.��~Hs�!e�z�in�&�����VC��՟�b�d	�?΄]�������u>}��QiV��c��՟�a�	�Y���(�a="��K9�zl]�א�a�'�x�Ub�5��t�E8���s)E8��R�p���+!���W:�5x�
��*���-E8aO�� ����Z�p�4�H5l`��\��_�p�w:J~p�;ޡk���-R|p`�xH������!�8��K�<8��^�(��.Р#u֠AG�l��:8�;X��� wz��4k��LRrp`dLRqp`#�KRpp�=dQz���!���q��a7��ǕW�f��^��+��DgD�e�� ] �?�%z;ц��b��'�"�;x�q����>�Mt�vb���h��'�f��.�&�M_h������f�_�ƍ��� �g��0��Ź�O{G�Po 7<pC����������3�p�G�z��%�}f�dn����=��=�3���������ƾS��#;���`�q���fp�{0�������`n���_����`n�'����`�����<�5��/�=�	�:�}�CA³��z+
=,���+�m�����k�J"���׽T�`J��UopJ'�sy&����Ot���I�4�< �ˉ�7z$��č���{m-��֑	cl'8����c�	��W}�1��1^��>Y1^�G���1��G���}�Afd��>�������t��dyW	A&�dY϶�ǰ����7�@S��}5�>�?(Yþ:G��A�B����23%;ȳw�1��Ӿ�lW��]���{��>(��nW��ݙ�5����h��!i��N�g}�X΄��n�����c����5�z�쏩9���:�*Dݏą�Z�-��W�j��/���ժ���9X
�Ր�~�ՐKQ?d�j��Q?T�j��Q?D�j�R?4�j(gE�iEݧ+�r�4CW�jտ�PV�!�V�7,a�JO�ίA�H�1+��K5��6,��S���Zjް�CJm�;�~(��`g���R���#�:j}�9p%�6�W��hD�o�U�x���ڀ�ՎG�JCm��z��$�����ox��C@m����^?��
���a�R>���C��i�p�R<m����N����?�����崑��%���x�5i�!�Q���cg�?$��!K��x���K��t<Β&�\�㱖�+��X�jJ���<��k���<�ә��CNk�:��!)���*���Ǳ#-/��GO�h}ɼ<�
����ǇD�?d��Q��y|���Cn*�����v)��Cv�si���<�[���دH��=.[X�*C���U�H��& ���c�V�!I�۞ �He�5�2<�IopZ��4���`K'8�`/��~І��_z/�h���h�г����l�n�ބ�U^O���'b���c���,0��M�an�H�6�0���l4]1� Jv�h?f��^SW��$<�bJ<�|P�@X�P2��j��B��a��Bh�)�h�R��Ҵ� �Am��sA�dI}��3$�dI��!�Ɣ�aS�b���d�e��V��g`d�~󁽊!X�5�	Ԭ<�e�&#������-F�+� �  /"�#���7�������i��YAb�b��v�Z�����b�zC���ی4:�x�8W��DK��:k�ЉX�>S�Z<v"*y��r�DT�L5g+۝�Kq���tސ�L���!���>(3����0Ӏ��H]�G���LM�q?T�_��eh|O�C�i��E�I�G��LM>r?���o^�1�*f���ŲЁ�R�i�?�YR�i��m�-��\A�RLN/T~(1��9\
1x墣�a�ƕ;)�4���_R���Do�~�0x�"��`p�!�S��jT_�
L�Q��LN��~�/�q]H�/�)���%)�Ov���z?�KR
􍖽%)�GT����y��,I)S�$KR�lB����������*4dWIJ���GSIJ�D���$叴P����O�Q���Z5�����F�~�t�X�h�N���QMv���qT��$)�d/I�WG��o��=���)�I����kP6�$���q�>�T"zdI*���$�uI*���$�Z��T>�|�A��G����$��7�?^������<@�w���Y�|�?�	A������?��5��z�?���o?����د�����7�X����;�,�x�?�)w-����p���@�?�|�?З�^?V��%��\>����W/�k���K"�l���D��?�u)�8>�u-ҧ�:u1��a����5q�y|�L�z���2���Wˤ�H,O��$y|��d:ֳi�*J�p|�cʪd�UJL�%S��l�u�>��ua2m� ��L��pU�L�>�=em2�T���4�
c��:��Wk�,O�x|������rߩ,P&�D��
e�9ċ*Q��ѭ.k�)��nY�L�VV)S�j�e����������=�       �     x���Kr�JDѱ�9��������U"�ЊP���`�'���ծ�Q���2�l�����2��>l����a�s<��\q�X�+j�]���rW\\Q3W�R���0��+*W��V���7����_O��3����xJ�����X�_�g�����[���7|�����3�;~e���g�9��%������J���,��k5�+}�e<}�g<}md<}��w͛������������������חZz��NKO__i�}}��u��NO__h����,=}}��������}���������y��y>��}�e���=W�|�x÷�����?2����.���O?�+�'~g��_����\z���+?��{.=}}���NO_�s���{.=}mf<}me<}mg<}��x�ƞ����5�{.��o���=W������7�\y�ƞ+O���q����{�<}cϕ������=W~��^����f|����~��x��=�;~d��ό�����3~㯌����|���_�>l���KO_�s��k-��k=��k#������7���ݟ��o��+O�x?W�����_�=W������7�\y�ƞ�������}cϕ�o���=W������7�\y�ƞ�������>���uz�׌�����3��G�����_��Ͼ������:��_�}��o�����'��7�O��o��(O�8?9�����Yz��>KO_�g����,=}}�����}�����s��W�����Yz��>KO_�g����,=}}��������q�y����\��J�7�6��W_�=���k�7|����?��<�c N?�3�~e���W¿ί>�\y�ƞ+O���z�f��o���=W������7�\y�ƞ+O��s��{n���?�A��s��{�<}cϕ�o���=W������7���?�遲�����7�\y�ƞ?�{���o��g�+�f|�G����>}������?3~�W�o���%�����}��WV?��}X��}���������Yz��>KO_�g����,=}}����w�<,��<+N]_g�7q}���o���iV���̊և���� �	��,=a}��'�����a���>��_��a�����-��}�jǐK��5�;�e��g��8�
�y��*|�y�����!��'��W��Ì�+��xf��%��B_K������;}-�w�Z��,��D�Y�k�����2}����k����F����Vݿ<}c͕�o̹��=W������7�\y�ƞ+O������?>�J��s��{�<}cϕ��~1���k�|��������;�_���?3�¯�o�3��W�׷�=W����v����=W������7�\y�ƞ+O��s��{.|�o���9=c����{}~N����s�����#�����{{~���}������o�W�߾��������bϤ�����2���?FϿ����?�3�~e���W�����g���������:=}}Ϥ������=����g����Lz���IO_߳�������o�/���IO_�?���{(=}}������������d+      �   `   x���;�0Cgs�O	ܥ�?G��%�PՓ�l��J��p�H�TJ��@�C	{G1�&Q�" ,!���U(C�j��i��PB��}B��� �.&�q�/     