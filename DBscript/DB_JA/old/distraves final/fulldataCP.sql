PGDMP              
        
    v            planning    10.3    10.3    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            �           1262    28388    planning    DATABASE     �   CREATE DATABASE planning WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Spanish_Colombia.1252' LC_CTYPE = 'Spanish_Colombia.1252';
    DROP DATABASE planning;
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            �           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3                        3079    12924    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            �           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1259    28389    abaTimeUnit_id_seq    SEQUENCE     �   CREATE SEQUENCE public."abaTimeUnit_id_seq"
    START WITH 2
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 +   DROP SEQUENCE public."abaTimeUnit_id_seq";
       public       postgres    false    3            �            1259    28391    aba_breeds_and_stages_id_seq    SEQUENCE     �   CREATE SEQUENCE public.aba_breeds_and_stages_id_seq
    START WITH 8
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 3   DROP SEQUENCE public.aba_breeds_and_stages_id_seq;
       public       postgres    false    3            �            1259    28393    aba_breeds_and_stages    TABLE       CREATE TABLE public.aba_breeds_and_stages (
    id integer DEFAULT nextval('public.aba_breeds_and_stages_id_seq'::regclass) NOT NULL,
    code character varying(100),
    name character varying(100),
    id_aba_consumption_and_mortality integer,
    id_process integer
);
 )   DROP TABLE public.aba_breeds_and_stages;
       public         postgres    false    197    3            �           0    0    TABLE aba_breeds_and_stages    COMMENT     o   COMMENT ON TABLE public.aba_breeds_and_stages IS 'Relaciona los procesos de ARP con el consumo y mortalidad ';
            public       postgres    false    198            �           0    0    COLUMN aba_breeds_and_stages.id    COMMENT     o   COMMENT ON COLUMN public.aba_breeds_and_stages.id IS 'Id de la relacion entre proceso y consumo y mortalidad';
            public       postgres    false    198            �           0    0 !   COLUMN aba_breeds_and_stages.code    COMMENT     u   COMMENT ON COLUMN public.aba_breeds_and_stages.code IS 'Codigo de la relacion entre proceso y consumo y mortalidad';
            public       postgres    false    198            �           0    0 !   COLUMN aba_breeds_and_stages.name    COMMENT     u   COMMENT ON COLUMN public.aba_breeds_and_stages.name IS 'Nombre de la relacion entre proceso y consumo y mortalidad';
            public       postgres    false    198            �           0    0 =   COLUMN aba_breeds_and_stages.id_aba_consumption_and_mortality    COMMENT     �   COMMENT ON COLUMN public.aba_breeds_and_stages.id_aba_consumption_and_mortality IS 'Id de tabla aba_consumption_and_mortality (FK)';
            public       postgres    false    198            �           0    0 '   COLUMN aba_breeds_and_stages.id_process    COMMENT     Y   COMMENT ON COLUMN public.aba_breeds_and_stages.id_process IS 'Id de la tabla mdprocess';
            public       postgres    false    198            �            1259    28397 $   aba_consumption_and_mortality_id_seq    SEQUENCE     �   CREATE SEQUENCE public.aba_consumption_and_mortality_id_seq
    START WITH 8
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 ;   DROP SEQUENCE public.aba_consumption_and_mortality_id_seq;
       public       postgres    false    3            �            1259    28399    aba_consumption_and_mortality    TABLE     $  CREATE TABLE public.aba_consumption_and_mortality (
    id integer DEFAULT nextval('public.aba_consumption_and_mortality_id_seq'::regclass) NOT NULL,
    code character varying(100),
    name character varying(100),
    id_breed integer,
    id_stage integer,
    id_aba_time_unit integer
);
 1   DROP TABLE public.aba_consumption_and_mortality;
       public         postgres    false    199    3            �           0    0 #   TABLE aba_consumption_and_mortality    COMMENT     �   COMMENT ON TABLE public.aba_consumption_and_mortality IS 'Almacena la información del consumo y mortalidad asociados a la combinacion de raza y etapa';
            public       postgres    false    200            �           0    0 '   COLUMN aba_consumption_and_mortality.id    COMMENT     �   COMMENT ON COLUMN public.aba_consumption_and_mortality.id IS 'Id de los datos de consumo y mortalidad asociados a una raza y una etapa';
            public       postgres    false    200            �           0    0 )   COLUMN aba_consumption_and_mortality.code    COMMENT     �   COMMENT ON COLUMN public.aba_consumption_and_mortality.code IS 'Codigo de los datos de consumo y mortalidad asociados a una raza y una etapa ';
            public       postgres    false    200            �           0    0 )   COLUMN aba_consumption_and_mortality.name    COMMENT     �   COMMENT ON COLUMN public.aba_consumption_and_mortality.name IS 'Nombre de los datos de consumo y mortalidad asociados a una raza y una etapa';
            public       postgres    false    200            �           0    0 -   COLUMN aba_consumption_and_mortality.id_breed    COMMENT     �   COMMENT ON COLUMN public.aba_consumption_and_mortality.id_breed IS 'Id de la raza asociada a los datos de consumo y mortalidad';
            public       postgres    false    200            �           0    0 -   COLUMN aba_consumption_and_mortality.id_stage    COMMENT     �   COMMENT ON COLUMN public.aba_consumption_and_mortality.id_stage IS 'id de la etapa en la que se encuentran los datos de consumo y mortalidad ';
            public       postgres    false    200            �           0    0 5   COLUMN aba_consumption_and_mortality.id_aba_time_unit    COMMENT     �   COMMENT ON COLUMN public.aba_consumption_and_mortality.id_aba_time_unit IS 'Id de la unidad de tiempo utilizada en los datos cargados en consumo y mortalidad (dias o semanas)';
            public       postgres    false    200            �            1259    28403 +   aba_consumption_and_mortality_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.aba_consumption_and_mortality_detail_id_seq
    START WITH 203
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 B   DROP SEQUENCE public.aba_consumption_and_mortality_detail_id_seq;
       public       postgres    false    3            �            1259    28405 $   aba_consumption_and_mortality_detail    TABLE     =  CREATE TABLE public.aba_consumption_and_mortality_detail (
    id integer DEFAULT nextval('public.aba_consumption_and_mortality_detail_id_seq'::regclass) NOT NULL,
    id_aba_consumption_and_mortality integer NOT NULL,
    time_unit_number integer,
    consumption double precision,
    mortality double precision
);
 8   DROP TABLE public.aba_consumption_and_mortality_detail;
       public         postgres    false    201    3            �           0    0 *   TABLE aba_consumption_and_mortality_detail    COMMENT     �   COMMENT ON TABLE public.aba_consumption_and_mortality_detail IS 'Almacena los detalles para la unidad de tiempo asociada a una determinada agrupación de consumo y mortalidad ';
            public       postgres    false    202            �           0    0 .   COLUMN aba_consumption_and_mortality_detail.id    COMMENT     �   COMMENT ON COLUMN public.aba_consumption_and_mortality_detail.id IS 'Id de los detalles para la unidad de tiempo asociada a una determinada agrupación de consumo y mortalidad ';
            public       postgres    false    202            �           0    0 L   COLUMN aba_consumption_and_mortality_detail.id_aba_consumption_and_mortality    COMMENT     �   COMMENT ON COLUMN public.aba_consumption_and_mortality_detail.id_aba_consumption_and_mortality IS 'Id de la agrupación de consumo y mortalidad asociada';
            public       postgres    false    202            �           0    0 <   COLUMN aba_consumption_and_mortality_detail.time_unit_number    COMMENT     �   COMMENT ON COLUMN public.aba_consumption_and_mortality_detail.time_unit_number IS 'Indica la unidad de tiempo asociada a la agrupacion de consumo y mortalidad';
            public       postgres    false    202            �           0    0 7   COLUMN aba_consumption_and_mortality_detail.consumption    COMMENT     �   COMMENT ON COLUMN public.aba_consumption_and_mortality_detail.consumption IS 'Consumo asociado a una determinada agrupación de consumo y mortalidad ';
            public       postgres    false    202            �           0    0 5   COLUMN aba_consumption_and_mortality_detail.mortality    COMMENT     �   COMMENT ON COLUMN public.aba_consumption_and_mortality_detail.mortality IS 'Mortalidad asociada a una determinada agrupación de consumo y mortalidad ';
            public       postgres    false    202            �            1259    28409    aba_elements_id_seq    SEQUENCE     �   CREATE SEQUENCE public.aba_elements_id_seq
    START WITH 22
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 *   DROP SEQUENCE public.aba_elements_id_seq;
       public       postgres    false    3            �            1259    28411    aba_elements    TABLE       CREATE TABLE public.aba_elements (
    id integer DEFAULT nextval('public.aba_elements_id_seq'::regclass) NOT NULL,
    code character varying(100),
    name character varying(100),
    id_aba_element_property integer,
    equivalent_quantity double precision
);
     DROP TABLE public.aba_elements;
       public         postgres    false    203    3            �           0    0    TABLE aba_elements    COMMENT     T   COMMENT ON TABLE public.aba_elements IS 'Almacena los datos de los macroelementos';
            public       postgres    false    204            �           0    0    COLUMN aba_elements.id    COMMENT     D   COMMENT ON COLUMN public.aba_elements.id IS 'Id del macroelemento';
            public       postgres    false    204            �           0    0    COLUMN aba_elements.code    COMMENT     J   COMMENT ON COLUMN public.aba_elements.code IS 'Codigo del macroelemento';
            public       postgres    false    204            �           0    0    COLUMN aba_elements.name    COMMENT     J   COMMENT ON COLUMN public.aba_elements.name IS 'Nombre del macroelemento';
            public       postgres    false    204            �           0    0 +   COLUMN aba_elements.id_aba_element_property    COMMENT     q   COMMENT ON COLUMN public.aba_elements.id_aba_element_property IS 'Id de la propiedad asociada al macroelemento';
            public       postgres    false    204            �           0    0 '   COLUMN aba_elements.equivalent_quantity    COMMENT     �   COMMENT ON COLUMN public.aba_elements.equivalent_quantity IS 'Cantidad de la propiedad asociada al macroelemento con el fin de realizar equivalencias';
            public       postgres    false    204            �            1259    28415 &   aba_elements_and_concentrations_id_seq    SEQUENCE     �   CREATE SEQUENCE public.aba_elements_and_concentrations_id_seq
    START WITH 105
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 =   DROP SEQUENCE public.aba_elements_and_concentrations_id_seq;
       public       postgres    false    3            �            1259    28417    aba_elements_and_concentrations    TABLE     k  CREATE TABLE public.aba_elements_and_concentrations (
    id integer DEFAULT nextval('public.aba_elements_and_concentrations_id_seq'::regclass) NOT NULL,
    id_aba_element integer,
    id_aba_formulation integer,
    proportion double precision,
    id_element_equivalent integer,
    id_aba_element_property integer,
    equivalent_quantity double precision
);
 3   DROP TABLE public.aba_elements_and_concentrations;
       public         postgres    false    205    3            �           0    0 %   TABLE aba_elements_and_concentrations    COMMENT     x   COMMENT ON TABLE public.aba_elements_and_concentrations IS 'Asocia una formula con los macroelementos que la componen';
            public       postgres    false    206            �           0    0 )   COLUMN aba_elements_and_concentrations.id    COMMENT     �   COMMENT ON COLUMN public.aba_elements_and_concentrations.id IS 'Id de la asociación entre una formula con los macroelementos que la componen';
            public       postgres    false    206            �           0    0 5   COLUMN aba_elements_and_concentrations.id_aba_element    COMMENT     g   COMMENT ON COLUMN public.aba_elements_and_concentrations.id_aba_element IS 'Id del elemento asociado';
            public       postgres    false    206            �           0    0 9   COLUMN aba_elements_and_concentrations.id_aba_formulation    COMMENT     l   COMMENT ON COLUMN public.aba_elements_and_concentrations.id_aba_formulation IS 'Id de la formula asociado';
            public       postgres    false    206            �           0    0 1   COLUMN aba_elements_and_concentrations.proportion    COMMENT     x   COMMENT ON COLUMN public.aba_elements_and_concentrations.proportion IS 'Proporción del elemento dentro de la formula';
            public       postgres    false    206            �            1259    28421    aba_elements_properties_id_seq    SEQUENCE     �   CREATE SEQUENCE public.aba_elements_properties_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 5   DROP SEQUENCE public.aba_elements_properties_id_seq;
       public       postgres    false    3            �            1259    28423    aba_elements_properties    TABLE     �   CREATE TABLE public.aba_elements_properties (
    id integer DEFAULT nextval('public.aba_elements_properties_id_seq'::regclass) NOT NULL,
    code character varying(100),
    name character varying(100)
);
 +   DROP TABLE public.aba_elements_properties;
       public         postgres    false    207    3            �           0    0    TABLE aba_elements_properties    COMMENT     �   COMMENT ON TABLE public.aba_elements_properties IS 'Almacena las propiedades que pueden llegar a tener los macroelementos para realizar la equivalencia';
            public       postgres    false    208            �           0    0 !   COLUMN aba_elements_properties.id    COMMENT     Z   COMMENT ON COLUMN public.aba_elements_properties.id IS 'Id de la propiedad del elemento';
            public       postgres    false    208            �           0    0 #   COLUMN aba_elements_properties.code    COMMENT     _   COMMENT ON COLUMN public.aba_elements_properties.code IS 'Codigode la propiedad del elemento';
            public       postgres    false    208            �           0    0 #   COLUMN aba_elements_properties.name    COMMENT     `   COMMENT ON COLUMN public.aba_elements_properties.name IS 'Nombre de la propiedad del elemento';
            public       postgres    false    208            �            1259    28427    aba_formulation_id_seq    SEQUENCE     �   CREATE SEQUENCE public.aba_formulation_id_seq
    START WITH 68
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 -   DROP SEQUENCE public.aba_formulation_id_seq;
       public       postgres    false    3            �            1259    28429    aba_formulation    TABLE     �   CREATE TABLE public.aba_formulation (
    id integer DEFAULT nextval('public.aba_formulation_id_seq'::regclass) NOT NULL,
    code character varying(100),
    name character varying(100)
);
 #   DROP TABLE public.aba_formulation;
       public         postgres    false    209    3            �           0    0    TABLE aba_formulation    COMMENT     g   COMMENT ON TABLE public.aba_formulation IS 'Almacena los datos del alimento balanceado para animales';
            public       postgres    false    210            �           0    0    COLUMN aba_formulation.id    COMMENT     [   COMMENT ON COLUMN public.aba_formulation.id IS 'Id del alimento balanceado para animales';
            public       postgres    false    210            �           0    0    COLUMN aba_formulation.code    COMMENT     a   COMMENT ON COLUMN public.aba_formulation.code IS 'Codigo del alimento balanceado para animales';
            public       postgres    false    210            �           0    0    COLUMN aba_formulation.name    COMMENT     a   COMMENT ON COLUMN public.aba_formulation.name IS 'Nombre del alimento balanceado para animales';
            public       postgres    false    210            �            1259    28433    aba_results_id_seq    SEQUENCE     �   CREATE SEQUENCE public.aba_results_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 )   DROP SEQUENCE public.aba_results_id_seq;
       public       postgres    false    3            �            1259    28435    aba_results    TABLE     �   CREATE TABLE public.aba_results (
    id integer DEFAULT nextval('public.aba_results_id_seq'::regclass) NOT NULL,
    id_aba_element integer,
    quantity double precision
);
    DROP TABLE public.aba_results;
       public         postgres    false    211    3            �            1259    28439 &   aba_stages_of_breeds_and_stages_id_seq    SEQUENCE     �   CREATE SEQUENCE public.aba_stages_of_breeds_and_stages_id_seq
    START WITH 24
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 =   DROP SEQUENCE public.aba_stages_of_breeds_and_stages_id_seq;
       public       postgres    false    3            �            1259    28441    aba_stages_of_breeds_and_stages    TABLE       CREATE TABLE public.aba_stages_of_breeds_and_stages (
    id integer DEFAULT nextval('public.aba_stages_of_breeds_and_stages_id_seq'::regclass) NOT NULL,
    id_aba_breeds_and_stages integer,
    id_formulation integer,
    name character varying(100),
    duration integer
);
 3   DROP TABLE public.aba_stages_of_breeds_and_stages;
       public         postgres    false    213    3            �           0    0 %   TABLE aba_stages_of_breeds_and_stages    COMMENT     �   COMMENT ON TABLE public.aba_stages_of_breeds_and_stages IS 'Almacena las fases asociadas a los animales considerados en la tabla de consumo y mortalidad y asocia el alimento a ser proporcionado en dicha fase';
            public       postgres    false    214            �           0    0 )   COLUMN aba_stages_of_breeds_and_stages.id    COMMENT     �   COMMENT ON COLUMN public.aba_stages_of_breeds_and_stages.id IS 'Id de la fase asociadas a los animales considerados en la tabla de consumo y mortalidad ';
            public       postgres    false    214            �           0    0 ?   COLUMN aba_stages_of_breeds_and_stages.id_aba_breeds_and_stages    COMMENT     �   COMMENT ON COLUMN public.aba_stages_of_breeds_and_stages.id_aba_breeds_and_stages IS 'Id de la tabla que almacena la relacion entre proceso y consumo y mortalidad';
            public       postgres    false    214            �           0    0 5   COLUMN aba_stages_of_breeds_and_stages.id_formulation    COMMENT     �   COMMENT ON COLUMN public.aba_stages_of_breeds_and_stages.id_formulation IS 'Id del alimento balanceado para animales asociado a la fase';
            public       postgres    false    214            �           0    0 +   COLUMN aba_stages_of_breeds_and_stages.name    COMMENT     �   COMMENT ON COLUMN public.aba_stages_of_breeds_and_stages.name IS 'Nombre de la fase asociadas a los animales considerados en la tabla de consumo y mortalidad ';
            public       postgres    false    214            �           0    0 /   COLUMN aba_stages_of_breeds_and_stages.duration    COMMENT     �   COMMENT ON COLUMN public.aba_stages_of_breeds_and_stages.duration IS 'Duracion de la fase asociadas a los animales considerados en la tabla de consumo y mortalidad ';
            public       postgres    false    214            �            1259    28445    aba_time_unit    TABLE     �   CREATE TABLE public.aba_time_unit (
    id integer DEFAULT nextval('public."abaTimeUnit_id_seq"'::regclass) NOT NULL,
    singular_name character varying(100),
    plural_name character varying(100)
);
 !   DROP TABLE public.aba_time_unit;
       public         postgres    false    196    3            �           0    0    TABLE aba_time_unit    COMMENT     L   COMMENT ON TABLE public.aba_time_unit IS 'Almacena las unidades de tiempo';
            public       postgres    false    215            �           0    0    COLUMN aba_time_unit.id    COMMENT     K   COMMENT ON COLUMN public.aba_time_unit.id IS 'Id de la unidad de tiempo
';
            public       postgres    false    215            �           0    0 "   COLUMN aba_time_unit.singular_name    COMMENT     e   COMMENT ON COLUMN public.aba_time_unit.singular_name IS 'Nombre en singular de la unidad de tiempo';
            public       postgres    false    215            �           0    0     COLUMN aba_time_unit.plural_name    COMMENT     a   COMMENT ON COLUMN public.aba_time_unit.plural_name IS 'Nombre en plural de la unidad de tiempo';
            public       postgres    false    215            �            1259    28449    availability_shed_id_seq    SEQUENCE     �   CREATE SEQUENCE public.availability_shed_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.availability_shed_id_seq;
       public       postgres    false    3            �            1259    28451    base_day_id_seq    SEQUENCE     x   CREATE SEQUENCE public.base_day_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.base_day_id_seq;
       public       postgres    false    3            �            1259    28453    breed_id_seq    SEQUENCE     u   CREATE SEQUENCE public.breed_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.breed_id_seq;
       public       postgres    false    3            �            1259    28455    broiler_detail_id_seq    SEQUENCE     ~   CREATE SEQUENCE public.broiler_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.broiler_detail_id_seq;
       public       postgres    false    3            �            1259    28457    broiler_id_seq    SEQUENCE     w   CREATE SEQUENCE public.broiler_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.broiler_id_seq;
       public       postgres    false    3            �            1259    28459    broiler_product_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.broiler_product_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public.broiler_product_detail_id_seq;
       public       postgres    false    3            �            1259    28461    broiler_product_id_seq    SEQUENCE        CREATE SEQUENCE public.broiler_product_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.broiler_product_id_seq;
       public       postgres    false    3            �            1259    28463    broilereviction_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.broilereviction_detail_id_seq
    START WITH 124
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public.broilereviction_detail_id_seq;
       public       postgres    false    3            �            1259    28465    broilereviction_id_seq    SEQUENCE     �   CREATE SEQUENCE public.broilereviction_id_seq
    START WITH 70
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.broilereviction_id_seq;
       public       postgres    false    3            �            1259    28467    brooder_id_seq    SEQUENCE     w   CREATE SEQUENCE public.brooder_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.brooder_id_seq;
       public       postgres    false    3            �            1259    28469    brooder_machines_id_seq    SEQUENCE     �   CREATE SEQUENCE public.brooder_machines_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.brooder_machines_id_seq;
       public       postgres    false    3            �            1259    28471    calendar_day_id_seq    SEQUENCE     |   CREATE SEQUENCE public.calendar_day_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.calendar_day_id_seq;
       public       postgres    false    3            �            1259    28473    calendar_id_seq    SEQUENCE     x   CREATE SEQUENCE public.calendar_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.calendar_id_seq;
       public       postgres    false    3            �            1259    28475    center_id_seq    SEQUENCE     v   CREATE SEQUENCE public.center_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.center_id_seq;
       public       postgres    false    3            �            1259    28477    egg_planning_id_seq    SEQUENCE     |   CREATE SEQUENCE public.egg_planning_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.egg_planning_id_seq;
       public       postgres    false    3            �            1259    28479    egg_required_id_seq    SEQUENCE     |   CREATE SEQUENCE public.egg_required_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.egg_required_id_seq;
       public       postgres    false    3            �            1259    28481    eggs_storage_id_seq    SEQUENCE     |   CREATE SEQUENCE public.eggs_storage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.eggs_storage_id_seq;
       public       postgres    false    3            �            1259    28483    farm_id_seq    SEQUENCE     t   CREATE SEQUENCE public.farm_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE public.farm_id_seq;
       public       postgres    false    3            �            1259    28485    farm_type_id_seq    SEQUENCE     y   CREATE SEQUENCE public.farm_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.farm_type_id_seq;
       public       postgres    false    3            �            1259    28487    holiday_id_seq    SEQUENCE     w   CREATE SEQUENCE public.holiday_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.holiday_id_seq;
       public       postgres    false    3            �            1259    28489    housing_way_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.housing_way_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.housing_way_detail_id_seq;
       public       postgres    false    3            �            1259    28491    housing_way_id_seq    SEQUENCE     {   CREATE SEQUENCE public.housing_way_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.housing_way_id_seq;
       public       postgres    false    3            �            1259    28493    incubator_id_seq    SEQUENCE     y   CREATE SEQUENCE public.incubator_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.incubator_id_seq;
       public       postgres    false    3            �            1259    28495    incubator_plant_id_seq    SEQUENCE        CREATE SEQUENCE public.incubator_plant_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.incubator_plant_id_seq;
       public       postgres    false    3            �            1259    28497    industry_id_seq    SEQUENCE     x   CREATE SEQUENCE public.industry_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.industry_id_seq;
       public       postgres    false    3            �            1259    28499    line_id_seq    SEQUENCE     t   CREATE SEQUENCE public.line_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE public.line_id_seq;
       public       postgres    false    3            �            1259    28501    lot_eggs_id_seq    SEQUENCE     x   CREATE SEQUENCE public.lot_eggs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.lot_eggs_id_seq;
       public       postgres    false    3            �            1259    28503    lot_fattening_id_seq    SEQUENCE     }   CREATE SEQUENCE public.lot_fattening_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.lot_fattening_id_seq;
       public       postgres    false    3            �            1259    28505 
   lot_id_seq    SEQUENCE     s   CREATE SEQUENCE public.lot_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 !   DROP SEQUENCE public.lot_id_seq;
       public       postgres    false    3            �            1259    28507    lot_liftbreeding_id_seq    SEQUENCE     �   CREATE SEQUENCE public.lot_liftbreeding_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.lot_liftbreeding_id_seq;
       public       postgres    false    3            �            1259    28509     mdapplication_application_id_seq    SEQUENCE     �   CREATE SEQUENCE public.mdapplication_application_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999999999999
    CACHE 1;
 7   DROP SEQUENCE public.mdapplication_application_id_seq;
       public       postgres    false    3            �            1259    28511    mdapplication    TABLE     �   CREATE TABLE public.mdapplication (
    application_id integer DEFAULT nextval('public.mdapplication_application_id_seq'::regclass) NOT NULL,
    application_name character varying(30) NOT NULL,
    type character varying(20)
);
 !   DROP TABLE public.mdapplication;
       public         postgres    false    246    3            �           0    0    TABLE mdapplication    COMMENT     X   COMMENT ON TABLE public.mdapplication IS 'Almacena la informacion de las aplicaciones';
            public       postgres    false    247            �           0    0 #   COLUMN mdapplication.application_id    COMMENT     P   COMMENT ON COLUMN public.mdapplication.application_id IS 'Id de la aplicació';
            public       postgres    false    247            �           0    0 %   COLUMN mdapplication.application_name    COMMENT     W   COMMENT ON COLUMN public.mdapplication.application_name IS 'Nombre de la aplicación';
            public       postgres    false    247            �           0    0    COLUMN mdapplication.type    COMMENT     W   COMMENT ON COLUMN public.mdapplication.type IS 'A qué tipo pertenece la aplicación';
            public       postgres    false    247            �            1259    28515    mdapplication_rol_id_seq    SEQUENCE     �   CREATE SEQUENCE public.mdapplication_rol_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9999999999999
    CACHE 1;
 /   DROP SEQUENCE public.mdapplication_rol_id_seq;
       public       postgres    false    3            �            1259    28517    mdapplication_rol    TABLE     �   CREATE TABLE public.mdapplication_rol (
    id integer DEFAULT nextval('public.mdapplication_rol_id_seq'::regclass) NOT NULL,
    application_id integer NOT NULL,
    rol_id integer NOT NULL
);
 %   DROP TABLE public.mdapplication_rol;
       public         postgres    false    248    3            �           0    0    TABLE mdapplication_rol    COMMENT     _   COMMENT ON TABLE public.mdapplication_rol IS 'Contiene los id de aplicación y los id de rol';
            public       postgres    false    249            �           0    0    COLUMN mdapplication_rol.id    COMMENT     \   COMMENT ON COLUMN public.mdapplication_rol.id IS 'Id la combinacion de aplicación y rol ';
            public       postgres    false    249            �           0    0 '   COLUMN mdapplication_rol.application_id    COMMENT     `   COMMENT ON COLUMN public.mdapplication_rol.application_id IS 'Identificador de la aplicación';
            public       postgres    false    249            �           0    0    COLUMN mdapplication_rol.rol_id    COMMENT     N   COMMENT ON COLUMN public.mdapplication_rol.rol_id IS 'Identificador del rol';
            public       postgres    false    249            �            1259    28521    mdbreed    TABLE     �   CREATE TABLE public.mdbreed (
    breed_id integer DEFAULT nextval('public.breed_id_seq'::regclass) NOT NULL,
    code character varying(20) NOT NULL,
    name character varying(45) NOT NULL
);
    DROP TABLE public.mdbreed;
       public         postgres    false    218    3            �           0    0    TABLE mdbreed    COMMENT     U   COMMENT ON TABLE public.mdbreed IS 'Tabla donde se almacenan las razas de las aves';
            public       postgres    false    250            �           0    0    COLUMN mdbreed.breed_id    COMMENT     >   COMMENT ON COLUMN public.mdbreed.breed_id IS 'Id de la raza';
            public       postgres    false    250            �           0    0    COLUMN mdbreed.code    COMMENT     >   COMMENT ON COLUMN public.mdbreed.code IS 'Codigo de la raza';
            public       postgres    false    250            �           0    0    COLUMN mdbreed.name    COMMENT     >   COMMENT ON COLUMN public.mdbreed.name IS 'Nombre de la Raza';
            public       postgres    false    250            �            1259    28525    mdbroiler_product    TABLE     �   CREATE TABLE public.mdbroiler_product (
    broiler_product_id integer DEFAULT nextval('public.broiler_product_id_seq'::regclass) NOT NULL,
    name character varying(45) NOT NULL,
    days_eviction integer,
    weight double precision
);
 %   DROP TABLE public.mdbroiler_product;
       public         postgres    false    222    3            �           0    0    TABLE mdbroiler_product    COMMENT     w   COMMENT ON TABLE public.mdbroiler_product IS 'Almacena los productos de salida de la etapa de engorda hacia desalojo';
            public       postgres    false    251            �           0    0 +   COLUMN mdbroiler_product.broiler_product_id    COMMENT     ^   COMMENT ON COLUMN public.mdbroiler_product.broiler_product_id IS 'Id de producto de engorde';
            public       postgres    false    251            �           0    0    COLUMN mdbroiler_product.name    COMMENT     T   COMMENT ON COLUMN public.mdbroiler_product.name IS 'Nombre de producto de engorde';
            public       postgres    false    251            �           0    0 &   COLUMN mdbroiler_product.days_eviction    COMMENT     y   COMMENT ON COLUMN public.mdbroiler_product.days_eviction IS 'Días necesarios para el desalojo del producto de engorde';
            public       postgres    false    251            �           0    0    COLUMN mdbroiler_product.weight    COMMENT     b   COMMENT ON COLUMN public.mdbroiler_product.weight IS 'Peso estimado del producto para su salida';
            public       postgres    false    251            �            1259    28529 
   mdfarmtype    TABLE     �   CREATE TABLE public.mdfarmtype (
    farm_type_id integer DEFAULT nextval('public.farm_type_id_seq'::regclass) NOT NULL,
    name character varying(45) NOT NULL
);
    DROP TABLE public.mdfarmtype;
       public         postgres    false    234    3            �           0    0    TABLE mdfarmtype    COMMENT     D   COMMENT ON TABLE public.mdfarmtype IS 'Define los tipos de granja';
            public       postgres    false    252            �           0    0    COLUMN mdfarmtype.farm_type_id    COMMENT     L   COMMENT ON COLUMN public.mdfarmtype.farm_type_id IS 'Id de tipo de granja';
            public       postgres    false    252            �           0    0    COLUMN mdfarmtype.name    COMMENT     O   COMMENT ON COLUMN public.mdfarmtype.name IS 'Nombre de la etapa de la granja';
            public       postgres    false    252            �            1259    28533    measure_id_seq    SEQUENCE     w   CREATE SEQUENCE public.measure_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.measure_id_seq;
       public       postgres    false    3            �            1259    28535 	   mdmeasure    TABLE     $  CREATE TABLE public.mdmeasure (
    measure_id integer DEFAULT nextval('public.measure_id_seq'::regclass) NOT NULL,
    name character varying(10) NOT NULL,
    abbreviation character varying(5) NOT NULL,
    originvalue double precision,
    valuekg double precision,
    is_unit boolean
);
    DROP TABLE public.mdmeasure;
       public         postgres    false    253    3            �           0    0    TABLE mdmeasure    COMMENT     _   COMMENT ON TABLE public.mdmeasure IS 'Almacena las medidas a utilizar en las planificaciones';
            public       postgres    false    254            �           0    0    COLUMN mdmeasure.measure_id    COMMENT     D   COMMENT ON COLUMN public.mdmeasure.measure_id IS 'Id de la medida';
            public       postgres    false    254            �           0    0    COLUMN mdmeasure.name    COMMENT     B   COMMENT ON COLUMN public.mdmeasure.name IS 'Nombre de la medida';
            public       postgres    false    254            �           0    0    COLUMN mdmeasure.abbreviation    COMMENT     O   COMMENT ON COLUMN public.mdmeasure.abbreviation IS 'Abreviatura de la medida';
            public       postgres    false    254            �           0    0    COLUMN mdmeasure.originvalue    COMMENT     Q   COMMENT ON COLUMN public.mdmeasure.originvalue IS 'Valor original de la medida';
            public       postgres    false    254            �           0    0    COLUMN mdmeasure.valuekg    COMMENT     R   COMMENT ON COLUMN public.mdmeasure.valuekg IS 'Valor en Kilogramos de la medida';
            public       postgres    false    254            �            1259    28539    parameter_id_seq    SEQUENCE     y   CREATE SEQUENCE public.parameter_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.parameter_id_seq;
       public       postgres    false    3                        1259    28541    mdparameter    TABLE     '  CREATE TABLE public.mdparameter (
    parameter_id integer DEFAULT nextval('public.parameter_id_seq'::regclass) NOT NULL,
    description character varying(250) NOT NULL,
    type character varying(10),
    measure_id integer,
    process_id integer,
    name character varying(250) NOT NULL
);
    DROP TABLE public.mdparameter;
       public         postgres    false    255    3            �           0    0    TABLE mdparameter    COMMENT     �   COMMENT ON TABLE public.mdparameter IS 'Almacena la definición de los parámetros a utilizar en la planificación regresiva junto a sus respectivas características';
            public       postgres    false    256            �           0    0    COLUMN mdparameter.parameter_id    COMMENT     N   COMMENT ON COLUMN public.mdparameter.parameter_id IS 'Id de los parámetros';
            public       postgres    false    256            �           0    0    COLUMN mdparameter.description    COMMENT     W   COMMENT ON COLUMN public.mdparameter.description IS 'Descripción de los parámetros';
            public       postgres    false    256            �           0    0    COLUMN mdparameter.type    COMMENT     D   COMMENT ON COLUMN public.mdparameter.type IS 'Tipo de parámetros';
            public       postgres    false    256            �           0    0    COLUMN mdparameter.measure_id    COMMENT     F   COMMENT ON COLUMN public.mdparameter.measure_id IS 'Id de la medida';
            public       postgres    false    256            �           0    0    COLUMN mdparameter.process_id    COMMENT     E   COMMENT ON COLUMN public.mdparameter.process_id IS 'Id del proceso';
            public       postgres    false    256            �           0    0    COLUMN mdparameter.name    COMMENT     F   COMMENT ON COLUMN public.mdparameter.name IS 'Nombre del parámetro';
            public       postgres    false    256                       1259    28548    process_id_seq    SEQUENCE     w   CREATE SEQUENCE public.process_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.process_id_seq;
       public       postgres    false    3                       1259    28550 	   mdprocess    TABLE     B  CREATE TABLE public.mdprocess (
    process_id integer DEFAULT nextval('public.process_id_seq'::regclass) NOT NULL,
    process_order integer NOT NULL,
    product_id integer NOT NULL,
    stage_id integer NOT NULL,
    historical_decrease double precision NOT NULL,
    theoretical_decrease double precision NOT NULL,
    historical_weight double precision NOT NULL,
    theoretical_weight double precision NOT NULL,
    historical_duration integer NOT NULL,
    theoretical_duration integer NOT NULL,
    calendar_id integer NOT NULL,
    visible boolean,
    name character varying(250) NOT NULL,
    predecessor_id integer,
    capacity integer NOT NULL,
    breed_id integer NOT NULL,
    gender character varying(30),
    fattening_goal double precision,
    type_posture character varying(30),
    biological_active boolean
);
    DROP TABLE public.mdprocess;
       public         postgres    false    257    3            �           0    0    TABLE mdprocess    COMMENT     �   COMMENT ON TABLE public.mdprocess IS 'Almacena los procesos definidos para la planificación progresiva junto a sus respectivas características';
            public       postgres    false    258            �           0    0    COLUMN mdprocess.process_id    COMMENT     G   COMMENT ON COLUMN public.mdprocess.process_id IS 'Id de los procesos';
            public       postgres    false    258            �           0    0    COLUMN mdprocess.process_order    COMMENT     M   COMMENT ON COLUMN public.mdprocess.process_order IS 'Orden de los procesos';
            public       postgres    false    258            �           0    0    COLUMN mdprocess.product_id    COMMENT     D   COMMENT ON COLUMN public.mdprocess.product_id IS 'Id del producto';
            public       postgres    false    258            �           0    0    COLUMN mdprocess.stage_id    COMMENT     >   COMMENT ON COLUMN public.mdprocess.stage_id IS 'Id de etapa';
            public       postgres    false    258                        0    0 $   COLUMN mdprocess.historical_decrease    COMMENT     Y   COMMENT ON COLUMN public.mdprocess.historical_decrease IS 'Merma historica del proceso';
            public       postgres    false    258                       0    0 %   COLUMN mdprocess.theoretical_decrease    COMMENT     Y   COMMENT ON COLUMN public.mdprocess.theoretical_decrease IS 'Merma teórica del proceso';
            public       postgres    false    258                       0    0 "   COLUMN mdprocess.historical_weight    COMMENT     V   COMMENT ON COLUMN public.mdprocess.historical_weight IS 'Peso historico del proceso';
            public       postgres    false    258                       0    0 #   COLUMN mdprocess.theoretical_weight    COMMENT     V   COMMENT ON COLUMN public.mdprocess.theoretical_weight IS 'Peso teórico del proceso';
            public       postgres    false    258                       0    0 $   COLUMN mdprocess.historical_duration    COMMENT     ^   COMMENT ON COLUMN public.mdprocess.historical_duration IS 'Duración histórica del proceso';
            public       postgres    false    258                       0    0 %   COLUMN mdprocess.theoretical_duration    COMMENT     ]   COMMENT ON COLUMN public.mdprocess.theoretical_duration IS 'Duración teórica del proceso';
            public       postgres    false    258                       0    0    COLUMN mdprocess.calendar_id    COMMENT     G   COMMENT ON COLUMN public.mdprocess.calendar_id IS 'Id del calendario';
            public       postgres    false    258                       0    0    COLUMN mdprocess.visible    COMMENT     I   COMMENT ON COLUMN public.mdprocess.visible IS 'Visibilidad del proceso';
            public       postgres    false    258                       0    0    COLUMN mdprocess.name    COMMENT     A   COMMENT ON COLUMN public.mdprocess.name IS 'Nombre del proceso';
            public       postgres    false    258            	           0    0    COLUMN mdprocess.predecessor_id    COMMENT     J   COMMENT ON COLUMN public.mdprocess.predecessor_id IS 'Id del predecesor';
            public       postgres    false    258            
           0    0    COLUMN mdprocess.capacity    COMMENT     X   COMMENT ON COLUMN public.mdprocess.capacity IS 'Capacidad semanal asociada al proceso';
            public       postgres    false    258                       0    0    COLUMN mdprocess.breed_id    COMMENT     @   COMMENT ON COLUMN public.mdprocess.breed_id IS 'Id de la raza';
            public       postgres    false    258                       0    0    COLUMN mdprocess.gender    COMMENT     N   COMMENT ON COLUMN public.mdprocess.gender IS 'Genero del producto de salida';
            public       postgres    false    258                       0    0    COLUMN mdprocess.fattening_goal    COMMENT     H   COMMENT ON COLUMN public.mdprocess.fattening_goal IS 'Meta de engorde';
            public       postgres    false    258                       0    0    COLUMN mdprocess.type_posture    COMMENT     s   COMMENT ON COLUMN public.mdprocess.type_posture IS 'Define el tipo de postura de acuerdo a la edad de la gallina';
            public       postgres    false    258                       0    0 "   COLUMN mdprocess.biological_active    COMMENT     h   COMMENT ON COLUMN public.mdprocess.biological_active IS 'Define si el proceso es un activo biológico';
            public       postgres    false    258                       1259    28554    product_id_seq    SEQUENCE     w   CREATE SEQUENCE public.product_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.product_id_seq;
       public       postgres    false    3                       1259    28556 	   mdproduct    TABLE     �   CREATE TABLE public.mdproduct (
    product_id integer DEFAULT nextval('public.product_id_seq'::regclass) NOT NULL,
    code character varying(20) NOT NULL,
    name character varying(45) NOT NULL
);
    DROP TABLE public.mdproduct;
       public         postgres    false    259    3                       0    0    TABLE mdproduct    COMMENT     Z   COMMENT ON TABLE public.mdproduct IS 'Almacena los productos utilizados en los procesos';
            public       postgres    false    260                       0    0    COLUMN mdproduct.product_id    COMMENT     D   COMMENT ON COLUMN public.mdproduct.product_id IS 'Id del producto';
            public       postgres    false    260                       0    0    COLUMN mdproduct.code    COMMENT     B   COMMENT ON COLUMN public.mdproduct.code IS 'Codigo del producto';
            public       postgres    false    260                       0    0    COLUMN mdproduct.name    COMMENT     B   COMMENT ON COLUMN public.mdproduct.name IS 'Nombre del producto';
            public       postgres    false    260                       1259    28560    mdrol_rol_id_seq    SEQUENCE        CREATE SEQUENCE public.mdrol_rol_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 10000000
    CACHE 1;
 '   DROP SEQUENCE public.mdrol_rol_id_seq;
       public       postgres    false    3                       1259    28562    mdrol    TABLE     	  CREATE TABLE public.mdrol (
    rol_id integer DEFAULT nextval('public.mdrol_rol_id_seq'::regclass) NOT NULL,
    rol_name character varying(80) NOT NULL,
    admin_user_creator character varying(80) NOT NULL,
    creation_date timestamp with time zone NOT NULL
);
    DROP TABLE public.mdrol;
       public         postgres    false    261    3                       0    0    TABLE mdrol    COMMENT     O   COMMENT ON TABLE public.mdrol IS 'Almacena los datos de los diferentes roles';
            public       postgres    false    262                       0    0    COLUMN mdrol.rol_id    COMMENT     7   COMMENT ON COLUMN public.mdrol.rol_id IS 'Id del rol';
            public       postgres    false    262                       0    0    COLUMN mdrol.rol_name    COMMENT     =   COMMENT ON COLUMN public.mdrol.rol_name IS 'Nombre del rol';
            public       postgres    false    262                       0    0    COLUMN mdrol.admin_user_creator    COMMENT     [   COMMENT ON COLUMN public.mdrol.admin_user_creator IS 'Especifica que usuario creo el rol';
            public       postgres    false    262                       0    0    COLUMN mdrol.creation_date    COMMENT     N   COMMENT ON COLUMN public.mdrol.creation_date IS 'Fecha de creación del rol';
            public       postgres    false    262                       1259    28566    scenario_id_seq    SEQUENCE     x   CREATE SEQUENCE public.scenario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.scenario_id_seq;
       public       postgres    false    3                       1259    28568 
   mdscenario    TABLE     d  CREATE TABLE public.mdscenario (
    scenario_id integer DEFAULT nextval('public.scenario_id_seq'::regclass) NOT NULL,
    description character varying(250) NOT NULL,
    date_start timestamp with time zone,
    date_end timestamp with time zone,
    name character varying(250) NOT NULL,
    status integer DEFAULT 0,
    calendar_id integer NOT NULL
);
    DROP TABLE public.mdscenario;
       public         postgres    false    263    3                       0    0    TABLE mdscenario    COMMENT     [   COMMENT ON TABLE public.mdscenario IS 'Almacena información de los distintos escenarios';
            public       postgres    false    264                       0    0    COLUMN mdscenario.scenario_id    COMMENT     G   COMMENT ON COLUMN public.mdscenario.scenario_id IS 'Id del escenario';
            public       postgres    false    264                       0    0    COLUMN mdscenario.description    COMMENT     P   COMMENT ON COLUMN public.mdscenario.description IS 'Descripcion del escenario';
            public       postgres    false    264                       0    0    COLUMN mdscenario.date_start    COMMENT     S   COMMENT ON COLUMN public.mdscenario.date_start IS 'Fecha de inicio del escenario';
            public       postgres    false    264                       0    0    COLUMN mdscenario.date_end    COMMENT     N   COMMENT ON COLUMN public.mdscenario.date_end IS 'Fecha de fin del escenario';
            public       postgres    false    264                       0    0    COLUMN mdscenario.name    COMMENT     D   COMMENT ON COLUMN public.mdscenario.name IS 'Nombre del escenario';
            public       postgres    false    264                       0    0    COLUMN mdscenario.status    COMMENT     F   COMMENT ON COLUMN public.mdscenario.status IS 'Estado del escenario';
            public       postgres    false    264            	           1259    28576    status_shed_id_seq    SEQUENCE     {   CREATE SEQUENCE public.status_shed_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.status_shed_id_seq;
       public       postgres    false    3            
           1259    28578    mdshedstatus    TABLE     �   CREATE TABLE public.mdshedstatus (
    shed_status_id integer DEFAULT nextval('public.status_shed_id_seq'::regclass) NOT NULL,
    name character varying(50) NOT NULL,
    description character varying(250) NOT NULL
);
     DROP TABLE public.mdshedstatus;
       public         postgres    false    265    3                        0    0    TABLE mdshedstatus    COMMENT     b   COMMENT ON TABLE public.mdshedstatus IS 'Almaceno los estatus de disponibilidad de los galpones';
            public       postgres    false    266            !           0    0 "   COLUMN mdshedstatus.shed_status_id    COMMENT     T   COMMENT ON COLUMN public.mdshedstatus.shed_status_id IS 'Id del estado del galpon';
            public       postgres    false    266            "           0    0    COLUMN mdshedstatus.name    COMMENT     a   COMMENT ON COLUMN public.mdshedstatus.name IS 'Nombre del estado en que se encuentra el galpon';
            public       postgres    false    266            #           0    0    COLUMN mdshedstatus.description    COMMENT     [   COMMENT ON COLUMN public.mdshedstatus.description IS 'Descripcion del estado del galpon
';
            public       postgres    false    266                       1259    28582    stage_id_seq    SEQUENCE     u   CREATE SEQUENCE public.stage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.stage_id_seq;
       public       postgres    false    3                       1259    28584    mdstage    TABLE     �   CREATE TABLE public.mdstage (
    stage_id integer DEFAULT nextval('public.stage_id_seq'::regclass) NOT NULL,
    order_ integer,
    name character varying(250) NOT NULL
);
    DROP TABLE public.mdstage;
       public         postgres    false    267    3            $           0    0    TABLE mdstage    COMMENT     d   COMMENT ON TABLE public.mdstage IS 'Almacena las etapas a utilizar en el proceso de planificacion';
            public       postgres    false    268            %           0    0    COLUMN mdstage.stage_id    COMMENT     ?   COMMENT ON COLUMN public.mdstage.stage_id IS 'Id de la etapa';
            public       postgres    false    268            &           0    0    COLUMN mdstage.order_    COMMENT     U   COMMENT ON COLUMN public.mdstage.order_ IS 'Orden en el que se muestras las etapas';
            public       postgres    false    268            '           0    0    COLUMN mdstage.name    COMMENT     ?   COMMENT ON COLUMN public.mdstage.name IS 'Nombre de la etapa';
            public       postgres    false    268                       1259    28588    mduser_user_id_seq    SEQUENCE     �   CREATE SEQUENCE public.mduser_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9999999999
    CACHE 1;
 )   DROP SEQUENCE public.mduser_user_id_seq;
       public       postgres    false    3                       1259    28590    mduser    TABLE     �  CREATE TABLE public.mduser (
    user_id integer DEFAULT nextval('public.mduser_user_id_seq'::regclass) NOT NULL,
    username character varying(80) NOT NULL,
    password character varying(80) NOT NULL,
    name character varying(80) NOT NULL,
    lastname character varying(80) NOT NULL,
    active boolean NOT NULL,
    admi_user_creator character varying(80) NOT NULL,
    rol_id integer NOT NULL,
    creation_date timestamp with time zone NOT NULL
);
    DROP TABLE public.mduser;
       public         postgres    false    269    3                       1259    28594    oscenter    TABLE       CREATE TABLE public.oscenter (
    center_id integer DEFAULT nextval('public.center_id_seq'::regclass) NOT NULL,
    partnership_id integer NOT NULL,
    farm_id integer NOT NULL,
    name character varying(45) NOT NULL,
    code character varying(20) NOT NULL,
    "order" integer
);
    DROP TABLE public.oscenter;
       public         postgres    false    229    3            (           0    0    TABLE oscenter    COMMENT     S   COMMENT ON TABLE public.oscenter IS 'Almacena los datos referentes a los nucleos';
            public       postgres    false    271            )           0    0    COLUMN oscenter.center_id    COMMENT     @   COMMENT ON COLUMN public.oscenter.center_id IS 'Id del nucleo';
            public       postgres    false    271            *           0    0    COLUMN oscenter.partnership_id    COMMENT     H   COMMENT ON COLUMN public.oscenter.partnership_id IS 'Id de la empresa';
            public       postgres    false    271            +           0    0    COLUMN oscenter.farm_id    COMMENT     @   COMMENT ON COLUMN public.oscenter.farm_id IS 'Id de la granja';
            public       postgres    false    271            ,           0    0    COLUMN oscenter.name    COMMENT     @   COMMENT ON COLUMN public.oscenter.name IS 'Nombre del nucleo
';
            public       postgres    false    271            -           0    0    COLUMN oscenter.code    COMMENT     ?   COMMENT ON COLUMN public.oscenter.code IS 'Codigo del nucleo';
            public       postgres    false    271                       1259    28598    oscenter_oswarehouse    TABLE     �   CREATE TABLE public.oscenter_oswarehouse (
    client_id integer NOT NULL,
    partnership_id integer NOT NULL,
    farm_id integer NOT NULL,
    center_id integer NOT NULL,
    warehouse_id integer NOT NULL,
    delete_mark integer
);
 (   DROP TABLE public.oscenter_oswarehouse;
       public         postgres    false    3            .           0    0    TABLE oscenter_oswarehouse    COMMENT     p   COMMENT ON TABLE public.oscenter_oswarehouse IS 'Relación que une los núcleos con sus respectivos almacenes';
            public       postgres    false    272            /           0    0 %   COLUMN oscenter_oswarehouse.client_id    COMMENT     M   COMMENT ON COLUMN public.oscenter_oswarehouse.client_id IS 'Id del cliente';
            public       postgres    false    272            0           0    0 *   COLUMN oscenter_oswarehouse.partnership_id    COMMENT     T   COMMENT ON COLUMN public.oscenter_oswarehouse.partnership_id IS 'Id de la empresa';
            public       postgres    false    272            1           0    0 #   COLUMN oscenter_oswarehouse.farm_id    COMMENT     L   COMMENT ON COLUMN public.oscenter_oswarehouse.farm_id IS 'Id de la granja';
            public       postgres    false    272            2           0    0 %   COLUMN oscenter_oswarehouse.center_id    COMMENT     L   COMMENT ON COLUMN public.oscenter_oswarehouse.center_id IS 'Id del nucleo';
            public       postgres    false    272            3           0    0 (   COLUMN oscenter_oswarehouse.warehouse_id    COMMENT     P   COMMENT ON COLUMN public.oscenter_oswarehouse.warehouse_id IS 'Id del almacen';
            public       postgres    false    272            4           0    0 '   COLUMN oscenter_oswarehouse.delete_mark    COMMENT     Q   COMMENT ON COLUMN public.oscenter_oswarehouse.delete_mark IS 'Marca de borrado';
            public       postgres    false    272                       1259    28601    osfarm    TABLE       CREATE TABLE public.osfarm (
    farm_id integer DEFAULT nextval('public.farm_id_seq'::regclass) NOT NULL,
    partnership_id integer,
    code character varying(20) NOT NULL,
    name character varying(45) NOT NULL,
    farm_type_id integer NOT NULL,
    "order" integer
);
    DROP TABLE public.osfarm;
       public         postgres    false    233    3            5           0    0    TABLE osfarm    COMMENT     p   COMMENT ON TABLE public.osfarm IS 'Almacena la información de la granja con sus respectivas características';
            public       postgres    false    273            6           0    0    COLUMN osfarm.farm_id    COMMENT     >   COMMENT ON COLUMN public.osfarm.farm_id IS 'Id de la granja';
            public       postgres    false    273            7           0    0    COLUMN osfarm.partnership_id    COMMENT     F   COMMENT ON COLUMN public.osfarm.partnership_id IS 'Id de la empresa';
            public       postgres    false    273            8           0    0    COLUMN osfarm.code    COMMENT     ?   COMMENT ON COLUMN public.osfarm.code IS 'Codigo de la granja';
            public       postgres    false    273            9           0    0    COLUMN osfarm.name    COMMENT     ?   COMMENT ON COLUMN public.osfarm.name IS 'Nombre de la granja';
            public       postgres    false    273            :           0    0    COLUMN osfarm.farm_type_id    COMMENT     I   COMMENT ON COLUMN public.osfarm.farm_type_id IS 'Id del tipo de granja';
            public       postgres    false    273                       1259    28605    osincubator    TABLE     �  CREATE TABLE public.osincubator (
    incubator_id integer DEFAULT nextval('public.incubator_id_seq'::regclass) NOT NULL,
    incubator_plant_id integer,
    name character varying(45) NOT NULL,
    code character varying(20) NOT NULL,
    description character varying(250) NOT NULL,
    capacity integer,
    sunday integer,
    monday integer,
    tuesday integer,
    wednesday integer,
    thursday integer,
    friday integer,
    saturday integer,
    available integer
);
    DROP TABLE public.osincubator;
       public         postgres    false    238    3            ;           0    0    TABLE osincubator    COMMENT     y   COMMENT ON TABLE public.osincubator IS 'Almacena las máquinas de incubación pertenecientes a cada una de las plantas';
            public       postgres    false    274            <           0    0    COLUMN osincubator.incubator_id    COMMENT     L   COMMENT ON COLUMN public.osincubator.incubator_id IS 'Id de la incubadora';
            public       postgres    false    274            =           0    0 %   COLUMN osincubator.incubator_plant_id    COMMENT     Y   COMMENT ON COLUMN public.osincubator.incubator_plant_id IS 'Id de la planta incubadora';
            public       postgres    false    274            >           0    0    COLUMN osincubator.name    COMMENT     H   COMMENT ON COLUMN public.osincubator.name IS 'Nombre de la incubadora';
            public       postgres    false    274            ?           0    0    COLUMN osincubator.code    COMMENT     H   COMMENT ON COLUMN public.osincubator.code IS 'Codigo de la incubadora';
            public       postgres    false    274            @           0    0    COLUMN osincubator.description    COMMENT     T   COMMENT ON COLUMN public.osincubator.description IS 'Descripcion de la incubadora';
            public       postgres    false    274            A           0    0    COLUMN osincubator.capacity    COMMENT     O   COMMENT ON COLUMN public.osincubator.capacity IS 'Capacidad de la incubadora';
            public       postgres    false    274            B           0    0    COLUMN osincubator.sunday    COMMENT     ]   COMMENT ON COLUMN public.osincubator.sunday IS 'Marca los dias de trabajo de la incubadora';
            public       postgres    false    274            C           0    0    COLUMN osincubator.monday    COMMENT     ^   COMMENT ON COLUMN public.osincubator.monday IS 'Marca los días de trabajo de la incubadora';
            public       postgres    false    274            D           0    0    COLUMN osincubator.tuesday    COMMENT     _   COMMENT ON COLUMN public.osincubator.tuesday IS 'Marca los días de trabajo de la incubadora';
            public       postgres    false    274            E           0    0    COLUMN osincubator.wednesday    COMMENT     a   COMMENT ON COLUMN public.osincubator.wednesday IS 'Marca los días de trabajo de la incubadora';
            public       postgres    false    274            F           0    0    COLUMN osincubator.thursday    COMMENT     `   COMMENT ON COLUMN public.osincubator.thursday IS 'Marca los días de trabajo de la incubadora';
            public       postgres    false    274            G           0    0    COLUMN osincubator.friday    COMMENT     ^   COMMENT ON COLUMN public.osincubator.friday IS 'Marca los días de trabajo de la incubadora';
            public       postgres    false    274            H           0    0    COLUMN osincubator.saturday    COMMENT     `   COMMENT ON COLUMN public.osincubator.saturday IS 'Marca los días de trabajo de la incubadora';
            public       postgres    false    274                       1259    28609    osincubatorplant    TABLE     �  CREATE TABLE public.osincubatorplant (
    incubator_plant_id integer DEFAULT nextval('public.incubator_plant_id_seq'::regclass) NOT NULL,
    name character varying(45) NOT NULL,
    code character varying(20) NOT NULL,
    description character varying(250),
    partnership_id integer,
    max_storage integer,
    min_storage integer,
    acclimatized boolean,
    suitable boolean,
    expired boolean
);
 $   DROP TABLE public.osincubatorplant;
       public         postgres    false    239    3            I           0    0    TABLE osincubatorplant    COMMENT     }   COMMENT ON TABLE public.osincubatorplant IS 'Almacena la información de la planta incubadora perteneciente a cada empresa';
            public       postgres    false    275            J           0    0 *   COLUMN osincubatorplant.incubator_plant_id    COMMENT     ^   COMMENT ON COLUMN public.osincubatorplant.incubator_plant_id IS 'Id de la planta incubadora';
            public       postgres    false    275            K           0    0    COLUMN osincubatorplant.name    COMMENT     T   COMMENT ON COLUMN public.osincubatorplant.name IS 'Nombre de la planta incubadora';
            public       postgres    false    275            L           0    0    COLUMN osincubatorplant.code    COMMENT     T   COMMENT ON COLUMN public.osincubatorplant.code IS 'Codigo de la planta incubadora';
            public       postgres    false    275            M           0    0 #   COLUMN osincubatorplant.description    COMMENT     a   COMMENT ON COLUMN public.osincubatorplant.description IS 'Descripción de la planta incubadora';
            public       postgres    false    275            N           0    0 &   COLUMN osincubatorplant.partnership_id    COMMENT     P   COMMENT ON COLUMN public.osincubatorplant.partnership_id IS 'Id de la empresa';
            public       postgres    false    275            O           0    0 #   COLUMN osincubatorplant.max_storage    COMMENT     ]   COMMENT ON COLUMN public.osincubatorplant.max_storage IS 'Numero máximo de almacenamiento';
            public       postgres    false    275            P           0    0 #   COLUMN osincubatorplant.min_storage    COMMENT     \   COMMENT ON COLUMN public.osincubatorplant.min_storage IS 'Numero minimo de almacenamiento';
            public       postgres    false    275                       1259    28613    partnership_id_seq    SEQUENCE     {   CREATE SEQUENCE public.partnership_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.partnership_id_seq;
       public       postgres    false    3                       1259    28615    ospartnership    TABLE     2  CREATE TABLE public.ospartnership (
    partnership_id integer DEFAULT nextval('public.partnership_id_seq'::regclass) NOT NULL,
    name character varying(45) NOT NULL,
    address character varying(250) NOT NULL,
    description character varying(250) NOT NULL,
    code character varying(20) NOT NULL
);
 !   DROP TABLE public.ospartnership;
       public         postgres    false    276    3            Q           0    0    TABLE ospartnership    COMMENT     j   COMMENT ON TABLE public.ospartnership IS 'Almacena la información referente a las empresas registradas';
            public       postgres    false    277            R           0    0 #   COLUMN ospartnership.partnership_id    COMMENT     M   COMMENT ON COLUMN public.ospartnership.partnership_id IS 'Id de la empresa';
            public       postgres    false    277            S           0    0    COLUMN ospartnership.name    COMMENT     G   COMMENT ON COLUMN public.ospartnership.name IS 'Nombre de la empresa';
            public       postgres    false    277            T           0    0    COLUMN ospartnership.address    COMMENT     M   COMMENT ON COLUMN public.ospartnership.address IS 'Direccion de la empresa';
            public       postgres    false    277            U           0    0     COLUMN ospartnership.description    COMMENT     T   COMMENT ON COLUMN public.ospartnership.description IS 'Descripción de la empresa';
            public       postgres    false    277            V           0    0    COLUMN ospartnership.code    COMMENT     G   COMMENT ON COLUMN public.ospartnership.code IS 'Codigo de la empresa';
            public       postgres    false    277                       1259    28622    shed_id_seq    SEQUENCE     t   CREATE SEQUENCE public.shed_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE public.shed_id_seq;
       public       postgres    false    3                       1259    28624    osshed    TABLE       CREATE TABLE public.osshed (
    shed_id integer DEFAULT nextval('public.shed_id_seq'::regclass) NOT NULL,
    partnership_id integer NOT NULL,
    farm_id integer NOT NULL,
    center_id integer NOT NULL,
    code character varying(20) NOT NULL,
    statusshed_id integer NOT NULL,
    type_id integer,
    building_date date,
    stall_width double precision NOT NULL,
    stall_height double precision NOT NULL,
    capacity_min double precision NOT NULL,
    capacity_max double precision NOT NULL,
    environment_id integer,
    rotation_days integer DEFAULT 0 NOT NULL,
    nests_quantity integer DEFAULT 0,
    cages_quantity integer DEFAULT 0,
    birds_quantity integer DEFAULT 0,
    capacity_theoretical integer DEFAULT 0,
    avaliable_date date,
    "order" integer
);
    DROP TABLE public.osshed;
       public         postgres    false    278    3            W           0    0    TABLE osshed    COMMENT     d   COMMENT ON TABLE public.osshed IS 'Almacena la informacion de los galpones asociados a la empresa';
            public       postgres    false    279            X           0    0    COLUMN osshed.shed_id    COMMENT     <   COMMENT ON COLUMN public.osshed.shed_id IS 'Id del galpon';
            public       postgres    false    279            Y           0    0    COLUMN osshed.partnership_id    COMMENT     F   COMMENT ON COLUMN public.osshed.partnership_id IS 'Id de la empresa';
            public       postgres    false    279            Z           0    0    COLUMN osshed.farm_id    COMMENT     >   COMMENT ON COLUMN public.osshed.farm_id IS 'Id de la granja';
            public       postgres    false    279            [           0    0    COLUMN osshed.center_id    COMMENT     >   COMMENT ON COLUMN public.osshed.center_id IS 'Id del nucleo';
            public       postgres    false    279            \           0    0    COLUMN osshed.code    COMMENT     =   COMMENT ON COLUMN public.osshed.code IS 'Codigo del galpon';
            public       postgres    false    279            ]           0    0    COLUMN osshed.statusshed_id    COMMENT     _   COMMENT ON COLUMN public.osshed.statusshed_id IS 'Identificador del estado actual del galpon';
            public       postgres    false    279            ^           0    0    COLUMN osshed.type_id    COMMENT     D   COMMENT ON COLUMN public.osshed.type_id IS 'Id del tipo de galpon';
            public       postgres    false    279            _           0    0    COLUMN osshed.building_date    COMMENT     c   COMMENT ON COLUMN public.osshed.building_date IS 'Almacena la fecha de construccion del edificio';
            public       postgres    false    279            `           0    0    COLUMN osshed.stall_width    COMMENT     M   COMMENT ON COLUMN public.osshed.stall_width IS 'Indica el ancho del galpon';
            public       postgres    false    279            a           0    0    COLUMN osshed.stall_height    COMMENT     M   COMMENT ON COLUMN public.osshed.stall_height IS 'Indica el alto del galpon';
            public       postgres    false    279            b           0    0    COLUMN osshed.capacity_min    COMMENT     D   COMMENT ON COLUMN public.osshed.capacity_min IS 'Capacidad minima';
            public       postgres    false    279            c           0    0    COLUMN osshed.capacity_max    COMMENT     F   COMMENT ON COLUMN public.osshed.capacity_max IS 'Capacidad máxima ';
            public       postgres    false    279            d           0    0    COLUMN osshed.environment_id    COMMENT     E   COMMENT ON COLUMN public.osshed.environment_id IS 'Id del ambiente';
            public       postgres    false    279            e           0    0    COLUMN osshed.rotation_days    COMMENT     H   COMMENT ON COLUMN public.osshed.rotation_days IS 'Días de rotación
';
            public       postgres    false    279            f           0    0    COLUMN osshed.nests_quantity    COMMENT     I   COMMENT ON COLUMN public.osshed.nests_quantity IS 'Cantidad de nidales';
            public       postgres    false    279            g           0    0    COLUMN osshed.cages_quantity    COMMENT     H   COMMENT ON COLUMN public.osshed.cages_quantity IS 'Cantidad de jaulas';
            public       postgres    false    279            h           0    0    COLUMN osshed.birds_quantity    COMMENT     F   COMMENT ON COLUMN public.osshed.birds_quantity IS 'Cantidad de aves';
            public       postgres    false    279            i           0    0 "   COLUMN osshed.capacity_theoretical    COMMENT     O   COMMENT ON COLUMN public.osshed.capacity_theoretical IS '	Capacidad teórica';
            public       postgres    false    279                       1259    28633    silo_id_seq    SEQUENCE     t   CREATE SEQUENCE public.silo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE public.silo_id_seq;
       public       postgres    false    3                       1259    28635    ossilo    TABLE     �  CREATE TABLE public.ossilo (
    silo_id integer DEFAULT nextval('public.silo_id_seq'::regclass) NOT NULL,
    client_id integer,
    partnership_id integer NOT NULL,
    farm_id integer NOT NULL,
    center_id integer NOT NULL,
    name character varying(45) NOT NULL,
    rings_height double precision,
    rings_height_id integer,
    height double precision NOT NULL,
    height_unit_id integer,
    diameter double precision NOT NULL,
    diameter_unit_id integer,
    total_rings_quantity integer,
    measuring_mechanism integer,
    cone_degrees double precision,
    total_capacity_1 double precision,
    total_capacity_2 double precision,
    capacity_unit_id_1 integer,
    capacity_unit_id_2 integer,
    central character varying(20)
);
    DROP TABLE public.ossilo;
       public         postgres    false    280    3            j           0    0    TABLE ossilo    COMMENT     E   COMMENT ON TABLE public.ossilo IS 'Almacena los datos de los silos';
            public       postgres    false    281            k           0    0    COLUMN ossilo.silo_id    COMMENT     :   COMMENT ON COLUMN public.ossilo.silo_id IS 'Id del silo';
            public       postgres    false    281            l           0    0    COLUMN ossilo.client_id    COMMENT     ?   COMMENT ON COLUMN public.ossilo.client_id IS 'Id del cliente';
            public       postgres    false    281            m           0    0    COLUMN ossilo.partnership_id    COMMENT     F   COMMENT ON COLUMN public.ossilo.partnership_id IS 'Id de la empresa';
            public       postgres    false    281            n           0    0    COLUMN ossilo.farm_id    COMMENT     >   COMMENT ON COLUMN public.ossilo.farm_id IS 'Id de la granja';
            public       postgres    false    281            o           0    0    COLUMN ossilo.center_id    COMMENT     >   COMMENT ON COLUMN public.ossilo.center_id IS 'Id del nucleo';
            public       postgres    false    281            p           0    0    COLUMN ossilo.name    COMMENT     ;   COMMENT ON COLUMN public.ossilo.name IS 'Nombre del silo';
            public       postgres    false    281            q           0    0    COLUMN ossilo.rings_height    COMMENT     E   COMMENT ON COLUMN public.ossilo.rings_height IS 'Numero de anillos';
            public       postgres    false    281            r           0    0    COLUMN ossilo.rings_height_id    COMMENT     R   COMMENT ON COLUMN public.ossilo.rings_height_id IS 'Unidad de medida del anillo';
            public       postgres    false    281            s           0    0    COLUMN ossilo.height    COMMENT     =   COMMENT ON COLUMN public.ossilo.height IS 'Altura del silo';
            public       postgres    false    281            t           0    0    COLUMN ossilo.height_unit_id    COMMENT     R   COMMENT ON COLUMN public.ossilo.height_unit_id IS 'Unidad de media de la altura';
            public       postgres    false    281            u           0    0    COLUMN ossilo.diameter    COMMENT     A   COMMENT ON COLUMN public.ossilo.diameter IS 'Diametro del silo';
            public       postgres    false    281            v           0    0    COLUMN ossilo.diameter_unit_id    COMMENT     T   COMMENT ON COLUMN public.ossilo.diameter_unit_id IS 'Unidad de media del diametro';
            public       postgres    false    281            w           0    0 "   COLUMN ossilo.total_rings_quantity    COMMENT     U   COMMENT ON COLUMN public.ossilo.total_rings_quantity IS 'Total de anillos del silo';
            public       postgres    false    281            x           0    0 !   COLUMN ossilo.measuring_mechanism    COMMENT     Y   COMMENT ON COLUMN public.ossilo.measuring_mechanism IS 'Mecanismo de medidad del silo
';
            public       postgres    false    281            y           0    0    COLUMN ossilo.cone_degrees    COMMENT     C   COMMENT ON COLUMN public.ossilo.cone_degrees IS 'Grados del cono';
            public       postgres    false    281            z           0    0    COLUMN ossilo.total_capacity_1    COMMENT     L   COMMENT ON COLUMN public.ossilo.total_capacity_1 IS 'Total de capacidad 1';
            public       postgres    false    281            {           0    0    COLUMN ossilo.total_capacity_2    COMMENT     L   COMMENT ON COLUMN public.ossilo.total_capacity_2 IS 'Total de capacidad 2';
            public       postgres    false    281            |           0    0     COLUMN ossilo.capacity_unit_id_1    COMMENT     X   COMMENT ON COLUMN public.ossilo.capacity_unit_id_1 IS 'Id de Capacidad de la unidad 1';
            public       postgres    false    281            }           0    0     COLUMN ossilo.capacity_unit_id_2    COMMENT     X   COMMENT ON COLUMN public.ossilo.capacity_unit_id_2 IS 'Id de Capacidad de la unidad 2';
            public       postgres    false    281            ~           0    0    COLUMN ossilo.central    COMMENT     6   COMMENT ON COLUMN public.ossilo.central IS 'Central';
            public       postgres    false    281                       1259    28639    ossilo_osshed    TABLE     �   CREATE TABLE public.ossilo_osshed (
    silo_id integer NOT NULL,
    shed_id integer NOT NULL,
    center_id integer NOT NULL,
    farm_id integer NOT NULL,
    partnership_id integer NOT NULL,
    client_id integer NOT NULL,
    deleted_mark integer
);
 !   DROP TABLE public.ossilo_osshed;
       public         postgres    false    3                       0    0    TABLE ossilo_osshed    COMMENT     R   COMMENT ON TABLE public.ossilo_osshed IS 'Tabla union de las tablas silo y shed';
            public       postgres    false    282            �           0    0    COLUMN ossilo_osshed.silo_id    COMMENT     A   COMMENT ON COLUMN public.ossilo_osshed.silo_id IS 'Id del silo';
            public       postgres    false    282            �           0    0    COLUMN ossilo_osshed.shed_id    COMMENT     C   COMMENT ON COLUMN public.ossilo_osshed.shed_id IS 'Id del galpon';
            public       postgres    false    282            �           0    0    COLUMN ossilo_osshed.center_id    COMMENT     E   COMMENT ON COLUMN public.ossilo_osshed.center_id IS 'Id del nucleo';
            public       postgres    false    282            �           0    0    COLUMN ossilo_osshed.farm_id    COMMENT     E   COMMENT ON COLUMN public.ossilo_osshed.farm_id IS 'Id de la granja';
            public       postgres    false    282            �           0    0 #   COLUMN ossilo_osshed.partnership_id    COMMENT     M   COMMENT ON COLUMN public.ossilo_osshed.partnership_id IS 'Id de la empresa';
            public       postgres    false    282            �           0    0    COLUMN ossilo_osshed.client_id    COMMENT     F   COMMENT ON COLUMN public.ossilo_osshed.client_id IS 'Id del cliente';
            public       postgres    false    282            �           0    0 !   COLUMN ossilo_osshed.deleted_mark    COMMENT     K   COMMENT ON COLUMN public.ossilo_osshed.deleted_mark IS 'Marca de borrado';
            public       postgres    false    282                       1259    28642    slaughterhouse_id_seq    SEQUENCE        CREATE SEQUENCE public.slaughterhouse_id_seq
    START WITH 33
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.slaughterhouse_id_seq;
       public       postgres    false    3                       1259    28644    osslaughterhouse    TABLE     Z  CREATE TABLE public.osslaughterhouse (
    slaughterhouse_id integer DEFAULT nextval('public.slaughterhouse_id_seq'::regclass) NOT NULL,
    name character varying(45) NOT NULL,
    address character varying(250) NOT NULL,
    description character varying(250) NOT NULL,
    code character varying(20) NOT NULL,
    capacity double precision
);
 $   DROP TABLE public.osslaughterhouse;
       public         postgres    false    283    3                       1259    28651    warehouse_id_seq    SEQUENCE     y   CREATE SEQUENCE public.warehouse_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.warehouse_id_seq;
       public       postgres    false    3                       1259    28653    oswarehouse    TABLE       CREATE TABLE public.oswarehouse (
    warehouse_id integer DEFAULT nextval('public.warehouse_id_seq'::regclass) NOT NULL,
    partnership_id integer NOT NULL,
    farm_id integer NOT NULL,
    name character varying(45) NOT NULL,
    code character varying(20) NOT NULL
);
    DROP TABLE public.oswarehouse;
       public         postgres    false    285    3            �           0    0    TABLE oswarehouse    COMMENT     \   COMMENT ON TABLE public.oswarehouse IS 'Almacena la informacion referente a los almacenes';
            public       postgres    false    286            �           0    0    COLUMN oswarehouse.warehouse_id    COMMENT     G   COMMENT ON COLUMN public.oswarehouse.warehouse_id IS 'Id del almacen';
            public       postgres    false    286            �           0    0 !   COLUMN oswarehouse.partnership_id    COMMENT     ^   COMMENT ON COLUMN public.oswarehouse.partnership_id IS 'Id de la empresa dueña del almacen';
            public       postgres    false    286            �           0    0    COLUMN oswarehouse.farm_id    COMMENT     C   COMMENT ON COLUMN public.oswarehouse.farm_id IS 'Id de la granja';
            public       postgres    false    286            �           0    0    COLUMN oswarehouse.name    COMMENT     C   COMMENT ON COLUMN public.oswarehouse.name IS 'Nombre del almacen';
            public       postgres    false    286            �           0    0    COLUMN oswarehouse.code    COMMENT     C   COMMENT ON COLUMN public.oswarehouse.code IS 'Codigo del almacen';
            public       postgres    false    286                       1259    28657    posture_curve_id_seq    SEQUENCE     }   CREATE SEQUENCE public.posture_curve_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.posture_curve_id_seq;
       public       postgres    false    3                        1259    28659    predecessor_id_seq    SEQUENCE     {   CREATE SEQUENCE public.predecessor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.predecessor_id_seq;
       public       postgres    false    3            !           1259    28661    process_class_id_seq    SEQUENCE     }   CREATE SEQUENCE public.process_class_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.process_class_id_seq;
       public       postgres    false    3            "           1259    28663    programmed_eggs_id_seq    SEQUENCE        CREATE SEQUENCE public.programmed_eggs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.programmed_eggs_id_seq;
       public       postgres    false    3            #           1259    28665    raspberry_id_seq    SEQUENCE     y   CREATE SEQUENCE public.raspberry_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.raspberry_id_seq;
       public       postgres    false    3            $           1259    28667    scenario_formula_id_seq    SEQUENCE     �   CREATE SEQUENCE public.scenario_formula_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.scenario_formula_id_seq;
       public       postgres    false    3            %           1259    28669    scenario_parameter_day_seq    SEQUENCE     �   CREATE SEQUENCE public.scenario_parameter_day_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.scenario_parameter_day_seq;
       public       postgres    false    3            &           1259    28671    scenario_parameter_id_seq    SEQUENCE     �   CREATE SEQUENCE public.scenario_parameter_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.scenario_parameter_id_seq;
       public       postgres    false    3            '           1259    28673    scenario_posture_id_seq    SEQUENCE     �   CREATE SEQUENCE public.scenario_posture_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.scenario_posture_id_seq;
       public       postgres    false    3            (           1259    28675    scenario_process_id_seq    SEQUENCE     �   CREATE SEQUENCE public.scenario_process_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.scenario_process_id_seq;
       public       postgres    false    3            )           1259    28677    txavailabilitysheds    TABLE       CREATE TABLE public.txavailabilitysheds (
    availability_shed_id integer DEFAULT nextval('public.availability_shed_id_seq'::regclass) NOT NULL,
    shed_id integer NOT NULL,
    init_date date,
    end_date date,
    lot_code character varying(20) NOT NULL
);
 '   DROP TABLE public.txavailabilitysheds;
       public         postgres    false    216    3            �           0    0    TABLE txavailabilitysheds    COMMENT     �   COMMENT ON TABLE public.txavailabilitysheds IS 'Almacena la disponibilidad en fechas de los galpones de acuerdo a la programación establecida';
            public       postgres    false    297            �           0    0 /   COLUMN txavailabilitysheds.availability_shed_id    COMMENT     �   COMMENT ON COLUMN public.txavailabilitysheds.availability_shed_id IS 'Id de la disponibilidad del almacen, indicando si este esta disponible';
            public       postgres    false    297            �           0    0 "   COLUMN txavailabilitysheds.shed_id    COMMENT     I   COMMENT ON COLUMN public.txavailabilitysheds.shed_id IS 'Id del galpon';
            public       postgres    false    297            �           0    0 $   COLUMN txavailabilitysheds.init_date    COMMENT     r   COMMENT ON COLUMN public.txavailabilitysheds.init_date IS 'Fecha de inicio de la programacion de uso del galpon';
            public       postgres    false    297            �           0    0 #   COLUMN txavailabilitysheds.end_date    COMMENT     r   COMMENT ON COLUMN public.txavailabilitysheds.end_date IS 'Fecha de cerrado de la programacion de uso del galpon';
            public       postgres    false    297            �           0    0 #   COLUMN txavailabilitysheds.lot_code    COMMENT     W   COMMENT ON COLUMN public.txavailabilitysheds.lot_code IS 'codigo del lote del galpon';
            public       postgres    false    297            *           1259    28681 	   txbroiler    TABLE     L  CREATE TABLE public.txbroiler (
    broiler_id integer DEFAULT nextval('public.broiler_id_seq'::regclass) NOT NULL,
    projected_date date,
    projected_quantity integer,
    partnership_id integer,
    scenario_id integer,
    breed_id integer,
    lot_incubator character varying(45) NOT NULL,
    programmed_eggs_id integer
);
    DROP TABLE public.txbroiler;
       public         postgres    false    220    3            �           0    0    TABLE txbroiler    COMMENT     c   COMMENT ON TABLE public.txbroiler IS 'Almacena la proyeccion realizada para el modulo de engorde';
            public       postgres    false    298            �           0    0    COLUMN txbroiler.broiler_id    COMMENT     U   COMMENT ON COLUMN public.txbroiler.broiler_id IS 'Id de la programacion de engorde';
            public       postgres    false    298            �           0    0    COLUMN txbroiler.projected_date    COMMENT     X   COMMENT ON COLUMN public.txbroiler.projected_date IS 'Fecha de proyección de engorde';
            public       postgres    false    298            �           0    0 #   COLUMN txbroiler.projected_quantity    COMMENT     `   COMMENT ON COLUMN public.txbroiler.projected_quantity IS 'Cantidad proyectada para el engorde';
            public       postgres    false    298            �           0    0    COLUMN txbroiler.partnership_id    COMMENT     I   COMMENT ON COLUMN public.txbroiler.partnership_id IS 'Id de la empresa';
            public       postgres    false    298            �           0    0    COLUMN txbroiler.scenario_id    COMMENT     G   COMMENT ON COLUMN public.txbroiler.scenario_id IS 'Id edl escenario ';
            public       postgres    false    298            �           0    0    COLUMN txbroiler.breed_id    COMMENT     K   COMMENT ON COLUMN public.txbroiler.breed_id IS 'Id de la raza a engordar';
            public       postgres    false    298            �           0    0    COLUMN txbroiler.lot_incubator    COMMENT     u   COMMENT ON COLUMN public.txbroiler.lot_incubator IS 'Lote de incubación de donde provienen los huevos proyectados';
            public       postgres    false    298            �           0    0 #   COLUMN txbroiler.programmed_eggs_id    COMMENT     Y   COMMENT ON COLUMN public.txbroiler.programmed_eggs_id IS 'Id de los huevos programados';
            public       postgres    false    298            +           1259    28685    txbroiler_detail    TABLE       CREATE TABLE public.txbroiler_detail (
    broiler_detail_id integer DEFAULT nextval('public.broiler_detail_id_seq'::regclass) NOT NULL,
    broiler_id integer NOT NULL,
    scheduled_date date,
    scheduled_quantity integer,
    farm_id integer NOT NULL,
    shed_id integer NOT NULL,
    confirm integer,
    execution_date date,
    execution_quantity integer,
    lot integer NOT NULL,
    broiler_product_id integer,
    center_id integer,
    executionfarm_id integer,
    executioncenter_id integer,
    executionshed_id integer
);
 $   DROP TABLE public.txbroiler_detail;
       public         postgres    false    219    3            �           0    0    TABLE txbroiler_detail    COMMENT     l   COMMENT ON TABLE public.txbroiler_detail IS 'Almacena la programacion y ejecuccion del proceso de engorde';
            public       postgres    false    299            �           0    0 )   COLUMN txbroiler_detail.broiler_detail_id    COMMENT     `   COMMENT ON COLUMN public.txbroiler_detail.broiler_detail_id IS 'Id de los detalles de engorde';
            public       postgres    false    299            �           0    0 "   COLUMN txbroiler_detail.broiler_id    COMMENT     \   COMMENT ON COLUMN public.txbroiler_detail.broiler_id IS 'Id de la programacion de engorde';
            public       postgres    false    299            �           0    0 &   COLUMN txbroiler_detail.scheduled_date    COMMENT     k   COMMENT ON COLUMN public.txbroiler_detail.scheduled_date IS 'Fecha programada para el proceso de engorde';
            public       postgres    false    299            �           0    0 *   COLUMN txbroiler_detail.scheduled_quantity    COMMENT     r   COMMENT ON COLUMN public.txbroiler_detail.scheduled_quantity IS 'Cantidad programada para el proceso de engorde';
            public       postgres    false    299            �           0    0    COLUMN txbroiler_detail.farm_id    COMMENT     H   COMMENT ON COLUMN public.txbroiler_detail.farm_id IS 'Id de la granja';
            public       postgres    false    299            �           0    0    COLUMN txbroiler_detail.shed_id    COMMENT     F   COMMENT ON COLUMN public.txbroiler_detail.shed_id IS 'Id del galpon';
            public       postgres    false    299            �           0    0    COLUMN txbroiler_detail.confirm    COMMENT     E   COMMENT ON COLUMN public.txbroiler_detail.confirm IS 'Confirmacion';
            public       postgres    false    299            �           0    0 &   COLUMN txbroiler_detail.execution_date    COMMENT     p   COMMENT ON COLUMN public.txbroiler_detail.execution_date IS 'Fecha de ejeccion de la planificacion de engorde';
            public       postgres    false    299            �           0    0 *   COLUMN txbroiler_detail.execution_quantity    COMMENT     u   COMMENT ON COLUMN public.txbroiler_detail.execution_quantity IS 'Cantidad ejecutada de la programación de engorde';
            public       postgres    false    299            �           0    0    COLUMN txbroiler_detail.lot    COMMENT     D   COMMENT ON COLUMN public.txbroiler_detail.lot IS 'Lote de engorde';
            public       postgres    false    299            �           0    0 *   COLUMN txbroiler_detail.broiler_product_id    COMMENT     ^   COMMENT ON COLUMN public.txbroiler_detail.broiler_product_id IS 'Id del producto de engorde';
            public       postgres    false    299            ,           1259    28689    txbroilereviction    TABLE     �  CREATE TABLE public.txbroilereviction (
    broilereviction_id integer DEFAULT nextval('public.broilereviction_id_seq'::regclass) NOT NULL,
    projected_date date,
    projected_quantity integer,
    partnership_id integer NOT NULL,
    scenario_id integer NOT NULL,
    breed_id integer NOT NULL,
    lot_incubator character varying(45) NOT NULL,
    broiler_detail_id integer NOT NULL
);
 %   DROP TABLE public.txbroilereviction;
       public         postgres    false    224    3            �           0    0    TABLE txbroilereviction    COMMENT     _   COMMENT ON TABLE public.txbroilereviction IS 'Almacena las proyeccion del modula de desalojo';
            public       postgres    false    300            �           0    0 +   COLUMN txbroilereviction.broilereviction_id    COMMENT     ^   COMMENT ON COLUMN public.txbroilereviction.broilereviction_id IS 'Id del modulo de desalojo';
            public       postgres    false    300            �           0    0 '   COLUMN txbroilereviction.projected_date    COMMENT     b   COMMENT ON COLUMN public.txbroilereviction.projected_date IS 'Fecha proyectada para el desalojo';
            public       postgres    false    300            �           0    0 +   COLUMN txbroilereviction.projected_quantity    COMMENT     i   COMMENT ON COLUMN public.txbroilereviction.projected_quantity IS 'Cantidad proyectada para el desalojo';
            public       postgres    false    300            �           0    0 '   COLUMN txbroilereviction.partnership_id    COMMENT     Q   COMMENT ON COLUMN public.txbroilereviction.partnership_id IS 'Id de la empresa';
            public       postgres    false    300            �           0    0 $   COLUMN txbroilereviction.scenario_id    COMMENT     N   COMMENT ON COLUMN public.txbroilereviction.scenario_id IS 'Id del escenario';
            public       postgres    false    300            �           0    0 !   COLUMN txbroilereviction.breed_id    COMMENT     H   COMMENT ON COLUMN public.txbroilereviction.breed_id IS 'Id de la raza';
            public       postgres    false    300            �           0    0 &   COLUMN txbroilereviction.lot_incubator    COMMENT     R   COMMENT ON COLUMN public.txbroilereviction.lot_incubator IS 'Lote de incubacion';
            public       postgres    false    300            -           1259    28693    txbroilereviction_detail    TABLE     9  CREATE TABLE public.txbroilereviction_detail (
    broilereviction_detail_id integer DEFAULT nextval('public.broilereviction_detail_id_seq'::regclass) NOT NULL,
    broilereviction_id integer NOT NULL,
    scheduled_date date,
    scheduled_quantity integer,
    farm_id integer NOT NULL,
    shed_id integer NOT NULL,
    confirm integer,
    execution_date date,
    execution_quantity integer,
    lot integer NOT NULL,
    broiler_product_id integer NOT NULL,
    slaughterhouse_id integer NOT NULL,
    center_id integer,
    executionslaughterhouse_id integer
);
 ,   DROP TABLE public.txbroilereviction_detail;
       public         postgres    false    223    3            �           0    0    TABLE txbroilereviction_detail    COMMENT     v   COMMENT ON TABLE public.txbroilereviction_detail IS 'Almacena la programación y ejecución del módulo de desalojo';
            public       postgres    false    301            �           0    0 9   COLUMN txbroilereviction_detail.broilereviction_detail_id    COMMENT     ~   COMMENT ON COLUMN public.txbroilereviction_detail.broilereviction_detail_id IS 'Id de los detalles del modulo de desarrollo';
            public       postgres    false    301            �           0    0 2   COLUMN txbroilereviction_detail.broilereviction_id    COMMENT     e   COMMENT ON COLUMN public.txbroilereviction_detail.broilereviction_id IS 'Id del modulo de desalojo';
            public       postgres    false    301            �           0    0 .   COLUMN txbroilereviction_detail.scheduled_date    COMMENT     i   COMMENT ON COLUMN public.txbroilereviction_detail.scheduled_date IS 'Fecha programada para el desalojo';
            public       postgres    false    301            �           0    0 2   COLUMN txbroilereviction_detail.scheduled_quantity    COMMENT     p   COMMENT ON COLUMN public.txbroilereviction_detail.scheduled_quantity IS 'Cantidad programada para el desalojo';
            public       postgres    false    301            �           0    0 '   COLUMN txbroilereviction_detail.farm_id    COMMENT     P   COMMENT ON COLUMN public.txbroilereviction_detail.farm_id IS 'Id de la granja';
            public       postgres    false    301            �           0    0 '   COLUMN txbroilereviction_detail.shed_id    COMMENT     N   COMMENT ON COLUMN public.txbroilereviction_detail.shed_id IS 'Id del galpon';
            public       postgres    false    301            �           0    0 '   COLUMN txbroilereviction_detail.confirm    COMMENT     M   COMMENT ON COLUMN public.txbroilereviction_detail.confirm IS 'Confirmacion';
            public       postgres    false    301            �           0    0 .   COLUMN txbroilereviction_detail.execution_date    COMMENT     \   COMMENT ON COLUMN public.txbroilereviction_detail.execution_date IS 'Fecha de ejecución ';
            public       postgres    false    301            �           0    0 2   COLUMN txbroilereviction_detail.execution_quantity    COMMENT     c   COMMENT ON COLUMN public.txbroilereviction_detail.execution_quantity IS 'Cantidad de ejecución ';
            public       postgres    false    301            �           0    0 #   COLUMN txbroilereviction_detail.lot    COMMENT     X   COMMENT ON COLUMN public.txbroilereviction_detail.lot IS 'Lote del modulo de desalojo';
            public       postgres    false    301            �           0    0 2   COLUMN txbroilereviction_detail.broiler_product_id    COMMENT     f   COMMENT ON COLUMN public.txbroilereviction_detail.broiler_product_id IS 'Id del producto de engorde';
            public       postgres    false    301            �           0    0 1   COLUMN txbroilereviction_detail.slaughterhouse_id    COMMENT     g   COMMENT ON COLUMN public.txbroilereviction_detail.slaughterhouse_id IS 'Id de la planta de beneficio';
            public       postgres    false    301            .           1259    28697    txbroilerproduct_detail    TABLE     �   CREATE TABLE public.txbroilerproduct_detail (
    broilerproduct_detail_id integer DEFAULT nextval('public.broiler_product_detail_id_seq'::regclass) NOT NULL,
    broiler_detail integer NOT NULL,
    broiler_product_id integer,
    quantity integer
);
 +   DROP TABLE public.txbroilerproduct_detail;
       public         postgres    false    221    3            �           0    0    TABLE txbroilerproduct_detail    COMMENT     h   COMMENT ON TABLE public.txbroilerproduct_detail IS 'Almacena los detalles de la produccion de engorde';
            public       postgres    false    302            �           0    0 7   COLUMN txbroilerproduct_detail.broilerproduct_detail_id    COMMENT     |   COMMENT ON COLUMN public.txbroilerproduct_detail.broilerproduct_detail_id IS 'Id de los detalles de produccion de engorde';
            public       postgres    false    302            �           0    0 -   COLUMN txbroilerproduct_detail.broiler_detail    COMMENT     Z   COMMENT ON COLUMN public.txbroilerproduct_detail.broiler_detail IS 'Detalles de engorde';
            public       postgres    false    302            �           0    0 1   COLUMN txbroilerproduct_detail.broiler_product_id    COMMENT     e   COMMENT ON COLUMN public.txbroilerproduct_detail.broiler_product_id IS 'Id del producto de engorde';
            public       postgres    false    302            �           0    0 '   COLUMN txbroilerproduct_detail.quantity    COMMENT     `   COMMENT ON COLUMN public.txbroilerproduct_detail.quantity IS 'Cantidad de producto de engorde';
            public       postgres    false    302            /           1259    28701    txbroodermachine    TABLE     �  CREATE TABLE public.txbroodermachine (
    brooder_machine_id_seq integer DEFAULT nextval('public.brooder_machines_id_seq'::regclass) NOT NULL,
    partnership_id integer NOT NULL,
    farm_id integer NOT NULL,
    capacity integer,
    sunday integer,
    monday integer,
    tuesday integer,
    wednesday integer,
    thursday integer,
    friday integer,
    saturday integer,
    name character varying(250)
);
 $   DROP TABLE public.txbroodermachine;
       public         postgres    false    226    3            �           0    0    TABLE txbroodermachine    COMMENT     ]   COMMENT ON TABLE public.txbroodermachine IS 'Almacena los datos de las maquinas de engorde';
            public       postgres    false    303            �           0    0 .   COLUMN txbroodermachine.brooder_machine_id_seq    COMMENT     c   COMMENT ON COLUMN public.txbroodermachine.brooder_machine_id_seq IS 'Id de la maquina de engorde';
            public       postgres    false    303            �           0    0 &   COLUMN txbroodermachine.partnership_id    COMMENT     P   COMMENT ON COLUMN public.txbroodermachine.partnership_id IS 'Id de la empresa';
            public       postgres    false    303            �           0    0    COLUMN txbroodermachine.farm_id    COMMENT     H   COMMENT ON COLUMN public.txbroodermachine.farm_id IS 'Id de la granja';
            public       postgres    false    303            �           0    0     COLUMN txbroodermachine.capacity    COMMENT     Q   COMMENT ON COLUMN public.txbroodermachine.capacity IS 'Capacidad de la maquina';
            public       postgres    false    303            �           0    0    COLUMN txbroodermachine.sunday    COMMENT     ?   COMMENT ON COLUMN public.txbroodermachine.sunday IS 'Domingo';
            public       postgres    false    303            �           0    0    COLUMN txbroodermachine.monday    COMMENT     =   COMMENT ON COLUMN public.txbroodermachine.monday IS 'Lunes';
            public       postgres    false    303            �           0    0    COLUMN txbroodermachine.tuesday    COMMENT     ?   COMMENT ON COLUMN public.txbroodermachine.tuesday IS 'Martes';
            public       postgres    false    303            �           0    0 !   COLUMN txbroodermachine.wednesday    COMMENT     D   COMMENT ON COLUMN public.txbroodermachine.wednesday IS 'Miercoles';
            public       postgres    false    303            �           0    0     COLUMN txbroodermachine.thursday    COMMENT     @   COMMENT ON COLUMN public.txbroodermachine.thursday IS 'Jueves';
            public       postgres    false    303            �           0    0    COLUMN txbroodermachine.friday    COMMENT     ?   COMMENT ON COLUMN public.txbroodermachine.friday IS 'Viernes';
            public       postgres    false    303            �           0    0     COLUMN txbroodermachine.saturday    COMMENT     @   COMMENT ON COLUMN public.txbroodermachine.saturday IS 'Sabado';
            public       postgres    false    303            �           0    0    COLUMN txbroodermachine.name    COMMENT     J   COMMENT ON COLUMN public.txbroodermachine.name IS 'Nombre de la maquina';
            public       postgres    false    303            0           1259    28705 
   txcalendar    TABLE     |  CREATE TABLE public.txcalendar (
    calendar_id integer DEFAULT nextval('public.calendar_id_seq'::regclass) NOT NULL,
    description character varying(250) NOT NULL,
    saturday character varying(15),
    sunday character varying(15),
    week_start character varying(15),
    code character(20) NOT NULL,
    year_start integer,
    year_end integer,
    generated integer
);
    DROP TABLE public.txcalendar;
       public         postgres    false    228    3            �           0    0    TABLE txcalendar    COMMENT     n   COMMENT ON TABLE public.txcalendar IS 'Almacena la informacion del calendario con la que trabaja el sistema';
            public       postgres    false    304            �           0    0    COLUMN txcalendar.calendar_id    COMMENT     H   COMMENT ON COLUMN public.txcalendar.calendar_id IS 'Id del calendario';
            public       postgres    false    304            �           0    0    COLUMN txcalendar.description    COMMENT     S   COMMENT ON COLUMN public.txcalendar.description IS 'Descripción del calendario
';
            public       postgres    false    304            �           0    0    COLUMN txcalendar.saturday    COMMENT     Z   COMMENT ON COLUMN public.txcalendar.saturday IS 'Indica si el día sábado es laborable';
            public       postgres    false    304            �           0    0    COLUMN txcalendar.sunday    COMMENT     X   COMMENT ON COLUMN public.txcalendar.sunday IS 'Indica si el día Domingo es laborable';
            public       postgres    false    304            �           0    0    COLUMN txcalendar.week_start    COMMENT     [   COMMENT ON COLUMN public.txcalendar.week_start IS 'Semana en la que inicia el calendario';
            public       postgres    false    304            �           0    0    COLUMN txcalendar.code    COMMENT     E   COMMENT ON COLUMN public.txcalendar.code IS 'Codigo del calendario';
            public       postgres    false    304            �           0    0    COLUMN txcalendar.year_start    COMMENT     Y   COMMENT ON COLUMN public.txcalendar.year_start IS 'Año en el que inicia el calendario';
            public       postgres    false    304            �           0    0    COLUMN txcalendar.year_end    COMMENT     Y   COMMENT ON COLUMN public.txcalendar.year_end IS 'Año en el que finaliza el calendario';
            public       postgres    false    304            �           0    0    COLUMN txcalendar.generated    COMMENT     u   COMMENT ON COLUMN public.txcalendar.generated IS 'Indica si el calendario fue generado a partir de otro calendario';
            public       postgres    false    304            1           1259    28709    txcalendarday    TABLE     �  CREATE TABLE public.txcalendarday (
    calendar_day_id integer DEFAULT nextval('public.calendar_day_id_seq'::regclass) NOT NULL,
    calendar_id integer NOT NULL,
    use_date timestamp with time zone NOT NULL,
    use_year integer NOT NULL,
    use_month integer NOT NULL,
    use_day integer NOT NULL,
    use_week timestamp with time zone NOT NULL,
    week_day integer NOT NULL,
    sequence integer NOT NULL,
    working_day integer NOT NULL
);
 !   DROP TABLE public.txcalendarday;
       public         postgres    false    227    3            �           0    0    TABLE txcalendarday    COMMENT     _   COMMENT ON TABLE public.txcalendarday IS 'Almacena los datos de los dias que son laborables ';
            public       postgres    false    305            �           0    0 $   COLUMN txcalendarday.calendar_day_id    COMMENT     W   COMMENT ON COLUMN public.txcalendarday.calendar_day_id IS 'Id del dia del calendario';
            public       postgres    false    305            �           0    0     COLUMN txcalendarday.calendar_id    COMMENT     K   COMMENT ON COLUMN public.txcalendarday.calendar_id IS 'Id del calendario';
            public       postgres    false    305            �           0    0    COLUMN txcalendarday.use_date    COMMENT     d   COMMENT ON COLUMN public.txcalendarday.use_date IS 'Fecha en el que se encuentra el dia laborable';
            public       postgres    false    305            �           0    0    COLUMN txcalendarday.use_year    COMMENT     c   COMMENT ON COLUMN public.txcalendarday.use_year IS 'Año en el que se encuentra el dia laborable';
            public       postgres    false    305            �           0    0    COLUMN txcalendarday.use_month    COMMENT     c   COMMENT ON COLUMN public.txcalendarday.use_month IS 'Mes en el que se encuentra el dia laborable';
            public       postgres    false    305            �           0    0    COLUMN txcalendarday.use_day    COMMENT     a   COMMENT ON COLUMN public.txcalendarday.use_day IS 'Dia en el que se encuentra el dia laborable';
            public       postgres    false    305            �           0    0    COLUMN txcalendarday.use_week    COMMENT     e   COMMENT ON COLUMN public.txcalendarday.use_week IS 'Semana en el que se encuentra el dia laborable';
            public       postgres    false    305            �           0    0    COLUMN txcalendarday.week_day    COMMENT     l   COMMENT ON COLUMN public.txcalendarday.week_day IS 'Dia de semana en el que se encuentra el dia laborable';
            public       postgres    false    305            �           0    0     COLUMN txcalendarday.working_day    COMMENT     Z   COMMENT ON COLUMN public.txcalendarday.working_day IS 'Indica si el dia es laboral o no';
            public       postgres    false    305            2           1259    28713    txeggs_movements_id_seq    SEQUENCE     �   CREATE SEQUENCE public.txeggs_movements_id_seq
    START WITH 170
    INCREMENT BY 2041
    NO MINVALUE
    MAXVALUE 9999999999999999
    CACHE 1;
 .   DROP SEQUENCE public.txeggs_movements_id_seq;
       public       postgres    false    3            3           1259    28715    txeggs_movements    TABLE     l  CREATE TABLE public.txeggs_movements (
    eggs_movements_id integer DEFAULT nextval('public.txeggs_movements_id_seq'::regclass) NOT NULL,
    fecha_movements date NOT NULL,
    lot integer NOT NULL,
    quantity integer NOT NULL,
    type_movements character varying NOT NULL,
    eggs_storage_id integer NOT NULL,
    description_adjustment character varying
);
 $   DROP TABLE public.txeggs_movements;
       public         postgres    false    306    3            4           1259    28722    txeggs_planning    TABLE       CREATE TABLE public.txeggs_planning (
    egg_planning_id integer DEFAULT nextval('public.egg_planning_id_seq'::regclass) NOT NULL,
    month_planning integer,
    year_planning integer,
    scenario_id integer,
    planned double precision,
    breed_id integer NOT NULL
);
 #   DROP TABLE public.txeggs_planning;
       public         postgres    false    230    3            �           0    0    TABLE txeggs_planning    COMMENT     g   COMMENT ON TABLE public.txeggs_planning IS 'Almacena los detalles de la planificación de los huevos';
            public       postgres    false    308            �           0    0 &   COLUMN txeggs_planning.egg_planning_id    COMMENT     [   COMMENT ON COLUMN public.txeggs_planning.egg_planning_id IS 'Id de planeación de huevos';
            public       postgres    false    308            �           0    0 %   COLUMN txeggs_planning.month_planning    COMMENT     c   COMMENT ON COLUMN public.txeggs_planning.month_planning IS 'Mes de planificación de los huevos
';
            public       postgres    false    308            �           0    0 $   COLUMN txeggs_planning.year_planning    COMMENT     b   COMMENT ON COLUMN public.txeggs_planning.year_planning IS 'Año de planificación de los huevos';
            public       postgres    false    308            �           0    0 "   COLUMN txeggs_planning.scenario_id    COMMENT     p   COMMENT ON COLUMN public.txeggs_planning.scenario_id IS 'Escenario al cual pertenecen los huevos planificados';
            public       postgres    false    308            �           0    0    COLUMN txeggs_planning.planned    COMMENT     X   COMMENT ON COLUMN public.txeggs_planning.planned IS 'Cantidad de huevos planificados
';
            public       postgres    false    308            �           0    0    COLUMN txeggs_planning.breed_id    COMMENT     T   COMMENT ON COLUMN public.txeggs_planning.breed_id IS 'Id de la raza de los huevos';
            public       postgres    false    308            5           1259    28726    txeggs_required    TABLE     
  CREATE TABLE public.txeggs_required (
    egg_required_id integer DEFAULT nextval('public.egg_required_id_seq'::regclass) NOT NULL,
    use_month integer,
    use_year integer,
    scenario_id integer NOT NULL,
    required double precision,
    breed_id integer
);
 #   DROP TABLE public.txeggs_required;
       public         postgres    false    231    3            �           0    0    TABLE txeggs_required    COMMENT     V   COMMENT ON TABLE public.txeggs_required IS 'Almacena los datos de huevos requeridos';
            public       postgres    false    309            �           0    0 &   COLUMN txeggs_required.egg_required_id    COMMENT     [   COMMENT ON COLUMN public.txeggs_required.egg_required_id IS 'Id de los huevos requeridos';
            public       postgres    false    309            �           0    0     COLUMN txeggs_required.use_month    COMMENT     =   COMMENT ON COLUMN public.txeggs_required.use_month IS 'Mes';
            public       postgres    false    309            �           0    0    COLUMN txeggs_required.use_year    COMMENT     =   COMMENT ON COLUMN public.txeggs_required.use_year IS 'Año';
            public       postgres    false    309            �           0    0 "   COLUMN txeggs_required.scenario_id    COMMENT     L   COMMENT ON COLUMN public.txeggs_required.scenario_id IS 'Id del escenario';
            public       postgres    false    309            �           0    0    COLUMN txeggs_required.required    COMMENT     K   COMMENT ON COLUMN public.txeggs_required.required IS 'Cantidad requerida';
            public       postgres    false    309            �           0    0    COLUMN txeggs_required.breed_id    COMMENT     F   COMMENT ON COLUMN public.txeggs_required.breed_id IS 'Id de la raza';
            public       postgres    false    309            6           1259    28730    txeggs_storage    TABLE     b  CREATE TABLE public.txeggs_storage (
    eggs_storage_id integer DEFAULT nextval('public.eggs_storage_id_seq'::regclass) NOT NULL,
    incubator_plant_id integer NOT NULL,
    scenario_id integer NOT NULL,
    breed_id integer NOT NULL,
    init_date date,
    end_date date,
    lot character varying(45),
    eggs integer,
    eggs_executed integer
);
 "   DROP TABLE public.txeggs_storage;
       public         postgres    false    232    3            �           0    0    TABLE txeggs_storage    COMMENT     ~   COMMENT ON TABLE public.txeggs_storage IS 'Guarda la informacion de almacenamiento de los huevos en las plantas incubadoras';
            public       postgres    false    310            �           0    0 %   COLUMN txeggs_storage.eggs_storage_id    COMMENT     W   COMMENT ON COLUMN public.txeggs_storage.eggs_storage_id IS 'Id del almacen de huevos';
            public       postgres    false    310            �           0    0 (   COLUMN txeggs_storage.incubator_plant_id    COMMENT     Y   COMMENT ON COLUMN public.txeggs_storage.incubator_plant_id IS 'Id de planta incubadora';
            public       postgres    false    310            �           0    0 !   COLUMN txeggs_storage.scenario_id    COMMENT     K   COMMENT ON COLUMN public.txeggs_storage.scenario_id IS 'Id del escenario';
            public       postgres    false    310            �           0    0    COLUMN txeggs_storage.breed_id    COMMENT     E   COMMENT ON COLUMN public.txeggs_storage.breed_id IS 'Id de la raza';
            public       postgres    false    310            �           0    0    COLUMN txeggs_storage.init_date    COMMENT     H   COMMENT ON COLUMN public.txeggs_storage.init_date IS 'Fecha de inicio';
            public       postgres    false    310            �           0    0    COLUMN txeggs_storage.end_date    COMMENT     J   COMMENT ON COLUMN public.txeggs_storage.end_date IS 'Fecha de terminado';
            public       postgres    false    310            �           0    0    COLUMN txeggs_storage.lot    COMMENT     7   COMMENT ON COLUMN public.txeggs_storage.lot IS 'Lote';
            public       postgres    false    310            �           0    0    COLUMN txeggs_storage.eggs    COMMENT     F   COMMENT ON COLUMN public.txeggs_storage.eggs IS 'Cantidad de huevos';
            public       postgres    false    310            7           1259    28734    txgoals_erp    TABLE     �   CREATE TABLE public.txgoals_erp (
    goals_erp_id bigint NOT NULL,
    use_week date,
    use_value integer,
    product_id integer NOT NULL,
    code character varying(10),
    scenario_id integer NOT NULL
);
    DROP TABLE public.txgoals_erp;
       public         postgres    false    3            �           0    0    TABLE txgoals_erp    COMMENT     �   COMMENT ON TABLE public.txgoals_erp IS 'Almacena los datos generados de las metas de producción de la planificación regresiva para ser enviados al ERP';
            public       postgres    false    311            �           0    0    COLUMN txgoals_erp.goals_erp_id    COMMENT     N   COMMENT ON COLUMN public.txgoals_erp.goals_erp_id IS 'Id de la meta del ERP';
            public       postgres    false    311            �           0    0    COLUMN txgoals_erp.use_week    COMMENT     ;   COMMENT ON COLUMN public.txgoals_erp.use_week IS 'Semana';
            public       postgres    false    311            �           0    0    COLUMN txgoals_erp.use_value    COMMENT     D   COMMENT ON COLUMN public.txgoals_erp.use_value IS 'Valor objetivo';
            public       postgres    false    311            �           0    0    COLUMN txgoals_erp.product_id    COMMENT     F   COMMENT ON COLUMN public.txgoals_erp.product_id IS 'Id del producto';
            public       postgres    false    311            �           0    0    COLUMN txgoals_erp.code    COMMENT     D   COMMENT ON COLUMN public.txgoals_erp.code IS 'Codigo del producto';
            public       postgres    false    311                        0    0    COLUMN txgoals_erp.scenario_id    COMMENT     H   COMMENT ON COLUMN public.txgoals_erp.scenario_id IS 'Id del escenario';
            public       postgres    false    311            8           1259    28737    txgoals_erp_goals_erp_id_seq    SEQUENCE     �   CREATE SEQUENCE public.txgoals_erp_goals_erp_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.txgoals_erp_goals_erp_id_seq;
       public       postgres    false    311    3                       0    0    txgoals_erp_goals_erp_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.txgoals_erp_goals_erp_id_seq OWNED BY public.txgoals_erp.goals_erp_id;
            public       postgres    false    312            9           1259    28739    txhousingway    TABLE     d  CREATE TABLE public.txhousingway (
    housing_way_id integer DEFAULT nextval('public.housing_way_id_seq'::regclass) NOT NULL,
    projected_quantity integer,
    projected_date date,
    stage_id integer NOT NULL,
    partnership_id integer NOT NULL,
    scenario_id integer NOT NULL,
    breed_id integer NOT NULL,
    predecessor_id integer NOT NULL
);
     DROP TABLE public.txhousingway;
       public         postgres    false    237    3                       0    0    TABLE txhousingway    COMMENT     t   COMMENT ON TABLE public.txhousingway IS 'Almacena la proyección de los módulos de levante, cría y reproductora';
            public       postgres    false    313                       0    0 "   COLUMN txhousingway.housing_way_id    COMMENT     �   COMMENT ON COLUMN public.txhousingway.housing_way_id IS 'Id de las proyecciones  de los módulos de levante, cría y reproductora';
            public       postgres    false    313                       0    0 &   COLUMN txhousingway.projected_quantity    COMMENT     S   COMMENT ON COLUMN public.txhousingway.projected_quantity IS 'Cantidad proyectada';
            public       postgres    false    313                       0    0 "   COLUMN txhousingway.projected_date    COMMENT     L   COMMENT ON COLUMN public.txhousingway.projected_date IS 'Fecha proyectada';
            public       postgres    false    313                       0    0    COLUMN txhousingway.stage_id    COMMENT     D   COMMENT ON COLUMN public.txhousingway.stage_id IS 'Id de la etapa';
            public       postgres    false    313                       0    0 "   COLUMN txhousingway.partnership_id    COMMENT     L   COMMENT ON COLUMN public.txhousingway.partnership_id IS 'Id de la empresa';
            public       postgres    false    313                       0    0    COLUMN txhousingway.breed_id    COMMENT     C   COMMENT ON COLUMN public.txhousingway.breed_id IS 'Id de la raza';
            public       postgres    false    313            	           0    0 "   COLUMN txhousingway.predecessor_id    COMMENT     N   COMMENT ON COLUMN public.txhousingway.predecessor_id IS 'Id del predecesor ';
            public       postgres    false    313            :           1259    28743    txhousingway_detail    TABLE     W  CREATE TABLE public.txhousingway_detail (
    housingway_detail_id integer DEFAULT nextval('public.housing_way_detail_id_seq'::regclass) NOT NULL,
    housing_way_id integer NOT NULL,
    scheduled_date date,
    scheduled_quantity integer,
    farm_id integer NOT NULL,
    shed_id integer NOT NULL,
    confirm integer,
    execution_date date,
    execution_quantity integer,
    lot character varying(45),
    incubator_plant_id integer,
    center_id integer,
    executionfarm_id integer,
    executioncenter_id integer,
    executionshed_id integer,
    executionincubatorplant_id integer
);
 '   DROP TABLE public.txhousingway_detail;
       public         postgres    false    236    3            
           0    0    TABLE txhousingway_detail    COMMENT     �   COMMENT ON TABLE public.txhousingway_detail IS 'Almacena la programación y la ejecución de los módulos de levante y cría y reproductora';
            public       postgres    false    314                       0    0 /   COLUMN txhousingway_detail.housingway_detail_id    COMMENT     �   COMMENT ON COLUMN public.txhousingway_detail.housingway_detail_id IS 'Id de la programación y ejecución de los modelos de levante y cría y reproductora';
            public       postgres    false    314                       0    0 )   COLUMN txhousingway_detail.housing_way_id    COMMENT     �   COMMENT ON COLUMN public.txhousingway_detail.housing_way_id IS 'Id de las proyecciones  de los módulos de levante, cría y reproductora';
            public       postgres    false    314                       0    0 )   COLUMN txhousingway_detail.scheduled_date    COMMENT     S   COMMENT ON COLUMN public.txhousingway_detail.scheduled_date IS 'Fecha programada';
            public       postgres    false    314                       0    0 -   COLUMN txhousingway_detail.scheduled_quantity    COMMENT     Z   COMMENT ON COLUMN public.txhousingway_detail.scheduled_quantity IS 'Cantidad programada';
            public       postgres    false    314                       0    0 "   COLUMN txhousingway_detail.farm_id    COMMENT     K   COMMENT ON COLUMN public.txhousingway_detail.farm_id IS 'Id de la granja';
            public       postgres    false    314                       0    0 "   COLUMN txhousingway_detail.shed_id    COMMENT     S   COMMENT ON COLUMN public.txhousingway_detail.shed_id IS 'Id del galpon utilizado';
            public       postgres    false    314                       0    0 "   COLUMN txhousingway_detail.confirm    COMMENT     [   COMMENT ON COLUMN public.txhousingway_detail.confirm IS 'Confirmacion de sincronizacion ';
            public       postgres    false    314                       0    0 )   COLUMN txhousingway_detail.execution_date    COMMENT     V   COMMENT ON COLUMN public.txhousingway_detail.execution_date IS 'Fecha de ejecución';
            public       postgres    false    314                       0    0 -   COLUMN txhousingway_detail.execution_quantity    COMMENT     Z   COMMENT ON COLUMN public.txhousingway_detail.execution_quantity IS 'Cantidad a ejecutar';
            public       postgres    false    314                       0    0    COLUMN txhousingway_detail.lot    COMMENT     I   COMMENT ON COLUMN public.txhousingway_detail.lot IS 'Lote seleccionado';
            public       postgres    false    314                       0    0 -   COLUMN txhousingway_detail.incubator_plant_id    COMMENT     a   COMMENT ON COLUMN public.txhousingway_detail.incubator_plant_id IS 'Id de la planta incubadora';
            public       postgres    false    314            ;           1259    28747    txlot    TABLE     n  CREATE TABLE public.txlot (
    lot_id integer DEFAULT nextval('public.lot_id_seq'::regclass) NOT NULL,
    lot_code character varying(20) NOT NULL,
    lot_origin character varying(150),
    status integer,
    proyected_date date,
    sheduled_date date,
    proyected_quantity integer,
    sheduled_quantity integer,
    released_quantity integer,
    product_id integer NOT NULL,
    breed_id integer NOT NULL,
    gender character varying(30),
    type_posture character varying(30),
    shed_id integer NOT NULL,
    origin character varying(30),
    farm_id integer NOT NULL,
    housing_way_id integer NOT NULL
);
    DROP TABLE public.txlot;
       public         postgres    false    244    3                       0    0    TABLE txlot    COMMENT     T   COMMENT ON TABLE public.txlot IS 'Almacena la informacion de los diferentes lotes';
            public       postgres    false    315                       0    0    COLUMN txlot.lot_id    COMMENT     8   COMMENT ON COLUMN public.txlot.lot_id IS 'Id del lote';
            public       postgres    false    315                       0    0    COLUMN txlot.lot_code    COMMENT     >   COMMENT ON COLUMN public.txlot.lot_code IS 'Codigo del lote';
            public       postgres    false    315                       0    0    COLUMN txlot.lot_origin    COMMENT     @   COMMENT ON COLUMN public.txlot.lot_origin IS 'Origen del lote';
            public       postgres    false    315                       0    0    COLUMN txlot.status    COMMENT     <   COMMENT ON COLUMN public.txlot.status IS 'Estado del lote';
            public       postgres    false    315                       0    0    COLUMN txlot.proyected_date    COMMENT     E   COMMENT ON COLUMN public.txlot.proyected_date IS 'Fecha proyectada';
            public       postgres    false    315                       0    0    COLUMN txlot.sheduled_date    COMMENT     D   COMMENT ON COLUMN public.txlot.sheduled_date IS 'Fecha programada';
            public       postgres    false    315                       0    0    COLUMN txlot.proyected_quantity    COMMENT     L   COMMENT ON COLUMN public.txlot.proyected_quantity IS 'Cantidad proyectada';
            public       postgres    false    315                       0    0    COLUMN txlot.sheduled_quantity    COMMENT     K   COMMENT ON COLUMN public.txlot.sheduled_quantity IS 'Cantidad programada';
            public       postgres    false    315                       0    0    COLUMN txlot.released_quantity    COMMENT     I   COMMENT ON COLUMN public.txlot.released_quantity IS 'Cantidad liberada';
            public       postgres    false    315                        0    0    COLUMN txlot.product_id    COMMENT     @   COMMENT ON COLUMN public.txlot.product_id IS 'Id del producto';
            public       postgres    false    315            !           0    0    COLUMN txlot.breed_id    COMMENT     <   COMMENT ON COLUMN public.txlot.breed_id IS 'Id de la raza';
            public       postgres    false    315            "           0    0    COLUMN txlot.gender    COMMENT     <   COMMENT ON COLUMN public.txlot.gender IS 'Genero del lote';
            public       postgres    false    315            #           0    0    COLUMN txlot.type_posture    COMMENT     B   COMMENT ON COLUMN public.txlot.type_posture IS 'Tipo de postura';
            public       postgres    false    315            $           0    0    COLUMN txlot.shed_id    COMMENT     ;   COMMENT ON COLUMN public.txlot.shed_id IS 'Id del galpon';
            public       postgres    false    315            %           0    0    COLUMN txlot.origin    COMMENT     3   COMMENT ON COLUMN public.txlot.origin IS 'Origen';
            public       postgres    false    315            &           0    0    COLUMN txlot.farm_id    COMMENT     =   COMMENT ON COLUMN public.txlot.farm_id IS 'Id de la granja';
            public       postgres    false    315            '           0    0    COLUMN txlot.housing_way_id    COMMENT     ~   COMMENT ON COLUMN public.txlot.housing_way_id IS 'Id del almacenamientos de la proyecciones de levante, cria y reproductora';
            public       postgres    false    315            <           1259    28751 
   txlot_eggs    TABLE     �   CREATE TABLE public.txlot_eggs (
    lot_eggs_id integer DEFAULT nextval('public.lot_eggs_id_seq'::regclass) NOT NULL,
    theorical_performance double precision,
    week_date date,
    week integer
);
    DROP TABLE public.txlot_eggs;
       public         postgres    false    242    3            (           0    0    TABLE txlot_eggs    COMMENT     S   COMMENT ON TABLE public.txlot_eggs IS 'Almacena los datos de los lotes de huevos';
            public       postgres    false    316            )           0    0    COLUMN txlot_eggs.lot_eggs_id    COMMENT     L   COMMENT ON COLUMN public.txlot_eggs.lot_eggs_id IS 'Id del lote de huevos';
            public       postgres    false    316            *           0    0 '   COLUMN txlot_eggs.theorical_performance    COMMENT     T   COMMENT ON COLUMN public.txlot_eggs.theorical_performance IS 'Rendimiento teorico';
            public       postgres    false    316            +           0    0    COLUMN txlot_eggs.week_date    COMMENT     G   COMMENT ON COLUMN public.txlot_eggs.week_date IS 'Fecha de la semana';
            public       postgres    false    316            ,           0    0    COLUMN txlot_eggs.week    COMMENT     6   COMMENT ON COLUMN public.txlot_eggs.week IS 'Semana';
            public       postgres    false    316            =           1259    28755    txposturecurve    TABLE     �  CREATE TABLE public.txposturecurve (
    posture_curve_id integer DEFAULT nextval('public.posture_curve_id_seq'::regclass) NOT NULL,
    week integer NOT NULL,
    breed_id integer NOT NULL,
    theorical_performance double precision NOT NULL,
    historical_performance double precision,
    theorical_accum_mortality integer,
    historical_accum_mortality integer,
    theorical_uniformity double precision,
    historical_uniformity double precision,
    type_posture character varying(30) NOT NULL
);
 "   DROP TABLE public.txposturecurve;
       public         postgres    false    287    3            -           0    0    TABLE txposturecurve    COMMENT        COMMENT ON TABLE public.txposturecurve IS 'Almacena la información de la curva de postura por cada raza separada por semana';
            public       postgres    false    317            .           0    0 &   COLUMN txposturecurve.posture_curve_id    COMMENT     Y   COMMENT ON COLUMN public.txposturecurve.posture_curve_id IS 'Id de la curva de postura';
            public       postgres    false    317            /           0    0    COLUMN txposturecurve.week    COMMENT     _   COMMENT ON COLUMN public.txposturecurve.week IS 'Semana en la que inicia la curva de postura';
            public       postgres    false    317            0           0    0    COLUMN txposturecurve.breed_id    COMMENT     P   COMMENT ON COLUMN public.txposturecurve.breed_id IS 'Identificador de la raza';
            public       postgres    false    317            1           0    0 +   COLUMN txposturecurve.theorical_performance    COMMENT     X   COMMENT ON COLUMN public.txposturecurve.theorical_performance IS 'Desempeño teórico';
            public       postgres    false    317            2           0    0 ,   COLUMN txposturecurve.historical_performance    COMMENT     [   COMMENT ON COLUMN public.txposturecurve.historical_performance IS 'Desempeño histórico';
            public       postgres    false    317            3           0    0 /   COLUMN txposturecurve.theorical_accum_mortality    COMMENT     h   COMMENT ON COLUMN public.txposturecurve.theorical_accum_mortality IS 'Acumulado de mortalidad teorico';
            public       postgres    false    317            4           0    0 0   COLUMN txposturecurve.historical_accum_mortality    COMMENT     k   COMMENT ON COLUMN public.txposturecurve.historical_accum_mortality IS 'Acumulado de mortalidad historico';
            public       postgres    false    317            5           0    0 *   COLUMN txposturecurve.theorical_uniformity    COMMENT     W   COMMENT ON COLUMN public.txposturecurve.theorical_uniformity IS 'Uniformidad teorica';
            public       postgres    false    317            6           0    0 +   COLUMN txposturecurve.historical_uniformity    COMMENT     Z   COMMENT ON COLUMN public.txposturecurve.historical_uniformity IS 'Uniformidad historica';
            public       postgres    false    317            7           0    0 "   COLUMN txposturecurve.type_posture    COMMENT     K   COMMENT ON COLUMN public.txposturecurve.type_posture IS 'Tipo de postura';
            public       postgres    false    317            >           1259    28759    txprogrammed_eggs    TABLE     �  CREATE TABLE public.txprogrammed_eggs (
    programmed_eggs_id integer DEFAULT nextval('public.programmed_eggs_id_seq'::regclass) NOT NULL,
    incubator_id integer NOT NULL,
    lot_breed character varying(45),
    lot_incubator character varying(45),
    use_date date,
    eggs integer,
    breed_id integer NOT NULL,
    execution_quantity integer,
    eggs_storage_id integer NOT NULL,
    confirm integer,
    released boolean,
    eggs_movements_id integer
);
 %   DROP TABLE public.txprogrammed_eggs;
       public         postgres    false    290    3            8           0    0    TABLE txprogrammed_eggs    COMMENT        COMMENT ON TABLE public.txprogrammed_eggs IS 'Almacena la proyección, programación y ejecución del módulo de incubadoras';
            public       postgres    false    318            9           0    0 +   COLUMN txprogrammed_eggs.programmed_eggs_id    COMMENT     j   COMMENT ON COLUMN public.txprogrammed_eggs.programmed_eggs_id IS 'Id de las programacion de incubadoras';
            public       postgres    false    318            :           0    0 %   COLUMN txprogrammed_eggs.incubator_id    COMMENT     O   COMMENT ON COLUMN public.txprogrammed_eggs.incubator_id IS 'Id de incubadora';
            public       postgres    false    318            ;           0    0 "   COLUMN txprogrammed_eggs.lot_breed    COMMENT     I   COMMENT ON COLUMN public.txprogrammed_eggs.lot_breed IS 'Lote por raza';
            public       postgres    false    318            <           0    0 &   COLUMN txprogrammed_eggs.lot_incubator    COMMENT     S   COMMENT ON COLUMN public.txprogrammed_eggs.lot_incubator IS 'Lote de incubadoras';
            public       postgres    false    318            =           0    0    COLUMN txprogrammed_eggs.eggs    COMMENT     I   COMMENT ON COLUMN public.txprogrammed_eggs.eggs IS 'Cantidad de huevos';
            public       postgres    false    318            >           0    0 !   COLUMN txprogrammed_eggs.breed_id    COMMENT     E   COMMENT ON COLUMN public.txprogrammed_eggs.breed_id IS 'Id de raza';
            public       postgres    false    318            ?           0    0 +   COLUMN txprogrammed_eggs.execution_quantity    COMMENT     [   COMMENT ON COLUMN public.txprogrammed_eggs.execution_quantity IS 'Cantidad de ejecución';
            public       postgres    false    318            ?           1259    28763    txscenarioformula    TABLE     �  CREATE TABLE public.txscenarioformula (
    scenario_formula_id integer DEFAULT nextval('public.scenario_formula_id_seq'::regclass) NOT NULL,
    process_id integer NOT NULL,
    predecessor_id integer NOT NULL,
    parameter_id integer NOT NULL,
    sign integer,
    divider double precision,
    duration integer,
    scenario_id integer NOT NULL,
    measure_id integer NOT NULL
);
 %   DROP TABLE public.txscenarioformula;
       public         postgres    false    292    3            @           0    0    TABLE txscenarioformula    COMMENT     �   COMMENT ON TABLE public.txscenarioformula IS 'Almacena los datos para la formulación de salida de la planificación regresiva';
            public       postgres    false    319            A           0    0 ,   COLUMN txscenarioformula.scenario_formula_id    COMMENT     d   COMMENT ON COLUMN public.txscenarioformula.scenario_formula_id IS 'Id de la formula del escenario';
            public       postgres    false    319            B           0    0 #   COLUMN txscenarioformula.process_id    COMMENT     K   COMMENT ON COLUMN public.txscenarioformula.process_id IS 'Id del proceso';
            public       postgres    false    319            C           0    0 '   COLUMN txscenarioformula.predecessor_id    COMMENT     R   COMMENT ON COLUMN public.txscenarioformula.predecessor_id IS 'Id del predecesor';
            public       postgres    false    319            D           0    0 %   COLUMN txscenarioformula.parameter_id    COMMENT     O   COMMENT ON COLUMN public.txscenarioformula.parameter_id IS 'Id del parametro';
            public       postgres    false    319            E           0    0    COLUMN txscenarioformula.sign    COMMENT     E   COMMENT ON COLUMN public.txscenarioformula.sign IS 'Firma de datos';
            public       postgres    false    319            F           0    0     COLUMN txscenarioformula.divider    COMMENT     J   COMMENT ON COLUMN public.txscenarioformula.divider IS 'divisor de datos';
            public       postgres    false    319            G           0    0 !   COLUMN txscenarioformula.duration    COMMENT     Q   COMMENT ON COLUMN public.txscenarioformula.duration IS 'Duracion de la formula';
            public       postgres    false    319            H           0    0 $   COLUMN txscenarioformula.scenario_id    COMMENT     N   COMMENT ON COLUMN public.txscenarioformula.scenario_id IS 'Id del escenario';
            public       postgres    false    319            I           0    0 #   COLUMN txscenarioformula.measure_id    COMMENT     M   COMMENT ON COLUMN public.txscenarioformula.measure_id IS 'Id de la medida
';
            public       postgres    false    319            @           1259    28767    txscenarioparameter    TABLE     c  CREATE TABLE public.txscenarioparameter (
    scenario_parameter_id integer DEFAULT nextval('public.scenario_parameter_id_seq'::regclass) NOT NULL,
    process_id integer NOT NULL,
    parameter_id integer NOT NULL,
    use_year integer,
    use_month integer,
    use_value integer,
    scenario_id integer NOT NULL,
    value_units integer DEFAULT 0
);
 '   DROP TABLE public.txscenarioparameter;
       public         postgres    false    294    3            J           0    0    TABLE txscenarioparameter    COMMENT     s   COMMENT ON TABLE public.txscenarioparameter IS 'Almacena las metas de producción ingresadas para los escenarios';
            public       postgres    false    320            K           0    0 0   COLUMN txscenarioparameter.scenario_parameter_id    COMMENT     l   COMMENT ON COLUMN public.txscenarioparameter.scenario_parameter_id IS 'Id de los parametros del escenario';
            public       postgres    false    320            L           0    0 %   COLUMN txscenarioparameter.process_id    COMMENT     M   COMMENT ON COLUMN public.txscenarioparameter.process_id IS 'Id del proceso';
            public       postgres    false    320            M           0    0 '   COLUMN txscenarioparameter.parameter_id    COMMENT     Q   COMMENT ON COLUMN public.txscenarioparameter.parameter_id IS 'Id del parametro';
            public       postgres    false    320            N           0    0 #   COLUMN txscenarioparameter.use_year    COMMENT     O   COMMENT ON COLUMN public.txscenarioparameter.use_year IS 'Año del parametro';
            public       postgres    false    320            O           0    0 $   COLUMN txscenarioparameter.use_month    COMMENT     O   COMMENT ON COLUMN public.txscenarioparameter.use_month IS 'Mes del parametro';
            public       postgres    false    320            P           0    0 $   COLUMN txscenarioparameter.use_value    COMMENT     Q   COMMENT ON COLUMN public.txscenarioparameter.use_value IS 'Valor del parametro';
            public       postgres    false    320            Q           0    0 &   COLUMN txscenarioparameter.scenario_id    COMMENT     P   COMMENT ON COLUMN public.txscenarioparameter.scenario_id IS 'Id del escenario';
            public       postgres    false    320            R           0    0 &   COLUMN txscenarioparameter.value_units    COMMENT     U   COMMENT ON COLUMN public.txscenarioparameter.value_units IS 'Valor de las unidades';
            public       postgres    false    320            A           1259    28772    txscenarioparameterday    TABLE     {  CREATE TABLE public.txscenarioparameterday (
    scenario_parameter_day_id integer DEFAULT nextval('public.scenario_parameter_day_seq'::regclass) NOT NULL,
    use_day integer,
    parameter_id integer NOT NULL,
    units_day integer,
    scenario_id integer NOT NULL,
    sequence integer,
    use_month integer,
    use_year integer,
    week_day integer,
    use_week date
);
 *   DROP TABLE public.txscenarioparameterday;
       public         postgres    false    293    3            S           0    0    TABLE txscenarioparameterday    COMMENT     V   COMMENT ON TABLE public.txscenarioparameterday IS 'Almcacena los parametros por dia';
            public       postgres    false    321            T           0    0 7   COLUMN txscenarioparameterday.scenario_parameter_day_id    COMMENT     m   COMMENT ON COLUMN public.txscenarioparameterday.scenario_parameter_day_id IS 'Id de los parametros del dia';
            public       postgres    false    321            U           0    0 %   COLUMN txscenarioparameterday.use_day    COMMENT     B   COMMENT ON COLUMN public.txscenarioparameterday.use_day IS 'Dia';
            public       postgres    false    321            V           0    0 *   COLUMN txscenarioparameterday.parameter_id    COMMENT     c   COMMENT ON COLUMN public.txscenarioparameterday.parameter_id IS 'Id de los parametros necesarios';
            public       postgres    false    321            W           0    0 '   COLUMN txscenarioparameterday.units_day    COMMENT     U   COMMENT ON COLUMN public.txscenarioparameterday.units_day IS 'Cantidad de material';
            public       postgres    false    321            X           0    0 )   COLUMN txscenarioparameterday.scenario_id    COMMENT     u   COMMENT ON COLUMN public.txscenarioparameterday.scenario_id IS 'Escenario al cual pertenece el scanrioparameterday';
            public       postgres    false    321            Y           0    0 &   COLUMN txscenarioparameterday.sequence    COMMENT     R   COMMENT ON COLUMN public.txscenarioparameterday.sequence IS 'Secuencia del dia
';
            public       postgres    false    321            Z           0    0 '   COLUMN txscenarioparameterday.use_month    COMMENT     ]   COMMENT ON COLUMN public.txscenarioparameterday.use_month IS 'Mes en que se ubica el día ';
            public       postgres    false    321            [           0    0 &   COLUMN txscenarioparameterday.use_year    COMMENT     ]   COMMENT ON COLUMN public.txscenarioparameterday.use_year IS 'Año en que se ubica el día ';
            public       postgres    false    321            \           0    0 &   COLUMN txscenarioparameterday.week_day    COMMENT     P   COMMENT ON COLUMN public.txscenarioparameterday.week_day IS 'Dia de la semana';
            public       postgres    false    321            ]           0    0 &   COLUMN txscenarioparameterday.use_week    COMMENT     F   COMMENT ON COLUMN public.txscenarioparameterday.use_week IS 'Semana';
            public       postgres    false    321            B           1259    28776    txscenarioposturecurve    TABLE     3  CREATE TABLE public.txscenarioposturecurve (
    scenario_posture_id integer DEFAULT nextval('public.scenario_posture_id_seq'::regclass) NOT NULL,
    posture_date date,
    eggs double precision,
    scenario_id integer NOT NULL,
    housingway_detail_id integer NOT NULL,
    breed_id integer NOT NULL
);
 *   DROP TABLE public.txscenarioposturecurve;
       public         postgres    false    295    3            ^           0    0    TABLE txscenarioposturecurve    COMMENT     o   COMMENT ON TABLE public.txscenarioposturecurve IS 'Almacena los datos que se utilizan en la curva de postura';
            public       postgres    false    322            _           0    0 1   COLUMN txscenarioposturecurve.scenario_posture_id    COMMENT     i   COMMENT ON COLUMN public.txscenarioposturecurve.scenario_posture_id IS 'Id de la postura del escenario';
            public       postgres    false    322            `           0    0 *   COLUMN txscenarioposturecurve.posture_date    COMMENT     W   COMMENT ON COLUMN public.txscenarioposturecurve.posture_date IS 'Fecha de la postura';
            public       postgres    false    322            a           0    0 "   COLUMN txscenarioposturecurve.eggs    COMMENT     N   COMMENT ON COLUMN public.txscenarioposturecurve.eggs IS 'Cantidad de huevos';
            public       postgres    false    322            b           0    0 )   COLUMN txscenarioposturecurve.scenario_id    COMMENT     R   COMMENT ON COLUMN public.txscenarioposturecurve.scenario_id IS 'Id del scenario';
            public       postgres    false    322            c           0    0 2   COLUMN txscenarioposturecurve.housingway_detail_id    COMMENT     �   COMMENT ON COLUMN public.txscenarioposturecurve.housingway_detail_id IS 'Id de la programación y ejecución de los modelos de levante y cría y reproductora';
            public       postgres    false    322            d           0    0 &   COLUMN txscenarioposturecurve.breed_id    COMMENT     M   COMMENT ON COLUMN public.txscenarioposturecurve.breed_id IS 'Id de la raza';
            public       postgres    false    322            C           1259    28780    txscenarioprocess    TABLE     4  CREATE TABLE public.txscenarioprocess (
    scenario_process_id integer DEFAULT nextval('public.scenario_process_id_seq'::regclass) NOT NULL,
    process_id integer NOT NULL,
    decrease_goal double precision,
    weight_goal double precision,
    duration_goal integer,
    scenario_id integer NOT NULL
);
 %   DROP TABLE public.txscenarioprocess;
       public         postgres    false    296    3            e           0    0    TABLE txscenarioprocess    COMMENT     m   COMMENT ON TABLE public.txscenarioprocess IS 'Almacena los procesos asociados a cada uno de los escenarios';
            public       postgres    false    323            f           0    0 ,   COLUMN txscenarioprocess.scenario_process_id    COMMENT     a   COMMENT ON COLUMN public.txscenarioprocess.scenario_process_id IS 'Id del proceso de escenario';
            public       postgres    false    323            g           0    0 #   COLUMN txscenarioprocess.process_id    COMMENT     V   COMMENT ON COLUMN public.txscenarioprocess.process_id IS 'Id del proceso a utilizar';
            public       postgres    false    323            h           0    0 &   COLUMN txscenarioprocess.decrease_goal    COMMENT     v   COMMENT ON COLUMN public.txscenarioprocess.decrease_goal IS 'Guarda los datos de la merma historia en dicho proceso';
            public       postgres    false    323            i           0    0 $   COLUMN txscenarioprocess.weight_goal    COMMENT     q   COMMENT ON COLUMN public.txscenarioprocess.weight_goal IS 'Guarda los datos del peso historio en dicho proceso';
            public       postgres    false    323            j           0    0 &   COLUMN txscenarioprocess.duration_goal    COMMENT     y   COMMENT ON COLUMN public.txscenarioprocess.duration_goal IS 'Guarda los datos de la duracion historia en dicho proceso';
            public       postgres    false    323            k           0    0 $   COLUMN txscenarioprocess.scenario_id    COMMENT     X   COMMENT ON COLUMN public.txscenarioprocess.scenario_id IS 'Id del escenario utilizado';
            public       postgres    false    323            D           1259    28784 #   user_application_application_id_seq    SEQUENCE     �   CREATE SEQUENCE public.user_application_application_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 :   DROP SEQUENCE public.user_application_application_id_seq;
       public       postgres    false    3            E           1259    28786     user_application_user_app_id_seq    SEQUENCE     �   CREATE SEQUENCE public.user_application_user_app_id_seq
    START WITH 215
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 7   DROP SEQUENCE public.user_application_user_app_id_seq;
       public       postgres    false    3            F           1259    28788    user_application_user_id_seq    SEQUENCE     �   CREATE SEQUENCE public.user_application_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 3   DROP SEQUENCE public.user_application_user_id_seq;
       public       postgres    false    3                       2604    28790    txgoals_erp goals_erp_id    DEFAULT     �   ALTER TABLE ONLY public.txgoals_erp ALTER COLUMN goals_erp_id SET DEFAULT nextval('public.txgoals_erp_goals_erp_id_seq'::regclass);
 G   ALTER TABLE public.txgoals_erp ALTER COLUMN goals_erp_id DROP DEFAULT;
       public       postgres    false    312    311            !          0    28393    aba_breeds_and_stages 
   TABLE DATA               m   COPY public.aba_breeds_and_stages (id, code, name, id_aba_consumption_and_mortality, id_process) FROM stdin;
    public       postgres    false    198   ��      #          0    28399    aba_consumption_and_mortality 
   TABLE DATA               m   COPY public.aba_consumption_and_mortality (id, code, name, id_breed, id_stage, id_aba_time_unit) FROM stdin;
    public       postgres    false    200   �      %          0    28405 $   aba_consumption_and_mortality_detail 
   TABLE DATA               �   COPY public.aba_consumption_and_mortality_detail (id, id_aba_consumption_and_mortality, time_unit_number, consumption, mortality) FROM stdin;
    public       postgres    false    202   *�      '          0    28411    aba_elements 
   TABLE DATA               d   COPY public.aba_elements (id, code, name, id_aba_element_property, equivalent_quantity) FROM stdin;
    public       postgres    false    204   G�      )          0    28417    aba_elements_and_concentrations 
   TABLE DATA               �   COPY public.aba_elements_and_concentrations (id, id_aba_element, id_aba_formulation, proportion, id_element_equivalent, id_aba_element_property, equivalent_quantity) FROM stdin;
    public       postgres    false    206   d�      +          0    28423    aba_elements_properties 
   TABLE DATA               A   COPY public.aba_elements_properties (id, code, name) FROM stdin;
    public       postgres    false    208   ��      -          0    28429    aba_formulation 
   TABLE DATA               9   COPY public.aba_formulation (id, code, name) FROM stdin;
    public       postgres    false    210   ��      /          0    28435    aba_results 
   TABLE DATA               C   COPY public.aba_results (id, id_aba_element, quantity) FROM stdin;
    public       postgres    false    212   ��      1          0    28441    aba_stages_of_breeds_and_stages 
   TABLE DATA               w   COPY public.aba_stages_of_breeds_and_stages (id, id_aba_breeds_and_stages, id_formulation, name, duration) FROM stdin;
    public       postgres    false    214   ��      2          0    28445    aba_time_unit 
   TABLE DATA               G   COPY public.aba_time_unit (id, singular_name, plural_name) FROM stdin;
    public       postgres    false    215   �      R          0    28511    mdapplication 
   TABLE DATA               O   COPY public.mdapplication (application_id, application_name, type) FROM stdin;
    public       postgres    false    247   N�      T          0    28517    mdapplication_rol 
   TABLE DATA               G   COPY public.mdapplication_rol (id, application_id, rol_id) FROM stdin;
    public       postgres    false    249   :�      U          0    28521    mdbreed 
   TABLE DATA               7   COPY public.mdbreed (breed_id, code, name) FROM stdin;
    public       postgres    false    250   ��      V          0    28525    mdbroiler_product 
   TABLE DATA               \   COPY public.mdbroiler_product (broiler_product_id, name, days_eviction, weight) FROM stdin;
    public       postgres    false    251   ��      W          0    28529 
   mdfarmtype 
   TABLE DATA               8   COPY public.mdfarmtype (farm_type_id, name) FROM stdin;
    public       postgres    false    252   ��      Y          0    28535 	   mdmeasure 
   TABLE DATA               b   COPY public.mdmeasure (measure_id, name, abbreviation, originvalue, valuekg, is_unit) FROM stdin;
    public       postgres    false    254   /�      [          0    28541    mdparameter 
   TABLE DATA               d   COPY public.mdparameter (parameter_id, description, type, measure_id, process_id, name) FROM stdin;
    public       postgres    false    256   `�      ]          0    28550 	   mdprocess 
   TABLE DATA               J  COPY public.mdprocess (process_id, process_order, product_id, stage_id, historical_decrease, theoretical_decrease, historical_weight, theoretical_weight, historical_duration, theoretical_duration, calendar_id, visible, name, predecessor_id, capacity, breed_id, gender, fattening_goal, type_posture, biological_active) FROM stdin;
    public       postgres    false    258   F�      _          0    28556 	   mdproduct 
   TABLE DATA               ;   COPY public.mdproduct (product_id, code, name) FROM stdin;
    public       postgres    false    260   ��      a          0    28562    mdrol 
   TABLE DATA               T   COPY public.mdrol (rol_id, rol_name, admin_user_creator, creation_date) FROM stdin;
    public       postgres    false    262   �      c          0    28568 
   mdscenario 
   TABLE DATA               o   COPY public.mdscenario (scenario_id, description, date_start, date_end, name, status, calendar_id) FROM stdin;
    public       postgres    false    264   U�      e          0    28578    mdshedstatus 
   TABLE DATA               I   COPY public.mdshedstatus (shed_status_id, name, description) FROM stdin;
    public       postgres    false    266   ��      g          0    28584    mdstage 
   TABLE DATA               9   COPY public.mdstage (stage_id, order_, name) FROM stdin;
    public       postgres    false    268   \�      i          0    28590    mduser 
   TABLE DATA                  COPY public.mduser (user_id, username, password, name, lastname, active, admi_user_creator, rol_id, creation_date) FROM stdin;
    public       postgres    false    270   ��      j          0    28594    oscenter 
   TABLE DATA               [   COPY public.oscenter (center_id, partnership_id, farm_id, name, code, "order") FROM stdin;
    public       postgres    false    271   E�      k          0    28598    oscenter_oswarehouse 
   TABLE DATA               x   COPY public.oscenter_oswarehouse (client_id, partnership_id, farm_id, center_id, warehouse_id, delete_mark) FROM stdin;
    public       postgres    false    272   ��      l          0    28601    osfarm 
   TABLE DATA               \   COPY public.osfarm (farm_id, partnership_id, code, name, farm_type_id, "order") FROM stdin;
    public       postgres    false    273   ��      m          0    28605    osincubator 
   TABLE DATA               �   COPY public.osincubator (incubator_id, incubator_plant_id, name, code, description, capacity, sunday, monday, tuesday, wednesday, thursday, friday, saturday, available) FROM stdin;
    public       postgres    false    274   �      n          0    28609    osincubatorplant 
   TABLE DATA               �   COPY public.osincubatorplant (incubator_plant_id, name, code, description, partnership_id, max_storage, min_storage, acclimatized, suitable, expired) FROM stdin;
    public       postgres    false    275   u�      p          0    28615    ospartnership 
   TABLE DATA               Y   COPY public.ospartnership (partnership_id, name, address, description, code) FROM stdin;
    public       postgres    false    277   ��      r          0    28624    osshed 
   TABLE DATA               /  COPY public.osshed (shed_id, partnership_id, farm_id, center_id, code, statusshed_id, type_id, building_date, stall_width, stall_height, capacity_min, capacity_max, environment_id, rotation_days, nests_quantity, cages_quantity, birds_quantity, capacity_theoretical, avaliable_date, "order") FROM stdin;
    public       postgres    false    279   �      t          0    28635    ossilo 
   TABLE DATA               ?  COPY public.ossilo (silo_id, client_id, partnership_id, farm_id, center_id, name, rings_height, rings_height_id, height, height_unit_id, diameter, diameter_unit_id, total_rings_quantity, measuring_mechanism, cone_degrees, total_capacity_1, total_capacity_2, capacity_unit_id_1, capacity_unit_id_2, central) FROM stdin;
    public       postgres    false    281   ��      u          0    28639    ossilo_osshed 
   TABLE DATA               v   COPY public.ossilo_osshed (silo_id, shed_id, center_id, farm_id, partnership_id, client_id, deleted_mark) FROM stdin;
    public       postgres    false    282   ��      w          0    28644    osslaughterhouse 
   TABLE DATA               i   COPY public.osslaughterhouse (slaughterhouse_id, name, address, description, code, capacity) FROM stdin;
    public       postgres    false    284   ��      y          0    28653    oswarehouse 
   TABLE DATA               X   COPY public.oswarehouse (warehouse_id, partnership_id, farm_id, name, code) FROM stdin;
    public       postgres    false    286   K�      �          0    28677    txavailabilitysheds 
   TABLE DATA               k   COPY public.txavailabilitysheds (availability_shed_id, shed_id, init_date, end_date, lot_code) FROM stdin;
    public       postgres    false    297   h�      �          0    28681 	   txbroiler 
   TABLE DATA               �   COPY public.txbroiler (broiler_id, projected_date, projected_quantity, partnership_id, scenario_id, breed_id, lot_incubator, programmed_eggs_id) FROM stdin;
    public       postgres    false    298   ��      �          0    28685    txbroiler_detail 
   TABLE DATA                 COPY public.txbroiler_detail (broiler_detail_id, broiler_id, scheduled_date, scheduled_quantity, farm_id, shed_id, confirm, execution_date, execution_quantity, lot, broiler_product_id, center_id, executionfarm_id, executioncenter_id, executionshed_id) FROM stdin;
    public       postgres    false    299   ��      �          0    28689    txbroilereviction 
   TABLE DATA               �   COPY public.txbroilereviction (broilereviction_id, projected_date, projected_quantity, partnership_id, scenario_id, breed_id, lot_incubator, broiler_detail_id) FROM stdin;
    public       postgres    false    300   ��      �          0    28693    txbroilereviction_detail 
   TABLE DATA                 COPY public.txbroilereviction_detail (broilereviction_detail_id, broilereviction_id, scheduled_date, scheduled_quantity, farm_id, shed_id, confirm, execution_date, execution_quantity, lot, broiler_product_id, slaughterhouse_id, center_id, executionslaughterhouse_id) FROM stdin;
    public       postgres    false    301   ��      �          0    28697    txbroilerproduct_detail 
   TABLE DATA               y   COPY public.txbroilerproduct_detail (broilerproduct_detail_id, broiler_detail, broiler_product_id, quantity) FROM stdin;
    public       postgres    false    302   ��      �          0    28701    txbroodermachine 
   TABLE DATA               �   COPY public.txbroodermachine (brooder_machine_id_seq, partnership_id, farm_id, capacity, sunday, monday, tuesday, wednesday, thursday, friday, saturday, name) FROM stdin;
    public       postgres    false    303   �      �          0    28705 
   txcalendar 
   TABLE DATA               �   COPY public.txcalendar (calendar_id, description, saturday, sunday, week_start, code, year_start, year_end, generated) FROM stdin;
    public       postgres    false    304   3�      �          0    28709    txcalendarday 
   TABLE DATA               �   COPY public.txcalendarday (calendar_day_id, calendar_id, use_date, use_year, use_month, use_day, use_week, week_day, sequence, working_day) FROM stdin;
    public       postgres    false    305   �      �          0    28715    txeggs_movements 
   TABLE DATA               �   COPY public.txeggs_movements (eggs_movements_id, fecha_movements, lot, quantity, type_movements, eggs_storage_id, description_adjustment) FROM stdin;
    public       postgres    false    307   :e      �          0    28722    txeggs_planning 
   TABLE DATA               y   COPY public.txeggs_planning (egg_planning_id, month_planning, year_planning, scenario_id, planned, breed_id) FROM stdin;
    public       postgres    false    308   We      �          0    28726    txeggs_required 
   TABLE DATA               p   COPY public.txeggs_required (egg_required_id, use_month, use_year, scenario_id, required, breed_id) FROM stdin;
    public       postgres    false    309   Bg      �          0    28730    txeggs_storage 
   TABLE DATA               �   COPY public.txeggs_storage (eggs_storage_id, incubator_plant_id, scenario_id, breed_id, init_date, end_date, lot, eggs, eggs_executed) FROM stdin;
    public       postgres    false    310   Vj      �          0    28734    txgoals_erp 
   TABLE DATA               g   COPY public.txgoals_erp (goals_erp_id, use_week, use_value, product_id, code, scenario_id) FROM stdin;
    public       postgres    false    311   ��      �          0    28739    txhousingway 
   TABLE DATA               �   COPY public.txhousingway (housing_way_id, projected_quantity, projected_date, stage_id, partnership_id, scenario_id, breed_id, predecessor_id) FROM stdin;
    public       postgres    false    313   ��      �          0    28743    txhousingway_detail 
   TABLE DATA               .  COPY public.txhousingway_detail (housingway_detail_id, housing_way_id, scheduled_date, scheduled_quantity, farm_id, shed_id, confirm, execution_date, execution_quantity, lot, incubator_plant_id, center_id, executionfarm_id, executioncenter_id, executionshed_id, executionincubatorplant_id) FROM stdin;
    public       postgres    false    314   3�      �          0    28747    txlot 
   TABLE DATA               �   COPY public.txlot (lot_id, lot_code, lot_origin, status, proyected_date, sheduled_date, proyected_quantity, sheduled_quantity, released_quantity, product_id, breed_id, gender, type_posture, shed_id, origin, farm_id, housing_way_id) FROM stdin;
    public       postgres    false    315   J�      �          0    28751 
   txlot_eggs 
   TABLE DATA               Y   COPY public.txlot_eggs (lot_eggs_id, theorical_performance, week_date, week) FROM stdin;
    public       postgres    false    316   g�      �          0    28755    txposturecurve 
   TABLE DATA               �   COPY public.txposturecurve (posture_curve_id, week, breed_id, theorical_performance, historical_performance, theorical_accum_mortality, historical_accum_mortality, theorical_uniformity, historical_uniformity, type_posture) FROM stdin;
    public       postgres    false    317   ��      �          0    28759    txprogrammed_eggs 
   TABLE DATA               �   COPY public.txprogrammed_eggs (programmed_eggs_id, incubator_id, lot_breed, lot_incubator, use_date, eggs, breed_id, execution_quantity, eggs_storage_id, confirm, released, eggs_movements_id) FROM stdin;
    public       postgres    false    318   $      �          0    28763    txscenarioformula 
   TABLE DATA               �   COPY public.txscenarioformula (scenario_formula_id, process_id, predecessor_id, parameter_id, sign, divider, duration, scenario_id, measure_id) FROM stdin;
    public       postgres    false    319   A      �          0    28767    txscenarioparameter 
   TABLE DATA               �   COPY public.txscenarioparameter (scenario_parameter_id, process_id, parameter_id, use_year, use_month, use_value, scenario_id, value_units) FROM stdin;
    public       postgres    false    320   �      �          0    28772    txscenarioparameterday 
   TABLE DATA               �   COPY public.txscenarioparameterday (scenario_parameter_day_id, use_day, parameter_id, units_day, scenario_id, sequence, use_month, use_year, week_day, use_week) FROM stdin;
    public       postgres    false    321   �      �          0    28776    txscenarioposturecurve 
   TABLE DATA               �   COPY public.txscenarioposturecurve (scenario_posture_id, posture_date, eggs, scenario_id, housingway_detail_id, breed_id) FROM stdin;
    public       postgres    false    322   5�      �          0    28780    txscenarioprocess 
   TABLE DATA               �   COPY public.txscenarioprocess (scenario_process_id, process_id, decrease_goal, weight_goal, duration_goal, scenario_id) FROM stdin;
    public       postgres    false    323   �?      l           0    0    abaTimeUnit_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public."abaTimeUnit_id_seq"', 2, false);
            public       postgres    false    196            m           0    0    aba_breeds_and_stages_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.aba_breeds_and_stages_id_seq', 8, false);
            public       postgres    false    197            n           0    0 +   aba_consumption_and_mortality_detail_id_seq    SEQUENCE SET     \   SELECT pg_catalog.setval('public.aba_consumption_and_mortality_detail_id_seq', 203, false);
            public       postgres    false    201            o           0    0 $   aba_consumption_and_mortality_id_seq    SEQUENCE SET     S   SELECT pg_catalog.setval('public.aba_consumption_and_mortality_id_seq', 8, false);
            public       postgres    false    199            p           0    0 &   aba_elements_and_concentrations_id_seq    SEQUENCE SET     V   SELECT pg_catalog.setval('public.aba_elements_and_concentrations_id_seq', 105, true);
            public       postgres    false    205            q           0    0    aba_elements_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.aba_elements_id_seq', 22, true);
            public       postgres    false    203            r           0    0    aba_elements_properties_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.aba_elements_properties_id_seq', 1, false);
            public       postgres    false    207            s           0    0    aba_formulation_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.aba_formulation_id_seq', 68, true);
            public       postgres    false    209            t           0    0    aba_results_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.aba_results_id_seq', 1, false);
            public       postgres    false    211            u           0    0 &   aba_stages_of_breeds_and_stages_id_seq    SEQUENCE SET     V   SELECT pg_catalog.setval('public.aba_stages_of_breeds_and_stages_id_seq', 24, false);
            public       postgres    false    213            v           0    0    availability_shed_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.availability_shed_id_seq', 301, true);
            public       postgres    false    216            w           0    0    base_day_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.base_day_id_seq', 3, true);
            public       postgres    false    217            x           0    0    breed_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.breed_id_seq', 17, true);
            public       postgres    false    218            y           0    0    broiler_detail_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.broiler_detail_id_seq', 67, true);
            public       postgres    false    219            z           0    0    broiler_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.broiler_id_seq', 142, true);
            public       postgres    false    220            {           0    0    broiler_product_detail_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.broiler_product_detail_id_seq', 2, true);
            public       postgres    false    221            |           0    0    broiler_product_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.broiler_product_id_seq', 21, true);
            public       postgres    false    222            }           0    0    broilereviction_detail_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.broilereviction_detail_id_seq', 140, true);
            public       postgres    false    223            ~           0    0    broilereviction_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.broilereviction_id_seq', 92, true);
            public       postgres    false    224                       0    0    brooder_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.brooder_id_seq', 52, true);
            public       postgres    false    225            �           0    0    brooder_machines_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.brooder_machines_id_seq', 7, true);
            public       postgres    false    226            �           0    0    calendar_day_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.calendar_day_id_seq', 22279, true);
            public       postgres    false    227            �           0    0    calendar_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.calendar_id_seq', 18, true);
            public       postgres    false    228            �           0    0    center_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.center_id_seq', 159, true);
            public       postgres    false    229            �           0    0    egg_planning_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.egg_planning_id_seq', 3507, true);
            public       postgres    false    230            �           0    0    egg_required_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.egg_required_id_seq', 3393, true);
            public       postgres    false    231            �           0    0    eggs_storage_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.eggs_storage_id_seq', 42757, true);
            public       postgres    false    232            �           0    0    farm_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.farm_id_seq', 165, true);
            public       postgres    false    233            �           0    0    farm_type_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.farm_type_id_seq', 3, true);
            public       postgres    false    234            �           0    0    holiday_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.holiday_id_seq', 297, true);
            public       postgres    false    235            �           0    0    housing_way_detail_id_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.housing_way_detail_id_seq', 839, true);
            public       postgres    false    236            �           0    0    housing_way_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.housing_way_id_seq', 962, true);
            public       postgres    false    237            �           0    0    incubator_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.incubator_id_seq', 23, true);
            public       postgres    false    238            �           0    0    incubator_plant_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.incubator_plant_id_seq', 19, true);
            public       postgres    false    239            �           0    0    industry_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.industry_id_seq', 1, true);
            public       postgres    false    240            �           0    0    line_id_seq    SEQUENCE SET     9   SELECT pg_catalog.setval('public.line_id_seq', 1, true);
            public       postgres    false    241            �           0    0    lot_eggs_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.lot_eggs_id_seq', 108, true);
            public       postgres    false    242            �           0    0    lot_fattening_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.lot_fattening_id_seq', 1, false);
            public       postgres    false    243            �           0    0 
   lot_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.lot_id_seq', 316, true);
            public       postgres    false    244            �           0    0    lot_liftbreeding_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.lot_liftbreeding_id_seq', 4, true);
            public       postgres    false    245            �           0    0     mdapplication_application_id_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('public.mdapplication_application_id_seq', 1, false);
            public       postgres    false    246            �           0    0    mdapplication_rol_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.mdapplication_rol_id_seq', 14, true);
            public       postgres    false    248            �           0    0    mdrol_rol_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.mdrol_rol_id_seq', 1, false);
            public       postgres    false    261            �           0    0    mduser_user_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.mduser_user_id_seq', 1, false);
            public       postgres    false    269            �           0    0    measure_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.measure_id_seq', 17, true);
            public       postgres    false    253            �           0    0    parameter_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.parameter_id_seq', 56, true);
            public       postgres    false    255            �           0    0    partnership_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.partnership_id_seq', 31, true);
            public       postgres    false    276            �           0    0    posture_curve_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.posture_curve_id_seq', 325, true);
            public       postgres    false    287            �           0    0    predecessor_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.predecessor_id_seq', 13, true);
            public       postgres    false    288            �           0    0    process_class_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.process_class_id_seq', 5, true);
            public       postgres    false    289            �           0    0    process_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.process_id_seq', 90, true);
            public       postgres    false    257            �           0    0    product_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.product_id_seq', 112, true);
            public       postgres    false    259            �           0    0    programmed_eggs_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.programmed_eggs_id_seq', 398, true);
            public       postgres    false    290            �           0    0    raspberry_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.raspberry_id_seq', 5, true);
            public       postgres    false    291            �           0    0    scenario_formula_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.scenario_formula_id_seq', 1288, true);
            public       postgres    false    292            �           0    0    scenario_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.scenario_id_seq', 170, true);
            public       postgres    false    263            �           0    0    scenario_parameter_day_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.scenario_parameter_day_seq', 50126, true);
            public       postgres    false    293            �           0    0    scenario_parameter_id_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('public.scenario_parameter_id_seq', 6389, true);
            public       postgres    false    294            �           0    0    scenario_posture_id_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.scenario_posture_id_seq', 68396, true);
            public       postgres    false    295            �           0    0    scenario_process_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.scenario_process_id_seq', 835, true);
            public       postgres    false    296            �           0    0    shed_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.shed_id_seq', 366, true);
            public       postgres    false    278            �           0    0    silo_id_seq    SEQUENCE SET     9   SELECT pg_catalog.setval('public.silo_id_seq', 4, true);
            public       postgres    false    280            �           0    0    slaughterhouse_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.slaughterhouse_id_seq', 36, true);
            public       postgres    false    283            �           0    0    stage_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.stage_id_seq', 27, true);
            public       postgres    false    267            �           0    0    status_shed_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.status_shed_id_seq', 10, true);
            public       postgres    false    265            �           0    0    txeggs_movements_id_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('public.txeggs_movements_id_seq', 296115, true);
            public       postgres    false    306            �           0    0    txgoals_erp_goals_erp_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.txgoals_erp_goals_erp_id_seq', 1920, true);
            public       postgres    false    312            �           0    0 #   user_application_application_id_seq    SEQUENCE SET     R   SELECT pg_catalog.setval('public.user_application_application_id_seq', 1, false);
            public       postgres    false    324            �           0    0     user_application_user_app_id_seq    SEQUENCE SET     P   SELECT pg_catalog.setval('public.user_application_user_app_id_seq', 215, true);
            public       postgres    false    325            �           0    0    user_application_user_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.user_application_user_id_seq', 1, false);
            public       postgres    false    326            �           0    0    warehouse_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.warehouse_id_seq', 124, true);
            public       postgres    false    285            K           2606    28792    aba_time_unit abaTimeUnit_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.aba_time_unit
    ADD CONSTRAINT "abaTimeUnit_pkey" PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.aba_time_unit DROP CONSTRAINT "abaTimeUnit_pkey";
       public         postgres    false    215            -           2606    28794 0   aba_breeds_and_stages aba_breeds_and_stages_pkey 
   CONSTRAINT     n   ALTER TABLE ONLY public.aba_breeds_and_stages
    ADD CONSTRAINT aba_breeds_and_stages_pkey PRIMARY KEY (id);
 Z   ALTER TABLE ONLY public.aba_breeds_and_stages DROP CONSTRAINT aba_breeds_and_stages_pkey;
       public         postgres    false    198            6           2606    28796 N   aba_consumption_and_mortality_detail aba_consumption_and_mortality_detail_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.aba_consumption_and_mortality_detail
    ADD CONSTRAINT aba_consumption_and_mortality_detail_pkey PRIMARY KEY (id);
 x   ALTER TABLE ONLY public.aba_consumption_and_mortality_detail DROP CONSTRAINT aba_consumption_and_mortality_detail_pkey;
       public         postgres    false    202            1           2606    28798 @   aba_consumption_and_mortality aba_consumption_and_mortality_pkey 
   CONSTRAINT     ~   ALTER TABLE ONLY public.aba_consumption_and_mortality
    ADD CONSTRAINT aba_consumption_and_mortality_pkey PRIMARY KEY (id);
 j   ALTER TABLE ONLY public.aba_consumption_and_mortality DROP CONSTRAINT aba_consumption_and_mortality_pkey;
       public         postgres    false    200            ;           2606    28800 D   aba_elements_and_concentrations aba_elements_and_concentrations_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.aba_elements_and_concentrations
    ADD CONSTRAINT aba_elements_and_concentrations_pkey PRIMARY KEY (id);
 n   ALTER TABLE ONLY public.aba_elements_and_concentrations DROP CONSTRAINT aba_elements_and_concentrations_pkey;
       public         postgres    false    206            9           2606    28802    aba_elements aba_elements_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.aba_elements
    ADD CONSTRAINT aba_elements_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.aba_elements DROP CONSTRAINT aba_elements_pkey;
       public         postgres    false    204            @           2606    28804 4   aba_elements_properties aba_elements_properties_pkey 
   CONSTRAINT     r   ALTER TABLE ONLY public.aba_elements_properties
    ADD CONSTRAINT aba_elements_properties_pkey PRIMARY KEY (id);
 ^   ALTER TABLE ONLY public.aba_elements_properties DROP CONSTRAINT aba_elements_properties_pkey;
       public         postgres    false    208            B           2606    28806 $   aba_formulation aba_formulation_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.aba_formulation
    ADD CONSTRAINT aba_formulation_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.aba_formulation DROP CONSTRAINT aba_formulation_pkey;
       public         postgres    false    210            D           2606    28808    aba_results aba_results_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.aba_results
    ADD CONSTRAINT aba_results_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.aba_results DROP CONSTRAINT aba_results_pkey;
       public         postgres    false    212            G           2606    28810 D   aba_stages_of_breeds_and_stages aba_stages_of_breeds_and_stages_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.aba_stages_of_breeds_and_stages
    ADD CONSTRAINT aba_stages_of_breeds_and_stages_pkey PRIMARY KEY (id);
 n   ALTER TABLE ONLY public.aba_stages_of_breeds_and_stages DROP CONSTRAINT aba_stages_of_breeds_and_stages_pkey;
       public         postgres    false    214            M           2606    28812    mdapplication application_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public.mdapplication
    ADD CONSTRAINT application_pkey PRIMARY KEY (application_id);
 H   ALTER TABLE ONLY public.mdapplication DROP CONSTRAINT application_pkey;
       public         postgres    false    247            Q           2606    28814 (   mdapplication_rol mdapplication_rol_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.mdapplication_rol
    ADD CONSTRAINT mdapplication_rol_pkey PRIMARY KEY (id);
 R   ALTER TABLE ONLY public.mdapplication_rol DROP CONSTRAINT mdapplication_rol_pkey;
       public         postgres    false    249            S           2606    28816    mdbreed mdbreed_code_key 
   CONSTRAINT     S   ALTER TABLE ONLY public.mdbreed
    ADD CONSTRAINT mdbreed_code_key UNIQUE (code);
 B   ALTER TABLE ONLY public.mdbreed DROP CONSTRAINT mdbreed_code_key;
       public         postgres    false    250            U           2606    28818    mdbreed mdbreed_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.mdbreed
    ADD CONSTRAINT mdbreed_pkey PRIMARY KEY (breed_id);
 >   ALTER TABLE ONLY public.mdbreed DROP CONSTRAINT mdbreed_pkey;
       public         postgres    false    250            W           2606    28820 (   mdbroiler_product mdbroiler_product_pkey 
   CONSTRAINT     v   ALTER TABLE ONLY public.mdbroiler_product
    ADD CONSTRAINT mdbroiler_product_pkey PRIMARY KEY (broiler_product_id);
 R   ALTER TABLE ONLY public.mdbroiler_product DROP CONSTRAINT mdbroiler_product_pkey;
       public         postgres    false    251            Y           2606    28822    mdfarmtype mdfarmtype_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.mdfarmtype
    ADD CONSTRAINT mdfarmtype_pkey PRIMARY KEY (farm_type_id);
 D   ALTER TABLE ONLY public.mdfarmtype DROP CONSTRAINT mdfarmtype_pkey;
       public         postgres    false    252            [           2606    28824    mdmeasure mdmeasure_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.mdmeasure
    ADD CONSTRAINT mdmeasure_pkey PRIMARY KEY (measure_id);
 B   ALTER TABLE ONLY public.mdmeasure DROP CONSTRAINT mdmeasure_pkey;
       public         postgres    false    254            _           2606    28826    mdparameter mdparameter_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.mdparameter
    ADD CONSTRAINT mdparameter_pkey PRIMARY KEY (parameter_id);
 F   ALTER TABLE ONLY public.mdparameter DROP CONSTRAINT mdparameter_pkey;
       public         postgres    false    256            e           2606    28828    mdprocess mdprocess_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.mdprocess
    ADD CONSTRAINT mdprocess_pkey PRIMARY KEY (process_id);
 B   ALTER TABLE ONLY public.mdprocess DROP CONSTRAINT mdprocess_pkey;
       public         postgres    false    258            g           2606    28830    mdproduct mdproduct_code_unique 
   CONSTRAINT     Z   ALTER TABLE ONLY public.mdproduct
    ADD CONSTRAINT mdproduct_code_unique UNIQUE (code);
 I   ALTER TABLE ONLY public.mdproduct DROP CONSTRAINT mdproduct_code_unique;
       public         postgres    false    260            i           2606    28832    mdproduct mdproduct_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.mdproduct
    ADD CONSTRAINT mdproduct_pkey PRIMARY KEY (product_id);
 B   ALTER TABLE ONLY public.mdproduct DROP CONSTRAINT mdproduct_pkey;
       public         postgres    false    260            p           2606    28834 !   mdscenario mdscenario_name_unique 
   CONSTRAINT     \   ALTER TABLE ONLY public.mdscenario
    ADD CONSTRAINT mdscenario_name_unique UNIQUE (name);
 K   ALTER TABLE ONLY public.mdscenario DROP CONSTRAINT mdscenario_name_unique;
       public         postgres    false    264            r           2606    28836    mdscenario mdscenario_pkey 
   CONSTRAINT     a   ALTER TABLE ONLY public.mdscenario
    ADD CONSTRAINT mdscenario_pkey PRIMARY KEY (scenario_id);
 D   ALTER TABLE ONLY public.mdscenario DROP CONSTRAINT mdscenario_pkey;
       public         postgres    false    264            t           2606    28838    mdshedstatus mdshedstatus_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public.mdshedstatus
    ADD CONSTRAINT mdshedstatus_pkey PRIMARY KEY (shed_status_id);
 H   ALTER TABLE ONLY public.mdshedstatus DROP CONSTRAINT mdshedstatus_pkey;
       public         postgres    false    266            v           2606    28840    mdstage mdstage_name_unique 
   CONSTRAINT     V   ALTER TABLE ONLY public.mdstage
    ADD CONSTRAINT mdstage_name_unique UNIQUE (name);
 E   ALTER TABLE ONLY public.mdstage DROP CONSTRAINT mdstage_name_unique;
       public         postgres    false    268            x           2606    28842    mdstage mdstage_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.mdstage
    ADD CONSTRAINT mdstage_pkey PRIMARY KEY (stage_id);
 >   ALTER TABLE ONLY public.mdstage DROP CONSTRAINT mdstage_pkey;
       public         postgres    false    268            {           2606    28844    mduser mduser_user_id_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY public.mduser
    ADD CONSTRAINT mduser_user_id_pkey PRIMARY KEY (user_id);
 D   ALTER TABLE ONLY public.mduser DROP CONSTRAINT mduser_user_id_pkey;
       public         postgres    false    270            }           2606    28846    mduser mduser_username_unique 
   CONSTRAINT     \   ALTER TABLE ONLY public.mduser
    ADD CONSTRAINT mduser_username_unique UNIQUE (username);
 G   ALTER TABLE ONLY public.mduser DROP CONSTRAINT mduser_username_unique;
       public         postgres    false    270            �           2606    28848 .   oscenter_oswarehouse oscenter_oswarehouse_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.oscenter_oswarehouse
    ADD CONSTRAINT oscenter_oswarehouse_pkey PRIMARY KEY (client_id, partnership_id, farm_id, center_id, warehouse_id);
 X   ALTER TABLE ONLY public.oscenter_oswarehouse DROP CONSTRAINT oscenter_oswarehouse_pkey;
       public         postgres    false    272    272    272    272    272            �           2606    28850 .   oscenter oscenter_partnership_farm_code_unique 
   CONSTRAINT     �   ALTER TABLE ONLY public.oscenter
    ADD CONSTRAINT oscenter_partnership_farm_code_unique UNIQUE (partnership_id, farm_id, code);
 X   ALTER TABLE ONLY public.oscenter DROP CONSTRAINT oscenter_partnership_farm_code_unique;
       public         postgres    false    271    271    271            �           2606    28852    oscenter oscenter_pkey 
   CONSTRAINT     [   ALTER TABLE ONLY public.oscenter
    ADD CONSTRAINT oscenter_pkey PRIMARY KEY (center_id);
 @   ALTER TABLE ONLY public.oscenter DROP CONSTRAINT oscenter_pkey;
       public         postgres    false    271            �           2606    28854 %   osfarm osfarm_partnership_code_unique 
   CONSTRAINT     p   ALTER TABLE ONLY public.osfarm
    ADD CONSTRAINT osfarm_partnership_code_unique UNIQUE (partnership_id, code);
 O   ALTER TABLE ONLY public.osfarm DROP CONSTRAINT osfarm_partnership_code_unique;
       public         postgres    false    273    273            �           2606    28856    osfarm osfarm_pkey 
   CONSTRAINT     U   ALTER TABLE ONLY public.osfarm
    ADD CONSTRAINT osfarm_pkey PRIMARY KEY (farm_id);
 <   ALTER TABLE ONLY public.osfarm DROP CONSTRAINT osfarm_pkey;
       public         postgres    false    273            �           2606    28858    osshed oshed_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.osshed
    ADD CONSTRAINT oshed_pkey PRIMARY KEY (shed_id);
 ;   ALTER TABLE ONLY public.osshed DROP CONSTRAINT oshed_pkey;
       public         postgres    false    279            �           2606    28860 2   osincubator osincubator_incubatorplant_code_unique 
   CONSTRAINT     �   ALTER TABLE ONLY public.osincubator
    ADD CONSTRAINT osincubator_incubatorplant_code_unique UNIQUE (incubator_plant_id, code);
 \   ALTER TABLE ONLY public.osincubator DROP CONSTRAINT osincubator_incubatorplant_code_unique;
       public         postgres    false    274    274            �           2606    28862    osincubator osincubator_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.osincubator
    ADD CONSTRAINT osincubator_pkey PRIMARY KEY (incubator_id);
 F   ALTER TABLE ONLY public.osincubator DROP CONSTRAINT osincubator_pkey;
       public         postgres    false    274            �           2606    28864 9   osincubatorplant osincubatorplant_partnership_code_unique 
   CONSTRAINT     �   ALTER TABLE ONLY public.osincubatorplant
    ADD CONSTRAINT osincubatorplant_partnership_code_unique UNIQUE (partnership_id, code);
 c   ALTER TABLE ONLY public.osincubatorplant DROP CONSTRAINT osincubatorplant_partnership_code_unique;
       public         postgres    false    275    275            �           2606    28866 &   osincubatorplant osincubatorplant_pkey 
   CONSTRAINT     t   ALTER TABLE ONLY public.osincubatorplant
    ADD CONSTRAINT osincubatorplant_pkey PRIMARY KEY (incubator_plant_id);
 P   ALTER TABLE ONLY public.osincubatorplant DROP CONSTRAINT osincubatorplant_pkey;
       public         postgres    false    275            �           2606    28868 '   ospartnership ospartnership_code_unique 
   CONSTRAINT     b   ALTER TABLE ONLY public.ospartnership
    ADD CONSTRAINT ospartnership_code_unique UNIQUE (code);
 Q   ALTER TABLE ONLY public.ospartnership DROP CONSTRAINT ospartnership_code_unique;
       public         postgres    false    277            �           2606    28870     ospartnership ospartnership_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY public.ospartnership
    ADD CONSTRAINT ospartnership_pkey PRIMARY KEY (partnership_id);
 J   ALTER TABLE ONLY public.ospartnership DROP CONSTRAINT ospartnership_pkey;
       public         postgres    false    277            �           2606    28872 1   osshed osshed_partnership_farm_center_code_unique 
   CONSTRAINT     �   ALTER TABLE ONLY public.osshed
    ADD CONSTRAINT osshed_partnership_farm_center_code_unique UNIQUE (partnership_id, farm_id, center_id, code);
 [   ALTER TABLE ONLY public.osshed DROP CONSTRAINT osshed_partnership_farm_center_code_unique;
       public         postgres    false    279    279    279    279            �           2606    28874     ossilo_osshed ossilo_osshed_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.ossilo_osshed
    ADD CONSTRAINT ossilo_osshed_pkey PRIMARY KEY (silo_id, shed_id);
 J   ALTER TABLE ONLY public.ossilo_osshed DROP CONSTRAINT ossilo_osshed_pkey;
       public         postgres    false    282    282            �           2606    28876    ossilo ossilo_pkey 
   CONSTRAINT     U   ALTER TABLE ONLY public.ossilo
    ADD CONSTRAINT ossilo_pkey PRIMARY KEY (silo_id);
 <   ALTER TABLE ONLY public.ossilo DROP CONSTRAINT ossilo_pkey;
       public         postgres    false    281            �           2606    28878 &   osslaughterhouse osslaughterhouse_pkey 
   CONSTRAINT     s   ALTER TABLE ONLY public.osslaughterhouse
    ADD CONSTRAINT osslaughterhouse_pkey PRIMARY KEY (slaughterhouse_id);
 P   ALTER TABLE ONLY public.osslaughterhouse DROP CONSTRAINT osslaughterhouse_pkey;
       public         postgres    false    284            �           2606    28880 4   oswarehouse oswarehouse_partnership_farm_code_unique 
   CONSTRAINT     �   ALTER TABLE ONLY public.oswarehouse
    ADD CONSTRAINT oswarehouse_partnership_farm_code_unique UNIQUE (partnership_id, farm_id, code);
 ^   ALTER TABLE ONLY public.oswarehouse DROP CONSTRAINT oswarehouse_partnership_farm_code_unique;
       public         postgres    false    286    286    286            �           2606    28882    oswarehouse oswarehouse_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.oswarehouse
    ADD CONSTRAINT oswarehouse_pkey PRIMARY KEY (warehouse_id);
 F   ALTER TABLE ONLY public.oswarehouse DROP CONSTRAINT oswarehouse_pkey;
       public         postgres    false    286            k           2606    28884    mdrol rol_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.mdrol
    ADD CONSTRAINT rol_pkey PRIMARY KEY (rol_id);
 8   ALTER TABLE ONLY public.mdrol DROP CONSTRAINT rol_pkey;
       public         postgres    false    262            �           2606    28886 ,   txavailabilitysheds txavailabilitysheds_pkey 
   CONSTRAINT     |   ALTER TABLE ONLY public.txavailabilitysheds
    ADD CONSTRAINT txavailabilitysheds_pkey PRIMARY KEY (availability_shed_id);
 V   ALTER TABLE ONLY public.txavailabilitysheds DROP CONSTRAINT txavailabilitysheds_pkey;
       public         postgres    false    297            �           2606    28888 &   txbroiler_detail txbroiler_detail_pkey 
   CONSTRAINT     s   ALTER TABLE ONLY public.txbroiler_detail
    ADD CONSTRAINT txbroiler_detail_pkey PRIMARY KEY (broiler_detail_id);
 P   ALTER TABLE ONLY public.txbroiler_detail DROP CONSTRAINT txbroiler_detail_pkey;
       public         postgres    false    299            �           2606    28890    txbroiler txbroiler_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.txbroiler
    ADD CONSTRAINT txbroiler_pkey PRIMARY KEY (broiler_id);
 B   ALTER TABLE ONLY public.txbroiler DROP CONSTRAINT txbroiler_pkey;
       public         postgres    false    298            �           2606    28892 6   txbroilereviction_detail txbroilereviction_detail_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.txbroilereviction_detail
    ADD CONSTRAINT txbroilereviction_detail_pkey PRIMARY KEY (broilereviction_detail_id);
 `   ALTER TABLE ONLY public.txbroilereviction_detail DROP CONSTRAINT txbroilereviction_detail_pkey;
       public         postgres    false    301            �           2606    28894 (   txbroilereviction txbroilereviction_pkey 
   CONSTRAINT     v   ALTER TABLE ONLY public.txbroilereviction
    ADD CONSTRAINT txbroilereviction_pkey PRIMARY KEY (broilereviction_id);
 R   ALTER TABLE ONLY public.txbroilereviction DROP CONSTRAINT txbroilereviction_pkey;
       public         postgres    false    300            �           2606    28896 4   txbroilerproduct_detail txbroilerproduct_detail_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.txbroilerproduct_detail
    ADD CONSTRAINT txbroilerproduct_detail_pkey PRIMARY KEY (broilerproduct_detail_id);
 ^   ALTER TABLE ONLY public.txbroilerproduct_detail DROP CONSTRAINT txbroilerproduct_detail_pkey;
       public         postgres    false    302            �           2606    28898 &   txbroodermachine txbroodermachine_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.txbroodermachine
    ADD CONSTRAINT txbroodermachine_pkey PRIMARY KEY (brooder_machine_id_seq);
 P   ALTER TABLE ONLY public.txbroodermachine DROP CONSTRAINT txbroodermachine_pkey;
       public         postgres    false    303            �           2606    28900    txcalendar txcalendar_pkey 
   CONSTRAINT     a   ALTER TABLE ONLY public.txcalendar
    ADD CONSTRAINT txcalendar_pkey PRIMARY KEY (calendar_id);
 D   ALTER TABLE ONLY public.txcalendar DROP CONSTRAINT txcalendar_pkey;
       public         postgres    false    304            �           2606    28902     txcalendarday txcalendarday_pkey 
   CONSTRAINT     k   ALTER TABLE ONLY public.txcalendarday
    ADD CONSTRAINT txcalendarday_pkey PRIMARY KEY (calendar_day_id);
 J   ALTER TABLE ONLY public.txcalendarday DROP CONSTRAINT txcalendarday_pkey;
       public         postgres    false    305            �           2606    28904 &   txeggs_movements txeggs_movements_pkey 
   CONSTRAINT     s   ALTER TABLE ONLY public.txeggs_movements
    ADD CONSTRAINT txeggs_movements_pkey PRIMARY KEY (eggs_movements_id);
 P   ALTER TABLE ONLY public.txeggs_movements DROP CONSTRAINT txeggs_movements_pkey;
       public         postgres    false    307            �           2606    28906 $   txeggs_planning txeggs_planning_pkey 
   CONSTRAINT     o   ALTER TABLE ONLY public.txeggs_planning
    ADD CONSTRAINT txeggs_planning_pkey PRIMARY KEY (egg_planning_id);
 N   ALTER TABLE ONLY public.txeggs_planning DROP CONSTRAINT txeggs_planning_pkey;
       public         postgres    false    308            �           2606    28908 $   txeggs_required txeggs_required_pkey 
   CONSTRAINT     o   ALTER TABLE ONLY public.txeggs_required
    ADD CONSTRAINT txeggs_required_pkey PRIMARY KEY (egg_required_id);
 N   ALTER TABLE ONLY public.txeggs_required DROP CONSTRAINT txeggs_required_pkey;
       public         postgres    false    309            �           2606    28910 "   txeggs_storage txeggs_storage_pkey 
   CONSTRAINT     m   ALTER TABLE ONLY public.txeggs_storage
    ADD CONSTRAINT txeggs_storage_pkey PRIMARY KEY (eggs_storage_id);
 L   ALTER TABLE ONLY public.txeggs_storage DROP CONSTRAINT txeggs_storage_pkey;
       public         postgres    false    310            �           2606    28912    txgoals_erp txgoals_erp_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.txgoals_erp
    ADD CONSTRAINT txgoals_erp_pkey PRIMARY KEY (goals_erp_id);
 F   ALTER TABLE ONLY public.txgoals_erp DROP CONSTRAINT txgoals_erp_pkey;
       public         postgres    false    311                       2606    28914 ,   txhousingway_detail txhousingway_detail_pkey 
   CONSTRAINT     |   ALTER TABLE ONLY public.txhousingway_detail
    ADD CONSTRAINT txhousingway_detail_pkey PRIMARY KEY (housingway_detail_id);
 V   ALTER TABLE ONLY public.txhousingway_detail DROP CONSTRAINT txhousingway_detail_pkey;
       public         postgres    false    314            �           2606    28916    txhousingway txhousingway_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public.txhousingway
    ADD CONSTRAINT txhousingway_pkey PRIMARY KEY (housing_way_id);
 H   ALTER TABLE ONLY public.txhousingway DROP CONSTRAINT txhousingway_pkey;
       public         postgres    false    313                       2606    28918    txlot_eggs txlot_eggs_pkey 
   CONSTRAINT     a   ALTER TABLE ONLY public.txlot_eggs
    ADD CONSTRAINT txlot_eggs_pkey PRIMARY KEY (lot_eggs_id);
 D   ALTER TABLE ONLY public.txlot_eggs DROP CONSTRAINT txlot_eggs_pkey;
       public         postgres    false    316            	           2606    28920    txlot txlot_lot_code_key 
   CONSTRAINT     W   ALTER TABLE ONLY public.txlot
    ADD CONSTRAINT txlot_lot_code_key UNIQUE (lot_code);
 B   ALTER TABLE ONLY public.txlot DROP CONSTRAINT txlot_lot_code_key;
       public         postgres    false    315                       2606    28922    txlot txlot_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.txlot
    ADD CONSTRAINT txlot_pkey PRIMARY KEY (lot_id);
 :   ALTER TABLE ONLY public.txlot DROP CONSTRAINT txlot_pkey;
       public         postgres    false    315                       2606    28924 "   txposturecurve txposturecurve_pkey 
   CONSTRAINT     n   ALTER TABLE ONLY public.txposturecurve
    ADD CONSTRAINT txposturecurve_pkey PRIMARY KEY (posture_curve_id);
 L   ALTER TABLE ONLY public.txposturecurve DROP CONSTRAINT txposturecurve_pkey;
       public         postgres    false    317                       2606    28926 (   txprogrammed_eggs txprogrammed_eggs_pkey 
   CONSTRAINT     v   ALTER TABLE ONLY public.txprogrammed_eggs
    ADD CONSTRAINT txprogrammed_eggs_pkey PRIMARY KEY (programmed_eggs_id);
 R   ALTER TABLE ONLY public.txprogrammed_eggs DROP CONSTRAINT txprogrammed_eggs_pkey;
       public         postgres    false    318                       2606    28928 (   txscenarioformula txscenarioformula_pkey 
   CONSTRAINT     w   ALTER TABLE ONLY public.txscenarioformula
    ADD CONSTRAINT txscenarioformula_pkey PRIMARY KEY (scenario_formula_id);
 R   ALTER TABLE ONLY public.txscenarioformula DROP CONSTRAINT txscenarioformula_pkey;
       public         postgres    false    319                        2606    28930 ,   txscenarioparameter txscenarioparameter_pkey 
   CONSTRAINT     }   ALTER TABLE ONLY public.txscenarioparameter
    ADD CONSTRAINT txscenarioparameter_pkey PRIMARY KEY (scenario_parameter_id);
 V   ALTER TABLE ONLY public.txscenarioparameter DROP CONSTRAINT txscenarioparameter_pkey;
       public         postgres    false    320            $           2606    28932 2   txscenarioparameterday txscenarioparameterday_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioparameterday
    ADD CONSTRAINT txscenarioparameterday_pkey PRIMARY KEY (scenario_parameter_day_id);
 \   ALTER TABLE ONLY public.txscenarioparameterday DROP CONSTRAINT txscenarioparameterday_pkey;
       public         postgres    false    321            *           2606    28934 2   txscenarioposturecurve txscenarioposturecurve_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioposturecurve
    ADD CONSTRAINT txscenarioposturecurve_pkey PRIMARY KEY (scenario_posture_id);
 \   ALTER TABLE ONLY public.txscenarioposturecurve DROP CONSTRAINT txscenarioposturecurve_pkey;
       public         postgres    false    322            .           2606    28936 (   txscenarioprocess txscenarioprocess_pkey 
   CONSTRAINT     w   ALTER TABLE ONLY public.txscenarioprocess
    ADD CONSTRAINT txscenarioprocess_pkey PRIMARY KEY (scenario_process_id);
 R   ALTER TABLE ONLY public.txscenarioprocess DROP CONSTRAINT txscenarioprocess_pkey;
       public         postgres    false    323            m           2606    28938    mdrol uniqueRolName 
   CONSTRAINT     T   ALTER TABLE ONLY public.mdrol
    ADD CONSTRAINT "uniqueRolName" UNIQUE (rol_name);
 ?   ALTER TABLE ONLY public.mdrol DROP CONSTRAINT "uniqueRolName";
       public         postgres    false    262            �           1259    28939    calendar_index    INDEX     N   CREATE INDEX calendar_index ON public.txcalendarday USING hash (calendar_id);
 "   DROP INDEX public.calendar_index;
       public         postgres    false    305            `           1259    28940    calendarid_index    INDEX     L   CREATE INDEX calendarid_index ON public.mdprocess USING hash (calendar_id);
 $   DROP INDEX public.calendarid_index;
       public         postgres    false    258            �           1259    28941 
   code_index    INDEX     H   CREATE UNIQUE INDEX code_index ON public.txcalendar USING btree (code);
    DROP INDEX public.code_index;
       public         postgres    false    304            �           1259    28942 
   date_index    INDEX     G   CREATE INDEX date_index ON public.txcalendarday USING hash (use_date);
    DROP INDEX public.date_index;
       public         postgres    false    305            2           1259    28943    fki_FK_ id_aba_time_unit    INDEX     p   CREATE INDEX "fki_FK_ id_aba_time_unit" ON public.aba_consumption_and_mortality USING btree (id_aba_time_unit);
 .   DROP INDEX public."fki_FK_ id_aba_time_unit";
       public         postgres    false    200            H           1259    28944    fki_FK_id_aba_breeds_and_stages    INDEX     �   CREATE INDEX "fki_FK_id_aba_breeds_and_stages" ON public.aba_stages_of_breeds_and_stages USING btree (id_aba_breeds_and_stages);
 5   DROP INDEX public."fki_FK_id_aba_breeds_and_stages";
       public         postgres    false    214            .           1259    28945 '   fki_FK_id_aba_consumption_and_mortality    INDEX     �   CREATE INDEX "fki_FK_id_aba_consumption_and_mortality" ON public.aba_breeds_and_stages USING btree (id_aba_consumption_and_mortality);
 =   DROP INDEX public."fki_FK_id_aba_consumption_and_mortality";
       public         postgres    false    198            7           1259    28946 (   fki_FK_id_aba_consumption_and_mortality2    INDEX     �   CREATE INDEX "fki_FK_id_aba_consumption_and_mortality2" ON public.aba_consumption_and_mortality_detail USING btree (id_aba_consumption_and_mortality);
 >   DROP INDEX public."fki_FK_id_aba_consumption_and_mortality2";
       public         postgres    false    202            <           1259    28947    fki_FK_id_aba_element    INDEX     m   CREATE INDEX "fki_FK_id_aba_element" ON public.aba_elements_and_concentrations USING btree (id_aba_element);
 +   DROP INDEX public."fki_FK_id_aba_element";
       public         postgres    false    206            E           1259    28948    fki_FK_id_aba_element2    INDEX     Z   CREATE INDEX "fki_FK_id_aba_element2" ON public.aba_results USING btree (id_aba_element);
 ,   DROP INDEX public."fki_FK_id_aba_element2";
       public         postgres    false    212            =           1259    28949    fki_FK_id_aba_element_property    INDEX        CREATE INDEX "fki_FK_id_aba_element_property" ON public.aba_elements_and_concentrations USING btree (id_aba_element_property);
 4   DROP INDEX public."fki_FK_id_aba_element_property";
       public         postgres    false    206            >           1259    28950    fki_FK_id_aba_formulation    INDEX     u   CREATE INDEX "fki_FK_id_aba_formulation" ON public.aba_elements_and_concentrations USING btree (id_aba_formulation);
 /   DROP INDEX public."fki_FK_id_aba_formulation";
       public         postgres    false    206            3           1259    28951    fki_FK_id_breed    INDEX     _   CREATE INDEX "fki_FK_id_breed" ON public.aba_consumption_and_mortality USING btree (id_breed);
 %   DROP INDEX public."fki_FK_id_breed";
       public         postgres    false    200            I           1259    28952    fki_FK_id_formulation    INDEX     m   CREATE INDEX "fki_FK_id_formulation" ON public.aba_stages_of_breeds_and_stages USING btree (id_formulation);
 +   DROP INDEX public."fki_FK_id_formulation";
       public         postgres    false    214            /           1259    28953    fki_FK_id_process    INDEX     [   CREATE INDEX "fki_FK_id_process" ON public.aba_breeds_and_stages USING btree (id_process);
 '   DROP INDEX public."fki_FK_id_process";
       public         postgres    false    198            4           1259    28954    fki_FK_id_stage    INDEX     _   CREATE INDEX "fki_FK_id_stage" ON public.aba_consumption_and_mortality USING btree (id_stage);
 %   DROP INDEX public."fki_FK_id_stage";
       public         postgres    false    200            N           1259    28955 )   fki_mdapplication_rol_application_id_fkey    INDEX     q   CREATE INDEX fki_mdapplication_rol_application_id_fkey ON public.mdapplication_rol USING btree (application_id);
 =   DROP INDEX public.fki_mdapplication_rol_application_id_fkey;
       public         postgres    false    249            O           1259    28956 !   fki_mdapplication_rol_rol_id_fkey    INDEX     a   CREATE INDEX fki_mdapplication_rol_rol_id_fkey ON public.mdapplication_rol USING btree (rol_id);
 5   DROP INDEX public.fki_mdapplication_rol_rol_id_fkey;
       public         postgres    false    249            \           1259    28957    fki_mdparameter_measure_id_fkey    INDEX     ]   CREATE INDEX fki_mdparameter_measure_id_fkey ON public.mdparameter USING btree (measure_id);
 3   DROP INDEX public.fki_mdparameter_measure_id_fkey;
       public         postgres    false    256            ]           1259    28958    fki_mdparameter_process_id_fkey    INDEX     ]   CREATE INDEX fki_mdparameter_process_id_fkey ON public.mdparameter USING btree (process_id);
 3   DROP INDEX public.fki_mdparameter_process_id_fkey;
       public         postgres    false    256            a           1259    28959    fki_mdprocess_breed_id_fkey    INDEX     U   CREATE INDEX fki_mdprocess_breed_id_fkey ON public.mdprocess USING btree (breed_id);
 /   DROP INDEX public.fki_mdprocess_breed_id_fkey;
       public         postgres    false    258            n           1259    28960    fki_mdscenario_calendar_id_fkey    INDEX     ]   CREATE INDEX fki_mdscenario_calendar_id_fkey ON public.mdscenario USING btree (calendar_id);
 3   DROP INDEX public.fki_mdscenario_calendar_id_fkey;
       public         postgres    false    264            y           1259    28961    fki_mduser_rol_id_fkey    INDEX     K   CREATE INDEX fki_mduser_rol_id_fkey ON public.mduser USING btree (rol_id);
 *   DROP INDEX public.fki_mduser_rol_id_fkey;
       public         postgres    false    270            ~           1259    28962    fki_oscenter_farm_id_fkey    INDEX     Q   CREATE INDEX fki_oscenter_farm_id_fkey ON public.oscenter USING btree (farm_id);
 -   DROP INDEX public.fki_oscenter_farm_id_fkey;
       public         postgres    false    271            �           1259    28963 '   fki_oscenter_oswarehouse_center_id_fkey    INDEX     m   CREATE INDEX fki_oscenter_oswarehouse_center_id_fkey ON public.oscenter_oswarehouse USING btree (center_id);
 ;   DROP INDEX public.fki_oscenter_oswarehouse_center_id_fkey;
       public         postgres    false    272            �           1259    28964 %   fki_oscenter_oswarehouse_farm_id_fkey    INDEX     i   CREATE INDEX fki_oscenter_oswarehouse_farm_id_fkey ON public.oscenter_oswarehouse USING btree (farm_id);
 9   DROP INDEX public.fki_oscenter_oswarehouse_farm_id_fkey;
       public         postgres    false    272            �           1259    28965 ,   fki_oscenter_oswarehouse_partnership_id_fkey    INDEX     w   CREATE INDEX fki_oscenter_oswarehouse_partnership_id_fkey ON public.oscenter_oswarehouse USING btree (partnership_id);
 @   DROP INDEX public.fki_oscenter_oswarehouse_partnership_id_fkey;
       public         postgres    false    272            �           1259    28966 *   fki_oscenter_oswarehouse_warehouse_id_fkey    INDEX     s   CREATE INDEX fki_oscenter_oswarehouse_warehouse_id_fkey ON public.oscenter_oswarehouse USING btree (warehouse_id);
 >   DROP INDEX public.fki_oscenter_oswarehouse_warehouse_id_fkey;
       public         postgres    false    272                       1259    28967     fki_oscenter_partnership_id_fkey    INDEX     _   CREATE INDEX fki_oscenter_partnership_id_fkey ON public.oscenter USING btree (partnership_id);
 4   DROP INDEX public.fki_oscenter_partnership_id_fkey;
       public         postgres    false    271            �           1259    28968    fki_osfarm_farm_type_id_fkey    INDEX     W   CREATE INDEX fki_osfarm_farm_type_id_fkey ON public.osfarm USING btree (farm_type_id);
 0   DROP INDEX public.fki_osfarm_farm_type_id_fkey;
       public         postgres    false    273            �           1259    28969    fki_osfarm_partnership_id_fkey    INDEX     [   CREATE INDEX fki_osfarm_partnership_id_fkey ON public.osfarm USING btree (partnership_id);
 2   DROP INDEX public.fki_osfarm_partnership_id_fkey;
       public         postgres    false    273            �           1259    28970 '   fki_osincubator_incubator_plant_id_fkey    INDEX     m   CREATE INDEX fki_osincubator_incubator_plant_id_fkey ON public.osincubator USING btree (incubator_plant_id);
 ;   DROP INDEX public.fki_osincubator_incubator_plant_id_fkey;
       public         postgres    false    274            �           1259    28971 (   fki_osincubatorplant_partnership_id_fkey    INDEX     o   CREATE INDEX fki_osincubatorplant_partnership_id_fkey ON public.osincubatorplant USING btree (partnership_id);
 <   DROP INDEX public.fki_osincubatorplant_partnership_id_fkey;
       public         postgres    false    275            �           1259    28972    fki_osshed_center_id_fkey    INDEX     Q   CREATE INDEX fki_osshed_center_id_fkey ON public.osshed USING btree (center_id);
 -   DROP INDEX public.fki_osshed_center_id_fkey;
       public         postgres    false    279            �           1259    28973    fki_osshed_farm_id_fkey    INDEX     M   CREATE INDEX fki_osshed_farm_id_fkey ON public.osshed USING btree (farm_id);
 +   DROP INDEX public.fki_osshed_farm_id_fkey;
       public         postgres    false    279            �           1259    28974    fki_osshed_partnership_id_fkey    INDEX     [   CREATE INDEX fki_osshed_partnership_id_fkey ON public.osshed USING btree (partnership_id);
 2   DROP INDEX public.fki_osshed_partnership_id_fkey;
       public         postgres    false    279            �           1259    28975    fki_osshed_statusshed_id_fkey    INDEX     Y   CREATE INDEX fki_osshed_statusshed_id_fkey ON public.osshed USING btree (statusshed_id);
 1   DROP INDEX public.fki_osshed_statusshed_id_fkey;
       public         postgres    false    279            �           1259    28976    fki_ossilo_center_id_fkey    INDEX     Q   CREATE INDEX fki_ossilo_center_id_fkey ON public.ossilo USING btree (center_id);
 -   DROP INDEX public.fki_ossilo_center_id_fkey;
       public         postgres    false    281            �           1259    28977    fki_ossilo_farm_id_fkey    INDEX     M   CREATE INDEX fki_ossilo_farm_id_fkey ON public.ossilo USING btree (farm_id);
 +   DROP INDEX public.fki_ossilo_farm_id_fkey;
       public         postgres    false    281            �           1259    28978     fki_ossilo_osshed_center_id_fkey    INDEX     _   CREATE INDEX fki_ossilo_osshed_center_id_fkey ON public.ossilo_osshed USING btree (center_id);
 4   DROP INDEX public.fki_ossilo_osshed_center_id_fkey;
       public         postgres    false    282            �           1259    28979    fki_ossilo_osshed_farm_id_fkey    INDEX     [   CREATE INDEX fki_ossilo_osshed_farm_id_fkey ON public.ossilo_osshed USING btree (farm_id);
 2   DROP INDEX public.fki_ossilo_osshed_farm_id_fkey;
       public         postgres    false    282            �           1259    28980 %   fki_ossilo_osshed_partnership_id_fkey    INDEX     i   CREATE INDEX fki_ossilo_osshed_partnership_id_fkey ON public.ossilo_osshed USING btree (partnership_id);
 9   DROP INDEX public.fki_ossilo_osshed_partnership_id_fkey;
       public         postgres    false    282            �           1259    28981    fki_ossilo_osshed_shed_id_fkey    INDEX     [   CREATE INDEX fki_ossilo_osshed_shed_id_fkey ON public.ossilo_osshed USING btree (shed_id);
 2   DROP INDEX public.fki_ossilo_osshed_shed_id_fkey;
       public         postgres    false    282            �           1259    28982    fki_ossilo_osshed_silo_id_fkey    INDEX     [   CREATE INDEX fki_ossilo_osshed_silo_id_fkey ON public.ossilo_osshed USING btree (silo_id);
 2   DROP INDEX public.fki_ossilo_osshed_silo_id_fkey;
       public         postgres    false    282            �           1259    28983    fki_ossilo_partnership_id_fkey    INDEX     [   CREATE INDEX fki_ossilo_partnership_id_fkey ON public.ossilo USING btree (partnership_id);
 2   DROP INDEX public.fki_ossilo_partnership_id_fkey;
       public         postgres    false    281            �           1259    28984    fki_oswarehouse_farm_id_fkey    INDEX     W   CREATE INDEX fki_oswarehouse_farm_id_fkey ON public.oswarehouse USING btree (farm_id);
 0   DROP INDEX public.fki_oswarehouse_farm_id_fkey;
       public         postgres    false    286            �           1259    28985 #   fki_oswarehouse_partnership_id_fkey    INDEX     e   CREATE INDEX fki_oswarehouse_partnership_id_fkey ON public.oswarehouse USING btree (partnership_id);
 7   DROP INDEX public.fki_oswarehouse_partnership_id_fkey;
       public         postgres    false    286            b           1259    28986    fki_process_product_id_fkey    INDEX     W   CREATE INDEX fki_process_product_id_fkey ON public.mdprocess USING btree (product_id);
 /   DROP INDEX public.fki_process_product_id_fkey;
       public         postgres    false    258            c           1259    28987    fki_process_stage_id_fkey    INDEX     S   CREATE INDEX fki_process_stage_id_fkey ON public.mdprocess USING btree (stage_id);
 -   DROP INDEX public.fki_process_stage_id_fkey;
       public         postgres    false    258            �           1259    28988 %   fki_txavailabilitysheds_lot_code_fkey    INDEX     i   CREATE INDEX fki_txavailabilitysheds_lot_code_fkey ON public.txavailabilitysheds USING btree (lot_code);
 9   DROP INDEX public.fki_txavailabilitysheds_lot_code_fkey;
       public         postgres    false    297            �           1259    28989 $   fki_txavailabilitysheds_shed_id_fkey    INDEX     g   CREATE INDEX fki_txavailabilitysheds_shed_id_fkey ON public.txavailabilitysheds USING btree (shed_id);
 8   DROP INDEX public.fki_txavailabilitysheds_shed_id_fkey;
       public         postgres    false    297            �           1259    28990 $   fki_txbroiler_detail_broiler_id_fkey    INDEX     g   CREATE INDEX fki_txbroiler_detail_broiler_id_fkey ON public.txbroiler_detail USING btree (broiler_id);
 8   DROP INDEX public.fki_txbroiler_detail_broiler_id_fkey;
       public         postgres    false    299            �           1259    28991 !   fki_txbroiler_detail_farm_id_fkey    INDEX     a   CREATE INDEX fki_txbroiler_detail_farm_id_fkey ON public.txbroiler_detail USING btree (farm_id);
 5   DROP INDEX public.fki_txbroiler_detail_farm_id_fkey;
       public         postgres    false    299            �           1259    28992 !   fki_txbroiler_detail_shed_id_fkey    INDEX     a   CREATE INDEX fki_txbroiler_detail_shed_id_fkey ON public.txbroiler_detail USING btree (shed_id);
 5   DROP INDEX public.fki_txbroiler_detail_shed_id_fkey;
       public         postgres    false    299            �           1259    28993 %   fki_txbroiler_programmed_eggs_id_fkey    INDEX     i   CREATE INDEX fki_txbroiler_programmed_eggs_id_fkey ON public.txbroiler USING btree (programmed_eggs_id);
 9   DROP INDEX public.fki_txbroiler_programmed_eggs_id_fkey;
       public         postgres    false    298            �           1259    28994 #   fki_txbroilereviction_breed_id_fkey    INDEX     e   CREATE INDEX fki_txbroilereviction_breed_id_fkey ON public.txbroilereviction USING btree (breed_id);
 7   DROP INDEX public.fki_txbroilereviction_breed_id_fkey;
       public         postgres    false    300            �           1259    28995 ,   fki_txbroilereviction_detail_broiler_id_fkey    INDEX        CREATE INDEX fki_txbroilereviction_detail_broiler_id_fkey ON public.txbroilereviction_detail USING btree (broilereviction_id);
 @   DROP INDEX public.fki_txbroilereviction_detail_broiler_id_fkey;
       public         postgres    false    301            �           1259    28996 4   fki_txbroilereviction_detail_broiler_product_id_fkey    INDEX     �   CREATE INDEX fki_txbroilereviction_detail_broiler_product_id_fkey ON public.txbroilereviction_detail USING btree (broiler_product_id);
 H   DROP INDEX public.fki_txbroilereviction_detail_broiler_product_id_fkey;
       public         postgres    false    301            �           1259    28997 )   fki_txbroilereviction_detail_farm_id_fkey    INDEX     q   CREATE INDEX fki_txbroilereviction_detail_farm_id_fkey ON public.txbroilereviction_detail USING btree (farm_id);
 =   DROP INDEX public.fki_txbroilereviction_detail_farm_id_fkey;
       public         postgres    false    301            �           1259    28998 )   fki_txbroilereviction_detail_shed_id_fkey    INDEX     q   CREATE INDEX fki_txbroilereviction_detail_shed_id_fkey ON public.txbroilereviction_detail USING btree (shed_id);
 =   DROP INDEX public.fki_txbroilereviction_detail_shed_id_fkey;
       public         postgres    false    301            �           1259    28999 3   fki_txbroilereviction_detail_slaughterhouse_id_fkey    INDEX     �   CREATE INDEX fki_txbroilereviction_detail_slaughterhouse_id_fkey ON public.txbroilereviction_detail USING btree (slaughterhouse_id);
 G   DROP INDEX public.fki_txbroilereviction_detail_slaughterhouse_id_fkey;
       public         postgres    false    301            �           1259    29000 )   fki_txbroilereviction_partnership_id_fkey    INDEX     q   CREATE INDEX fki_txbroilereviction_partnership_id_fkey ON public.txbroilereviction USING btree (partnership_id);
 =   DROP INDEX public.fki_txbroilereviction_partnership_id_fkey;
       public         postgres    false    300            �           1259    29001 &   fki_txbroilereviction_scenario_id_fkey    INDEX     k   CREATE INDEX fki_txbroilereviction_scenario_id_fkey ON public.txbroilereviction USING btree (scenario_id);
 :   DROP INDEX public.fki_txbroilereviction_scenario_id_fkey;
       public         postgres    false    300            �           1259    29002 /   fki_txbroilerproduct_detail_broiler_detail_fkey    INDEX     }   CREATE INDEX fki_txbroilerproduct_detail_broiler_detail_fkey ON public.txbroilerproduct_detail USING btree (broiler_detail);
 C   DROP INDEX public.fki_txbroilerproduct_detail_broiler_detail_fkey;
       public         postgres    false    302            �           1259    29003 "   fki_txbroodermachines_farm_id_fkey    INDEX     b   CREATE INDEX fki_txbroodermachines_farm_id_fkey ON public.txbroodermachine USING btree (farm_id);
 6   DROP INDEX public.fki_txbroodermachines_farm_id_fkey;
       public         postgres    false    303            �           1259    29004 )   fki_txbroodermachines_partnership_id_fkey    INDEX     p   CREATE INDEX fki_txbroodermachines_partnership_id_fkey ON public.txbroodermachine USING btree (partnership_id);
 =   DROP INDEX public.fki_txbroodermachines_partnership_id_fkey;
       public         postgres    false    303            �           1259    29005 !   fki_txeggs_planning_breed_id_fkey    INDEX     a   CREATE INDEX fki_txeggs_planning_breed_id_fkey ON public.txeggs_planning USING btree (breed_id);
 5   DROP INDEX public.fki_txeggs_planning_breed_id_fkey;
       public         postgres    false    308            �           1259    29006 $   fki_txeggs_planning_scenario_id_fkey    INDEX     g   CREATE INDEX fki_txeggs_planning_scenario_id_fkey ON public.txeggs_planning USING btree (scenario_id);
 8   DROP INDEX public.fki_txeggs_planning_scenario_id_fkey;
       public         postgres    false    308            �           1259    29007 !   fki_txeggs_required_breed_id_fkey    INDEX     a   CREATE INDEX fki_txeggs_required_breed_id_fkey ON public.txeggs_required USING btree (breed_id);
 5   DROP INDEX public.fki_txeggs_required_breed_id_fkey;
       public         postgres    false    309            �           1259    29008 $   fki_txeggs_required_scenario_id_fkey    INDEX     g   CREATE INDEX fki_txeggs_required_scenario_id_fkey ON public.txeggs_required USING btree (scenario_id);
 8   DROP INDEX public.fki_txeggs_required_scenario_id_fkey;
       public         postgres    false    309            �           1259    29009     fki_txeggs_storage_breed_id_fkey    INDEX     _   CREATE INDEX fki_txeggs_storage_breed_id_fkey ON public.txeggs_storage USING btree (breed_id);
 4   DROP INDEX public.fki_txeggs_storage_breed_id_fkey;
       public         postgres    false    310            �           1259    29010 *   fki_txeggs_storage_incubator_plant_id_fkey    INDEX     s   CREATE INDEX fki_txeggs_storage_incubator_plant_id_fkey ON public.txeggs_storage USING btree (incubator_plant_id);
 >   DROP INDEX public.fki_txeggs_storage_incubator_plant_id_fkey;
       public         postgres    false    310            �           1259    29011 #   fki_txeggs_storage_scenario_id_fkey    INDEX     e   CREATE INDEX fki_txeggs_storage_scenario_id_fkey ON public.txeggs_storage USING btree (scenario_id);
 7   DROP INDEX public.fki_txeggs_storage_scenario_id_fkey;
       public         postgres    false    310            �           1259    29012    fki_txfattening_breed_id_fkey    INDEX     W   CREATE INDEX fki_txfattening_breed_id_fkey ON public.txbroiler USING btree (breed_id);
 1   DROP INDEX public.fki_txfattening_breed_id_fkey;
       public         postgres    false    298            �           1259    29013 #   fki_txfattening_partnership_id_fkey    INDEX     c   CREATE INDEX fki_txfattening_partnership_id_fkey ON public.txbroiler USING btree (partnership_id);
 7   DROP INDEX public.fki_txfattening_partnership_id_fkey;
       public         postgres    false    298            �           1259    29014     fki_txfattening_scenario_id_fkey    INDEX     ]   CREATE INDEX fki_txfattening_scenario_id_fkey ON public.txbroiler USING btree (scenario_id);
 4   DROP INDEX public.fki_txfattening_scenario_id_fkey;
       public         postgres    false    298            �           1259    29015    fki_txgoals_erp_product_id_fkey    INDEX     ]   CREATE INDEX fki_txgoals_erp_product_id_fkey ON public.txgoals_erp USING btree (product_id);
 3   DROP INDEX public.fki_txgoals_erp_product_id_fkey;
       public         postgres    false    311            �           1259    29016     fki_txgoals_erp_scenario_id_fkey    INDEX     _   CREATE INDEX fki_txgoals_erp_scenario_id_fkey ON public.txgoals_erp USING btree (scenario_id);
 4   DROP INDEX public.fki_txgoals_erp_scenario_id_fkey;
       public         postgres    false    311            �           1259    29017    fki_txhousingway_breed_id_fkey    INDEX     [   CREATE INDEX fki_txhousingway_breed_id_fkey ON public.txhousingway USING btree (breed_id);
 2   DROP INDEX public.fki_txhousingway_breed_id_fkey;
       public         postgres    false    313            �           1259    29018 $   fki_txhousingway_detail_farm_id_fkey    INDEX     g   CREATE INDEX fki_txhousingway_detail_farm_id_fkey ON public.txhousingway_detail USING btree (farm_id);
 8   DROP INDEX public.fki_txhousingway_detail_farm_id_fkey;
       public         postgres    false    314            �           1259    29019 +   fki_txhousingway_detail_housing_way_id_fkey    INDEX     u   CREATE INDEX fki_txhousingway_detail_housing_way_id_fkey ON public.txhousingway_detail USING btree (housing_way_id);
 ?   DROP INDEX public.fki_txhousingway_detail_housing_way_id_fkey;
       public         postgres    false    314            �           1259    29020 /   fki_txhousingway_detail_incubator_plant_id_fkey    INDEX     }   CREATE INDEX fki_txhousingway_detail_incubator_plant_id_fkey ON public.txhousingway_detail USING btree (incubator_plant_id);
 C   DROP INDEX public.fki_txhousingway_detail_incubator_plant_id_fkey;
       public         postgres    false    314                        1259    29021 $   fki_txhousingway_detail_shed_id_fkey    INDEX     g   CREATE INDEX fki_txhousingway_detail_shed_id_fkey ON public.txhousingway_detail USING btree (shed_id);
 8   DROP INDEX public.fki_txhousingway_detail_shed_id_fkey;
       public         postgres    false    314            �           1259    29022 $   fki_txhousingway_partnership_id_fkey    INDEX     g   CREATE INDEX fki_txhousingway_partnership_id_fkey ON public.txhousingway USING btree (partnership_id);
 8   DROP INDEX public.fki_txhousingway_partnership_id_fkey;
       public         postgres    false    313            �           1259    29023 !   fki_txhousingway_scenario_id_fkey    INDEX     a   CREATE INDEX fki_txhousingway_scenario_id_fkey ON public.txhousingway USING btree (scenario_id);
 5   DROP INDEX public.fki_txhousingway_scenario_id_fkey;
       public         postgres    false    313            �           1259    29024    fki_txhousingway_stage_id_fkey    INDEX     [   CREATE INDEX fki_txhousingway_stage_id_fkey ON public.txhousingway USING btree (stage_id);
 2   DROP INDEX public.fki_txhousingway_stage_id_fkey;
       public         postgres    false    313                       1259    29025    fki_txlot_breed_id_fkey    INDEX     M   CREATE INDEX fki_txlot_breed_id_fkey ON public.txlot USING btree (breed_id);
 +   DROP INDEX public.fki_txlot_breed_id_fkey;
       public         postgres    false    315                       1259    29026    fki_txlot_farm_id_fkey    INDEX     K   CREATE INDEX fki_txlot_farm_id_fkey ON public.txlot USING btree (farm_id);
 *   DROP INDEX public.fki_txlot_farm_id_fkey;
       public         postgres    false    315                       1259    29027    fki_txlot_housin_way_id_fkey    INDEX     X   CREATE INDEX fki_txlot_housin_way_id_fkey ON public.txlot USING btree (housing_way_id);
 0   DROP INDEX public.fki_txlot_housin_way_id_fkey;
       public         postgres    false    315                       1259    29028    fki_txlot_product_id_fkey    INDEX     Q   CREATE INDEX fki_txlot_product_id_fkey ON public.txlot USING btree (product_id);
 -   DROP INDEX public.fki_txlot_product_id_fkey;
       public         postgres    false    315                       1259    29029    fki_txlot_shed_id_fkey    INDEX     K   CREATE INDEX fki_txlot_shed_id_fkey ON public.txlot USING btree (shed_id);
 *   DROP INDEX public.fki_txlot_shed_id_fkey;
       public         postgres    false    315                       1259    29030     fki_txposturecurve_breed_id_fkey    INDEX     _   CREATE INDEX fki_txposturecurve_breed_id_fkey ON public.txposturecurve USING btree (breed_id);
 4   DROP INDEX public.fki_txposturecurve_breed_id_fkey;
       public         postgres    false    317                       1259    29031 #   fki_txprogrammed_eggs_breed_id_fkey    INDEX     e   CREATE INDEX fki_txprogrammed_eggs_breed_id_fkey ON public.txprogrammed_eggs USING btree (breed_id);
 7   DROP INDEX public.fki_txprogrammed_eggs_breed_id_fkey;
       public         postgres    false    318                       1259    29032 *   fki_txprogrammed_eggs_eggs_storage_id_fkey    INDEX     s   CREATE INDEX fki_txprogrammed_eggs_eggs_storage_id_fkey ON public.txprogrammed_eggs USING btree (eggs_storage_id);
 >   DROP INDEX public.fki_txprogrammed_eggs_eggs_storage_id_fkey;
       public         postgres    false    318                       1259    29033 '   fki_txprogrammed_eggs_incubator_id_fkey    INDEX     m   CREATE INDEX fki_txprogrammed_eggs_incubator_id_fkey ON public.txprogrammed_eggs USING btree (incubator_id);
 ;   DROP INDEX public.fki_txprogrammed_eggs_incubator_id_fkey;
       public         postgres    false    318                       1259    29034 %   fki_txscenarioformula_measure_id_fkey    INDEX     i   CREATE INDEX fki_txscenarioformula_measure_id_fkey ON public.txscenarioformula USING btree (measure_id);
 9   DROP INDEX public.fki_txscenarioformula_measure_id_fkey;
       public         postgres    false    319                       1259    29035 '   fki_txscenarioformula_parameter_id_fkey    INDEX     m   CREATE INDEX fki_txscenarioformula_parameter_id_fkey ON public.txscenarioformula USING btree (parameter_id);
 ;   DROP INDEX public.fki_txscenarioformula_parameter_id_fkey;
       public         postgres    false    319                       1259    29036 %   fki_txscenarioformula_process_id_fkey    INDEX     i   CREATE INDEX fki_txscenarioformula_process_id_fkey ON public.txscenarioformula USING btree (process_id);
 9   DROP INDEX public.fki_txscenarioformula_process_id_fkey;
       public         postgres    false    319                       1259    29037 &   fki_txscenarioformula_scenario_id_fkey    INDEX     k   CREATE INDEX fki_txscenarioformula_scenario_id_fkey ON public.txscenarioformula USING btree (scenario_id);
 :   DROP INDEX public.fki_txscenarioformula_scenario_id_fkey;
       public         postgres    false    319                       1259    29038 )   fki_txscenarioparameter_parameter_id_fkey    INDEX     q   CREATE INDEX fki_txscenarioparameter_parameter_id_fkey ON public.txscenarioparameter USING btree (parameter_id);
 =   DROP INDEX public.fki_txscenarioparameter_parameter_id_fkey;
       public         postgres    false    320                       1259    29039 '   fki_txscenarioparameter_process_id_fkey    INDEX     m   CREATE INDEX fki_txscenarioparameter_process_id_fkey ON public.txscenarioparameter USING btree (process_id);
 ;   DROP INDEX public.fki_txscenarioparameter_process_id_fkey;
       public         postgres    false    320                       1259    29040 (   fki_txscenarioparameter_scenario_id_fkey    INDEX     o   CREATE INDEX fki_txscenarioparameter_scenario_id_fkey ON public.txscenarioparameter USING btree (scenario_id);
 <   DROP INDEX public.fki_txscenarioparameter_scenario_id_fkey;
       public         postgres    false    320            !           1259    29041 ,   fki_txscenarioparameterday_parameter_id_fkey    INDEX     w   CREATE INDEX fki_txscenarioparameterday_parameter_id_fkey ON public.txscenarioparameterday USING btree (parameter_id);
 @   DROP INDEX public.fki_txscenarioparameterday_parameter_id_fkey;
       public         postgres    false    321            "           1259    29042 +   fki_txscenarioparameterday_scenario_id_fkey    INDEX     u   CREATE INDEX fki_txscenarioparameterday_scenario_id_fkey ON public.txscenarioparameterday USING btree (scenario_id);
 ?   DROP INDEX public.fki_txscenarioparameterday_scenario_id_fkey;
       public         postgres    false    321            %           1259    29043 (   fki_txscenarioposturecurve_breed_id_fkey    INDEX     o   CREATE INDEX fki_txscenarioposturecurve_breed_id_fkey ON public.txscenarioposturecurve USING btree (breed_id);
 <   DROP INDEX public.fki_txscenarioposturecurve_breed_id_fkey;
       public         postgres    false    322            &           1259    29044 4   fki_txscenarioposturecurve_housingway_detail_id_fkey    INDEX     �   CREATE INDEX fki_txscenarioposturecurve_housingway_detail_id_fkey ON public.txscenarioposturecurve USING btree (housingway_detail_id);
 H   DROP INDEX public.fki_txscenarioposturecurve_housingway_detail_id_fkey;
       public         postgres    false    322            '           1259    29045 +   fki_txscenarioposturecurve_scenario_id_fkey    INDEX     u   CREATE INDEX fki_txscenarioposturecurve_scenario_id_fkey ON public.txscenarioposturecurve USING btree (scenario_id);
 ?   DROP INDEX public.fki_txscenarioposturecurve_scenario_id_fkey;
       public         postgres    false    322            +           1259    29046 %   fki_txscenarioprocess_process_id_fkey    INDEX     i   CREATE INDEX fki_txscenarioprocess_process_id_fkey ON public.txscenarioprocess USING btree (process_id);
 9   DROP INDEX public.fki_txscenarioprocess_process_id_fkey;
       public         postgres    false    323            ,           1259    29047 &   fki_txscenarioprocess_scenario_id_fkey    INDEX     k   CREATE INDEX fki_txscenarioprocess_scenario_id_fkey ON public.txscenarioprocess USING btree (scenario_id);
 :   DROP INDEX public.fki_txscenarioprocess_scenario_id_fkey;
       public         postgres    false    323            (           1259    29048    posturedate_index    INDEX     [   CREATE INDEX posturedate_index ON public.txscenarioposturecurve USING hash (posture_date);
 %   DROP INDEX public.posturedate_index;
       public         postgres    false    322            �           1259    29049    sequence_index    INDEX     L   CREATE INDEX sequence_index ON public.txcalendarday USING btree (sequence);
 "   DROP INDEX public.sequence_index;
       public         postgres    false    305            9           2606    29050 ;   aba_stages_of_breeds_and_stages FK_id_aba_breeds_and_stages    FK CONSTRAINT     �   ALTER TABLE ONLY public.aba_stages_of_breeds_and_stages
    ADD CONSTRAINT "FK_id_aba_breeds_and_stages" FOREIGN KEY (id_aba_breeds_and_stages) REFERENCES public.aba_breeds_and_stages(id) ON DELETE CASCADE;
 g   ALTER TABLE ONLY public.aba_stages_of_breeds_and_stages DROP CONSTRAINT "FK_id_aba_breeds_and_stages";
       public       postgres    false    214    198    3117            /           2606    29055 9   aba_breeds_and_stages FK_id_aba_consumption_and_mortality    FK CONSTRAINT     �   ALTER TABLE ONLY public.aba_breeds_and_stages
    ADD CONSTRAINT "FK_id_aba_consumption_and_mortality" FOREIGN KEY (id_aba_consumption_and_mortality) REFERENCES public.aba_consumption_and_mortality(id);
 e   ALTER TABLE ONLY public.aba_breeds_and_stages DROP CONSTRAINT "FK_id_aba_consumption_and_mortality";
       public       postgres    false    200    198    3121            4           2606    29060 I   aba_consumption_and_mortality_detail FK_id_aba_consumption_and_mortality2    FK CONSTRAINT     �   ALTER TABLE ONLY public.aba_consumption_and_mortality_detail
    ADD CONSTRAINT "FK_id_aba_consumption_and_mortality2" FOREIGN KEY (id_aba_consumption_and_mortality) REFERENCES public.aba_consumption_and_mortality(id) ON DELETE CASCADE;
 u   ALTER TABLE ONLY public.aba_consumption_and_mortality_detail DROP CONSTRAINT "FK_id_aba_consumption_and_mortality2";
       public       postgres    false    202    3121    200            5           2606    29065 1   aba_elements_and_concentrations FK_id_aba_element    FK CONSTRAINT     �   ALTER TABLE ONLY public.aba_elements_and_concentrations
    ADD CONSTRAINT "FK_id_aba_element" FOREIGN KEY (id_aba_element) REFERENCES public.aba_elements(id);
 ]   ALTER TABLE ONLY public.aba_elements_and_concentrations DROP CONSTRAINT "FK_id_aba_element";
       public       postgres    false    206    3129    204            8           2606    29070    aba_results FK_id_aba_element2    FK CONSTRAINT     �   ALTER TABLE ONLY public.aba_results
    ADD CONSTRAINT "FK_id_aba_element2" FOREIGN KEY (id_aba_element) REFERENCES public.aba_elements(id);
 J   ALTER TABLE ONLY public.aba_results DROP CONSTRAINT "FK_id_aba_element2";
       public       postgres    false    3129    204    212            6           2606    29075 :   aba_elements_and_concentrations FK_id_aba_element_property    FK CONSTRAINT     �   ALTER TABLE ONLY public.aba_elements_and_concentrations
    ADD CONSTRAINT "FK_id_aba_element_property" FOREIGN KEY (id_aba_element_property) REFERENCES public.aba_elements_properties(id);
 f   ALTER TABLE ONLY public.aba_elements_and_concentrations DROP CONSTRAINT "FK_id_aba_element_property";
       public       postgres    false    3136    208    206            7           2606    29080 5   aba_elements_and_concentrations FK_id_aba_formulation    FK CONSTRAINT     �   ALTER TABLE ONLY public.aba_elements_and_concentrations
    ADD CONSTRAINT "FK_id_aba_formulation" FOREIGN KEY (id_aba_formulation) REFERENCES public.aba_formulation(id) ON DELETE CASCADE;
 a   ALTER TABLE ONLY public.aba_elements_and_concentrations DROP CONSTRAINT "FK_id_aba_formulation";
       public       postgres    false    3138    206    210            1           2606    29085 1   aba_consumption_and_mortality FK_id_aba_time_unit    FK CONSTRAINT     �   ALTER TABLE ONLY public.aba_consumption_and_mortality
    ADD CONSTRAINT "FK_id_aba_time_unit" FOREIGN KEY (id_aba_time_unit) REFERENCES public.aba_time_unit(id);
 ]   ALTER TABLE ONLY public.aba_consumption_and_mortality DROP CONSTRAINT "FK_id_aba_time_unit";
       public       postgres    false    3147    200    215            2           2606    29090 )   aba_consumption_and_mortality FK_id_breed    FK CONSTRAINT     �   ALTER TABLE ONLY public.aba_consumption_and_mortality
    ADD CONSTRAINT "FK_id_breed" FOREIGN KEY (id_breed) REFERENCES public.mdbreed(breed_id);
 U   ALTER TABLE ONLY public.aba_consumption_and_mortality DROP CONSTRAINT "FK_id_breed";
       public       postgres    false    250    200    3157            :           2606    29095 1   aba_stages_of_breeds_and_stages FK_id_formulation    FK CONSTRAINT     �   ALTER TABLE ONLY public.aba_stages_of_breeds_and_stages
    ADD CONSTRAINT "FK_id_formulation" FOREIGN KEY (id_formulation) REFERENCES public.aba_formulation(id);
 ]   ALTER TABLE ONLY public.aba_stages_of_breeds_and_stages DROP CONSTRAINT "FK_id_formulation";
       public       postgres    false    214    3138    210            0           2606    29100 #   aba_breeds_and_stages FK_id_process    FK CONSTRAINT     �   ALTER TABLE ONLY public.aba_breeds_and_stages
    ADD CONSTRAINT "FK_id_process" FOREIGN KEY (id_process) REFERENCES public.mdprocess(process_id);
 O   ALTER TABLE ONLY public.aba_breeds_and_stages DROP CONSTRAINT "FK_id_process";
       public       postgres    false    258    198    3173            3           2606    29105 )   aba_consumption_and_mortality FK_id_stage    FK CONSTRAINT     �   ALTER TABLE ONLY public.aba_consumption_and_mortality
    ADD CONSTRAINT "FK_id_stage" FOREIGN KEY (id_stage) REFERENCES public.mdstage(stage_id);
 U   ALTER TABLE ONLY public.aba_consumption_and_mortality DROP CONSTRAINT "FK_id_stage";
       public       postgres    false    268    3192    200            x           2606    29110 *   txeggs_movements eggs_movements_storage_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.txeggs_movements
    ADD CONSTRAINT eggs_movements_storage_id FOREIGN KEY (eggs_storage_id) REFERENCES public.txeggs_storage(eggs_storage_id);
 T   ALTER TABLE ONLY public.txeggs_movements DROP CONSTRAINT eggs_movements_storage_id;
       public       postgres    false    307    3314    310            ;           2606    29115 7   mdapplication_rol mdapplication_rol_application_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.mdapplication_rol
    ADD CONSTRAINT mdapplication_rol_application_id_fkey FOREIGN KEY (application_id) REFERENCES public.mdapplication(application_id);
 a   ALTER TABLE ONLY public.mdapplication_rol DROP CONSTRAINT mdapplication_rol_application_id_fkey;
       public       postgres    false    3149    247    249            <           2606    29120 /   mdapplication_rol mdapplication_rol_rol_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.mdapplication_rol
    ADD CONSTRAINT mdapplication_rol_rol_id_fkey FOREIGN KEY (rol_id) REFERENCES public.mdrol(rol_id);
 Y   ALTER TABLE ONLY public.mdapplication_rol DROP CONSTRAINT mdapplication_rol_rol_id_fkey;
       public       postgres    false    249    262    3179            =           2606    29125 '   mdparameter mdparameter_measure_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.mdparameter
    ADD CONSTRAINT mdparameter_measure_id_fkey FOREIGN KEY (measure_id) REFERENCES public.mdmeasure(measure_id) ON UPDATE CASCADE ON DELETE CASCADE;
 Q   ALTER TABLE ONLY public.mdparameter DROP CONSTRAINT mdparameter_measure_id_fkey;
       public       postgres    false    3163    254    256            >           2606    29130 '   mdparameter mdparameter_process_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.mdparameter
    ADD CONSTRAINT mdparameter_process_id_fkey FOREIGN KEY (process_id) REFERENCES public.mdprocess(process_id) ON UPDATE CASCADE ON DELETE CASCADE;
 Q   ALTER TABLE ONLY public.mdparameter DROP CONSTRAINT mdparameter_process_id_fkey;
       public       postgres    false    256    3173    258            ?           2606    29135 !   mdprocess mdprocess_breed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.mdprocess
    ADD CONSTRAINT mdprocess_breed_id_fkey FOREIGN KEY (breed_id) REFERENCES public.mdbreed(breed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 K   ALTER TABLE ONLY public.mdprocess DROP CONSTRAINT mdprocess_breed_id_fkey;
       public       postgres    false    3157    250    258            @           2606    29140 $   mdprocess mdprocess_calendar_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.mdprocess
    ADD CONSTRAINT mdprocess_calendar_id_fkey FOREIGN KEY (calendar_id) REFERENCES public.txcalendar(calendar_id) ON UPDATE CASCADE ON DELETE CASCADE;
 N   ALTER TABLE ONLY public.mdprocess DROP CONSTRAINT mdprocess_calendar_id_fkey;
       public       postgres    false    304    3294    258            A           2606    29145 #   mdprocess mdprocess_product_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.mdprocess
    ADD CONSTRAINT mdprocess_product_id_fkey FOREIGN KEY (product_id) REFERENCES public.mdproduct(product_id) ON UPDATE CASCADE ON DELETE CASCADE;
 M   ALTER TABLE ONLY public.mdprocess DROP CONSTRAINT mdprocess_product_id_fkey;
       public       postgres    false    258    3177    260            B           2606    29150 !   mdprocess mdprocess_stage_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.mdprocess
    ADD CONSTRAINT mdprocess_stage_id_fkey FOREIGN KEY (stage_id) REFERENCES public.mdstage(stage_id) ON UPDATE CASCADE ON DELETE CASCADE;
 K   ALTER TABLE ONLY public.mdprocess DROP CONSTRAINT mdprocess_stage_id_fkey;
       public       postgres    false    3192    258    268            C           2606    29155 &   mdscenario mdscenario_calendar_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.mdscenario
    ADD CONSTRAINT mdscenario_calendar_id_fkey FOREIGN KEY (calendar_id) REFERENCES public.txcalendar(calendar_id);
 P   ALTER TABLE ONLY public.mdscenario DROP CONSTRAINT mdscenario_calendar_id_fkey;
       public       postgres    false    304    3294    264            D           2606    29160    mduser mduser_rol_id_fkey    FK CONSTRAINT     {   ALTER TABLE ONLY public.mduser
    ADD CONSTRAINT mduser_rol_id_fkey FOREIGN KEY (rol_id) REFERENCES public.mdrol(rol_id);
 C   ALTER TABLE ONLY public.mduser DROP CONSTRAINT mduser_rol_id_fkey;
       public       postgres    false    3179    262    270            E           2606    29165    oscenter oscenter_farm_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.oscenter
    ADD CONSTRAINT oscenter_farm_id_fkey FOREIGN KEY (farm_id) REFERENCES public.osfarm(farm_id) ON UPDATE CASCADE ON DELETE CASCADE;
 H   ALTER TABLE ONLY public.oscenter DROP CONSTRAINT oscenter_farm_id_fkey;
       public       postgres    false    273    271    3215            G           2606    29170 8   oscenter_oswarehouse oscenter_oswarehouse_center_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.oscenter_oswarehouse
    ADD CONSTRAINT oscenter_oswarehouse_center_id_fkey FOREIGN KEY (center_id) REFERENCES public.oscenter(center_id) ON UPDATE CASCADE ON DELETE CASCADE;
 b   ALTER TABLE ONLY public.oscenter_oswarehouse DROP CONSTRAINT oscenter_oswarehouse_center_id_fkey;
       public       postgres    false    3203    271    272            H           2606    29175 6   oscenter_oswarehouse oscenter_oswarehouse_farm_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.oscenter_oswarehouse
    ADD CONSTRAINT oscenter_oswarehouse_farm_id_fkey FOREIGN KEY (farm_id) REFERENCES public.osfarm(farm_id) ON UPDATE CASCADE ON DELETE CASCADE;
 `   ALTER TABLE ONLY public.oscenter_oswarehouse DROP CONSTRAINT oscenter_oswarehouse_farm_id_fkey;
       public       postgres    false    273    3215    272            I           2606    29180 =   oscenter_oswarehouse oscenter_oswarehouse_partnership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.oscenter_oswarehouse
    ADD CONSTRAINT oscenter_oswarehouse_partnership_id_fkey FOREIGN KEY (partnership_id) REFERENCES public.ospartnership(partnership_id) ON UPDATE CASCADE ON DELETE CASCADE;
 g   ALTER TABLE ONLY public.oscenter_oswarehouse DROP CONSTRAINT oscenter_oswarehouse_partnership_id_fkey;
       public       postgres    false    277    3229    272            J           2606    29185 ;   oscenter_oswarehouse oscenter_oswarehouse_warehouse_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.oscenter_oswarehouse
    ADD CONSTRAINT oscenter_oswarehouse_warehouse_id_fkey FOREIGN KEY (warehouse_id) REFERENCES public.oswarehouse(warehouse_id) ON UPDATE CASCADE ON DELETE CASCADE;
 e   ALTER TABLE ONLY public.oscenter_oswarehouse DROP CONSTRAINT oscenter_oswarehouse_warehouse_id_fkey;
       public       postgres    false    272    3257    286            F           2606    29190 %   oscenter oscenter_partnership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.oscenter
    ADD CONSTRAINT oscenter_partnership_id_fkey FOREIGN KEY (partnership_id) REFERENCES public.ospartnership(partnership_id) ON UPDATE CASCADE ON DELETE CASCADE;
 O   ALTER TABLE ONLY public.oscenter DROP CONSTRAINT oscenter_partnership_id_fkey;
       public       postgres    false    271    277    3229            K           2606    29195    osfarm osfarm_farm_type_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.osfarm
    ADD CONSTRAINT osfarm_farm_type_id_fkey FOREIGN KEY (farm_type_id) REFERENCES public.mdfarmtype(farm_type_id) ON UPDATE CASCADE ON DELETE CASCADE;
 I   ALTER TABLE ONLY public.osfarm DROP CONSTRAINT osfarm_farm_type_id_fkey;
       public       postgres    false    273    252    3161            L           2606    29200 !   osfarm osfarm_partnership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.osfarm
    ADD CONSTRAINT osfarm_partnership_id_fkey FOREIGN KEY (partnership_id) REFERENCES public.ospartnership(partnership_id) ON UPDATE CASCADE ON DELETE CASCADE;
 K   ALTER TABLE ONLY public.osfarm DROP CONSTRAINT osfarm_partnership_id_fkey;
       public       postgres    false    277    273    3229            M           2606    29205 /   osincubator osincubator_incubator_plant_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.osincubator
    ADD CONSTRAINT osincubator_incubator_plant_id_fkey FOREIGN KEY (incubator_plant_id) REFERENCES public.osincubatorplant(incubator_plant_id) ON UPDATE CASCADE ON DELETE CASCADE;
 Y   ALTER TABLE ONLY public.osincubator DROP CONSTRAINT osincubator_incubator_plant_id_fkey;
       public       postgres    false    275    3225    274            N           2606    29210 5   osincubatorplant osincubatorplant_partnership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.osincubatorplant
    ADD CONSTRAINT osincubatorplant_partnership_id_fkey FOREIGN KEY (partnership_id) REFERENCES public.ospartnership(partnership_id) ON UPDATE CASCADE ON DELETE CASCADE;
 _   ALTER TABLE ONLY public.osincubatorplant DROP CONSTRAINT osincubatorplant_partnership_id_fkey;
       public       postgres    false    275    277    3229            O           2606    29215    osshed osshed_center_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.osshed
    ADD CONSTRAINT osshed_center_id_fkey FOREIGN KEY (center_id) REFERENCES public.oscenter(center_id) ON UPDATE CASCADE ON DELETE CASCADE;
 F   ALTER TABLE ONLY public.osshed DROP CONSTRAINT osshed_center_id_fkey;
       public       postgres    false    279    271    3203            P           2606    29220    osshed osshed_farm_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.osshed
    ADD CONSTRAINT osshed_farm_id_fkey FOREIGN KEY (farm_id) REFERENCES public.osfarm(farm_id) ON UPDATE CASCADE ON DELETE CASCADE;
 D   ALTER TABLE ONLY public.osshed DROP CONSTRAINT osshed_farm_id_fkey;
       public       postgres    false    3215    279    273            Q           2606    29225 !   osshed osshed_partnership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.osshed
    ADD CONSTRAINT osshed_partnership_id_fkey FOREIGN KEY (partnership_id) REFERENCES public.ospartnership(partnership_id) ON UPDATE CASCADE ON DELETE CASCADE;
 K   ALTER TABLE ONLY public.osshed DROP CONSTRAINT osshed_partnership_id_fkey;
       public       postgres    false    3229    279    277            R           2606    29230     osshed osshed_statusshed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.osshed
    ADD CONSTRAINT osshed_statusshed_id_fkey FOREIGN KEY (statusshed_id) REFERENCES public.mdshedstatus(shed_status_id) ON UPDATE CASCADE ON DELETE CASCADE;
 J   ALTER TABLE ONLY public.osshed DROP CONSTRAINT osshed_statusshed_id_fkey;
       public       postgres    false    3188    279    266            S           2606    29235    ossilo ossilo_center_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ossilo
    ADD CONSTRAINT ossilo_center_id_fkey FOREIGN KEY (center_id) REFERENCES public.oscenter(center_id) ON UPDATE CASCADE ON DELETE CASCADE;
 F   ALTER TABLE ONLY public.ossilo DROP CONSTRAINT ossilo_center_id_fkey;
       public       postgres    false    3203    281    271            T           2606    29240    ossilo ossilo_farm_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ossilo
    ADD CONSTRAINT ossilo_farm_id_fkey FOREIGN KEY (farm_id) REFERENCES public.osfarm(farm_id) ON UPDATE CASCADE ON DELETE CASCADE;
 D   ALTER TABLE ONLY public.ossilo DROP CONSTRAINT ossilo_farm_id_fkey;
       public       postgres    false    3215    281    273            V           2606    29245 *   ossilo_osshed ossilo_osshed_center_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ossilo_osshed
    ADD CONSTRAINT ossilo_osshed_center_id_fkey FOREIGN KEY (center_id) REFERENCES public.oscenter(center_id) ON UPDATE CASCADE ON DELETE CASCADE;
 T   ALTER TABLE ONLY public.ossilo_osshed DROP CONSTRAINT ossilo_osshed_center_id_fkey;
       public       postgres    false    3203    282    271            W           2606    29250 (   ossilo_osshed ossilo_osshed_farm_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ossilo_osshed
    ADD CONSTRAINT ossilo_osshed_farm_id_fkey FOREIGN KEY (farm_id) REFERENCES public.osfarm(farm_id) ON UPDATE CASCADE ON DELETE CASCADE;
 R   ALTER TABLE ONLY public.ossilo_osshed DROP CONSTRAINT ossilo_osshed_farm_id_fkey;
       public       postgres    false    3215    282    273            X           2606    29255 /   ossilo_osshed ossilo_osshed_partnership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ossilo_osshed
    ADD CONSTRAINT ossilo_osshed_partnership_id_fkey FOREIGN KEY (partnership_id) REFERENCES public.ospartnership(partnership_id) ON UPDATE CASCADE ON DELETE CASCADE;
 Y   ALTER TABLE ONLY public.ossilo_osshed DROP CONSTRAINT ossilo_osshed_partnership_id_fkey;
       public       postgres    false    282    277    3229            Y           2606    29260 (   ossilo_osshed ossilo_osshed_shed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ossilo_osshed
    ADD CONSTRAINT ossilo_osshed_shed_id_fkey FOREIGN KEY (shed_id) REFERENCES public.osshed(shed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 R   ALTER TABLE ONLY public.ossilo_osshed DROP CONSTRAINT ossilo_osshed_shed_id_fkey;
       public       postgres    false    3235    279    282            Z           2606    29265 (   ossilo_osshed ossilo_osshed_silo_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ossilo_osshed
    ADD CONSTRAINT ossilo_osshed_silo_id_fkey FOREIGN KEY (silo_id) REFERENCES public.ossilo(silo_id) ON UPDATE CASCADE ON DELETE CASCADE;
 R   ALTER TABLE ONLY public.ossilo_osshed DROP CONSTRAINT ossilo_osshed_silo_id_fkey;
       public       postgres    false    281    3242    282            U           2606    29270 !   ossilo ossilo_partnership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ossilo
    ADD CONSTRAINT ossilo_partnership_id_fkey FOREIGN KEY (partnership_id) REFERENCES public.ospartnership(partnership_id) ON UPDATE CASCADE ON DELETE CASCADE;
 K   ALTER TABLE ONLY public.ossilo DROP CONSTRAINT ossilo_partnership_id_fkey;
       public       postgres    false    281    277    3229            [           2606    29275 $   oswarehouse oswarehouse_farm_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.oswarehouse
    ADD CONSTRAINT oswarehouse_farm_id_fkey FOREIGN KEY (farm_id) REFERENCES public.osfarm(farm_id) ON UPDATE CASCADE ON DELETE CASCADE;
 N   ALTER TABLE ONLY public.oswarehouse DROP CONSTRAINT oswarehouse_farm_id_fkey;
       public       postgres    false    3215    286    273            \           2606    29280 +   oswarehouse oswarehouse_partnership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.oswarehouse
    ADD CONSTRAINT oswarehouse_partnership_id_fkey FOREIGN KEY (partnership_id) REFERENCES public.ospartnership(partnership_id) ON UPDATE CASCADE ON DELETE CASCADE;
 U   ALTER TABLE ONLY public.oswarehouse DROP CONSTRAINT oswarehouse_partnership_id_fkey;
       public       postgres    false    3229    286    277            ]           2606    29285 5   txavailabilitysheds txavailabilitysheds_lot_code_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txavailabilitysheds
    ADD CONSTRAINT txavailabilitysheds_lot_code_fkey FOREIGN KEY (lot_code) REFERENCES public.txlot(lot_code) ON UPDATE CASCADE ON DELETE CASCADE;
 _   ALTER TABLE ONLY public.txavailabilitysheds DROP CONSTRAINT txavailabilitysheds_lot_code_fkey;
       public       postgres    false    3337    315    297            ^           2606    29290 4   txavailabilitysheds txavailabilitysheds_shed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txavailabilitysheds
    ADD CONSTRAINT txavailabilitysheds_shed_id_fkey FOREIGN KEY (shed_id) REFERENCES public.osshed(shed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 ^   ALTER TABLE ONLY public.txavailabilitysheds DROP CONSTRAINT txavailabilitysheds_shed_id_fkey;
       public       postgres    false    3235    279    297            _           2606    29295 !   txbroiler txbroiler_breed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroiler
    ADD CONSTRAINT txbroiler_breed_id_fkey FOREIGN KEY (breed_id) REFERENCES public.mdbreed(breed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 K   ALTER TABLE ONLY public.txbroiler DROP CONSTRAINT txbroiler_breed_id_fkey;
       public       postgres    false    298    3157    250            c           2606    29300 1   txbroiler_detail txbroiler_detail_broiler_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroiler_detail
    ADD CONSTRAINT txbroiler_detail_broiler_id_fkey FOREIGN KEY (broiler_id) REFERENCES public.txbroiler(broiler_id) ON UPDATE CASCADE ON DELETE CASCADE;
 [   ALTER TABLE ONLY public.txbroiler_detail DROP CONSTRAINT txbroiler_detail_broiler_id_fkey;
       public       postgres    false    298    299    3267            d           2606    29305 0   txbroiler_detail txbroiler_detail_center_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroiler_detail
    ADD CONSTRAINT txbroiler_detail_center_id_fkey FOREIGN KEY (center_id) REFERENCES public.oscenter(center_id);
 Z   ALTER TABLE ONLY public.txbroiler_detail DROP CONSTRAINT txbroiler_detail_center_id_fkey;
       public       postgres    false    271    299    3203            e           2606    29310 9   txbroiler_detail txbroiler_detail_executioncenter_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroiler_detail
    ADD CONSTRAINT txbroiler_detail_executioncenter_id_fkey FOREIGN KEY (executioncenter_id) REFERENCES public.oscenter(center_id);
 c   ALTER TABLE ONLY public.txbroiler_detail DROP CONSTRAINT txbroiler_detail_executioncenter_id_fkey;
       public       postgres    false    3203    299    271            f           2606    29315 7   txbroiler_detail txbroiler_detail_executionfarm_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroiler_detail
    ADD CONSTRAINT txbroiler_detail_executionfarm_id_fkey FOREIGN KEY (executionfarm_id) REFERENCES public.osfarm(farm_id);
 a   ALTER TABLE ONLY public.txbroiler_detail DROP CONSTRAINT txbroiler_detail_executionfarm_id_fkey;
       public       postgres    false    299    3215    273            g           2606    29320 7   txbroiler_detail txbroiler_detail_executionshed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroiler_detail
    ADD CONSTRAINT txbroiler_detail_executionshed_id_fkey FOREIGN KEY (executionshed_id) REFERENCES public.osshed(shed_id);
 a   ALTER TABLE ONLY public.txbroiler_detail DROP CONSTRAINT txbroiler_detail_executionshed_id_fkey;
       public       postgres    false    3235    279    299            h           2606    29325 .   txbroiler_detail txbroiler_detail_farm_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroiler_detail
    ADD CONSTRAINT txbroiler_detail_farm_id_fkey FOREIGN KEY (farm_id) REFERENCES public.osfarm(farm_id) ON UPDATE CASCADE ON DELETE CASCADE;
 X   ALTER TABLE ONLY public.txbroiler_detail DROP CONSTRAINT txbroiler_detail_farm_id_fkey;
       public       postgres    false    299    273    3215            i           2606    29330 .   txbroiler_detail txbroiler_detail_shed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroiler_detail
    ADD CONSTRAINT txbroiler_detail_shed_id_fkey FOREIGN KEY (shed_id) REFERENCES public.osshed(shed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 X   ALTER TABLE ONLY public.txbroiler_detail DROP CONSTRAINT txbroiler_detail_shed_id_fkey;
       public       postgres    false    279    299    3235            `           2606    29335 '   txbroiler txbroiler_partnership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroiler
    ADD CONSTRAINT txbroiler_partnership_id_fkey FOREIGN KEY (partnership_id) REFERENCES public.ospartnership(partnership_id) ON UPDATE CASCADE ON DELETE CASCADE;
 Q   ALTER TABLE ONLY public.txbroiler DROP CONSTRAINT txbroiler_partnership_id_fkey;
       public       postgres    false    3229    277    298            a           2606    29340 +   txbroiler txbroiler_programmed_eggs_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroiler
    ADD CONSTRAINT txbroiler_programmed_eggs_id_fkey FOREIGN KEY (programmed_eggs_id) REFERENCES public.txprogrammed_eggs(programmed_eggs_id) ON UPDATE CASCADE ON DELETE CASCADE;
 U   ALTER TABLE ONLY public.txbroiler DROP CONSTRAINT txbroiler_programmed_eggs_id_fkey;
       public       postgres    false    3349    298    318            b           2606    29345 $   txbroiler txbroiler_scenario_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroiler
    ADD CONSTRAINT txbroiler_scenario_id_fkey FOREIGN KEY (scenario_id) REFERENCES public.mdscenario(scenario_id) ON UPDATE CASCADE ON DELETE CASCADE;
 N   ALTER TABLE ONLY public.txbroiler DROP CONSTRAINT txbroiler_scenario_id_fkey;
       public       postgres    false    298    3186    264            j           2606    29350 1   txbroilereviction txbroilereviction_breed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroilereviction
    ADD CONSTRAINT txbroilereviction_breed_id_fkey FOREIGN KEY (breed_id) REFERENCES public.mdbreed(breed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 [   ALTER TABLE ONLY public.txbroilereviction DROP CONSTRAINT txbroilereviction_breed_id_fkey;
       public       postgres    false    3157    250    300            m           2606    29355 A   txbroilereviction_detail txbroilereviction_detail_broiler_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroilereviction_detail
    ADD CONSTRAINT txbroilereviction_detail_broiler_id_fkey FOREIGN KEY (broilereviction_id) REFERENCES public.txbroilereviction(broilereviction_id) ON UPDATE CASCADE ON DELETE CASCADE;
 k   ALTER TABLE ONLY public.txbroilereviction_detail DROP CONSTRAINT txbroilereviction_detail_broiler_id_fkey;
       public       postgres    false    301    3277    300            n           2606    29360 I   txbroilereviction_detail txbroilereviction_detail_broiler_product_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroilereviction_detail
    ADD CONSTRAINT txbroilereviction_detail_broiler_product_id_fkey FOREIGN KEY (broiler_product_id) REFERENCES public.mdbroiler_product(broiler_product_id) ON UPDATE CASCADE ON DELETE CASCADE;
 s   ALTER TABLE ONLY public.txbroilereviction_detail DROP CONSTRAINT txbroilereviction_detail_broiler_product_id_fkey;
       public       postgres    false    3159    251    301            o           2606    29365 @   txbroilereviction_detail txbroilereviction_detail_center_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroilereviction_detail
    ADD CONSTRAINT txbroilereviction_detail_center_id_fkey FOREIGN KEY (center_id) REFERENCES public.oscenter(center_id);
 j   ALTER TABLE ONLY public.txbroilereviction_detail DROP CONSTRAINT txbroilereviction_detail_center_id_fkey;
       public       postgres    false    3203    271    301            p           2606    29370 M   txbroilereviction_detail txbroilereviction_detail_execution_slaughterhouse_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroilereviction_detail
    ADD CONSTRAINT txbroilereviction_detail_execution_slaughterhouse_id FOREIGN KEY (executionslaughterhouse_id) REFERENCES public.osslaughterhouse(slaughterhouse_id);
 w   ALTER TABLE ONLY public.txbroilereviction_detail DROP CONSTRAINT txbroilereviction_detail_execution_slaughterhouse_id;
       public       postgres    false    3251    284    301            q           2606    29375 >   txbroilereviction_detail txbroilereviction_detail_farm_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroilereviction_detail
    ADD CONSTRAINT txbroilereviction_detail_farm_id_fkey FOREIGN KEY (farm_id) REFERENCES public.osfarm(farm_id) ON UPDATE CASCADE ON DELETE CASCADE;
 h   ALTER TABLE ONLY public.txbroilereviction_detail DROP CONSTRAINT txbroilereviction_detail_farm_id_fkey;
       public       postgres    false    3215    273    301            r           2606    29380 >   txbroilereviction_detail txbroilereviction_detail_shed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroilereviction_detail
    ADD CONSTRAINT txbroilereviction_detail_shed_id_fkey FOREIGN KEY (shed_id) REFERENCES public.osshed(shed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 h   ALTER TABLE ONLY public.txbroilereviction_detail DROP CONSTRAINT txbroilereviction_detail_shed_id_fkey;
       public       postgres    false    3235    279    301            s           2606    29385 H   txbroilereviction_detail txbroilereviction_detail_slaughterhouse_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroilereviction_detail
    ADD CONSTRAINT txbroilereviction_detail_slaughterhouse_id_fkey FOREIGN KEY (slaughterhouse_id) REFERENCES public.osslaughterhouse(slaughterhouse_id) ON UPDATE CASCADE ON DELETE CASCADE;
 r   ALTER TABLE ONLY public.txbroilereviction_detail DROP CONSTRAINT txbroilereviction_detail_slaughterhouse_id_fkey;
       public       postgres    false    3251    301    284            k           2606    29390 7   txbroilereviction txbroilereviction_partnership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroilereviction
    ADD CONSTRAINT txbroilereviction_partnership_id_fkey FOREIGN KEY (partnership_id) REFERENCES public.ospartnership(partnership_id) ON UPDATE CASCADE ON DELETE CASCADE;
 a   ALTER TABLE ONLY public.txbroilereviction DROP CONSTRAINT txbroilereviction_partnership_id_fkey;
       public       postgres    false    3229    300    277            l           2606    29395 4   txbroilereviction txbroilereviction_scenario_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroilereviction
    ADD CONSTRAINT txbroilereviction_scenario_id_fkey FOREIGN KEY (scenario_id) REFERENCES public.mdscenario(scenario_id) ON UPDATE CASCADE ON DELETE CASCADE;
 ^   ALTER TABLE ONLY public.txbroilereviction DROP CONSTRAINT txbroilereviction_scenario_id_fkey;
       public       postgres    false    300    264    3186            t           2606    29400 C   txbroilerproduct_detail txbroilerproduct_detail_broiler_detail_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroilerproduct_detail
    ADD CONSTRAINT txbroilerproduct_detail_broiler_detail_fkey FOREIGN KEY (broiler_detail) REFERENCES public.txbroiler_detail(broiler_detail_id) ON UPDATE CASCADE ON DELETE CASCADE;
 m   ALTER TABLE ONLY public.txbroilerproduct_detail DROP CONSTRAINT txbroilerproduct_detail_broiler_detail_fkey;
       public       postgres    false    302    3272    299            u           2606    29405 .   txbroodermachine txbroodermachine_farm_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroodermachine
    ADD CONSTRAINT txbroodermachine_farm_id_fkey FOREIGN KEY (farm_id) REFERENCES public.osfarm(farm_id) ON UPDATE CASCADE ON DELETE CASCADE;
 X   ALTER TABLE ONLY public.txbroodermachine DROP CONSTRAINT txbroodermachine_farm_id_fkey;
       public       postgres    false    303    3215    273            v           2606    29410 5   txbroodermachine txbroodermachine_partnership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroodermachine
    ADD CONSTRAINT txbroodermachine_partnership_id_fkey FOREIGN KEY (partnership_id) REFERENCES public.ospartnership(partnership_id) ON UPDATE CASCADE ON DELETE CASCADE;
 _   ALTER TABLE ONLY public.txbroodermachine DROP CONSTRAINT txbroodermachine_partnership_id_fkey;
       public       postgres    false    303    3229    277            w           2606    29415 ,   txcalendarday txcalendarday_calendar_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txcalendarday
    ADD CONSTRAINT txcalendarday_calendar_id_fkey FOREIGN KEY (calendar_id) REFERENCES public.txcalendar(calendar_id) ON UPDATE CASCADE ON DELETE CASCADE;
 V   ALTER TABLE ONLY public.txcalendarday DROP CONSTRAINT txcalendarday_calendar_id_fkey;
       public       postgres    false    305    3294    304            y           2606    29420 -   txeggs_planning txeggs_planning_breed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txeggs_planning
    ADD CONSTRAINT txeggs_planning_breed_id_fkey FOREIGN KEY (breed_id) REFERENCES public.mdbreed(breed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 W   ALTER TABLE ONLY public.txeggs_planning DROP CONSTRAINT txeggs_planning_breed_id_fkey;
       public       postgres    false    308    3157    250            z           2606    29425 0   txeggs_planning txeggs_planning_scenario_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txeggs_planning
    ADD CONSTRAINT txeggs_planning_scenario_id_fkey FOREIGN KEY (scenario_id) REFERENCES public.mdscenario(scenario_id);
 Z   ALTER TABLE ONLY public.txeggs_planning DROP CONSTRAINT txeggs_planning_scenario_id_fkey;
       public       postgres    false    3186    264    308            {           2606    29430 -   txeggs_required txeggs_required_breed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txeggs_required
    ADD CONSTRAINT txeggs_required_breed_id_fkey FOREIGN KEY (breed_id) REFERENCES public.mdbreed(breed_id);
 W   ALTER TABLE ONLY public.txeggs_required DROP CONSTRAINT txeggs_required_breed_id_fkey;
       public       postgres    false    250    3157    309            |           2606    29435 0   txeggs_required txeggs_required_scenario_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txeggs_required
    ADD CONSTRAINT txeggs_required_scenario_id_fkey FOREIGN KEY (scenario_id) REFERENCES public.mdscenario(scenario_id);
 Z   ALTER TABLE ONLY public.txeggs_required DROP CONSTRAINT txeggs_required_scenario_id_fkey;
       public       postgres    false    264    3186    309            }           2606    29440 +   txeggs_storage txeggs_storage_breed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txeggs_storage
    ADD CONSTRAINT txeggs_storage_breed_id_fkey FOREIGN KEY (breed_id) REFERENCES public.mdbreed(breed_id);
 U   ALTER TABLE ONLY public.txeggs_storage DROP CONSTRAINT txeggs_storage_breed_id_fkey;
       public       postgres    false    250    3157    310            ~           2606    29445 5   txeggs_storage txeggs_storage_incubator_plant_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txeggs_storage
    ADD CONSTRAINT txeggs_storage_incubator_plant_id_fkey FOREIGN KEY (incubator_plant_id) REFERENCES public.osincubatorplant(incubator_plant_id);
 _   ALTER TABLE ONLY public.txeggs_storage DROP CONSTRAINT txeggs_storage_incubator_plant_id_fkey;
       public       postgres    false    310    275    3225                       2606    29450 .   txeggs_storage txeggs_storage_scenario_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txeggs_storage
    ADD CONSTRAINT txeggs_storage_scenario_id_fkey FOREIGN KEY (scenario_id) REFERENCES public.mdscenario(scenario_id);
 X   ALTER TABLE ONLY public.txeggs_storage DROP CONSTRAINT txeggs_storage_scenario_id_fkey;
       public       postgres    false    264    310    3186            �           2606    29455 '   txgoals_erp txgoals_erp_product_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txgoals_erp
    ADD CONSTRAINT txgoals_erp_product_id_fkey FOREIGN KEY (product_id) REFERENCES public.mdproduct(product_id);
 Q   ALTER TABLE ONLY public.txgoals_erp DROP CONSTRAINT txgoals_erp_product_id_fkey;
       public       postgres    false    311    3177    260            �           2606    29460 (   txgoals_erp txgoals_erp_scenario_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txgoals_erp
    ADD CONSTRAINT txgoals_erp_scenario_id_fkey FOREIGN KEY (scenario_id) REFERENCES public.mdscenario(scenario_id);
 R   ALTER TABLE ONLY public.txgoals_erp DROP CONSTRAINT txgoals_erp_scenario_id_fkey;
       public       postgres    false    311    3186    264            �           2606    29465 '   txhousingway txhousingway_breed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txhousingway
    ADD CONSTRAINT txhousingway_breed_id_fkey FOREIGN KEY (breed_id) REFERENCES public.mdbreed(breed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 Q   ALTER TABLE ONLY public.txhousingway DROP CONSTRAINT txhousingway_breed_id_fkey;
       public       postgres    false    313    3157    250            �           2606    29470 6   txhousingway_detail txhousingway_detail_center_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txhousingway_detail
    ADD CONSTRAINT txhousingway_detail_center_id_fkey FOREIGN KEY (center_id) REFERENCES public.oscenter(center_id);
 `   ALTER TABLE ONLY public.txhousingway_detail DROP CONSTRAINT txhousingway_detail_center_id_fkey;
       public       postgres    false    314    271    3203            �           2606    29475 @   txhousingway_detail txhousingway_detail_execution_center_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txhousingway_detail
    ADD CONSTRAINT txhousingway_detail_execution_center_id_fkey FOREIGN KEY (executioncenter_id) REFERENCES public.oscenter(center_id);
 j   ALTER TABLE ONLY public.txhousingway_detail DROP CONSTRAINT txhousingway_detail_execution_center_id_fkey;
       public       postgres    false    271    3203    314            �           2606    29480 >   txhousingway_detail txhousingway_detail_execution_farm_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txhousingway_detail
    ADD CONSTRAINT txhousingway_detail_execution_farm_id_fkey FOREIGN KEY (executionfarm_id) REFERENCES public.osfarm(farm_id);
 h   ALTER TABLE ONLY public.txhousingway_detail DROP CONSTRAINT txhousingway_detail_execution_farm_id_fkey;
       public       postgres    false    314    3215    273            �           2606    29485 >   txhousingway_detail txhousingway_detail_execution_shed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txhousingway_detail
    ADD CONSTRAINT txhousingway_detail_execution_shed_id_fkey FOREIGN KEY (executionshed_id) REFERENCES public.osshed(shed_id);
 h   ALTER TABLE ONLY public.txhousingway_detail DROP CONSTRAINT txhousingway_detail_execution_shed_id_fkey;
       public       postgres    false    3235    279    314            �           2606    29490 G   txhousingway_detail txhousingway_detail_executionincubatorplant_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txhousingway_detail
    ADD CONSTRAINT txhousingway_detail_executionincubatorplant_id_fkey FOREIGN KEY (executionincubatorplant_id) REFERENCES public.osincubatorplant(incubator_plant_id);
 q   ALTER TABLE ONLY public.txhousingway_detail DROP CONSTRAINT txhousingway_detail_executionincubatorplant_id_fkey;
       public       postgres    false    3225    314    275            �           2606    29495 4   txhousingway_detail txhousingway_detail_farm_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txhousingway_detail
    ADD CONSTRAINT txhousingway_detail_farm_id_fkey FOREIGN KEY (farm_id) REFERENCES public.osfarm(farm_id) ON UPDATE CASCADE ON DELETE CASCADE;
 ^   ALTER TABLE ONLY public.txhousingway_detail DROP CONSTRAINT txhousingway_detail_farm_id_fkey;
       public       postgres    false    273    314    3215            �           2606    29500 ;   txhousingway_detail txhousingway_detail_housing_way_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txhousingway_detail
    ADD CONSTRAINT txhousingway_detail_housing_way_id_fkey FOREIGN KEY (housing_way_id) REFERENCES public.txhousingway(housing_way_id) ON UPDATE CASCADE ON DELETE CASCADE;
 e   ALTER TABLE ONLY public.txhousingway_detail DROP CONSTRAINT txhousingway_detail_housing_way_id_fkey;
       public       postgres    false    313    3324    314            �           2606    29505 4   txhousingway_detail txhousingway_detail_shed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txhousingway_detail
    ADD CONSTRAINT txhousingway_detail_shed_id_fkey FOREIGN KEY (shed_id) REFERENCES public.osshed(shed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 ^   ALTER TABLE ONLY public.txhousingway_detail DROP CONSTRAINT txhousingway_detail_shed_id_fkey;
       public       postgres    false    3235    314    279            �           2606    29510 -   txhousingway txhousingway_partnership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txhousingway
    ADD CONSTRAINT txhousingway_partnership_id_fkey FOREIGN KEY (partnership_id) REFERENCES public.ospartnership(partnership_id) ON UPDATE CASCADE ON DELETE CASCADE;
 W   ALTER TABLE ONLY public.txhousingway DROP CONSTRAINT txhousingway_partnership_id_fkey;
       public       postgres    false    3229    313    277            �           2606    29515 *   txhousingway txhousingway_scenario_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txhousingway
    ADD CONSTRAINT txhousingway_scenario_id_fkey FOREIGN KEY (scenario_id) REFERENCES public.mdscenario(scenario_id) ON UPDATE CASCADE ON DELETE CASCADE;
 T   ALTER TABLE ONLY public.txhousingway DROP CONSTRAINT txhousingway_scenario_id_fkey;
       public       postgres    false    264    313    3186            �           2606    29520 '   txhousingway txhousingway_stage_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txhousingway
    ADD CONSTRAINT txhousingway_stage_id_fkey FOREIGN KEY (stage_id) REFERENCES public.mdstage(stage_id);
 Q   ALTER TABLE ONLY public.txhousingway DROP CONSTRAINT txhousingway_stage_id_fkey;
       public       postgres    false    3192    268    313            �           2606    29525    txlot txlot_breed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txlot
    ADD CONSTRAINT txlot_breed_id_fkey FOREIGN KEY (breed_id) REFERENCES public.mdbreed(breed_id);
 C   ALTER TABLE ONLY public.txlot DROP CONSTRAINT txlot_breed_id_fkey;
       public       postgres    false    3157    315    250            �           2606    29530    txlot txlot_farm_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txlot
    ADD CONSTRAINT txlot_farm_id_fkey FOREIGN KEY (farm_id) REFERENCES public.osfarm(farm_id) ON UPDATE CASCADE ON DELETE CASCADE;
 B   ALTER TABLE ONLY public.txlot DROP CONSTRAINT txlot_farm_id_fkey;
       public       postgres    false    315    3215    273            �           2606    29535    txlot txlot_housing_way_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txlot
    ADD CONSTRAINT txlot_housing_way_id_fkey FOREIGN KEY (housing_way_id) REFERENCES public.txhousingway(housing_way_id) ON UPDATE CASCADE ON DELETE CASCADE;
 I   ALTER TABLE ONLY public.txlot DROP CONSTRAINT txlot_housing_way_id_fkey;
       public       postgres    false    3324    313    315            �           2606    29540    txlot txlot_product_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txlot
    ADD CONSTRAINT txlot_product_id_fkey FOREIGN KEY (product_id) REFERENCES public.mdproduct(product_id);
 E   ALTER TABLE ONLY public.txlot DROP CONSTRAINT txlot_product_id_fkey;
       public       postgres    false    315    3177    260            �           2606    29545    txlot txlot_shed_id_fkey    FK CONSTRAINT     }   ALTER TABLE ONLY public.txlot
    ADD CONSTRAINT txlot_shed_id_fkey FOREIGN KEY (shed_id) REFERENCES public.osshed(shed_id);
 B   ALTER TABLE ONLY public.txlot DROP CONSTRAINT txlot_shed_id_fkey;
       public       postgres    false    315    279    3235            �           2606    29550 +   txposturecurve txposturecurve_breed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txposturecurve
    ADD CONSTRAINT txposturecurve_breed_id_fkey FOREIGN KEY (breed_id) REFERENCES public.mdbreed(breed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 U   ALTER TABLE ONLY public.txposturecurve DROP CONSTRAINT txposturecurve_breed_id_fkey;
       public       postgres    false    250    317    3157            �           2606    29555 1   txprogrammed_eggs txprogrammed_eggs_breed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txprogrammed_eggs
    ADD CONSTRAINT txprogrammed_eggs_breed_id_fkey FOREIGN KEY (breed_id) REFERENCES public.mdbreed(breed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 [   ALTER TABLE ONLY public.txprogrammed_eggs DROP CONSTRAINT txprogrammed_eggs_breed_id_fkey;
       public       postgres    false    318    250    3157            �           2606    29560 5   txprogrammed_eggs txprogrammed_eggs_eggs_movements_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.txprogrammed_eggs
    ADD CONSTRAINT txprogrammed_eggs_eggs_movements_id FOREIGN KEY (eggs_movements_id) REFERENCES public.txeggs_movements(eggs_movements_id);
 _   ALTER TABLE ONLY public.txprogrammed_eggs DROP CONSTRAINT txprogrammed_eggs_eggs_movements_id;
       public       postgres    false    307    3301    318            �           2606    29565 8   txprogrammed_eggs txprogrammed_eggs_eggs_storage_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txprogrammed_eggs
    ADD CONSTRAINT txprogrammed_eggs_eggs_storage_id_fkey FOREIGN KEY (eggs_storage_id) REFERENCES public.txeggs_storage(eggs_storage_id) ON UPDATE CASCADE ON DELETE CASCADE;
 b   ALTER TABLE ONLY public.txprogrammed_eggs DROP CONSTRAINT txprogrammed_eggs_eggs_storage_id_fkey;
       public       postgres    false    318    310    3314            �           2606    29570 5   txprogrammed_eggs txprogrammed_eggs_incubator_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txprogrammed_eggs
    ADD CONSTRAINT txprogrammed_eggs_incubator_id_fkey FOREIGN KEY (incubator_id) REFERENCES public.osincubator(incubator_id) ON UPDATE CASCADE ON DELETE CASCADE;
 _   ALTER TABLE ONLY public.txprogrammed_eggs DROP CONSTRAINT txprogrammed_eggs_incubator_id_fkey;
       public       postgres    false    3220    318    274            �           2606    29575 3   txscenarioformula txscenarioformula_measure_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioformula
    ADD CONSTRAINT txscenarioformula_measure_id_fkey FOREIGN KEY (measure_id) REFERENCES public.mdmeasure(measure_id) ON UPDATE CASCADE ON DELETE CASCADE;
 ]   ALTER TABLE ONLY public.txscenarioformula DROP CONSTRAINT txscenarioformula_measure_id_fkey;
       public       postgres    false    3163    319    254            �           2606    29580 5   txscenarioformula txscenarioformula_parameter_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioformula
    ADD CONSTRAINT txscenarioformula_parameter_id_fkey FOREIGN KEY (parameter_id) REFERENCES public.mdparameter(parameter_id) ON UPDATE CASCADE ON DELETE CASCADE;
 _   ALTER TABLE ONLY public.txscenarioformula DROP CONSTRAINT txscenarioformula_parameter_id_fkey;
       public       postgres    false    256    319    3167            �           2606    29585 3   txscenarioformula txscenarioformula_process_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioformula
    ADD CONSTRAINT txscenarioformula_process_id_fkey FOREIGN KEY (process_id) REFERENCES public.mdprocess(process_id) ON UPDATE CASCADE;
 ]   ALTER TABLE ONLY public.txscenarioformula DROP CONSTRAINT txscenarioformula_process_id_fkey;
       public       postgres    false    319    3173    258            �           2606    29590 4   txscenarioformula txscenarioformula_scenario_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioformula
    ADD CONSTRAINT txscenarioformula_scenario_id_fkey FOREIGN KEY (scenario_id) REFERENCES public.mdscenario(scenario_id) ON UPDATE CASCADE ON DELETE CASCADE;
 ^   ALTER TABLE ONLY public.txscenarioformula DROP CONSTRAINT txscenarioformula_scenario_id_fkey;
       public       postgres    false    319    264    3186            �           2606    29595 9   txscenarioparameter txscenarioparameter_parameter_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioparameter
    ADD CONSTRAINT txscenarioparameter_parameter_id_fkey FOREIGN KEY (parameter_id) REFERENCES public.mdparameter(parameter_id) ON UPDATE CASCADE ON DELETE CASCADE;
 c   ALTER TABLE ONLY public.txscenarioparameter DROP CONSTRAINT txscenarioparameter_parameter_id_fkey;
       public       postgres    false    320    3167    256            �           2606    29600 7   txscenarioparameter txscenarioparameter_process_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioparameter
    ADD CONSTRAINT txscenarioparameter_process_id_fkey FOREIGN KEY (process_id) REFERENCES public.mdprocess(process_id) ON UPDATE CASCADE ON DELETE CASCADE;
 a   ALTER TABLE ONLY public.txscenarioparameter DROP CONSTRAINT txscenarioparameter_process_id_fkey;
       public       postgres    false    320    258    3173            �           2606    29605 8   txscenarioparameter txscenarioparameter_scenario_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioparameter
    ADD CONSTRAINT txscenarioparameter_scenario_id_fkey FOREIGN KEY (scenario_id) REFERENCES public.mdscenario(scenario_id) ON UPDATE CASCADE ON DELETE CASCADE;
 b   ALTER TABLE ONLY public.txscenarioparameter DROP CONSTRAINT txscenarioparameter_scenario_id_fkey;
       public       postgres    false    264    3186    320            �           2606    29610 ?   txscenarioparameterday txscenarioparameterday_parameter_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioparameterday
    ADD CONSTRAINT txscenarioparameterday_parameter_id_fkey FOREIGN KEY (parameter_id) REFERENCES public.mdparameter(parameter_id);
 i   ALTER TABLE ONLY public.txscenarioparameterday DROP CONSTRAINT txscenarioparameterday_parameter_id_fkey;
       public       postgres    false    321    3167    256            �           2606    29615 >   txscenarioparameterday txscenarioparameterday_scenario_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioparameterday
    ADD CONSTRAINT txscenarioparameterday_scenario_id_fkey FOREIGN KEY (scenario_id) REFERENCES public.mdscenario(scenario_id);
 h   ALTER TABLE ONLY public.txscenarioparameterday DROP CONSTRAINT txscenarioparameterday_scenario_id_fkey;
       public       postgres    false    321    3186    264            �           2606    29620 ;   txscenarioposturecurve txscenarioposturecurve_breed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioposturecurve
    ADD CONSTRAINT txscenarioposturecurve_breed_id_fkey FOREIGN KEY (breed_id) REFERENCES public.mdbreed(breed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 e   ALTER TABLE ONLY public.txscenarioposturecurve DROP CONSTRAINT txscenarioposturecurve_breed_id_fkey;
       public       postgres    false    322    3157    250            �           2606    29625 G   txscenarioposturecurve txscenarioposturecurve_housingway_detail_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioposturecurve
    ADD CONSTRAINT txscenarioposturecurve_housingway_detail_id_fkey FOREIGN KEY (housingway_detail_id) REFERENCES public.txhousingway_detail(housingway_detail_id) ON UPDATE CASCADE ON DELETE CASCADE;
 q   ALTER TABLE ONLY public.txscenarioposturecurve DROP CONSTRAINT txscenarioposturecurve_housingway_detail_id_fkey;
       public       postgres    false    3330    322    314            �           2606    29630 >   txscenarioposturecurve txscenarioposturecurve_scenario_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioposturecurve
    ADD CONSTRAINT txscenarioposturecurve_scenario_id_fkey FOREIGN KEY (scenario_id) REFERENCES public.mdscenario(scenario_id) ON UPDATE CASCADE ON DELETE CASCADE;
 h   ALTER TABLE ONLY public.txscenarioposturecurve DROP CONSTRAINT txscenarioposturecurve_scenario_id_fkey;
       public       postgres    false    3186    264    322            �           2606    29635 3   txscenarioprocess txscenarioprocess_process_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioprocess
    ADD CONSTRAINT txscenarioprocess_process_id_fkey FOREIGN KEY (process_id) REFERENCES public.mdprocess(process_id) ON UPDATE CASCADE ON DELETE CASCADE;
 ]   ALTER TABLE ONLY public.txscenarioprocess DROP CONSTRAINT txscenarioprocess_process_id_fkey;
       public       postgres    false    323    3173    258            �           2606    29640 4   txscenarioprocess txscenarioprocess_scenario_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioprocess
    ADD CONSTRAINT txscenarioprocess_scenario_id_fkey FOREIGN KEY (scenario_id) REFERENCES public.mdscenario(scenario_id) ON UPDATE CASCADE ON DELETE CASCADE;
 ^   ALTER TABLE ONLY public.txscenarioprocess DROP CONSTRAINT txscenarioprocess_scenario_id_fkey;
       public       postgres    false    264    323    3186            !      x������ � �      #      x������ � �      %      x������ � �      '      x������ � �      )      x������ � �      +   4   x�3�4�TUHIU((�/I��K�2�4��NN��2�4�H%&�f��r��qqq '[�      -      x������ � �      /      x������ � �      1      x������ � �      2   "   x�3�N�M�K�R�\��.��&��b�=... ��
�      R   �   x�}��j1���d��^z�a!�܋�U6[
���>}��²���f4�t��ĳ��7D���0�J�60 �kﴪ��t4*<a趎*~�Z]� b"�X�h�l�R<;������{�O�͈��P�g��BF�$�����H�Vq��Ά頥6�}���G��_Ƴc�DVn�'`��^�M�
���(8R�G�>�2_�8����v��4|�����>����      T   E   x���	�@�����K��#�y�FE��$m�����i�n�=����ǵ��UPr�.T�{"�R�B      U   ,   x�34�200��/.�24q�8�󓒸�AcN�=... ��C      V     x�e�Kn�0��)|A"����AЍ��&H���K*m�NC��33}<��m,��~��E
��5�p��c9��@�-�v������j����A{���`Փ	�����'�	@-||h��}���D�W���k2,�ͤ"���\�ۅ��`�5��њB ���٠�sߞ#h�9���0����b~�u�+{f6��a�4�����&�GF2�8�u����d�C�ϟ!�W����F"�^��2�L镓j��/��)���      W   9   x�3�J-(�O)MN�<�9�ˈ�5/=�(%�˘�'�,1�$U�R�����D�=... �6>      Y   !   x�3�,�/rIMKM.����4��=... m%�      [   �   x���A�0E��)��
��İ$�u3@55նx+O��Lm�����$�JiXkk����T��B��v��;YJM6�d����y��~B��O��(�1�C����$�G=��,���Yx1��Fۑd]5���UQf��O�WQ��i/cP]/a�l��j򑤭R�/ �	9���Q2�\FyJ��VO*��l���N(�7S��%      ]   [  x��S�N�0<O��/�l���� ��̥M���׏�	�����$E�_��5;3s�r42�);x]��	��p�uw��xK�n��p^6�����X<���[��	^/Ȳ��{���+�|���v��ҖbMsm�td���~�n�3ZGS�dUD4���Hi�ȡ/���\3ǪF��W`��D�Hɩ�`b9?�
D����<)}S�*r��V����O*A�z�T���!���P��"�IcK,�H�Lٓx9�����@H^Q�	��>?V���y*�u�yv�6���8�J�g0�*��JW��J�Xi"J��9�8�
��_S��bV�S	b�	ꫩ����!n      _   F  x����n�0E��W�*�yz		�����u�@$"��j
��׾v�����3�;�a��	g�z���4DF���Q5�#�,������>�Cɠ(�I�	B��@�Aվ���*��UL�!C�R2�SS��z���j[��%��jA��XU�vD����\/��3���2$�2>8R��"W�Oo���廆��{�q�3����ԇ�@����o��,�%<�%`Z���%�F*�_��v��� g8��s������"��E����W�з_[�Gn��s�;�v����pʘ�8՞���?�q�x���+NM��Ѭ�F� \.���3�O��o���J      a   >   x�3�tL����,.)JL�/�LD�Z�X��(XX�Y����Z���r��qqq ��{      c   �   x���A
�0EדS�"�S��Sx 7��P�P��z/f��ޛ���=�>����%��5D�"RF�-ؔ=h1�X�i���J$�A����`-�~��Z�$F�X����������z���\zv�y;(���6Q      e   `   x�3�t�,.���L�IU(H,JTp���J��L�+��'�eę�\Z�����L83�2�2s2K@��.cβ���k��)gQjqjQH!������ P4b      g   N   x�3�4�t�K�/JI�2�4���K.MJL����2�4�J-(�O)M.�/J,�2�4�t.:�6Q�R�'�,1�$�+F��� ۏ�      i   {   x�3�LL����,.)JL�/�T1JR14R	5)��I4�Lw�H7����I����*u�Nr+�(4H�/�0M��w��H�J�t�t!���Y`�id`h�k D�
�V�V�&��\1z\\\ �+B      j   Z  x�uW�R#9=�W�z���v4KC�!��S_�K�[.yj!�f>a�s��ZR�R��v>�R/_.Esv��	�����[IvF��A�_'��Ni΃�μ��cw�����[�u���~a���?��sid�B��6����Jt,�p����ӽ�Ʌ2�ޅg[��3�{إ!;�I�[4S0�q�';��|�����/%�g��s٪W��5w1
r����(KeKճ���o�,�!�"2A��[���£�� j�pHH�春l:����û�vf9-B�,��x�;���b�ގ)�%c��%v�۵Ŕpw �]�V6��0v�S5 �@@]Fk��%�y�bҪ���9
m:{���l�
��z������*<��v�ޤ�҉��{v�SX%x���6�]�tb��),-��7E(�%6Q���N<^���D������%�F�b�yI �����ʬ��x8��6F�k�GU�;0z-W�s"ڢ���<K翵
�R�Th���y��ƒ��i��8x��/9�h�`/��֚E$��ƓDn�����Q5s䤝<��y=��0�h��K٭'�yl(y��e���������|���G���N��q(���8�^[�]W[A��(S��N��  �3.��h4�ܗG�'��A���)��ٔ���,��P&��[BxaH���*�J��(A
�<ܹ��R�&��ۡZ�[6K�¨�l��#��������V��ɦ2��[#�@�ZcD�ac/�������u1�\x&�u��aBa�A�3���vʽ����s���A��FA1��@�9���@��*�JxǷ�q�8�����%W�I�"_��4P!���WnR�I�%Ⱥ�m�r�I:%r��8*���Z
��\K����}̹(=�}�Ny�������:+���a��0�}ÛX+�X��#�'E4����(�pzWy�n[���|�.��0��@�A��HCvnT���|]��y8��>i�Π�������y��d��uH�Ϝ���V(��-<w|�i-�U3�����2�e��^�t�� C%���4�q��6�����i�
J3�m��؆����;l�"��
l��/n�4h��AqK��B�Pt�{��UT�š��t�n�l���� �3�%�y(@u9쪺{�i����
�j�o�U�A�NeI�:�ӫ��� 3�'=�f�S�%r���췮�l
Co�5L�W�7ҵq�����J�p����֦2��F����.&>;Ҝ��tp��`��Ȏ c��̏ �l�q�ã(���ߤ�`Y"3l�L�����r�O�����}���4��:>?��>����ZB�N����������3      k      x������ � �      l   @  x�mV]n�6~�O�laR�~���fS'5�`�(&��K�]J
�ܦ�C�#�bR3t@��p�o~H��K���R|�0� �Ð��G'��� r��n%�5�)6ۆ�0�/�-�޽���	�bc'7fm�Y�>���� ��� ���7��\P)�g�m�v�B-�,�?w#�ce.0Ӎ�`�1:K!%x�Ϛ>�xH�*	*�)l��rPEP%�� S:��ﵸ�,2���%���lĕ7#Ul������@65S*08��=�{M�T2=��M7U���O��jb�������H�t�����_M�TTyc&?"��c��$j�⁭���e���:
p�ugg��P�U��Ue��4ؕ;׃�nt���fy����G�<I�R���?�H�	|��Z�2��T*cEv����He,�}7��:I����=����$����̀�CjM�Ot�y!�$*u�����1���a�n��1B��*$�#�+SE����Pk%�ŗ�f0�ut�u�O�H���8ۢO��"eC�~�Ƅ��:b.��z��#X�&���}b/Ib�Z|�F9zh]v=c�?xa#���n8$�$�R�qIe7`z�t%I!\-������YV�U��&�*I�8��<枨�͌��>%�%K����cK��|)�4�<�J1�F|��ϔbx�p6����"�E��㲓{\�-X�`��x�-�<�����SfD��1"���$��Z|3anlw���ɛ����b��ʉr-�����%�x��Z\�q�-'p�.�_�T̜%��Uǭ�A(�8��t�������Ω�E�>��HH1\�K���)A֭����>Hn��f7��G���f��� �
�}�]�QV�"G�e�q:ـd�ž�aCY�`V���1���uj����7����E��jdgkR�Ҩ�"�q�%��5� O��[�H$-#�ӧ��Q�֝Rkk��x�.݁J�l�w�?AJN�\Z�]h�4��qw�j�lY��&qT-�_%M|����r��%����'w��B#�������[4�hR��M�qA� ?�4���>��9>Y�X.'�'Z4(V�Y�V�ؖ|	      m   I  x����N1�k�S�	��ھ��Jj�PP�H�P��s�t�;9�qaK�v~������6��ᾟ�����~|=~������`aq�Y!��\����B:�t�t��A!!C%$A ���d��@�B"!����������*�)�oɹ�$�	��{��t�tPNg�I#N�8��D�1�c�t6z��k�r�7�1Mǘ���� \(������k��'y:���igN5���) YZ��w�gA�����F/J�\9F'6�S"� ��w�S���r
׮3�ڮkM���VM��Y�Mt��ʛH&I-'Q9���'����i�W��<      n   9   x�3����K.MJL�/JTp�,.)J,K-�t��4E�IIE�46�4�4�,A�=... =�      p   J   x�36�t�,.)�L*�L�/JTp,;�69?'Q!X�Q/X�ө49�(171/=QG�9?'?7)3�NWC�=... g��      r   �  x��[[����3��	����2�q\0�Y�;�$�J%��e!��H~��(/���_r���i{zyG�r��
�����_n�u���D7Sy������TR@�0S�#&K�D�g��N��]{:��,�R�|h��g��ٹ=��h~��hr������ɒ��������Eե��61�ª���(�N�S��nn�k�/{8[�	��` �8�p�v.�a���t�q*�KN!�_�^i�'3o\��⓫�A���lb>��������*_q���r�o�����~T�\"�p��Uq!��wI�W�������KD�*�f�� �d�ɜs0���M�Ǟ�n��xN����@��YŨ�wsO�s�*Nq9+*�d���h�կ{���ն��������{I\��ÕA���5�p��	M��+��N\ �|�A��+��5���#��ˆ(���#tն�Cf�~IQ�4��~�=�i�Y�ԝY�quU�����	�xt��ࠁ���#�a:�gw�����#^�������ejN��j
ZD�H&ࢿ���I��
�h�����X�e�l���<|22�X�ԛN��J�5G#�D� �����n��i����J7��n�j�,�]�ᭊ�G�'PZbvS�ym���8�π��_����W��S͝��Q��u��W .^�� �_�<�a���1�����(5~VJ��ԗ{���~��Jv@jF�y�a��̚C]�̂/P�:�2*/����[]�b>� �_�z�rӈf ��×�O+U)*w������L��Ty�G�*0P7H*gTCes�O���f�R.�e��
ԭqD���	Z����9��[��|23��G�$֟��@�@&Q����xJ$ }#Q����[LY����u���Q�~U�+��\=셪;��,��\�S�[Ն|�)C�9���@c_�/OJ��zz�NK�~��V�̶���7ۢ���U4����͊FۉGES��z&�\+��r]���?��*k��s�c�2���᳦x��Nt_�9\s���f�sz�+nD����4��h�k8����.�}����~Q����ٕ���:��Y�A��1+�zuK���k�s��' ��ǺO=�E��ͤ|�{��:g��VCu�����m�Y����J\�xnV�%y�dh��j~X	�t��Z�-v�V�
*��n�5��Z9S�	��Hٔ���:���
W .F�]��p	pA�������K�l��J�5Ձ�׆w��4�}�q�/��s�S6Ņ�5��ɫ�i>Y�q���o��z��� 	�;�{��F�#g[n��QͽwmJ�۔�1�Cy�
}�s�/q�/�y�/�^x��-�8�C��>�B��LŃ��Q�m�	��r-܌����d-xe溙6��Åv��j�9��1r�MlˇO�'��h�!�˴�Ě��k=���X�p�q!�i��2������܋�ּ�T�e&���{�Ƶv���L�:��p�-p�N��,�4�p���Q�2D�,iL֠|���,��J{v5/)~�9���+%��78)3P2R�],������,�v;M�qԵL�0��-XS`��#���m��{��dek�3ȐQQ�Y�y�mD4qo0�ǌjYq���8"�CP]�{p�#�Y�M4f��x���4&;�|�����Q�&H|�~5�Oo8�T�3�� ���'������c�/����,'I��=��,vIG��{2�?U2���h_ =-�`-}�
m�BK�`wG�a��.À�Kd5mxХmL[�	�8�$��Q����E�a�����n��}�p9�w���;E�7Ұ���A{14�P�nM�&�Ɣ���v�Ig�[7����\V��z�}mM��~3�F�2�0��.ZĐ#<�k�)L�6�.���wj.H[w��֔��Q}r��i�cMO�V�� �3�2g����B�?�LoePE�B�5k�H	��i���V����ȇU��t�	�;�)U��)�;m��������)�(!
<lg(��d؈xh8�T� �C��2UT��()f_�h�/�g�L�oC�[WQ�\|�㡒\N)�a}:\ZzX�)��Ii��&��t���üA������YJ�����بH..,m`�V��qJj�����pkl���O3��.7���
R���s�<�|��I�C��>�~��������� ������2�7�/�I֋2����헒��$��\�]]oHS�f�>�</j=�,��r�[�_Ih�HB���+��Z@���m"�mT�w�v���ʧ�\�����v\c�@��˜1�1���k ؃�0��c�&Ӥ�5��냜'�q�7��Wk�oz�*W��N�/.v\c��T��Z�c�]d���n�1h��K��\��,W�۪͂�����v:�`��"$s�Ǩ���켚��1I+mz��)_|���׬����6_x �6��=Y�7�Q�^�[B敒�f�w�l�(f��j������Y=_��w�Nx� Ѩ���;4�	���^�_;d��&�_~\ˤ���nn;?�s�G�)�*8b���z���V%�"�JV��7cWdj��q�jث���6c����[咈���4����<�aS�U���}���м��� LF�6����.ߙ>�Jz����������:Q�f[<</��j'g�2	�,W�l�m;{2�Ms]��A�6�i�fh�LU�+|�&�v��VP;�F�m[(K�JM�S�Z�{Wn��[D�6e�a6��K�w��Gr}����d��
������ul`i�2�;�V#��Z�$9�Z=�U�BѨ
P�L%f�+:},i=)A}x�",��2��u}e?���lר�&O���5*~S)���'U �y����$�oWR���m"���ji �F̀~H���E��ˊ?� |	_�����8�&�O.V_����ϟ��{s�      t      x������ � �      u      x������ � �      w   Y   x�36��I�+ITHIUpJ�KM�L��Wp�Qp�L�J�r:�&'���:
��9��I��Xu9.��%E�e�Ŝ�����@����� oh!�      y      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �   �   x���M�0���=�i+.��
���̀�4!����X���I]@@J�E�&�/�MC�4�d-K,�VԇsF,t�ESM�Vw|A��O�㡹"`ě�b�̻�v��`fzn9=����bl�~�����BP�}x�]U ����M�js�T��Z�w#��6�'�/P�	�ŝ��z�[��q帹��j�⟺��W3I���mC�ul�q      �      x�|]]��<�|�]�l`"�i����M�ę�)}��	���hͶ���|���;�������[��߻���+]��'��k�ہdR>����$RH�)���He ��ʿHq ���/����3������9��@�Ȑ?��@&Y_�+�d8��@���S��P扒n���P����s�D���O��#f8�M�mS������~X����L��q�����3�G�`��&꼩����s�D�7�ϟ�?��8QN�����8Nԅ�����#~0Ήu�|�4�?�řzqN_0���8���iʡ?Ba���L�8����q^�����s�G�`�g�Ź}��?�řzq�_0U��yq�^��L��q^������G�`�g��y}��?�řzq���~��yq�^\�����q^\�����OP�u8q�iH�>��z�ǇWr(䫐�<���6���P�7!���~�ǃWq(䋐�L�ʸ����:�=�;	����]͡��A޹H�)Wy�wu�B�y'#E,�x��|	��F�X�q�5
�dKG� =λ�	��l�H��W�yﺝ��|$�Ì���q���l�H8 �qy`�c�H�|$�����90�G��#�\�=0΃Y>�-	0���ƹ0�G��#F���|��#�� #��sb��d�G�|=0΋Y>�-	�����,ɖ�ğs�ǋ��b��d�GL�����y1�G��#�^���优�#�� #�/N΋Y>�-	0�j�'��,ɖ��q�ǋ��b��d�G�1����y1�G��#��\���优�#�J_(b�ǉ��ą>����7׫?>��C!^S��"�z\8��8M���ݏ��P�ϔ�}���?��C!.S��;|����o.�xLُ��S���o��8Lُ�"��㽹9�/�E,�8o���b�� #fz�7��=��=������켗=��?�8��ov������@�����/{t{tǟs�ǁ�s`��.��0�P�����]��`�5.�A��.��0B�����0{t{t�~�ǉ�sb��.��0b�ǋ��b��.���������]���5/.΋٣�أ;��w���⼘=��=�L�����y1{t{t�v�ǋ��b��.��0���W����]��`�5/�΋٣�أ;H�|��:/f��b���s��xqu^��y}���/�΋Y+�X+!���/�΋Y+�X+!���/���U�I����Z����P��՝Ԑi\��V
񚺟��^�vk�X���n@��z�ԆC!V���ҵz���=��?~�>���#�=��=���B�Cqw��K��K�N���Hf�j�@�����,�W�GM����,W�G���<��㙅�ja4P!��y8�Y � �fQ����BW��p�?~��k8�YЩt���O~���F�E�����J��>4���F�E;����<,��FF;���g��ш�v���_�p|���BF��?��x:�Y����+�=�ݵ�,f4�G���7��c�Ōf1���Wp����Y�h3�X�鉮�xf1�Y�8��=</�3��bF8������vD���,h �.
��,j4�H�� �fa�Y�@�k 9�Y�h7ZH�hȑ�$���Gl������g�w�"�ȏh���Qu�o��>W�L�ݥN{}�
�V� ��uZ��]��x>{:����J� $�D��ͪ�n�uaJ��tvL�¸[a�4��Q�j�n5m ��:�ΎkV�v+G���Avvd�J�[%���ő͊�nE` ��i����f�[��- 	/dG6+���^=�*�őͪ�nUS ��]٬�W���Np}���F��#H�\''@�Y�AƎ G�&`�,+� cG���OL}��F��#H���v��"4�� #�9�����"Ȱ���L7�4� �"H ��2�n�jA�E�p@�]�us\�2,� mv����fdX	@�#Pwd�2,� ��Avwd�2,����� �;�YA��K �;�YA��K �;�y)�����J��
�����"�>��Iˠ�ˠ�#'�>��I령렀���*q�Bh�Bh�P%8 ��'���UBH+u=Ѭ��'�-�s��1�j�i�P8�[�z:�Y14�
@���qͪ�i�P ��dOG6+��H��^�lVM��fV��#��C�� $��٬�V M�@�rd�zhZ=�,�^�lVM���������f����L��r� ��z�lf�l&���Q1��b\4�Y;�Y!T	� �p8$ꯝ���-�,�C����L��<����̲l&йI��,�Y�� UM�trL�lfY6����)9�Y6�,�	@���qͲ�e��
�J�� ;;�Y6�,�	@��|�5��ק�!u򣬵C��ÃR�>O�z��
1W�_��R��(Yͷ�����Z�v�G�jݡC���(B���y���D��D�>�������Q�p�w�0���Q�p����;������!�W)��c�}�}�j6���̾վG{�����R�j�}��b��y8�Yf_-����<<�3K�%�G}��y8�Y^_-�8��zx�g�ַ+}}�����o4\�#\��>=3���o4^�#^��>=��뜢o4`�#`��>=S��o4b�1��!��C�9��h�h2������8f!�Y�8�p|H^�d2���p@�0zX^�e2������E����BF��p�o~x^�g2�����}��vG4��bF?(�1͂F���T�� rT���,j ��7��@�k6���p@e�	 G6��S���=�>�x;m���7VG�);R7��~�hT����Cʦ��Q�0}��v��p����i�N���H� :;�Y;���6��tvL�v@�v@ �	<P�լЭ�T7�����4��T��ّ����������]٬Э�4��ő����H5�]٬Э軓 �8�Y;�[;  �n��#�����6�A�9�7h�矮�{p}�B��Q�Xi��sdn�2��b�JL��n����CH�Q��gE8hB��܃��f!d��'i�L7�4!�BH �t T7G5!�BH R�\7�5!�BH�?d7G6!�BH ҿdwG6!�BH ҿdwG6!�BH8��p���,�!H&���,�!H�����f!dX	@ڼ�Ñ͚&��7�y��2q�:h�h���	T�e⤅�<��0���>��(:<8��sm@��R��.��6���Y'NZM���6�A�tD�bhZ1�����c�UCӪ��~���jVM+���6�z:�Y=4�
@ڸ�ˑ�
�iQ8��{��٬ �V ��ˑ�
�iQ Ҫd/G6+��DHu��٬ �VE�C獞�#�D�
� ������E��u�/M+p��!a�l&|��:p��!Q�l&�h3�3	�kg3G�d}��f3˲� ��{��,����Q2��f�̲l& i�T'G5�f�e3H�p��,�Y�̈́��dgG6�f�e3H�S�ّͲ�e�L �#(SvR�E�bQ �RRvR�E�bQ8�AJ�NJb�hY,
@������Ţe�( iRRqRݔq_�H'K��q��{��7��aQ&��1��ؖ��Mh"���A�1��؂���h"��݁�c"Nϱ�&�����>d#qr����wN���)�]�t�ӽ���$g0�����f�wV�4����M�*�;��g3F�Ru�ӕH��k"��`�9��:�{'6IG&�xs��UD��&񄮦��1N��;�D$}��7�8]t�x��ƛc���w@�HZ:����(�'�w��1!�>����}H߳�Ǆ\�ؾ���E �,��cB.},������\��	����n/��@��!�>&����:�]�����c��K���u�tG7�gK�R\N'H�{8��7[���0)��#��j�emI| �ri:�5[�O�`V.M�8ݑf    K�"�x���t���f��,"	���t���d��,"�`\.M�8�+f��R\`v�˥��;�l)XD/��\:��ĥ�``.s���J{�V< �-��10�����`E q̥c`.�Vio��@���|�%�}*����$���|;�iD���ߪ�`h�7�|��t���$���|;�iD��MI7�- 9�iD�uK�� F�rr�ӈ�-�$q�����%[D	H��Ṝ�4�d�(Iw�����%[D�k�	�'�8�(�"J@/��\ΎqQ�E��$^���rv��f�����\w؁�]aN�exږ�#�3�s���vGAB}�w}9���i~#�X�e�����y统w�%���wu9�rݹ��2 1=��\ѱp�{/.ˣ���1릹WڲbMT��8�c�4�6Z���uu\Ǫi�E�H�P\��ȎE��;`�X�e���X3ͽ�u�5���#;�Lso^e@b~T��:�c�4��T$�Ga��#;Ls�;e���+ssd�zi�U�H̏�2��l���w�(�$���B�|W�21>J�ܖ�	���='["zU��'���|Ws2�*8 �����k5P ��1��{#&; �&����1��{�%*���:f5s�d@���<�1��{�$��#;�4so�lˤ ���Ȏ)�܋���d���̽s��:T�=�1��{]"; �G%��#;�3so:d@b~�yd�-���R�p�$��y6�C(*W���M��z�C*��w
��������1=
ȼ��!�X������rD�o���[0���_���
��rT�o~�o~ ���1;a�%��&?�V�����X��l�#�)����)��/��@�@�*N�b���ɏ �: ��c��(o���&��;=Ǫ��hH��@2��1`��Xqu��A Rdvz��V�n @�1`��Xiu[����id�إ�ئ�������!�Tݴ}p[�  �"���o�>��}Nh��WG8m��>H*���X������� `�9�i���A@ҷ��xs����m�(�o�q�>��}�T
��1N����� `�;�i���A8��J0��l�,Y�  ��B~�ۥ�[�ҡ���0o��y��q�P:�׸��v阷KwY!�)R��c�.}\����@���c�.}\�c7�D$q �ܥ�ئ�ݮOhA������v3NDR%|�7U_�V��� ��p��ڍ4I5 0>�T}��d��"H`|:Ʃ�j7�D$�E�O�8U_����J��q���,� `|9Ʃ�j��D$���r�S�u_Y���~��%~�H���D ��ߥc�.�BR>Z�����t��%~�G�GO2��^0~����o�H����@�?勗��uB x�vlӀb�cD$q �����M�]m�T�@r|Ӏb�RD$� @xr�ӀbWJ1E����h@�P�8�rr�Ӏ�-�$�L����%[@	'T��1NJ���T ��1NJ���T ��1�PJ�B/�4^���1N���H)��w�9�]���R�8*��]_N~ⴛ#�
�z��T�xp������R$�R 1>��\ѬK�����\ӬK��K�T��Qͺɺ1�
��\׬?��?�T �Ց����룶�Ց������ဘ�e��l֟L֟@*����f��d�� �B �n�l֟L֟@����Ȏ��ܗ�Q1E�@vsd3�������u;�f��|�Rc8*��6���ڰ�U ��v8D�Bf�pT �=;"J孶�8<�z�0�5���� ���fjk6�5j)��c������ �2 ��j��fS[�� �z8��ښMm��(&�pd3�5������%�pd3�5���T�Ñ���ljk R! dOG6S[���QK �=�Lm-W��료������4ߛ��Ed��������U�s:BP�_���2 �^�á��/U���'���� �<�Ǽ��_����1;�ž�HE P���_����o���*�r�b9H �0iWn'K��fg��|� ȩN,)�� �sv�����R�~)G@N�b9H�$�/�����f9H�$ �E� ;9�YR,	@�f�Jrd��^�G��	����ִ�nM�����i8��;�=��JZ�|?�EGL�ٺ���!���P>�c���r�PT- �<F�JvD�PT- ��u%;�Y(�����u%;�Y(����1W�t])�k��T�A��u�8�YR-	@b}�ו��f9H�$�c��G6�A�� H̏�R�,��� a	�u�:�YRm�+ ��1^W�#�M|�˸��TLוs��-R��"5�#��p]9���&��nRc8zm,�>g��*���Rc���1ZW��:�Km����X�u圬c���^�ƀ�"L֕�f�Y	@z#.�n�iA�E�x�� ���f�Y	@�@�p]�kA�E� �dwG6� �"H8�������f�Y	@�vc��tG6� �"H ���+ݑ�"H����0]W�#�E�f$^'#@ {8�y)�H̏�rNױ�j�ݨ�p���+�pۨ6ߍj�s1>f��9[�6��w���[���9Z�6��w�Ñ��+�dۨ6�F5$��h]��h֓����Waf��tL��l��l �c��LG5��v�� �=���r\�z�[=��1]W�#��C�F5$��t]Y�lVu�����t]Y�lVu�����ue9�Y=ԭ���\5c�٬�V 1?j�z;�Y=ԭ
@b~Ԍ���F��nTc���Q2ֳdd��Q���Q1ֳbd��Q���?��,�F��nTc8bzԋ5��e!ò������ȃqX��/{:!5��<φ%#��`����ٰ�! �3B��qĒ�a�C g�6X��%Ò�p@���MD��a�C �A�ő͒�a�C ��kqd��aX���!!���f�ð�!nJ �}ʃ�~�����G�:XOup�o�<����h�)N�͟��>�C��68�7�o~��C��48i�0-y@��`��h�<���bzH���s,y��<�UiW�4X���%Ӓ� $��4X��%Ӓ� $Ƈ4X�#�%��,�X�`m�l�<LK���`m�l�<LK��qPkwd��aZ���iy�vG6K�%qU� ����f�ô�! U�ݑ͚�k�B����)��Y�{;�<]�`=�Av;�|oga8Ep��)��Y�{;Ñ��`=�Av;�|oga8byH�����,s��2٦4���;��=�=��m�g 9��=�=�P�*F���&��/��@⍘�+�S��#��#? ���r�&��/�ȏ��U17W�#���y}iod'G6{�{� )�15W�d��b?�[���b}���e�=��Ǟᨪ��t8$4���8���gJ��]�G;� �̈�m�s�f?�:rUd�%;��#�["HzȈKvL�G~�G~ R]TgG5{�W{�����q����H;� �8��#��Ӟ	KȈKq\�G~�G~ �##.ő����Q �]��_������#�=�=��6�Avud�G~�G~8 �GV\ά��l?ߛ��XIq9�bv��|��g8*��3'fw���n{���}0}���r��^n�|�7��Ȉ˙����ݞ����f!��G�����4�4!�BH8���n�jB��� ��}p��,�4!H[� �;�YiB�TVȈKwd��,�D}D�@vwd��,� Uv@vwd��,� �����f!�Y	����#���f!$ ������fUC�>������Y1��~�w�3�u@�9/�����#Wø\9�������hkD��r����q�p�WŴ\9����s�q���1.W�#��.��.��:`z:����M@@����Q�z��z�H;��z9�YAԭ �� ����fQ��( ��11W�#�D�
� ���^�lVu+��m����٬ �V �����٬ �V ����ގl�M_8b}Ԍ���5/��    ��;��P2ֳd4��A��cM���"ȸ���cM���"ȸ���byԋ��c���}��A�)9��%3���2�)�s,��� �:Sr&c�hX(
@b����	V,+���Y@��N�bYѰ�( ���w5;���轗��?����Ʋ�aYQ R�`��Ʋ�aYQ~Vˎl�ˊ�� �8�YV4,+
@�G��őͲ�aYQ8 ���]-�l���S�Q�
\��w�ƴyĴPX5��s�nҘ6��
����z��M��i3p��ϱ�Ic��1-���1vWϱ;v/����0 ի@tuD��hZV�`��c���i�( �d����eEӲ� $�0zW��eEӲ� $���]m�l�Mˊ��#@ �9�YV4-+
@�X����fYѴ�( �f��#�eEӲ�p 	��l�MˊP ���,+��&@�X��s��K3�{i�
V����c����^��d��c𮞃w�^���K�p����s��K3�{i��cw��c���}/;�z��h���$���p�XZ�,-
@��a�NG5�f�e3Hl�ѻ:�,�Y�ͬx�^��]��l�ͼ��0 �>F��td�lfY6�����ӑͲ�e�L8 ���]]�l��,�f���wu9�Y6�,�	@��a��.G6�f�e3H̏ѻz���]ks�Z������ݵ6��5$���];F�讵�w�1 1?F��1zGw�ͽk��c��72} ����G"���w�vlӕ�?~i��0~�nG7�p�H��0�ג�.�w(!W�O�p�5�ޱ$"��m�1N�����8�Ж�to���I<!^�B�%�8]p�p��P@�����O"��}� [v����(I� %dˎq�;���\]#H`<;����HH� 5d;jH�km�]kH| 5d;jH�km�]k쀸 j�vԐt��ܻ��x j�vԐt��ܻ���q("�QD�]k�v�1$q ���8���v��Nn��:
�V�tc��l�H�(%[u|ӕ���="���l�Nw����xB| �d��q��ݶ�G$q���:���v[����C=ٚc��m���I� ek�q������k	�7�8]�n��#�xJ���ts�^G��	PR������~@��|�QR������~@���>JJ�k�4���r()�QR�]k?��4d%%ݵ����o'`��lݱM�-%�H�(+�ptӀb[I"R$�=�4��Z�xB\ �e�pPl/ID@mنc��l% ��P\���%[@	H��.�t�Ӏ�-��5�Ƨc��l% ���l�1NJ����P`���(%}�/@��v�������Zc8�(0�.0ߵ��]kG,�����r�]k�ݵ�p��(/�./ߵ��]k�][#8`zW���Z[{�㣺l��uy����~�s�nZ{�; �Gu�oGu,��޵ƀ���.����U�ڻ����e�ٱhZ{�룶�#;�Lk�Z[��k���ɑK��w�1 1?*˞ٱbZ{���ɑ��w��b~ԕ=9�c����5$�GYٓ#;�K�ݵ�p�l(*{>�f��zw�11>Jʞ��	������Fp�tn'���Zc8bz��=���zw�1�<�ɞ׉��w��byT��8�c^���5$�G5ًc:�5k�Zc@b4T��8�cV���5$�G5ً�:&5k�Z[���ős��w�1 �>j�^�1�Y{���Ց3��w��b~T��:�cB���5$�G!٫#;�3�ݵ�p�l�*yN�-�g/�7��}wHL�RK��ɛH��j9��b ��u@2���^�aI�2y3 I����:�{9,�^&o �]0@4�,�^&o ������f�2y3���#�%���� $��`\]�l�_/�7���qu9�iz}_���. ���W��}�=Ҷ��f�o��r�>�7�u�`\;��U�s_uπ���k�`��~��x!@@����m�z@R�eɱM�������=w��5Z��V���s���M������$.�	���^��^H��ᝪ��^��^H��������m�zT/��ٶ��^��^H�t��1N������$^�W�Zv��z��z=�/��U-;�?J��Ty���U������g@�w���U������g@�v���U�������� ����*z���W�3 � �\Վ���U�Ӯ�gH�����ئjg��	m�����j��H�H�t���M�Nۑ����V�:©�iK�#�� ^�j�1N�NےN�A��1N�N[����ƛc����'="i��7�8U;mQz<!^�7�Zs�S��6�G$��j՚c����*="��Əw��U���_@�x�ǻV����� ����V�x׊^u��_@R{�]�v�kE����/ ���U;޵�W�?����
/`�;�iD���Y�u���M#�]
�������4�ح�I\ �[���Ů�%�� ����%[D	H������%[D	H�u��1N#J��Nh��O�8�(�"J@/@�٦c�F�l% ��Pa���%[D	H�����gA����5Pa�]a.~����!v�y����s��ו�3�cԗmחT3xp�����˶��ů�_�U�G5��ů�_��{�}��rG4k&k ��g 9�YC0YC0�AT�f�d� $�Gu�o�5k&k �]&�٬!��!h�d'G6k&k ������f�d� $vCeٓ#�5�5���eO�l�|&�x!@ ;9�c���U�H{� ;d��f�W�3����с�ڰ���(){���@�l��e���(oy3����t8D�&o m����f�c0�-('{��(ө~��QN��f�f6y3 ��QN��f�f6y3P�dG6�7ߔ�i�dWG6�7�ɛH;� �:����M�@b~���:����Mތ� ����f�f6y3 ����A6��~�W�3�>���n�C(*��>㣈�-;�P����#�_/����G����%��]o�ᐧG�*i���T+�<<�N����v�5�CŤ�����(�ƉÒ�bIQ8����,)*��rM����fIQ��( 5��Q͒�bIQ ���%EŒ�xs� ����fIQ��( U�ݑ͒�bIQ ��Ĥ]�l�K�$@ {8�YRT,)
@b~������*i^_@b~�����fIQ��( ��1e��A6�Ͷ��l�]\sM����	����~͉	�>��!���1ܯ91_�gs8��QwR>�c����p�s�ZR������͒����XL�麾�,)�� 1=���rT���ZR�� p��,)�� ���rd�����ƀ����ˑ͒�jIQ8 �ǀݸ�,)�� 1?����fIQ��( ��1`���>�(�y~��1`7nG6���6������l��m����X�u㜯c��ֻ����1^7��:��m�����b{L׍s���f[�n6�#��p�8���n��w�1 ����BH��_.��t�ȎiB���xw� ���f!�Y	@b{�׍�~sB��� $�ǀ�ȎlB���p@��	���,�4!H̏	�Q�,�4!H̏	�Q�,�4!H̏	�Q�,�4!���ő�BH������őͺ�}O��>��9`�v��w7�\����q�ױ�l����p����x�Ͷ��lGL��qNױ�l����p��g��v����m��k���q4G4+��DH,��q4�4+��DHL��q4G5+��D��E�h�kVu+��E�h�lVu+�P����٬ �V�!@ �;�YAԭ �W���lVu+�P ��٬ �V qz��;�YA4���y�j�q֌l7�zw�1�"8��,�n���fc8b{T���n���fc8bz��,�n���f[����g��.�Y�b$�G�8�#�E�a$ ��Q1��fdX	��(�tT�2,� �=j�1�,�� H���qLG6� �"H ��fˑ�"Ȱw�	�^�lA�E� $�G�8�#�E�a$ ��Q3���fdX	����rd�R���5�<k�I�    �y}��b}���,'��殇��<+Ƹ�������2�`�g�8i=4w=p����Y/NZM�����L�hV�ӣb��1��i�P ӣd��Q��i�P ۣf��q��i�P �f�ɑ�����Ŗ�]5�̎lVM����5�̎lVM�����}'��B��l*����޺�!�� �2
d��d&揾i�W�������L���c �V:���&���1 Iv�������c8 �5��zwd3,�H�������c �V:��l���1 i+dG6�ϋ��Q� �=٬>/V� 1?��pd���X}�TI�Ñ���z��ϵ����5��~���3m���IC�!X��L��\���N��z�%�R1/�gw8$	�;��j����9O�]Wˮ�v�A�rD��Zv�����c�e�ղ�p@L�����,���] �=&��r\��Zv�TH�ˑͲ�j�u �N���#�e�ղ�(GP�#�e�ղ� $���ܸ�,���] 1?F����f�u��:P!eȑͲ�v�/m���sb�]_�������Au��Pݮ��#�Ǽ�8�����뽾����q�q�˱���{}=�Q<��r������g@�H���"H�h#Lg�4� �"H �c^ndG5� �"H �c`nd�5� �"H RdgG6� �"HT#����Q�,�4� H� �8�YiA��s�8�YiA�1?&�Fqd��,� �Q@vqd�R>Em���sb�]_�����6���90Ǯ�_��������y9v}�z��g8b{�ˍs\�]_������( ���c�ׯ}}=;��t�Ѭ���I�I��1��n�P �c`n4G5����CHl��q4���z�[=��͑����z��t��٬�V ������f�P�z(H��#��C�� TdwG6����CH���٬�V ����fd�ׯ�������g�Ȯ�_���G���q�#��~���3�=
�q�����^_�>WD��"��e�\���A�tD�2��k+LO�4!�BH ӣd�Q�BȰ���q�BȰ�TI�ӑ�B�{�
�V:�^�lB���p@[� {9�YB��E�X�lB��� $�G�8�#���a!$ ����#���a!$��B�8oG6k���Sm�nz��5�xe��01>J�y�����^��>ۣb�g��.^Y��+GU�	׻g��Q��N:x>�I�iQ �N:�N�hVM+��!@`:9�YA4� 
@bzԌ39�YA4� 
@*����fѴ�(�h�ّ�
�iQ �h�ّ�
�iQ �h�ّ�
�iQ �h�ّ�
�iь���ƙ٬ �V 1�x�vb�ӂc8�V�ؔ��Ă����j���Xp��X3s�8���u�/1F��92�hp\;8�x�����<'��kǀ�&���ܢ�q��p��y�y��-���s�<���9/�hp\�Xs�:�Yp\�Xs�:�Yp\��#s�:�Yp\�3����e�1 ��137�#��e�1 ��137�#��e�1�cfn6G6�˂c �cfn6G6�˂c �cfn6G6�˂c ҢCs�;��k��el�V����9��m��mI_���16G7�����!i�ss󘛣���^����#��cp��n[{wCRe�s����ۖ-ocP:��9�t	���I�R?� ��s�����\>�P }8��"�{ǔ�����p��U��*J]�ts:��2�{ǕxB}�ts:��:�{G��΀a�9�t!��cK�Rg�8ݜ�v�����%B�3`�nNG;]Jp��B�1R7���#����΀��y��=nk�qcH�����T]��"7v@]cu��������Ɛ�0W7��:��m�UnI�u����ܖ�rcP���[���n`w?,��蝎�r��춄=B�#`�nݎt���ְG(�٭۱N���"�xByĘ����=�_P��[��Nw��2��΀Q���t��c�P��[��N���Bvr%�B���h��m%{�Rg���ʎv��ݖ�G(u�ܭc䎮u{~����[G�I���������u�t���!�'��\G�I7��>�_H�(;�Qv��n?�`��v���QNLv?�o�O�!@9�i��5(J��*�t`lJ<���t�:0�
%B�+�
]��NL� ��P���h�&[�	P��DWu�� �-��u�
ګ���l&@�3�]��NL� ��P���h�&[�	'�ފ���K��i�a�������ؐ�����s�l1�0��E����Z�����^X4J�-�mh��w/,����v5��;g�P˄�� ]�,>���TLW�4�N˄� $���¬�j��	k�6��uu\�ȴLX@�� �͑���{0Ү�n�l��	kH̏����,&-�bkH�@vsd����w�����#�ţe�� �ki����f�hY�8��4@�1�@o^�`��O&�0�c��^��5�IL�a���x�{�ү�Ə!z���ސ6!��1�@o^v0���3@�p��t�����'·㜦;��;�:��p��t�t'@��[`}8�i�s[��~��>4�y�fP�~h��v��ܖ���B���h���m�N�Rg�dÜ�v��ܖ��_s�OG;MwnKw_G�/G;MwnKwԯ�ڗ��.�M�g�G}�p���}+0CRW���<&��k_L{C�Ώz/���3�_?�F�vH�ݶ��F�_;#)�H��l�m�	�:��QN{˶�6B�`�aݎs�[����ۚ�r��޲m��P�vXO�u�[�=��9�P�=9�io�6�F(�L=��h��e�u��0�������m�m<��m���h��e�w�~О���lo#ԯ�ڳ�����5zI}����?5�����v@�����ҋH��8? �5�?���kg�������ׁ_7�׌���.ԋP�(=Wq���bW�E(�T��8�i|�K�"ԯ�ҋ#���V��֫c���X/B����:�i|�_����U�4�d�/�:*�U�4�d�/������N�K����P���h��%[|	P��GWs�ėb�q����Ҳ�v�;eP��������:��P��O��?�9��P>Wݔ��t�RJ�B�%��p(���/���(ӡt�ҿP���e9��P�������ۡL�2�>׏��PCY�?�B��E�5����:���7q�M_8�wq翉�o�_8�F��<8Q�6��q�É�p�_8�I��yq�^��ׁ�g���D�8��t���8ԓ����mD���ˉ�r�_8�m��3G�ޜ���-���s����/��B���L�9�/��2���s��8�}��8�ԟ���0��k�Ǩ?��������8�ԟs����|��8�ԟs��i:%�y���s_8�k?���9S�����Ã��9S�������s��\��t�g�/=p�?���[�����i���O�z���R=`��"i��}�ͳ;��ק����o���!_�|$*`ƅ���lr���DޙJ�\�+��`�Wb��F~̅����r��oD޹J��ÑWr0��/{�0bp����|��V� ��W9aX�b7�E�~�P˹1KW��+g\o7m9?f��IH�G^ΑY�������%��'�t�n��8�zG��se��dKW������|��+v^���3?�ցC�9�/����f���MzG쾀��A�9��R���q��ҕw^����,{ı��O����_��{g{��w�^����D��L�:�I��w.W���[�3�Ca���i�=s�ÂO��p��S�T�C�U܁�_������ǈ��}��^U�����g.���T��r�k�\�X�pt�p��!��Xnpt�p��!_�b�\��>�0���ΙY�T,w
8� 8�7W����������NC�^w~`��%C�^�9|��� ��    0�	hwiF���	�ဈի=r�O�y���J�1�Z���7��#��	�j�j5` ���p賉Pi�U��!|<���+�r�U9H����2�Ҳ�ZYh>}֥��t�>�z���u�v8�
�W�uW�^ݶW���y�[�uN�n�̭��n}���u3�n!e��n}��u3�@j&���C7����:�v��LZu�fn��n���%6����: i��q�t�e��u3�n	za��í;�O���pt�~�t8��}��=$8��uJ����]�u��zⰺ�[�hk1h8 b�n�F ������ؾ[f�tC�P9�X*������������%	i�r� ��m�r@$�uK�{Htn]�"A�[3) iX�[�íu��:|��\�u�����np�������ۭ�.u�[�z�0�����cA�[�u���0�@z�ܺ����0�!y ��)����: �� �; ����:P��֧6�[s� ����O�iP���H{�p�S왴ڜ�Y��cA���>ŕI��y�/��n}��֛sכ3$;��>ŃI�ig �#n}v�'�8�?�
�>�㓖��J� �W����~��5級3 ��­��E索s���Y��g�uҪsZ���� �>[��������I���˹5�;�՝Hs/����[4I_���=~���٢I��Iz��g�:b�����bd���%�˒����M�ǭs����$= �U?@��/K�Wx.H�L �'Kҗ%�Hu�
��HL[���*�=n��%	j˒� �u��v�$}Y���2� s�睂 ����x}����$�D��u���!�T�T����gG��Oxv'���kG$93���vHL���o��v·kqHL��sG$]o��D�����;"iV ��!19����lb���ݲCb��;"�K��1<��3�nyR�l����R����\HK]8�1+�>f��U<�
3��I�K{v)Uy\�-r;��@���r��	^�����#�7�+�	L��٢[TF.< ��z��d��_0����\���+|�.�;�83W��L����଼*V^�q!��g�/��*V]��\����{D������]���j+��p�~Q���VZ+��ޢ'�ʪXe腻P���
�b�U��E��qpVW��S��%��q�oVV�˺��_�#���5�YUU���oFCgp�oVTU+��0����9��r�����	a�>gS����������Ê�jEU �?��oΊ�jEU �^<�;Q�UՊ� XhT�K'갢�ZQ���	����|�]���}zK����͜�N�G�x�p�o���_�������ܺ�[ �7���jԭ�?~c�7���jԭ��u,�Lp��4����: ����ˉ:̭��u�F���^N�an�̭�����ۉ:̭��u8�Z�)�sk�+h�+@����r��7+�������h�ܮ��j�~Y����$IJ���J�~�/�� ͔s��ӊ�[� J#�f�9L�iAխW����%�N�a�T�^A8�Zڄى:,��K �,
m��D�LwK��6���ŉ:,��M�B�7�uq�����
���.����z\��g/fJRq�o����_8������}3����\G����Lɠn=̭��p�s�dP���H��[7��0���A�P-nݜ���z�[ 5ܺ9Q���0��҈| ��N�an=̭��[w'�0���ှy ������yza�"c�$u��f��<�0~�1W���}��s^�����>c�$�c%�V��*� �����Ϲ�IK�i%g�Fn=���j�i5g Ҽn=��Ê��Yv@��[O'갪sZ���"=��r�+;���H���r��;�՝A����%i��7K���)|�3
n�\�%����ߧ��9x��7K��5�p4Dk�v�o��/K��}���s�d�$}��'iͅ�`r�Kҗ%�H�t��tX��,I���5��NN�aI��$= �D:�ى:,I_��GiD����N�aI��$= �L��������J_�&���k{�7k�~��.��M_h�����;���ɒge5�Ӭ�^$͘-��lɳ��"�/$��
�]��Cߕ��sqD�����:���{{wD�30��	;�}�{�w<�/����c���wo��H*����v�;o�v����
�9�o�ݻ���ޡ?�L�������]2�ǐI�x�{��M��m��ǐI�x�z�p���F_�|d��\�,��^��s�l�-�����|dj�l�-h���N��LV���/��;�&83X����g�g�*���������V_��t!���	:��*V^�z!���):��*V]�q!���I:��*V\�Bo���tXmU��
8z=6p���J�b�U�F.���:I�UV�*���\���y��s�p���߬����
0zA��feU���.=��sXUU��
��o����ê�jUU ��#��tXUU���ڈ����TVU���oF���Y��*�>�x�9LXiUU��
@j��p����UUH���p�o�����0&�0Mx��fN��c�	Ä�t�o��������Tp������[7s� ���[O��0�n��ယ	n=���ܺ�[ 5�z9U��u3�@:��>g	u�fn�����,a�n�̭������T����:j#�����߬��ק��F��r��7+��U�>�-h�$��f5U���*1�$��f%U�fA �7#�MIN�a�?l�q�����
�n͂ ���M�����n͂���w}�s���\�[.��K�>�9R_� P�:��r��ũ:,��M }����k3�W���;ܺ��7s�q��鋄��T]�����*1p��z�̭��u�F�/���I:̭�_�/0X���t�[s�p@��us�s�an��p�s�dP���H� �>GKu�an�/��ݩ:̭��u R���ݹ5k��6"E	����7+7����ɒ4\��՛��_������
�y�/��['鰊sZ���/���i:��Vr�B���t��9�՜Hߌ�[��%��ӊΨ���-���Vu �t��˩:��Vv�*����SuX�9��@�n�z��7K�ו�pT�D{�v�o������Bkv4o��fI����>3����Ò�eIz�F���+T�I�H�t�uX��,I��t�ђE���v�U Z��hɢI��$= �[w�|g��$}Y�c���N�aI��$= �������(�}���Ť-��7�yۻ}��.��MG��k~��"���5����{�vDҹE�vq�]6yoߎH��oW������v�(X���|�<P��_H�b2���0y�E�H�� ���v���{�wDҺ�ݜ�C������	}3~�������T>t��2I[sӡ��mt2�ǐI�XV���F�BW����!���#֖�F�.Hp��H�_[�43KñD�*��5�M��ׁ��h�_[���C文�~Ul�h�[N	<�~ѯ����H��!w���_��b6�	qp$o�H���FK[i��Ñ��#{K|�c��#�8��|do�/\L{�b
�!����#{K|�a�[#�.�D8����
������� �r�o�o��w2$����#�K|w�^X�N� �+G����}� C@W�$.�}u{�<Cҝ����#�K|cݾpb	�'@W�4���N���x���W�/[�[���:i\�i��ԗ-P�8�!�+;�[|m���54�s�?�����ŗ���,����ĕ��-��{]���XI\�I����^����n�r
��I������?̀�������_������6K{�p�oX^{�2�?\���+��^i��������:�n��!̀t,���ek{׵�R�8b}do�U�C��/��{}�U���61���$�t��[��ĉ�����.�vV��ֽ8 b{��$	�H�J���>���Ddn��(�n�~_�U�[��HL�v�b �ͻp    ��	jvH���cD��í�+��}e���1!\fr8�h�և,3x���ج�N�p���2ǉs����et71�z�����ŤHl��ಊ"&+��$��|pY�����/�.2&#�ǀpY��HT�U7H��	�zgDBQ�k@�RӫbD�K �ʾ�&	������������>�c<�w�ѷw���.Ñ�0VSu8��u'!Gl����ƉÒ�jIH��cB������8�3b5Dl_-	��M��!���"���� ݖ]�N ��TKB�1>��j��ĴjIH �cN���HP��� 1?�j9ܚ�����w���cP���p�[�í�|iŠX����nۭ�bӫbR��q�0�n��H~W̊�v; ���������sĭ��u �WôXm��n��hܺ���u3��2��ŴX���n���_�b�;.�[7s��T���ݹ5˭���H�aZ��ï����}�}�~=��!��;�8b|��11~߹u��cX����a���5�eta�zDl�-�@b{L����}��:�6 ��\'˭���H�aZ���HL�[�b|L���	j�r� $�ǴX]�������2 1?���}�5{w���.2-#�ǰX���!n=�[]��3q��:n6�ǭ[�O����: �?L���D�z�[�b{L���q�an����ki�@̭��u ���n9; ����:�ˈ�1-�rs@ĭ��u �cZ��y1���q�� ���܆/���'���cJ8}\0g7�E$]ͅf�1&�>�t�;�"����y8���y�[Ԣ�Q	r�tr��f��E$i �L'��=ovS�L������{�7��+"�-V�{��ӗ��=V�j4P{�k�ӗ���QH| �|�'~]S��5E q< �1.��I�fl&c��	Pn'�P�;�"��4h��N��n� ��*x
���=��m�]D��ρ���C���*Ҡɒ��C�V�E$q<	Jrru�l���((�5�Y���!C%4�뉳[:�����]� (烀�1\���	&�����`z�ڷ�2 ���(���Ŗ���D�.N�a-�d-� $���p)N�a-�W�f@b{����b��꣎��:���ؒ���X�å:�����zy��ܺ��8�E��SQ�n�\O��"�U�>��cå5�C[���
ܺ���"��"� �uwB�E��<�yP15\�SzX/"[/" �p��֋�֋��[���^D�^D R�n=���zvEz< ���IN�a��l�� $���I�)�j�r}�ƪ����뉳Z�\���&O�l������s'e���Z�j����z9���Z�j� ��z9���Z�j�p@l�������j�b�V �c��N�a�V�Z+ �D�6���Vk�����7Q��r;���Z�j� $���I�]S�%!���x�@��8����YR��p�l̛��Z�,	����\l�i�zN���z�~���]�hf���$�Z�T�A�0;�%!Ւ��c��1pR��zX��ˀ��8��i=,	���D�D����i=,	��� �'�8��%!Ւ�p@̏��Z\S��u;�:̝V̛��z�̭���a�bܤV�gn�.눇`�&��6ao���V/�cڤ6��0�n��Q1 �usRs�fn���MjsZs�fn���p������: �D��N�an�̭иNjwZs�fnt�"@p���,��ק�
�z��8˭�e-��aܤ�g�u����ôI=�M�[�k�����1nR�zXn���T��[O���ܺ[n���8��I=,��[Cl�������r�n�u R�n����r�n�uTL������,��[ �_n�\S�������Р'~��8s�q�/1>�M~����-��#�ǴI;�M�[�k�ջ��!�ǸIKN�an핗�S�&-9����0�����0pҒ��̭��u �8i�i=̭��u8��������: ��1pҲ�z�[s� $���I�έ�|=L���J4�.����慎��ZA���*��F������P�~,#%om�H��D$tÏm��?�C�B���g:���d����4�{��{���d��B�Mߢ��3]7������j�r�p�
T>��C� ���z���|�/ }�Z�r�p�
T�W��	q< �rZu������+�?�ŞԿ��
4X�搸ړL�	O��@9v�%��.�r��$>��@IN�n��d"Hh�$'�P�wIߣ@�%9����7'��((�5�Y�-]�?I%4�뉳[:�����]� (ٵ�Y�-]��f<JvBk�%k� Uh���)=�Ŗ����`𰜃�L���/ ��0xX��C&f�T��u@̉��R���Zl�ZlH%8vuZk�%k� }�~]���Zl�Zl�!���ai�)�z��_8�����뉳^D޽����*&Ks-q֋�����1xX�zX/"[/"P�nݽ�A��T���5
��9x�^���/ ���<d��=�&(�Sc ����^D�^D R�n=���z�z��pU������*W���1|R�뉳Z�\��sh��ӵ�Y�U�񅣯Q���눳Z�X�����;)�	=���/lys'e9���Z�j� ��Q�����x��L���N�9xXh�U��
@B7�N���Vk����h�f����j�b�V �)�f��[�Z�X�����9��5�YR���`}L��B�q������G�&Kr-q����:�a��bڤ&'��$�Z�T�A�0;��%!Ւ�p@_�@��7�4	��� �=N�9pRiR-		@b{�����TKB���ܺ8��%!Ւ� �/R����zXR-	��� ���k�3�n�[�KS+�Mju=q����_��{p��Z�̭۵�p���6��	=̭��u R�nݜ��ܺ�[C_��[��&��u3�@]�����I�w��G���ք���ڝ��ܺ�[ �h���i=̭��u8��Q����z�[7s� Tn=\S�����_8�����뉳ܺ��:��z��8˭�5�p�G`ڤ'��ܺ[n�[O���ܺ[n��5
��9o�� ��: ���I=N:ͭ���H7�í��zXn�-�@*�����zXn�-�:��G�^N�a:_7�/ ��1p�n�gn=�����S�&�v=q��c�u���(��]K�������b{L���uę[s� �
�������: �k�z�y�A�z�[Cl���v�����: ��1pҲ�z�[s먘�������: �?�������:�cझ'�֞���CT��[�yڤ_�y|=B2Ӑ��3M���1��#|.��vfi�~=��z�������a_�i_� $^�,��Yڤ_�i_���5�i�L�&�zL�z �j�i����ok_�� ������3Oc��}���	yZ;�4v�����I���?�4v����1 ����3O[�뱮�3n}�i��f�nͬ�ܺ#Kkg��hkf��L���>�4v����=@mߑ��3Kc��k��8�iZ;�4v�����b{�im��Z3�Z3Hl�<��y��m���yZ;�4v����-�OU��<��y��m����yZ;�4z�������r�<�y��m����N��ڑ���־���#O�G�F/x[v�C�#S�G�F/x[W���K� W�G�F/x[v��"U����#[��-���!� ]�G�F/x[v��s�oFۑ��#_��-���!�o���	��m�o�x2�����`�n>F$��l�H��ok_�ƀ�	���#e���}��"U����#e���}�@�֏a�Rﲗzۃzu	�cH��Ի�^��ט���)a�Rﲗz�	as����/�.{��!�`R������e/�2$��
�cV��Ի��E�e�	0,܏aa�Rﲗz�x���1-L_�]����֠^�����/���R/�n8�1-L_�]��^v 	���/�.{��!�k`\������e/�2�!Hp�c^��Ի��E�e� ����a�RﲗzR���13L_    �]�R�b{P	~Lӗz���ː�)��cl��Ի�^vB� �c��7}�7��K�G| cc��_��/� L��wj8=��3��`��wh�9J�ί4�@b~��wh@�� U;�����Ό��_�?@�HH���xGƞs�4}Lv�cb�R������Ib|��w`������1/6�y1����e��0���t�5�bTz��#�ǰ�H���?3��<�b#���a�8BF�F.'NH�����: �Ǭ������$�=��F^��=�)��e
�.��c|��1,6J;�B�����H��a�Q�"1Ë8�w�i`Xl��HP��"�0,6���d˩ �+}.�Ǭب�ᐿ��rJq���v8�+;Z�=F�F+'��Ţux{X/��[�����X�@]���m9 �-�㴌 ��{>�X�.�P�&��Fo�|c�E�p@��	nݧ"��b�: U�[���7�X�@C����pk��4��r���;M'f���X�^�G��Y�1o�C�_w���2�����aѺZ�@b{����}���=���\��}�h���+; b�j�: ��1,֊돳
k;��
�6_�gּ�;no����8��浾p�b~�N�a�մA�p@�t�}Xu5m�# ի!��TV\M�@C��?��pk� �$Uk�oN�a�մA� $e:�~kN�a�մA�(����0�n����5�w�g�ߺ���������8��֖�N�uw�q&��k~���[��0�o��$k��É>L�[&� �=�~N�a��2�/�'�>'��pk� ǀT��[O'�0�o������t����I~H�����av��7�1 Uk���5��������o�_�/N��݇c�guǌp[�-N��ݗu�����~��8]vo׎H*נ�r;݇���o�� ��~�"�{;wD��p�j�vo�H�נ����CW��۽��!>�Qវ�Cw��ۿ#�8���1,Lo�[vUO	}���T�Kק4"���{v}q*���~��pϮ-Nſ}�O��p/�+N�?��'"�Z/N����Y�0�ҋ�}��gw�D$q̢��*���?D=$8xu������$��Qzu�����xB� �(�G���.{��!�^o�1N|ﻋ@R�c�7����EE����8u��.u��w��n+�x"Hp��d���ƿ�3)�;݇:�-��'䛍��>��Cܶ�E$�k���)?��m�]D�\JN��n{!N���~���{���ːT���Og�ngS��zr������4�u�?U�	�)}��x(M8��s� ����4��/�j���i>�4}���$��\J_N�	����@�>�R�}; �����j:.��}Bi�G�?�S)�v�O(M��$,a(e�C)Tix���Uk�qIN�	������#)#��8k"�+��1�2�k��&r�Mdro��8�Ȯ;Κ���_8�ՠ����Ú�ٚ���΁a����Ú�ٚ���΁a�Q��Ú�ٚ���΁a�Q��Ú�x����1�2��}X9[���90�2�r@T��&��w&QFu�k"gk"��:QFu�q����\;�uu�q��տp�#����,Z�k}}�Zܺ9͇E�b�: �kbe4'��h],Z�!@p��T��E� i�Z��N�aѺX�@���@��>,Z��Q< �u_��>y|�Z�N�aѺX��51~2���d��ў�p��>c:b�zH~a@cb�d��p����A�P�n=�b�5��$���ɘN�aѺZ��� ���S}X�����5p��d��E�p@l�ٓV\��X���y����8���e��p�|C�o��I�i��H���}X}�T���!��TV^M�@�Q�U����ׁ$@h�7����j�`| R���tV[M�@�z���>���64y�!��tVYM�Y�T�A���9���u|���a�u�g�ߺ���F���\S��xp���*����}��L�@�v�z8Շi~�4� $�G�o��>L�[��MCl��ߦ�}��L�@*�����}��L������t����i~H~'������4�e�_8�r�z��8��ϟ�/W���k��a��p���9lǢR�}qk���`3��v��mX8 �h��N���·G�D��V���C��oH�[a�''��a�ۆ��	l�ZIN���·$}����:,|۰p@'��a�N���·MC4��S�Ϧ���\��av�q��ٸpTOm�cU]��h_�T�����P��n�'��
8xq�U����$.�Q�^��C�?�("�`�W'�P��.���
6p�����M@I_���W'�P�Ϯ"� ����~��gwE$l���uƩ���w�\o�1N<��q�)�HI|�]���"��5p������m�]�4��
8xw�up��� ���S~���<"�|8�:�m��H*�����~���
�xB_���'�P�xI� �)}:�:x6H*����돳IKS�����W���k��I�thazc`0���D���Xs<LZ̥���l�2٤e җ+���i>l�2�����K�ˉ>l�2٤e �c.e��R�'�/ Uk�q�������Ň���\����d6�l.> ��1�2�퀨�M�	o̤���:T��6�����_�Fbr�q�D����b}���m��/�j�F̮;Κ��Z_8�r���i>�����L.��Fى>�����L���Fũ>�����L��F��>�����Ln��E��>�����L.�E��>�����L���D���Y�.W����+��յ�Y�.�\|���ʨ�������Q�^�\s�E����Z�d�U�8z1�8>�a/��u<��J �{ԃ�Q�-Tn����u�NAe�����:�c�����a����u9�FdP�H���o����5��H���o���=\Z�d�����ۺX��g�}�m}�m]�̭�g*
��o���yr�'s^�ݧ�}k¬�#|2�&�T��}n^x-W¡E��� �%y>�~��.@��H�LI��������j����������ۺ �[Zk�}�m��m]͓C�=��~G������ihԦ����x�� �5i�w��>���[V�(��:���q�p�ѧI��:�����/�/Z�M�ئuz���_p�ѥI��:���_�h�]��.���@��o�TЦIl�:���_�h�}��>��k���_���FM�����Ӻ ��Q�بuJ��.V�ѨIl�:�uwZ�U~4jr'Z�k���_�V�ѨIl����h��R}�i��A�����*>�4�mڠ���^�u��!h�$�i�^�ï��j�>Mb�6�?��/V�ѨIl����_�h�����Fm�k��_���c�V��������FMb�6h�>�I/@��h�$6j�&=�'=��CѨIl�&���.["}��>mRZ�/������c�6)���g�]��.mRZO�uZ?m��6���@��v3�>Mb�6)��Ӻ �ڣQ�بMJ�鴮[Vh�Ii=��h���FmRZO�u9���FMb�6)��Ӻ ��Q�&�O���gUa�a������R|D�5F������pV�֘&�O?8� SM�,�@��H��Lh�	a�	a�������HkL��o��h�a�a����~�U|d�5f����h��߸�"$�1$L���@�Z�GHXcH�,?����8�����cF�,?��<;�U|D�5F������}�j���Ƅ��&]�I/@���iL+m��?��=�b�%Y�4�@�=�b�J�t�&� ��#,�1"L��~��MzM�, �:f������V��&�O_ �0oi1)a���j�O)���h`v�	���/ҳC�7�"/�!'����H��φĘ��0[��]�A�[��fPN�����l*��ʦf$�4ą�T@��1� �
  1�a�����y�2* =�!4̶��ȏi��5��?�i;d*���5j߉�B�!Ӑf�P���*"�f�P_��C2* E�as[��"��112���.�鯅�PƄ7Hv����-CT�A�$���gc�?5V�.6&�Q���vl����W(c%�ZXa���������z���ٱ
e\x�dW{��FT��N�8�{��[��ul�qፓ]-,�c;Q_��C2*�y���mvl)ꋤ~�7縐 iD�Tw��B&�ڱ����~�7�v6��J;�P��2&��ڱŨ/ԗ��1A@��֎mFԳ�2*�۱ը��;(も�a�ۍ
~6��ͺ-��'B���Ɨ�Ǘb<����&6�<>� ۖ�%���/��/��AWZҙ�Tޏk�3���df6���T^=����L*3���!;�< [�IdfSy���8A�V�d�����T^�V���p_�9Ꞝsd���+�'l*��8�/��[��}�hݝ�h�[��}�hݝ�ՄX@����F��.@���J��Y$��8������ʝdf6������#x+OR���2���1��~�����Sy�V푺�'y'ljI&D+���(q��3�𩼚���=a�� ��[�雳ye�T^�V�[��>a��� ��Z�d��Ye�T^�案�J��Yߟ�^��q~��z��#�̌�3к,[T�me&���z.2�ئ"l+3�̌��i]�V푵���F��:�j����՟��z:�����$���z:�Ъ=��"�>a��N�b��#k+��F��.@�z�֚�F�鴮&ġۊ&Z�|�#\�V���M:3�I��~�����$3��D���1�"EK*3�G��Ϊ=ڒw¦q�X���ph�p�%�/P���y�L�	�Dĭ�jB����>as��uV�V���3�'l
���s=��]�>a3��uV�V���И� KD_6��*?2z%��5�zx��ߠ�"¡w��Y���:�����IefM�.2�U�� ���;aM�z�^�V��И�(��г;`�h�$��5��MzZ�G�C�d��&]�I/@���p��֤�7��0����>aM�z�^���D�Cc��l}��?)K�ݓ�L�:Ҏk�drb:��L�;���]|��{$a��y��C21AIW��i_~W(ky��А�`�D���߼˂�g2Q��Gڗ�ʈ�D����7��/ˉaP��L.
}I��B}�]��B�BҾL�'��uh�u�}�������$-S?�<��q�դ,S?�<\X.1���k����}�"�PM�2����+�Bt��`KE�54>���<���W��}�e�%��i��U�2&tH�-�(�O��*�����|&��i�Vu��r�z�͢����a@Z>��L��}�q!	��eJ�+P�$���+	˔��7��Ƅ	]�J�2���{�B&�2l�(��_��|��w�P(���;�0&�~'�R���S��	���E�T�w� @�'�&(��-<ʸ��z������	#���O�:��/�����oO\��=t~y|~)�@?9���x����tr�_�_
��!]�$4����o<���tf6���e��rPA@N�$3����ñ�?t����t,�>�WbA�8�{����cyZ�G>N�d�����X^��E�J�	˻�����P��0Zw�uZ�G@Nb@�SZw�u��� ���0Zw�u��?h}g����y�>rr'���.�;�3=@�'��lr��>��?h�q��-����j�|�<�=aS�� ���n���6�ˋ7`�hݓ�&�?v@���{�Oؼ2|,�.����A���cy2���?a�����?h=���h=��ߌ�"'#�̌�3кl-T��d&���z�2�M�z��q�椴�N��j�@���0ZO�uu!h-�>a��N�d�h-�?a��N�r�����${��z:��*>�p�hNJ��.@f{�֚�F��.ހ��AkMB3�J��z���p�&���$���U����$3��D��ñ�?4��Tf6��{g���="ڒy¦����j����=a���wV����>q&��M"��Y�,�y�L�	�CĽ������Y����^^�q��6��{g�����Dk6��{g��*?Bz%��5�zl�����tf֤k���ߨ�"¡1�A�q~p\d.Q>E�C�d��&]�I/@���p��֤�7�ՅX@����֤�7��������֤�7����=BCd����� �{�OX��ޤ�O����@a]�z�^��3�О�eTmǽC���H�2��Å��rɳ!ȡ!��r�H�C21Q�B�A��A�r���9t&�U�U��� �g2QhP�yP�@}�� �L.
�6�g���P��P��	�g��>@tI.
�6�V�Tx0]��B��̓��Ȁh�J����v����� �5)��O;�~ZA�$�A��`�9_$�!��PM
��N�ӊ3`Lx ,�d!P?�t?�@}�(�-�(�O;�O�6�����[rQ��v~��
eL萖�3Gd?'��'>���3�(�O;�O+P��?��3�(�O;�O+PF�i�L�2���^V.$(�W�ع��wH��?���`K:_$וK0b!AW���L��/��PƄ	���C�T�����T���B��/��P��?�~'�R�_eS��	�����	�p�7"��@�'�(���:�
e\PP�I.
���І�eT�������W6U*      �      x������ � �      �   �  x�]�I�� ׮��������D�^� ��̒��C���C�>L�B��^XR�Q���ɩ4e��(@�*�Ʀ�����E�XS��E��C��]�����Y2��Eq}��<yS��8<v�NMU}paf�Cha�X�xW��]W��U�'�:mGF�XE7�����:k5Iy*�T�R
sA투)�&7U�Ð���4��䦐*P���Y\�\�5Xx�/�>)�}3U�4�֐ܺY�������bYaٹR��6SU��Q_�o۔H�iפz�x�&���ݳ
����6���`���o,�e��m��Q��޶�/�|=���'����g%k2'䯽�����Pq{$M��H8M6�o���hWUVq.���X;U�^�������Ǣp*ҟ���Q�fpn�w�ŷGA1ʚԾp����呫ޠ�]�c�H��Z�3�~0{om֏�~�]�����ѓ�jǉu%�o|>��5s��      �     x�]�ۍc!@�o�Y��hb+���X&"��N��=ƃ��g����?�/Ar�f5F's�f�9PN��4�]� ��� ���`;�Z����m���"f�n����1�4��c�T�=��2/��$o�1� ��#�D�&��A܄��<v�D@����K��{G���P����s>���+oF�+S�|��00�������Q#U��oЪ�"D�E6�U�E<|ر�|�o���!Y�� |�g
ɥ������Q��N�H��M�ԍ�Z#�ĩ��N!O��!�1��a�&x閰�1tٖD��Q^���e+��/���tb��tc�7,��@�#��H^��Uׁ~ԅ�\e����w�K^��5�趗�<N�D�>��b��_!���D�@�����2X(���B�B"u���a]6�^kBR�F�E��ix��)����`To/w�t�ܵ����St�j临\V֎Y�x���uAc���x�k*�\֡y���Լ�̆�$�۫}�(��	y�l���H�ˍ��4��rO�ײ�r�q=)���\6"d1o�厪c�]��j�`�ջ:������Nı���I��2�F;��u%� �q�EUE����*6�'�1��eS����v�4��v?]�5ψ&q�T�|�<MJ5D_����"�O���nsx��S:5j�@߄/�&�]���Kbu���N�|�&�K��� �B~d4ȍ�(Ք�Q��ƽ�.]©/�*���%awI+��G�g��˥������|>��@B��      �      x�l�[��J�%�}k.	�ԋ�D� �?����l�hxi���[d��m�������>�m�������sۿ��<>�{?����������w�w���"�0�1�oO��xx�?����v#ppL`#�1�3��؎� ����:ދ،���Q��ަ��Y��ަ��U��ަ��]��ަ��(�ǟ���=3�M���S�M��V�9ߦ�}'�1��t�ߋ0��t�?�0��t�?�0��t��s��>�A��}:��E��}:ߏ"��>����mE���=��s������~��� ����}>p���x�/�����>��+����{�ܕh���̽�g�J���|���Cw%��}>u�㩻��>����]�v~����x�>��������{�ݕ0��t�{�.�fη����]	s�M���J��m:�=wWo��ﹻ�|=~qo��ﹻ�|���ݕ0��t��E��}:��E��}:�$vs�O���s�O���|���ws�O�㹻�|���sw!�����?E���{�aΗ��}+����z,�9��&��Y����6�;�{;̷��z,D;̷�C��v��o������1�y���c��6����|<������ŏh��6x\o��h��6x\o��h��6x\o��h��6x\o�I<��6����B��m:o��0��t>���>s�M��m��|����`!��z�aη�|�o��x,�9ߦ��6Xs�O��mp���4��t>�a���|����x,�9ߧ��6Xs�O��m��|_��"��>����v���9������$���6x���'aί��s���v������M��9����پ��9����پ��9����پ݆�W;�g��z�/D;�g��z�/D;�g��z�/D;�g��z�/D;�g��z�/D;�g��z�O�mη���l���A��m:�=�Wo���پ�|�����0��t�{���9_��"��6����0��t>nX>�m�|����B��}:7,��{��|�?���0��t>nX����*����.�����~?��v��{�g�s<�W����پ�|y��~�_	s~=�_�m���z����`!��9����J���|����~|�H?������u]�/D;ͷ��i_�v��o��uվ��5���}!��k�^�e�B���|�����h��6x]����_�m�o�I���5���6X	s�M�����5���m��|��7:��=�k�^��`!��6��������O�|����`!��6����B��m:o�I�=�k�^��`!��>����B��}:o��i�|����`!��>����B��}:o��0��r�a���|�_o���l_s~��׳�u��L���5���پ��=����پ��=����پ��=����پ��=����پ��=����پ��=������|�g��ý��}=������}=������}=�'!�p��l_���0��t>��aη�|<�o��x�?��0��t>��aη�|<���O�|��ǳ}!��6��g�B��m:��I�=�{>����R��I-�p��l�K��9ߧ���R'a���|<����x�/�9ߧ��l_s�/�O�|��ǳ�����G����>~�_s~=ۏ�m0	��{�g�q�_����`!��1���6X�v~̷�q����mp\o���?���mp\o��h��|��`!��1���6X�v~̷�q����mp\o�I�=�1���6X�v~̷�q��?�~�9ߦ��6Xs�M��m��|����`!��6����B��m:o��0���S�9ߦ��68��G��m:o�I�=�1���6Xs�O��m��|����`!��>����B��}:o��0��t>���������mp\o��0��r�a���|����xLB���6�\o��0����s�_o���6ض� ��g�>��`!��g�>��`!��g�>��`!��g�>��`!��g�>��`!��g�>��`!��g�>���6�%�.�p��6�\o��h��6�\o��h��6�\o��0��t>�aη�|�o��x,�9ߦ���`�|��0��t�{��9_��"��6����x�=�g�>�m��|��o��0��t�{��9ߧ���`��߃0��t�{��9ߧ���`%��>���"��>��G�|_��"��>��o$��s���x���q�9o��؋0��mp�"����:>�(��~���,����Q�㫈r��G���"����:���>�r��G���"���e����{�?�ElE��?�E�E��?�u�aη�|{aη���l�߯� ��6����+aη�|<�o��x�/�9_��"��6��g�$��ߏr[�|��ǳ�}ϒ�����\�{�|��ǳ}!��>��g�B��}:���0��t>��a���|<�B3�>���<��c��[��}����O ,-�����b� 4XB��Z��\�u쓄,�}�Ѐ�Of��OrױO-!�IB����~,����'	���$��b�$4`A쓄,�}�Ѐ�O朱�k	XrױO-!�IB��q��>Ih���'	���$�b�$4`A��Z�rױO6���'�vޱOrױO-!�IB�%�>Ih����,����'	X�$�b�$4`A쓄FK�}�0�}����}^K����}��h	�O-!�IB�>Ih���'	X�$�b���<��c�$�9c� ��c�$4ZB쓄FK�}��h	�O朱O<K��c�$4`A쓄,�}�Ѐ�O��c��x�\�>Ih����\�S.�:�IB�%�>Ih���'	X�$�b�$4`A쓄,�}^K����}����}��hi9~��h	�O-!�IB�%�>Ih����,Ͼ���'	X�$�b�$�9c� d�c�$4ZZ��"4ZB��Z���u쓄FK�}�0�}�Ѐ�O� �IB�>I��}^K��A��}��hi9~��h	�O-!�IB�%�>Ih���'	X�����)�p�$�b�$�9c� �
�c�$4ZZ��"4ZB쓄FK�}^K��k��}�Ѐ�O� �IB�>Ih���'	s��'�����,O����'	���$��b�$4ZB쓄9g쓄9g쓄,�}^Kb�{��}�h��!�p�$��b�$4ZZ��"4ZB쓄FK�}��h	�O� �IB�>Ih���'	s��'����'	�������)�p�$��b�$4ZB쓄FK�}�Ѐ�O� �IB�>�%`y�=\�>A�=\�>Ih���'	�������$��b�$4ZB��Z����u쓄,�}�Ѐ�O��c� ��c�$4ZB쓄FK����
X�rױO-!�IB�%�>Ih���'	X�$�b�$�9c������c�$4ZB쓄FK��S�FK�}��h	�O-!�IB�>�%`y�=\�>Ih���'	s��'����'	���$���r���b������c�$4ZB쓄9g쓄9g쓄,�}�h�G�>A�=�Q��k	X^rwT쓄FK�}��hi9~��h	�O-!�IB�%�>Ih����,/����'	s��'����'	���$��b�$4ZZ��"4ZB��Z����u쓄FK�}�Ѐ�O� �I3�	B��:�IB�%�>�%`y�=\�>Ih��?Eh���'	���$��b�$�9c�$�9c������c� ��c�$4`A쓄�3�IB�%�>Ih��?Eh����,/����'	���$�b�$�9c� ��c�$4`A쓄FK�}^K��{��}��hi9~��h	�O-!�IB�%�>Ih���'	s���,/����'	X�$��b�$4ZB쓄FK��S�FK�}��h	��k	X^rױO朱O�\b� �Nb�$4`A쓄��}��h	��k	X^}'�O-!�IB�%�>Ih���'	X�$a����p�������p�$��b�$4ZB쓄FK��S�FK�}��h	�O-!�y-˫��$�I3�Y�w��I쓄9g쓄FK�}�    �h	�O��2ڝ���o"�Q�^�� G�x���	����^��!�1 ��Xn��縀��f@����ɜ���sw#r�����9���s�C� �7��A��m
��	�?�� �8�Γ�M9��A��-��A�s��0�:a�1tc��K<�u0?c��9��9s��s�C�I��9��A�s��0�:?����9��9�v^C� �y��h�5t����9�v^C� �y���6�������9�v^C� �y��h�5tc��9��9s��󓸍?��s��s�C� ��6����4�:a�1to9tc���8^�0�:a�1tc��9��9s��s�C�I��9�������2�:a�1tc�D;��s����A��:��k��|RF��~��:��k�<�w;��s����A��:��k��9����{�s�C� �9��A�s��0�:aη���l_�Üc��9�������s���9s��s�C� �9��A�s��0�:a�1t~����Üc�<��9��9s��s�C� �9��A�s��h�5t~���?����A��:��k�D;��� �[;��s����A��:?���P�qk�5t����9s��s�C� �9��A�s��0�:�#Ƴ�k�:a�1tc��9�r��9��9s��s�C�'q~��G�s��0�:a�1t������9s��s�C� �9��O�>ҶC��j�D;��s����A��:��k�D;���$�����n�^Ç����9�v^C� �y��h�5tc��9��9s������·����9s�rWC� �9��A�s��0�[��0�:?���<����A�s��0�:a�1tc�<	����s�C���>����A�s��0�:a�1t��:����h��ο����h��Γ�{��C� ����s����9�v~p�D;?8t�����I��+��C� �9��A�s��0�:OB��j��9��9s���8/�~�9��9s���9s��s�C� �9��A�s��0�:?���ם����9s��s�C� �9��A�s��0�:a�1t~g{�#�y��h�5t����yrWC� �y��h�5t�����qOj����s����A��:a�1tc��9��y�����8?��#��z<�0�:a�1tc��9�r��9��9s���8���s��s�C�I�=\��0�:a�1tc��9���Il�>s��s�C� �y��(�=t����y}�C� �y����O����9�r�C� �y��(�=t����9s��s�C�'q�r��=\�'��p=t�ǣs��s�C� �9��A��-��_���k�����A�s��0�:a�1t�D����9s��s�|_&���#?����پ�|y��~�_	s~=۷�6X	s~=۷�6X	s>~�?�{�|�o�m�V��o�m����~^��#��ߏr_E��m���6X�v�ͷ�6�+�η�6���`%��6��x�D;���`o��h��|l�m���`?��o�m����J��m:��Vo���m�}��G���"��6���+a���Q�9ߦ���`�!��ߏ2�c�!��6�����0��t>�a���|����x,�9ߧ��6����u�s��G��:�	����`��}�`	�O ,!�	@�%�>h����,����'��#�c� ��c�$4`A쓄FK�}��hi9Eh����,����'	���$��b�$4`A쓄9g��\�u쓄,�}nK����}��hi9Eh���'	���$��b�$4ZB쓄9g�s[��\�u��M.�:�IB�>Ih���'	lg쓄FK��(B�%�>��|	���$��b�$4ZB�D;��'����'	X�$�b��X��FK�}��hi9Eh���'	���$��b�$4ZB쓄9g�s��q�A��}�Ѐ�O� �IB�%�>Ih���"4ZB쓄FK�}n3`�-!�IB�%�>I�s�>A� \�>Ih���'	X�$a��}n3`�--ǣ���$��b�$4ZB쓄FK�}�h��!�p��f��%4`A쓄,�}��h	�O--ǣ���$��b�$4ZB�s�˗�h	�O朱O}'�O� �IB�>Ih���'	������X��FK�}��h	�O-!�IB�%�>I�s�>A� \�>Ih����6�/�b�$4`A쓄�3�IB���x��b�$4ZB쓄FK�}n3`���c� ��c�$4`A쓄,�}�Ѐ�O-!�IB����s���%rױO-!�IB�%�>I���OrױO� �IB�>��|	X�$��b�$4ZZ�G-!�IB�%�>Ih���'	s���6��&�p�$�b�$4`A쓄,�}���v�>Ih���"4ZB�s�˗�h	�O-!�I��w����u쓄,�}�Ѐ�O� ��̀�Kh���'	����Q�FK�}��h	�O-!�Io�}��{��}n3`�� �IB�>Ih���'	���$���r<��h	�O-!��̀�Kh���'	s��'����'	s��'	X�$�b�$4`A�s�˗��v�>Ih���"4ZB쓄FK�}�h�G�>A�=�Q�O� ��̀�Kh���'	X�$�b�$4ZB쓄FK��(B�%�>Ih����6�/a��!�pG�>Ih���'	X�$�b�$4`A쓄FK�}n3`�--ǣ���$��b�$�9c� ��c�$�9c�$�9c��X��,�}�Ѐ�O� �I���$���r<��h	�O��c��X�M��:�IB�%�>Ih���'	X�$�b�$4`A쓄FK�}n3`�--ǣ���$a��!�p�$��b�$4`A쓄,�}n3`�� �IB�>Ih���'	����Q�FK�}�0�}��{��}n3`�-!�I3�IB�>Ih���'	X�$a��}��hi9��o��}'�O�\b� �Nb�$4ZB쓄,�}�Ѐ�O� ��̀�Kh���'	���$���r<��h	�O朱�J�}'�O-!��̀�Kh���'	X�$�b�$4`A쓄FK�}��hi9Eh����6�/a����p�$��b�$�9c�$�9c�$4`A쓄,�}n3`�� �I�b�>Ih���"�y�>AH�ڱO-!�IB�%�>��|	X�$�b�$4`A쓄,�}��h	�O--ǣs���6�c� �c�$4ZB쓄FK�}�0�}�0�}�0�}�Ќ%c����c��.�p�$�{w2�I�i��' ����'�2�I@���}�`)c�˗Ѐ%c�d�B��}@h�����2�a���AB.�*��W��%4Z���FK���h)c�d�B��}@h���X2��W��%4Z���9G쓄\�U�B���}@h�����2�a������rW�X2��K�> 4`��D;��'	�����FK�|���rW���2���R�> 4`���,��Ѐ%c�d�sW���\�>I�\�> 4Z���FK���h)c-e�B��}��1X�� \�> 4`���,��0�}��A��}@h�����2��#F��� \�> 4Z���9G�B��}@h���X2���+�9��W�r�A��}@h�����2���R�> 4Z���,��Ѐ%c�����"4`���,��0�}��A��}@h�����2���R�>���� \�> 4`���,��Ѐ%c�d�#�IB�*�9�+`�� \�> 4Z���FK���h)c��s���,�����e�b��b�$d�b--ǣ��2���R�> 4Z���$���.�p��Ѐ%c�d�B��}@�s�>I�%\�> 4ZZ�?�W�r�k��}@h�����2���R�> 4`���,��Ѐ%c�������U쓄��U�B���x��R    �> 4Z���FK���h)c�������U�B��}@h����v^�OrW�����Q�FK��ݪ���.�p���h)c-e�B��}@h���X2�a��������b--ǣ��2���R�> 4Z���FK��Ѐ%c�������U�B��}@�s�>I�=\�> 4ZZ�G-e�B���}��1u�{��}@h���s���9G�B��}@��Orw0��3`��=����FK��(B���}@h�����2���R�> 4`���$���.�pc��O��;���h)c--ǣ��2���R�>'q,��;���h)c�d�B��}@�s�>I�=\�> 4Z���$���!�p���h)c-e�B���}@h���s���9G�sW��{��}��{��}@h���ݺ������Q�FK���h)c�?b,�����FK��Ѐ%c��OrW�X2���R�>'q,�����FK���h)c-e�B���}@h���s���$���!�p��Ѐ%c-e�B���x��R�> 4Z���FK����<��b���rޱO}ױX2��[w2����r�9���lf���FK���h)c-e�B��}@�s�>I�=\�>'q,��P���2����r<��h)c-e�B���}@h�����X��lf���9G�D��u�B��}@h�������Q�FK����<��c-e�B���}@�s�> �9b�$��c�d��G�����p��Э;���hi9Eh�����2���R�> 4Z���$���!Aj�>IH�Z�X2��K�> 4Z���FK��(B���}N�
X�V�B���}@h���s��'�����0�}@�|{��{�� t�=��h�v 2H@{��xМ!Á�f�n ��#���8���\�U6 �bd L6�:؞� l�l �n��l ���|�b������(Bc��@�������@h̐� l�l ��g6�9�Yv>t3�3d6 Bc��@h̰�"4f�l �9G6��\�U6�9��{{Bc��@�`{f t�=�:؞� �2 �1Cf 4fX����qɧ\�U6 #HB.�* �1Cf �9�:؞� l�l��������@h̐� ���Q����h�$�p��И!���x��fd t�=�:؞� �2 �1Cf 4fX�G3d6p����X73 HB�* �1Cf t�=�:؞� l�l �����m3d6 Bc��x�1Cf �9��$d��3d6 #�{>�{P�* ��������@�6��@h̐� ���QD;�l�$c�ᩛ��И!�3d6 B�3 ��������@h̐����'�nf@6 Bc��xaΑ$����И!�3d6 B�38���"t�=�:؞� �2 �1Cf 4fX�G��@/��l��5�=�2 �1Cf �9�:؞� l�l �n��l �����6r��nf@6 ��W6��nf@6 Bc��@h̐� l�l ��g6�G��A�`{f 4f�l ����Иa9E�sdI�fd 4f�l�$�e�?Bc��@�`{f t�=�:؞� �2 �1Cf 4fX������9G6����U6 Bc��@h̐� s�l �9G6 B�38��>t3���#�3d6 �������@h̐� �28���}�fd t�=�:؞� l�l ����И!��|=������{��� �2 �1Cf 4f�l ��g6 B�3 �����k�[�K��* �1Cf �9��$���3d6 Bc��@h̐��I<Ǹ�K��* aΑ�����@�`{f tGf �yeI�fd��"4f�l ����И!�3d6 B�3 ��������>�N��nf@6 #HB73�ǣ�2 �1Cf 4f�l ����������������@�`{f 4f�l �9G6����U6 Bc��Nb�~C�{��@h̐� �2 aΑ�����@�`{f tGf����7d�̀l ��nf@6 Bc��@h̐� �2 �1Cf t�=��?b���������@h̐� s�l 	�̰�"4f�l ���|Ν�� 4f�l ��������@�`{f t�=�3d6 #8���=�߲���3d6 Bc��@h̐� �2 aΑ�0�������g6 B�3 ��+HB73  �1�r<�И!�3d6p��u3�3d6 B�3 ��������@�sdI�=\g����Ah̰�"4f�l ����И!�3d6 # aΑ��{�� s�O��m0������\�w3,ǣs�O��m���e:o���,��~=۟��Zo	R��6دg�B���m�_���h��|��׳}!��>����l_�v��g�~=�'!A�>����l_�v��g�~=�#m{K���g�~=�����پ_���h��|��׳}!��>����l_�v��g�~=�o��x�/�f,�}������c��[��}Ѝ�}�`i94XB��K�}�`	�϶,o����'	K��$a��$Ѷ;�	B730�IB�>I�6�>������O-!�IB�%�>Ih���'	���$�b�$�9c��X~��o����'	X�$��b�$4ZZ�G-!�IB�%�>Ih���g;�� 4ZB쓄,�}�0�}V�A��}�Ѐ�O-!�IB�������=�$���'	���$��b�$4ZB쓄9g�D;��'�����$�� 4`A쓄n�@쓄FK��(B�%�>Ih���'	���$��b�?b�,�}�0�}����}�Ѐ�O-!�IB���x��b�m	X����'	���$��b�$4`A쓄9g���u쓄,�}Nb���!�p�$���r<��h	�O-!�IB�%�>Ih���'	s���|���C�:�	B730�IB�>Ih���'	�Ɓ�'	����Q�FK�}�%`9t3c�$4ZB쓄FK�}�h���p�$�b�$4`A�s�xR�f�>Ih���"4ZB쓄FK�}��h	�O-!�I3��΀e2ױO� �IB�>Ih���'	����Q�FK�}��h	�϶,��u쓄FK�}�0�}����}�Ѐ�O� �IB�q ��΀e<�u3c�$4ZB쓄FK�}��h	�O-!�I��w��nf`�s�xR�f�>Ih���'	���$���r<��h	�O-!�IB�%�>��rױO朱OrױO� �IB�>Ih���'	���㸹������'	���$��b�$4ZB쓄9g�����'	X�lg���,�}�Ѐ�O���O--ǣ���$��b�$4ZB쳝�1�v~T��nf`쓄,�}�Ѐ�O� �IB�%�>Ih����Y���b�$4ZB쓄FK�}�0�b� ��c�$4`A쓄,�}�%`��=\�>Ih���'	����Q�FK�}��h	�O-!�I3��΀�w���{��}�0�}�Ѐ�O� �IB�>I�6�>Ih�����_+�-!�IB�%�>I��}����}�Ѐ�O� �IB�>��|t3c�$4ZB쓄FK��(B�%�>Ih���'	s��'�����K�ǧ�>rױO� �IB�>Ih���'	���$���r<��h	�϶,����'	s��'����'	s��'	X�$�b�$4`A쳝��Y730�IB���x��b�$4ZB�D9��'����'	X���6�Ժ���O� �IB�>Ih���'	����Q�FK�}��h	��v,�A�s�>A�f��}�Ѐ�O� �IB�>Ih���'	���l�N����$��b�$4ZB쓄9g�D��I쓄9g쓄9g�-�G730�IB�>Ih���'	��    ��'	����Q�FK�}�h���S��O��M730�IB�%�>Ih���'	X�$�b�$4`A쓄FK�}�3`����r<��h	�O朱O�v쓄FK�}�Ѐ�O� ��f��I�ڱO� �IB�%�>Ih���"4ZB쓄9g����lg���FK�}�0�}�Ѐ�O� �IB�>IX���'	�������<�Kh���'�vޱO�v쓄FK�}��h��O朱O��d�s������5\�>	hŒ�O�w'c�4XZ���Ӽb� ��b�4X���F��%�X���%K�}@�l�> 4`���,��Ѐ%c�u'c�/qm��m�b�$��b-e�B���}@h���X2��K�>_��V�%4`���,���h)c�|=$��b-e�B���}��3׶�/��R�> 4`���,��Ѐ%c�d�B���}@�s�>'1��|n2W���2���R�> 4Z���9G�B��}@h������V�%4`���n���D;��'	����FK���h)c-e�������,��Ѐ%c�d�B���}@���x��A��}Nb,_B���}@h�����2��K�> 4`���,��Ѐ%c�?�6�$d�b��O��U�B���}@h�����2�a�����j��9G�B��}@h���X2���+�IB�*���R�>'1��&�p���h)c-e�B��}@h���X2��K�>�o[͗0�}��A��}@h�����2���R�> 4Z���,����V�%4`���,��Ѐ%c��O2W���2���R�>'1�/��R�> 4Z���9G�B��}@h���X2���+�9�����5\�> 4Z���FK���h)c-e�B��}@h������V�%4`���,��0�}��{��}@h�����2���R�>'1�/��R�> 4`���,��Ѐ%c�d�#�IB��*���U���}	��2���R�> 4Z���FK��0�}@�s�> 4`���$ƶ�/�K�> ����'	��;���hi9Eh�����2���R�>'1��|	��2��K�> 4`���,��0�}��{����������V�%4Z���FK���h)c-e�B��}@h���X2�9��|	s��'	�����FK��(B���}@h�����2���R�>'1��|6�����9G�B��}@��}����b�u'c--��%c[͗�h)c-e�B���}@h���X2��K�> �9b���g�{��}@h�������Q�FK���h)c-e�B���}Nbl���d�B��}@�s�>I�=\�> 4Z���FK��(B���}Nb,_B���}@h�����2�a���Ѐ%c�c�$d�b���j��n����FK��(B���}@h�����2���R�> 4`����ƓZ63T�#�IB63T�B���}@h���"4Z���FK�|�k[͗�h)c-e�B��}@h���s��'������FK����V�%4ZZ�G-e�B���}@h�����2�a���0�}��_��%�y�>I�f��}@h���ݺ������Q�FK���h)c���j��FK���h)c�d�#�IB�Ԋ}@h�����2�9����Kh���"4Z���FK���h)c-e�B��}@�s�>�Xv	R+��K�> 4Z���FK��(B���}@h�����2�9����Kh���s��D;��'		R+��K�> t�N�> 4ZZ�?�c[͗�h)c-e�B���}@h�������0�}��/�pK�����팱��r� ���n Vrp7�9���X��� ���e �q�6 �8���V�8��6�$>F`G���6�$�6�q$a���#	��mI�pn�8��3�mI�rn�HsG��8�h罍#����6�$�yo�x��}�h罍#�v��8�h罍#�v��8�h罍#�v��8�h罍�K�^�A�sn��nι�#	s�mI�sn�Ho��ﹾ��8�0����%�?��ws�mI��-�A�sn�HsGs�mI�sn�HsǗ���<�%�9�q$aι�#	s�mI�sn�HsG��8�0������qi����{G���q$��{G���q$��{G���q$��{�Il�W�g;�mI���ƑD;�mA��9�q$aι�#	s�mI�sn�8�k]�9ߦ��6Xs�mI�sn�HsG�|�}��8�x�sn�g�ۜsG��8�0��Ƒ�9�6�$�9�q$aι�#	s�m�{�u��8�0������{G���q$��{G���q$��{��o�A���ƑD;�mI���ƑD;�mI�����ǜsG��8��{��?��8�0��Ƒ�9ߦ��l_s�mI�s�0��Ƒ�9�r@������cι�c%r��8�0��Ƒ�9�6�$�9�q$aι�#	s�m'��aι�#	s�mI�sn�HsGr��8�h�Gm�H�����K<_�A��q$���h��A��� ���}����>�v~p@�����v~�=��}��8�0��Ƒ�9�6�$�9�q$aΗY��6Xs�m_���V�%��ro��0��Ƒ�9�r@?	���mI�sn�HsG��8Nb|��!�p��#	s�mI�sn�HsG��8�0�������6�/q^��s�mI���ƑD;�mI���ƑD;�mI���ƑD;�m_b?ƓZ��zG���q!�p��#�v��8�0��Ƒ�9�6�$�9�q���1s�mI�s�0��Ƒ�9_��m���8��{��Ƒ�9�6�/q�|?sG��8�0��Ƒ�9�6�$�9�q$aι�#	s�m'�_�9�6� ���q$aι�#�r.�8�(粍#�r.�8�(粍�K�����\�q$Q�eG�\�q$Q�eG}'�8�(粍#	s�m'1�,}'�8�0��Ƒ�9�6�$�9�A�sn�H/��V�9�6��y.��G�=�l�HsG��8�0��Ƒ�9�6�$�9�q$aι��$n�ާ��dG��8�0��ƱϾ��mI�sn�HsG���q|�c|����p��#�v��8�h罍#�v��8�h罍#�v��8��{8���%��׏h罍#�v��8�0��Ƒ�9�6�$�9�q$aι�#	s��?b|B���p��#	s�o���{8�Ƒ�9�r@�9�6�$�9�q$aι��K��� �9�q$aι�#	s�mI�sn�HsG}'�8�0���q�E�sn�H���6�$�yo�H���6�$�yo�H���6�$�yo�x.?�}'�8��{8�ƑD;�mI���Ƒ�9�6�$��6��g�B��m:����#��6��g�B��98�0��t>��aΗ�x�O�����(�aη�|<����x�?F��{�?�u|a���|<����x�/�9ߧ��l_s�O��پ�|��ǳ}!��>���~�S�?�����(�a��=��[��m���߳��a���������G���"�F���,B3�>ߟ|��S��:�	���>�%\�>+ wp����<�=F�`	�O ,!��/�S�:�IB�>Ih���'	X�$��b�$�7c� d�c��|_�FK�x���b�$4ZB쓄9g쓄,�}�Ѐ�O� ��/�S�:�I��w���u쓄FK�݊�h	�O-!�IB�>�%`y���>Ih���'	X�$��b�$�9c��x� \�>Ih�4��'�X^2ױO-!�IB�>Ih���'	X�$�b�$4ZB�s_�_2ױO2ױO-���پ-!�IB�%�>I�s�>I�s�>�%`y� \�>Ih���'	X�$��;�	B�:�IB�%�>Ih�4����X^2ױO-!�IB�>Ih���'	X�$�b�$�9c����d�c�$4ZB쓄FK�8���b�$4ZB쓄,�}�Ѐ�O<Kd�c�$    4`A쓄9g���u쓄FK�}��hi�m �p�ܗ��%�p�$a��$�b�$4`A쓄,�}�h���p�ܗ��%�p�$���<n�"4ZB쓄FK�}�Ѐ�O� �IB�>�%`y� \�>I�s�>A� \�>Ih���'	���q<�B�%�>Ih����,/���'	X�$�b�$4`A쓄9g��\�u쓄FK�}�K��k��}��h	�O-!�I3�I3�IB�>Ih����,/��;*�	B�ᎊ}��h	�O-!�IB��yo���h	�O-!��/�K�ᎊ}�Ѐ�O� �I3�	B�ᎊ}��h	�O-!��/�K�ᎊ}��h	�O-!�IB�>Ih���'	X�$a��ܗ��-�p�$��b�$4ZB쓄FK�8���b�$4ZB쓄9g�s_����u쓄,�}�h��!�p�$a��}��h	�O-!��/�[��:�IB�%�>Ih���'	X�$�b�$�9c� ��c������c�$4ZB쓄FK�}��hi��`!4ZB쓄FK�}�Ѐ��}	X�rױO朱OrױO-!�IB�%�>Ih���'	���q����-�p�$��b�$�9c�$4`A�D9��'���'	lg�s_���u쓄FK�}��hi�ᝄFK�}��h	�O� �IB�>�%`y� \�>A�=��>Ih���'	���$��b�$4Z���V�FK�}�K��A��}�Ѐ�O� �I3�	B�:�IB�%�>Ih����,o���'	���qL�,�FK�}��h	�O朱O朱O��c�����Nb�$4`A쓄�3�IB�%�>Ih���'	�����6X	���ܗ��-Aj�>Ih���'	s��'	R;�IB�>Ih���'	���ܗ��-Aj�>Ih�4����Jh���'	���$�b�$�9c� $H���,o	R;�IB�%�>Ih���'	���$���<���Bh���'	���ܗ��-Aj�>I��}V� �c�$4`A쓄�3�IB�%�>Ih����,���$��b�$4ZB쓄,�}�0�}�� �c�$4`A�s_�C�Ԏ}��h	�O-!�IB��y����h	�O-!�IB�>�%`9�Nb� $H��'	X�$��b�$4ZB쓄FK�}��hiǳ�
X	R;�IB�%�>I�s�>I�s�>A�Nc�$6#�$�b�����'�C��:�	@�� �	@�� �	@�%�>h�4��� 4XB�@_�v�/�!�p�$�b�$4`A쓄,�}��h	�O-!�IB�%�>�������<�oEh���'	s��'�����'	X�$�b�$4`A�/�!�p�$��b�$4ZB쓄FK�݊�h	�O��c� d�c�}	X���'	X�$�b�$4ZB쓄FK�}��h	�O-����>�C�:�Io�}��A��}�Ѐ�O� �IB�>Ih���g_����'	���$���<���+��b�$�9c� d�c�$�9c�}	X���'	X�$�b�$t�b�$4ZB쓄FK�}��hio��2ױO2ױO� �IB�>Ih���'	X�$��b�}	X���'	���$���<���B�s�>+�A��}�Ѐ�O� �Y�%���'	X�$��b�$4ZB쓄FK�}��hi�m�A��}�%`�� \�>I�s�>Ih���'	X�$�b�$t�b�$4ZB�/˧/�$�IB��y�nE��}��A��}�Ѐ�O� �IB�>��|d�c�$4ZB쓄FK�}��h	�O-��x�/�9g���u�/�G�:�IB�>Ih���'	X�$��b�$4ZB쓄FK�}�%`��5\�>I�s�>A�=\�>I�s�>I�s�>Ih���'	X��K��{��b�$t�b�$4ZB쓄FK�}�h�G�>A�=�Q�O-!�ٗ��#�pG�>Ih���'	X�$�b�$4ZB쓄FK�}��h	�Ͼ,��;*�	B�ᎊ}��h	�O� �IB�>Ih���'	X��K��{��}��h	�O-!�I3�	B��:�IB�%�>I�s�>��|��c�$4`A쓄,�}�Ѐ�O�u�O-!�I��w�/�G��:�IB�%�>Ih���'	X�$�b�$4`A쓄,�}�%`��=\�>Ih���'	s��'����'	���$��b�$4`A�/�G��:�IB�>Ih���'	���$��b�$�9c�I|�rױ�~,'��b�$4ZB쓄9g쓄,�}�Ѐ�O� �IB�� �ٯ��$4ZB�D9��'���'	���$��b�$4`A쓄,�}�+`9	X�$�b�$4ZB쓄FK�}�0�}��A��}��h	��~,'��b�$4`A쓄,�}�Ѐ�O� �IB�%�>Ih���g���0�}����$��b�$4ZB쓄9g쓄9g쓄,�}�+`9	X�$�b�$t�b�$�y�>AH�ڱO-���m�-!�ٯ��$4ZB쓄,�}�Ѐ�O� �IB�>Ih���'	s��g������9c�$4Z����`%4ZB쓄FK�}�Ѐ�O� �IB�>����,�}��h	�O朱O�v쓄FK�8���b�$4ZB�_�I�s�>Ih���'	X�$�b�$t�b�$�y�>AH�ڱ�~,'���<�g�Bh���'	���$�b�$4`A쓄,�}�Ѐ��~,'��b�$�9c� $H��'	���q<�B�%�>Ih���'	X��W�r� �IB�>Ih���'	���$a��!Aj�>Ih�4����XNB�%�>Ih���'	s��'	s��'	8��'���}��ΗQ���?�����n��[��r�~��n���|N'��#�	���}w0p��À��O�i@�۟����ˈ��6"��A�l�ۃ0��Ob3��Խߊ0��a·����o�?c��)Ǹ=s�q{��� �9��A�s�ۃ0����?�q%����� �9��A�s�ۃ0��a�1n��׸=�v^��'q�P?��׸=�v^�� �y��'qo�5n��׸=�v^�� �y�������h�5nc��9Ǹ=s�q{��� �9��x�s�۟�m\�ls�M翷�J�s�ۃ0�[�ۃ0��a�1nc��9Ǹ�q�a�1nc�>��9Ǹ=s�q{��� �9��A�s��������k�D;�q{����A�����k�>�W;�q{�����'�9��#�y�ۃh�5n��׸=s�q{��� �9��A�s����?a�1n��ۜc��9ߦ��V�9Ǹ=s��=s�q{����>��{�s�ۃ0��a�1nc�>�Üc��9Ǹ=s�q��8?�#�9��A�s�ۃ0�����ۃh���A���� ���q�����g����ۃh���A���� ���q{���=�v~p��9Ǹ�I��Q�9Ǹ=s�q{���A�rwp��9Ǹ=s�M��m�����x��rwp��9Ǹ=s�q{��� �9��A�s��'!�p5n��}��� �9��A�s�ۃ0��a�1nc�D;�q���?��rW�� �y��'!�p5n��׸=�v^�� �y�ۃh�5n�w��>����A���a�1nc��9Ǹ}rW�� ��z�9���λ��ո=s�q{��� ����� �9��A�s�ۃ0��?���5�9Ǹ}rW�� �9��A�s�ۃ0��a�1nc����8��=\�ۃ(�=n����=�r���I�=\�ۃ(�=n�����������(�=n����=�r��� �y�ۃ0��a�1nc��$�1H��=\�ۃ0���(c��9Ǹ=s�q{�|�q{���'�~�������A�s�ۃ0��O���z��9Ǹ=s�q{���'����{��a�1nc��9Ǹ=�v^�� �y��'��p=n��E�����k�D;�q{����A�����k�D;�q�?�6��}��� �9���{��a�1n�ǣs�q{���'�������A��-��A�    s�ۃ0��a�1nĽ��z��9Ǹ�Iܟ�A�s�ۃ0��a�1nc��9Ǹ=s�q{����O��G���k�>����q{����A�����k�D;�q{��������ǽ��z�D;�q{��� �9���{��a�1n�ǟ������ �9��A�s�ۃ0�[�ۃ0��a�1nc��yNM�`���p=nc��9Ǹ=s�q{��� �9��A�s�۟����a�1nc��۸}���q{��y|���ww#��ￒs�ȏx��OO#fs�{��ˈc[o#�%�^�!�6�+��E��m:�$x7~���s~����|��o��0��t�{��9_�G�|��o��0�Kg�{��9ߦ���`%��6������>s�O�c�!��>����I�n�(��Y�9ߧ��6Xs�O��m��|_�G6�����c��!�p�a# +�`# �_# ��^#�z��v��I�^��,ح��8�o�Ѐ�5��d	�5��fi9Eh���Ih������[#	���[���b��w���A� \��HB��HB��HB�%��HB���x��vk��|�_�FK���h	�5��h	�5�0�ܭ���n�$4`�n�$4`�n�s\r|��.�p�[#	l�n�$4ZZ�G-a�F-e�B�%��H���n�3`y��h~� \��HB��HB��HB��HB�%��HB���x��vk��vB���}@h���I��m:�w2׻5�Ѐ�5�Ѐ�5�Ѐ�5N���FKح��FK��(B�%��HB���}@h���I�s��B�z���������n�$4`�n�$4`�n�$l���5��hi9Eh���Ih���������!�p�[#�v޻5��A�ޭ��,ح��,ح��,ح��FKحq�Eh���"4Z�n�$4Z���FKح��9ߦ�}'!�p�[#	X�[��-�B��HB��HB�%��HB���x��vk$��R�> 4Z�n�3`y�aι[#^���"�"�9wk$�vk$�vk$�vk$a��ܭq,�xRK�z0���vk$��R�> ����'		R�> 4`�n�$4`�n���~�� �`�B��HB�%��HB���x��vk$��R�> �9wk��c<����Ih���Ih���Ih���Ih���Ih���Ih���#x�=\��HB���}@�s��B��z�F朻5�0�ܭ��,حq,c��!�p�[#	X�[#	l�n�$4ZZ�G-a�F��wk!�p�[�U�%����Ih���Ih���Ih���Ih���Ih���Ih���϶�Gh���I�s�>A<��wk$��vk$�vk$�vk$�vk�y�v�,ح��FKح��FK��(B�%��Hs�Fr׻5��h	�5΀�s�F�`�F�`�F�`�F6���Ih���"4Z�n�3`y�Q�e�F2׻5��h	�5�Ѐ�5�Ѐ�5�Ѐ�5�Ѐ�5΀e��2׻5��hi9Eh���I�s��B�z�F-a�F�`����p�)�p�[#	X�[#	X�[#	���[#	����Q�FKح��9�n���~�!?�Nvk$��vk$aι[#	s��Ih���Ih���Ih���gb�l�n�$4ZZ�G��wk��p�[#	���[#	���[#	X�[�$��;���[#	X�[#	X�[#	���[#	����Q�9�n� $H��g�r�%H��Ih���Ih���Ih���Ih���Ih���Ih���g���B���xaι[#	R{�F-a�F-a�F朻5�Ѐ�5���3|H�ڻ5�Ѐ�5���v��HB���x��{�F��n�$4Z�n��9�S��ޭ��,ح��,ح��,ح��,ح��FKح��FK����\��#�9wk!Aj��HB�%��HB�%��HB��HB��HB��8�&���S��ޭ��FKح��FK��(s��J�$H��Ih���Ih���'��0�ܭ��9�n�$4`�n�$4`�n�$4`�����}@|�X���J�����,���R�> 4Z���FK��Ѐ%c�d�B��}N��PƋ�p���h)c�|=$xg���2���R�> 4Z��珸=a���0�}@�s�> �9b-e�#�Ib��[��X������o��k����n@T, �D��a�sO"a�2 
 o"`9�^�� #"`!�"��	��	ӝ	�	�˟��ا�z��XH��XH��XH��XH��X@<�y,$�9��x���h�XH�s,$�9��v΀�D;g�B��3`�#^�/o�v΀ī�3`!a�3`!a�3`!a�3`!a�3`!aη���*x��c�<�<�<�|�����ۜg�Bg�Bg��>��9π��9π��9π��9π��9π��9π��9π变���<��v΀�D;g�B��3`!����h�X�,9�9��v΀ħ�3`!a�3`!a�3`!a�3`�#�i�a�3`!aη�|<�g�Bg�Bg�Bo��x�_��f�3`�#Ή�a�3`!a�3`!a�3`!a�3`!a�3`!a�3`!a�3`��;��"�y,$�y, 6s�	s��v~ `!��,$������_��,$�����D;?��h���@�bo���@��_&z��<�<�<�<�|����`!�y,$�y,��}g�B.�,$�y,$�y,$�y,$�y,$�y,���6s�	s�	s�	s���c�Bg�Bg��w��CQo��c�B��3`!����h�XH�s,$�9��?b����{8,$�9�	s�	s�	s�	s�	s�M�� ��O�<rǀ��9ߦ��=�<�?��.g�Bg�Bg�Bg�Bg�Bg�B����#F���{8,$�y,$�y,$�y,$�y,$�y,$�y,�����}W�r^�r^	s�	s��q�����<�<��|=~�0����{�
XH��m:�����9π��9π��9π��9π��9π��9π��9π司6~����$�������9π��9π��9π�D;g�B��3`�G�O9}W�v΀�D;g�B��3`��p��h�XH�s,��U�����0���0���0���0���0�Y��0��qL�����{�
XH��XH��m:o��0���0���0���0���#FR|�=\,$�y,$�y, ���<�<�<�?b_D;g�B��3`!����h�XH�s,$�9}W�?b|���{�
XH�s,$�9�<�<�<�<�?b{<aγ!a���D��U�Bg�Bo��x�/�9π��9π司���G��U�Bg�Bg�Bg�Bg����*`!a�3`�G�a�3`!a�3`!a�3`!���x����8����$��k���{8	X@�N���؍����݈XH<�Ȁ��9π�1�3������9π��9π��9�j��$`!a�3`!a�3`�#Ηُ0�[,$�y,$�y,$�y,$�y,$�y,$�y,�ya���=��Q��s�O翷�J��}:��V_o��o��0������`%���6��6X	�X�<g�r�8\�>hł�'�z�K� ����' ��㇀K�}�`	��)k,���'	X�$�b�$4`A쓄,�}�0ߌ}��A��}�K��A��}��h	�O-!�IB�%�>I�s�>I�s�>Ih����,���'�vޱO2ױO-!�IB����)B�%�>Ih����Rd�c�$4`A쓄,�}�Ѐ�O朱O2ױO-!�y.�G�:�IB�%�>Ih���'	���$�b�$4`A쓄,�}�K��A��}��A��}��h	�O--�O-!�IB�%    �>Ih����,���'	X�$�b�$�y�>A���>Ih���'	��������'	���$��b�$4`A쓄,�}�Ѐ�O朱�s	X>2ױO-!�IB����)B�%�>Ih���'	���$�b���|d�c�$4`A쓄9g���u쓄FK�}��hi9~��h	��s	X>�v쓄FK�}�0�}�0�}�Ѐ�O����'	R��}�K�� ���'	���$���r���b�$4ZB쓄FK�}�Ѐ��s	X>��$a��!A�Q�O-!�IB�%�>Ih��?Eh����,	R��}��h	�O� �IB�>I�s�>����c�$4ZB��˗�h	�O--�O-!�IB�%�>Ih���'	s��'	s���9�/��;�	B��:�IB�>I�`;c�$4ZB쓄FK��S�FK�}�3��-!�IB�%�>Ih���'	s��'����'	X�$��b�$4ZB쓄FK��S�FK�}��h	�O-!�IB�>I�s�>��l7����'	X�$��b�$4ZB쓄FK��S�FK�}��h	��s,_B�%�>I�s�>I�s�}��A��}�Ѐ�O6���'	���<g��%4ZZ��"4ZB쓄FK�}��h	�O� �I3�	���$�y΀�Kh���'	���$��b�$4ZZ��"4ZB쓄FK�}��h	��s,_B�>I�s�>A� \�>Ih���'	���$��b�$4ZZ�?�#`�-!�IB�%�>Ih���'	s��'	s��'�����'	X�<g��%4`A쓄�3�IB�%�>Ih��?Eh���'	���$��b��X�D;��'	R;�IB�>Ih���'	���$��b�$4ZZ�c��3�v� �c�$4ZB쓄FK�}�0�}�� �c�$4`A쓄,�}�3`�-!�IB�%�>Ih��?Eh���'	���$��b�$�9c��X�M�Ԏ}�Ѐ�O� �I���$��b�$4ZZ��"4ZB��˗�h	�O-!�I��w����$�b�$4`A쓄FK�}�3`�-!�IB����)B�%�>Ih���'	���$a��!Aj�>��|	X�$�b�$4ZB쓄FK�}��hi9~��h	�O-!�y΀e�$H��'	s��'	R;�IB�>Ih���'	X�$a��}�3`�-!�IB����)B�%�>Ih���'���}��=��>Ih����9�/�b�$4`A쓄FK�}��h	�O--�O-!�IB�%�>��|	s�!�	��p�$�b�$4`A쓄,�}��h	�O-!�y΀�Kh��?Eh���'	���$a���{8�}�0�}��-�5�z�f�[��j�Z�&P��z�5�Z��#�	���`M��� k�?��W���'p���x�ը=�
�����*��]� �v���h�5�
�}��*��������f� L9�WA�s��&ѫQ{|�9ߦ��S�9��*s���/����0�[���0�_a�1�
c|�9��*s���$z5j����1���^��� �9�WA�s���0�_a�1�
c|D;?8��%���^��� ����� �^��� ����U����*�v~p|D;?8��Gl�A��� ����U�� �9�WA�s��&�ը�G���"�9�W�� ��6�o�"�9�WA�s���0�[���0�_a�1�
c|�$�/���Ip5��Q��s��U�� �9�WA�s���0�_=���\��0�_a�1�
����*�v^�I��y���h�5�
�������������WA��_��k|D;��U���WA�s���0�_��-���g��a�1�
c|�9��*s�M�ۧs��U�� ����'q~�G�s���0�_a�1���Ӝc|�9��*s��U��'�>�Ӝc|�9��*s��U�� �y���(�=��ī������Q�{|D9��U��WA��_Q�{|D9��U��WO��������U��I�=\���0�_a�1�
c|�9���I<^�I��p=�
o9�
c|�9��*s���$���WA�s����}�L�}�� �9�WA�s���0�_a�1�
c|�9���߭�qD;���$���WA��_��k|D;��U���WA��_�#n�A��_��k|D;��U�|�����"�}�� �9�WA�s����u3q�{�_a�1�
c|�9��*s���*s��U��'q�`������U�� �9�WA�s���0�_a�1�
c|�K�?#��=\���0�_��k|5�����U���WA��_��k|�$�͸�=\���h�5�
����*�v^� �y���0��t�{�/D�����I��~�9��*s��U�|=E�s���0�_a�1�
o9��Gl�A�s���0�_M���z|�9��*s��U�� �9�WO��yc|�9��*s��U�� �9�W��=����،���?b��w����*��9�
�a�r<�x�� ^F��*��9�
�0"�WO�q�yg�I���WA�s���0�_a�1�
�ǣs��Փ��o�0�_a�1�
o9�
c|5	����*s��U��'q���s��U�� �9�WA�s���0�_a�1�
c|�����w����*�v>�6�w|����6�,D;���`�m��|�o����B��}�������6���c��|�o����B��}�������6�L��p�G���"��6����B��m:o��0��t~M����n�(��]�9_�G�|�ηO�|�ί��I��m:��Vo���m0���Ər�?b�����(����,����*����.���(_o��x9�����ݯ��n�(q/����s�>���{��\�g��>�����]�v~����x�D;����}<wW����s�>����o��Íe�ݕh���ܽ���J���|���sw%��}>w�㹻�|�����$x7~���,o��x��}���p�G���"��z<�0��t>��aη�|<wo��x�.�9ߦ���]����O�f��q��b�
�4\�>������>W�&,�Z} Z��c��`;�U�Y�������d)c�,�[�I���V}2�ߪOB|�>	X����<d��U��,�V}-�[�Ih���"4Z���FK�V}�ߪ�����p�G���X��$4`���ЀߪOB|�>	����$4ZZ���?��U�������h	ߪO�[�A� �|�>	s�o�'a����$4`���?g�2~y���o�'��U����[�Ih���"4Z��D;?�$!A�����>��h	ߪOB|�>	X��$4`���ЀߪOB�%|�>	����p����R�> �9�U��G}�>	����$4`���ЀߪOB|��s,� 4`����h	ߪOB���x��R�> �9�U�����$4Z·�?g�2~C� ��U��9��ЀߪOB|�>	X��$l��ߪOB�������3��~�[�+� ����FK�V}-�[�Ih��o�'��U��,�V��[��>X��$4Z·��hi9E�s�>IH��ߪOB�%|�>	������<��U��,�V}��[�Ih��o�'���U��FK��(�[����q{J��ߪOB�%|�>	����$�9�U��,�V}��[�Ih��o՟���)�p���$4ZZ�G�\�U������h	ߪOB�%|�>	X����l�ЀߪOB|�>	X��$4Z·��hi9E�s�>I� \���,O��o�'���U��,�V}��[�Ih��o�'��U��FK�V�g	X�2�ߪO�[�A�=�|�>	����$4Z·�0��V}�ߪ�,˳���[�Ih��o�'��U����[�I���V}�����h	ߪ?��s-�[�Ih��o�'��U��,�V}��[�Ih��o�'���U�9�������� $H�o�'���U��FK�V}    -�[�Ih��o�'��U�Y������$4`����h	ߪO�[�AH��ߪOB�%|�>	������<%H�o�'a����$4`���ЀߪOB|�>	l��h������<%H�o�'���U��FK�V}-�[�Ih��o�'��U��,�V�g	X������h	ߪO���K���V}-�[�Ih��o�'���U�c��[�Ih��o�'��U��,�V}-�[�I�s~�>	R�[��3`�B�%|�>	����$4Z·�0��V}�ߪOB|�>	X����/��[�I|���ꃐA��V��o�rI����3(%SR�.s�sL�JA���.6ԇ�۟�E�%��z�����Y$Z�ߪg�hI�����X����E��z	X���Y$`�ߪgQ3�ߪG����?���F�hI��oqk��"ђ�V=�DK�[�,-�oճH���U�"��V=�,�[�,��o�?7������?���v����?���F�hI��E�%��z�����Y$Z�ߪgQ3�ߪ���ZE����Eb	X�}T̙����í���=�DK��E�%b���ϵ����}T$Z"�Q�h��GEb	X�}T$`!�QQ3���g� �W�ԁ}T$Zڞ�(-��H�D�"����DK�>*����|H�GEb5saR�Q�hi{^�H�D�"���]� �W�ԁ}T$`!�Q����GEbX�}T̙��"@��>w�,_R�Q�h��GE�%b	X�}T$`!�Q����GEb��b�� ��������E�ԁ}T$Z"�Q�h��GE�%bywG�}�7c��u�y��ܒ��}�%�y�A]�A0����>[��p������>�X�cn^�a�dI�}X�Y�e��tهE�%]�a��E�}X$`�e�s,�����"��e��tهE���y�"ђ.��H���>,-�Ϲ��X���}X$`�e	XtهE�ܗ}P�"ܼ��"ђ.��H��=���X�cn^�a�hI�}X$Z�e��tهE�ܗ}X��}هŜ�e��x�ﰨ����,���"����DK��E�%]�a�hI�}X$Z�e�s,�R/c	XtهE�\؇E����GE]�a�hI�}X$Zڞk�k��;@�5.��H���>,-��DK���"�.������
��)����_��;@���"ђ.��H��=�Q$Z�e��tهE�%]�a�hI�}��|H��}X��}�E�ԧ���,���"�.��ȫ;���"�������X��>�e��tهE�%]�a�hI�}X̙��>(���>,��Ϲ�� ���DK���"����F�hI�}X$Z�e��tهE�%]�97�� u^�A u^�a��E�}X$`�e��tهE���y�"ђ.��`��:/��H���>,-����/���E�yهE]�a��E�}��|�"ܼ��"����F�hI�}X$Z�e��tهE�%]�a1f�}��\�7/��H���>,���DK���"����F�hI�}X$Z�e�s,W,���>,-����/���E�yهE]�a��E�}X$Z�e�s,��..��H���>,-��DK���"ђ.���������0/��`��2ü��"�.��H���>,��.��H��=�Q$Z�e��t��� � u^�a1g>/���:/��H���>,���,���"ђ.��`����>,-��DK���"ђ.�����g�>�E��yهE]�97�rH��}X$`�e��tهE���y�"ђ.��H���>,-�Ϲ�+@��b~�}X$`�e	XtهE]�a�WwtهE����o�o�rH��}X$Z�e��tهŜ���"@���"�.��H���>�X� ���DK���"����F�hI�}X$Z�e��tهE���=�ߟ�o�rH��}X$`�e	XtهE]�a�hI�}X$Zڞ�(-�Ϲ�+@���"ђ.������� �����/��H���>,��Ϲ�+��eyuG�}X$Zڞ�(-��DK����Y�.���E�y��� ��p��,���"�.��H���>,-��DK��E�%]�97�r�e�yهE�ܗ}����e	XtهE]�a��E�}X$`�e�s,ϸ�0/��H��=�Q$Z�e��tهE�ܗ}P�e�yهE�ܗ}��<�2ü��"�.��H���>,�������>,-m�k��t��� ���í�z�O�w u^�a�hI�}X$`�e	XtهE]�a��E�}��<���>,-m�k��tهE�ܗ}PH��}X$Z�e	Xt��� �3@���"�.��ȫ;���"ђ.��H��=�Q$Z�e5s_�97���:/��H���>,���,���"����DK��E�%]�97���:/��H���>,���e�n�S^��(���,���"�.��`yH��}X$Zڞ�(-��DK���"ђ.�������}��}�7cy�:��>���>���>Ұ� H�"� �����#�sl���p��_��A�p��H�$��"ђ��,�>,��7`Y?�cnb	X�}X$Z�a�hi{^������E��}X$Z�9n��*-	��H�"��"���,�>,���H�$��"����7��������������H�$��"ђ����������,�>�X�`X�k`	X�}X��as���>[q�	�z��"ђ��DK�>w�H��H�$��"���,�>,���H�"��"ђ�����\3�8~� ���>,-	��H�$��"ђ��,�>,���H�"����� ub���}X�̍}PH�؇E�%a���}X$Z�9n��� ub5sc	X�}X$`�a��E؇Ŝ�s`R�����*-	��H�$��"ђ��DK�>,���H�"��"����[��>�,�>,j���~lE�ԉ}X$Z�a�hI؇E�%a���}��|�"���,�>,���H�"�âfn��_����y�"ђ��q��*-	��H�$��"ђ������,�>,���H�"�s���bnb�7��DK�>,-	��H�$��"ђ��,�>�X�H�"��"���,�>,j����"�&�a�hI؇E�%a��,�gI,�M��"ђ��,�>,���H�"��"�������q���$�&�a�hI؇E�%a���}X$Z�aQ37�aQ37���U��oq�ab	X�}X̙O�"@��>,-m�k���}X$Z���?߫H�$��"ђ��,�>,���H�"�âfn�R'�9n�r�"����F�hI؇E�%a���}X$Z�a��E؇Ea��,�*��������:��DK��E�%a���}X$Z�9n�r�"ђ������,�>,����3��E�ԉ}X$Zڞ�f�X�5�o�hI؇E�%a���}X$Z�a��E؇Ea	X�}��<VQ37�A ub����5�DK�>,-	��H�$��"ђ��]||�"���,�>,��������:��DK��E�%a��,�"ђ��DK�>,-	����������H�"���Y���]������í���5���#��"����F�hI؇E�%a���}X$Z�9n��>cnb	X�}X��?�}P�e��}X$Z�a�hi{^�H�$��x|�YE�%a���}X$Z�a��E؇Ea5scq�ab��,kq�ab����5�DK�>,-	��H�$��"ђ������]�w���0��9�}P�e��}X$`�a�Ww�}X$Zڞ�(-	�7`�XE�%a���}X$Z�a��E؇E���E�ԉ}X$`���&��r�ԉ}X$Zڞ�(-	��H�$��"ђ��DK�>,������f"@��>{qH�؇E^��a�hI؇E���y�"ђ��DK�>�X>W�hI؇Ea	X�}X̙O�"@��>,-m�k���}��|�"ђ��DK�>,-	��H�"��"�   ��,�>,j��>w��o?�N��"����F�hI؇E�%a���}X$Z�a���܎>ތ�o���\�f���܍f0������8����3��3���3ע���~D�e�E��%g�E��%g�E��%g�E��%g�E��yk-�E_f؞�~<�R�ї��"����"����"۹�"����"����"���}� �W�K�\�VQ3�Z4��)��5�\r�Z��\r�Z��\r�Z��\l�Z�]\��%y�Ak�*j�Z�V�K�\�V�K�\�V1g��Z4�����h��=�����c}�v�e�E��%g�E��%g�E���\�V�K�\�V�K�\�V�K�\��)�"�ї�͢/3p-ZE.9o�k��̵h��̵h��̵h��ε���o8�-rək�*rək�*rək�*j�Z�fї��"����5�\r�Z��X�<�2עU�3עU�b;עU�̵�"����"����b�|�E?�e�ח��"����5�\r�Z��\r�Z��\r�Z������h��̵��x��̵h��̵h5s�E�x�e�E��%��y�"����"�����[�B������h��εh��̵h��̵h��̵h5s�E���\�>~ ��*r�y{^��%g�E��%g�E��%g�E���v�E��%g�E���k-�.�ҿE.9s-ZŜ�X�f�pc-ZE.9s-ZE.9o�k��̵h��̵�b����\�V����"����"�����f����4x}��k�*rək��`Y?��2���F�K�\�V�K�\�V�K�\�V���\�V�K�\�V�K�\�>~ ��Y�����ק����\�V�K�\�V�K���E.9s-ZE.9s-ZE.9s-�.ןU�b;עU�̵�"����b�|�E����͵h��̵h��=�w}��-�/3p-ZE.9s-ZE.9s-ZE^f�Z��\r�Z��\r�Z����֢�?���<�2עU�3עU����F�K�\�V�K�\�V�K�\�V���\�>~ �c��̵h��̵h5��=����]�e�E��%g�E��%��y�"���}� ��U�3עU�3עU�b;עU�3עU�̵�B3_����t�pc-��,�U�3עU�3עU����F�K�\�V�K�\�V�K�\�V���}��s0/3h-ZE�\k�,�2�֢U�3עU�3עU����F�K�\�>~ ���/3p-ZE.9s-ZE.�s-ZE.9s-ZE�\k�(μ�pp-ZE.9s-����ZE.9s-ZE.9o�k��̵h��̵h��̵h��εh��̵�������֢Y�"�X�V�K�\�V�K�\�V�K�\�V�K���E.9s-��8�W�K�\�V�K�\�V����b�|�E���Z�V�K�\�V�K�\�>~ �s��̵h��=�Q�3עU�3עU�3עU�b;עU�̵}� ��o�μ̠�h��̵h��̵h��̵h��=�Q�3עU�3ע����ї��"����b�|�E���c-ZE.9s-ZE.9o�k��̵���g��̵h��̵hy��k�*rək�*rək�*j�Z�f����?��������~�\r�Z��\rޞ�(rək�*rək�*rək�*r��k�*j��{�=����ϼ�p�g�ڳ|5��=�ߟ�[ �����X?w��f~�g��sw/j����FQ3�~��Q�̷����[�񹊚��s��1�����s}�E���[��>��f��-�\�{1g~�?��i�R�����>�b��|����x�7�3@���48ק�^̙��O�s}�Ŝ���48ק�^̙��O�s}�Ŝ���48ק�^̙��O�s}�Ŝ���48ק���o�g|w�?��i��=���48ק�^��?�3��4؋���{濟{Q3ߟ�(j���~�E���=�u���o?�{���ip��f�����^�̏��ק�V�̏��ק�����������4؊�����4؊d,�>�7c9�>�5��T,�>�:���K��r�`I�A�%a�
�}`����>,���H�"��"���,�>,-	��H��=��X>�2���DK�>,j��>(�2���,�>,���H�"����*���H�$��"����F�hI؇E�%a5sc}�A���ϼ�`�âfn��"���,�>,������>,-m��/��X_�}�eas�ρ}P�ea���}X$`�a��E؇Ea��8�H�"��"ђ��DK��E�%a5sc}�A؇E�%a��X��3/3��H�"��"���,�>,-	��H��=�Q$Z�yl��/3���p���\؇E�%a5sc	X�}X$`�a��E��q��[F_f�a�hi{^�H�$��b�<���� ��"ђ��,�>��|�ec	X�}X$`�a�hI؇E���y�"ђ�������[�����g_f�a�hI؇Ea	X�}X$`�a��E؇E�%a��X�U$Zڞ�(-	���������>,-	���������<n��~R�e��}X$`�a��E؇E^f�a�hi{^��3��E_f�����p}�A؇E�%a	X�}X$`�a��E؇Ea���}7`Y��}�a{^������W_f�a�hI؇E�%a	X�}X$`�yl��+/3O��H�"��"ђ��DK��E���E_f�a�hI��q��U$Z�aQ37�a��E؇Ea	X�}X�ea������o���=\`}�A؇E�%a���}X$`�a��E؇Ea����\Ea���}X$Zڞ�(j��>(�2���DK�>,-	�<n�r�"���,�>,���H�"��"ђ��DK��E����q������ ��"ђ��DK�>,j��>,j��>,���H�"��������*������>,�Ul�˅��[����9�DK�>,-	��H�$�������>,���H�"��"���DK�>,j���rї�}7`Y�!�ea���}X$Z�a��E؇Ea	X�}X$`�����r_f�aQ37�Aї�}X$Z�a�hI؇E�%a5sc��X��2���,�>,���H�"��b�|b}�a{^�H�$��� �W_f�a�hI؇E�%a	X�}X$`�a��E؇Ea��������>(�2���F�hI؇E�%a���}X$Z�a��E������`��"����� �âfn��}�a{^�H�$��"ђ����>�[$Z�a��E؇Ea	X�}X�ea����5�9�}7`���/3��H�$��"ђ��,�>,���H�"��"����,�U$Z�a�hi{^��������>,-	��H�$��"���c,�y��؇Ea	X�}X$Z�a�hi{^������:��c,�R'�a�hI؇E��؇E��؇Ea	X�}X$`�yl��;@��>,��'�A ub���}X$Z�a�hI؇Ea��X��{��}X$`�a��E؇E�%a5��y����&�a�hI�����nb���}X�̍}X�̍}X�̍}X�̏��ק�o������?@�$�      �      x������ � �      �   m  x���[�� �o��L
�^f��K���J��;u�WЄ��¢E�}��r/Z*V*l���VD��Abe=݈v%����D�t'xp߈#�I��,�*���D�齃'�+�
ݫTʅ"+Oq�� uE��BU�4:��fw�m���O�m�c�&���?�+�dAw���;��
��ƾg�ROE\}	l��̱y�FaFΦN��~1��ڻ���x_MdKS�M���:&0h<5����<�:
_L]?����s��zˌ>k��ҳ�L��N�0̗�v�L�}1ؗj�U��q^�3z��Fef7�`z��l�?���j�M��1J������=F��0��vnzX�Hl��z�v��,���~�x      �     x�}�Q�� E����+A�Ĭ`���b��&U��L�Ӏ4s) ����%�(8� �%�����������d�T9�dCCL�R4���b�EUQ�~�u�%����+h �?�)p�5QR���@&�y��$m��m]C�J:�7��XQ�婊J�!~#[N�s*�F�yXv^B{�:CI(���J�y�(r�������`yA�	�eC�G�/;H���N.%~�MR⃔
��5��[��zNe��!�o��
V�d���)��b�U��a�����3
�(�P^܊/�xDᆊ^�n��GZqA��Kv�K���5 ���޶0Ve�Rש��Xjݨt�ꣳ�m����n�ᣑD���޶�"!�t�c�K� �iDB��Db}�G7k���s�w�y�zP[�P��J�6T
汢� +�_r�)�|ȩ�fT�`Mĥ{ô7�7�`(w�:#�;���S��<�4���%Go(�hN��I�h
��]�3,�vs�nn�v�K}�T�zԮF���u[ u��ɰ�8�H��E���M�����mYe������@φDq�c\�����Ⱥ�����t�:��X�C�Zt��V���tm(�@�P��~8h�<�䄊iC��"P��xC�6
WTo{��ԇY���,�,�j�Sz��r�C�����Y�b�1�c`�� :��ab]�W+��o���,��A�7����ҷ-l,�W�q�).�����9���ɺG������ؑm,���cr̓�u}`�#�v��^gO���*C!�.�v�y����ۺ6�逊�*����E���<<�z���OS�uQc}&�ơ�ai=aJ�]��-l,=&xa�%�Yqe�������Ү�L��~�{�N��R�(^X�.�k����,�Y>�u+]ݨ'z���_�L':�Nt�y�v�����;��9��vG��jXJ��O�{��Q�{����Hs$���A���4]Љ����{�sR:z�w�nE�'oͱ�,��^C�j�����~cE�����ˮ�+�� ~�}>�����      �      x������ � �      �      x������ � �      �   �  x���Mr1���0]!]!g�6�d��9yH1�Y�[�{�1& t�z����������cв0=�������h<����W����h�輖]	�@�׺=�	�,<��	K�0��WY��w]���$� �'�ir=�x΀;_p��� 9`���gN�;�"L�z�L`Āǽp���沐�ہ���fGtb��}�+y�����U#���hʧ�U����ӈ��z�����O+�'Jb�}VK4�p6��i&��'v��>��Њ�
��9KfV�'�D7�-K������x�̥ZvU2�Kcmݝ���	�f���,����K	]�M�`ĲP���������t�o2$k�G<�J��`�ٻ�X�>���?���}�»W���}�3i���&��Lj��g	����T*��9�{����p�:�j9n]EiXCx��F��e;b�Fl�j�Xw٣CϮ.��]��t���
�z�.Uc�A��a����R�m�c�;�Ŧ�pr��4#<o1H�od89Gl��k�*,�;���J�X�ӣ�Uٵ�E�����P=(:%�A��R��;G��'9�\��/��cۃ_�(9z��U��U�z��o<T�;�\�<*�d"��n7�g�!��P>Y�?��VY67��X������\��\����-ﲿ��y��TW���
��mH$C+�r�R��8���(��t�1��ף��t�rth���wΉڎ��c�sN�1�{���@U���=�&�qW��1�n׃4�r���ڝsV���w	�{��e�c��H�N��}�y�����<J��Nd0�{J������	�a�)S���f���*��ٮ��,p�[��P�D����o�o�f�}9���������{4      �      x������ � �      �   �  x���[�� D��Ō���r���	Nb#��Z]�q�fF����J)���	Ƿ���Y۩�_��=%e��u8���bqfI�o���
#���/o�Ts��B���g����p��y���N0�>_ǩv3�|2~V=��|e85�y��p�y��ꜯP���|�%��4o�)���m>%�/�|�6_��/�|�C>�����u)�|j-�gI��Z"��u���4x>���.�^\B�q�η����oo)�%I�B�q���"84��e�( �t���v��S�
!7���NQi��))�˗���Q 7.\���]��P���嫁�|�Q �\3���<�5X(`
F
l�5
���r�ͷ��H��._	��SG��|[](`J��ǿ�if�@��t{q��)(�-ɞD�Ɠ]��.�X���u
�]��S`��L_O1�-}���Fq�Wzg��M+:<���"���cL�@�&�w�y�J��ȗ����M��W�O=�j!�w���+ݦҾ�8�Lw|��N�G�~�ݎ���h�>��#��m*�e�1s�t�#%	@ی��
��藵���m;Rjh��2�@�
~�4��M/�@�J@ct%�+)h}���n
��e�T4m3�h����m{p3�@�
~ ڰ����~�9K-      �   �  x�u�[��*D�������r�?�� "0����q0!�֞�O�O
�?1>�%<�O����'�B	�>=?��/����xC�,��hPnH�%�P~��Rn��n���ʄ�~e}��便�������UYz�<D�t�n7�&�b�vɰL�a{�%�\�bX���)�7�����労}�
����:�t�l'�f�����#'����Ӳ�t�����;*Eޞ�U��������0]��K8��{�"�L���C��Eu��,WD�Nz$���w�隄��=0"�"=�$U�Y6e�_�劆��7����r�{]l [����9��/���#���5��fC�}[����Ҋ"�hE괢)m"���DE�D%�D	m"�2�2�B�L��+G�׀M�*]4Α���*�y�����4]�p>��l2��zYk�,W(�&g��b��L�&y$�5���� ��6GXD� ����m�����������On=���0]S�3���3�Ni��N�\��3��,Ϙ�
E���:������!��3D=c�魸�G���������,��1M��L�g(���1��=&����a[�}Ӳ]%�����i�Ξ�k2���3
�e}j�m��a���XV�O��@|�KÑ��x`\#9L�t�=_�/Hr@���[�k�O�U	�Ҭ3=��~��A�4|FF����̚tX�(��E(u�&�\Q�3փ^�;Ǉ隆��t�E� ѽg��cqO䍀⣩{��t@7�l����`b�������ڏ_�	%@[��`"D*!j���A!��@����l0�#!jv���B�L��A�,�+!jPa�vֳ�F�̄��R��3�����MU�l�I�S�RM�Uth��}w^�i��UT��/[5mSuA��jئ�����Pf�������P�1������m���Qe��y{bڦ�U�/��m�}e�6Y*�)��3!����&1巵a�PV:>~�_mMӇHPcم?bj��DL[{����kj�$Q�a�f�ؒ�>�f�>45�v���g���i�������ƞ�g^YOcB��5��{0��æ��V���GB��g���ג�>d������;d5��}I��A�~[w�7�tMG�����i���x�%���5`	5����է�E��E�7���VQ�^�o����U��nb4��
��ڑ�z$�g_eIj}[r��S�g�wLC��UPa�x˿	���R1{�����-RQ���~3'͉�+*�S��
�|v>��S��CPa'���dX�h��ĘЋ"q�*�$��x�y���h��a��(��O�5�f#�VTX[��a�r�S*lJ����R@����:~X�h�������MF����)}MD��${�)V���E,�Ћ"q�0�$��x�yՆFfc�a�������}��"]\����GT䛢��\J5��UhH�h7���_��B/�$G��+�p�f.�vԀ/��T�yR�I�階�t�p9H���P��EM�j��m��� �iWM��wMC�^5)տ�H�Q/���Q�������:�k>�ͷ#�����ȮN�z?Yj>��ߓ���\O��#��4��ϑHK����H���b�泚~�r-T���r�R����OX�B��KXҜ���w/(GU���\��*r�o"��Y�/W�r����1�*����Iv
g������2mW�\a��g,�:M������3��h��s_5T�\�C�P4]X�p����}%��t<?F���~!"�[�S���vUF�X�%�UA��k
�E��"��Hj�i�+��1�Y�s�yG������\_g��_��)�_�D��&��~8M�DP:R@rv����`(7��C��wx�vU����g��Q��kn{�!�Qy�\r^��,Wd�d<����V
n�-��ʢ}��KťM=hX%ұ,J�B�-dے�9�lSU
O~�5��0]CaE���X�-��
��=�]�b7���k�Σ�lb������_�/D��H�#��8b�k�[�˔�����	C5L�@'f�!�#������K�7[�I�W7\ ��%)��;�tQ��B}(�F�>��T�a�,�%|3��M[����a���&�̫!��0]�w�f�K	v����G���������A�K	~+!���"��@}�k	��NB�8��q�O�G�����]F��8<��>?M��,��N'*��ռ�<,��A�!�ث�ZL�g��I����7]D���FtE�Q��)���W�i��b��+���nEf��a��KJ�x'�K<Z(_��B����x�P�\tX�_.:,$�r�a!���Y��k�Q���b�r3 ��E�����B�^��^.�M��ԑ�P:ʓ��(��GqP9��ꑆ��,����Bg��ځ~��ʑf�ߗ#)r$q@��ȑ,9 ���E�����Z9q޿���\ �S ��I �	 ��)
�D��2�"�K
e�ty߹v.����MZ�TH��H��p

��(�򽼳Eό�&��D(H"��PP$B��B"�{g��U�Х-"tU�]�"BW��(]�"B��Ѕ,"�ĳ]ȪH��zE��^�+WD��S}��,B���ﵜE�ؐP��؈PI����P����D��E$~��,�8�"�]�+XD
��@*��@��@�7s^��Z!I�9/9s�8[B���dΑ�΍�TΊ��o.�H���7�D�G�(]�ޅ��z�#�T�<P�d��Kv�P?�[@zd��zB�.����y+�|d���%O]�^�ԅڑ��G~
H��t�.��B��D�#$G�	(_2΅�%�\�^2΅g�@:g�@��[NR���$��t��Y"�uX"��t���/R�M������D�I��D"�䁤o%~�7%����E�� H�r@��D��(�-��������"�-��|k�N�������<�W�      �      x���۲�)ʭ�g�KE���������;�u�zjv��)��R�/���rj~��b�����?�?���i��/Z�2-����;������z����_�pd8������:����U,L��5������kc���M�'��������;��o��-^�w��lke������V���`���"Y+[Wp��Y+�Wp��X+{|W0�I׿h���]�M�)��e��Sqpn����0�l��L<a��,^�G��o���k��JYky}d���Xk{}d�c]��ͽ>�<*��Uke��l���,�^�����V���#k��_�V���#���/X+�{Wp��[+{xWp��/Z+{���Mo�q{k�m�n�� N�[�3\-���
��g�?F�����e{�G���+{�Ag���{�A��^h�V�����]{�����ڳ�t��'t`o=��.���]"{�A�w7��[��+���ުt��
�<���z��]�M�.U��f�*m����@��H�~��$���gް߁��w�788��n��#�>6}���F�>6}љ������A����\��k�/_�E7�}|m�98�}|m&=.�`��]��~���v�	&���=>Ȇ�>��2\-��w��a�qo� ��G��}8��wБ�b����A��^���Y��-���;�n�O辿�Ǌ$k$/߸����Q`����^ׂ������^7�W���k���:Y:�W����!k�ơ譍��.���_u��{]dӍ弄����t�{���禯c�#׺_��{ў弄��{с弄��O'�OX+�7~Ӊ������o:�ay|���%[���ay���K��ʇ�A��/٢��>�rѝ��J��岒�ke�^n�s�t����M�@�����7����]8��0�4B�	��	w���wA��GԖ��,�.�pd�+<��sA���hi.����-����v��\8�����s�N:��i.�t~p�\8���`���I�w7���n�]�MsA�㻂��+j8]�|l��:t,�>�}�}E�F��m��8����Ǯ/����Ǘf�ȸ�5���o͢�N�}|m�v�p�{�o�ȈU����w�6]��t��]�M�}9�tyWp�m_N'��\>2�`�l�]�E��9X+[xWp�~_N'�>�7}�r�����^��c/X�V�+���r�*�.��U��]�9��ʄ�hU82-�G��ba���J����z�*�����|$:���4�4f��֣U�h�Oh=Z���7i=Z���
nZ�V�����֣Uh��ܴ�J�wC�4�ZO:�+����:`�F��[�����pq|�;k���b��k��� V:�n:�A�ty}d�Ib���#ˣJ��Xwݽ>��"�����Ms�����#�nr+]>|dzT�r+�>|d���A���ܴ��X���-��W��J��o٢w�uܮ�2AQay>Ȃw���;��9ʈ�'�k�<Ίr�w���#���\k=��t�4�ZOZLB���\k=i�1�q������'4�ZO:����b7���t~p�\k=���হ�z��]�Ms����W0�Is���㻂������t�a��������Y���v���\j����p�a��^79l8���"ӡ��cM��E�9l86�}�Ȣ�>\dѕÆ�N_��؍Æ�._�Ew�}|Ɉ.TܱV&���t�B�ke
�
n:p�p��]�M���:���{��ԕ
�G�w����hU8{����h=��p����]>>G�����>>Ȣ;{��%0���M'����NL'K{�����],أ��[33�B?�eC��f̚l:�Gtx�dә=�ӻ;�.�tyWpF��N�*�O����3�	��///���.Hv��,8쟱N8�;���3�	����t� Q�A��h�Z�~|�EK�z���c/Z��á�Ǘlх�������Y�Վ����ߦG�_��t�����=.�`פ����:<5X+��{Ӂ�����zo:r�p���<��jg�ڎҋ	���|�g�qn��b��1�a�eL�r��pb8Z�I��ta:[�K����X��ĸBw�1��Z�H�[#{��'t�W�d�O�(1���]@?W{\��Z�ۻ����2M�����.�*��t�W���`(�n�*]��t�W��Z#���W�nm�s��N⭉�q���ˣr�W����$1���u�Mg�q��.2*�q�E���,�J��t�p�E7�q��.��.1����K6W{\�Z��Ǘl�^b\u(��%[t�W����r?z��-���t��뻂��۵S��y��_�{�e����D�������ڥn�>��p�pۮ}���l�]��������Z�� �ܘ�z�������������_^�c?�8��p��~�q��������Ƕ,���I��}Y4?�8��?6f�������,��t��]??��X+C}pӞ弄��
n:�t��
�8����A�w7��^:����3�K]��t����}8�\oz�c��!|xբ���5�+�� >��ɢGd�w2�wM6�� >�Ľk����A�׫�
��%X+czWpӉ�.�
n:�$�V7��j>It�.�0Q:y����>M:
],��qr�Y轗ޭ�եw>O�
o�R��ڙ��lq�'��Y�|�x��8����K�[Ss�˨x�C���]G�3�*�?r>���ǚZ��Bn��r��c!7��`9�����|�x�X��,�R��:o����Uz�c]�YC�;28�`�T:��ࠓ5S�c��.V.�硷f���.������.���Ç�,��r�x�p��ˑ{���]��=���.�C�p��.ӻ���>r�r�x�Xȍ˹{���{7}���ZSK���-�lW+'��_؏��]]i�I�?�|�m��AG�����ֳ��]�������4t:[ڳ���Y-��ojg|���tZC[|Q�Į~��YE����׏e��k䇓'�?�q��r����Xȍ�����q~;y��c!�Ś���׏��8�+Ǧ�磳7&NX:<�\h�X:}|��0�S��.�{Ӓ�x�p��K�r��r��/I��_�pI[<}9��%o9��� ϒ�x��&�Uϒ�྾I������7iᒻx�X��^Y��/�q�^�},����͍6xҳjj>����zƒw��+)��ރ�BGK��{�Y�l���AW�����=p53(�'.��^�L����㳈���{��cg2U��^?�q�r�x�XǍ���x��q9x<~,�L�j������B.���{��c!7�%�P�,�
Kk�\P}�=���χӆ_3�&��N_��̹����_]$T�}}�{�*��:����-�I.�x���-�K.�x���M�.k�/˸܋JF�T�>�q�ArAu �����*>r�U���!},�Ƴ䂊����x�\P�����_���H��K��6�tF*u>60Ra��~��a��P;���T�0�Tj�͗�F*���KC#��g��MC��g󥡑
C�����H����|ih��P�l�44Rahn6_�4Tah� �Z	U�8(���0�� �8 Rah�A���
C��̶������T��UC#��NkC#���[KC#���[K������Q�Ϸ��F*mF�F*�Ϸ��F*�Ϸ��F*�Ϸ�ƣܻ���o-�T��o-�T��G��1Vah�Y�0����
C��Ua���֋ C#���9C#��^�0���[��*-�V��
CU�Ui���BoU�0���[�F*-:�V��
C�^�Ui��Ш(d]
�0��kn`���b�@@7�0��9P8��yÅ��
C�U��
C�M��
C�]�F���(}�FB���J#���J#���J#����PH���,����]��~� h�>�v6���R��Oa���R��Oa���R��O`������F*-{9��F*�~K�6B�F��Y#�
C�� �Fu#���F*m�5��0�����0Rah��W]i����k"TahT�6B�Fk$T    ah%ʽ�4Rah%ɽ�4Rah%˽�4Rah�Ƚ�4Rah�ʽ�4Rah�ɽ$4Tah�K�4Rah�[?�*�z9,�F*�9,�F*�F9,�F*�&9,�J���j��Ii���j��Ii���*Z�O}�
C�\8`���*Xz��
Ck\P�0���F*�IAࠑ
CkR8h��К���Z
JC�֤ p�H��5)4RahM
�TZ���A�wݥ �4Tah]
�T=�.�TZ�;j8`���z�Qñ�H���g�F*���0�^9j8h���z�᠑
C띣�c�
Cw�/��F*�y���0t�r:h���]��頑
Cw�/�ã�
Cw�/��F*����0t'GkL,ـT���Ua��Н�
w��н�C���hU�0t�G��H��{=Z�F*��Ѫt����ɻף�i��н�J#����H��{=Z�F*��ѪtWp�A�V��
Cz�*�Tz�Z�A#���z8 Ra�k��Tz�Z��H����0Ra�Aj��Tz�Z�A#���z�H��G����Tz�Z�A#���z�H��G��4Ra�Qj��Tz�Z��QH��G��4Ra�Qj��Tz�Z��܆�
C�\k=`����Z�*=q����
CORk=h��Г�Z�0�$�փF*=I�Ui��Г�Z�0�$�փF*=I����
CORk=h��г�Z��*=K����
C_�̆F*��;��
C�����
C�y�Ƕ#�>���Tz�;l8`���s�᠑
C��5�0t*�X�
C��5�0t*�X+�
C�N/k%Ta�TܱVB�N�k%Ta�TܱVB�N�k%Ta腏��{�0Ra腏�F*��Ѫ0Ta蕏�F*�jت4Ra�U�V��
C�IQ��
C�YQ��
C�EQ��
C�UQ��
C�MQ��
C�]Q��
CoNQ��
Co^Q��
Co�3��RH��7�뀑
	���H��7�뀑
Co�3�#��4jU�0��Q��H��7�Z�F*���z8Ra��K��4Ra�=H��4Ra�=J��4Ra�=I��4Ra�=K��4Ra�H��ΊTz��*�Tzo�*�T�|0�Y��0d:ד��
À=�
�>�*���J�A'�m��
à����H�a�E�Ui��0�*ު4PatoU�0���*T��N��i��0h/ު4PatoU�0:��*T�?I�
T�?I0Pa0�$y8	Pa0�$y�@�!�?�*��$�0�l�I��4Pa��$y�Pa��$y�@�a�Q"��
à�D$J�Ag�H�*�.��C�AW�H�*�n�(T�%"Q�0dGŝ>�/-�0�s�&0Rap��Ma��0����@�a���7��
À��oJ�A�C�c9�
À����
À�����C�*٭~WC�A�C��*���J#�A�C��*�NR{Q�0:�t�@�a�Ej/J�AW���Tݤ��4Pat�ڋ�@�!��Ek%Ra��b�Z	T�ؠ4Pat�b��@�a�I�J�Ag)6(T]�ؠkT]�ؠ4Pat�b��@�a�]�J��H�g��%�Ra���Dh��0�����H�aБ���
à��'�#�<�~��	�0�ȉ"8Tax�#Eq��0�&g��H�a�]ő
Cv�ɩ�8Ra��cEp��0� ��H�a�Qő
����,�#��g��y�
à��0�rd�4Rat��@i��0�α��H�!���kp��0p=ou�݇�l\\ő
����U�0\�\ő
����U�0\]u��.ӻ����#��뱫8Ra����#��h4�5�0�sv��H�aЁ�;��
à#�wJ#���Oi��0����H�a�ER<ő
����x�C��7I�G*��)�T�'Ě
U.'�0\^N*U.O'�0\�N8Ra�<�<p��0p�W�Mu@�aМ�4Ra4g,�T�)�A#���,�T.I˱�����x�Z�0\ҖG*�����
��%q9p��0p�\p_ߤ�K�r�H�a���8Ra�$/�T���8Ra8�}�j�~�0�^��
à��=h��0h>x�0�ރF*������
����U�0\�G*�����
C�Q�G*�����
����U�0\�G*�����
����=p��0py�~�H����p.�>��
à�J#�Aw��F*�'ǹ��H�a�^rAő
����� ��k��(���H�a�IrAő
���䂊#��>��0�J.��>r�MrAő
C���P�!��$T�0�K.�8Ra���z�7�Z�`(�0��������߬/H0v�d}�H�a����]}����./�/xu]^0P_ȞR+c_�깼`��0��ry�@za�����W����~�F�.�,�` �0�9�톁�������`�����ٯ�?�^�oɅ���?��U^0\�zVy�@oa��U����G��V��/������ד�Z^/*/H-x=��`���}[�)/'r��mx=��`��0������vOz�X��0ش�SY �0ؼݓY��0ز�SY ��4.�������@`a���Sa���=m���+�@RvO������@\a���Sa����L0"i�gvO������@Xa�u���E@Wa�m���Y���}���U�Lӳ�a���`��ە�
�|�+$�nW(*8�ݮ���nW�)��ݮ0�Sp�]a��0��w��@La���v�!����w|�+����PRl�XX �0��7��@Ga��o`f���`������"
�r4��H(�s�.kr���a���`=�����������F؅�	�M���%`h'�H�.0�Np���a��0�&��@8a�]""��nB�ID$0�M���H`��0� ��@4a�Q""��f�DDɄg�u�c ń�u�	�����$��%�K�#�������{�uZ	�}Vl����rڮ,PJ,������r�.,�I,������Jڮ0PI���
��Kڮ0�H�!K�.0�H���
��Kڮ0H���
}�Kڮ0�G���#u�Kڮ0Gp�q�z�Fl�q��@a�}��}@!��v�,F��8@a��0��q��@a��� ��*�[s��D���Qh"��0�Dp�{Ga��0�����@a���u"���Cu|�(����Q�!��� �C,��-����l��B,��"%����,B��
���0�A��
w�l��;�&�'�Pa�rx*4,���@a�rx*,����]���)0�?��
��K�Sa�~0`�y����r�SY�}�C皧�5�>,�<�������@�`�R�T�Xj�
كK�S��Xj�
уK�Sa�y0`�y*$,5O���A�Nj��D@�`�R�T�Xj�
��s͓�@���r�SY v0X�y
��h\�vOe���`��0P:��<B����@� G/5O�����R�T�Xj�
��K�Sa�q0`�y*$,5O�������08pe�T���@@]���@@Y�n�#-��k n0X�e���`�����(��8P8s �;t\8P��Q9P��q �05p�@@a�i0�=��! i0`ρ��@�`���������,[���Oe���`��65����,3���-�k�)0�2�Sü�0R2�$��K4)0�1p�hR` c0�(�$�H�`�I�I������D���&y�/
�������t��~����#e�|�`�#e�z�`5��X#O��v��5�HXO�!�\0�"a��@�`�U�T��n�����قw	S��v�
D�%L���
$%L(8q���S    
�Y��X�W@�O�a�\=�j�Ejq5<�0+�ﮌuP�`�l1�A���J���
�;c *�7]�u
��L0B�z n�J��	��H��^��f`�Q@�q�30�(XO�n�D
�t'	�+�l��+�jX$O@ֺa�:Al��H�`>n3�Am��zȘ�	�K8cT&�O���P����c �%�wI�e	�]�Ƈ�*=b
��H���n`�I@/��70�$X�����n)�c�x�P� β��"=z�R����U7,R#�G0���z�u/�"�3��]$E@﹊a�Ar�TBY$D@O����0�!HN�J(�d蝘7�Az��}P���yc � �o|��P�`�@3B�� ��s5c ������P~�޶Ec THk&�m Hk"�#�z����J�5�����@Z��o	�5��^�;���F�iM!�a�:���F���j;�V���9@/}�cɁ4�7�]P30��Z|n���7&&(7@���1C�zv䍑Xl�ށyc%��WJ�X����X0Vb�z���Xh�^�c%��7P�X�e�4c%V�'S�xkC"��Խ�i�#�l`(1@﫪��� �(������ �?��D,/@o�����s%o������+�� �n
�J,-@/Â�+�c�`�����,+�� ��
�J,+@�����
�9b|F�eG�XT �	��5�0~�PR �*�CE�4ǋ�0Hk��MC=����_4�Hk��MC5��F��4HE^*�R�'�JC)�T�M��XI yT�4HE^*uR�g�JCz��V:�"���0�n��PC��5C	z�.�R��Iޘ����76b� z�䍑X>��yc%V��O�X���m^0Vb� z*��X:���c%V��U�X���mU4V"݀�̦���@/g7+w�uY��mG0=�4pg8[�6���7����Gp��O:3�M�-ݘ���{��AsC�~��w_�S��Љ�b��ǟt�֯��5�狧��ָ��'��8����
�Á��Ó�˻����f'�$�N�gKf�ݵ"'L�3�wA�֬/��˻ ������� kk�T?5[�����:�.���o�������qf�ƿ.���x���å�Fv�v�F���co:�Df��e�	S����5qnMZ������2�f��k�\l*�y�R-�N��HA���Ƿ�Oz��1t���,z>B4[^'��f���$mҼ�y�0L������X���/�n<�y#83\�e#�1���D/�4w���-yЉi�����Fte:YZ6Ri���Z6�㻀��9�5��[���<�Ͻ6���;��k��<��� w�A��ĵ5A��c���$��;���	rGt�p�E�yl�pmM�;�ӻ��.�nd�F�FR�`�Ͽ6��n�c�����η�A׏��\����;��}|���;���=Xt�[�ˇ����o���N��ķޱ5�]��5T�#ؗ��4��Av����5'�W�rܫ�ښ�H���ޚ��L{�����5'ݘN�^��o�[uk��{kN:1�����J���:�9h�;p�vJ; 9������ �Z#y#S���#��Ƶ7��ُ�k�۾�N��&����;�����)�f;|8�\���=v���I�=v���I�=vn�p�vN; 9�������!�%፜�fn��O��b�wA6���w��]�M�}�t���̍,n�z�b��o͢���N:~���z=e0��Ztܷ�I�w�j��o�s#û���;3<��� �q�"�����v� �lj30(�>߃�0*D�x����AA �ī��'
���M��@��Wk�O�x,
�&^�m?hT�4�jm�I��@�#��N��@��W�c��@�}Ϭ6(�>_�b_�cC��@�3�����@�3����@��W�: *D�y���@��W�: *ľ^"ͼ�"����"M��b_��
�/�c�|� i���.�
�&^�R� �h�U�.�
�f^�R� �h�U�.�
��^��,s���AA 9�)0,$y�-
�9�ȃ�D��F*�
�9�ȃ�4'^yK��@����QA ͉W�Ҡ �h╷[
�^�ρ
�^ُ�
�^�;�AA ��+-
i�a=
Ҵĺ*$�y�K��@��W��*$�y�K��@rs��F��@��W�s��@��W�c��@rK��Р ��T+��
�^y�R� ���14($x�K��@��W��*$y��&��7
�F^e��@��W��� ���+C��@��W{kͼ�[�4*$z���AA �ԫ�5
��^��Q����A��@r336{
��FS�~� ��|=d`PH4��[�B�D3���(TH4��[�B�4g^Y�B�4g^Y�B�D3��u)TH4�*Z�B�4g^ٍ�D#��F��f�YlPHn=44($x����@��W��*$x���@r���Р ���+뀨 �h�U��
�^E��� �h�Ղ;����4�A����0 �s��0k��X����LG���I�sЍ�bi	v�f�)��)�C�;��Ά��t}�/�IK��4K���i	v:�+�iv�����k�+;�����s�s���E�bW	v�|�Ȣ%�9���$��`�X��:��	v:�N��&��A�w��b7	v����n:��z���I���� �}|�޴�g?G�_��?�1J�p��ح��z���G]�F=�Ͻ&��n|�t|�dӝoԃ.��,�;�Q��뷶�*;֥ZxpӁoԃ�N27�8��o=��,x�'��|~����Y$H?Ɣ�!��֓�LgK��zҍi/����UiQ�ҍ��֓NLכ�QV�h=�j�o���G�A�F�.��|��t|W0�Is���˻��޵�s'�]�����Z��ή����z��]���4��[���]�Ms����� {k��znMxdўk�'�_�Z[��z��u�Ms�����Ԧ��z��å�F�]�;7�Y#yo�.ѝ��_�j�]�;����a.ѝt}M�4����s��{k�Dw��u�E.ѝt�����\�;���Y4��έ	N2�&p����,�72��	�u�����
����e�y#83\-,yЍ�(�_"�4�V��-yЉ��\�(yЕ�li�H�Y��\��&-y��]�M�FtyWp���~l_����g?up����~���p���˻ kk�6���� ��|�[�Y�R�1t�p��"��z��å�F=�ƿ����XL�]�M�yld�F�F&�#���S�+5n&�?���ѽ/�ǜ�u��0�G�[50��G7~70���4)�?Fu���.�DB�yMxc#��GW�7F�yrzc%��G�J0V������f"��t<
g��ǣ((+�l<�j���ƣP9+�d�<�7ޡ�xt�'ùx�'4/O��S�(<��e�����߻c�nҰ,4�G��7F�x�:xc%��g�����n:Hò��X�MGiXNãh6+�0<
����£�'+�(<���k�S���G����n����6���l6l��g'G�婒q�h��G�Q�up)Pzc�G��7��Qx�xc �Gʫ�G�Q��pE>�G�f0�Qxc �G!U4�QxyIG^.�F��%y��2�d�t����d�t�ņ׸�D+>����᥻{�h^^O�o��~��tw��v�_,'JKw������o8�~��٠p�h���G�n0�Qx��c �GҤ�G�R�/�ݯ�Qx�c��Fᑪd�X<
�4O�a��}�)�{p���?��v����8o��F��H����(<�^��@8
��-�1��#��`���H	3}�n��m�T����7����m8����b?2{Q��n?X4
��e��>4
o	l�lx?�f˺�6�;�ٺ��.�No������(<���<8
��m����#�Oo���H
7�(<��@����,8����!4
��C�1    ��#��}���Y4
/ϟ_o��#a�bX~-�aOH[ʏGᑈ)���Qx$�upI�zc�G����G��y��k�F�e4,���o^7�F�Xe5���#��nX4
//!�F��H����(<���<8
/������H1�����H�5����H^3����H)6����H�3�K�����ID�p��no�KD$0�G����G*����G�����G�����G���FF��H?4�(<��@8
��F��Pyl�Gb�Ѱh^q��va�aA>0�����,���;o��#So����H����(��\�w�lg�����H ��(<��@8
��T�1��#��`���H�2�(<R'�@8
����"4
��L�a�(�2���>�z��Sa�(<���TG`4
��B��Qx���yp��zc�G�G��j0�Qx����p�c �WV7��D�]�gIuF��H?4�(<R���:o��+QOa�(<&͆���&9<����Hô����88
o���(�� ĩ��8�m�7�&���(<R��hIS�h`4
�DWC6p�n�YO��(<h�@8
�$/�1��#1�`\��#u�dX4
�d_��k4
�t4�a�(�-{�hޔ�4��QxSL֘G�⴨�[�F�M��f`4
��<�30�G���``4
�d?C20�G���G�Dh0�Qx$~��pɉ.��zƣ�Jᚧ���,b? 1�r�p	�6âQxS���"��:8
o�D��(��hj샣𦢤1��#��`���H}2�(<�J�@8
��*�1��#Y�`���H�2�(�)�j\��#�hX4
��p��k4
��2�a�(<�u�E����QxS�XG�MNc�7�b�}p��4�Qx�,��p^YZ�7�F�
m0�Qx$��p)�c �W:��r�h��J�E���9J�ٌF�UǇ��h�|���F��GY��h�|�Ro�/����(<z�g����W0��£�3ۉ����iX0�Qx��&�(<zF��p�|�c��£gf-�(���#�k4
�z��HY4
�z��HY4
o�bsF�����F���7�僣��"�p^]��7�F���F�
G��K�`V�£�I���G��Y8
��1c �G/�1�£'O�G��k>c�W����Qxum7�F���IG��_���jL�/�Qx�:�yp^��qO8
����'�WW���Qx5��=�(���uO<
�&o��«�ڥ�«)Z�ģ�jJ�X���j��X���j*�X���j��X���jj�X���j�O� G��잠�«�?'-�Wsx"8
��cc �Wi$�1�«�F��WGpe"<
��c�j`4
��c�n`4
����Dx��_lg�«ų+3G��ؕ�E���o�«%�+�F�ՒŕF��j)|��B�Qx�T�r��Qx�4�r�E��j�|��F������«�˕#0�W+?�P�«5ʕ#0�W��b���*=t1�Qx���(�J5c �W��b���*�\��p^�!��@8
��c�僣�*=v1�>8
�R��|��(�J�]���«4��|��(�:�`���*j0�QxuW�G�Uj�7�Qx4i�s~�«ݛ8��(�ڃ	T�«�b�a8
��dC���j�6V�1
��b����j�6Z�1
��f����j�6^�1
������+8�%�x�x^��/�J<
���Ĭx^s�	Z�(�沉Z��k�����(�6»�F�5�L��c^�w��Qx�;���׼���Qx�����|���Qx�'���׼��B��t��Qx��'�ţ�oO�G�5ߟ��k��k�X<
�Q]�>��(��c�(�F�g��Fᵐl �G���0�ז�M���^��O��G��1��k�ע�.�Qx-�'^���Z�6`���=�1V�Qx-�'d���ZLÒGᵘ����k��4�(��T��k��5�(��d��k��6�(���t��k��7�(��ބ��k��8�(��ޔ��k��9�(��ޤ��k��:�(��޴��k��;�(<��h�a���� -����a���hU8"a���hU
��G��P�e9Z��� -��*4hY�V��W����hU
�"G��P�9Z��� ���*4hE�V���
Τ�9Z��� ��Ѫ4h��l4h4K�l<h��Ca�V�>��� ��}+�Z�|+�Z|�Ca�F���4�Z��X�Q d��� ��9�J��s��X�QA�X��Ut��X��3c%h��A�4h��w����uڔB�P$���%�D�*@k���� ��߁O4M�����E� �����E� �����E� ������z �������ݩl����"5��W���"1��W���"-��W���B)���l^l|�lŚ4b�
��p�t2��ֻ�E2 }=�97� ��j�D�@w����"��╋E ݭ��'� �[���E ݭ׆'����[�O��w���,���n=5<Y����zix����~=4<Y��O���v��قNh�E��}Ω�P��ߧ�������GAQ���dN5��uG^,������ɢ���#�����(,j��Ԗt������y��ݿ���(,���!�?2��{(�¢^�꾮�qP��}��:�;����F�ݾ��Ͽ�_�.�����u�wzo5������-C=�=f���ۻ��׵��ÿ��׵������׵������׵�j����u-lx�l����讐��~RCI7�z��|�}�������1Fag_�H],j���1&,������ml���1&,���ﯯ�j����BQO�~}E-�=����ϊ:�{���+(j��9��WX������0���sᯯ����S	�66����6�����6�����6u����65�w*��.�P/�ofa/��aQ/��<������G�������G�������G�������G�؆�{u�����{������{��oMؽO�@㍢��^9���1�'
[�ih�QԹ߫$�¢��9��6���韷e�m�f��4صO�?�mlڟs?o�`�>M��m�e���m��4�3ܶ��}��n�`�>���v}���nu��d�75�����BQ�>��7���U�F|��0ة�{�]XԨO�=�m�ӧ������p�p���i�g�m�M�4�3ܶ�}��n�P�~�S=o�P�~����5��蹾j�M?����y��=�8�ǣ���;��$�z��9��9�
z�sr<
Z�˜�y�:�˜�y[������i�/���N�o��9���,��/4�3ěM��w�|��]�����,j�/4�3ܶ���BC;�mj�/�?����%+������G�`Ѝ_hRg�QЌ_hL�>J�����=
Z�����e��ЈN_����eN�l7�����fA~��!�,��/s6g�YЄ_�d��6ԃ_�\��6Ԃ_h*g�mC���]{�[�6�w�QQ�_\ܵGAQ�}qq����E�=*�\{T�����,h�/.q�QX�y_\�ڣ���ĵGeA�}q�k�ʂ�����]��%�=
���K\{T���epO����u�������f*{��ᾐ4e�7��7{�/�����ot����o�P�}�A��6�ڗ)�q[�Z�˜�y��:�˜�yۆ�M��m�Ͼ����m�;�隗ۀ.�2gk޶�&�B#��m�/4Ws}�<��ž���
:���,��M�l7
��+>*��+>*����Y/5����FX�[_������2�hޟu֗9B3_,j�/4A3ܶ���B4�mj�/4?3޶���2�n�M��Fgޫ�z��ͼ���y�A��&f�K����ܷ�����аL�P;}�Y�����n�B�2��
����I����җ9'��Z�V�BS2ý������:�F��'d^,�/s>�mj�/4    3ܶ�.�B�1�mj�/4�6���y[�Z��żC��Ͷ��eļ�B��ŭ�����4������4�����4��z���4��Z���yۆ:�����6��y�#�/4��G�6_h��폰k��闷?���B�/�c���9vQ�|������:�����a�иK�~�B�.M����4G)�/s��mj�/s��m�/4��D�U��)��m�S����v�(_��ˋ��-���fA�|��-�͂.�B�-�M���O��l�o���jYot�?��/4��g�o�<�/s���=��(�t�U�_|܏��X���FX��DX�_h���C��e��-C��e���MC}�e��mCm�FW��6�_��ʋM�e���mC=�e��mC-�e���mC�&V��6�?��G����[�(����Q*,�/�o��]C����o����z���(�u ���|�
:��(4�6�Q*,�l�#M�?�r���~���5q�`�l���vm��qz:n��{o��p���Gq���'r���gs���t���Wu���7v���ww���q��;��}��w�����w�6ޱ'nzl��O��>�l�O��>�b�O��>�j�O��>�fP��>�n"P��>gBP��>`obP��>�`�P��>�h�P��>�d�P��>�l�P��>�bQ��>�j"Q��>�fBQ��>�nbQ��>B)g�Q��>Xo�Q��>�`���^���|¨�}�� ��}��F ��}�ņ ��}��� ��}����}��F��}�TΆ��}�6G��탵I�k��`[�`m� ����	��}�O� {��d
��}�O� ;���
��}d'O� ���d��}�O� ������}�O� {��d��}�|x�u�R��'��ˇ����}�|x
���G�����}�rx
���,�'ð�}�rx
�Z�,�����}�rx
��,����}�rx2��,����{}�rx
���G�/����w}��\YaԺ>�OZ�m:��I+,j\l�'���o}�y��¢���>iF]��|��V��/Ԃ�����w>iF-�#�w|�
�:����5�8�I+[�>�nÑOZ�Q������n�g>iF��.|�
�z�<a����AlV����bA��`�o���K�?�^,�W����bA��`W������-�^=pz����N5�xu�]0�Z�j��`ж>��w��o}���A�z	eu0�\��'������Nz�<�\.��;_�\,�^�|�r���n_�|�r��}�������G{:��^�]0ha/��1����w{����]0�b�z�w���}�����>���{��w�6��]0�d�z�w���}�m��T&J?z�۷{*��Khn�'���}�~�����}���Sa��>���0hhpb�Tt�8�{
�Z�\�==���
���7vO�AW��;�� �����=F}���
����}�������}�+Z���ݮ�z���ݮlx?�f˾ەM��탭|�+����nW����ݮ��K�`�؇:���nW8}|Iڄ��
��G��M�N|���.�g����M��s������6���v������蝽�a������a������a������a����	�a��`�	�a��`�	�a��`�	�a��`�	�a��`��a�{����P�����a�����a������`������`������`������`�{!3���7���w�������1@�����u`����u`����u`����u`����u`����u`��9m����m���]Y�?XNە��%&NۅE����]Y�
?XI�����]a�?`I�����]`�?`I�����]a�?`I����]a�_b��]`�?`I�M�;P/]�;P��6�8@���6�8@Y�?��q� 3~��� �Ak���
���w�t�@s|���0���{Ga�?���� ?~����A����;�D�C���[�����G~U-��6�SY�\$�K�*���m�ښ'j��E`�|�O�w��ҩ����ڔ1���:�mn����n`�-OEY�vy�z�8 ��S���a�Jd&��T�5q n��z��p�<ՆM���c�5O�5Oed ¶��l���ϊ��a��,��`��,O�`�����ըw~ֲ��Q�<M�����m"|�>O�E���y���7�Ϫ�1v�S��D���~Ր�S����M ���s&�]��y�6��p}r���>9�y*�:��6�����^���0j�O��@ w�'�m ���gA����6��i��^0ꨧڰ7.�Z�R�z��<��FM�Tԫ�E]�i�F�,j����ɢp_=U�M��Ӟ�|�4꬧R�ɢpk=UM�{�g����Ff�(�]OEK�E��z���,
��S��dQ����o�r��T5wCB-�)��),챧ک�2j�O�	=q�}�O���h�I�gOEYM�F{��hv�S�F��՞ʋ6����T��$l��Z��&a���Ea�=U���W��fQ�=��R��{*q���-�T�3�Q�}JO䉛��g"O�uO�Sy�{*�,
��S�Ն���*�6L���T��a*l��2�Sa�=�!m�
��,d�T�}OUK���{*"�0��S����W��ba>�C�u���S�8؃�Vo��&|�dY_�]�Th�����e���SUֺ'lħ�uO؉O%\랰�
j�=q/>�{�{�f|������T6���S�?��~|*$�c6�SU�Eu�S��˰%�
��X�=��>?��|*ڠv�S-۞��-�*�6}�T��ļ2��0`g>�!m�[��c"ܛOUKa��|��w�S��D�=?͇Wg���� z��AU�nu�S���Sq���՛sèI?�b*#�K�j���]ԦO5�bXԧ�:?�P5�Sy�TFp�~v�VFp�>��Le��S]�TFp�>�Lew�S�TFp�>U�Le��S��TFp�>��Lew�Sy�TFp�>��Le���=r�25��=q��Q�~��/���=o��Q�~��/5��=m�ZԹ����F��T2ܻ����F��y�ҺZܽOQ����Ur�aؿO�S��~*P�P�G^:7[���e��=�T�5��&~*�p�G?�rM������e&`���O�_��F~*�ِw�S��Ƭ���jq6hŽ�TW6an����ֽﰛ���&����T�3q�秊��{pC?�M�������&v���O%D����r��^4�S�ф�?���zn��m�T/2쏾~�g�7�S}Ɇ�����6�ŭ�T��A,���k��yh����&-�0��ꪹhp{�*v�0���k��W�����勆�yOZ>�濚����I�{��|Ѱ�?����?���?��D��?yT�4l��{��E�N�\�4�V�g�P�����ٟ��&T���T4�*n��J��6p�?�m�����n���OF�p����ی��SAҦ����6��]�T!�In��r��:p�?U�lځ��<j���?O��ƭ�TM�/���T�����T{����T�����?�b����T3Q� �뚨� ��L��C���&j��@�65�����~� Pa�D?d ��l��: T�3Q�! �I���@��~��C) �`'C- �C1�U�a�@E����(�#޻� ��hҷ� TK7��E *W���$ ��M��C���&}�!
@�M���P�"�I�~�P=Ԥo?t�hhҷ� T?��)LL�� Tb���@���b7���Yo
����"a �\zc��7�Aa *szc���7�5�w_�
�y�^0��[0Ba *�c �Z]0Ba ��c ��^0Ba *�Fc (�    !��rH���4���zHs�5(�!��"a �-ҡ{�H����X���yP�J�����j鍁P������j��P�
�������P��������P�J�Ӿ:dI���v����|�x�X��\ͰH`�oo	���悑0���&#a�Y=3�Aa�Y6Ba ��c �r0Ba ��c �zs0Ba ��c ��i�EH��}ѰH����l(����"a �ww�& 0���H`�ƣ��0�,6�0�,���0�,L�0 %�������b��"f0Ba *"c ���}�ca�2��n	Pq�	Pm�>��0���:#a�Y�2�Aa�Yt5�Aa�Y�2�Aa�Y�5k���<���������V��E� T��E� ��v�H`և�uP`��yP`֒�}P`���Aa �;��P��DDX�j�&"�� T"4�z����0 �MD�����>([����0@�Z������`$P�V�e �[����0 �4M�����i{u|*Ca��v�~�H����,�n���Ba��v�~�H�:N�	T�i�#a��8m?`$0KZ�>(P=���������0@���0����F� �sڮpC� �s�~�H�����"$P���,��S��>$@��fX$0����H`��n	̊�1
���
��1
P�0�0 U1
Pi1�0 U̓1
��8�Q	����s�H�F[��� 5ښ'���<�0@��扅j�5O,P�S��� 5>5O,P�S��� 5=5O,P�S��� 5=5O,P�S��� 5=5O,P�S��� 5=5O,P�S��� 5ٚ'���<�0�H�l��j�5O,P�S��� 5?5O,P�S��� 5?5O,P�S��� 5?5O,P�S��� 5?5O,P�S��� �<5O,P�S��� �D`a�Z�	�0@-�X��b,PK�� ���@ �"5O��0@��X�Vo,P��<F� �F`a�Z��0@��X��b,P�V�#a�:�]	��M��js&��� �y�Eaa�ڂɢ�0@]��n	T*��0@�#���F� �
/�>(P��b�� �����P�R���*^��P�R���*^��P��h�v,P{2w;�=���^�ݎ�jBO,P�zba���MBa���MBa���MBa���MBa���MBa���MBa���MBa���MBa��I��0@s�$FX��n#,м3�hޛ�4�D�X��'��� �?�'h>�,
4_�0
4_�0
4ߞ0
4ߟ0
���0
���0
��0
��0
4z�bX$�B�'-h�½Y$�B�'-h�=���z�x�H��V�1
����� -��=�0@��qO(�H���Z��=�0@�ź'h�Z��� -6�X�E��h��䇅Z�?�aa���O~X�%��h������䇅Zz~��� -=?�aa�F�]��P��k#(�H���i�f�CH��c�j`$��K70h�#�70h9���\Lƅ�hhA6,h3μY$�r7h�ٌ��Me�Le�Me��Le��me�Rle��O%�� �4[�� �t[�� ��?S�� ���0@���1
4���0@���1
��me�=��2	�=����0@ۃ�/	�=����0@ۃ�/	�qә� h{0�#a��S_0h{0�#a�F�1�A��Z+&N�!�Z5��a�6���� �u��h�.ט��0@�G��F,���1�m��C�Q���4�>2Vba�F_��X���c%h���X��Z�OЊ�:���V(Н7a�a�>}�a(�]4��a�\t�m��C��bc�� �U�������0@w݆�?��w6~�!н��a����ba����ba����ba�N#���X����硍��\}�X���a(������ }ͮ�`(���ꋆ� }O�>i,���ꋆ� }ϯ�h(�C~�,�Cy,�C}",�C{B,�Cb,У{�,��i`a��T���5!a��d��fX��7��� =����M8�0@�oƁ�zzS,�ӛs`a��ޤ��fX��7��� =�y�ب�TM��C��f��� =u5��ٙ��0@��F?�z6j�!�s�Q�a����~��<��!���Z��0@��s�� =?���<��!���b��0@/ϓ�� ��e��;$������ �$����%���0@_!�MCa�^�M�~t
���X��nӷ� ��8c%�T�1Vz(Щ�c��� �
:�J,Щ�c��� ���+�0@�Ŧo?�:)����6����n���Y�=��82�-Lς���?��2<?z���s��̋ݙN���p�IL,��/�-6z��˾N1�52�k�Nz��6t����cnC�kOz�����k��snC�w7=�s:�+��>t��+8�P�>_t��+�iz�m6�]F>5�`8\6�0=n4p�L<a�����pyԜ�m������Mw��lz��1tx]d��e����"�P+��F��Ȧ��nC��E6=w�Mw.�&=_w:|�Ȣ��nC�w7=�w��+��������KV'=_x�tq_�E���#�w�����^p߮}���j��"���a��a�'�߮}���d�]���Oʦٵ��b��ϥ�KߚX��h�̮}�ٮ�Ѕ]���]>�+��A�w��Z�Ʈ�t��n��kt|Wp���kt~W�^nڳkt}Wp�a��V�Aأ�3[8��c��|�j��~��p{�qÅ�cgܻ���t��ǹ�qp��c�98�򱏓��〃n߄���sp���&,:pp��㛰��q�A��o¢�]�\3����t��_�AՄٺ�co��V��c�X����b��$���NG�G8�W�1�+G����a���N��>�jWC�ħ�A�w9����8ݜ�cӅO჎v��|
t��'t�S���~�SS�S������^�qn��w7��>������� ���]�MG0��+����A�w7�G������M����?�i9Z��v�3�2��z��cs-G�A��͙t����ˇ{ϭ,r�t�p�E��z,��p�E��z��ý-G�A�w�V9Z��+���)�A�w7=��)���S��Ϯ��϶������aP�j�iP���hT�j��iP�,���o���o�ڻ/���o��o�k����AA`Ы���AA`г���xP�l�aP���aP�l�aP���aP�궹7��mn�Z�o�,�j��iX� `6�j��iX����aA�����:�o�j��h\�]{J�������60*x�ٵF���¨ �]��K��J���wY\[iT�k+�
�Uqm�QA��&�-4,x�ŵ�F�Ҩ ��V�Է3[�
�G�F��
������ �QA��"q��*�����QA��&q�Ҩ �}�8@iT��I�4*��%�}D��J���Q� �QA��$q�Ҩ �C�8@iT����c�GA�	Z�$hU|��UaT�S��� ��SXaT�1�)�0*������ �c�SXiT�1�)�4.�X�V&Q-�
>69��F���J���ONNa�aA�'/��Ҩ �S�SXiT�)J��4*��$�P|��U7|ңUiT�I�V]nT�I�V�QA���/�F��hU|֣UiT�Y�V]nT�Y�V�QA�g=Z�F��hU|.�b)�
>ϳux*�x 0�y�^,*�<O֋E� _f5�bQ1��W�7�j    ��Z�	�R�/�p���˪\0*��
�� ��:��2�/�p°
�˪\0*��� �j ���������* ~
�߻�
 ���Ţ��י�_,J��l9�Y���:���Eɿ_O��F���+��`����R�F��o+�`�����Fy�o+�v�������Q�����/%������Q�����/�������a���lb�ܟ	������|j��,L��߮�,��{خ�,L�׫�ƹ~O��
�T�gve�a�����0��]Ya���Ʈ,0N�{gWVe���i0�$?8Ϯ�0�����7����}�+�2��ҾڕE	~��7����
_��!(��q���>��W��(���ծ0��w|�+�R��=_��{(�>�ծ0J샏|�+����_�
��>��W��(�������>x	<��9}�x
�R��%�e�!8��E	}�#aQ>B��AX�·��~�(�!I� 0J�C�1���!�F�|U"�Q&B��A`�ȇ�%b��!:��i|�^"�Qb��A`�ć%b��!&�F)|�|x�n�>D9<F	|�rx�2��=D9<F�{�rx*������T%�!��0��C��S���!��0��C��Sa���$���(o�H^$0J�C��爏�G�w��<=/��!���bQ��L�/��a���0J�C^i�	ô=䕶_0J�C^i���=䕶_0J�C^i���=䕶�0L�C^i���=䕶_0J�CYi���=���_0J�C�i���(me�����Pf�~�(mS
�fQ��L�/�������i������Fi{(+m�`������Fi{�+m�`������k�Q��J�/�������Q��J�/�������Q��J�O��a�ӟ��d��a�_,J�Ò�X���%/|�(mK^�bQ����	ô=ly�Fi{��������/��a�_0J�Ö>a���-/|�0m�����-/|�0m�a]��´��u�,L�{ZW��´}*��,L�{�W��!0m�u_������~�0m�}_�����ܾ�����}������¾�����}�0JۣK�j?`��G���~�(m���m��?���$�d���I�),Jۣ��SX�����,J���q�(m�>p� ,J���Y��G�$b���g�6����D��=�*��(m��I� 0Jۣ�1����Dô=/��(m�!H� 0J�c�1����D��=��y��6J�c(�	�����E��(m���Fi{]�"�Q����H`���ȇ���=� y�,3J�c��	�����E��=�,y��(m��H^$0J��lm��R������t�(i����BQ����O��q)^,��cZ��=����,��cZ��ɢt=����,��cZ��ɢd=���,��cZ��ɢT=����,��c^��ɢD=敨�,���Lծ-Fiz�@/e�1�,�DQ��L�O��1��DQ��J���Ez�+C?Y��Ǽ��E�y,+??Y��ǲ��E�y,+;?�%籬��dQn���O�汬��dQf���O%汬��`a^Kۮ[��=J�c��uEYy�n���()�s�����<ְ]WP��ǥx�0#�5��
��X3���(����
���X+���(����2��X;���(�ͱ�
�R��<���(�3q��%�q�Q�P��ǖ�m-(J��TܿP���V���}@Ixl�okaQ[��ZX������f���m-,L����Z���=�m-,L�{��ZX�}�ķ��0��okaa���>��ٟ�w���Q�yw��w��QP�w��l\(J�Ӝz�(�N.�sTP�t'�9*(ʹ�K|�
�R��2����wr��QaQ\�sTX�o'�����u>G�E�v��Qfa����sTX�k'����G�E�v�C aQ��<���(�N^�GaQ������(�N^�GaQ����¢;9�E)v
r<
�2��x��E	v
r<
����x��)��(,ʮS(�����ڄ(�m��uZ]B��봚�.��)���bQ���J�/e�)���a���J�/��)���Q���J�/e�)�,��Q���J�O��)�<��Q���J�/e�)�L��Q���J�/��i���n�d;��l_,ʶS���Ţt;��n_,ʷS���Ţ�;��p_;�2�V�}�(�Ni���r�W�}�(�Ny%����W�}�4J�S^i���;�w_0J�S^����;�y_0J�S^��	��;�?C,�l��|�ܷ++���T�veeQ���śE�w*a���(O%�+3�T���(O%�++�r�T
���(	O��++��p�T�@����ٕFyx��]Ya����ٕF�x��齁();ڕE����jW%��}�+���T_��!(O��ծ0��Sm|�+��T;_�
��<5�W��;�d[V��O�%Ҝ��G @r����,��IHm)�R����=����hW%�'>�FYyv�QZ�{�]a���^�i��_O�����x��<�������far>��1��|Dc���H�ƀ��=���a�>��1�����o����9�����G�>��1�,�<��1�4�<��p�^��o8Q/O�7���'�N��S|^s���W^���4��l�<�y*������|����E8a/A6O�Q�^B�yN�KH>/�9{	��E8i/���g�%T�ᴽ�F +h����.��e�O�,J�K|�8���^�`�^����i{�;m�`����_�`�^byMR�i{�;m�`����^s`�^b�Q�i{��5E��%=�!
0m/)�f(�������6J�KZi�Ţ����'�����������ɓ7�����k�L�K�)60m/i��������5���%�����_l`�^rz��i{��5���%������\_�k`�^r{��i���_3��?5�K;����R�3�NY���θ;eQ�^J<��Ei{)���	��R2ϺS���u�0J�K�<�Na����xН�(m/��;�a�^��1w
���ԇ��)���R�S�텇%����$â���$â���$â��Ƞ$�����d`���d`����d`���d`����d����"#����"����"����"�������9�����x´��������.�0m/��7���Uڝ80m/=�L�KO�� ����&�0J�K/�ƀ��ҫ�1ഽ��o8m/��N�K���G�>c�#m���H�G�7�?�����Ᏼ}dc�#m�1s8m�)s8m��E���5c}�ᴽ>�ϋp�^�<����D�ᴽ>��E8m�O�yN��S|^�����fˡ��f�\"�����t\�`.���	ng.���Q���y������m<�Ǚ4a��tP�]ָVGW���Ùai�y����7��@�!�P�AX��-xhr1��:�7�+����Ԃ˞sb��>�,\�
��3�ҝ���~F�z���rM�o(�g���3����9C@,ݘn�g^�����e��XK����K׷�X4e�Α�m��yy��.��"�?���9p;#@,]?j9��(:K�����8C=�k�� �5t��X�;�ΘK����k�
�������K���Y,�>Bj�}������+[Ι��W]��G����.�#��k� nG�����������Eo!AG��A�#�_]�!u������7�\C����<t����#��#�R��^�.�O���p=^7�`�x��+<v�����l���u��{�в��hz��=/���#�'���LWOG����Q�e�=
�����'tf�:��{���m�C�_�|��dG���N��x������<��5�x��e�5��GԈ�x��ǯ��;H��5t�����ǯ1v��Mg>~��\�!���x��-x���݃��Ǒ$2�C���o\�Y"�>�{{��C>Q����aY����Cj��aӁOTC׏(Y?r�����#J6��D5֎Q���'���ۂ�7��O��}f
�Lü7<��r�vܮ�oz��j-���.Wv��;�    Q�ezם=��q;R��n7tf:�o��5O7����]��9�	���Noƴh�`f������4|�?r�;�!i�����C����,����G�,�$9$���G�lZIC�w�lc'9$��Arh9$����~��2����m�C�!i������4_�2w�C����C�0�n!��-��1��#��rH:}ɦ�4t}�vM�C���$��C�;������4ty��rH��-��E���ׄ��mέ�м�*M~�J͛����g%ᙇ�ew5x<;��������T\�W�g���!/\6X�7���e�U��8{�q�1,\�X�'oG�e�5x�0d��|�||���痞o�i�?��]�?��\?̲}D����,�/��l�P��R>�r��;���G|<��l�>�k���j�ñ��:x�����+��K����1�_*N��痒���J��_In�s��*>>����;x�6x����贽��*�#`�x�6�������O�>�j���6^x�6x����0W����Y.m��R���ׇ�?�iv�����o��>�.�RCw�����Tqz�w?[�xX���K�/��	_?��K�����R�����~�1�D��K>>y���Ko��0��~��[:}��8i���[�0˦��G��ǇY��5>�f�.��^���<��k��_�.T
��8�W|m���k��a��#���q�C��Rc�������g����=V�����w.z������w��p����h�Q�'x���6.g���W��g�S�=~�Ƌ�-N��a��#�@�>�F�0����K`�Ow�Y/}�<��t������g�t:(M		i!���T����(��ų����Z�	�_�"�����z�7^��3�����k��aǃ��k��a�u�Y�����0 �l��Nv�.g�t�0˦#o�_ѵ|y�>�_�q�x-^>̲}y�x������k�>����Z<�q�4��k��aȃ���<=��@�ӽ��ׇ��4~~�O����tݾ>�M��O[�?_�\�Ĳ<���k�����f�>����0>�������G�Ì�G���1�;��}���Ð�)��)ѤG�~���4��XK[@qp�?��p����M~[p�񚆖^����SX-/����T��4�^��T���,]x��?c�6I�I�J��w�J7ji�n�u+"a��=9>zM�s�l�I�'�K7�~.�X8���[zd��D�k��6ȡw��M��A�k�L��ǷAM�c�.�:t����h"َ�R�����t��#o�����nG��G�Tz"����h����h���k�ћn�xhڗ����O�Ʀ����Ar������M"R��W�Ѝg�|t���YC4�mGVvd���`iuhn�H��#����e�Ǝ4�t/T�#-��ͣ���8R���Zi����4q��?:a-�T:~t&Zi���}�m|F�4�#�5|F�=�߸����4pz���i������3�����k����دN��4�GC�6v�3��&��3Ҹ�Oi���i菶�C�_���G�#���u<��������O<p�S����;X��O=c���;�t�S�Я�4�3�z��hڮ�O=C����g\����]��W]�.��g��Ý]����7��ڃ]#p������8Z�
�{��� �QZރgOGq�����"t�(��%���w���=*�.���Gs��vޕG4����Bb�?22�~�y������H��7��AG��O���EG�ڢ��r���?����w����F��r�)��$r�"瘺棁h[;��{Ct~����c�&aGnف�o�ߌ�_t
o�lx�z��9t�SO����,G�J�3���M�$����e�5����֠C9���h���U�ȏ~�C7���vh��S> ��|/$�մ�f�d'��<ӆ|/C`��(]�NJ/�%��(ݙ���ˎ��oG���z�?Og������t{�o=�^�����N�2����tz[��r�Qߴ��<�ˎ��y��g��(�>Bd;�eG��#��ˎ��#H6��5v|�vM�ˎ��$��ˎ��m�m쬗uMx��MNT��G��E��_H�_�����u���!,�<?���?Bd�9Q�n1��('���y��ml����j�$��r�*]�Ar�"'���m��������m�C79Q�.A�Yxk���HD���a�Z�f��Xp�U�6�{��G���Ѕ��i�ZݙB��h�Z�.K��r�в�:3�<-[����_!��ekU�~�m@�ek5tz[0�EK����m�Cs��x�y��'�Z��o���j��6�vd�Z���� ��Z���� �5Mj��5�m�CK����R�5Mj����:��Z�k�;�-�VC珐Z�l\�3���G�o:�����7nkw.�8�⁥Dg�������1�����5]Jt�N� 9���]?��e�.%:C���f�R�3��A�\ӥ�j��$�>��Q�b�Ǚv�՘M__��(�ٳǍ��v��Y�����zup�W����܀^��fX��M'�2�`X�΋�ك
��m�ՏM!�h���܁��o�x�ܒC��8��Pw+����%tP���6�a�9D\ߦX�/(����m��s��ķ)\�	j��E����Q��v�O������х\�o�����}��>v_8ǡ1\x���pNC�"��I��!��@����-��\A����J-�#�(�JK3�EZ �GEPg�>�5α����?��:%�)h`�����7�t�pdq4�YXʑ��Hg���s
�,,���h����#���� �44�Y�-�/�,P��U��@g�_4�YX�~�Pg������,��{�u�Y��3� �:��F�Pg����,ж} B����$�Y��/�,Ж�Wuh��:t~�Pg�N����,��| B��rF��4�Y�O$� �:d���,�>} B�����zGS��Y �E#�ڢ����}���Hga�J�EC��%,�uXY����񺡑��ҖL7�u��d�4�YX���Hg�N���th��F:�#�:�(��P�`���Hga�L���:t�FRPg�v蘽���}رy�,�1�O#�������t���`��@��C�,�3#���Y��(���:Kl���YXr�>����u�xk#���b�4�YX����Hga)������BY�䋅*��6�"��%;�8i,�줅����F
Kvr8	,�ƿ��0�WX����H^a�Nv#u�%;�,���8'�a����,nI+��碁����VX��.�����E�U�M6�(��
�GEPT�N��j*����rHR�����P�s	*ԇOCc7��@�NpA��"\A5:l��"(�@�YtQ�hC�.���}>�ETR g�%7~$��������}���PF��`�*
��Ti,�@���A5(��jh(�@���C��

���M��P@�Bx��	t�$�I,�P�K��܏7�x��C�:�ܢ�vm��-K'З\�a�:��@,�@�lt�u賎. �l��� VM�M<� Ģ	u���5ꪄ�0�L���C�ڍ���|��C׳�%��\Ha���)��@�]t!����.��VB=�W/J%�|�Bq�X)���HC���p0�IX{t�i,��>��h����;�$����4�H�mc;��P"�bs;��P!�Ζ�HCC���%9s?/��#K��Ucy���y7��^w�Pa}P.��6�:]�ai���� ����F�X��� uh��. �,B��4��t��u`Q���2�&��a\@aI�uCp����B
"��]Ha=�Z��4Ɔr�DRX�]Ha1�H�Bz�ń?�����R���`�����x�Xa}���Pa�Ow4�A���l�Bc�6�֪4A��<[��P�Ζ��*%֖����L�^Ｕ*��r���k�s����r��ց����.[    ��P��>��K�a] b�ڣ�@,|@Ar�ĺt��ecك�n�0T=�{|�CуsC���|E�r$,.����ڽ\ b��}D�
�3��r�; �F�X�6�����G�e?s�Z�|�H���v�P砝ɭ�E*+��"����׋�
k�7��(�苵,R7����W�Bmڀ�k�,R6��M�נ�A[�ėא�A[�/�>�,J��P�h��{�P�`�w�A5��y�A��(�w�A%��dPǀ>�xT1�:�A5hW�� �zOyY��U&�P�^p��E�kw��*���8P����xT-����Y@�U�8P��F�;p�^���J�&M��^��%�f�f�������4�z����Lg�`��R��0�P�r9w�֎z�pK%y�r�xO%u��`���=���J�P�p[�����2�W3�Q�f�ƚWzu�pg���8����\�ὕ�Ѹ�Û+��r!�wWCv!��Wj�r!�����y�n����ކ�;l����,�b˺��0�c�^�o���Ex����"��R��E�gI��E�hk^�Ý���F�E�0��Ջź04Vx8���D��>��*� >��´&�Si¬����Hf͚�7a�t��`$�f�9� =����S�!9����S�����s�)��`h�mHFZ04a9� �R047�(�J0k��"(�&�(�:0kv��"(�����C*04��-����Z�f�L�w�a$C�|�"� C�|��"( C�|��"��B�w��"������ETi��Bۡ�H������"헶�+���/�TW.)��S\�`$��Nm��P�����#ٗ~*+�T_�)�X� ї~�*�4_���%H�FͺE@��X�� _�.��0�{�����{���ET{�Q��E{�y��E�z����E�zY�q]A����^h��[1�y�y�n�P�f��`��B���8(�Bz��8��B���9(���#���H�� ���H��(������$�ɻ��W�^�E�.=��EY$��#_]���.=��Ea$�ң\]F�.=��E`(�ғ\]F�.=��Ea$�ғ\]��.=��Ea$�B�8�*��KO|uQ�=�wX��(�]z����HХ'��(��\z���ɹ�,W���K�ruQ���,W��r�9�A�0�r��$�݇�\������\z�?����s�Ra$��s�R����1���Ra$������Hå����H¥���z)�����T	��"���E�-���),�o�E6Of�zK/�y
��[�OJF�-���y`,�ҫn�#�^u�	�П�x�d������H��W�y*�T[z嚧�����5O���m!,�<�E�-�J�Sa��ҫ�<F�-�I�S=��Zz����H��7�y*��Zz���z���&5O��VKo\4S�!��޸h��CJ-�q�LY$�қ�F:-{������u�#]�f
#��ޥh�0i�]�f
#��ޥh�A-�K�Sa����q_|J=,Rh���OY����q�a�BK��>#����}
C��>�}F
-}���Z�`�)Z�`�)����30Rh高}�)��Qn�P����s�
-��.n�P�eE�8��2�͗�8��2�pJ#��AUrP�ePm؅ThT�t!Z�s�>�>��2�s��!���ϰH�e<|�)����>c7��2�}F
-#��g`��2�}F
-#��g<�ZF���H�e����$�-#���-H�e����H�e����H�ek�0gQ�`$0�fQ�`$0�fQ�`�0TfQ�`$0fQ�>��.ˈ��eYF�?��TYFl?�`(�2b��.	�4YF��%��$�H��uI<�YF����H�e�(]鱌��K�$�2R��.	��X�������q�FZ,#��[1�b��܂��HC�%0b���E�a9ȟ�F2,��..�
хa$��Bj�z��BJ��5|]i;˵ �A�1�E,#wN(���+#N(�E�+�<�P
��WF	�P2�Wa��H{e�S���Hze�S�t�Xye��ou0^��;�tWFi��,�dWF��X��2���"ѕQI(F�+�I(��Hre��iEPqeP��E\4�;;##��AE��`$�2j��R`��2V��[�ԸY��2v��#��AUDPieл+EPhe�IFPgeP�Ņ�YTu)��Hee��ow0Y4�;8i���Xl�{8,�X���Pce����Hce�G2J�����A2J���ʠ����Hce�$��Hce�[�x�Pce��oq0�X$���4VFo?�
��2z��E@����ϭj���HF)0�X#HF)FF+cD�(F+c��g`��2F��R��4V�(�Q
�4Vƨ�Q
�4V�h?�
��2F��E@��1�ϭi���y�'v+����@ce�|�h�L8I9N`��2�,�812�X�p�r��@ce�U�q��	��O�*�X���C��	��)0�X)O��S�4V&�(�PceҺ�
�4V&�[��HceҺ�25V&����HceҺ�
�4V&�Αi�����sˆ+e�톑�Jy��s��+��
=>r�(��&~d�&&�y�F+�β�
�?B��E�^16�X�t��Wh��2�&���Hce���5V&<~nPc�<�.p�Hce�Av`1�X�t�-Xh��2�${��Hce�Y6a���W��M3�\HA��IWن�F+�n�ݿ�d92�#�sY��ʄّ
#���dv��h�LX)4�X��8Ri��2iq��Hce��H���ʤőJ#��I�#�F+�G*=��JY�v�F+?�j��R�U��a��2��'���� ��|��k�|�rdI|�*�4V&��DU��Բ�i\ B��B�. ��ʤ��J#��I��D+�3Ra��R��g��Hce�rF����ʤ�Ti�LZ�H���ʤ�Tz|ɲH�3R���d�rF*�4V&-g���Â��5�c��	�֪0�X)O�U�4V&,[��PceҲ�*�4V&-[��HceҲ�
5V&-[��HceҲ�*�4V&-[��h�L��V57�X�0o�
#���t�ZF+�鲵*=�j���֪��_�i�Z�F+���U��R���U��4V&-[��HceҲ�*�4V&�9Yj�Lz�ܲ��Jy��F+?�h��2���i�L:�F�4�X�t�Xi��2�����#�]y#V���m�P��@��2�����Â���9��맹����3Y���d&�F��Lt��/�Lv��l���l^_�a�$�d�(�E���A"�� �m�{5,���"bY 0�=BĲ@���zxy�Lt���Z ]�C,
� &�G�X(L6�/Բ�m�툰ǆXGķ���X� L��oӲ�8�ia�� " �뻴,� (!R���~�4��~�P �(3/ Lt���'�׵���O��K�eA��d�Y4�O���e���پ�C����2dY��_B:N[�^���=Nc��O�8MQ��?Qv������4aQ��d�iʂ��ɲӔ-��e�	�:�'�NS4�O��i���_B>g�Z��O��i��n���,h��,�iʂ^����;nP��d�LSt�O��4eA��d�LSG��ɶ#2�iʂ6�Ɏ_������{����w/ ��O4�sJY��?�t�)5��l>甲����Sʂ����sN)��'��9���o�mG�KZ]#�������v}Š����!,��ldG�'��̢���fv������!,��leG0[@S�d۹0(z�'��PK�D��^��/a�R,
�'����O6��#,h�l�G���'���4�O���#,��l�GZ�'�΅AY��?������':~�jQ	�nb���'��t�O6�9%,h�l�sJ�z�'�����O��9%,���l�sJX��?���8��O���KYо?Y��P�[���/a�EDPм?Q��0�z�'*aA��d�"",�ܟ�\D�E��������o�r��OV."̢����EDXд?Y���'@�~�_D�b�e�|t�OT."����EDXЯ_VY��    �?Y����'+aA��d�""� ����r�1Z�';~�P�~����+@���{�O�Q�?fA��d��l0Х?�,���I�E�?fA��d��̂��69���C�]�?fA��d����1�������=ۣ��;��=*��'�ۣ��7��=2[�'�ۣ��3��=*�'�ۣ��/��=*��'˵GeAW~����k�)�����}�=�GEAK�D���,�ȟ,�����ڣ:��O�k�ʂv��r�QYЍ?Y�=�#@3�d���,��/1�2�:��O����b�����(hğ(���}���2��y����e,eA�d���,h,���=���2�:��O�k�ʂ�;���6�O���(꿟(;MP�~?��8MX�}?Yq���~��4aA��d�i��ɊӘE����	�'+N�ݗX�߽\�v?��W���'�bQ��D����2����>����~���?aA��d�~��v>����~���?aA�}���4qh��(�ib1�k?Q>����3MX�i?���ib0�h?Y9ӄ}���3MX�f?Y9ӄ]���3M��'+g����~nE�=�pP�b?Q�-��~��=

�'*ۣ���~��=2���'+ۣ���~��=
��'+�#���~��=
Z�'+ۣ���~n/�=��@c�Dy{낾����((h���l������(,h���l���S?Y��-����QX�Q?Y����~��=
�����pz ,h��l���E����{���~��w/�җ�q܋E���-���qA'�d+o�F��6�J�}�����
��';x+G�.�y�zx+4�O6��r�K)ꡟh<���O4�2�:�'���-�_aA��d+��Y�>?���t�O�����������y�zؿ�������s~��n������_�Q�7?��7
��'Z��t�O��Q)�M�m|T
z�'�����Ov�Q)���B��Q),h��l�RX�/?���c��O4���E���|�	��'[����^��V>����m|�	:�'���4�Ov��'�}�3�|�����@S�Y�%?ٸ��ظ�4�O4m�z�'���-�-��ʂ����`aQ��d�q���?~��8XY�?�q,,ꎟI�s�,h��l8V��O�jj2�?�s�)
:�'z5EAc�D�PS��O�55.h��,jʂ����,h��,jj\�?�H>Ԕ-��CMY�?�s���@C�Dϡ�(臟�9�E���C����7�~=�����xl������	O���8����;p`�~3Nh:;)n�O��Gfq|j�A
[>�Q������a���5��-��=m�a��������d����wz�{�I�a�;��?������^c�����^�Kƽ��6�^1n}O+7�X���v���������8�����!�����r�Cw�S?A�C7����x��y�7��9��N�
�*p�;�z����u�����;�
lw�������%j��7��G�w�^�����"��^��Q�;��Ay��7��縏ޟ.����Oق���sܧ,�r��O`��v�°�=?�>�a�{~�}���}
���}
����}
����'Y3����'Y6��=Ȳ��=��@����L�����L�p��Y��8�°�=��@�Ȱ�=��@�]��L�0ljߍ��`O;����[��5���No��C�����;�p?;=Tw�vvz?�(����n9�Q����l�������xGne�o����i���9��)lA��9��),�c���g�d��ӻݳy
���9��<�M���l��vz|6O�a;��?�����}�n6��k�{ɸ�z ���uzz~/w��,����y���p�w���;�p�:=�w���u�[�w���uz/��}���:�!���WK��?ܵNO��%�uz�}���ӳ�{��e�^�{��c�^��;�p�:�w��~uz���]�^��;�p�:�F�w��fuz�ý���vZ��~���[}?tw0�W��On7;��΃]@�:=�����Q�:�ަ���a�:=�sӨs����{Өw�ކ�'{Ѱ{�%�7{Ө� ��$�`���~ٰ��^<�U�.vj.���}�y�.�F����:�����Թ| �nvz2} �~vz�} vꋈ> aO;=ȏ> aW{�c�nG�������C��y
�a�۞Ϥ��F����
�͇���tӨ�=�aA7�z��tӨ�=�qA�kP�{>�n�|�r�`G�]H��ם^\������wx�����HC���<đJÞwzF|ih��N���#�����q�Ұ�)G:��wj�5�����u��wz �;�/ᜑ�F=���=$O�.xz]|D�>x�z>�`'<=��>�`/<�ݎ>�`7<�TDR���`s�D��a��_x��m�P~�+��$ٰ/���Q�3�ިR�7�^<GR�;���FRb�OO�)�!_v�����8tèG����.��K6��˩��4�/�hsӨW����E�n�r�67���˩��4�/�ps��̗S��������/���g?�9_v����'��9fh�=_N��Q�|9���ب����M��r*87����)�ܮA}���pnuҗ]Ĺ�z�ˮ��0�/��s�ӗSǹi�Q_N!�QO}9���ب���R�M���Z�9���F���sn�֗S͹����)��4�/E.;1��þ��(�z��C��]���eGi�g_�^v�F����eGh�k_�^v�F����eGi�o_�^v�����eGi�sOM~!����u��{zL�;�"�������>������R;������m}H�|z�}H�|z}H�.|��>�`>5���N|zd��{�O�/v�S�A����{�;��{�!{���t�!����n�!���|�!;��qv�!{�K�Zk>b؝_��ZF����9v�Ӄc��
�z��u��Z�F]���\j���}���Yj�B�N}��Z�ШW��K��iحO����*4�ק΄<�:��I|��D=�eh�U��x���V�Q�~��*4�ܧW����ݧ��мkP�>�叏�Q�>�!��Ө��6�@��O�� ����K�}��R��P'?u!�^���Z�Ш��޿��~~zx�w ;���o������1{u�ӻ��C
��Ӄ��C
v�� �������đ����Z�����u�Ҩß^��#��=�5�#�F]�5�#�F}�5�#����5�#�F��5�#�F��u������}[u��=*��Q�=��ou��3,��Q�=Ӣoנ��z�E�4���g^�M���zF߮A���L��i� @���B� 5��u@ j`�ˀ: �p>���J ����(��^_���j ��7���z �2?���� ��;���� ��8���� 5˟��j�a�P��K`�@ϔ���6��(��F� �Mzq4�XO����B �+p��4��Uj�?c)�t�����R�zM���j��X���[���^�y��h������������TXM > �n �9�> �r ��D�P;�:�@�@ړ�$����l� @�~�PC����E�"��Z�����z���*	���> �� =ۏ> �� ���> �� =4�> �� 5D�PS`�+�0�@7�T�s�0�����`�,P�?�`�-p�G;��^��h�/�^�7O#�z|�^WiP�{�i�2@O���J#�z��^W)PF�DZ�z�У��a�7��;|@AŁ�!�#
j��>��� 5�DRPw��b��F���=6O#��LI���� �6�؆���އ6T XM/>���z��#
����>���z��c
*л��c
j���X������i�G@��S�4R$h{���g��I������m���a�K����F�팞�h�M����F�팞�i�O���鋆
팞�i�Q����F*-�!i�t
蕿_T*�g�~P�`���
H�`5���z    �	�)�X@�%ч�,���C
�����C
�P�J�!�Z�C�i�ȇ���zA�|H���
g>�`В��F-�!ih�bВ��F:-�!i���Z�C��Hˠ%9$��Z�5�5{�O=��xoU+�ě���5�4N�s˲���,۫�Xנe�_���A˲�j�,;��Xݠe�b�Z�=��P�ѣ5�S�q�r���c����ϯ�����±�A+��e�C��V"���GP��������A+�wf�CŃF#�|8b̓F��>��A#Y`�X�����k�����_
�>h����`��Vo�ƊP����;���B���h�C�FE_X�� %_X�Q���VBh��6mp���*�TƧ����*���P�5v���"Bk�Rű&Bk�R��~���KuZ�*��Z�j#�&.58TGhM\jp��������P!��QK75ڞ�t�P%���K7uڙ�t�*%�3v�ơVB;��n�%�3|�6:�Khg�ҍ�;n�L75Z����&��g�����V|�*'�!g��"�NhC�R�C��6�,58�OhC�R�C�6�,5F�
m�Yjp��І���:
m����Y�TRh�7^CC-����kh����x�z
�������� �q�x5�#��XU�?���*�G6^�Ce����kp����x�١�Bx�54�W�7^CC��d�58�X�A6^�#��Ѓl��:=��kp��Ѓl���Pk��x�?�]d�58�[��sB���B_�/gv���i��t���6~~�Xw��(�%��Py��$�%������/t��pP�S�ȇ#V`�4�ɇ#�`�ˣ	š
C���W�F@(�`�i�2$�_�i��R�/��Gv)/��'v�.t��E��B'9�q�.t��/�-t�T,
�z�ú�tzڳ��Dz^cM����B�k���*�W�y51$�V�y�41(V�t���
�*t��]a%:U~���z
�d3^a�z��L����B'i�+���B/k��q�P�{ԓ1P�{ғ!��7m��=�ĠH:�Ӗr�
�M�g̓A�hB?S����dP$��ό'c���ψ'�"���'<Q�ޚ��gͷ�O�%߾�<]0���3��a���IO����~f=]t���l�iO����{�4.��3��a����OK����&+������a���5��a���5��a�����a���A��y{�#N.�x;��q�K���1'��a���ܱ@\��}�:�hX��Z}o��n��,��au�w�s���n��|��v���hX���{���NeR��۩J�B
�u;	���e�>/HɅ���!���_E�>đÚn�H�aI�u$Ӹ���x�}�#����>ԑL�rn�H�a5�u�а�;u�а�;��s�ƥ��ğ[5��'�ܢq!w<YNT�awPeʅ.㎧ʉ*4�⎧ɉ*4,⎧ˉ*Ɔ5�A<. q	w�GNT�aw9#ŀ��;������rF
˷#�)����oG�3RhX�A�H�a�v=#�ذr;���B���zF
�c	�{�܆k>�}w���5�coP�y�0�<L�tpg8��������ț΁����HGg��Ыwp��HG7����#o�<L����ˑ�N�~���jh�����jUK�ę;�i}�ں\߿q[{�js�x���3�;���������5i����� 9�:#���;H�������C�3���#H�k�:#o�>A�iZ�3w�&���tF:8[�\0��no�l��ۯ3��6ȡ����6�vM^g���� �^g���;���H����l��3���R�.�tt���Ȳ��9�����t<n/����x�M����v�yq������..�v�WB|92�N�Q*��Й����n7tc�y��ە���đqw�S=���������m��i8Hi�{z�-x������HvdM?��Z߿q���_F�x��'�1v|��m�Z�D5ty��C7>Q�?�d���'�1_��M>Q�?�d������m��
dR�y[����R��Gns�������~�y��>Q�>���V�D5t��6��D5���m��'�1v��M>Q]���Ʀ/ۇT�o�o�i��:ⱅ��i��pt���0�=̷Vw�����*4=?H�k��+��rk5tf�zZn��nLG���G�[�ҁ� �Zn��No��?�.�VC׷7=��j�����j̝�?r�{�����̷V��Y�rk5Ǝ1�i���|ɦ��j����!�Vc���C˭���m�cl����Ht}��*-6�����j��I��;a��8|6q�|k5p��j6,�Vc��#�Ƣ��j��Q��[���� �I˭���R��[�qM|�ԡ��j��6�vd�[���ۂ��[�q{x[p�}	�H��z0�5P�bA�`�k��ł��d�M�Q�`�{�������=�A�`�{���Qݠ��Mr0(Ե��`P5����0(LxMV��j�]�U/�&�f�^,�Lv�on#�����t���	����r���|��n�Z0�u��aP,��~��V0�=b�¨T0�5c�23�Lv�X�XP(�욱z��N0ٕ0�Fe�	���^���"��W�xàF0�,�%���+�0�Lx��7
�3V/_������5���&����Ձɖ�keAq`��}-0�L�����	w�� 20����S�� .0���V�&��
�����ϭ&�n�&0��sk@%��V>(�Ƞ"0���  0��� 0������S>(Հ	>(ŀ	G>(F��	��[*L6��"P%`���ր
��|P*� n|P*� �|P*� |P��A�>��RaP�p��RaP��y6"��0�s�4,( L��<�����S`��O�o������i`��O�o�
���|�40H�����i`��O�o������Ә���=7OÂ����iX��O�o��� �0�<r�	���� �0�<��@�?a�y$��|�40���<���87Ocf��s�4,L�ǹy���o���0�|�40���<��7O�<���x���ᛧ�Q��y%��ᛧ�5��ÚE�czH�w����͢?,ٛ�E9~x����0�Ͼ�\0��ó�.�r��쫋���C�W�F9~��r�(�a_].��a�Q�º�\,��� �Y���Ls��!���?�}u�`�ㇰ�.��P�⾺\0��C�W�F9~�{:��a����.3�?ĕ�],��C\y�Ţ?ĝ�]FF9~�;�`�ㇸ�F9~�;�`�ㇴ��#(�i�}�r��v�w�(�i�Ĺ|�r���Uփ���?��}mX��T���r�����0��a'�?�q|m`�㯁��a����r�@
���(�$`��r|j�v��9~���s���ϭ��!�sP#�?�N���!�sP���_��"�����(�9~ �E��U]\�?��s��9~(��s�P�ϭ����sP���vD0�e����(�TuqQs�@UE0����(�9~�|��_k�?T�y*�r�P��,��C��'�8�Un�
�?T�y*�r�P��)0��C����(�Mn�
�?4�y*�r����fF9~h|�T�����SY��&7O52��C����(�Mn�
�?4�y��P���<F9~�r�T�����S`���7O53��C盧�(��o�ʢ?t�y��Q���<F9~�r�T�����Sa��!7O����<�9�����0�r�T_������5�+�_�.f�Kb�Ba��{M,�����-����طˢ�>>��bX���g�Y,�r���+�eQj�}c�,���:Z.��>>�bQ���g]W,����;.㢬~?�Y���g�U,�r��U���1웊eQFþ�X%�1��|���|+ǳ�E�|+ų(��cX�EQ2�N�qQ.���,�R�vzgY��Ǹ�;ˢD>Ɲ�YG�<>Ɲ�Y��1��β(��q��FI|ܓ�&��?r���DY��qO��(���eX l  ���3!ʲ(�g>�eQ��t(���=��P�E�{<��,�r�x�BY��qO���E�{�3�,���'BY���̃��Ei{<Ӡ,���xfAY%��L��C9{<s�,�R�x�@Ye��̀2,L�� e͋����?Y��qO�C�z<��,�����¢\=ҵ����3��e���|�,J���dY���·�����·GAQ��EIz,r{<,��c�ۣ�(E�En�¢=V�=2t:E��#,��c�ۣ�(=�Un�¢�<V�=�yQr+�E�y�|{����Q��2�X��(,J�c�ۣ�(/�Mn�b0���&�GaQV���EI9}_����ƷG1/J�c�ۣ�(#��o����<6�=�qQ>���E�xlr{e���QX���.�Gq��c�ۣ�(�]n�¢L<v�=��Q";���4���T<vOi`�����)���E(n&��xJC��<�GTq��I�ؗ���i�����4L�GI\�aj>d<��ar>d<����C�S*�!�)�)��"C�ƆI���J�4==���+���� W�T==Q$ȕF�zz�(�+�����xJ�aO��F){zd<��(iO���T����">��F�{zt<��(uO��q�Q򞂎�T��)DWנ>�$��J�>O�4J�S���v�Ƨ�S�`YU�ȧ�3��R�x���Q2���PS���_��(�OQf���$k��aR���=���e���Qb���P34J�S�9���(�O����)�Q�?E�3j��R�D�Q0�_Ò}H�4?Q�ŇL�U\|H�T?Q�ŇL�S�9�J�t?��&on��5S��(�Ok�3J�S�/�4J��j��4J�]�|H��?QƇL�Ua|H��?QƇ, �̷ְ�%�,�,C�FE��e��¨���Z�ƅ����jhT
HEn��FŀT�֪4,�"�VC��@*rk54*	�"�VC��@*|k5�Fe�T��j`TH�o�F��T��j����ȭ�Ш<���Z�
�ʭ՘�R�[��Q� U���	HB(�����T��j̍J$�=�������\���Z��Q�`I��� ��t��`�`�g�� �H�>� ���F�> a�Թ�@X< �����������^���      �      x���[�$˒%���\ ��c��=na[���X�T��lMԡK?�{i<.����.����=]�������;��������~���~������������z�:z�Z�F����{=�=������k���x����+������|��S��I_�|�L�:��u��!�o����|��u�����q����y��9��xϯA^�{~�:�˩ϯu��� ���\y�心}����}[�:��m���^���u��礯���&�~����t�������?�__����|;U����o��R�}�_�L�[��I��5�����o=ߧ��Y���g�ϋ���|?/ζ����8��^��yq�}������u��g���~^��������u��7g���~^�m_�{~O�:��i���^Γ���r�:�����~A\־�w{v}������:�����u������|��g���^ޓ���z�u��������>o������y�~�>^���ϟ��'�����������|�����T�����ߓ��~{�w}���>o�W���>��:��}����߾K_绽ϻ��w{�w}���>��:��}��u������|��y���n�������y�U绽ϛ�]绽ϻ��w{�w}���>��:��}��u������|�����u�X?��|��y���n������]���������>�T���ꯓ�R��|��=��Z�}�ߪL�{��I���5�տ'�+��~��:�������~>������~n�:������|?�綯����۾�w�AE���n?���:��G���������n?���:��]_�����/u�ۏ+����r��u��ۤ�������_־���~n�:������|?�綯����۾���~n�:������O���������~�����>o�K��I��6�o��'���ǤT�����_��U�{ҿ���]��=O��V�{�����<9�[��yr��:��}��zx�y��:o�:��m��u��˼��l�wy���n�r��u�ۛ���`������_�:��E��u�ۋ���`�y���n/򮯣�^�]_g��Ȼ�w{�_���_D��:��E������������/��R�u�_��M�[��I��s����t]������������_��U�{ҿ�o/���n/殯��^�]_绽��������^�]_绽����w{3w}���j��:�����u��˹��|��U�_��z�����`������`���n�箯����]_绽����w{?w}����|_~���ԫ���~n�:������|?����~>h��~���}����/��Z�������~���^�������'���ǤV�����_��]�������굽������y��u��u���y���~^�]^g�y�wy��U��u��7y���~^���O��5�s��ǻ����o�s���-��u���x�ש~��]^��y�wy���~�x�}����u��7x���no𮯓���]_G�����Rg�����w{�_�_���������_&���뤿W�����Y�cҿ��|?\��׵W���Q�����������|�Wx���n����^�]_绽�/�������n������]_绽Ȼ��w{�7���w{�w}���.��:��e~���k_绽ͻ��w{�w}���>��:��}��u������|��y���n�����u���{��:��}����y�O���ϯ���������?�ן��������޷e���?�L�W��I���5��� �׿'�9��}��u>��e��y}ޗ����'��Ϗ:������<?�˶����/۾���l�:������|?�ˮ��~ޗ�������Y��y_�}���}��u��ˤ��\'}���6��|/�I4���? 4��"Բ�i������N+� x�Ҋ< ��"��(���!F+҃�H;`�"=��ъ��F+҃����7h�ъ��!B+�C��H"�"9\�Њ��B+��H; �"y�Ҋ�p!F+���	0�hEz���AC�V�1Z�t�hEz����)n���7=h�MqӃ����3h�ъ��!F+҃��H:`�"=�ъ��F+҃�Hb�"=h�ъ��!F+��b�"=h�ъ��!F+҃�H:`�"=�ъ��F+҃��Hb�"=h�ъ�����Hb�"=h�ъ�u�J+���*�H:0�A&��+���}~����}~���o�!F+҃��Hb�"=h�ъ��F+҃�H:`�"=�ъ��!F+҃����;h�ъ��!F+҃��Hb�"=�ъ��F+҃�H:`�"=h�ъ��!F+��b�"=h�ъ��!F+҃��H_竴"=�ъ��F+҃�H_�c��AC�V��4�hEz���AC�V�1Z�t�hEz���A�V�0Z�4�hEz��{�1Z�4�hEz���AC�V�0Z�t�hEz���A�V�1Z�4�h�޿AC�V�1Z�4�hEz����|�V���UZ�t����Alz�t����k���⯿�@C�V�1Z�4�hEz���AC�V��0Z�x`p�����|/'����^N�!��=���H"�"9`�Њ�p!B+����H< �"9�Њ�F+҃�Hb�b�/�!F+҃��Hb�"=h�ъ��!F+���*�H:`�"=�ъ��F+���Xi��_AC�V�1Z�4�hEz���AC�V�0Z�t�hEz���A�V�1Z��7���AC�V�1Z�4�hEz���A�V�0Z�t�hEz���AC�V��4�hEz���AC�V�1Z�4�hE�:_���|�V���F+҃���G��V�1Z�4�hEz���AC�V�1Z��@hEr ���V$ Z��O����B�V$Z�,DhEr����B�V$Z�z@hEr���a�V��,�hEz���C�V��1Z�4�hEz����l�V���UZ�t`ԃ���7�ъ�5MVZ�4�hEz���AC�V�1Z�4�hEz���A�V�0Z��_O�F+҃��Hb�"=h�ъ��!F+҃��Hb�"=�ъ��F+҃���3(�ъ��!F+҃��Hb�"=h�ъ��!��sP�������V��A���sP��?��Zp�ן����V��A;�V��0Z���hEzP��AC�V�0Z�t�hEz��{0Z�4�hEz���AC�V�1Z�4�hEz����|�V���UZ���Wi����|�V�1Z�4�hEz�Z�����?�w��=�����6z ����̏��?~�� �5y���ߦ���-^^k���i�c�z��X _&=��I��m�c�|��X o�����@~Nz,�_���o����B�4�P=Oz,�/������-qz�=ȷI��}�c����X ?'=ȯA���=��|?�^��[�׾����m������I��u�c�|��X �'=��}?�����zz,�����^�m��{�c�z��X��'=ȗI���|o�_���c�|��X �'=ȏI��s���������@~Oz,T?������ //?Oz,�/�����6�@�Oz,�����^������v��5�@~O�:��}��X��'=./�Lz,������^������>�@~Lz,�����o����=�P=Mz,T?�{nz,�/�����6�@�Oz,���������y�����c����X��&=��I��e�c�|��X �&=ț��W��`z,�M\������_�@6q�d���|�&.���^M\��B�D^z\^n"/=�&��c�l"/=�&��c�l�z�qy�
��X o"����^{,�ߓ#�Ӥ�J�<�@�Lz,����ۤ�yy_�������D��w,����פ��=�1T=MzUϓ3�M����n���:�1D�Mz��C�Ǡ`����"�&=�ȟ�]>_=־�w{�w=���I�;�/�C����6�1D�Oz����}��w�o����5�1D~Oz    UO�C����2�1D�I�__���~��I�!�}�c�����|
w=�ȯI�!�{�c��������c�z��"_&=���I�!�m�c�|��"?=�0� ��������7�ȯI�!�{���n����|7�����z�a~��"��_�>��1D�Mz��}����z���C�פ��=�1X���Ua[�1C�r̐��3�� �Y0ޞ�s�x�1C�D��7����!�&=���I���i�c�z���"_&=���I�5�&z�>X=k�9�}��*��E���#?'=�ȯI�=�{���n/����T��2����2�G�Nz�o��W��'=�ȏI�=�&>���5��5ȱF~r�UO�[�� ��2ȱD�r��{�9�0�3d�ɱB~r��_��� �DUOr,V?�z�~Þ��ˤ��:�@��;��I��c�c����X ��?=ȶ�O_�{��z,bm�����?=˶�O��m���m����^{,��������5�x~Oz,bO�������s���O ?=��I���m��������1�x~Nz,�_���m
������e�I���ϓ��ˤ���:�x����I���g�����Y{,�����&=.�O�:������|?���~?���>o�W��B����O���:��U���&������T����ꟓ�V�k�߫�|?<������7a��u��0��:�M�t}��&L���|7a��u��0��:�m��=�[��z��|�iw���n�����]_绽ϻ��w{�w}���>�zX����� oKсp]��<���Et��x *ED�Jс�R�Ez��cсb�Xt��X8����cс"�Xt��H9�!R�Ez��cс"�Xt��H9�!�Ez��cсb�XtT�;ӏEz��cс"�Xt��H9�!R�Ez��cс"�Xt���с��WDz��I�;ӏEz��cсw���)Ǣ=Dʱ�@�r,:�C���)Ǣ=�±�@�p,:��C,��Ǣ=Dʱ�@�r,:�C���)Ǣ=Dʱ�@�r,:�C,��Ǣ�zޙ~,:�C,��)Ǣ=Dʱ�@�r,:�C���)Ǣ=Dʱ�@_�+�=�±訞w���Ǣ=�L?�!R�Ez��cс"�Xt��H9�!R�Ez��cсb�XtT�;ӏEz��cс"�Xt��H9�!R�Ez��cс"�Xt��H9�!�Ez��c�Q=�L?�!�Ez��cс"�Xt��H9�!R�Ez��cс"�Xt���с��WDG�'�L?�!�Ez��cсw���)Ǣ=Dʱ�@�r,:�C���)Ǣ=�±訞w���Ǣ=�±�@�r,:�C���)Ǣ=Dʱ�@�r,:�C���Ǣ�z^�~,:�C,��Ǣ=Dʱ�@�r,:�C���)Ǣ=Dʱ�@�r,:����訞W���Ǣ=�±�@��ӏEz��cс"�Xt��H9�!R�Ez��cсb�XtTϫӏEz��cсb�Xt��H9�!R�Ez��cс"�Xt��H9�!R�Ez��c�Q�b�Xt��X8�!�Ez��cс"�Xt��H9�!R�Ez��cс"�Xt����Q���WDz��Q�p,:�C,���:�Xt��H9�!R�Ez��cс"�Xt��H9����Cсn�Pt [8ȡErh�Cс�Pt �F9ȡQ�Ezp�cс�XtT�ԏEz��cсb�Xt��X8�!R�Ez��cс"�Xt��H9�!R�Ez��c�Q���WD�:_�!F=�±�@�p,:���cс"�Xt��H9�!REr��Cё�u�G9��Er��Cсn�Pt �[8ȡQErh�Cс�Pt �F9ȡQEG�5�Xt Y8�!�Ez��cсb�Xt�H9�!R�Ez��cс"�Xt��H9�_ R�E�:^�!�Ez��cсb�Xt���Ǣ=Dʱ�@�r,:�C���)Ǣ�z��.�=�±�@�p,:�C,��Ǣ=Dʱ�@�r,:�C���)Ǣ=Dʱ�w���@�p,:�C,��Ǣ=�±�@�r,:�C���)Ǣ=Dʱ�@�r,:��C���u�":�����@�0�!�Ez��cс��":�C���)Ǣ=Dʱ���r,:�C���Ǣ=�±�@�p,:�C,��)Ǣ=Dʱ�@�r,:�C����)Ǣ=Dʱ�@_�+�}����u�":��*��8��*����*:� ~����< ���c����#@����P)&:�C,��H�`�#=Ă���&:�C���H�b�#=D����C���H�b�#=D����&:�C,��H�`�#=Ă���)&:�C���H�b��� RLt��H1ё"�DG�:_��|Ut��X�&:�C,��H�ߡa�#=D����?C���H�b�#=D����)&:�C,��H�`�#=Ă���&:�C���H�b�c�/)&:�C���H�b�#=D����&:�C,��H�`�#=Ă���)&:�C�����+D����)&:�C���H�b�#}�����&:�C,��H�`�#=~�����)&:���b�#=D����)&:�C���H�`�#=Ă���&:�C,��H�b�#=D�����C���H�b�#=D����)&:�C,��H�`�#=Ă���&:�C���H�b�c�)&:�C���H�b�#=D����u�*:�����H�`�#=Ă���&:��wh����'D����)&:�C���H�b�#=D����&:�C,��H�`�#=Ă���)&:���b�#=D����)&:�C���H�b�#=Ă���&:�C,��H�`�#=D����C���H�b�#=D����)&:�C���H_竢#=Ă���&:�C,��H�ߡa�c�o�D����)&:�C���H�b�#=D����&:�C,��H�`�#=Ă���)&:���b�#=D����)&:�C���H�b�#=Ă���&:�C,��H�`�#=D�����@���H�b�#=D����)&:�C���H_竢#}������b�DGz�{�᳊��)&:�C���H�b�#=D����)&:��-��H� �#9؂���P":���"�#94�����(":�C���H�b�#=<����&:�C,��H�`�#=Ă����C���H�b�#=D����)&:�C���H�b�#}�����u�*:�C,�z�{��Y0ё�C�DGz��!RLt��Hё EDGrx��Dt$�[ёnADǞ?�Dt$�FёEDGrhɡQDt$�FёEDGr��!Lt��X0ѱ�/��RLt��H1ё"�DGz��!RLt��H1ё��WEGz��!Lt��b�DGz��!RLt��H1ё"�DGz��!RLt��X0ёb�DGz����`�#=D����)&:�C���H�b�#=D����)&:�C,��H�`�#=Ă���?C,��H�b�#=D����)&:�C���H�b�#=D����u�*:�����H�0�/&:�C,��H�ߡa�#=D����)&:�C���H�b�#=D����&:�C,����+Ă���&:�C���H�b�#=D����)&:�C���H�b�#=~�����u�*:��V竢#=�����h.��:+��+�X�+���+�س+��+�X���mt$�c�nd��h�� I_�|5���Q�ų�������Hz,ڷ���O�E�}�c�nb$=�&F�c�nbd�qG���Hz,�M����y����y�=ϗI�E����X��I�E����X��I�E����X�o��g��?�hOz,�M�����Hz,�M��Ǣ�Hz,�M��Ǣ��@=�h7��v �h7��/,�M���� I�ų	��X<oB���[{��o$=�&@�c�n$=�&@�c�n$=�&@�w�o    ����g �x6��g �h7��v �h7��������X�� I�E�	���r¢�Hz,�M�����Hz,�M��Ǣ�s�b!=�&@�c�n$=�&@�c�n$=�&@���E�	��u����@,��� I�������Hz��ob$=�&F�c�nb$=���������'#�h71��g#�x61��g#�h�|�>�h�Mz,�M��Ǣ��Hz,�M����v#�h71�����@,����Hz,�M��Ǣ�Hz,�M��Ǣ�Hz,�M��Ǣ�s�ba�oX�� I�E�	��u�W �x6��g �q�	��X���@,�Ǣ�Hz,�M��Ǣ���߱h7��v �x6��������X<� I�E�	��X�� I�E�	��X�� I�E�	�������@,�Ǣ�Hz,�M�����Hz,�M��Ǣ�Hz,�M��Ǣ}�y�����Ǣ�Hz,�M����v �h7����j$}���Hz,�?C�����ˤ��&F�c�nb$=�&F�c�nbd�qU���Hz,�?C�����������Hz�M������Hz,�M��Ǥ��Hzl�?�{ �c������lb$=f�&F�c�nb$=��&F�c�l$=�ϟ�=�1l7��v �1l7��v ���a�	���� I�a��|������^�$�ӽ
 I���$��Y�Hr��/^$9&��E�c���F��K,�=�k�^�]�E����X��I�E�����>�I�񳉑�X?�@,�Ǩ�:�j71��v#{���&F�c�nb$=��&F�c��٣����?�&=��&@�c�l$=��&@�c�n$=��&@���}�g��o���v ɱn ��v ��l�@�c�, $9��@��*���~@��9v�@�c�. d�qO�I Hr���$Ǧ] HrL��$���s�b!=�&@�c�l$=�&@�c�n$=�&@���E�	��X��@,�Ǣ�Hz,�M�����Hz,�M�����Hz,�M��Ǣ�s�b!=�&@���E�	��X�� I�E�	��X�� I_�{5���w3�b!=ϣ�g#�q����X���{��~21��v#�hߦ�ߟ�z,�_��v#�x61��g#�x61��v#�h�L=��޿�h�Oz,�M��Ǣ��Hz,�M��Ǣ��H�:ߋ���u�ۂ|�8����^L��Ǣ�Hz,�M����v ��|/&@��w,� I�߱�9����c�H�:߫	����^��O�?�7��U�	����M��������T�@,�?Wo$��z �՛ I��H�{��	��u�g �aTt\�`Tt�`���
.:� T���< ���#@����P)&:����H_&EEG�:f�!F=Ă�����*:��~�!RLt��H1ё"�DGz��!RLt��X0ёb�DGz�{�;�Ut��H1ё"�DGz��!RLt��H1ё"�DGz��!Lt��X0ѱ�7��!RLt��H1ё"�DGz��!RLt��H1ё��WEGz��!Lt��b�DGz��o�#=D����)&:�C���H�b�#=D����&:�C,��H�`�c�qG����)&:�C���H�b�#=D����)&:�C���H�`�#=Ă���&:��	�`�#=D����)&:�C���H�b�#=D����)&:�����H_竢#=�¤��'�!Lt���&:�C���H�b�#=D����)&:�C���H�`�#=Ă�����*:�C,��H�b�#=D����)&:�C���H�b�#=D����&:�C,�����'��!Lt��H1ё"�DGz��!RLt��H1ё"�DG�:_�!Lt�=�h?��H�`�#=��7ё"�DGz��!RLt��H1ё"�DGz��!Lt�=�hWёb�DGz��!RLt��H1ё"�DGz��!RLt��X0ёb�D��_!Lt��X0ё"�DGz��!RLt��H1ё"�DGz���|Ut���Uѱ���]EGz��!Lt���&:�C���H�b�#=D����)&:�C���H�`�c��&:�C,��H�`�#=D����)&:�C���H�b�#=D����)&:�C,����Ă���&:�C,��H�b�#=D����)&:�C���H�b�#=D����u�&:��Y�k�#9��$ZёfADGr\�/�#94�����(&:�ã��H�b�#=D����Ǎ�*:�C,��H�`�#=Ă���)&:�C���H�b�#=D����)&:�C�����7Ă��� &:��,��H�`�#=D����)&:�C���H�"�#9<�����(":��ǩ��DGr���Dt$�[ё7��H�"�#94�����(":�C���H�"�c�qG����&:�C,��H�`�#=D����)&:�C���H�b�#=D����)&:���`�#=Ă���&:�C,��H�b�#=D����)&:�C���H�b�#=D���������H_竢#=�¨�X0ёb�DGz��o�#=D����)&:�C���H�b�c�qG����&:�C,��H�`�#=Ă���)&:�C���H�b�#=D����)&:���b�#=Ă���&:�C,��H�`�#=D����)&:�C���H�b�#=D����@���H_竢#=Ă���&:�C,��H_bAEGz��!RLt��H1ё"�D��?!RLt�/���#}������
*:n��
*:� ����<�ߠ��#@����P)*:� T�����T����@)&:җIQё��YEGz��Q�`�#=Ă����&:�C���H�b�c��)&:�C���H�b�#=Ă���&:�C,��H�`�#=D����)&:�C�����'��!RLt��H1ёb�DGz��!Lt��X0ё"�DGz��!RLt��"�DGz��!RLt���Uёb�DGz��!Lt����0ё"�DGz�{�H1ё"�DGz��!Lt��X0ёb�DGz��!RLt��H1ё"�D��_!RLt��H1ё"�DGz��!Lt��X0ёb�DGz��!RLt��H1ѱ�7��!RLt��H1ё��WEG�:_�!F=Ă���&:��wh��H�b�c��)&:�C���H�b�#=D����&:�C,��H�`�#=Ă���)&:�C�����D����)&:�C���H�b�#=Ă���&:�C,��H�`�#=D����)&:��	�b�#=D����)&:�C���H_竢#=Ă���&:�C,��H�ߡa�#=D����A���H�b�#=D����)&:�C,��H�`�#=Ă���&:�C���H�b�c��)&:�C���H�b�#=D����&:�C,��H�`�#=Ă���)&:�C�����_'��!RLt��H1ё"�DG�:_��|Ut��X0ёb�DGz���;4Lt��"�DGz��!RLt��H1ё"�DGz��!Lt��X0ёb�DGz�{�H1ё"�DGz��!RLt��H1ёb�DGz��!Lt��X0ё"�D��_!RLt��H1ё"�DGz��!RDt$��5ё�N�DGr��I� �#9̂��=����$�#94�����(&:�ã��H�b�#=D����&:�C,��H�`�#=Ă����C���H�b�#=D����)&:�C���H�b�#=Ă��� &:��,��H�`�c�)&:�C���H�"�#9@�����(":�����H^gk�#9܂���p":��-����g�O":�C���H�"�#94�����(":�C���H�`�#=Ă���&:�C,����D����)&:�C���H�b�#=D����)&:�C,��H�`�#=Ă���&:���b�#=D����)&:�C���H�b�#=D����u�*:�����H�0�!Lt���Ă����&:�C���H�b�#=D����)&:�C���H�`�#=Ă���&:���`�#=D����)&:�C���H�b�#=D����)&:�C,��H�`�#=Ă��    ��@,��H�b�#=D����)&:�C���H�b�#=D����u�*:�C,��H�`�c�&:��wh��H�b�#=D����)&:�C���H�b�#}���|Ut�_.�?]�������������yA�,���,���,��,7��,W��,w�_.��c�;��~����;������u�/w��r�/w����~��_��o����_��o����_��o�����������������m�������m�������m����������K7����m�������}[�t�r�/w��r�/w�?��;��ϸ�}[�t�r�/w��r�/w�w����_��~?�X��/,��~����;��~����;��~����;�������Ӥ���"��;��~����;��~����;��˂����m�������m�������;ڷ�y���������������]�������]�������]�����?w����~:c�|��?w�7���M�s��������]?��?�����;�����{�Ѯ����;�ӯw�w����_��������?w�7���M�s��������M�;�u��~x����w����~x��������뿧���w��-����~�oX<_&���M�sӯw�w�z����pz��zׯw�w�zׯw�w�z�߱x������灟~����;��~����;��~����;��~��:]�~����qG�6������_�������_�������������]�������]�������}[lw�zׯw�?�>�矙ӯw�w�zׯw�w�zׯw�w�zׯ��/���ҿ�|��vׯ��w�zׯ�w�zׯW�w�z���u{}��zׯW�w�z����}���U�]�^����U�����ӯW�w�zׯW�w�zׯW�w�rӟq]��>���r��o�\��,����u���r�o�\��,W�?.k�\��,�����JnoΟ`���7X.����������}[�}�q���6���
��_�o����_n��z\s��ͺ~������˿v��_�������_�������_�������_��_~Z�ӯ��7=�9�F�]�^c���5�]�^c���5�]�^c������b����5�]�^c���5�M���F�]�^c���5��{��z�}ׯ��w�z�}ׯ��w�z�}�/�ط�r��}��}����'�r�}�/�ط�r�}�/�ط�r�}�/��ߗ�֮�p���>�����_���z\s��X�~�ƾ�k��~�����_��o����_��o����_��o����b�9�˿G��k��~�ƾ�k��~�ƾ�k��~�ƾ�k���������?'�r�}׿0
O����_��o�:�����u�(��׾�wC)]_绡���(�1�1
N�:��4���wC)]_���R�?��_�w��R���wC)]_绡�����PJ���n(���|��s����������u�����˩�w[;w}��v��:��y���������V�e�/����_����������������QDXn��X��?��_��o����_��o����_��o����_��?�����PD����_��o����_��o����_��?��_��o����_���z\s~2�~�ƾ�k��~���h��~�ƾ�k��~�ƾ�k��~�ƾ�k�ל+�H�\c���5�m�\c���5�m�\c���5�m�\c4ZM�\c���5�]�m����_��o����_��?��_��o����_��o����_���z\s�("�r����?�r�}�/�ط�r�}�/�ط�r�}��\c���5�����5�M�k�E�_�������_������������O��E���ƾ���o��k�uL��/������a("�����k����O?��>�����5�]�s������S\s�("��5�M�s�}��\c��?��7�z�}���O?��>�����k���+�9W�~x�}��������뿧��ƾ���o��k�������ƾ��k���a("�z�}ׯ��w�z�}ׯ��w�z�}�/�ط�r����?�r�}�/��w=�9W�~�ƾ�k��~�ƾ��k�F���k�~�ƾ��k�~�ƾ��k�ל+�H�^c��zx^�~�ƾ��k�~�ƾ��k�~�ƾ��k�~���`��~�ƾ�o@�"ү��w�z�}ׯ��w�z�}ׯ����ӯ�w�z�}ׯW�w�z�}��kʬ("�z�}ׯ����ӯ��w�z�}ׯ��w�z�}ׯ��w�z�}ׯ�������+�H��f���m�]��f���m�]��f���m���/�8����r_�o�\P�<3n��+������`�e���=��x�3Xn���������`��7Xn��	pS�����%�m�����5�m�����E�m�����M�����M�]�����)\QD��&��_o����&��_o�?��_o����&��_o����&��_o���v�0�~�	�`��~�	��כ�~�	��כ�~�	��כ�~�	�`��~�	��qS�����M�]�����M�]�����M�G����M�m�����M�m�����)\QD��&��_n�?��_n�o��&��_n�o��&��_n�o��&��_n�?����C闛��~�	�헛��~�	�헛��~�	�h��~�	�헛��~�	��o��H�����M�m��4�O�����M�m�����M�m��������;F��"�u�H�a("=P����@�"�u�H�a("=F��"�c�o("=F��"���ѿ���@�"�u�H�a("=P����@�"�u�H_�("}��������Q��o?)�QDz��E����� ���"��C� ~����z ��PD= �a(� �0Q v���A�_����A�c�/(�z�A�u���CPD�@�"��Q=P�����|E�c�/(�z��ET�ѿ����ET�!(�z�A�u���CPD�@�"���_P���O����A�c�/(�z�A�u���CPD�@�"��Q=P����1����ѿ���1�Q=F��"��m�@2���CF��uȈ�z�A�u�����PD�K�����1������A��79���CPD�@�"��Q=P����:E��b�/(�z��ET�ѿ���1�Q=P����:ET�!(�z�A�u���CPD�F��"���_PD�����A�uL��7������1y�߀:&��P����:E�������A�c�/(�z��ET��� (�z�A�u���CPD�@�"�����ѿ���1�Q=F��"���_PD�@�"��Q=P����:ET�!(�z�A�����A�c�/(�z��ET�!(�z�A�u���CPD�@�"����U�k(��:_C�c�?�1�Q=F��"��orQ=P����:ET�!(�z�A��@�"���_PD�����A�c�/(�z�A�u���CPD�@�"�����P����1�Q=F��"���_PD�����CPD�@�"��Q=P����:E�?u�����PD�u��"����c�/(�z��ET��� (�z�A�u���CPD�P����:ET�ѿ���1�Q=F��"���_PD�@�"��Q=P����:E��u���CPD��D����\��\� f�� f�� f<� fE$�f�� f���W�ET !p�g p�g p�g��� (�z8AՃm���CPD�;P����:ET�ѿ���1�Q=F��"���_PD�@�"��Q=P����:E� u���CPD�����A�c�/(�z��ET�!(�z�A�u���CPD�'P����:ET_�k(��:_C�c�/(�z��ET�ѿ����ET�!(�z�    A�_@�"��Q=P����1�Q=F��"���_PD�����CPD�@�"�����!(�z�A�u���A�c�/(�z��ET�ѿ���:ET�!(�z�A{�<u���CPD�@�"���5Q=F��"���_PD������APD�@�"�����!(�z�A�u���A�c�/(�z��ET�ѿ���:ET�!(�z�A�/@�"��Q=P����1�Q=F��"���_PD�����CPD�@�"����
�!(�z�A�u�����PD�u��"���o�"�T/(�z��?�����T��E�y s���\��<�[�?���,�ߣ0>��f�<z ��!��?�c4|���&=F��I���c�c��w��_����������&=F��I���e�c4|���&=F��I���������\{����Q�k�c���u�ۨ��q�y�c4<�1������1+�c4|���&=F��I���c����K��1
Mz����~xݿ��ׯ?o
?Mz��ϓ��ˤ�h�:�1�Mz�������޾�����M��y�c�����'=FçI���y�c4|������1�~xa4|���'=FÏA��(�9�1
Mz��ߓ����>]���׵�h�<�1�Lz������ۤ�h�>�1~��7�o����� ~z��_������h�4�1>Oz��/�����h�s���h�>�1~�3F��I�Q�k�c����&=Fß�=}��uY{��/�����h�6�1�Oz�����Q�s�c�!1~�����K�Q�{���n���7��'=Fã��ˤ�h�:�1���׷����=n
�F�]�������פ�(�=�1>Mz����h��Qz����h��Qz���7�o(��1
Nz��7t��~x��ϸ)��7=FçI���y�c4|��_'=F÷I�Ѱ����c4l�(=Fᆎ�cn�(=F��I_绽ϻ7��'=F�R��~~�=F×I���u�c4|��)|{�w=FÏI���������i�_�������h�4�1>Oz��/�����h�6�q��J�����.�F�]�Q�s�c�����'=ƧI���y�c�������c�|��X'��c����v�1�1
Nzl�_�����~+��ϫ�:�?����A�'=fЗI��u�c}�����4=f�2�����yw��!���r�5�1dOz̠O�3����2�1�ސ�W�~������7n>�#&�c�䘸?9�A���{�c�����o��s��>Oz�/�������6���?�'=�ϏI�}��p��x�=���I�}�{���n/�ǥ��I������2���������ƥ���5=���I���c��R��Ǿ�5�oOz�7��~��9���A���e�c�|�s\x�m����� ���1�1l����'�ۚc��䘵�9VϧA���y�c�|�<_������~Þ��������1�hNz,�_���������]��������ȸ��d��X<��~��X<��>=϶�O�ų-���{[اǢ�s����w��c����X<�&=��I���e�c�|���|{+w=ϟ�=}��x~Lz,ڟ���פǢ�=�x>Mz,�ϓ��m
��������:�X<�&=��I���c�c����X��&=ퟩ���#�e��|�v������/�]�k�/���k쯓�\�m�_��|?,~�k��I��9�hMz,�ߓ���|��u�������o�￿��]��y��}��&j���w5]_绉�����DM���n����|�i������w��6���:�m���u���\���W�u���n������]���Ut��p����<��Ut�\a��#@�����
*:� ������3Ă���&:�ä��H�b�#=D����)&:�C���H�b�#=Ă���&:�ל��H�`�#=D����)&:�C���H�b�#=D����)&:�����H�k�Mh�=�9Wёb�DGz��q�����)&:�C���H�b�#=D����)&:�C,���{\s��#=Ă���&:�C���H�b�#=D����)&:�C���H�b�#=Ă����C,��H�`�#=Ă���)&:�C���H�b�#=D����)&:�C���H_竢c�q͹���&:�C,��H�k�Mt��H1ё"�DGz��!RLt��H1ёb�D���s�!Lt��X0ё"�DGz��!RLt��H1ё"�DGz��!Lt�=�9Wёb�DGz��!RLt��H1ё"�DGz��!RLt��H1ё��WE�޿�|Ut��X�&:�C,��H�k�Mt��H1ё"�DGz��!RLt��H1��ןy͹���&:�C,��H�`�#=D����)&:�C���H�b�#=D����)&:���`�#=Ă���&:�C,��H�b�#=D����)&:�C���H�b�#=D���������H�k�Mt��X0ёb�DGz��q�����)&:�C���H�b�#=D�����5�*:�C,��H�`�#=Ă���&:�C���H�b�#=D����)&:�C����{\���#=Ă���&:�C,��H�`�#=D����)&:�C���H�b�#=D�����C���H_竢#=Ă���&:�C,��H���Mt��H1ё"�DGz��!RLt�=.nWёh�DGz���Lt��[ё EDGrx��QDt$�FёEDǞ��v�!Lt��X0ёb�DGz��!RLt��H1ё"�DGz��!RLt��"�DG�:_�q#�	���b�DGz��q#����)&:�C���H�b�c�qc����)&:��-��H� �#9܂���p":�C���H�"�#94�����(":��nj7ёEDGr�ɁDt$�YёdADGrh��QLt��G1ё �D�ޟ!RLt��H1ё��VEGz��!Lt��X0ёw��H�b�#=D����)&:�w���H�b�#=Ă���&:�C,��H�`�#=D����)&:�C���H�b�c�qG����)&:�C,��H�`�#=Ă���&:�C���H�b�#=D����)&:���b�#=D����u�*:��~�!F=Ă���&:�ע]EGz��!RLt��"�DGz��!RLt��X0ёb�DGz��!Lt��H1ё"�DGz�{��H1ё"�DGz���|Ut���Uё��WEG�:_�!RLt��H1ё~�
��? ����������b���?�h����+�,^�`��X�^�_��_�B�/b������E,��"�~m����6||`����W��+t���|�
]�X�&�T���@~o��*��B�~�
m�P��_�B�/T���p4mJ�P��_�B�_OUh��*��B�~�
m�P������m����Uh��*��B�~�
]^�B�/T�hʘ~�
m�P��_�B�/T�����Uh��*<:wY��*t�e�
m�P��_�B�/T�����U����*��B�~�
m�P���.T�����Ux~�h������Uh��*��B�~�
m�P��_�����~�����*��B�~�
m�P��_�B�/T����~?��~�
]�R��_�B��W���+U���*t�J���X?�W���+U���*t�J�~�
]�R���T�����c�W���+U���*t�J�~�
]�R��_����w�~��*��B���P��_�B�/T�����Ux|�.����P��_�B�/T�����U���B�~�
��?��ϫ��*��B�~�
m�P��_�B�/T����Y��ן��B���P��_�B�/T�����Uh��*��J�~�
]�R��_�����J�~�
]�R����{�W���+Th��)4���|U
M�"�&_���/�ץ?�F��W����Q���(t�j�~5
]���W��������_�BׯF��/�Q���(t�j�~5
���t]����(t���~U
]�2��_�BׯP    �鯫TX>���~�
]�R��_�BׯT��W���+U���*,w6�q]W���+Uh��J�~�
]�R��_�B�/T������?T���B��P����
M�C���P��_�·A�_���*4��Uh����?Z���B��x��_���ǁۚ��?f`!�,$����3��|��`����9ూ�&_�B��\��W���s�
M�Z�&��
ߟ ֿ��+U���*t�J�~�
]�R��_�B��P��[�7�k�
]�R��_�BׯT��W���+U���*Џ�+U���*t�J���R��_�BׯT��W�p��ׯ�{�
m�P��_�B�/T�����Uh��i�
��w^��*��B�~�
m�P��_�B�/T���p_�|�	?�P����=������B~�|�?�ҁe��ӯt��W:��+���t�J���ҁ�_������گt��W:��+���t�J�~�]�ҁ�����Y<����9���,^����=��|7
��u��z������y j@iE���PZ��C�V���C�V���y <�hEz���a�V�0Z�z�hEz���AC�V�Z��w���C�V$�Z�,DhE�:Z���d�V$0Z�t�hEz���kz��b��!F+҃��Hb�"=h�MwӃ��t7=�MwӃ�t7=�MwӃ�Hb�b!F+҃��Hb�"=h�ъ��!F+҃�H:`�"=�ъ��F+҃����h�ъ��!F+҃��Hb�"=h�ъ�u�J+҃�H:`�"=�ъ�5=VZ���%]iEz���AC�V����ߠ!���t`�~~�L��oЁ���:`�"=h�ъ��q1Z�4�hEz���AC�V�1Z�t�hEz���A�V�0Z�4�h�ޟAC�V�1Z�4�hEz���AC�V���UZ���WiEzЁQ:`�"=�ъ��-�J+҃��Hb�"=h�ъ��!F+҃��H:`�"=�ъ��F+҃���+h�ъ��!F+҃��Hb�"=h�ъ��!F+҃�H:`�"=�ъ��F+��b�"=h�ъ��!F+҃��Hb�"=h�ъ�u�J+���*�H:0�A�V��t�hE�Z +�Hb�"=h�ъ��!6�Ob����!6�O:`����F+҃����ъ��!F+҃��Hb�"=h��|�!��}��L��?09� �Њ� B+��	? �"9\�Њ�`!B+����H"�"9X�ъ�p!F+���*�H:`�"=�ъ��-�J+���XiEz���AC�V�1Z�4�hEz���A�V�0Z�t�h�޿A�V�1Z�4�hEz���AC�V�1Z�4�hEz���A�V�0Z��?O�F+҃��Hb�"=h�ъ��!F+҃��Hb�"}��Ҋ�u�J+҃Lzܙ��"=�ъ�5DVZ�4�hEz���AC�V�1Z�4�hEz���a�V��x�hEz���AC�V�1Z�4DhEr���C�V$�Z��@hEr �{~ Z��@hEr����B�V$Z�,DhEr����B�V$�SUZ�t�h���t��A�V���Ҋ��!F+҃��Hb�"=h�ъ��!F+҃�H:`�b��F+҃�Hb�"=h�ъ��!F+҃��Hb�"=h�ъ��F+҃����ъ��F+҃��Hb�"=h�ъ��!F+҃��Hb�"}��Ҋ�u�J+��	j0�A�V�E0Z����_�"
r�ן�~<�����9h��_���ן�����/���A�V�0Z��hEz���AC�V�1Z�4�hEz���A�V��t�hEz���A�V�1Z�4�hEz���AC�V�1Z�4�hE�:_���T竴"=��4py/�����y �����ߣj���r� n>>��~�2z ���,����\��k��Z{,���_#m��s�c����X0�'=n�I����ҿp��ˤǂ�:�`�Mz,��������X0>'=�����[��\{,ߓ�Ӥǂ�<�`�Lz,���ۤǂ����������埿����ǂ�5�`|O�:��;���`<Oz\~������zz,/���ǂ�6豀�����Ǥǂ�9�`�|?|�2�,�c����X��&=��I��e�c�x��X0�����~���i�<���?��ǂ�9�`|Mz,ߓ�Ӥǂ�<�`������]{,���۠�������`|Lz,���פǂ�s�������|��y�c�x����x�c�x��X0^=6P����`�|?���Ǥǂ�9�`|Mz,ߓ�Ӥǂ�<�`����痵ǂ�:�_X0�&=��I��c�c����X0�&=����W��`z�N���ǂ�DgzLMt�=./Wљ#F��b4�3F�c4�CF��|�&���d�1d4�C�M�}���o�6��2�'=���I�!�s�c����2�'=�n���z?<_k�!�y�c�x��2^�C�ۤǐ�>�1d|Lz7�?�{����']�!�{�c�v��2�'=���I�!�u������|�z�=���I�!�c�c����2�&=���I_绽��C���c�_��w �z/�s��}��2�&=���I�!�c�c�����������M�64�zߓ[�Ӥǐ�<�1d�����I�!�&����O�!�}�c����2>'=���I�)�{�c�v��3n������y��;̷�y��1g�Nz�o�����Ǣ�1�1i|Nzl?�{����~^}��mh��u��м�q#�y�c�x��Em��1k�Mz�Ey%ǪQ�Wr�Ey%Ǧ�5�1i|r,�N��F�{ɱg���7�c�A���=�u�c�x���3�'=���I�=�s�c�����3n���c� ����P�c�x���3^=�2�@M�=�m�c�x���3n������b�U��>��g|Mz�ߓ��w�'v=���I���'=�W�>��_�s�� ǚ�6�1f�rl�S�� ���5�1���:�!�1�����*���� �f�6�1��r,&7A���=�=��I���k�c1���Xԝ&=��A��η�y�c1�	�/��c1i� =�&�c1i� =�&�c1i� =�&����-�_���k���y���]u���ˤ�T����o��V�}�߫�|?��?��ꟓ�Y�kҿ�O�w��M���:��U��u�ۻ���|���ߟ��:߿�`����M����o:��u���������۾�w[�w}��h}�,���w[�w}��h��:�m���u�ۢ���|�E{���n������[����|��M���u{����ˠ����N�s��I��>�����￿~�[��I��5�տ'�3�����u������|��u�?�|��������G������:�����u������|��g���n�Ϯ���ޟ]�˴Ut��p����<��Ut��ty \AEG�WPё Tt�ܹl�#=P����0)&:҃���H�b�#=D����A,��H�`�#=Ă���&:�C���H�b�#=D����)&:�C���H�b�c�qg����&:�C,��H�`�#=D����)&:�C���H�b�#=D����)&:��3�LWё��WEGz��!Lt��X0ёw.��H�b�#=D����)&:�C�����3D����&:�C,��H�`�#=Ă���)&:�C���H�b�#=D����)&:�w���H�`�#=Ă���&:�C,��H�b�#=D����)&:�C���H�b�c�qg����u�*:�����H�0�!Lt��X0ёw.��H�b�#=D����)&:���b�#=D����&:�C,��H�`�#=Ă���)&:�C���H�b�#=D�����C���H�b�#=Ă���&:�C,��H�`�#=D����)&:�C���H�b�c�qg����)&:�����H�`�#=Ă���&:���e�!RLt��H1ё"�D    ��?!RLt��H1ёb�DGz��!Lt��X0ё"�DGz��!RLt��H1ѱ��:�l�#=D����&:�C,��H�`�#=Ă���)&:�C���H�b�#=D�������*:�C���H_竢#}������b�DGz��q�����)&:�C�����/'��!RLt��H1ёb�DGz��!Lt��X0ё"�DGz��!RLt�=�NWё"�DGz��!Lt��X0ёb�DGz��!RLt��H1ё"�D����t�!RLt��H1ё��WEGz��Lt��Z0ё1��H�"�#9@���=�£��H�"�#94����0":��,��H�`�#=Ă����(&:����H�b�c�q�����)&:�C���H�`�#=Ă���&:�C,��H�b�#=D����)&:���b�#=D����)&:�����H_竢#=�¨�[ёnADGr��,�#98���=@���H�"�#94�����(":��-��H� �#9Ԃ���@":�C���H�"�c�qg�����(&:�ã��H�b�#=Ă���&:�C,��H�`�#=D����)&:�w���H�b�#=D����)&:�����H_竢#=�¨�X0ёb�DG�Z�����7D����)&:�C���H�b�#=D����&:�C,��H�`�#=Ă���)&:���	"�DGz��!RLt��H1ё"�DGz��!Lt��X0ё��WEGz�{�H1ё"�DGz��!RLt��X0ёb�DGz��K,��H�b�#=D�����@���H�b�#=D����&:�C,��H�`�#=Ă���)&:�C���H�b�c�)&:�C���H�b�#=~�����}_�=P�ACEG=p�Lt��<��� W0�Q�+�� Lt�[-�MtT�"��z�Ճ���"EDG�)":��X�Q=Ă���!DtT� �#�"EDG�)":��H�Q=D����!RDtT�"��z��C,��bADG�":�? RDtT�"��z��C���"EDG�)":���5�Q}�����!DtT� �#�bADG��":��H�Q=D����!RDtT�"��z��C,��bADG�":ҿ DtT�"��z��C���"EDG�)":��H�Q=Ă���!DtT� �#�bADG�)":��H�Q=D����!RDtT�"��z�����訾��DG���v�X�Q=Ă����;4DtT�"��z��C���"EDG�)":��X�Q=Ă���g��C,��"EDG�)":��H�Q=D����!RDtT�"��z��C,��H�X�Q=Ă���!RDtT�"��z��C���"EDG�)":���5�Q=Ă���W��C,���CCDG�)":��H�Q=D����!RDtT�"��z��C,��H�X�Q=Ă���!RDtT�"��z��C���"EDG�)":��X�Q=Ă���w��C,��"EDG�)":��H�Q=D����!RDtT�"���:_�����H��X�":��X�Q=~�����!RDtT�"��z��C���"EDG�":�?!DtT� ��z��C���"EDG�)":��H�Q=D����!RDtT� �#�bADG�":��X�Q=D����!RDtT�"��z��C���"EDG�u�&:ҿaDtT� ��z���h��"�XtT�r,:*�G9�����ʡQ�EG�0Ǣc��'��c�Q9Ă���!DtT�"��z��C���"EDG�)":��H�Q=Ă���g��C,��bADG�)":��H�Q=D����!RDtT�"��z������H��5�Q=�¨�[8��-����[4�EG��(Ǣ�rh�c�Q94ʱ��XtT�r,:�_��EG�`Ǣ�r��c�Q9�±��XtT�r,:*�F�Q=8�����QDtT�"�#�bADG�":��X�Q=Ă���!RDtT�"��z��C���"EDG�)":���|MtT_�k��z��Q� ��z���wh��"EDG�)":��H�Q=D������C,��bADG�":��X�Q=D����!RDtT�"��z��C���H��H�Q=Ă���!DtT� ��z��C���"EDG�)":��H�Q=D����/��C,��bADG��":��H�Q=D����!RDtT�"��z��C,��H��X�Q=Ă���!DtT�"��z��C���"EDG�)":��H�Q=~�������#[�Nt�s=��#\򀋎< ���#�+��� *:� ~�����@)&:�ä��H�b�c��)&:�C���H�`�#=Ă���&:�C,��H�b�#=D����)&:�C�����D����)&:�C,��H�`�#=Ă���&:�C���H�b�#=D����)&:��
�b�#=D����u�*:�����H�`�#=Ă���&:��wh��H�b�#=D�����A���H�b�#=D����&:�C,��H�`�#=Ă���)&:�C���H�b�c��)&:�C���H�b�#=Ă���&:�C,��H�`�#=D����)&:�C�����D����)&:�C���H_竢#}������b�DGz���;4Lt��H1ѱ�O��!RLt��H1ё"�DGz��!Lt��X0ёb�DGz��!RLt��"�DGz��!RLt��H1ёb�DGz��!Lt��X0ё"�DGz�{��H1ё"�DGz��!RLt���Uёb�DGz��!Lt����0ё"�D�_�<A���H�b�#=D����)&:�C,��H�`�#=Ă���&:�C���H�b�c��)&:�C���H�b�#=D����&:�C,��H�`�#=Ă���)&:�C�����D����)&:�C���H�b�#}�����u�*:�C,�z��!Lt����0ѱ�W��!RLt��H1ё"�DGz��!Lt��X0ёb�DGz��!RLt��"�DGz��!RLt��H1ё"�DGz��!Lt��X0ёb�DGz�{�H1ё"�DGz��!RLt��H1ё��WEGz��Lt��Z0ё�D�D��? RDt$HёEDGrpɡQDt$�YёdADGr��!Lt��G1ѱ�O��!RLt��H1ё"�DGz��!Lt��X0ёb�DGz��!RLt��"�DGz��!RLt��H1ё"�DG�:_��|Ut�[�p":��-����w��Mt$GёEDGrhɡQDt$�FёnADGr�ɡDt$Z��N�(":�C���H�b�#=8�����(&:����H�`�#=Ă���&:�C,����3D����)&:�C���H�b�#=D����)&:�����H_竢#=�¨�X0ѱ����;4Lt��H1ё"�DGz��!RLt��H1ёb�DGz��!Lt��b�DGz��!RLt��H1ё"�DGz��!RLt��X0ёb�DGz�{��Uё"�DGz��!RLt��H1ё"�DGz��!Lt��X0ё�C�D���!RLt��H1ё"�DGz��!RLt��X0ёb�DGz��!Lt��H1ѱ���!RLt��H1ё"�DGz��1�5�����laMC�Ú��������>y�7�?F`k��"VDD���]DD�XĊ������"�2鱈�Nz,bE�T�E������]DD�XĊ����פ���=鱈=Mz,bE�T�E���걈Q=�""��"�>�yG�c�c���XĊx��g�c+"��:_�c;걈�Lz,b����[߿yG���걈Q=�""��"�5�qG�{�c{��XĊx��X�c+"�z,bED����I�E�c�c���XĊx��g�c+"�z,bEDT�E�e�c{��X����X/ !
  �c+"�z,bEDT�E����x~O�:��}��XĞ'=�"^��"V�C�XĊpH�;����Ǥ�"�9鱈}Mz��.�z,bE8T�E��걈�Lz,b����۠��"X��"VK�XĊp��X�c����XĞ&=��I�E��걈�R=�"�߱��P=��I�E�s�c���X<�`��X,���^E8T�E��"VDD�X�^=�h�Mz,b��X/�c+"�z,bEDT�;�EDT�E�i�c{��X�^&=�"^��v�c+"�z,bEDT�E�s�c���X<�'=�"^��"VDD�XĊ���X�yG�m�c{��X�>&=�"^��"VDD�X<������DD�XĞ'=��I�E�u��v/�c+¡zlbE8T�Q�s�c������=鱋=MzcE�T�a���1���׿N���6�1��Ozc��X,�c+��z̞E8T�a���1�=Ozc/�����U�"X��0VK�Ɗp��X�c���>�'=���I_�{�R=����X�/�Ɗ���X�c{��X�>&=��I�}����q]�����cQ9��*�rd/}��گ��� �@�غT�}���걏Q=��""����=鱏=Mz�cϓ�X/�yc���걏Q=��""��>�1鱏}Nz�c_��g/�c+"��:_�c;����^&=���I�}�m�c{�������1�=V�c{�"*�u��A�m�i�c{�s��~>�.�c{�"*�.�XET�Y챊���� �(�9ȱ�}rL���K�XĊ���X�qG���걈�Nz,bo������"V�K�XĊ��w��������"�2鱈���w��H��X)�c����[,T�E�s�c������=鱈�����6�(�{�Q�=����DVdcDs�J�����^/bC��׋�&��"�{߿�z�h�|�׋��+�^Ć0Y��!L��El��z��d��}>��������|�w�o߇@���-�����پ�&��-����{a�~�>�������W����|�oߏ&��G��g�ϧ���ևH�G~��X��>D�����?��|��\"e�}}���?�����/�~���X�}KD�߶�CD\���!"����xz}�o��/�o���۷D���["b��}��_����{~�o����E��}o!^��*���:�*���a�|���):�@*%EH������| �R��^(�D��u�=E��B�z���%:�%RJt�K��蠗H)�A/�R��^"�D�DJ����%Jt�K,�蠗X(�A/�P��^"�D�DJ�z���)%:�%RJt�K���8�n���߾):�%^�%Jt�K,��׍��)%:�%RJt�K��蠗H)�qz�h/�A/�P��^b�D��B�z���)%:�%RJt�K��蠗H)�A/�R�����^��^b�D��B�z���%:�%RJt�K��蠗H)�A/�R��^"�D��}��D��M�A/�P��^b�D��B�z�h/�A/�R��^"�D�DJ�z������%:�%Jt�K,�蠗X(�A/�P��^"�D�DJ�z���)%:�%RJt��7�Kt�K,�蠗X(�A/�P��^b�D�DJ�z���)%:�%RJt�K���8�o���߾):�o�z���z���%:�u��D�DJ�z���)%:N��%:�%RJt�K,�蠗X(�A/�P��^b�D�DJ�z���)%:�%RJt��7�Kt�K��蠗X(�A/�P��^b�D��B�z���)%:�%RJt�K���8�]"�D�DJ��훢�^b�D��B�z�����^��^"�D�DJ�z�������)%:�%Jt�K,�蠗X(�A/�P��^"�D�DJ�z���)%:N�S�%:�%RJt�K,�蠗X(�A/�P��^b�D�DJ�z���)%:�%RJt�ާ�Kt�K���߾):�o�z���z���%:�u��D�DJ�z�������)%:�%RBt��-�� [�A.���\h!D�4J�ri���(%:���ﵗ蠗G)�A/�R��^b�D��B�z���%:�%RJt�K��蠗H)�qz_l/�A/�R��^"�D��M�A�}StЋ,���,��Z(�A���%:�%RJt���Ct���� �G	�A.���\n!D��B�r���r!:ȥQBt�K���8�ﴇ� �F	�A.���\%D��B�z���%:�u��D�DJ�z�������)%:�%RJt�K,�蠗X(�A/�P��^7�Kt�K��蠗H)�A/�R���w���)%:�%Jt�K,�蠗X(�A/�P��^"�D�DJ�z���)%:N��%:�%RJt�K,�蠗X(�A/�P��^b�D�DJ�z���)%:�%RJt���DJ�z����7E��M�A/��R/�P��^b�D�^��蠗H)�A/�R����J��蠗H)�A/�R��^b�D��B�z���%:�%RJt�K��蠗H)�q�/���)%:�%RJt�o���7E��M�A�}St|�������=�C      �   �   x���Q��0D��a��`�]���XCS)��Xq��1d�V���������ue��_(X�̏��T	x٫����PǏ����dA�����nv�f[�����͍(�h*7��3Q7��~d�5��v�۹��r�b��칈s-���E�X4���ۀf�v*����c�n�3�Q�d?�F��$���$���$�Yj苗{�Q�����m����9V�h�U@^c��MH�ۄD��0x�?/l�&     