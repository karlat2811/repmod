PGDMP     '                	    v           planning    10.3    10.3 �   �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            �           1262    16393    planning    DATABASE     �   CREATE DATABASE planning WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_United States.1252' LC_CTYPE = 'English_United States.1252';
    DROP DATABASE planning;
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            �           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3                        3079    12924    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            �           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1259    16394    abaTimeUnit_id_seq    SEQUENCE     �   CREATE SEQUENCE public."abaTimeUnit_id_seq"
    START WITH 2
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 +   DROP SEQUENCE public."abaTimeUnit_id_seq";
       public       postgres    false    3            �            1259    16396    aba_breeds_and_stages_id_seq    SEQUENCE     �   CREATE SEQUENCE public.aba_breeds_and_stages_id_seq
    START WITH 8
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 3   DROP SEQUENCE public.aba_breeds_and_stages_id_seq;
       public       postgres    false    3            �            1259    16398    aba_breeds_and_stages    TABLE       CREATE TABLE public.aba_breeds_and_stages (
    id integer DEFAULT nextval('public.aba_breeds_and_stages_id_seq'::regclass) NOT NULL,
    code character varying(100),
    name character varying(100),
    id_aba_consumption_and_mortality integer,
    id_process integer
);
 )   DROP TABLE public.aba_breeds_and_stages;
       public         postgres    false    197    3            �           0    0    TABLE aba_breeds_and_stages    COMMENT     o   COMMENT ON TABLE public.aba_breeds_and_stages IS 'Relaciona los procesos de ARP con el consumo y mortalidad ';
            public       postgres    false    198            �           0    0    COLUMN aba_breeds_and_stages.id    COMMENT     o   COMMENT ON COLUMN public.aba_breeds_and_stages.id IS 'Id de la relacion entre proceso y consumo y mortalidad';
            public       postgres    false    198            �           0    0 !   COLUMN aba_breeds_and_stages.code    COMMENT     u   COMMENT ON COLUMN public.aba_breeds_and_stages.code IS 'Codigo de la relacion entre proceso y consumo y mortalidad';
            public       postgres    false    198            �           0    0 !   COLUMN aba_breeds_and_stages.name    COMMENT     u   COMMENT ON COLUMN public.aba_breeds_and_stages.name IS 'Nombre de la relacion entre proceso y consumo y mortalidad';
            public       postgres    false    198            �           0    0 =   COLUMN aba_breeds_and_stages.id_aba_consumption_and_mortality    COMMENT     �   COMMENT ON COLUMN public.aba_breeds_and_stages.id_aba_consumption_and_mortality IS 'Id de tabla aba_consumption_and_mortality (FK)';
            public       postgres    false    198            �           0    0 '   COLUMN aba_breeds_and_stages.id_process    COMMENT     Y   COMMENT ON COLUMN public.aba_breeds_and_stages.id_process IS 'Id de la tabla mdprocess';
            public       postgres    false    198            �            1259    16402 $   aba_consumption_and_mortality_id_seq    SEQUENCE     �   CREATE SEQUENCE public.aba_consumption_and_mortality_id_seq
    START WITH 8
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 ;   DROP SEQUENCE public.aba_consumption_and_mortality_id_seq;
       public       postgres    false    3            �            1259    16404    aba_consumption_and_mortality    TABLE     $  CREATE TABLE public.aba_consumption_and_mortality (
    id integer DEFAULT nextval('public.aba_consumption_and_mortality_id_seq'::regclass) NOT NULL,
    code character varying(100),
    name character varying(100),
    id_breed integer,
    id_stage integer,
    id_aba_time_unit integer
);
 1   DROP TABLE public.aba_consumption_and_mortality;
       public         postgres    false    199    3            �           0    0 #   TABLE aba_consumption_and_mortality    COMMENT     �   COMMENT ON TABLE public.aba_consumption_and_mortality IS 'Almacena la información del consumo y mortalidad asociados a la combinacion de raza y etapa';
            public       postgres    false    200            �           0    0 '   COLUMN aba_consumption_and_mortality.id    COMMENT     �   COMMENT ON COLUMN public.aba_consumption_and_mortality.id IS 'Id de los datos de consumo y mortalidad asociados a una raza y una etapa';
            public       postgres    false    200            �           0    0 )   COLUMN aba_consumption_and_mortality.code    COMMENT     �   COMMENT ON COLUMN public.aba_consumption_and_mortality.code IS 'Codigo de los datos de consumo y mortalidad asociados a una raza y una etapa ';
            public       postgres    false    200            �           0    0 )   COLUMN aba_consumption_and_mortality.name    COMMENT     �   COMMENT ON COLUMN public.aba_consumption_and_mortality.name IS 'Nombre de los datos de consumo y mortalidad asociados a una raza y una etapa';
            public       postgres    false    200            �           0    0 -   COLUMN aba_consumption_and_mortality.id_breed    COMMENT     �   COMMENT ON COLUMN public.aba_consumption_and_mortality.id_breed IS 'Id de la raza asociada a los datos de consumo y mortalidad';
            public       postgres    false    200            �           0    0 -   COLUMN aba_consumption_and_mortality.id_stage    COMMENT     �   COMMENT ON COLUMN public.aba_consumption_and_mortality.id_stage IS 'id de la etapa en la que se encuentran los datos de consumo y mortalidad ';
            public       postgres    false    200            �           0    0 5   COLUMN aba_consumption_and_mortality.id_aba_time_unit    COMMENT     �   COMMENT ON COLUMN public.aba_consumption_and_mortality.id_aba_time_unit IS 'Id de la unidad de tiempo utilizada en los datos cargados en consumo y mortalidad (dias o semanas)';
            public       postgres    false    200            �            1259    16408 +   aba_consumption_and_mortality_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.aba_consumption_and_mortality_detail_id_seq
    START WITH 203
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 B   DROP SEQUENCE public.aba_consumption_and_mortality_detail_id_seq;
       public       postgres    false    3            �            1259    16410 $   aba_consumption_and_mortality_detail    TABLE     =  CREATE TABLE public.aba_consumption_and_mortality_detail (
    id integer DEFAULT nextval('public.aba_consumption_and_mortality_detail_id_seq'::regclass) NOT NULL,
    id_aba_consumption_and_mortality integer NOT NULL,
    time_unit_number integer,
    consumption double precision,
    mortality double precision
);
 8   DROP TABLE public.aba_consumption_and_mortality_detail;
       public         postgres    false    201    3            �           0    0 *   TABLE aba_consumption_and_mortality_detail    COMMENT     �   COMMENT ON TABLE public.aba_consumption_and_mortality_detail IS 'Almacena los detalles para la unidad de tiempo asociada a una determinada agrupación de consumo y mortalidad ';
            public       postgres    false    202            �           0    0 .   COLUMN aba_consumption_and_mortality_detail.id    COMMENT     �   COMMENT ON COLUMN public.aba_consumption_and_mortality_detail.id IS 'Id de los detalles para la unidad de tiempo asociada a una determinada agrupación de consumo y mortalidad ';
            public       postgres    false    202            �           0    0 L   COLUMN aba_consumption_and_mortality_detail.id_aba_consumption_and_mortality    COMMENT     �   COMMENT ON COLUMN public.aba_consumption_and_mortality_detail.id_aba_consumption_and_mortality IS 'Id de la agrupación de consumo y mortalidad asociada';
            public       postgres    false    202            �           0    0 <   COLUMN aba_consumption_and_mortality_detail.time_unit_number    COMMENT     �   COMMENT ON COLUMN public.aba_consumption_and_mortality_detail.time_unit_number IS 'Indica la unidad de tiempo asociada a la agrupacion de consumo y mortalidad';
            public       postgres    false    202            �           0    0 7   COLUMN aba_consumption_and_mortality_detail.consumption    COMMENT     �   COMMENT ON COLUMN public.aba_consumption_and_mortality_detail.consumption IS 'Consumo asociado a una determinada agrupación de consumo y mortalidad ';
            public       postgres    false    202            �           0    0 5   COLUMN aba_consumption_and_mortality_detail.mortality    COMMENT     �   COMMENT ON COLUMN public.aba_consumption_and_mortality_detail.mortality IS 'Mortalidad asociada a una determinada agrupación de consumo y mortalidad ';
            public       postgres    false    202            �            1259    16414    aba_elements_id_seq    SEQUENCE     �   CREATE SEQUENCE public.aba_elements_id_seq
    START WITH 22
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 *   DROP SEQUENCE public.aba_elements_id_seq;
       public       postgres    false    3            �            1259    16416    aba_elements    TABLE       CREATE TABLE public.aba_elements (
    id integer DEFAULT nextval('public.aba_elements_id_seq'::regclass) NOT NULL,
    code character varying(100),
    name character varying(100),
    id_aba_element_property integer,
    equivalent_quantity double precision
);
     DROP TABLE public.aba_elements;
       public         postgres    false    203    3            �           0    0    TABLE aba_elements    COMMENT     T   COMMENT ON TABLE public.aba_elements IS 'Almacena los datos de los macroelementos';
            public       postgres    false    204            �           0    0    COLUMN aba_elements.id    COMMENT     D   COMMENT ON COLUMN public.aba_elements.id IS 'Id del macroelemento';
            public       postgres    false    204            �           0    0    COLUMN aba_elements.code    COMMENT     J   COMMENT ON COLUMN public.aba_elements.code IS 'Codigo del macroelemento';
            public       postgres    false    204            �           0    0    COLUMN aba_elements.name    COMMENT     J   COMMENT ON COLUMN public.aba_elements.name IS 'Nombre del macroelemento';
            public       postgres    false    204            �           0    0 +   COLUMN aba_elements.id_aba_element_property    COMMENT     q   COMMENT ON COLUMN public.aba_elements.id_aba_element_property IS 'Id de la propiedad asociada al macroelemento';
            public       postgres    false    204            �           0    0 '   COLUMN aba_elements.equivalent_quantity    COMMENT     �   COMMENT ON COLUMN public.aba_elements.equivalent_quantity IS 'Cantidad de la propiedad asociada al macroelemento con el fin de realizar equivalencias';
            public       postgres    false    204            �            1259    16420 &   aba_elements_and_concentrations_id_seq    SEQUENCE     �   CREATE SEQUENCE public.aba_elements_and_concentrations_id_seq
    START WITH 105
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 =   DROP SEQUENCE public.aba_elements_and_concentrations_id_seq;
       public       postgres    false    3            �            1259    16422    aba_elements_and_concentrations    TABLE     k  CREATE TABLE public.aba_elements_and_concentrations (
    id integer DEFAULT nextval('public.aba_elements_and_concentrations_id_seq'::regclass) NOT NULL,
    id_aba_element integer,
    id_aba_formulation integer,
    proportion double precision,
    id_element_equivalent integer,
    id_aba_element_property integer,
    equivalent_quantity double precision
);
 3   DROP TABLE public.aba_elements_and_concentrations;
       public         postgres    false    205    3            �           0    0 %   TABLE aba_elements_and_concentrations    COMMENT     x   COMMENT ON TABLE public.aba_elements_and_concentrations IS 'Asocia una formula con los macroelementos que la componen';
            public       postgres    false    206            �           0    0 )   COLUMN aba_elements_and_concentrations.id    COMMENT     �   COMMENT ON COLUMN public.aba_elements_and_concentrations.id IS 'Id de la asociación entre una formula con los macroelementos que la componen';
            public       postgres    false    206            �           0    0 5   COLUMN aba_elements_and_concentrations.id_aba_element    COMMENT     g   COMMENT ON COLUMN public.aba_elements_and_concentrations.id_aba_element IS 'Id del elemento asociado';
            public       postgres    false    206            �           0    0 9   COLUMN aba_elements_and_concentrations.id_aba_formulation    COMMENT     l   COMMENT ON COLUMN public.aba_elements_and_concentrations.id_aba_formulation IS 'Id de la formula asociado';
            public       postgres    false    206            �           0    0 1   COLUMN aba_elements_and_concentrations.proportion    COMMENT     x   COMMENT ON COLUMN public.aba_elements_and_concentrations.proportion IS 'Proporción del elemento dentro de la formula';
            public       postgres    false    206            �            1259    16426    aba_elements_properties_id_seq    SEQUENCE     �   CREATE SEQUENCE public.aba_elements_properties_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 5   DROP SEQUENCE public.aba_elements_properties_id_seq;
       public       postgres    false    3            �            1259    16428    aba_elements_properties    TABLE     �   CREATE TABLE public.aba_elements_properties (
    id integer DEFAULT nextval('public.aba_elements_properties_id_seq'::regclass) NOT NULL,
    code character varying(100),
    name character varying(100)
);
 +   DROP TABLE public.aba_elements_properties;
       public         postgres    false    207    3            �           0    0    TABLE aba_elements_properties    COMMENT     �   COMMENT ON TABLE public.aba_elements_properties IS 'Almacena las propiedades que pueden llegar a tener los macroelementos para realizar la equivalencia';
            public       postgres    false    208            �           0    0 !   COLUMN aba_elements_properties.id    COMMENT     Z   COMMENT ON COLUMN public.aba_elements_properties.id IS 'Id de la propiedad del elemento';
            public       postgres    false    208            �           0    0 #   COLUMN aba_elements_properties.code    COMMENT     _   COMMENT ON COLUMN public.aba_elements_properties.code IS 'Codigode la propiedad del elemento';
            public       postgres    false    208            �           0    0 #   COLUMN aba_elements_properties.name    COMMENT     `   COMMENT ON COLUMN public.aba_elements_properties.name IS 'Nombre de la propiedad del elemento';
            public       postgres    false    208            �            1259    16432    aba_formulation_id_seq    SEQUENCE     �   CREATE SEQUENCE public.aba_formulation_id_seq
    START WITH 68
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 -   DROP SEQUENCE public.aba_formulation_id_seq;
       public       postgres    false    3            �            1259    16434    aba_formulation    TABLE     �   CREATE TABLE public.aba_formulation (
    id integer DEFAULT nextval('public.aba_formulation_id_seq'::regclass) NOT NULL,
    code character varying(100),
    name character varying(100)
);
 #   DROP TABLE public.aba_formulation;
       public         postgres    false    209    3            �           0    0    TABLE aba_formulation    COMMENT     g   COMMENT ON TABLE public.aba_formulation IS 'Almacena los datos del alimento balanceado para animales';
            public       postgres    false    210            �           0    0    COLUMN aba_formulation.id    COMMENT     [   COMMENT ON COLUMN public.aba_formulation.id IS 'Id del alimento balanceado para animales';
            public       postgres    false    210            �           0    0    COLUMN aba_formulation.code    COMMENT     a   COMMENT ON COLUMN public.aba_formulation.code IS 'Codigo del alimento balanceado para animales';
            public       postgres    false    210            �           0    0    COLUMN aba_formulation.name    COMMENT     a   COMMENT ON COLUMN public.aba_formulation.name IS 'Nombre del alimento balanceado para animales';
            public       postgres    false    210            �            1259    16438    aba_results_id_seq    SEQUENCE     �   CREATE SEQUENCE public.aba_results_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 )   DROP SEQUENCE public.aba_results_id_seq;
       public       postgres    false    3            �            1259    16440    aba_results    TABLE     �   CREATE TABLE public.aba_results (
    id integer DEFAULT nextval('public.aba_results_id_seq'::regclass) NOT NULL,
    id_aba_element integer,
    quantity double precision
);
    DROP TABLE public.aba_results;
       public         postgres    false    211    3            �            1259    16444 &   aba_stages_of_breeds_and_stages_id_seq    SEQUENCE     �   CREATE SEQUENCE public.aba_stages_of_breeds_and_stages_id_seq
    START WITH 24
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 =   DROP SEQUENCE public.aba_stages_of_breeds_and_stages_id_seq;
       public       postgres    false    3            �            1259    16446    aba_stages_of_breeds_and_stages    TABLE       CREATE TABLE public.aba_stages_of_breeds_and_stages (
    id integer DEFAULT nextval('public.aba_stages_of_breeds_and_stages_id_seq'::regclass) NOT NULL,
    id_aba_breeds_and_stages integer,
    id_formulation integer,
    name character varying(100),
    duration integer
);
 3   DROP TABLE public.aba_stages_of_breeds_and_stages;
       public         postgres    false    213    3            �           0    0 %   TABLE aba_stages_of_breeds_and_stages    COMMENT     �   COMMENT ON TABLE public.aba_stages_of_breeds_and_stages IS 'Almacena las fases asociadas a los animales considerados en la tabla de consumo y mortalidad y asocia el alimento a ser proporcionado en dicha fase';
            public       postgres    false    214            �           0    0 )   COLUMN aba_stages_of_breeds_and_stages.id    COMMENT     �   COMMENT ON COLUMN public.aba_stages_of_breeds_and_stages.id IS 'Id de la fase asociadas a los animales considerados en la tabla de consumo y mortalidad ';
            public       postgres    false    214            �           0    0 ?   COLUMN aba_stages_of_breeds_and_stages.id_aba_breeds_and_stages    COMMENT     �   COMMENT ON COLUMN public.aba_stages_of_breeds_and_stages.id_aba_breeds_and_stages IS 'Id de la tabla que almacena la relacion entre proceso y consumo y mortalidad';
            public       postgres    false    214            �           0    0 5   COLUMN aba_stages_of_breeds_and_stages.id_formulation    COMMENT     �   COMMENT ON COLUMN public.aba_stages_of_breeds_and_stages.id_formulation IS 'Id del alimento balanceado para animales asociado a la fase';
            public       postgres    false    214            �           0    0 +   COLUMN aba_stages_of_breeds_and_stages.name    COMMENT     �   COMMENT ON COLUMN public.aba_stages_of_breeds_and_stages.name IS 'Nombre de la fase asociadas a los animales considerados en la tabla de consumo y mortalidad ';
            public       postgres    false    214            �           0    0 /   COLUMN aba_stages_of_breeds_and_stages.duration    COMMENT     �   COMMENT ON COLUMN public.aba_stages_of_breeds_and_stages.duration IS 'Duracion de la fase asociadas a los animales considerados en la tabla de consumo y mortalidad ';
            public       postgres    false    214            �            1259    16450    aba_time_unit    TABLE     �   CREATE TABLE public.aba_time_unit (
    id integer DEFAULT nextval('public."abaTimeUnit_id_seq"'::regclass) NOT NULL,
    singular_name character varying(100),
    plural_name character varying(100)
);
 !   DROP TABLE public.aba_time_unit;
       public         postgres    false    196    3            �           0    0    TABLE aba_time_unit    COMMENT     L   COMMENT ON TABLE public.aba_time_unit IS 'Almacena las unidades de tiempo';
            public       postgres    false    215            �           0    0    COLUMN aba_time_unit.id    COMMENT     K   COMMENT ON COLUMN public.aba_time_unit.id IS 'Id de la unidad de tiempo
';
            public       postgres    false    215            �           0    0 "   COLUMN aba_time_unit.singular_name    COMMENT     e   COMMENT ON COLUMN public.aba_time_unit.singular_name IS 'Nombre en singular de la unidad de tiempo';
            public       postgres    false    215            �           0    0     COLUMN aba_time_unit.plural_name    COMMENT     a   COMMENT ON COLUMN public.aba_time_unit.plural_name IS 'Nombre en plural de la unidad de tiempo';
            public       postgres    false    215            �            1259    16454    availability_shed_id_seq    SEQUENCE     �   CREATE SEQUENCE public.availability_shed_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.availability_shed_id_seq;
       public       postgres    false    3            �            1259    16456    base_day_id_seq    SEQUENCE     x   CREATE SEQUENCE public.base_day_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.base_day_id_seq;
       public       postgres    false    3            �            1259    16458    breed_id_seq    SEQUENCE     u   CREATE SEQUENCE public.breed_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.breed_id_seq;
       public       postgres    false    3            �            1259    16460    broiler_detail_id_seq    SEQUENCE     ~   CREATE SEQUENCE public.broiler_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.broiler_detail_id_seq;
       public       postgres    false    3            �            1259    16462    broiler_id_seq    SEQUENCE     w   CREATE SEQUENCE public.broiler_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.broiler_id_seq;
       public       postgres    false    3            �            1259    16464    broiler_product_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.broiler_product_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public.broiler_product_detail_id_seq;
       public       postgres    false    3            �            1259    16466    broiler_product_id_seq    SEQUENCE        CREATE SEQUENCE public.broiler_product_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.broiler_product_id_seq;
       public       postgres    false    3            �            1259    16468    broilereviction_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.broilereviction_detail_id_seq
    START WITH 124
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public.broilereviction_detail_id_seq;
       public       postgres    false    3            �            1259    16470    broilereviction_id_seq    SEQUENCE     �   CREATE SEQUENCE public.broilereviction_id_seq
    START WITH 70
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.broilereviction_id_seq;
       public       postgres    false    3            �            1259    16472    brooder_id_seq    SEQUENCE     w   CREATE SEQUENCE public.brooder_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.brooder_id_seq;
       public       postgres    false    3            �            1259    16474    brooder_machines_id_seq    SEQUENCE     �   CREATE SEQUENCE public.brooder_machines_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.brooder_machines_id_seq;
       public       postgres    false    3            �            1259    16476    calendar_day_id_seq    SEQUENCE     |   CREATE SEQUENCE public.calendar_day_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.calendar_day_id_seq;
       public       postgres    false    3            �            1259    16478    calendar_id_seq    SEQUENCE     x   CREATE SEQUENCE public.calendar_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.calendar_id_seq;
       public       postgres    false    3            �            1259    16480    center_id_seq    SEQUENCE     v   CREATE SEQUENCE public.center_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.center_id_seq;
       public       postgres    false    3            �            1259    16482    egg_planning_id_seq    SEQUENCE     |   CREATE SEQUENCE public.egg_planning_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.egg_planning_id_seq;
       public       postgres    false    3            �            1259    16484    egg_required_id_seq    SEQUENCE     |   CREATE SEQUENCE public.egg_required_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.egg_required_id_seq;
       public       postgres    false    3            �            1259    16486    eggs_storage_id_seq    SEQUENCE     |   CREATE SEQUENCE public.eggs_storage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.eggs_storage_id_seq;
       public       postgres    false    3            �            1259    16488    farm_id_seq    SEQUENCE     t   CREATE SEQUENCE public.farm_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE public.farm_id_seq;
       public       postgres    false    3            �            1259    16490    farm_type_id_seq    SEQUENCE     y   CREATE SEQUENCE public.farm_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.farm_type_id_seq;
       public       postgres    false    3            �            1259    16492    holiday_id_seq    SEQUENCE     w   CREATE SEQUENCE public.holiday_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.holiday_id_seq;
       public       postgres    false    3            �            1259    16494    housing_way_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.housing_way_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.housing_way_detail_id_seq;
       public       postgres    false    3            �            1259    16496    housing_way_id_seq    SEQUENCE     {   CREATE SEQUENCE public.housing_way_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.housing_way_id_seq;
       public       postgres    false    3            �            1259    16498    incubator_id_seq    SEQUENCE     y   CREATE SEQUENCE public.incubator_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.incubator_id_seq;
       public       postgres    false    3            �            1259    16500    incubator_plant_id_seq    SEQUENCE        CREATE SEQUENCE public.incubator_plant_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.incubator_plant_id_seq;
       public       postgres    false    3            �            1259    16502    industry_id_seq    SEQUENCE     x   CREATE SEQUENCE public.industry_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.industry_id_seq;
       public       postgres    false    3            �            1259    16504    line_id_seq    SEQUENCE     t   CREATE SEQUENCE public.line_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE public.line_id_seq;
       public       postgres    false    3            �            1259    16506    lot_eggs_id_seq    SEQUENCE     x   CREATE SEQUENCE public.lot_eggs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.lot_eggs_id_seq;
       public       postgres    false    3            �            1259    16508    lot_fattening_id_seq    SEQUENCE     }   CREATE SEQUENCE public.lot_fattening_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.lot_fattening_id_seq;
       public       postgres    false    3            �            1259    16510 
   lot_id_seq    SEQUENCE     s   CREATE SEQUENCE public.lot_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 !   DROP SEQUENCE public.lot_id_seq;
       public       postgres    false    3            �            1259    16512    lot_liftbreeding_id_seq    SEQUENCE     �   CREATE SEQUENCE public.lot_liftbreeding_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.lot_liftbreeding_id_seq;
       public       postgres    false    3            �            1259    16514     mdapplication_application_id_seq    SEQUENCE     �   CREATE SEQUENCE public.mdapplication_application_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999999999999
    CACHE 1;
 7   DROP SEQUENCE public.mdapplication_application_id_seq;
       public       postgres    false    3            �            1259    16516    mdapplication    TABLE     �   CREATE TABLE public.mdapplication (
    application_id integer DEFAULT nextval('public.mdapplication_application_id_seq'::regclass) NOT NULL,
    application_name character varying(30) NOT NULL,
    type character varying(20)
);
 !   DROP TABLE public.mdapplication;
       public         postgres    false    246    3            �           0    0    TABLE mdapplication    COMMENT     X   COMMENT ON TABLE public.mdapplication IS 'Almacena la informacion de las aplicaciones';
            public       postgres    false    247            �           0    0 #   COLUMN mdapplication.application_id    COMMENT     P   COMMENT ON COLUMN public.mdapplication.application_id IS 'Id de la aplicació';
            public       postgres    false    247            �           0    0 %   COLUMN mdapplication.application_name    COMMENT     W   COMMENT ON COLUMN public.mdapplication.application_name IS 'Nombre de la aplicación';
            public       postgres    false    247            �           0    0    COLUMN mdapplication.type    COMMENT     W   COMMENT ON COLUMN public.mdapplication.type IS 'A qué tipo pertenece la aplicación';
            public       postgres    false    247            �            1259    16520    mdapplication_rol_id_seq    SEQUENCE     �   CREATE SEQUENCE public.mdapplication_rol_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9999999999999
    CACHE 1;
 /   DROP SEQUENCE public.mdapplication_rol_id_seq;
       public       postgres    false    3            �            1259    16522    mdapplication_rol    TABLE     �   CREATE TABLE public.mdapplication_rol (
    id integer DEFAULT nextval('public.mdapplication_rol_id_seq'::regclass) NOT NULL,
    application_id integer NOT NULL,
    rol_id integer NOT NULL
);
 %   DROP TABLE public.mdapplication_rol;
       public         postgres    false    248    3            �           0    0    TABLE mdapplication_rol    COMMENT     _   COMMENT ON TABLE public.mdapplication_rol IS 'Contiene los id de aplicación y los id de rol';
            public       postgres    false    249            �           0    0    COLUMN mdapplication_rol.id    COMMENT     \   COMMENT ON COLUMN public.mdapplication_rol.id IS 'Id la combinacion de aplicación y rol ';
            public       postgres    false    249            �           0    0 '   COLUMN mdapplication_rol.application_id    COMMENT     `   COMMENT ON COLUMN public.mdapplication_rol.application_id IS 'Identificador de la aplicación';
            public       postgres    false    249            �           0    0    COLUMN mdapplication_rol.rol_id    COMMENT     N   COMMENT ON COLUMN public.mdapplication_rol.rol_id IS 'Identificador del rol';
            public       postgres    false    249            �            1259    16526    mdbreed    TABLE     �   CREATE TABLE public.mdbreed (
    breed_id integer DEFAULT nextval('public.breed_id_seq'::regclass) NOT NULL,
    code character varying(20) NOT NULL,
    name character varying(45) NOT NULL
);
    DROP TABLE public.mdbreed;
       public         postgres    false    218    3            �           0    0    TABLE mdbreed    COMMENT     U   COMMENT ON TABLE public.mdbreed IS 'Tabla donde se almacenan las razas de las aves';
            public       postgres    false    250            �           0    0    COLUMN mdbreed.breed_id    COMMENT     >   COMMENT ON COLUMN public.mdbreed.breed_id IS 'Id de la raza';
            public       postgres    false    250            �           0    0    COLUMN mdbreed.code    COMMENT     >   COMMENT ON COLUMN public.mdbreed.code IS 'Codigo de la raza';
            public       postgres    false    250            �           0    0    COLUMN mdbreed.name    COMMENT     >   COMMENT ON COLUMN public.mdbreed.name IS 'Nombre de la Raza';
            public       postgres    false    250            �            1259    16530    mdbroiler_product    TABLE     �   CREATE TABLE public.mdbroiler_product (
    broiler_product_id integer DEFAULT nextval('public.broiler_product_id_seq'::regclass) NOT NULL,
    name character varying(45) NOT NULL,
    days_eviction integer,
    weight double precision
);
 %   DROP TABLE public.mdbroiler_product;
       public         postgres    false    222    3            �           0    0    TABLE mdbroiler_product    COMMENT     w   COMMENT ON TABLE public.mdbroiler_product IS 'Almacena los productos de salida de la etapa de engorda hacia desalojo';
            public       postgres    false    251            �           0    0 +   COLUMN mdbroiler_product.broiler_product_id    COMMENT     ^   COMMENT ON COLUMN public.mdbroiler_product.broiler_product_id IS 'Id de producto de engorde';
            public       postgres    false    251            �           0    0    COLUMN mdbroiler_product.name    COMMENT     T   COMMENT ON COLUMN public.mdbroiler_product.name IS 'Nombre de producto de engorde';
            public       postgres    false    251            �           0    0 &   COLUMN mdbroiler_product.days_eviction    COMMENT     y   COMMENT ON COLUMN public.mdbroiler_product.days_eviction IS 'Días necesarios para el desalojo del producto de engorde';
            public       postgres    false    251            �           0    0    COLUMN mdbroiler_product.weight    COMMENT     b   COMMENT ON COLUMN public.mdbroiler_product.weight IS 'Peso estimado del producto para su salida';
            public       postgres    false    251            �            1259    16534 
   mdfarmtype    TABLE     �   CREATE TABLE public.mdfarmtype (
    farm_type_id integer DEFAULT nextval('public.farm_type_id_seq'::regclass) NOT NULL,
    name character varying(45) NOT NULL
);
    DROP TABLE public.mdfarmtype;
       public         postgres    false    234    3            �           0    0    TABLE mdfarmtype    COMMENT     D   COMMENT ON TABLE public.mdfarmtype IS 'Define los tipos de granja';
            public       postgres    false    252            �           0    0    COLUMN mdfarmtype.farm_type_id    COMMENT     L   COMMENT ON COLUMN public.mdfarmtype.farm_type_id IS 'Id de tipo de granja';
            public       postgres    false    252            �           0    0    COLUMN mdfarmtype.name    COMMENT     O   COMMENT ON COLUMN public.mdfarmtype.name IS 'Nombre de la etapa de la granja';
            public       postgres    false    252            �            1259    16538    measure_id_seq    SEQUENCE     w   CREATE SEQUENCE public.measure_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.measure_id_seq;
       public       postgres    false    3            �            1259    16540 	   mdmeasure    TABLE     $  CREATE TABLE public.mdmeasure (
    measure_id integer DEFAULT nextval('public.measure_id_seq'::regclass) NOT NULL,
    name character varying(10) NOT NULL,
    abbreviation character varying(5) NOT NULL,
    originvalue double precision,
    valuekg double precision,
    is_unit boolean
);
    DROP TABLE public.mdmeasure;
       public         postgres    false    253    3            �           0    0    TABLE mdmeasure    COMMENT     _   COMMENT ON TABLE public.mdmeasure IS 'Almacena las medidas a utilizar en las planificaciones';
            public       postgres    false    254            �           0    0    COLUMN mdmeasure.measure_id    COMMENT     D   COMMENT ON COLUMN public.mdmeasure.measure_id IS 'Id de la medida';
            public       postgres    false    254            �           0    0    COLUMN mdmeasure.name    COMMENT     B   COMMENT ON COLUMN public.mdmeasure.name IS 'Nombre de la medida';
            public       postgres    false    254            �           0    0    COLUMN mdmeasure.abbreviation    COMMENT     O   COMMENT ON COLUMN public.mdmeasure.abbreviation IS 'Abreviatura de la medida';
            public       postgres    false    254            �           0    0    COLUMN mdmeasure.originvalue    COMMENT     Q   COMMENT ON COLUMN public.mdmeasure.originvalue IS 'Valor original de la medida';
            public       postgres    false    254            �           0    0    COLUMN mdmeasure.valuekg    COMMENT     R   COMMENT ON COLUMN public.mdmeasure.valuekg IS 'Valor en Kilogramos de la medida';
            public       postgres    false    254            �            1259    16544    parameter_id_seq    SEQUENCE     y   CREATE SEQUENCE public.parameter_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.parameter_id_seq;
       public       postgres    false    3                        1259    16546    mdparameter    TABLE     '  CREATE TABLE public.mdparameter (
    parameter_id integer DEFAULT nextval('public.parameter_id_seq'::regclass) NOT NULL,
    description character varying(250) NOT NULL,
    type character varying(10),
    measure_id integer,
    process_id integer,
    name character varying(250) NOT NULL
);
    DROP TABLE public.mdparameter;
       public         postgres    false    255    3            �           0    0    TABLE mdparameter    COMMENT     �   COMMENT ON TABLE public.mdparameter IS 'Almacena la definición de los parámetros a utilizar en la planificación regresiva junto a sus respectivas características';
            public       postgres    false    256            �           0    0    COLUMN mdparameter.parameter_id    COMMENT     N   COMMENT ON COLUMN public.mdparameter.parameter_id IS 'Id de los parámetros';
            public       postgres    false    256            �           0    0    COLUMN mdparameter.description    COMMENT     W   COMMENT ON COLUMN public.mdparameter.description IS 'Descripción de los parámetros';
            public       postgres    false    256            �           0    0    COLUMN mdparameter.type    COMMENT     D   COMMENT ON COLUMN public.mdparameter.type IS 'Tipo de parámetros';
            public       postgres    false    256            �           0    0    COLUMN mdparameter.measure_id    COMMENT     F   COMMENT ON COLUMN public.mdparameter.measure_id IS 'Id de la medida';
            public       postgres    false    256            �           0    0    COLUMN mdparameter.process_id    COMMENT     E   COMMENT ON COLUMN public.mdparameter.process_id IS 'Id del proceso';
            public       postgres    false    256            �           0    0    COLUMN mdparameter.name    COMMENT     F   COMMENT ON COLUMN public.mdparameter.name IS 'Nombre del parámetro';
            public       postgres    false    256                       1259    16553    process_id_seq    SEQUENCE     w   CREATE SEQUENCE public.process_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.process_id_seq;
       public       postgres    false    3                       1259    16555 	   mdprocess    TABLE     B  CREATE TABLE public.mdprocess (
    process_id integer DEFAULT nextval('public.process_id_seq'::regclass) NOT NULL,
    process_order integer NOT NULL,
    product_id integer NOT NULL,
    stage_id integer NOT NULL,
    historical_decrease double precision NOT NULL,
    theoretical_decrease double precision NOT NULL,
    historical_weight double precision NOT NULL,
    theoretical_weight double precision NOT NULL,
    historical_duration integer NOT NULL,
    theoretical_duration integer NOT NULL,
    calendar_id integer NOT NULL,
    visible boolean,
    name character varying(250) NOT NULL,
    predecessor_id integer,
    capacity integer NOT NULL,
    breed_id integer NOT NULL,
    gender character varying(30),
    fattening_goal double precision,
    type_posture character varying(30),
    biological_active boolean
);
    DROP TABLE public.mdprocess;
       public         postgres    false    257    3            �           0    0    TABLE mdprocess    COMMENT     �   COMMENT ON TABLE public.mdprocess IS 'Almacena los procesos definidos para la planificación progresiva junto a sus respectivas características';
            public       postgres    false    258            �           0    0    COLUMN mdprocess.process_id    COMMENT     G   COMMENT ON COLUMN public.mdprocess.process_id IS 'Id de los procesos';
            public       postgres    false    258            �           0    0    COLUMN mdprocess.process_order    COMMENT     M   COMMENT ON COLUMN public.mdprocess.process_order IS 'Orden de los procesos';
            public       postgres    false    258            �           0    0    COLUMN mdprocess.product_id    COMMENT     D   COMMENT ON COLUMN public.mdprocess.product_id IS 'Id del producto';
            public       postgres    false    258            �           0    0    COLUMN mdprocess.stage_id    COMMENT     >   COMMENT ON COLUMN public.mdprocess.stage_id IS 'Id de etapa';
            public       postgres    false    258            �           0    0 $   COLUMN mdprocess.historical_decrease    COMMENT     Y   COMMENT ON COLUMN public.mdprocess.historical_decrease IS 'Merma historica del proceso';
            public       postgres    false    258            �           0    0 %   COLUMN mdprocess.theoretical_decrease    COMMENT     Y   COMMENT ON COLUMN public.mdprocess.theoretical_decrease IS 'Merma teórica del proceso';
            public       postgres    false    258            �           0    0 "   COLUMN mdprocess.historical_weight    COMMENT     V   COMMENT ON COLUMN public.mdprocess.historical_weight IS 'Peso historico del proceso';
            public       postgres    false    258            �           0    0 #   COLUMN mdprocess.theoretical_weight    COMMENT     V   COMMENT ON COLUMN public.mdprocess.theoretical_weight IS 'Peso teórico del proceso';
            public       postgres    false    258            �           0    0 $   COLUMN mdprocess.historical_duration    COMMENT     ^   COMMENT ON COLUMN public.mdprocess.historical_duration IS 'Duración histórica del proceso';
            public       postgres    false    258            �           0    0 %   COLUMN mdprocess.theoretical_duration    COMMENT     ]   COMMENT ON COLUMN public.mdprocess.theoretical_duration IS 'Duración teórica del proceso';
            public       postgres    false    258            �           0    0    COLUMN mdprocess.calendar_id    COMMENT     G   COMMENT ON COLUMN public.mdprocess.calendar_id IS 'Id del calendario';
            public       postgres    false    258            �           0    0    COLUMN mdprocess.visible    COMMENT     I   COMMENT ON COLUMN public.mdprocess.visible IS 'Visibilidad del proceso';
            public       postgres    false    258            �           0    0    COLUMN mdprocess.name    COMMENT     A   COMMENT ON COLUMN public.mdprocess.name IS 'Nombre del proceso';
            public       postgres    false    258            �           0    0    COLUMN mdprocess.predecessor_id    COMMENT     J   COMMENT ON COLUMN public.mdprocess.predecessor_id IS 'Id del predecesor';
            public       postgres    false    258            �           0    0    COLUMN mdprocess.capacity    COMMENT     X   COMMENT ON COLUMN public.mdprocess.capacity IS 'Capacidad semanal asociada al proceso';
            public       postgres    false    258                        0    0    COLUMN mdprocess.breed_id    COMMENT     @   COMMENT ON COLUMN public.mdprocess.breed_id IS 'Id de la raza';
            public       postgres    false    258                       0    0    COLUMN mdprocess.gender    COMMENT     N   COMMENT ON COLUMN public.mdprocess.gender IS 'Genero del producto de salida';
            public       postgres    false    258                       0    0    COLUMN mdprocess.fattening_goal    COMMENT     H   COMMENT ON COLUMN public.mdprocess.fattening_goal IS 'Meta de engorde';
            public       postgres    false    258                       0    0    COLUMN mdprocess.type_posture    COMMENT     s   COMMENT ON COLUMN public.mdprocess.type_posture IS 'Define el tipo de postura de acuerdo a la edad de la gallina';
            public       postgres    false    258                       0    0 "   COLUMN mdprocess.biological_active    COMMENT     h   COMMENT ON COLUMN public.mdprocess.biological_active IS 'Define si el proceso es un activo biológico';
            public       postgres    false    258                       1259    16559    product_id_seq    SEQUENCE     w   CREATE SEQUENCE public.product_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.product_id_seq;
       public       postgres    false    3                       1259    16561 	   mdproduct    TABLE     �   CREATE TABLE public.mdproduct (
    product_id integer DEFAULT nextval('public.product_id_seq'::regclass) NOT NULL,
    code character varying(20) NOT NULL,
    name character varying(45) NOT NULL
);
    DROP TABLE public.mdproduct;
       public         postgres    false    259    3                       0    0    TABLE mdproduct    COMMENT     Z   COMMENT ON TABLE public.mdproduct IS 'Almacena los productos utilizados en los procesos';
            public       postgres    false    260                       0    0    COLUMN mdproduct.product_id    COMMENT     D   COMMENT ON COLUMN public.mdproduct.product_id IS 'Id del producto';
            public       postgres    false    260                       0    0    COLUMN mdproduct.code    COMMENT     B   COMMENT ON COLUMN public.mdproduct.code IS 'Codigo del producto';
            public       postgres    false    260                       0    0    COLUMN mdproduct.name    COMMENT     B   COMMENT ON COLUMN public.mdproduct.name IS 'Nombre del producto';
            public       postgres    false    260                       1259    16565    mdrol_rol_id_seq    SEQUENCE        CREATE SEQUENCE public.mdrol_rol_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 10000000
    CACHE 1;
 '   DROP SEQUENCE public.mdrol_rol_id_seq;
       public       postgres    false    3                       1259    16567    mdrol    TABLE     	  CREATE TABLE public.mdrol (
    rol_id integer DEFAULT nextval('public.mdrol_rol_id_seq'::regclass) NOT NULL,
    rol_name character varying(80) NOT NULL,
    admin_user_creator character varying(80) NOT NULL,
    creation_date timestamp with time zone NOT NULL
);
    DROP TABLE public.mdrol;
       public         postgres    false    261    3            	           0    0    TABLE mdrol    COMMENT     O   COMMENT ON TABLE public.mdrol IS 'Almacena los datos de los diferentes roles';
            public       postgres    false    262            
           0    0    COLUMN mdrol.rol_id    COMMENT     7   COMMENT ON COLUMN public.mdrol.rol_id IS 'Id del rol';
            public       postgres    false    262                       0    0    COLUMN mdrol.rol_name    COMMENT     =   COMMENT ON COLUMN public.mdrol.rol_name IS 'Nombre del rol';
            public       postgres    false    262                       0    0    COLUMN mdrol.admin_user_creator    COMMENT     [   COMMENT ON COLUMN public.mdrol.admin_user_creator IS 'Especifica que usuario creo el rol';
            public       postgres    false    262                       0    0    COLUMN mdrol.creation_date    COMMENT     N   COMMENT ON COLUMN public.mdrol.creation_date IS 'Fecha de creación del rol';
            public       postgres    false    262                       1259    16571    scenario_id_seq    SEQUENCE     x   CREATE SEQUENCE public.scenario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.scenario_id_seq;
       public       postgres    false    3                       1259    16573 
   mdscenario    TABLE     d  CREATE TABLE public.mdscenario (
    scenario_id integer DEFAULT nextval('public.scenario_id_seq'::regclass) NOT NULL,
    description character varying(250) NOT NULL,
    date_start timestamp with time zone,
    date_end timestamp with time zone,
    name character varying(250) NOT NULL,
    status integer DEFAULT 0,
    calendar_id integer NOT NULL
);
    DROP TABLE public.mdscenario;
       public         postgres    false    263    3                       0    0    TABLE mdscenario    COMMENT     [   COMMENT ON TABLE public.mdscenario IS 'Almacena información de los distintos escenarios';
            public       postgres    false    264                       0    0    COLUMN mdscenario.scenario_id    COMMENT     G   COMMENT ON COLUMN public.mdscenario.scenario_id IS 'Id del escenario';
            public       postgres    false    264                       0    0    COLUMN mdscenario.description    COMMENT     P   COMMENT ON COLUMN public.mdscenario.description IS 'Descripcion del escenario';
            public       postgres    false    264                       0    0    COLUMN mdscenario.date_start    COMMENT     S   COMMENT ON COLUMN public.mdscenario.date_start IS 'Fecha de inicio del escenario';
            public       postgres    false    264                       0    0    COLUMN mdscenario.date_end    COMMENT     N   COMMENT ON COLUMN public.mdscenario.date_end IS 'Fecha de fin del escenario';
            public       postgres    false    264                       0    0    COLUMN mdscenario.name    COMMENT     D   COMMENT ON COLUMN public.mdscenario.name IS 'Nombre del escenario';
            public       postgres    false    264                       0    0    COLUMN mdscenario.status    COMMENT     F   COMMENT ON COLUMN public.mdscenario.status IS 'Estado del escenario';
            public       postgres    false    264            	           1259    16581    status_shed_id_seq    SEQUENCE     {   CREATE SEQUENCE public.status_shed_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.status_shed_id_seq;
       public       postgres    false    3            
           1259    16583    mdshedstatus    TABLE     �   CREATE TABLE public.mdshedstatus (
    shed_status_id integer DEFAULT nextval('public.status_shed_id_seq'::regclass) NOT NULL,
    name character varying(50) NOT NULL,
    description character varying(250) NOT NULL
);
     DROP TABLE public.mdshedstatus;
       public         postgres    false    265    3                       0    0    TABLE mdshedstatus    COMMENT     b   COMMENT ON TABLE public.mdshedstatus IS 'Almaceno los estatus de disponibilidad de los galpones';
            public       postgres    false    266                       0    0 "   COLUMN mdshedstatus.shed_status_id    COMMENT     T   COMMENT ON COLUMN public.mdshedstatus.shed_status_id IS 'Id del estado del galpon';
            public       postgres    false    266                       0    0    COLUMN mdshedstatus.name    COMMENT     a   COMMENT ON COLUMN public.mdshedstatus.name IS 'Nombre del estado en que se encuentra el galpon';
            public       postgres    false    266                       0    0    COLUMN mdshedstatus.description    COMMENT     [   COMMENT ON COLUMN public.mdshedstatus.description IS 'Descripcion del estado del galpon
';
            public       postgres    false    266                       1259    16587    stage_id_seq    SEQUENCE     u   CREATE SEQUENCE public.stage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.stage_id_seq;
       public       postgres    false    3                       1259    16589    mdstage    TABLE     �   CREATE TABLE public.mdstage (
    stage_id integer DEFAULT nextval('public.stage_id_seq'::regclass) NOT NULL,
    order_ integer,
    name character varying(250) NOT NULL
);
    DROP TABLE public.mdstage;
       public         postgres    false    267    3                       0    0    TABLE mdstage    COMMENT     d   COMMENT ON TABLE public.mdstage IS 'Almacena las etapas a utilizar en el proceso de planificacion';
            public       postgres    false    268                       0    0    COLUMN mdstage.stage_id    COMMENT     ?   COMMENT ON COLUMN public.mdstage.stage_id IS 'Id de la etapa';
            public       postgres    false    268                       0    0    COLUMN mdstage.order_    COMMENT     U   COMMENT ON COLUMN public.mdstage.order_ IS 'Orden en el que se muestras las etapas';
            public       postgres    false    268                       0    0    COLUMN mdstage.name    COMMENT     ?   COMMENT ON COLUMN public.mdstage.name IS 'Nombre de la etapa';
            public       postgres    false    268                       1259    16593    mduser_user_id_seq    SEQUENCE     �   CREATE SEQUENCE public.mduser_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9999999999
    CACHE 1;
 )   DROP SEQUENCE public.mduser_user_id_seq;
       public       postgres    false    3                       1259    16595    mduser    TABLE     �  CREATE TABLE public.mduser (
    user_id integer DEFAULT nextval('public.mduser_user_id_seq'::regclass) NOT NULL,
    username character varying(80) NOT NULL,
    password character varying(80) NOT NULL,
    name character varying(80) NOT NULL,
    lastname character varying(80) NOT NULL,
    active boolean NOT NULL,
    admi_user_creator character varying(80) NOT NULL,
    rol_id integer NOT NULL,
    creation_date timestamp with time zone NOT NULL
);
    DROP TABLE public.mduser;
       public         postgres    false    269    3                       1259    16599    oscenter    TABLE       CREATE TABLE public.oscenter (
    center_id integer DEFAULT nextval('public.center_id_seq'::regclass) NOT NULL,
    partnership_id integer NOT NULL,
    farm_id integer NOT NULL,
    name character varying(45) NOT NULL,
    code character varying(20) NOT NULL
);
    DROP TABLE public.oscenter;
       public         postgres    false    229    3                       0    0    TABLE oscenter    COMMENT     S   COMMENT ON TABLE public.oscenter IS 'Almacena los datos referentes a los nucleos';
            public       postgres    false    271                       0    0    COLUMN oscenter.center_id    COMMENT     @   COMMENT ON COLUMN public.oscenter.center_id IS 'Id del nucleo';
            public       postgres    false    271                       0    0    COLUMN oscenter.partnership_id    COMMENT     H   COMMENT ON COLUMN public.oscenter.partnership_id IS 'Id de la empresa';
            public       postgres    false    271                        0    0    COLUMN oscenter.farm_id    COMMENT     @   COMMENT ON COLUMN public.oscenter.farm_id IS 'Id de la granja';
            public       postgres    false    271            !           0    0    COLUMN oscenter.name    COMMENT     @   COMMENT ON COLUMN public.oscenter.name IS 'Nombre del nucleo
';
            public       postgres    false    271            "           0    0    COLUMN oscenter.code    COMMENT     ?   COMMENT ON COLUMN public.oscenter.code IS 'Codigo del nucleo';
            public       postgres    false    271                       1259    16603    oscenter_oswarehouse    TABLE     �   CREATE TABLE public.oscenter_oswarehouse (
    client_id integer NOT NULL,
    partnership_id integer NOT NULL,
    farm_id integer NOT NULL,
    center_id integer NOT NULL,
    warehouse_id integer NOT NULL,
    delete_mark integer
);
 (   DROP TABLE public.oscenter_oswarehouse;
       public         postgres    false    3            #           0    0    TABLE oscenter_oswarehouse    COMMENT     p   COMMENT ON TABLE public.oscenter_oswarehouse IS 'Relación que une los núcleos con sus respectivos almacenes';
            public       postgres    false    272            $           0    0 %   COLUMN oscenter_oswarehouse.client_id    COMMENT     M   COMMENT ON COLUMN public.oscenter_oswarehouse.client_id IS 'Id del cliente';
            public       postgres    false    272            %           0    0 *   COLUMN oscenter_oswarehouse.partnership_id    COMMENT     T   COMMENT ON COLUMN public.oscenter_oswarehouse.partnership_id IS 'Id de la empresa';
            public       postgres    false    272            &           0    0 #   COLUMN oscenter_oswarehouse.farm_id    COMMENT     L   COMMENT ON COLUMN public.oscenter_oswarehouse.farm_id IS 'Id de la granja';
            public       postgres    false    272            '           0    0 %   COLUMN oscenter_oswarehouse.center_id    COMMENT     L   COMMENT ON COLUMN public.oscenter_oswarehouse.center_id IS 'Id del nucleo';
            public       postgres    false    272            (           0    0 (   COLUMN oscenter_oswarehouse.warehouse_id    COMMENT     P   COMMENT ON COLUMN public.oscenter_oswarehouse.warehouse_id IS 'Id del almacen';
            public       postgres    false    272            )           0    0 '   COLUMN oscenter_oswarehouse.delete_mark    COMMENT     Q   COMMENT ON COLUMN public.oscenter_oswarehouse.delete_mark IS 'Marca de borrado';
            public       postgres    false    272                       1259    16606    osfarm    TABLE     �   CREATE TABLE public.osfarm (
    farm_id integer DEFAULT nextval('public.farm_id_seq'::regclass) NOT NULL,
    partnership_id integer,
    code character varying(20) NOT NULL,
    name character varying(45) NOT NULL,
    farm_type_id integer NOT NULL
);
    DROP TABLE public.osfarm;
       public         postgres    false    233    3            *           0    0    TABLE osfarm    COMMENT     p   COMMENT ON TABLE public.osfarm IS 'Almacena la información de la granja con sus respectivas características';
            public       postgres    false    273            +           0    0    COLUMN osfarm.farm_id    COMMENT     >   COMMENT ON COLUMN public.osfarm.farm_id IS 'Id de la granja';
            public       postgres    false    273            ,           0    0    COLUMN osfarm.partnership_id    COMMENT     F   COMMENT ON COLUMN public.osfarm.partnership_id IS 'Id de la empresa';
            public       postgres    false    273            -           0    0    COLUMN osfarm.code    COMMENT     ?   COMMENT ON COLUMN public.osfarm.code IS 'Codigo de la granja';
            public       postgres    false    273            .           0    0    COLUMN osfarm.name    COMMENT     ?   COMMENT ON COLUMN public.osfarm.name IS 'Nombre de la granja';
            public       postgres    false    273            /           0    0    COLUMN osfarm.farm_type_id    COMMENT     I   COMMENT ON COLUMN public.osfarm.farm_type_id IS 'Id del tipo de granja';
            public       postgres    false    273                       1259    16610    osincubator    TABLE     �  CREATE TABLE public.osincubator (
    incubator_id integer DEFAULT nextval('public.incubator_id_seq'::regclass) NOT NULL,
    incubator_plant_id integer,
    name character varying(45) NOT NULL,
    code character varying(20) NOT NULL,
    description character varying(250) NOT NULL,
    capacity integer,
    sunday integer,
    monday integer,
    tuesday integer,
    wednesday integer,
    thursday integer,
    friday integer,
    saturday integer,
    available integer
);
    DROP TABLE public.osincubator;
       public         postgres    false    238    3            0           0    0    TABLE osincubator    COMMENT     y   COMMENT ON TABLE public.osincubator IS 'Almacena las máquinas de incubación pertenecientes a cada una de las plantas';
            public       postgres    false    274            1           0    0    COLUMN osincubator.incubator_id    COMMENT     L   COMMENT ON COLUMN public.osincubator.incubator_id IS 'Id de la incubadora';
            public       postgres    false    274            2           0    0 %   COLUMN osincubator.incubator_plant_id    COMMENT     Y   COMMENT ON COLUMN public.osincubator.incubator_plant_id IS 'Id de la planta incubadora';
            public       postgres    false    274            3           0    0    COLUMN osincubator.name    COMMENT     H   COMMENT ON COLUMN public.osincubator.name IS 'Nombre de la incubadora';
            public       postgres    false    274            4           0    0    COLUMN osincubator.code    COMMENT     H   COMMENT ON COLUMN public.osincubator.code IS 'Codigo de la incubadora';
            public       postgres    false    274            5           0    0    COLUMN osincubator.description    COMMENT     T   COMMENT ON COLUMN public.osincubator.description IS 'Descripcion de la incubadora';
            public       postgres    false    274            6           0    0    COLUMN osincubator.capacity    COMMENT     O   COMMENT ON COLUMN public.osincubator.capacity IS 'Capacidad de la incubadora';
            public       postgres    false    274            7           0    0    COLUMN osincubator.sunday    COMMENT     ]   COMMENT ON COLUMN public.osincubator.sunday IS 'Marca los dias de trabajo de la incubadora';
            public       postgres    false    274            8           0    0    COLUMN osincubator.monday    COMMENT     ^   COMMENT ON COLUMN public.osincubator.monday IS 'Marca los días de trabajo de la incubadora';
            public       postgres    false    274            9           0    0    COLUMN osincubator.tuesday    COMMENT     _   COMMENT ON COLUMN public.osincubator.tuesday IS 'Marca los días de trabajo de la incubadora';
            public       postgres    false    274            :           0    0    COLUMN osincubator.wednesday    COMMENT     a   COMMENT ON COLUMN public.osincubator.wednesday IS 'Marca los días de trabajo de la incubadora';
            public       postgres    false    274            ;           0    0    COLUMN osincubator.thursday    COMMENT     `   COMMENT ON COLUMN public.osincubator.thursday IS 'Marca los días de trabajo de la incubadora';
            public       postgres    false    274            <           0    0    COLUMN osincubator.friday    COMMENT     ^   COMMENT ON COLUMN public.osincubator.friday IS 'Marca los días de trabajo de la incubadora';
            public       postgres    false    274            =           0    0    COLUMN osincubator.saturday    COMMENT     `   COMMENT ON COLUMN public.osincubator.saturday IS 'Marca los días de trabajo de la incubadora';
            public       postgres    false    274                       1259    16614    osincubatorplant    TABLE     �  CREATE TABLE public.osincubatorplant (
    incubator_plant_id integer DEFAULT nextval('public.incubator_plant_id_seq'::regclass) NOT NULL,
    name character varying(45) NOT NULL,
    code character varying(20) NOT NULL,
    description character varying(250),
    partnership_id integer,
    max_storage integer,
    min_storage integer,
    acclimatized boolean,
    suitable boolean,
    expired boolean
);
 $   DROP TABLE public.osincubatorplant;
       public         postgres    false    239    3            >           0    0    TABLE osincubatorplant    COMMENT     }   COMMENT ON TABLE public.osincubatorplant IS 'Almacena la información de la planta incubadora perteneciente a cada empresa';
            public       postgres    false    275            ?           0    0 *   COLUMN osincubatorplant.incubator_plant_id    COMMENT     ^   COMMENT ON COLUMN public.osincubatorplant.incubator_plant_id IS 'Id de la planta incubadora';
            public       postgres    false    275            @           0    0    COLUMN osincubatorplant.name    COMMENT     T   COMMENT ON COLUMN public.osincubatorplant.name IS 'Nombre de la planta incubadora';
            public       postgres    false    275            A           0    0    COLUMN osincubatorplant.code    COMMENT     T   COMMENT ON COLUMN public.osincubatorplant.code IS 'Codigo de la planta incubadora';
            public       postgres    false    275            B           0    0 #   COLUMN osincubatorplant.description    COMMENT     a   COMMENT ON COLUMN public.osincubatorplant.description IS 'Descripción de la planta incubadora';
            public       postgres    false    275            C           0    0 &   COLUMN osincubatorplant.partnership_id    COMMENT     P   COMMENT ON COLUMN public.osincubatorplant.partnership_id IS 'Id de la empresa';
            public       postgres    false    275            D           0    0 #   COLUMN osincubatorplant.max_storage    COMMENT     ]   COMMENT ON COLUMN public.osincubatorplant.max_storage IS 'Numero máximo de almacenamiento';
            public       postgres    false    275            E           0    0 #   COLUMN osincubatorplant.min_storage    COMMENT     \   COMMENT ON COLUMN public.osincubatorplant.min_storage IS 'Numero minimo de almacenamiento';
            public       postgres    false    275                       1259    16618    partnership_id_seq    SEQUENCE     {   CREATE SEQUENCE public.partnership_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.partnership_id_seq;
       public       postgres    false    3                       1259    16620    ospartnership    TABLE     2  CREATE TABLE public.ospartnership (
    partnership_id integer DEFAULT nextval('public.partnership_id_seq'::regclass) NOT NULL,
    name character varying(45) NOT NULL,
    address character varying(250) NOT NULL,
    description character varying(250) NOT NULL,
    code character varying(20) NOT NULL
);
 !   DROP TABLE public.ospartnership;
       public         postgres    false    276    3            F           0    0    TABLE ospartnership    COMMENT     j   COMMENT ON TABLE public.ospartnership IS 'Almacena la información referente a las empresas registradas';
            public       postgres    false    277            G           0    0 #   COLUMN ospartnership.partnership_id    COMMENT     M   COMMENT ON COLUMN public.ospartnership.partnership_id IS 'Id de la empresa';
            public       postgres    false    277            H           0    0    COLUMN ospartnership.name    COMMENT     G   COMMENT ON COLUMN public.ospartnership.name IS 'Nombre de la empresa';
            public       postgres    false    277            I           0    0    COLUMN ospartnership.address    COMMENT     M   COMMENT ON COLUMN public.ospartnership.address IS 'Direccion de la empresa';
            public       postgres    false    277            J           0    0     COLUMN ospartnership.description    COMMENT     T   COMMENT ON COLUMN public.ospartnership.description IS 'Descripción de la empresa';
            public       postgres    false    277            K           0    0    COLUMN ospartnership.code    COMMENT     G   COMMENT ON COLUMN public.ospartnership.code IS 'Codigo de la empresa';
            public       postgres    false    277                       1259    16627    shed_id_seq    SEQUENCE     t   CREATE SEQUENCE public.shed_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE public.shed_id_seq;
       public       postgres    false    3                       1259    16629    osshed    TABLE     �  CREATE TABLE public.osshed (
    shed_id integer DEFAULT nextval('public.shed_id_seq'::regclass) NOT NULL,
    partnership_id integer NOT NULL,
    farm_id integer NOT NULL,
    center_id integer NOT NULL,
    code character varying(20) NOT NULL,
    statusshed_id integer NOT NULL,
    type_id integer,
    building_date date,
    stall_width double precision NOT NULL,
    stall_height double precision NOT NULL,
    capacity_min double precision NOT NULL,
    capacity_max double precision NOT NULL,
    environment_id integer,
    rotation_days integer DEFAULT 0 NOT NULL,
    nests_quantity integer DEFAULT 0,
    cages_quantity integer DEFAULT 0,
    birds_quantity integer DEFAULT 0,
    capacity_theoretical integer DEFAULT 0,
    avaliable_date date
);
    DROP TABLE public.osshed;
       public         postgres    false    278    3            L           0    0    TABLE osshed    COMMENT     d   COMMENT ON TABLE public.osshed IS 'Almacena la informacion de los galpones asociados a la empresa';
            public       postgres    false    279            M           0    0    COLUMN osshed.shed_id    COMMENT     <   COMMENT ON COLUMN public.osshed.shed_id IS 'Id del galpon';
            public       postgres    false    279            N           0    0    COLUMN osshed.partnership_id    COMMENT     F   COMMENT ON COLUMN public.osshed.partnership_id IS 'Id de la empresa';
            public       postgres    false    279            O           0    0    COLUMN osshed.farm_id    COMMENT     >   COMMENT ON COLUMN public.osshed.farm_id IS 'Id de la granja';
            public       postgres    false    279            P           0    0    COLUMN osshed.center_id    COMMENT     >   COMMENT ON COLUMN public.osshed.center_id IS 'Id del nucleo';
            public       postgres    false    279            Q           0    0    COLUMN osshed.code    COMMENT     =   COMMENT ON COLUMN public.osshed.code IS 'Codigo del galpon';
            public       postgres    false    279            R           0    0    COLUMN osshed.statusshed_id    COMMENT     _   COMMENT ON COLUMN public.osshed.statusshed_id IS 'Identificador del estado actual del galpon';
            public       postgres    false    279            S           0    0    COLUMN osshed.type_id    COMMENT     D   COMMENT ON COLUMN public.osshed.type_id IS 'Id del tipo de galpon';
            public       postgres    false    279            T           0    0    COLUMN osshed.building_date    COMMENT     c   COMMENT ON COLUMN public.osshed.building_date IS 'Almacena la fecha de construccion del edificio';
            public       postgres    false    279            U           0    0    COLUMN osshed.stall_width    COMMENT     M   COMMENT ON COLUMN public.osshed.stall_width IS 'Indica el ancho del galpon';
            public       postgres    false    279            V           0    0    COLUMN osshed.stall_height    COMMENT     M   COMMENT ON COLUMN public.osshed.stall_height IS 'Indica el alto del galpon';
            public       postgres    false    279            W           0    0    COLUMN osshed.capacity_min    COMMENT     D   COMMENT ON COLUMN public.osshed.capacity_min IS 'Capacidad minima';
            public       postgres    false    279            X           0    0    COLUMN osshed.capacity_max    COMMENT     F   COMMENT ON COLUMN public.osshed.capacity_max IS 'Capacidad máxima ';
            public       postgres    false    279            Y           0    0    COLUMN osshed.environment_id    COMMENT     E   COMMENT ON COLUMN public.osshed.environment_id IS 'Id del ambiente';
            public       postgres    false    279            Z           0    0    COLUMN osshed.rotation_days    COMMENT     H   COMMENT ON COLUMN public.osshed.rotation_days IS 'Días de rotación
';
            public       postgres    false    279            [           0    0    COLUMN osshed.nests_quantity    COMMENT     I   COMMENT ON COLUMN public.osshed.nests_quantity IS 'Cantidad de nidales';
            public       postgres    false    279            \           0    0    COLUMN osshed.cages_quantity    COMMENT     H   COMMENT ON COLUMN public.osshed.cages_quantity IS 'Cantidad de jaulas';
            public       postgres    false    279            ]           0    0    COLUMN osshed.birds_quantity    COMMENT     F   COMMENT ON COLUMN public.osshed.birds_quantity IS 'Cantidad de aves';
            public       postgres    false    279            ^           0    0 "   COLUMN osshed.capacity_theoretical    COMMENT     O   COMMENT ON COLUMN public.osshed.capacity_theoretical IS '	Capacidad teórica';
            public       postgres    false    279                       1259    16638    silo_id_seq    SEQUENCE     t   CREATE SEQUENCE public.silo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE public.silo_id_seq;
       public       postgres    false    3                       1259    16640    ossilo    TABLE     �  CREATE TABLE public.ossilo (
    silo_id integer DEFAULT nextval('public.silo_id_seq'::regclass) NOT NULL,
    client_id integer,
    partnership_id integer NOT NULL,
    farm_id integer NOT NULL,
    center_id integer NOT NULL,
    name character varying(45) NOT NULL,
    rings_height double precision,
    rings_height_id integer,
    height double precision NOT NULL,
    height_unit_id integer,
    diameter double precision NOT NULL,
    diameter_unit_id integer,
    total_rings_quantity integer,
    measuring_mechanism integer,
    cone_degrees double precision,
    total_capacity_1 double precision,
    total_capacity_2 double precision,
    capacity_unit_id_1 integer,
    capacity_unit_id_2 integer,
    central character varying(20)
);
    DROP TABLE public.ossilo;
       public         postgres    false    280    3            _           0    0    TABLE ossilo    COMMENT     E   COMMENT ON TABLE public.ossilo IS 'Almacena los datos de los silos';
            public       postgres    false    281            `           0    0    COLUMN ossilo.silo_id    COMMENT     :   COMMENT ON COLUMN public.ossilo.silo_id IS 'Id del silo';
            public       postgres    false    281            a           0    0    COLUMN ossilo.client_id    COMMENT     ?   COMMENT ON COLUMN public.ossilo.client_id IS 'Id del cliente';
            public       postgres    false    281            b           0    0    COLUMN ossilo.partnership_id    COMMENT     F   COMMENT ON COLUMN public.ossilo.partnership_id IS 'Id de la empresa';
            public       postgres    false    281            c           0    0    COLUMN ossilo.farm_id    COMMENT     >   COMMENT ON COLUMN public.ossilo.farm_id IS 'Id de la granja';
            public       postgres    false    281            d           0    0    COLUMN ossilo.center_id    COMMENT     >   COMMENT ON COLUMN public.ossilo.center_id IS 'Id del nucleo';
            public       postgres    false    281            e           0    0    COLUMN ossilo.name    COMMENT     ;   COMMENT ON COLUMN public.ossilo.name IS 'Nombre del silo';
            public       postgres    false    281            f           0    0    COLUMN ossilo.rings_height    COMMENT     E   COMMENT ON COLUMN public.ossilo.rings_height IS 'Numero de anillos';
            public       postgres    false    281            g           0    0    COLUMN ossilo.rings_height_id    COMMENT     R   COMMENT ON COLUMN public.ossilo.rings_height_id IS 'Unidad de medida del anillo';
            public       postgres    false    281            h           0    0    COLUMN ossilo.height    COMMENT     =   COMMENT ON COLUMN public.ossilo.height IS 'Altura del silo';
            public       postgres    false    281            i           0    0    COLUMN ossilo.height_unit_id    COMMENT     R   COMMENT ON COLUMN public.ossilo.height_unit_id IS 'Unidad de media de la altura';
            public       postgres    false    281            j           0    0    COLUMN ossilo.diameter    COMMENT     A   COMMENT ON COLUMN public.ossilo.diameter IS 'Diametro del silo';
            public       postgres    false    281            k           0    0    COLUMN ossilo.diameter_unit_id    COMMENT     T   COMMENT ON COLUMN public.ossilo.diameter_unit_id IS 'Unidad de media del diametro';
            public       postgres    false    281            l           0    0 "   COLUMN ossilo.total_rings_quantity    COMMENT     U   COMMENT ON COLUMN public.ossilo.total_rings_quantity IS 'Total de anillos del silo';
            public       postgres    false    281            m           0    0 !   COLUMN ossilo.measuring_mechanism    COMMENT     Y   COMMENT ON COLUMN public.ossilo.measuring_mechanism IS 'Mecanismo de medidad del silo
';
            public       postgres    false    281            n           0    0    COLUMN ossilo.cone_degrees    COMMENT     C   COMMENT ON COLUMN public.ossilo.cone_degrees IS 'Grados del cono';
            public       postgres    false    281            o           0    0    COLUMN ossilo.total_capacity_1    COMMENT     L   COMMENT ON COLUMN public.ossilo.total_capacity_1 IS 'Total de capacidad 1';
            public       postgres    false    281            p           0    0    COLUMN ossilo.total_capacity_2    COMMENT     L   COMMENT ON COLUMN public.ossilo.total_capacity_2 IS 'Total de capacidad 2';
            public       postgres    false    281            q           0    0     COLUMN ossilo.capacity_unit_id_1    COMMENT     X   COMMENT ON COLUMN public.ossilo.capacity_unit_id_1 IS 'Id de Capacidad de la unidad 1';
            public       postgres    false    281            r           0    0     COLUMN ossilo.capacity_unit_id_2    COMMENT     X   COMMENT ON COLUMN public.ossilo.capacity_unit_id_2 IS 'Id de Capacidad de la unidad 2';
            public       postgres    false    281            s           0    0    COLUMN ossilo.central    COMMENT     6   COMMENT ON COLUMN public.ossilo.central IS 'Central';
            public       postgres    false    281                       1259    16644    ossilo_osshed    TABLE     �   CREATE TABLE public.ossilo_osshed (
    silo_id integer NOT NULL,
    shed_id integer NOT NULL,
    center_id integer NOT NULL,
    farm_id integer NOT NULL,
    partnership_id integer NOT NULL,
    client_id integer NOT NULL,
    deleted_mark integer
);
 !   DROP TABLE public.ossilo_osshed;
       public         postgres    false    3            t           0    0    TABLE ossilo_osshed    COMMENT     R   COMMENT ON TABLE public.ossilo_osshed IS 'Tabla union de las tablas silo y shed';
            public       postgres    false    282            u           0    0    COLUMN ossilo_osshed.silo_id    COMMENT     A   COMMENT ON COLUMN public.ossilo_osshed.silo_id IS 'Id del silo';
            public       postgres    false    282            v           0    0    COLUMN ossilo_osshed.shed_id    COMMENT     C   COMMENT ON COLUMN public.ossilo_osshed.shed_id IS 'Id del galpon';
            public       postgres    false    282            w           0    0    COLUMN ossilo_osshed.center_id    COMMENT     E   COMMENT ON COLUMN public.ossilo_osshed.center_id IS 'Id del nucleo';
            public       postgres    false    282            x           0    0    COLUMN ossilo_osshed.farm_id    COMMENT     E   COMMENT ON COLUMN public.ossilo_osshed.farm_id IS 'Id de la granja';
            public       postgres    false    282            y           0    0 #   COLUMN ossilo_osshed.partnership_id    COMMENT     M   COMMENT ON COLUMN public.ossilo_osshed.partnership_id IS 'Id de la empresa';
            public       postgres    false    282            z           0    0    COLUMN ossilo_osshed.client_id    COMMENT     F   COMMENT ON COLUMN public.ossilo_osshed.client_id IS 'Id del cliente';
            public       postgres    false    282            {           0    0 !   COLUMN ossilo_osshed.deleted_mark    COMMENT     K   COMMENT ON COLUMN public.ossilo_osshed.deleted_mark IS 'Marca de borrado';
            public       postgres    false    282                       1259    16647    slaughterhouse_id_seq    SEQUENCE        CREATE SEQUENCE public.slaughterhouse_id_seq
    START WITH 33
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.slaughterhouse_id_seq;
       public       postgres    false    3                       1259    16649    osslaughterhouse    TABLE     Z  CREATE TABLE public.osslaughterhouse (
    slaughterhouse_id integer DEFAULT nextval('public.slaughterhouse_id_seq'::regclass) NOT NULL,
    name character varying(45) NOT NULL,
    address character varying(250) NOT NULL,
    description character varying(250) NOT NULL,
    code character varying(20) NOT NULL,
    capacity double precision
);
 $   DROP TABLE public.osslaughterhouse;
       public         postgres    false    283    3                       1259    16656    warehouse_id_seq    SEQUENCE     y   CREATE SEQUENCE public.warehouse_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.warehouse_id_seq;
       public       postgres    false    3                       1259    16658    oswarehouse    TABLE       CREATE TABLE public.oswarehouse (
    warehouse_id integer DEFAULT nextval('public.warehouse_id_seq'::regclass) NOT NULL,
    partnership_id integer NOT NULL,
    farm_id integer NOT NULL,
    name character varying(45) NOT NULL,
    code character varying(20) NOT NULL
);
    DROP TABLE public.oswarehouse;
       public         postgres    false    285    3            |           0    0    TABLE oswarehouse    COMMENT     \   COMMENT ON TABLE public.oswarehouse IS 'Almacena la informacion referente a los almacenes';
            public       postgres    false    286            }           0    0    COLUMN oswarehouse.warehouse_id    COMMENT     G   COMMENT ON COLUMN public.oswarehouse.warehouse_id IS 'Id del almacen';
            public       postgres    false    286            ~           0    0 !   COLUMN oswarehouse.partnership_id    COMMENT     ^   COMMENT ON COLUMN public.oswarehouse.partnership_id IS 'Id de la empresa dueña del almacen';
            public       postgres    false    286                       0    0    COLUMN oswarehouse.farm_id    COMMENT     C   COMMENT ON COLUMN public.oswarehouse.farm_id IS 'Id de la granja';
            public       postgres    false    286            �           0    0    COLUMN oswarehouse.name    COMMENT     C   COMMENT ON COLUMN public.oswarehouse.name IS 'Nombre del almacen';
            public       postgres    false    286            �           0    0    COLUMN oswarehouse.code    COMMENT     C   COMMENT ON COLUMN public.oswarehouse.code IS 'Codigo del almacen';
            public       postgres    false    286                       1259    16662    posture_curve_id_seq    SEQUENCE     }   CREATE SEQUENCE public.posture_curve_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.posture_curve_id_seq;
       public       postgres    false    3                        1259    16664    predecessor_id_seq    SEQUENCE     {   CREATE SEQUENCE public.predecessor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.predecessor_id_seq;
       public       postgres    false    3            !           1259    16666    process_class_id_seq    SEQUENCE     }   CREATE SEQUENCE public.process_class_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.process_class_id_seq;
       public       postgres    false    3            "           1259    16668    programmed_eggs_id_seq    SEQUENCE        CREATE SEQUENCE public.programmed_eggs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.programmed_eggs_id_seq;
       public       postgres    false    3            #           1259    16670    raspberry_id_seq    SEQUENCE     y   CREATE SEQUENCE public.raspberry_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.raspberry_id_seq;
       public       postgres    false    3            $           1259    16672    scenario_formula_id_seq    SEQUENCE     �   CREATE SEQUENCE public.scenario_formula_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.scenario_formula_id_seq;
       public       postgres    false    3            %           1259    16674    scenario_parameter_day_seq    SEQUENCE     �   CREATE SEQUENCE public.scenario_parameter_day_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.scenario_parameter_day_seq;
       public       postgres    false    3            &           1259    16676    scenario_parameter_id_seq    SEQUENCE     �   CREATE SEQUENCE public.scenario_parameter_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.scenario_parameter_id_seq;
       public       postgres    false    3            '           1259    16678    scenario_posture_id_seq    SEQUENCE     �   CREATE SEQUENCE public.scenario_posture_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.scenario_posture_id_seq;
       public       postgres    false    3            (           1259    16680    scenario_process_id_seq    SEQUENCE     �   CREATE SEQUENCE public.scenario_process_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.scenario_process_id_seq;
       public       postgres    false    3            )           1259    16682    txavailabilitysheds    TABLE       CREATE TABLE public.txavailabilitysheds (
    availability_shed_id integer DEFAULT nextval('public.availability_shed_id_seq'::regclass) NOT NULL,
    shed_id integer NOT NULL,
    init_date date,
    end_date date,
    lot_code character varying(20) NOT NULL
);
 '   DROP TABLE public.txavailabilitysheds;
       public         postgres    false    216    3            �           0    0    TABLE txavailabilitysheds    COMMENT     �   COMMENT ON TABLE public.txavailabilitysheds IS 'Almacena la disponibilidad en fechas de los galpones de acuerdo a la programación establecida';
            public       postgres    false    297            �           0    0 /   COLUMN txavailabilitysheds.availability_shed_id    COMMENT     �   COMMENT ON COLUMN public.txavailabilitysheds.availability_shed_id IS 'Id de la disponibilidad del almacen, indicando si este esta disponible';
            public       postgres    false    297            �           0    0 "   COLUMN txavailabilitysheds.shed_id    COMMENT     I   COMMENT ON COLUMN public.txavailabilitysheds.shed_id IS 'Id del galpon';
            public       postgres    false    297            �           0    0 $   COLUMN txavailabilitysheds.init_date    COMMENT     r   COMMENT ON COLUMN public.txavailabilitysheds.init_date IS 'Fecha de inicio de la programacion de uso del galpon';
            public       postgres    false    297            �           0    0 #   COLUMN txavailabilitysheds.end_date    COMMENT     r   COMMENT ON COLUMN public.txavailabilitysheds.end_date IS 'Fecha de cerrado de la programacion de uso del galpon';
            public       postgres    false    297            �           0    0 #   COLUMN txavailabilitysheds.lot_code    COMMENT     W   COMMENT ON COLUMN public.txavailabilitysheds.lot_code IS 'codigo del lote del galpon';
            public       postgres    false    297            *           1259    16686 	   txbroiler    TABLE     L  CREATE TABLE public.txbroiler (
    broiler_id integer DEFAULT nextval('public.broiler_id_seq'::regclass) NOT NULL,
    projected_date date,
    projected_quantity integer,
    partnership_id integer,
    scenario_id integer,
    breed_id integer,
    lot_incubator character varying(45) NOT NULL,
    programmed_eggs_id integer
);
    DROP TABLE public.txbroiler;
       public         postgres    false    220    3            �           0    0    TABLE txbroiler    COMMENT     c   COMMENT ON TABLE public.txbroiler IS 'Almacena la proyeccion realizada para el modulo de engorde';
            public       postgres    false    298            �           0    0    COLUMN txbroiler.broiler_id    COMMENT     U   COMMENT ON COLUMN public.txbroiler.broiler_id IS 'Id de la programacion de engorde';
            public       postgres    false    298            �           0    0    COLUMN txbroiler.projected_date    COMMENT     X   COMMENT ON COLUMN public.txbroiler.projected_date IS 'Fecha de proyección de engorde';
            public       postgres    false    298            �           0    0 #   COLUMN txbroiler.projected_quantity    COMMENT     `   COMMENT ON COLUMN public.txbroiler.projected_quantity IS 'Cantidad proyectada para el engorde';
            public       postgres    false    298            �           0    0    COLUMN txbroiler.partnership_id    COMMENT     I   COMMENT ON COLUMN public.txbroiler.partnership_id IS 'Id de la empresa';
            public       postgres    false    298            �           0    0    COLUMN txbroiler.scenario_id    COMMENT     G   COMMENT ON COLUMN public.txbroiler.scenario_id IS 'Id edl escenario ';
            public       postgres    false    298            �           0    0    COLUMN txbroiler.breed_id    COMMENT     K   COMMENT ON COLUMN public.txbroiler.breed_id IS 'Id de la raza a engordar';
            public       postgres    false    298            �           0    0    COLUMN txbroiler.lot_incubator    COMMENT     u   COMMENT ON COLUMN public.txbroiler.lot_incubator IS 'Lote de incubación de donde provienen los huevos proyectados';
            public       postgres    false    298            �           0    0 #   COLUMN txbroiler.programmed_eggs_id    COMMENT     Y   COMMENT ON COLUMN public.txbroiler.programmed_eggs_id IS 'Id de los huevos programados';
            public       postgres    false    298            +           1259    16690    txbroiler_detail    TABLE     �  CREATE TABLE public.txbroiler_detail (
    broiler_detail_id integer DEFAULT nextval('public.broiler_detail_id_seq'::regclass) NOT NULL,
    broiler_id integer NOT NULL,
    scheduled_date date,
    scheduled_quantity integer,
    farm_id integer NOT NULL,
    shed_id integer NOT NULL,
    confirm integer,
    execution_date date,
    execution_quantity integer,
    lot integer NOT NULL,
    broiler_product_id integer
);
 $   DROP TABLE public.txbroiler_detail;
       public         postgres    false    219    3            �           0    0    TABLE txbroiler_detail    COMMENT     l   COMMENT ON TABLE public.txbroiler_detail IS 'Almacena la programacion y ejecuccion del proceso de engorde';
            public       postgres    false    299            �           0    0 )   COLUMN txbroiler_detail.broiler_detail_id    COMMENT     `   COMMENT ON COLUMN public.txbroiler_detail.broiler_detail_id IS 'Id de los detalles de engorde';
            public       postgres    false    299            �           0    0 "   COLUMN txbroiler_detail.broiler_id    COMMENT     \   COMMENT ON COLUMN public.txbroiler_detail.broiler_id IS 'Id de la programacion de engorde';
            public       postgres    false    299            �           0    0 &   COLUMN txbroiler_detail.scheduled_date    COMMENT     k   COMMENT ON COLUMN public.txbroiler_detail.scheduled_date IS 'Fecha programada para el proceso de engorde';
            public       postgres    false    299            �           0    0 *   COLUMN txbroiler_detail.scheduled_quantity    COMMENT     r   COMMENT ON COLUMN public.txbroiler_detail.scheduled_quantity IS 'Cantidad programada para el proceso de engorde';
            public       postgres    false    299            �           0    0    COLUMN txbroiler_detail.farm_id    COMMENT     H   COMMENT ON COLUMN public.txbroiler_detail.farm_id IS 'Id de la granja';
            public       postgres    false    299            �           0    0    COLUMN txbroiler_detail.shed_id    COMMENT     F   COMMENT ON COLUMN public.txbroiler_detail.shed_id IS 'Id del galpon';
            public       postgres    false    299            �           0    0    COLUMN txbroiler_detail.confirm    COMMENT     E   COMMENT ON COLUMN public.txbroiler_detail.confirm IS 'Confirmacion';
            public       postgres    false    299            �           0    0 &   COLUMN txbroiler_detail.execution_date    COMMENT     p   COMMENT ON COLUMN public.txbroiler_detail.execution_date IS 'Fecha de ejeccion de la planificacion de engorde';
            public       postgres    false    299            �           0    0 *   COLUMN txbroiler_detail.execution_quantity    COMMENT     u   COMMENT ON COLUMN public.txbroiler_detail.execution_quantity IS 'Cantidad ejecutada de la programación de engorde';
            public       postgres    false    299            �           0    0    COLUMN txbroiler_detail.lot    COMMENT     D   COMMENT ON COLUMN public.txbroiler_detail.lot IS 'Lote de engorde';
            public       postgres    false    299            �           0    0 *   COLUMN txbroiler_detail.broiler_product_id    COMMENT     ^   COMMENT ON COLUMN public.txbroiler_detail.broiler_product_id IS 'Id del producto de engorde';
            public       postgres    false    299            ,           1259    16694    txbroilereviction    TABLE     �  CREATE TABLE public.txbroilereviction (
    broilereviction_id integer DEFAULT nextval('public.broilereviction_id_seq'::regclass) NOT NULL,
    projected_date date,
    projected_quantity integer,
    partnership_id integer NOT NULL,
    scenario_id integer NOT NULL,
    breed_id integer NOT NULL,
    lot_incubator character varying(45) NOT NULL,
    broiler_detail_id integer NOT NULL
);
 %   DROP TABLE public.txbroilereviction;
       public         postgres    false    224    3            �           0    0    TABLE txbroilereviction    COMMENT     _   COMMENT ON TABLE public.txbroilereviction IS 'Almacena las proyeccion del modula de desalojo';
            public       postgres    false    300            �           0    0 +   COLUMN txbroilereviction.broilereviction_id    COMMENT     ^   COMMENT ON COLUMN public.txbroilereviction.broilereviction_id IS 'Id del modulo de desalojo';
            public       postgres    false    300            �           0    0 '   COLUMN txbroilereviction.projected_date    COMMENT     b   COMMENT ON COLUMN public.txbroilereviction.projected_date IS 'Fecha proyectada para el desalojo';
            public       postgres    false    300            �           0    0 +   COLUMN txbroilereviction.projected_quantity    COMMENT     i   COMMENT ON COLUMN public.txbroilereviction.projected_quantity IS 'Cantidad proyectada para el desalojo';
            public       postgres    false    300            �           0    0 '   COLUMN txbroilereviction.partnership_id    COMMENT     Q   COMMENT ON COLUMN public.txbroilereviction.partnership_id IS 'Id de la empresa';
            public       postgres    false    300            �           0    0 $   COLUMN txbroilereviction.scenario_id    COMMENT     N   COMMENT ON COLUMN public.txbroilereviction.scenario_id IS 'Id del escenario';
            public       postgres    false    300            �           0    0 !   COLUMN txbroilereviction.breed_id    COMMENT     H   COMMENT ON COLUMN public.txbroilereviction.breed_id IS 'Id de la raza';
            public       postgres    false    300            �           0    0 &   COLUMN txbroilereviction.lot_incubator    COMMENT     R   COMMENT ON COLUMN public.txbroilereviction.lot_incubator IS 'Lote de incubacion';
            public       postgres    false    300            -           1259    16698    txbroilereviction_detail    TABLE     �  CREATE TABLE public.txbroilereviction_detail (
    broilereviction_detail_id integer DEFAULT nextval('public.broilereviction_detail_id_seq'::regclass) NOT NULL,
    broilereviction_id integer NOT NULL,
    scheduled_date date,
    scheduled_quantity integer,
    farm_id integer NOT NULL,
    shed_id integer NOT NULL,
    confirm integer,
    execution_date date,
    execution_quantity integer,
    lot integer NOT NULL,
    broiler_product_id integer NOT NULL,
    slaughterhouse_id integer NOT NULL
);
 ,   DROP TABLE public.txbroilereviction_detail;
       public         postgres    false    223    3            �           0    0    TABLE txbroilereviction_detail    COMMENT     v   COMMENT ON TABLE public.txbroilereviction_detail IS 'Almacena la programación y ejecución del módulo de desalojo';
            public       postgres    false    301            �           0    0 9   COLUMN txbroilereviction_detail.broilereviction_detail_id    COMMENT     ~   COMMENT ON COLUMN public.txbroilereviction_detail.broilereviction_detail_id IS 'Id de los detalles del modulo de desarrollo';
            public       postgres    false    301            �           0    0 2   COLUMN txbroilereviction_detail.broilereviction_id    COMMENT     e   COMMENT ON COLUMN public.txbroilereviction_detail.broilereviction_id IS 'Id del modulo de desalojo';
            public       postgres    false    301            �           0    0 .   COLUMN txbroilereviction_detail.scheduled_date    COMMENT     i   COMMENT ON COLUMN public.txbroilereviction_detail.scheduled_date IS 'Fecha programada para el desalojo';
            public       postgres    false    301            �           0    0 2   COLUMN txbroilereviction_detail.scheduled_quantity    COMMENT     p   COMMENT ON COLUMN public.txbroilereviction_detail.scheduled_quantity IS 'Cantidad programada para el desalojo';
            public       postgres    false    301            �           0    0 '   COLUMN txbroilereviction_detail.farm_id    COMMENT     P   COMMENT ON COLUMN public.txbroilereviction_detail.farm_id IS 'Id de la granja';
            public       postgres    false    301            �           0    0 '   COLUMN txbroilereviction_detail.shed_id    COMMENT     N   COMMENT ON COLUMN public.txbroilereviction_detail.shed_id IS 'Id del galpon';
            public       postgres    false    301            �           0    0 '   COLUMN txbroilereviction_detail.confirm    COMMENT     M   COMMENT ON COLUMN public.txbroilereviction_detail.confirm IS 'Confirmacion';
            public       postgres    false    301            �           0    0 .   COLUMN txbroilereviction_detail.execution_date    COMMENT     \   COMMENT ON COLUMN public.txbroilereviction_detail.execution_date IS 'Fecha de ejecución ';
            public       postgres    false    301            �           0    0 2   COLUMN txbroilereviction_detail.execution_quantity    COMMENT     c   COMMENT ON COLUMN public.txbroilereviction_detail.execution_quantity IS 'Cantidad de ejecución ';
            public       postgres    false    301            �           0    0 #   COLUMN txbroilereviction_detail.lot    COMMENT     X   COMMENT ON COLUMN public.txbroilereviction_detail.lot IS 'Lote del modulo de desalojo';
            public       postgres    false    301            �           0    0 2   COLUMN txbroilereviction_detail.broiler_product_id    COMMENT     f   COMMENT ON COLUMN public.txbroilereviction_detail.broiler_product_id IS 'Id del producto de engorde';
            public       postgres    false    301            �           0    0 1   COLUMN txbroilereviction_detail.slaughterhouse_id    COMMENT     g   COMMENT ON COLUMN public.txbroilereviction_detail.slaughterhouse_id IS 'Id de la planta de beneficio';
            public       postgres    false    301            .           1259    16702    txbroilerproduct_detail    TABLE     �   CREATE TABLE public.txbroilerproduct_detail (
    broilerproduct_detail_id integer DEFAULT nextval('public.broiler_product_detail_id_seq'::regclass) NOT NULL,
    broiler_detail integer NOT NULL,
    broiler_product_id integer,
    quantity integer
);
 +   DROP TABLE public.txbroilerproduct_detail;
       public         postgres    false    221    3            �           0    0    TABLE txbroilerproduct_detail    COMMENT     h   COMMENT ON TABLE public.txbroilerproduct_detail IS 'Almacena los detalles de la produccion de engorde';
            public       postgres    false    302            �           0    0 7   COLUMN txbroilerproduct_detail.broilerproduct_detail_id    COMMENT     |   COMMENT ON COLUMN public.txbroilerproduct_detail.broilerproduct_detail_id IS 'Id de los detalles de produccion de engorde';
            public       postgres    false    302            �           0    0 -   COLUMN txbroilerproduct_detail.broiler_detail    COMMENT     Z   COMMENT ON COLUMN public.txbroilerproduct_detail.broiler_detail IS 'Detalles de engorde';
            public       postgres    false    302            �           0    0 1   COLUMN txbroilerproduct_detail.broiler_product_id    COMMENT     e   COMMENT ON COLUMN public.txbroilerproduct_detail.broiler_product_id IS 'Id del producto de engorde';
            public       postgres    false    302            �           0    0 '   COLUMN txbroilerproduct_detail.quantity    COMMENT     `   COMMENT ON COLUMN public.txbroilerproduct_detail.quantity IS 'Cantidad de producto de engorde';
            public       postgres    false    302            /           1259    16706    txbroodermachine    TABLE     �  CREATE TABLE public.txbroodermachine (
    brooder_machine_id_seq integer DEFAULT nextval('public.brooder_machines_id_seq'::regclass) NOT NULL,
    partnership_id integer NOT NULL,
    farm_id integer NOT NULL,
    capacity integer,
    sunday integer,
    monday integer,
    tuesday integer,
    wednesday integer,
    thursday integer,
    friday integer,
    saturday integer,
    name character varying(250)
);
 $   DROP TABLE public.txbroodermachine;
       public         postgres    false    226    3            �           0    0    TABLE txbroodermachine    COMMENT     ]   COMMENT ON TABLE public.txbroodermachine IS 'Almacena los datos de las maquinas de engorde';
            public       postgres    false    303            �           0    0 .   COLUMN txbroodermachine.brooder_machine_id_seq    COMMENT     c   COMMENT ON COLUMN public.txbroodermachine.brooder_machine_id_seq IS 'Id de la maquina de engorde';
            public       postgres    false    303            �           0    0 &   COLUMN txbroodermachine.partnership_id    COMMENT     P   COMMENT ON COLUMN public.txbroodermachine.partnership_id IS 'Id de la empresa';
            public       postgres    false    303            �           0    0    COLUMN txbroodermachine.farm_id    COMMENT     H   COMMENT ON COLUMN public.txbroodermachine.farm_id IS 'Id de la granja';
            public       postgres    false    303            �           0    0     COLUMN txbroodermachine.capacity    COMMENT     Q   COMMENT ON COLUMN public.txbroodermachine.capacity IS 'Capacidad de la maquina';
            public       postgres    false    303            �           0    0    COLUMN txbroodermachine.sunday    COMMENT     ?   COMMENT ON COLUMN public.txbroodermachine.sunday IS 'Domingo';
            public       postgres    false    303            �           0    0    COLUMN txbroodermachine.monday    COMMENT     =   COMMENT ON COLUMN public.txbroodermachine.monday IS 'Lunes';
            public       postgres    false    303            �           0    0    COLUMN txbroodermachine.tuesday    COMMENT     ?   COMMENT ON COLUMN public.txbroodermachine.tuesday IS 'Martes';
            public       postgres    false    303            �           0    0 !   COLUMN txbroodermachine.wednesday    COMMENT     D   COMMENT ON COLUMN public.txbroodermachine.wednesday IS 'Miercoles';
            public       postgres    false    303            �           0    0     COLUMN txbroodermachine.thursday    COMMENT     @   COMMENT ON COLUMN public.txbroodermachine.thursday IS 'Jueves';
            public       postgres    false    303            �           0    0    COLUMN txbroodermachine.friday    COMMENT     ?   COMMENT ON COLUMN public.txbroodermachine.friday IS 'Viernes';
            public       postgres    false    303            �           0    0     COLUMN txbroodermachine.saturday    COMMENT     @   COMMENT ON COLUMN public.txbroodermachine.saturday IS 'Sabado';
            public       postgres    false    303            �           0    0    COLUMN txbroodermachine.name    COMMENT     J   COMMENT ON COLUMN public.txbroodermachine.name IS 'Nombre de la maquina';
            public       postgres    false    303            0           1259    16710 
   txcalendar    TABLE     |  CREATE TABLE public.txcalendar (
    calendar_id integer DEFAULT nextval('public.calendar_id_seq'::regclass) NOT NULL,
    description character varying(250) NOT NULL,
    saturday character varying(15),
    sunday character varying(15),
    week_start character varying(15),
    code character(20) NOT NULL,
    year_start integer,
    year_end integer,
    generated integer
);
    DROP TABLE public.txcalendar;
       public         postgres    false    228    3            �           0    0    TABLE txcalendar    COMMENT     n   COMMENT ON TABLE public.txcalendar IS 'Almacena la informacion del calendario con la que trabaja el sistema';
            public       postgres    false    304            �           0    0    COLUMN txcalendar.calendar_id    COMMENT     H   COMMENT ON COLUMN public.txcalendar.calendar_id IS 'Id del calendario';
            public       postgres    false    304            �           0    0    COLUMN txcalendar.description    COMMENT     S   COMMENT ON COLUMN public.txcalendar.description IS 'Descripción del calendario
';
            public       postgres    false    304            �           0    0    COLUMN txcalendar.saturday    COMMENT     Z   COMMENT ON COLUMN public.txcalendar.saturday IS 'Indica si el día sábado es laborable';
            public       postgres    false    304            �           0    0    COLUMN txcalendar.sunday    COMMENT     X   COMMENT ON COLUMN public.txcalendar.sunday IS 'Indica si el día Domingo es laborable';
            public       postgres    false    304            �           0    0    COLUMN txcalendar.week_start    COMMENT     [   COMMENT ON COLUMN public.txcalendar.week_start IS 'Semana en la que inicia el calendario';
            public       postgres    false    304            �           0    0    COLUMN txcalendar.code    COMMENT     E   COMMENT ON COLUMN public.txcalendar.code IS 'Codigo del calendario';
            public       postgres    false    304            �           0    0    COLUMN txcalendar.year_start    COMMENT     Y   COMMENT ON COLUMN public.txcalendar.year_start IS 'Año en el que inicia el calendario';
            public       postgres    false    304            �           0    0    COLUMN txcalendar.year_end    COMMENT     Y   COMMENT ON COLUMN public.txcalendar.year_end IS 'Año en el que finaliza el calendario';
            public       postgres    false    304            �           0    0    COLUMN txcalendar.generated    COMMENT     u   COMMENT ON COLUMN public.txcalendar.generated IS 'Indica si el calendario fue generado a partir de otro calendario';
            public       postgres    false    304            1           1259    16714    txcalendarday    TABLE     �  CREATE TABLE public.txcalendarday (
    calendar_day_id integer DEFAULT nextval('public.calendar_day_id_seq'::regclass) NOT NULL,
    calendar_id integer NOT NULL,
    use_date timestamp with time zone NOT NULL,
    use_year integer NOT NULL,
    use_month integer NOT NULL,
    use_day integer NOT NULL,
    use_week timestamp with time zone NOT NULL,
    week_day integer NOT NULL,
    sequence integer NOT NULL,
    working_day integer NOT NULL
);
 !   DROP TABLE public.txcalendarday;
       public         postgres    false    227    3            �           0    0    TABLE txcalendarday    COMMENT     _   COMMENT ON TABLE public.txcalendarday IS 'Almacena los datos de los dias que son laborables ';
            public       postgres    false    305            �           0    0 $   COLUMN txcalendarday.calendar_day_id    COMMENT     W   COMMENT ON COLUMN public.txcalendarday.calendar_day_id IS 'Id del dia del calendario';
            public       postgres    false    305            �           0    0     COLUMN txcalendarday.calendar_id    COMMENT     K   COMMENT ON COLUMN public.txcalendarday.calendar_id IS 'Id del calendario';
            public       postgres    false    305            �           0    0    COLUMN txcalendarday.use_date    COMMENT     d   COMMENT ON COLUMN public.txcalendarday.use_date IS 'Fecha en el que se encuentra el dia laborable';
            public       postgres    false    305            �           0    0    COLUMN txcalendarday.use_year    COMMENT     c   COMMENT ON COLUMN public.txcalendarday.use_year IS 'Año en el que se encuentra el dia laborable';
            public       postgres    false    305            �           0    0    COLUMN txcalendarday.use_month    COMMENT     c   COMMENT ON COLUMN public.txcalendarday.use_month IS 'Mes en el que se encuentra el dia laborable';
            public       postgres    false    305            �           0    0    COLUMN txcalendarday.use_day    COMMENT     a   COMMENT ON COLUMN public.txcalendarday.use_day IS 'Dia en el que se encuentra el dia laborable';
            public       postgres    false    305            �           0    0    COLUMN txcalendarday.use_week    COMMENT     e   COMMENT ON COLUMN public.txcalendarday.use_week IS 'Semana en el que se encuentra el dia laborable';
            public       postgres    false    305            �           0    0    COLUMN txcalendarday.week_day    COMMENT     l   COMMENT ON COLUMN public.txcalendarday.week_day IS 'Dia de semana en el que se encuentra el dia laborable';
            public       postgres    false    305            �           0    0     COLUMN txcalendarday.working_day    COMMENT     Z   COMMENT ON COLUMN public.txcalendarday.working_day IS 'Indica si el dia es laboral o no';
            public       postgres    false    305            E           1259    18787    txeggs_movements_id_seq    SEQUENCE     �   CREATE SEQUENCE public.txeggs_movements_id_seq
    START WITH 1
    INCREMENT BY 170
    NO MINVALUE
    MAXVALUE 9999999999999999
    CACHE 1;
 .   DROP SEQUENCE public.txeggs_movements_id_seq;
       public       postgres    false    3            F           1259    18789    txeggs_movements    TABLE     l  CREATE TABLE public.txeggs_movements (
    eggs_movements_id integer DEFAULT nextval('public.txeggs_movements_id_seq'::regclass) NOT NULL,
    fecha_movements date NOT NULL,
    lot integer NOT NULL,
    quantity integer NOT NULL,
    type_movements character varying NOT NULL,
    eggs_storage_id integer NOT NULL,
    description_adjustment character varying
);
 $   DROP TABLE public.txeggs_movements;
       public         postgres    false    325    3            2           1259    16718    txeggs_planning    TABLE       CREATE TABLE public.txeggs_planning (
    egg_planning_id integer DEFAULT nextval('public.egg_planning_id_seq'::regclass) NOT NULL,
    month_planning integer,
    year_planning integer,
    scenario_id integer,
    planned double precision,
    breed_id integer NOT NULL
);
 #   DROP TABLE public.txeggs_planning;
       public         postgres    false    230    3            �           0    0    TABLE txeggs_planning    COMMENT     g   COMMENT ON TABLE public.txeggs_planning IS 'Almacena los detalles de la planificación de los huevos';
            public       postgres    false    306            �           0    0 &   COLUMN txeggs_planning.egg_planning_id    COMMENT     [   COMMENT ON COLUMN public.txeggs_planning.egg_planning_id IS 'Id de planeación de huevos';
            public       postgres    false    306            �           0    0 %   COLUMN txeggs_planning.month_planning    COMMENT     c   COMMENT ON COLUMN public.txeggs_planning.month_planning IS 'Mes de planificación de los huevos
';
            public       postgres    false    306            �           0    0 $   COLUMN txeggs_planning.year_planning    COMMENT     b   COMMENT ON COLUMN public.txeggs_planning.year_planning IS 'Año de planificación de los huevos';
            public       postgres    false    306            �           0    0 "   COLUMN txeggs_planning.scenario_id    COMMENT     p   COMMENT ON COLUMN public.txeggs_planning.scenario_id IS 'Escenario al cual pertenecen los huevos planificados';
            public       postgres    false    306            �           0    0    COLUMN txeggs_planning.planned    COMMENT     X   COMMENT ON COLUMN public.txeggs_planning.planned IS 'Cantidad de huevos planificados
';
            public       postgres    false    306            �           0    0    COLUMN txeggs_planning.breed_id    COMMENT     T   COMMENT ON COLUMN public.txeggs_planning.breed_id IS 'Id de la raza de los huevos';
            public       postgres    false    306            3           1259    16722    txeggs_required    TABLE     
  CREATE TABLE public.txeggs_required (
    egg_required_id integer DEFAULT nextval('public.egg_required_id_seq'::regclass) NOT NULL,
    use_month integer,
    use_year integer,
    scenario_id integer NOT NULL,
    required double precision,
    breed_id integer
);
 #   DROP TABLE public.txeggs_required;
       public         postgres    false    231    3            �           0    0    TABLE txeggs_required    COMMENT     V   COMMENT ON TABLE public.txeggs_required IS 'Almacena los datos de huevos requeridos';
            public       postgres    false    307            �           0    0 &   COLUMN txeggs_required.egg_required_id    COMMENT     [   COMMENT ON COLUMN public.txeggs_required.egg_required_id IS 'Id de los huevos requeridos';
            public       postgres    false    307            �           0    0     COLUMN txeggs_required.use_month    COMMENT     =   COMMENT ON COLUMN public.txeggs_required.use_month IS 'Mes';
            public       postgres    false    307            �           0    0    COLUMN txeggs_required.use_year    COMMENT     =   COMMENT ON COLUMN public.txeggs_required.use_year IS 'Año';
            public       postgres    false    307            �           0    0 "   COLUMN txeggs_required.scenario_id    COMMENT     L   COMMENT ON COLUMN public.txeggs_required.scenario_id IS 'Id del escenario';
            public       postgres    false    307            �           0    0    COLUMN txeggs_required.required    COMMENT     K   COMMENT ON COLUMN public.txeggs_required.required IS 'Cantidad requerida';
            public       postgres    false    307            �           0    0    COLUMN txeggs_required.breed_id    COMMENT     F   COMMENT ON COLUMN public.txeggs_required.breed_id IS 'Id de la raza';
            public       postgres    false    307            4           1259    16726    txeggs_storage    TABLE     b  CREATE TABLE public.txeggs_storage (
    eggs_storage_id integer DEFAULT nextval('public.eggs_storage_id_seq'::regclass) NOT NULL,
    incubator_plant_id integer NOT NULL,
    scenario_id integer NOT NULL,
    breed_id integer NOT NULL,
    init_date date,
    end_date date,
    lot character varying(45),
    eggs integer,
    eggs_executed integer
);
 "   DROP TABLE public.txeggs_storage;
       public         postgres    false    232    3            �           0    0    TABLE txeggs_storage    COMMENT     ~   COMMENT ON TABLE public.txeggs_storage IS 'Guarda la informacion de almacenamiento de los huevos en las plantas incubadoras';
            public       postgres    false    308            �           0    0 %   COLUMN txeggs_storage.eggs_storage_id    COMMENT     W   COMMENT ON COLUMN public.txeggs_storage.eggs_storage_id IS 'Id del almacen de huevos';
            public       postgres    false    308            �           0    0 (   COLUMN txeggs_storage.incubator_plant_id    COMMENT     Y   COMMENT ON COLUMN public.txeggs_storage.incubator_plant_id IS 'Id de planta incubadora';
            public       postgres    false    308            �           0    0 !   COLUMN txeggs_storage.scenario_id    COMMENT     K   COMMENT ON COLUMN public.txeggs_storage.scenario_id IS 'Id del escenario';
            public       postgres    false    308            �           0    0    COLUMN txeggs_storage.breed_id    COMMENT     E   COMMENT ON COLUMN public.txeggs_storage.breed_id IS 'Id de la raza';
            public       postgres    false    308            �           0    0    COLUMN txeggs_storage.init_date    COMMENT     H   COMMENT ON COLUMN public.txeggs_storage.init_date IS 'Fecha de inicio';
            public       postgres    false    308            �           0    0    COLUMN txeggs_storage.end_date    COMMENT     J   COMMENT ON COLUMN public.txeggs_storage.end_date IS 'Fecha de terminado';
            public       postgres    false    308            �           0    0    COLUMN txeggs_storage.lot    COMMENT     7   COMMENT ON COLUMN public.txeggs_storage.lot IS 'Lote';
            public       postgres    false    308            �           0    0    COLUMN txeggs_storage.eggs    COMMENT     F   COMMENT ON COLUMN public.txeggs_storage.eggs IS 'Cantidad de huevos';
            public       postgres    false    308            5           1259    16730    txgoals_erp    TABLE     �   CREATE TABLE public.txgoals_erp (
    goals_erp_id bigint NOT NULL,
    use_week date,
    use_value integer,
    product_id integer NOT NULL,
    code character varying(10),
    scenario_id integer NOT NULL
);
    DROP TABLE public.txgoals_erp;
       public         postgres    false    3            �           0    0    TABLE txgoals_erp    COMMENT     �   COMMENT ON TABLE public.txgoals_erp IS 'Almacena los datos generados de las metas de producción de la planificación regresiva para ser enviados al ERP';
            public       postgres    false    309            �           0    0    COLUMN txgoals_erp.goals_erp_id    COMMENT     N   COMMENT ON COLUMN public.txgoals_erp.goals_erp_id IS 'Id de la meta del ERP';
            public       postgres    false    309            �           0    0    COLUMN txgoals_erp.use_week    COMMENT     ;   COMMENT ON COLUMN public.txgoals_erp.use_week IS 'Semana';
            public       postgres    false    309            �           0    0    COLUMN txgoals_erp.use_value    COMMENT     D   COMMENT ON COLUMN public.txgoals_erp.use_value IS 'Valor objetivo';
            public       postgres    false    309            �           0    0    COLUMN txgoals_erp.product_id    COMMENT     F   COMMENT ON COLUMN public.txgoals_erp.product_id IS 'Id del producto';
            public       postgres    false    309            �           0    0    COLUMN txgoals_erp.code    COMMENT     D   COMMENT ON COLUMN public.txgoals_erp.code IS 'Codigo del producto';
            public       postgres    false    309            �           0    0    COLUMN txgoals_erp.scenario_id    COMMENT     H   COMMENT ON COLUMN public.txgoals_erp.scenario_id IS 'Id del escenario';
            public       postgres    false    309            6           1259    16733    txgoals_erp_goals_erp_id_seq    SEQUENCE     �   CREATE SEQUENCE public.txgoals_erp_goals_erp_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.txgoals_erp_goals_erp_id_seq;
       public       postgres    false    309    3            �           0    0    txgoals_erp_goals_erp_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.txgoals_erp_goals_erp_id_seq OWNED BY public.txgoals_erp.goals_erp_id;
            public       postgres    false    310            7           1259    16735    txhousingway    TABLE     d  CREATE TABLE public.txhousingway (
    housing_way_id integer DEFAULT nextval('public.housing_way_id_seq'::regclass) NOT NULL,
    projected_quantity integer,
    projected_date date,
    stage_id integer NOT NULL,
    partnership_id integer NOT NULL,
    scenario_id integer NOT NULL,
    breed_id integer NOT NULL,
    predecessor_id integer NOT NULL
);
     DROP TABLE public.txhousingway;
       public         postgres    false    237    3            �           0    0    TABLE txhousingway    COMMENT     t   COMMENT ON TABLE public.txhousingway IS 'Almacena la proyección de los módulos de levante, cría y reproductora';
            public       postgres    false    311            �           0    0 "   COLUMN txhousingway.housing_way_id    COMMENT     �   COMMENT ON COLUMN public.txhousingway.housing_way_id IS 'Id de las proyecciones  de los módulos de levante, cría y reproductora';
            public       postgres    false    311            �           0    0 &   COLUMN txhousingway.projected_quantity    COMMENT     S   COMMENT ON COLUMN public.txhousingway.projected_quantity IS 'Cantidad proyectada';
            public       postgres    false    311            �           0    0 "   COLUMN txhousingway.projected_date    COMMENT     L   COMMENT ON COLUMN public.txhousingway.projected_date IS 'Fecha proyectada';
            public       postgres    false    311            �           0    0    COLUMN txhousingway.stage_id    COMMENT     D   COMMENT ON COLUMN public.txhousingway.stage_id IS 'Id de la etapa';
            public       postgres    false    311            �           0    0 "   COLUMN txhousingway.partnership_id    COMMENT     L   COMMENT ON COLUMN public.txhousingway.partnership_id IS 'Id de la empresa';
            public       postgres    false    311            �           0    0    COLUMN txhousingway.breed_id    COMMENT     C   COMMENT ON COLUMN public.txhousingway.breed_id IS 'Id de la raza';
            public       postgres    false    311            �           0    0 "   COLUMN txhousingway.predecessor_id    COMMENT     N   COMMENT ON COLUMN public.txhousingway.predecessor_id IS 'Id del predecesor ';
            public       postgres    false    311            8           1259    16739    txhousingway_detail    TABLE     �  CREATE TABLE public.txhousingway_detail (
    housingway_detail_id integer DEFAULT nextval('public.housing_way_detail_id_seq'::regclass) NOT NULL,
    housing_way_id integer NOT NULL,
    scheduled_date date,
    scheduled_quantity integer,
    farm_id integer NOT NULL,
    shed_id integer NOT NULL,
    confirm integer,
    execution_date date,
    execution_quantity integer,
    lot character varying(45),
    incubator_plant_id integer
);
 '   DROP TABLE public.txhousingway_detail;
       public         postgres    false    236    3            �           0    0    TABLE txhousingway_detail    COMMENT     �   COMMENT ON TABLE public.txhousingway_detail IS 'Almacena la programación y la ejecución de los módulos de levante y cría y reproductora';
            public       postgres    false    312                        0    0 /   COLUMN txhousingway_detail.housingway_detail_id    COMMENT     �   COMMENT ON COLUMN public.txhousingway_detail.housingway_detail_id IS 'Id de la programación y ejecución de los modelos de levante y cría y reproductora';
            public       postgres    false    312                       0    0 )   COLUMN txhousingway_detail.housing_way_id    COMMENT     �   COMMENT ON COLUMN public.txhousingway_detail.housing_way_id IS 'Id de las proyecciones  de los módulos de levante, cría y reproductora';
            public       postgres    false    312                       0    0 )   COLUMN txhousingway_detail.scheduled_date    COMMENT     S   COMMENT ON COLUMN public.txhousingway_detail.scheduled_date IS 'Fecha programada';
            public       postgres    false    312                       0    0 -   COLUMN txhousingway_detail.scheduled_quantity    COMMENT     Z   COMMENT ON COLUMN public.txhousingway_detail.scheduled_quantity IS 'Cantidad programada';
            public       postgres    false    312                       0    0 "   COLUMN txhousingway_detail.farm_id    COMMENT     K   COMMENT ON COLUMN public.txhousingway_detail.farm_id IS 'Id de la granja';
            public       postgres    false    312                       0    0 "   COLUMN txhousingway_detail.shed_id    COMMENT     S   COMMENT ON COLUMN public.txhousingway_detail.shed_id IS 'Id del galpon utilizado';
            public       postgres    false    312                       0    0 "   COLUMN txhousingway_detail.confirm    COMMENT     [   COMMENT ON COLUMN public.txhousingway_detail.confirm IS 'Confirmacion de sincronizacion ';
            public       postgres    false    312                       0    0 )   COLUMN txhousingway_detail.execution_date    COMMENT     V   COMMENT ON COLUMN public.txhousingway_detail.execution_date IS 'Fecha de ejecución';
            public       postgres    false    312                       0    0 -   COLUMN txhousingway_detail.execution_quantity    COMMENT     Z   COMMENT ON COLUMN public.txhousingway_detail.execution_quantity IS 'Cantidad a ejecutar';
            public       postgres    false    312            	           0    0    COLUMN txhousingway_detail.lot    COMMENT     I   COMMENT ON COLUMN public.txhousingway_detail.lot IS 'Lote seleccionado';
            public       postgres    false    312            
           0    0 -   COLUMN txhousingway_detail.incubator_plant_id    COMMENT     a   COMMENT ON COLUMN public.txhousingway_detail.incubator_plant_id IS 'Id de la planta incubadora';
            public       postgres    false    312            9           1259    16743    txlot    TABLE     n  CREATE TABLE public.txlot (
    lot_id integer DEFAULT nextval('public.lot_id_seq'::regclass) NOT NULL,
    lot_code character varying(20) NOT NULL,
    lot_origin character varying(150),
    status integer,
    proyected_date date,
    sheduled_date date,
    proyected_quantity integer,
    sheduled_quantity integer,
    released_quantity integer,
    product_id integer NOT NULL,
    breed_id integer NOT NULL,
    gender character varying(30),
    type_posture character varying(30),
    shed_id integer NOT NULL,
    origin character varying(30),
    farm_id integer NOT NULL,
    housing_way_id integer NOT NULL
);
    DROP TABLE public.txlot;
       public         postgres    false    244    3                       0    0    TABLE txlot    COMMENT     T   COMMENT ON TABLE public.txlot IS 'Almacena la informacion de los diferentes lotes';
            public       postgres    false    313                       0    0    COLUMN txlot.lot_id    COMMENT     8   COMMENT ON COLUMN public.txlot.lot_id IS 'Id del lote';
            public       postgres    false    313                       0    0    COLUMN txlot.lot_code    COMMENT     >   COMMENT ON COLUMN public.txlot.lot_code IS 'Codigo del lote';
            public       postgres    false    313                       0    0    COLUMN txlot.lot_origin    COMMENT     @   COMMENT ON COLUMN public.txlot.lot_origin IS 'Origen del lote';
            public       postgres    false    313                       0    0    COLUMN txlot.status    COMMENT     <   COMMENT ON COLUMN public.txlot.status IS 'Estado del lote';
            public       postgres    false    313                       0    0    COLUMN txlot.proyected_date    COMMENT     E   COMMENT ON COLUMN public.txlot.proyected_date IS 'Fecha proyectada';
            public       postgres    false    313                       0    0    COLUMN txlot.sheduled_date    COMMENT     D   COMMENT ON COLUMN public.txlot.sheduled_date IS 'Fecha programada';
            public       postgres    false    313                       0    0    COLUMN txlot.proyected_quantity    COMMENT     L   COMMENT ON COLUMN public.txlot.proyected_quantity IS 'Cantidad proyectada';
            public       postgres    false    313                       0    0    COLUMN txlot.sheduled_quantity    COMMENT     K   COMMENT ON COLUMN public.txlot.sheduled_quantity IS 'Cantidad programada';
            public       postgres    false    313                       0    0    COLUMN txlot.released_quantity    COMMENT     I   COMMENT ON COLUMN public.txlot.released_quantity IS 'Cantidad liberada';
            public       postgres    false    313                       0    0    COLUMN txlot.product_id    COMMENT     @   COMMENT ON COLUMN public.txlot.product_id IS 'Id del producto';
            public       postgres    false    313                       0    0    COLUMN txlot.breed_id    COMMENT     <   COMMENT ON COLUMN public.txlot.breed_id IS 'Id de la raza';
            public       postgres    false    313                       0    0    COLUMN txlot.gender    COMMENT     <   COMMENT ON COLUMN public.txlot.gender IS 'Genero del lote';
            public       postgres    false    313                       0    0    COLUMN txlot.type_posture    COMMENT     B   COMMENT ON COLUMN public.txlot.type_posture IS 'Tipo de postura';
            public       postgres    false    313                       0    0    COLUMN txlot.shed_id    COMMENT     ;   COMMENT ON COLUMN public.txlot.shed_id IS 'Id del galpon';
            public       postgres    false    313                       0    0    COLUMN txlot.origin    COMMENT     3   COMMENT ON COLUMN public.txlot.origin IS 'Origen';
            public       postgres    false    313                       0    0    COLUMN txlot.farm_id    COMMENT     =   COMMENT ON COLUMN public.txlot.farm_id IS 'Id de la granja';
            public       postgres    false    313                       0    0    COLUMN txlot.housing_way_id    COMMENT     ~   COMMENT ON COLUMN public.txlot.housing_way_id IS 'Id del almacenamientos de la proyecciones de levante, cria y reproductora';
            public       postgres    false    313            :           1259    16747 
   txlot_eggs    TABLE     �   CREATE TABLE public.txlot_eggs (
    lot_eggs_id integer DEFAULT nextval('public.lot_eggs_id_seq'::regclass) NOT NULL,
    theorical_performance double precision,
    week_date date,
    week integer
);
    DROP TABLE public.txlot_eggs;
       public         postgres    false    242    3                       0    0    TABLE txlot_eggs    COMMENT     S   COMMENT ON TABLE public.txlot_eggs IS 'Almacena los datos de los lotes de huevos';
            public       postgres    false    314                       0    0    COLUMN txlot_eggs.lot_eggs_id    COMMENT     L   COMMENT ON COLUMN public.txlot_eggs.lot_eggs_id IS 'Id del lote de huevos';
            public       postgres    false    314                       0    0 '   COLUMN txlot_eggs.theorical_performance    COMMENT     T   COMMENT ON COLUMN public.txlot_eggs.theorical_performance IS 'Rendimiento teorico';
            public       postgres    false    314                        0    0    COLUMN txlot_eggs.week_date    COMMENT     G   COMMENT ON COLUMN public.txlot_eggs.week_date IS 'Fecha de la semana';
            public       postgres    false    314            !           0    0    COLUMN txlot_eggs.week    COMMENT     6   COMMENT ON COLUMN public.txlot_eggs.week IS 'Semana';
            public       postgres    false    314            ;           1259    16751    txposturecurve    TABLE     �  CREATE TABLE public.txposturecurve (
    posture_curve_id integer DEFAULT nextval('public.posture_curve_id_seq'::regclass) NOT NULL,
    week integer NOT NULL,
    breed_id integer NOT NULL,
    theorical_performance double precision NOT NULL,
    historical_performance double precision,
    theorical_accum_mortality integer,
    historical_accum_mortality integer,
    theorical_uniformity double precision,
    historical_uniformity double precision,
    type_posture character varying(30) NOT NULL
);
 "   DROP TABLE public.txposturecurve;
       public         postgres    false    287    3            "           0    0    TABLE txposturecurve    COMMENT        COMMENT ON TABLE public.txposturecurve IS 'Almacena la información de la curva de postura por cada raza separada por semana';
            public       postgres    false    315            #           0    0 &   COLUMN txposturecurve.posture_curve_id    COMMENT     Y   COMMENT ON COLUMN public.txposturecurve.posture_curve_id IS 'Id de la curva de postura';
            public       postgres    false    315            $           0    0    COLUMN txposturecurve.week    COMMENT     _   COMMENT ON COLUMN public.txposturecurve.week IS 'Semana en la que inicia la curva de postura';
            public       postgres    false    315            %           0    0    COLUMN txposturecurve.breed_id    COMMENT     P   COMMENT ON COLUMN public.txposturecurve.breed_id IS 'Identificador de la raza';
            public       postgres    false    315            &           0    0 +   COLUMN txposturecurve.theorical_performance    COMMENT     X   COMMENT ON COLUMN public.txposturecurve.theorical_performance IS 'Desempeño teórico';
            public       postgres    false    315            '           0    0 ,   COLUMN txposturecurve.historical_performance    COMMENT     [   COMMENT ON COLUMN public.txposturecurve.historical_performance IS 'Desempeño histórico';
            public       postgres    false    315            (           0    0 /   COLUMN txposturecurve.theorical_accum_mortality    COMMENT     h   COMMENT ON COLUMN public.txposturecurve.theorical_accum_mortality IS 'Acumulado de mortalidad teorico';
            public       postgres    false    315            )           0    0 0   COLUMN txposturecurve.historical_accum_mortality    COMMENT     k   COMMENT ON COLUMN public.txposturecurve.historical_accum_mortality IS 'Acumulado de mortalidad historico';
            public       postgres    false    315            *           0    0 *   COLUMN txposturecurve.theorical_uniformity    COMMENT     W   COMMENT ON COLUMN public.txposturecurve.theorical_uniformity IS 'Uniformidad teorica';
            public       postgres    false    315            +           0    0 +   COLUMN txposturecurve.historical_uniformity    COMMENT     Z   COMMENT ON COLUMN public.txposturecurve.historical_uniformity IS 'Uniformidad historica';
            public       postgres    false    315            ,           0    0 "   COLUMN txposturecurve.type_posture    COMMENT     K   COMMENT ON COLUMN public.txposturecurve.type_posture IS 'Tipo de postura';
            public       postgres    false    315            <           1259    16755    txprogrammed_eggs    TABLE     �  CREATE TABLE public.txprogrammed_eggs (
    programmed_eggs_id integer DEFAULT nextval('public.programmed_eggs_id_seq'::regclass) NOT NULL,
    incubator_id integer NOT NULL,
    lot_breed character varying(45),
    lot_incubator character varying(45),
    use_date date,
    eggs integer,
    breed_id integer NOT NULL,
    execution_quantity integer,
    eggs_storage_id integer NOT NULL,
    confirm integer,
    released boolean
);
 %   DROP TABLE public.txprogrammed_eggs;
       public         postgres    false    290    3            -           0    0    TABLE txprogrammed_eggs    COMMENT        COMMENT ON TABLE public.txprogrammed_eggs IS 'Almacena la proyección, programación y ejecución del módulo de incubadoras';
            public       postgres    false    316            .           0    0 +   COLUMN txprogrammed_eggs.programmed_eggs_id    COMMENT     j   COMMENT ON COLUMN public.txprogrammed_eggs.programmed_eggs_id IS 'Id de las programacion de incubadoras';
            public       postgres    false    316            /           0    0 %   COLUMN txprogrammed_eggs.incubator_id    COMMENT     O   COMMENT ON COLUMN public.txprogrammed_eggs.incubator_id IS 'Id de incubadora';
            public       postgres    false    316            0           0    0 "   COLUMN txprogrammed_eggs.lot_breed    COMMENT     I   COMMENT ON COLUMN public.txprogrammed_eggs.lot_breed IS 'Lote por raza';
            public       postgres    false    316            1           0    0 &   COLUMN txprogrammed_eggs.lot_incubator    COMMENT     S   COMMENT ON COLUMN public.txprogrammed_eggs.lot_incubator IS 'Lote de incubadoras';
            public       postgres    false    316            2           0    0    COLUMN txprogrammed_eggs.eggs    COMMENT     I   COMMENT ON COLUMN public.txprogrammed_eggs.eggs IS 'Cantidad de huevos';
            public       postgres    false    316            3           0    0 !   COLUMN txprogrammed_eggs.breed_id    COMMENT     E   COMMENT ON COLUMN public.txprogrammed_eggs.breed_id IS 'Id de raza';
            public       postgres    false    316            4           0    0 +   COLUMN txprogrammed_eggs.execution_quantity    COMMENT     [   COMMENT ON COLUMN public.txprogrammed_eggs.execution_quantity IS 'Cantidad de ejecución';
            public       postgres    false    316            =           1259    16759    txscenarioformula    TABLE     �  CREATE TABLE public.txscenarioformula (
    scenario_formula_id integer DEFAULT nextval('public.scenario_formula_id_seq'::regclass) NOT NULL,
    process_id integer NOT NULL,
    predecessor_id integer NOT NULL,
    parameter_id integer NOT NULL,
    sign integer,
    divider double precision,
    duration integer,
    scenario_id integer NOT NULL,
    measure_id integer NOT NULL
);
 %   DROP TABLE public.txscenarioformula;
       public         postgres    false    292    3            5           0    0    TABLE txscenarioformula    COMMENT     �   COMMENT ON TABLE public.txscenarioformula IS 'Almacena los datos para la formulación de salida de la planificación regresiva';
            public       postgres    false    317            6           0    0 ,   COLUMN txscenarioformula.scenario_formula_id    COMMENT     d   COMMENT ON COLUMN public.txscenarioformula.scenario_formula_id IS 'Id de la formula del escenario';
            public       postgres    false    317            7           0    0 #   COLUMN txscenarioformula.process_id    COMMENT     K   COMMENT ON COLUMN public.txscenarioformula.process_id IS 'Id del proceso';
            public       postgres    false    317            8           0    0 '   COLUMN txscenarioformula.predecessor_id    COMMENT     R   COMMENT ON COLUMN public.txscenarioformula.predecessor_id IS 'Id del predecesor';
            public       postgres    false    317            9           0    0 %   COLUMN txscenarioformula.parameter_id    COMMENT     O   COMMENT ON COLUMN public.txscenarioformula.parameter_id IS 'Id del parametro';
            public       postgres    false    317            :           0    0    COLUMN txscenarioformula.sign    COMMENT     E   COMMENT ON COLUMN public.txscenarioformula.sign IS 'Firma de datos';
            public       postgres    false    317            ;           0    0     COLUMN txscenarioformula.divider    COMMENT     J   COMMENT ON COLUMN public.txscenarioformula.divider IS 'divisor de datos';
            public       postgres    false    317            <           0    0 !   COLUMN txscenarioformula.duration    COMMENT     Q   COMMENT ON COLUMN public.txscenarioformula.duration IS 'Duracion de la formula';
            public       postgres    false    317            =           0    0 $   COLUMN txscenarioformula.scenario_id    COMMENT     N   COMMENT ON COLUMN public.txscenarioformula.scenario_id IS 'Id del escenario';
            public       postgres    false    317            >           0    0 #   COLUMN txscenarioformula.measure_id    COMMENT     M   COMMENT ON COLUMN public.txscenarioformula.measure_id IS 'Id de la medida
';
            public       postgres    false    317            >           1259    16763    txscenarioparameter    TABLE     c  CREATE TABLE public.txscenarioparameter (
    scenario_parameter_id integer DEFAULT nextval('public.scenario_parameter_id_seq'::regclass) NOT NULL,
    process_id integer NOT NULL,
    parameter_id integer NOT NULL,
    use_year integer,
    use_month integer,
    use_value integer,
    scenario_id integer NOT NULL,
    value_units integer DEFAULT 0
);
 '   DROP TABLE public.txscenarioparameter;
       public         postgres    false    294    3            ?           0    0    TABLE txscenarioparameter    COMMENT     s   COMMENT ON TABLE public.txscenarioparameter IS 'Almacena las metas de producción ingresadas para los escenarios';
            public       postgres    false    318            @           0    0 0   COLUMN txscenarioparameter.scenario_parameter_id    COMMENT     l   COMMENT ON COLUMN public.txscenarioparameter.scenario_parameter_id IS 'Id de los parametros del escenario';
            public       postgres    false    318            A           0    0 %   COLUMN txscenarioparameter.process_id    COMMENT     M   COMMENT ON COLUMN public.txscenarioparameter.process_id IS 'Id del proceso';
            public       postgres    false    318            B           0    0 '   COLUMN txscenarioparameter.parameter_id    COMMENT     Q   COMMENT ON COLUMN public.txscenarioparameter.parameter_id IS 'Id del parametro';
            public       postgres    false    318            C           0    0 #   COLUMN txscenarioparameter.use_year    COMMENT     O   COMMENT ON COLUMN public.txscenarioparameter.use_year IS 'Año del parametro';
            public       postgres    false    318            D           0    0 $   COLUMN txscenarioparameter.use_month    COMMENT     O   COMMENT ON COLUMN public.txscenarioparameter.use_month IS 'Mes del parametro';
            public       postgres    false    318            E           0    0 $   COLUMN txscenarioparameter.use_value    COMMENT     Q   COMMENT ON COLUMN public.txscenarioparameter.use_value IS 'Valor del parametro';
            public       postgres    false    318            F           0    0 &   COLUMN txscenarioparameter.scenario_id    COMMENT     P   COMMENT ON COLUMN public.txscenarioparameter.scenario_id IS 'Id del escenario';
            public       postgres    false    318            G           0    0 &   COLUMN txscenarioparameter.value_units    COMMENT     U   COMMENT ON COLUMN public.txscenarioparameter.value_units IS 'Valor de las unidades';
            public       postgres    false    318            ?           1259    16768    txscenarioparameterday    TABLE     {  CREATE TABLE public.txscenarioparameterday (
    scenario_parameter_day_id integer DEFAULT nextval('public.scenario_parameter_day_seq'::regclass) NOT NULL,
    use_day integer,
    parameter_id integer NOT NULL,
    units_day integer,
    scenario_id integer NOT NULL,
    sequence integer,
    use_month integer,
    use_year integer,
    week_day integer,
    use_week date
);
 *   DROP TABLE public.txscenarioparameterday;
       public         postgres    false    293    3            H           0    0    TABLE txscenarioparameterday    COMMENT     V   COMMENT ON TABLE public.txscenarioparameterday IS 'Almcacena los parametros por dia';
            public       postgres    false    319            I           0    0 7   COLUMN txscenarioparameterday.scenario_parameter_day_id    COMMENT     m   COMMENT ON COLUMN public.txscenarioparameterday.scenario_parameter_day_id IS 'Id de los parametros del dia';
            public       postgres    false    319            J           0    0 %   COLUMN txscenarioparameterday.use_day    COMMENT     B   COMMENT ON COLUMN public.txscenarioparameterday.use_day IS 'Dia';
            public       postgres    false    319            K           0    0 *   COLUMN txscenarioparameterday.parameter_id    COMMENT     c   COMMENT ON COLUMN public.txscenarioparameterday.parameter_id IS 'Id de los parametros necesarios';
            public       postgres    false    319            L           0    0 '   COLUMN txscenarioparameterday.units_day    COMMENT     U   COMMENT ON COLUMN public.txscenarioparameterday.units_day IS 'Cantidad de material';
            public       postgres    false    319            M           0    0 )   COLUMN txscenarioparameterday.scenario_id    COMMENT     u   COMMENT ON COLUMN public.txscenarioparameterday.scenario_id IS 'Escenario al cual pertenece el scanrioparameterday';
            public       postgres    false    319            N           0    0 &   COLUMN txscenarioparameterday.sequence    COMMENT     R   COMMENT ON COLUMN public.txscenarioparameterday.sequence IS 'Secuencia del dia
';
            public       postgres    false    319            O           0    0 '   COLUMN txscenarioparameterday.use_month    COMMENT     ]   COMMENT ON COLUMN public.txscenarioparameterday.use_month IS 'Mes en que se ubica el día ';
            public       postgres    false    319            P           0    0 &   COLUMN txscenarioparameterday.use_year    COMMENT     ]   COMMENT ON COLUMN public.txscenarioparameterday.use_year IS 'Año en que se ubica el día ';
            public       postgres    false    319            Q           0    0 &   COLUMN txscenarioparameterday.week_day    COMMENT     P   COMMENT ON COLUMN public.txscenarioparameterday.week_day IS 'Dia de la semana';
            public       postgres    false    319            R           0    0 &   COLUMN txscenarioparameterday.use_week    COMMENT     F   COMMENT ON COLUMN public.txscenarioparameterday.use_week IS 'Semana';
            public       postgres    false    319            @           1259    16772    txscenarioposturecurve    TABLE     3  CREATE TABLE public.txscenarioposturecurve (
    scenario_posture_id integer DEFAULT nextval('public.scenario_posture_id_seq'::regclass) NOT NULL,
    posture_date date,
    eggs double precision,
    scenario_id integer NOT NULL,
    housingway_detail_id integer NOT NULL,
    breed_id integer NOT NULL
);
 *   DROP TABLE public.txscenarioposturecurve;
       public         postgres    false    295    3            S           0    0    TABLE txscenarioposturecurve    COMMENT     o   COMMENT ON TABLE public.txscenarioposturecurve IS 'Almacena los datos que se utilizan en la curva de postura';
            public       postgres    false    320            T           0    0 1   COLUMN txscenarioposturecurve.scenario_posture_id    COMMENT     i   COMMENT ON COLUMN public.txscenarioposturecurve.scenario_posture_id IS 'Id de la postura del escenario';
            public       postgres    false    320            U           0    0 *   COLUMN txscenarioposturecurve.posture_date    COMMENT     W   COMMENT ON COLUMN public.txscenarioposturecurve.posture_date IS 'Fecha de la postura';
            public       postgres    false    320            V           0    0 "   COLUMN txscenarioposturecurve.eggs    COMMENT     N   COMMENT ON COLUMN public.txscenarioposturecurve.eggs IS 'Cantidad de huevos';
            public       postgres    false    320            W           0    0 )   COLUMN txscenarioposturecurve.scenario_id    COMMENT     R   COMMENT ON COLUMN public.txscenarioposturecurve.scenario_id IS 'Id del scenario';
            public       postgres    false    320            X           0    0 2   COLUMN txscenarioposturecurve.housingway_detail_id    COMMENT     �   COMMENT ON COLUMN public.txscenarioposturecurve.housingway_detail_id IS 'Id de la programación y ejecución de los modelos de levante y cría y reproductora';
            public       postgres    false    320            Y           0    0 &   COLUMN txscenarioposturecurve.breed_id    COMMENT     M   COMMENT ON COLUMN public.txscenarioposturecurve.breed_id IS 'Id de la raza';
            public       postgres    false    320            A           1259    16776    txscenarioprocess    TABLE     4  CREATE TABLE public.txscenarioprocess (
    scenario_process_id integer DEFAULT nextval('public.scenario_process_id_seq'::regclass) NOT NULL,
    process_id integer NOT NULL,
    decrease_goal double precision,
    weight_goal double precision,
    duration_goal integer,
    scenario_id integer NOT NULL
);
 %   DROP TABLE public.txscenarioprocess;
       public         postgres    false    296    3            Z           0    0    TABLE txscenarioprocess    COMMENT     m   COMMENT ON TABLE public.txscenarioprocess IS 'Almacena los procesos asociados a cada uno de los escenarios';
            public       postgres    false    321            [           0    0 ,   COLUMN txscenarioprocess.scenario_process_id    COMMENT     a   COMMENT ON COLUMN public.txscenarioprocess.scenario_process_id IS 'Id del proceso de escenario';
            public       postgres    false    321            \           0    0 #   COLUMN txscenarioprocess.process_id    COMMENT     V   COMMENT ON COLUMN public.txscenarioprocess.process_id IS 'Id del proceso a utilizar';
            public       postgres    false    321            ]           0    0 &   COLUMN txscenarioprocess.decrease_goal    COMMENT     v   COMMENT ON COLUMN public.txscenarioprocess.decrease_goal IS 'Guarda los datos de la merma historia en dicho proceso';
            public       postgres    false    321            ^           0    0 $   COLUMN txscenarioprocess.weight_goal    COMMENT     q   COMMENT ON COLUMN public.txscenarioprocess.weight_goal IS 'Guarda los datos del peso historio en dicho proceso';
            public       postgres    false    321            _           0    0 &   COLUMN txscenarioprocess.duration_goal    COMMENT     y   COMMENT ON COLUMN public.txscenarioprocess.duration_goal IS 'Guarda los datos de la duracion historia en dicho proceso';
            public       postgres    false    321            `           0    0 $   COLUMN txscenarioprocess.scenario_id    COMMENT     X   COMMENT ON COLUMN public.txscenarioprocess.scenario_id IS 'Id del escenario utilizado';
            public       postgres    false    321            B           1259    16780 #   user_application_application_id_seq    SEQUENCE     �   CREATE SEQUENCE public.user_application_application_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 :   DROP SEQUENCE public.user_application_application_id_seq;
       public       postgres    false    3            C           1259    16782     user_application_user_app_id_seq    SEQUENCE     �   CREATE SEQUENCE public.user_application_user_app_id_seq
    START WITH 215
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 7   DROP SEQUENCE public.user_application_user_app_id_seq;
       public       postgres    false    3            D           1259    16784    user_application_user_id_seq    SEQUENCE     �   CREATE SEQUENCE public.user_application_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 3   DROP SEQUENCE public.user_application_user_id_seq;
       public       postgres    false    3                       2604    16786    txgoals_erp goals_erp_id    DEFAULT     �   ALTER TABLE ONLY public.txgoals_erp ALTER COLUMN goals_erp_id SET DEFAULT nextval('public.txgoals_erp_goals_erp_id_seq'::regclass);
 G   ALTER TABLE public.txgoals_erp ALTER COLUMN goals_erp_id DROP DEFAULT;
       public       postgres    false    310    309                      0    16398    aba_breeds_and_stages 
   TABLE DATA               m   COPY public.aba_breeds_and_stages (id, code, name, id_aba_consumption_and_mortality, id_process) FROM stdin;
    public       postgres    false    198   P�                0    16404    aba_consumption_and_mortality 
   TABLE DATA               m   COPY public.aba_consumption_and_mortality (id, code, name, id_breed, id_stage, id_aba_time_unit) FROM stdin;
    public       postgres    false    200   m�                0    16410 $   aba_consumption_and_mortality_detail 
   TABLE DATA               �   COPY public.aba_consumption_and_mortality_detail (id, id_aba_consumption_and_mortality, time_unit_number, consumption, mortality) FROM stdin;
    public       postgres    false    202   ��                0    16416    aba_elements 
   TABLE DATA               d   COPY public.aba_elements (id, code, name, id_aba_element_property, equivalent_quantity) FROM stdin;
    public       postgres    false    204   ��                0    16422    aba_elements_and_concentrations 
   TABLE DATA               �   COPY public.aba_elements_and_concentrations (id, id_aba_element, id_aba_formulation, proportion, id_element_equivalent, id_aba_element_property, equivalent_quantity) FROM stdin;
    public       postgres    false    206   Į                 0    16428    aba_elements_properties 
   TABLE DATA               A   COPY public.aba_elements_properties (id, code, name) FROM stdin;
    public       postgres    false    208   �      "          0    16434    aba_formulation 
   TABLE DATA               9   COPY public.aba_formulation (id, code, name) FROM stdin;
    public       postgres    false    210   %�      $          0    16440    aba_results 
   TABLE DATA               C   COPY public.aba_results (id, id_aba_element, quantity) FROM stdin;
    public       postgres    false    212   B�      &          0    16446    aba_stages_of_breeds_and_stages 
   TABLE DATA               w   COPY public.aba_stages_of_breeds_and_stages (id, id_aba_breeds_and_stages, id_formulation, name, duration) FROM stdin;
    public       postgres    false    214   _�      '          0    16450    aba_time_unit 
   TABLE DATA               G   COPY public.aba_time_unit (id, singular_name, plural_name) FROM stdin;
    public       postgres    false    215   |�      G          0    16516    mdapplication 
   TABLE DATA               O   COPY public.mdapplication (application_id, application_name, type) FROM stdin;
    public       postgres    false    247   ��      I          0    16522    mdapplication_rol 
   TABLE DATA               G   COPY public.mdapplication_rol (id, application_id, rol_id) FROM stdin;
    public       postgres    false    249   ��      J          0    16526    mdbreed 
   TABLE DATA               7   COPY public.mdbreed (breed_id, code, name) FROM stdin;
    public       postgres    false    250   �      K          0    16530    mdbroiler_product 
   TABLE DATA               \   COPY public.mdbroiler_product (broiler_product_id, name, days_eviction, weight) FROM stdin;
    public       postgres    false    251   �      L          0    16534 
   mdfarmtype 
   TABLE DATA               8   COPY public.mdfarmtype (farm_type_id, name) FROM stdin;
    public       postgres    false    252   Q�      N          0    16540 	   mdmeasure 
   TABLE DATA               b   COPY public.mdmeasure (measure_id, name, abbreviation, originvalue, valuekg, is_unit) FROM stdin;
    public       postgres    false    254   ��      P          0    16546    mdparameter 
   TABLE DATA               d   COPY public.mdparameter (parameter_id, description, type, measure_id, process_id, name) FROM stdin;
    public       postgres    false    256   �      R          0    16555 	   mdprocess 
   TABLE DATA               J  COPY public.mdprocess (process_id, process_order, product_id, stage_id, historical_decrease, theoretical_decrease, historical_weight, theoretical_weight, historical_duration, theoretical_duration, calendar_id, visible, name, predecessor_id, capacity, breed_id, gender, fattening_goal, type_posture, biological_active) FROM stdin;
    public       postgres    false    258   s�      T          0    16561 	   mdproduct 
   TABLE DATA               ;   COPY public.mdproduct (product_id, code, name) FROM stdin;
    public       postgres    false    260   ;�      V          0    16567    mdrol 
   TABLE DATA               T   COPY public.mdrol (rol_id, rol_name, admin_user_creator, creation_date) FROM stdin;
    public       postgres    false    262   ��      X          0    16573 
   mdscenario 
   TABLE DATA               o   COPY public.mdscenario (scenario_id, description, date_start, date_end, name, status, calendar_id) FROM stdin;
    public       postgres    false    264   <�      Z          0    16583    mdshedstatus 
   TABLE DATA               I   COPY public.mdshedstatus (shed_status_id, name, description) FROM stdin;
    public       postgres    false    266   ��      \          0    16589    mdstage 
   TABLE DATA               9   COPY public.mdstage (stage_id, order_, name) FROM stdin;
    public       postgres    false    268   #�      ^          0    16595    mduser 
   TABLE DATA                  COPY public.mduser (user_id, username, password, name, lastname, active, admi_user_creator, rol_id, creation_date) FROM stdin;
    public       postgres    false    270   ��      _          0    16599    oscenter 
   TABLE DATA               R   COPY public.oscenter (center_id, partnership_id, farm_id, name, code) FROM stdin;
    public       postgres    false    271   �      `          0    16603    oscenter_oswarehouse 
   TABLE DATA               x   COPY public.oscenter_oswarehouse (client_id, partnership_id, farm_id, center_id, warehouse_id, delete_mark) FROM stdin;
    public       postgres    false    272   ۶      a          0    16606    osfarm 
   TABLE DATA               S   COPY public.osfarm (farm_id, partnership_id, code, name, farm_type_id) FROM stdin;
    public       postgres    false    273   ��      b          0    16610    osincubator 
   TABLE DATA               �   COPY public.osincubator (incubator_id, incubator_plant_id, name, code, description, capacity, sunday, monday, tuesday, wednesday, thursday, friday, saturday, available) FROM stdin;
    public       postgres    false    274   ��      c          0    16614    osincubatorplant 
   TABLE DATA               �   COPY public.osincubatorplant (incubator_plant_id, name, code, description, partnership_id, max_storage, min_storage, acclimatized, suitable, expired) FROM stdin;
    public       postgres    false    275   3�      e          0    16620    ospartnership 
   TABLE DATA               Y   COPY public.ospartnership (partnership_id, name, address, description, code) FROM stdin;
    public       postgres    false    277   {�      g          0    16629    osshed 
   TABLE DATA               &  COPY public.osshed (shed_id, partnership_id, farm_id, center_id, code, statusshed_id, type_id, building_date, stall_width, stall_height, capacity_min, capacity_max, environment_id, rotation_days, nests_quantity, cages_quantity, birds_quantity, capacity_theoretical, avaliable_date) FROM stdin;
    public       postgres    false    279   (�      i          0    16640    ossilo 
   TABLE DATA               ?  COPY public.ossilo (silo_id, client_id, partnership_id, farm_id, center_id, name, rings_height, rings_height_id, height, height_unit_id, diameter, diameter_unit_id, total_rings_quantity, measuring_mechanism, cone_degrees, total_capacity_1, total_capacity_2, capacity_unit_id_1, capacity_unit_id_2, central) FROM stdin;
    public       postgres    false    281   ��      j          0    16644    ossilo_osshed 
   TABLE DATA               v   COPY public.ossilo_osshed (silo_id, shed_id, center_id, farm_id, partnership_id, client_id, deleted_mark) FROM stdin;
    public       postgres    false    282   ݹ      l          0    16649    osslaughterhouse 
   TABLE DATA               i   COPY public.osslaughterhouse (slaughterhouse_id, name, address, description, code, capacity) FROM stdin;
    public       postgres    false    284   ��      n          0    16658    oswarehouse 
   TABLE DATA               X   COPY public.oswarehouse (warehouse_id, partnership_id, farm_id, name, code) FROM stdin;
    public       postgres    false    286   O�      y          0    16682    txavailabilitysheds 
   TABLE DATA               k   COPY public.txavailabilitysheds (availability_shed_id, shed_id, init_date, end_date, lot_code) FROM stdin;
    public       postgres    false    297   ں      z          0    16686 	   txbroiler 
   TABLE DATA               �   COPY public.txbroiler (broiler_id, projected_date, projected_quantity, partnership_id, scenario_id, breed_id, lot_incubator, programmed_eggs_id) FROM stdin;
    public       postgres    false    298   ��      {          0    16690    txbroiler_detail 
   TABLE DATA               �   COPY public.txbroiler_detail (broiler_detail_id, broiler_id, scheduled_date, scheduled_quantity, farm_id, shed_id, confirm, execution_date, execution_quantity, lot, broiler_product_id) FROM stdin;
    public       postgres    false    299   �      |          0    16694    txbroilereviction 
   TABLE DATA               �   COPY public.txbroilereviction (broilereviction_id, projected_date, projected_quantity, partnership_id, scenario_id, breed_id, lot_incubator, broiler_detail_id) FROM stdin;
    public       postgres    false    300   1�      }          0    16698    txbroilereviction_detail 
   TABLE DATA               �   COPY public.txbroilereviction_detail (broilereviction_detail_id, broilereviction_id, scheduled_date, scheduled_quantity, farm_id, shed_id, confirm, execution_date, execution_quantity, lot, broiler_product_id, slaughterhouse_id) FROM stdin;
    public       postgres    false    301   N�      ~          0    16702    txbroilerproduct_detail 
   TABLE DATA               y   COPY public.txbroilerproduct_detail (broilerproduct_detail_id, broiler_detail, broiler_product_id, quantity) FROM stdin;
    public       postgres    false    302   k�                0    16706    txbroodermachine 
   TABLE DATA               �   COPY public.txbroodermachine (brooder_machine_id_seq, partnership_id, farm_id, capacity, sunday, monday, tuesday, wednesday, thursday, friday, saturday, name) FROM stdin;
    public       postgres    false    303   ��      �          0    16710 
   txcalendar 
   TABLE DATA               �   COPY public.txcalendar (calendar_id, description, saturday, sunday, week_start, code, year_start, year_end, generated) FROM stdin;
    public       postgres    false    304   ��      �          0    16714    txcalendarday 
   TABLE DATA               �   COPY public.txcalendarday (calendar_day_id, calendar_id, use_date, use_year, use_month, use_day, use_week, week_day, sequence, working_day) FROM stdin;
    public       postgres    false    305   ��      �          0    18789    txeggs_movements 
   TABLE DATA               �   COPY public.txeggs_movements (eggs_movements_id, fecha_movements, lot, quantity, type_movements, eggs_storage_id, description_adjustment) FROM stdin;
    public       postgres    false    326   �      �          0    16718    txeggs_planning 
   TABLE DATA               y   COPY public.txeggs_planning (egg_planning_id, month_planning, year_planning, scenario_id, planned, breed_id) FROM stdin;
    public       postgres    false    306   �      �          0    16722    txeggs_required 
   TABLE DATA               p   COPY public.txeggs_required (egg_required_id, use_month, use_year, scenario_id, required, breed_id) FROM stdin;
    public       postgres    false    307   k�      �          0    16726    txeggs_storage 
   TABLE DATA               �   COPY public.txeggs_storage (eggs_storage_id, incubator_plant_id, scenario_id, breed_id, init_date, end_date, lot, eggs, eggs_executed) FROM stdin;
    public       postgres    false    308   ��      �          0    16730    txgoals_erp 
   TABLE DATA               g   COPY public.txgoals_erp (goals_erp_id, use_week, use_value, product_id, code, scenario_id) FROM stdin;
    public       postgres    false    309   ��      �          0    16735    txhousingway 
   TABLE DATA               �   COPY public.txhousingway (housing_way_id, projected_quantity, projected_date, stage_id, partnership_id, scenario_id, breed_id, predecessor_id) FROM stdin;
    public       postgres    false    311   ǁ      �          0    16739    txhousingway_detail 
   TABLE DATA               �   COPY public.txhousingway_detail (housingway_detail_id, housing_way_id, scheduled_date, scheduled_quantity, farm_id, shed_id, confirm, execution_date, execution_quantity, lot, incubator_plant_id) FROM stdin;
    public       postgres    false    312   �      �          0    16743    txlot 
   TABLE DATA               �   COPY public.txlot (lot_id, lot_code, lot_origin, status, proyected_date, sheduled_date, proyected_quantity, sheduled_quantity, released_quantity, product_id, breed_id, gender, type_posture, shed_id, origin, farm_id, housing_way_id) FROM stdin;
    public       postgres    false    313   �      �          0    16747 
   txlot_eggs 
   TABLE DATA               Y   COPY public.txlot_eggs (lot_eggs_id, theorical_performance, week_date, week) FROM stdin;
    public       postgres    false    314   �      �          0    16751    txposturecurve 
   TABLE DATA               �   COPY public.txposturecurve (posture_curve_id, week, breed_id, theorical_performance, historical_performance, theorical_accum_mortality, historical_accum_mortality, theorical_uniformity, historical_uniformity, type_posture) FROM stdin;
    public       postgres    false    315   ;�      �          0    16755    txprogrammed_eggs 
   TABLE DATA               �   COPY public.txprogrammed_eggs (programmed_eggs_id, incubator_id, lot_breed, lot_incubator, use_date, eggs, breed_id, execution_quantity, eggs_storage_id, confirm, released) FROM stdin;
    public       postgres    false    316   ��      �          0    16759    txscenarioformula 
   TABLE DATA               �   COPY public.txscenarioformula (scenario_formula_id, process_id, predecessor_id, parameter_id, sign, divider, duration, scenario_id, measure_id) FROM stdin;
    public       postgres    false    317   ��      �          0    16763    txscenarioparameter 
   TABLE DATA               �   COPY public.txscenarioparameter (scenario_parameter_id, process_id, parameter_id, use_year, use_month, use_value, scenario_id, value_units) FROM stdin;
    public       postgres    false    318   N�      �          0    16768    txscenarioparameterday 
   TABLE DATA               �   COPY public.txscenarioparameterday (scenario_parameter_day_id, use_day, parameter_id, units_day, scenario_id, sequence, use_month, use_year, week_day, use_week) FROM stdin;
    public       postgres    false    319   X�      �          0    16772    txscenarioposturecurve 
   TABLE DATA               �   COPY public.txscenarioposturecurve (scenario_posture_id, posture_date, eggs, scenario_id, housingway_detail_id, breed_id) FROM stdin;
    public       postgres    false    320   �      �          0    16776    txscenarioprocess 
   TABLE DATA               �   COPY public.txscenarioprocess (scenario_process_id, process_id, decrease_goal, weight_goal, duration_goal, scenario_id) FROM stdin;
    public       postgres    false    321   ,�      a           0    0    abaTimeUnit_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public."abaTimeUnit_id_seq"', 2, false);
            public       postgres    false    196            b           0    0    aba_breeds_and_stages_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.aba_breeds_and_stages_id_seq', 8, false);
            public       postgres    false    197            c           0    0 +   aba_consumption_and_mortality_detail_id_seq    SEQUENCE SET     \   SELECT pg_catalog.setval('public.aba_consumption_and_mortality_detail_id_seq', 203, false);
            public       postgres    false    201            d           0    0 $   aba_consumption_and_mortality_id_seq    SEQUENCE SET     S   SELECT pg_catalog.setval('public.aba_consumption_and_mortality_id_seq', 8, false);
            public       postgres    false    199            e           0    0 &   aba_elements_and_concentrations_id_seq    SEQUENCE SET     V   SELECT pg_catalog.setval('public.aba_elements_and_concentrations_id_seq', 105, true);
            public       postgres    false    205            f           0    0    aba_elements_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.aba_elements_id_seq', 22, true);
            public       postgres    false    203            g           0    0    aba_elements_properties_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.aba_elements_properties_id_seq', 1, false);
            public       postgres    false    207            h           0    0    aba_formulation_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.aba_formulation_id_seq', 68, true);
            public       postgres    false    209            i           0    0    aba_results_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.aba_results_id_seq', 1, false);
            public       postgres    false    211            j           0    0 &   aba_stages_of_breeds_and_stages_id_seq    SEQUENCE SET     V   SELECT pg_catalog.setval('public.aba_stages_of_breeds_and_stages_id_seq', 24, false);
            public       postgres    false    213            k           0    0    availability_shed_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.availability_shed_id_seq', 301, true);
            public       postgres    false    216            l           0    0    base_day_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.base_day_id_seq', 3, true);
            public       postgres    false    217            m           0    0    breed_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.breed_id_seq', 18, true);
            public       postgres    false    218            n           0    0    broiler_detail_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.broiler_detail_id_seq', 93, true);
            public       postgres    false    219            o           0    0    broiler_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.broiler_id_seq', 113, true);
            public       postgres    false    220            p           0    0    broiler_product_detail_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.broiler_product_detail_id_seq', 2, true);
            public       postgres    false    221            q           0    0    broiler_product_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.broiler_product_id_seq', 22, true);
            public       postgres    false    222            r           0    0    broilereviction_detail_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.broilereviction_detail_id_seq', 206, true);
            public       postgres    false    223            s           0    0    broilereviction_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.broilereviction_id_seq', 151, true);
            public       postgres    false    224            t           0    0    brooder_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.brooder_id_seq', 52, true);
            public       postgres    false    225            u           0    0    brooder_machines_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.brooder_machines_id_seq', 7, true);
            public       postgres    false    226            v           0    0    calendar_day_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.calendar_day_id_seq', 25565, true);
            public       postgres    false    227            w           0    0    calendar_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.calendar_id_seq', 19, true);
            public       postgres    false    228            x           0    0    center_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.center_id_seq', 170, true);
            public       postgres    false    229            y           0    0    egg_planning_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.egg_planning_id_seq', 3232, true);
            public       postgres    false    230            z           0    0    egg_required_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.egg_required_id_seq', 3429, true);
            public       postgres    false    231            {           0    0    eggs_storage_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.eggs_storage_id_seq', 36807, true);
            public       postgres    false    232            |           0    0    farm_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.farm_id_seq', 176, true);
            public       postgres    false    233            }           0    0    farm_type_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.farm_type_id_seq', 3, true);
            public       postgres    false    234            ~           0    0    holiday_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.holiday_id_seq', 297, true);
            public       postgres    false    235                       0    0    housing_way_detail_id_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.housing_way_detail_id_seq', 658, true);
            public       postgres    false    236            �           0    0    housing_way_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.housing_way_id_seq', 861, true);
            public       postgres    false    237            �           0    0    incubator_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.incubator_id_seq', 29, true);
            public       postgres    false    238            �           0    0    incubator_plant_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.incubator_plant_id_seq', 20, true);
            public       postgres    false    239            �           0    0    industry_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.industry_id_seq', 1, true);
            public       postgres    false    240            �           0    0    line_id_seq    SEQUENCE SET     9   SELECT pg_catalog.setval('public.line_id_seq', 1, true);
            public       postgres    false    241            �           0    0    lot_eggs_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.lot_eggs_id_seq', 108, true);
            public       postgres    false    242            �           0    0    lot_fattening_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.lot_fattening_id_seq', 1, false);
            public       postgres    false    243            �           0    0 
   lot_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.lot_id_seq', 316, true);
            public       postgres    false    244            �           0    0    lot_liftbreeding_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.lot_liftbreeding_id_seq', 4, true);
            public       postgres    false    245            �           0    0     mdapplication_application_id_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('public.mdapplication_application_id_seq', 1, false);
            public       postgres    false    246            �           0    0    mdapplication_rol_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.mdapplication_rol_id_seq', 17, true);
            public       postgres    false    248            �           0    0    mdrol_rol_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.mdrol_rol_id_seq', 3, true);
            public       postgres    false    261            �           0    0    mduser_user_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.mduser_user_id_seq', 1, false);
            public       postgres    false    269            �           0    0    measure_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.measure_id_seq', 19, true);
            public       postgres    false    253            �           0    0    parameter_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.parameter_id_seq', 58, true);
            public       postgres    false    255            �           0    0    partnership_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.partnership_id_seq', 33, true);
            public       postgres    false    276            �           0    0    posture_curve_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.posture_curve_id_seq', 365, true);
            public       postgres    false    287            �           0    0    predecessor_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.predecessor_id_seq', 13, true);
            public       postgres    false    288            �           0    0    process_class_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.process_class_id_seq', 5, true);
            public       postgres    false    289            �           0    0    process_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.process_id_seq', 94, true);
            public       postgres    false    257            �           0    0    product_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.product_id_seq', 99, true);
            public       postgres    false    259            �           0    0    programmed_eggs_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.programmed_eggs_id_seq', 353, true);
            public       postgres    false    290            �           0    0    raspberry_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.raspberry_id_seq', 5, true);
            public       postgres    false    291            �           0    0    scenario_formula_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.scenario_formula_id_seq', 1302, true);
            public       postgres    false    292            �           0    0    scenario_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.scenario_id_seq', 172, true);
            public       postgres    false    263            �           0    0    scenario_parameter_day_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.scenario_parameter_day_seq', 57418, true);
            public       postgres    false    293            �           0    0    scenario_parameter_id_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('public.scenario_parameter_id_seq', 6485, true);
            public       postgres    false    294            �           0    0    scenario_posture_id_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.scenario_posture_id_seq', 61914, true);
            public       postgres    false    295            �           0    0    scenario_process_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.scenario_process_id_seq', 843, true);
            public       postgres    false    296            �           0    0    shed_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.shed_id_seq', 370, true);
            public       postgres    false    278            �           0    0    silo_id_seq    SEQUENCE SET     9   SELECT pg_catalog.setval('public.silo_id_seq', 4, true);
            public       postgres    false    280            �           0    0    slaughterhouse_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.slaughterhouse_id_seq', 37, true);
            public       postgres    false    283            �           0    0    stage_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.stage_id_seq', 27, true);
            public       postgres    false    267            �           0    0    status_shed_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.status_shed_id_seq', 10, true);
            public       postgres    false    265            �           0    0    txeggs_movements_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.txeggs_movements_id_seq', 2041, true);
            public       postgres    false    325            �           0    0    txgoals_erp_goals_erp_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.txgoals_erp_goals_erp_id_seq', 1920, true);
            public       postgres    false    310            �           0    0 #   user_application_application_id_seq    SEQUENCE SET     R   SELECT pg_catalog.setval('public.user_application_application_id_seq', 1, false);
            public       postgres    false    322            �           0    0     user_application_user_app_id_seq    SEQUENCE SET     P   SELECT pg_catalog.setval('public.user_application_user_app_id_seq', 215, true);
            public       postgres    false    323            �           0    0    user_application_user_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.user_application_user_id_seq', 1, false);
            public       postgres    false    324            �           0    0    warehouse_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.warehouse_id_seq', 135, true);
            public       postgres    false    285            K           2606    16788    aba_time_unit abaTimeUnit_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.aba_time_unit
    ADD CONSTRAINT "abaTimeUnit_pkey" PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.aba_time_unit DROP CONSTRAINT "abaTimeUnit_pkey";
       public         postgres    false    215            -           2606    16790 0   aba_breeds_and_stages aba_breeds_and_stages_pkey 
   CONSTRAINT     n   ALTER TABLE ONLY public.aba_breeds_and_stages
    ADD CONSTRAINT aba_breeds_and_stages_pkey PRIMARY KEY (id);
 Z   ALTER TABLE ONLY public.aba_breeds_and_stages DROP CONSTRAINT aba_breeds_and_stages_pkey;
       public         postgres    false    198            6           2606    16792 N   aba_consumption_and_mortality_detail aba_consumption_and_mortality_detail_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.aba_consumption_and_mortality_detail
    ADD CONSTRAINT aba_consumption_and_mortality_detail_pkey PRIMARY KEY (id);
 x   ALTER TABLE ONLY public.aba_consumption_and_mortality_detail DROP CONSTRAINT aba_consumption_and_mortality_detail_pkey;
       public         postgres    false    202            1           2606    16794 @   aba_consumption_and_mortality aba_consumption_and_mortality_pkey 
   CONSTRAINT     ~   ALTER TABLE ONLY public.aba_consumption_and_mortality
    ADD CONSTRAINT aba_consumption_and_mortality_pkey PRIMARY KEY (id);
 j   ALTER TABLE ONLY public.aba_consumption_and_mortality DROP CONSTRAINT aba_consumption_and_mortality_pkey;
       public         postgres    false    200            ;           2606    16796 D   aba_elements_and_concentrations aba_elements_and_concentrations_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.aba_elements_and_concentrations
    ADD CONSTRAINT aba_elements_and_concentrations_pkey PRIMARY KEY (id);
 n   ALTER TABLE ONLY public.aba_elements_and_concentrations DROP CONSTRAINT aba_elements_and_concentrations_pkey;
       public         postgres    false    206            9           2606    16798    aba_elements aba_elements_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.aba_elements
    ADD CONSTRAINT aba_elements_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.aba_elements DROP CONSTRAINT aba_elements_pkey;
       public         postgres    false    204            @           2606    16800 4   aba_elements_properties aba_elements_properties_pkey 
   CONSTRAINT     r   ALTER TABLE ONLY public.aba_elements_properties
    ADD CONSTRAINT aba_elements_properties_pkey PRIMARY KEY (id);
 ^   ALTER TABLE ONLY public.aba_elements_properties DROP CONSTRAINT aba_elements_properties_pkey;
       public         postgres    false    208            B           2606    16802 $   aba_formulation aba_formulation_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.aba_formulation
    ADD CONSTRAINT aba_formulation_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.aba_formulation DROP CONSTRAINT aba_formulation_pkey;
       public         postgres    false    210            D           2606    16804    aba_results aba_results_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.aba_results
    ADD CONSTRAINT aba_results_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.aba_results DROP CONSTRAINT aba_results_pkey;
       public         postgres    false    212            G           2606    16806 D   aba_stages_of_breeds_and_stages aba_stages_of_breeds_and_stages_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.aba_stages_of_breeds_and_stages
    ADD CONSTRAINT aba_stages_of_breeds_and_stages_pkey PRIMARY KEY (id);
 n   ALTER TABLE ONLY public.aba_stages_of_breeds_and_stages DROP CONSTRAINT aba_stages_of_breeds_and_stages_pkey;
       public         postgres    false    214            M           2606    16808    mdapplication application_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public.mdapplication
    ADD CONSTRAINT application_pkey PRIMARY KEY (application_id);
 H   ALTER TABLE ONLY public.mdapplication DROP CONSTRAINT application_pkey;
       public         postgres    false    247            Q           2606    16810 (   mdapplication_rol mdapplication_rol_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.mdapplication_rol
    ADD CONSTRAINT mdapplication_rol_pkey PRIMARY KEY (id);
 R   ALTER TABLE ONLY public.mdapplication_rol DROP CONSTRAINT mdapplication_rol_pkey;
       public         postgres    false    249            S           2606    16812    mdbreed mdbreed_code_key 
   CONSTRAINT     S   ALTER TABLE ONLY public.mdbreed
    ADD CONSTRAINT mdbreed_code_key UNIQUE (code);
 B   ALTER TABLE ONLY public.mdbreed DROP CONSTRAINT mdbreed_code_key;
       public         postgres    false    250            U           2606    16814    mdbreed mdbreed_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.mdbreed
    ADD CONSTRAINT mdbreed_pkey PRIMARY KEY (breed_id);
 >   ALTER TABLE ONLY public.mdbreed DROP CONSTRAINT mdbreed_pkey;
       public         postgres    false    250            W           2606    16816 (   mdbroiler_product mdbroiler_product_pkey 
   CONSTRAINT     v   ALTER TABLE ONLY public.mdbroiler_product
    ADD CONSTRAINT mdbroiler_product_pkey PRIMARY KEY (broiler_product_id);
 R   ALTER TABLE ONLY public.mdbroiler_product DROP CONSTRAINT mdbroiler_product_pkey;
       public         postgres    false    251            Y           2606    16818    mdfarmtype mdfarmtype_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.mdfarmtype
    ADD CONSTRAINT mdfarmtype_pkey PRIMARY KEY (farm_type_id);
 D   ALTER TABLE ONLY public.mdfarmtype DROP CONSTRAINT mdfarmtype_pkey;
       public         postgres    false    252            [           2606    16820    mdmeasure mdmeasure_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.mdmeasure
    ADD CONSTRAINT mdmeasure_pkey PRIMARY KEY (measure_id);
 B   ALTER TABLE ONLY public.mdmeasure DROP CONSTRAINT mdmeasure_pkey;
       public         postgres    false    254            _           2606    16822    mdparameter mdparameter_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.mdparameter
    ADD CONSTRAINT mdparameter_pkey PRIMARY KEY (parameter_id);
 F   ALTER TABLE ONLY public.mdparameter DROP CONSTRAINT mdparameter_pkey;
       public         postgres    false    256            e           2606    16824    mdprocess mdprocess_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.mdprocess
    ADD CONSTRAINT mdprocess_pkey PRIMARY KEY (process_id);
 B   ALTER TABLE ONLY public.mdprocess DROP CONSTRAINT mdprocess_pkey;
       public         postgres    false    258            g           2606    16826    mdproduct mdproduct_code_unique 
   CONSTRAINT     Z   ALTER TABLE ONLY public.mdproduct
    ADD CONSTRAINT mdproduct_code_unique UNIQUE (code);
 I   ALTER TABLE ONLY public.mdproduct DROP CONSTRAINT mdproduct_code_unique;
       public         postgres    false    260            i           2606    16828    mdproduct mdproduct_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.mdproduct
    ADD CONSTRAINT mdproduct_pkey PRIMARY KEY (product_id);
 B   ALTER TABLE ONLY public.mdproduct DROP CONSTRAINT mdproduct_pkey;
       public         postgres    false    260            p           2606    16830 !   mdscenario mdscenario_name_unique 
   CONSTRAINT     \   ALTER TABLE ONLY public.mdscenario
    ADD CONSTRAINT mdscenario_name_unique UNIQUE (name);
 K   ALTER TABLE ONLY public.mdscenario DROP CONSTRAINT mdscenario_name_unique;
       public         postgres    false    264            r           2606    16832    mdscenario mdscenario_pkey 
   CONSTRAINT     a   ALTER TABLE ONLY public.mdscenario
    ADD CONSTRAINT mdscenario_pkey PRIMARY KEY (scenario_id);
 D   ALTER TABLE ONLY public.mdscenario DROP CONSTRAINT mdscenario_pkey;
       public         postgres    false    264            t           2606    16834    mdshedstatus mdshedstatus_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public.mdshedstatus
    ADD CONSTRAINT mdshedstatus_pkey PRIMARY KEY (shed_status_id);
 H   ALTER TABLE ONLY public.mdshedstatus DROP CONSTRAINT mdshedstatus_pkey;
       public         postgres    false    266            v           2606    16836    mdstage mdstage_name_unique 
   CONSTRAINT     V   ALTER TABLE ONLY public.mdstage
    ADD CONSTRAINT mdstage_name_unique UNIQUE (name);
 E   ALTER TABLE ONLY public.mdstage DROP CONSTRAINT mdstage_name_unique;
       public         postgres    false    268            x           2606    16838    mdstage mdstage_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.mdstage
    ADD CONSTRAINT mdstage_pkey PRIMARY KEY (stage_id);
 >   ALTER TABLE ONLY public.mdstage DROP CONSTRAINT mdstage_pkey;
       public         postgres    false    268            {           2606    16840    mduser mduser_user_id_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY public.mduser
    ADD CONSTRAINT mduser_user_id_pkey PRIMARY KEY (user_id);
 D   ALTER TABLE ONLY public.mduser DROP CONSTRAINT mduser_user_id_pkey;
       public         postgres    false    270            }           2606    16842    mduser mduser_username_unique 
   CONSTRAINT     \   ALTER TABLE ONLY public.mduser
    ADD CONSTRAINT mduser_username_unique UNIQUE (username);
 G   ALTER TABLE ONLY public.mduser DROP CONSTRAINT mduser_username_unique;
       public         postgres    false    270            �           2606    16844 .   oscenter_oswarehouse oscenter_oswarehouse_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.oscenter_oswarehouse
    ADD CONSTRAINT oscenter_oswarehouse_pkey PRIMARY KEY (client_id, partnership_id, farm_id, center_id, warehouse_id);
 X   ALTER TABLE ONLY public.oscenter_oswarehouse DROP CONSTRAINT oscenter_oswarehouse_pkey;
       public         postgres    false    272    272    272    272    272            �           2606    16846 .   oscenter oscenter_partnership_farm_code_unique 
   CONSTRAINT     �   ALTER TABLE ONLY public.oscenter
    ADD CONSTRAINT oscenter_partnership_farm_code_unique UNIQUE (partnership_id, farm_id, code);
 X   ALTER TABLE ONLY public.oscenter DROP CONSTRAINT oscenter_partnership_farm_code_unique;
       public         postgres    false    271    271    271            �           2606    16848    oscenter oscenter_pkey 
   CONSTRAINT     [   ALTER TABLE ONLY public.oscenter
    ADD CONSTRAINT oscenter_pkey PRIMARY KEY (center_id);
 @   ALTER TABLE ONLY public.oscenter DROP CONSTRAINT oscenter_pkey;
       public         postgres    false    271            �           2606    16850 %   osfarm osfarm_partnership_code_unique 
   CONSTRAINT     p   ALTER TABLE ONLY public.osfarm
    ADD CONSTRAINT osfarm_partnership_code_unique UNIQUE (partnership_id, code);
 O   ALTER TABLE ONLY public.osfarm DROP CONSTRAINT osfarm_partnership_code_unique;
       public         postgres    false    273    273            �           2606    16852    osfarm osfarm_pkey 
   CONSTRAINT     U   ALTER TABLE ONLY public.osfarm
    ADD CONSTRAINT osfarm_pkey PRIMARY KEY (farm_id);
 <   ALTER TABLE ONLY public.osfarm DROP CONSTRAINT osfarm_pkey;
       public         postgres    false    273            �           2606    16854    osshed oshed_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.osshed
    ADD CONSTRAINT oshed_pkey PRIMARY KEY (shed_id);
 ;   ALTER TABLE ONLY public.osshed DROP CONSTRAINT oshed_pkey;
       public         postgres    false    279            �           2606    16856 2   osincubator osincubator_incubatorplant_code_unique 
   CONSTRAINT     �   ALTER TABLE ONLY public.osincubator
    ADD CONSTRAINT osincubator_incubatorplant_code_unique UNIQUE (incubator_plant_id, code);
 \   ALTER TABLE ONLY public.osincubator DROP CONSTRAINT osincubator_incubatorplant_code_unique;
       public         postgres    false    274    274            �           2606    16858    osincubator osincubator_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.osincubator
    ADD CONSTRAINT osincubator_pkey PRIMARY KEY (incubator_id);
 F   ALTER TABLE ONLY public.osincubator DROP CONSTRAINT osincubator_pkey;
       public         postgres    false    274            �           2606    16860 9   osincubatorplant osincubatorplant_partnership_code_unique 
   CONSTRAINT     �   ALTER TABLE ONLY public.osincubatorplant
    ADD CONSTRAINT osincubatorplant_partnership_code_unique UNIQUE (partnership_id, code);
 c   ALTER TABLE ONLY public.osincubatorplant DROP CONSTRAINT osincubatorplant_partnership_code_unique;
       public         postgres    false    275    275            �           2606    16862 &   osincubatorplant osincubatorplant_pkey 
   CONSTRAINT     t   ALTER TABLE ONLY public.osincubatorplant
    ADD CONSTRAINT osincubatorplant_pkey PRIMARY KEY (incubator_plant_id);
 P   ALTER TABLE ONLY public.osincubatorplant DROP CONSTRAINT osincubatorplant_pkey;
       public         postgres    false    275            �           2606    16864 '   ospartnership ospartnership_code_unique 
   CONSTRAINT     b   ALTER TABLE ONLY public.ospartnership
    ADD CONSTRAINT ospartnership_code_unique UNIQUE (code);
 Q   ALTER TABLE ONLY public.ospartnership DROP CONSTRAINT ospartnership_code_unique;
       public         postgres    false    277            �           2606    16866     ospartnership ospartnership_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY public.ospartnership
    ADD CONSTRAINT ospartnership_pkey PRIMARY KEY (partnership_id);
 J   ALTER TABLE ONLY public.ospartnership DROP CONSTRAINT ospartnership_pkey;
       public         postgres    false    277            �           2606    16868 1   osshed osshed_partnership_farm_center_code_unique 
   CONSTRAINT     �   ALTER TABLE ONLY public.osshed
    ADD CONSTRAINT osshed_partnership_farm_center_code_unique UNIQUE (partnership_id, farm_id, center_id, code);
 [   ALTER TABLE ONLY public.osshed DROP CONSTRAINT osshed_partnership_farm_center_code_unique;
       public         postgres    false    279    279    279    279            �           2606    16870     ossilo_osshed ossilo_osshed_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.ossilo_osshed
    ADD CONSTRAINT ossilo_osshed_pkey PRIMARY KEY (silo_id, shed_id);
 J   ALTER TABLE ONLY public.ossilo_osshed DROP CONSTRAINT ossilo_osshed_pkey;
       public         postgres    false    282    282            �           2606    16872    ossilo ossilo_pkey 
   CONSTRAINT     U   ALTER TABLE ONLY public.ossilo
    ADD CONSTRAINT ossilo_pkey PRIMARY KEY (silo_id);
 <   ALTER TABLE ONLY public.ossilo DROP CONSTRAINT ossilo_pkey;
       public         postgres    false    281            �           2606    16874 &   osslaughterhouse osslaughterhouse_pkey 
   CONSTRAINT     s   ALTER TABLE ONLY public.osslaughterhouse
    ADD CONSTRAINT osslaughterhouse_pkey PRIMARY KEY (slaughterhouse_id);
 P   ALTER TABLE ONLY public.osslaughterhouse DROP CONSTRAINT osslaughterhouse_pkey;
       public         postgres    false    284            �           2606    16876 4   oswarehouse oswarehouse_partnership_farm_code_unique 
   CONSTRAINT     �   ALTER TABLE ONLY public.oswarehouse
    ADD CONSTRAINT oswarehouse_partnership_farm_code_unique UNIQUE (partnership_id, farm_id, code);
 ^   ALTER TABLE ONLY public.oswarehouse DROP CONSTRAINT oswarehouse_partnership_farm_code_unique;
       public         postgres    false    286    286    286            �           2606    16878    oswarehouse oswarehouse_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.oswarehouse
    ADD CONSTRAINT oswarehouse_pkey PRIMARY KEY (warehouse_id);
 F   ALTER TABLE ONLY public.oswarehouse DROP CONSTRAINT oswarehouse_pkey;
       public         postgres    false    286            k           2606    16880    mdrol rol_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.mdrol
    ADD CONSTRAINT rol_pkey PRIMARY KEY (rol_id);
 8   ALTER TABLE ONLY public.mdrol DROP CONSTRAINT rol_pkey;
       public         postgres    false    262            �           2606    16882 ,   txavailabilitysheds txavailabilitysheds_pkey 
   CONSTRAINT     |   ALTER TABLE ONLY public.txavailabilitysheds
    ADD CONSTRAINT txavailabilitysheds_pkey PRIMARY KEY (availability_shed_id);
 V   ALTER TABLE ONLY public.txavailabilitysheds DROP CONSTRAINT txavailabilitysheds_pkey;
       public         postgres    false    297            �           2606    16884 &   txbroiler_detail txbroiler_detail_pkey 
   CONSTRAINT     s   ALTER TABLE ONLY public.txbroiler_detail
    ADD CONSTRAINT txbroiler_detail_pkey PRIMARY KEY (broiler_detail_id);
 P   ALTER TABLE ONLY public.txbroiler_detail DROP CONSTRAINT txbroiler_detail_pkey;
       public         postgres    false    299            �           2606    16886    txbroiler txbroiler_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.txbroiler
    ADD CONSTRAINT txbroiler_pkey PRIMARY KEY (broiler_id);
 B   ALTER TABLE ONLY public.txbroiler DROP CONSTRAINT txbroiler_pkey;
       public         postgres    false    298            �           2606    16888 6   txbroilereviction_detail txbroilereviction_detail_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.txbroilereviction_detail
    ADD CONSTRAINT txbroilereviction_detail_pkey PRIMARY KEY (broilereviction_detail_id);
 `   ALTER TABLE ONLY public.txbroilereviction_detail DROP CONSTRAINT txbroilereviction_detail_pkey;
       public         postgres    false    301            �           2606    16890 (   txbroilereviction txbroilereviction_pkey 
   CONSTRAINT     v   ALTER TABLE ONLY public.txbroilereviction
    ADD CONSTRAINT txbroilereviction_pkey PRIMARY KEY (broilereviction_id);
 R   ALTER TABLE ONLY public.txbroilereviction DROP CONSTRAINT txbroilereviction_pkey;
       public         postgres    false    300            �           2606    16892 4   txbroilerproduct_detail txbroilerproduct_detail_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.txbroilerproduct_detail
    ADD CONSTRAINT txbroilerproduct_detail_pkey PRIMARY KEY (broilerproduct_detail_id);
 ^   ALTER TABLE ONLY public.txbroilerproduct_detail DROP CONSTRAINT txbroilerproduct_detail_pkey;
       public         postgres    false    302            �           2606    16894 &   txbroodermachine txbroodermachine_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.txbroodermachine
    ADD CONSTRAINT txbroodermachine_pkey PRIMARY KEY (brooder_machine_id_seq);
 P   ALTER TABLE ONLY public.txbroodermachine DROP CONSTRAINT txbroodermachine_pkey;
       public         postgres    false    303            �           2606    16896    txcalendar txcalendar_pkey 
   CONSTRAINT     a   ALTER TABLE ONLY public.txcalendar
    ADD CONSTRAINT txcalendar_pkey PRIMARY KEY (calendar_id);
 D   ALTER TABLE ONLY public.txcalendar DROP CONSTRAINT txcalendar_pkey;
       public         postgres    false    304            �           2606    16898     txcalendarday txcalendarday_pkey 
   CONSTRAINT     k   ALTER TABLE ONLY public.txcalendarday
    ADD CONSTRAINT txcalendarday_pkey PRIMARY KEY (calendar_day_id);
 J   ALTER TABLE ONLY public.txcalendarday DROP CONSTRAINT txcalendarday_pkey;
       public         postgres    false    305            .           2606    18797 &   txeggs_movements txeggs_movements_pkey 
   CONSTRAINT     s   ALTER TABLE ONLY public.txeggs_movements
    ADD CONSTRAINT txeggs_movements_pkey PRIMARY KEY (eggs_movements_id);
 P   ALTER TABLE ONLY public.txeggs_movements DROP CONSTRAINT txeggs_movements_pkey;
       public         postgres    false    326            �           2606    16900 $   txeggs_planning txeggs_planning_pkey 
   CONSTRAINT     o   ALTER TABLE ONLY public.txeggs_planning
    ADD CONSTRAINT txeggs_planning_pkey PRIMARY KEY (egg_planning_id);
 N   ALTER TABLE ONLY public.txeggs_planning DROP CONSTRAINT txeggs_planning_pkey;
       public         postgres    false    306            �           2606    16902 $   txeggs_required txeggs_required_pkey 
   CONSTRAINT     o   ALTER TABLE ONLY public.txeggs_required
    ADD CONSTRAINT txeggs_required_pkey PRIMARY KEY (egg_required_id);
 N   ALTER TABLE ONLY public.txeggs_required DROP CONSTRAINT txeggs_required_pkey;
       public         postgres    false    307            �           2606    16904 "   txeggs_storage txeggs_storage_pkey 
   CONSTRAINT     m   ALTER TABLE ONLY public.txeggs_storage
    ADD CONSTRAINT txeggs_storage_pkey PRIMARY KEY (eggs_storage_id);
 L   ALTER TABLE ONLY public.txeggs_storage DROP CONSTRAINT txeggs_storage_pkey;
       public         postgres    false    308            �           2606    16906    txgoals_erp txgoals_erp_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.txgoals_erp
    ADD CONSTRAINT txgoals_erp_pkey PRIMARY KEY (goals_erp_id);
 F   ALTER TABLE ONLY public.txgoals_erp DROP CONSTRAINT txgoals_erp_pkey;
       public         postgres    false    309                        2606    16908 ,   txhousingway_detail txhousingway_detail_pkey 
   CONSTRAINT     |   ALTER TABLE ONLY public.txhousingway_detail
    ADD CONSTRAINT txhousingway_detail_pkey PRIMARY KEY (housingway_detail_id);
 V   ALTER TABLE ONLY public.txhousingway_detail DROP CONSTRAINT txhousingway_detail_pkey;
       public         postgres    false    312            �           2606    16910    txhousingway txhousingway_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public.txhousingway
    ADD CONSTRAINT txhousingway_pkey PRIMARY KEY (housing_way_id);
 H   ALTER TABLE ONLY public.txhousingway DROP CONSTRAINT txhousingway_pkey;
       public         postgres    false    311                       2606    16912    txlot_eggs txlot_eggs_pkey 
   CONSTRAINT     a   ALTER TABLE ONLY public.txlot_eggs
    ADD CONSTRAINT txlot_eggs_pkey PRIMARY KEY (lot_eggs_id);
 D   ALTER TABLE ONLY public.txlot_eggs DROP CONSTRAINT txlot_eggs_pkey;
       public         postgres    false    314                       2606    16914    txlot txlot_lot_code_key 
   CONSTRAINT     W   ALTER TABLE ONLY public.txlot
    ADD CONSTRAINT txlot_lot_code_key UNIQUE (lot_code);
 B   ALTER TABLE ONLY public.txlot DROP CONSTRAINT txlot_lot_code_key;
       public         postgres    false    313            	           2606    16916    txlot txlot_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.txlot
    ADD CONSTRAINT txlot_pkey PRIMARY KEY (lot_id);
 :   ALTER TABLE ONLY public.txlot DROP CONSTRAINT txlot_pkey;
       public         postgres    false    313                       2606    16918 "   txposturecurve txposturecurve_pkey 
   CONSTRAINT     n   ALTER TABLE ONLY public.txposturecurve
    ADD CONSTRAINT txposturecurve_pkey PRIMARY KEY (posture_curve_id);
 L   ALTER TABLE ONLY public.txposturecurve DROP CONSTRAINT txposturecurve_pkey;
       public         postgres    false    315                       2606    16920 (   txprogrammed_eggs txprogrammed_eggs_pkey 
   CONSTRAINT     v   ALTER TABLE ONLY public.txprogrammed_eggs
    ADD CONSTRAINT txprogrammed_eggs_pkey PRIMARY KEY (programmed_eggs_id);
 R   ALTER TABLE ONLY public.txprogrammed_eggs DROP CONSTRAINT txprogrammed_eggs_pkey;
       public         postgres    false    316                       2606    16922 (   txscenarioformula txscenarioformula_pkey 
   CONSTRAINT     w   ALTER TABLE ONLY public.txscenarioformula
    ADD CONSTRAINT txscenarioformula_pkey PRIMARY KEY (scenario_formula_id);
 R   ALTER TABLE ONLY public.txscenarioformula DROP CONSTRAINT txscenarioformula_pkey;
       public         postgres    false    317                       2606    16924 ,   txscenarioparameter txscenarioparameter_pkey 
   CONSTRAINT     }   ALTER TABLE ONLY public.txscenarioparameter
    ADD CONSTRAINT txscenarioparameter_pkey PRIMARY KEY (scenario_parameter_id);
 V   ALTER TABLE ONLY public.txscenarioparameter DROP CONSTRAINT txscenarioparameter_pkey;
       public         postgres    false    318            "           2606    16926 2   txscenarioparameterday txscenarioparameterday_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioparameterday
    ADD CONSTRAINT txscenarioparameterday_pkey PRIMARY KEY (scenario_parameter_day_id);
 \   ALTER TABLE ONLY public.txscenarioparameterday DROP CONSTRAINT txscenarioparameterday_pkey;
       public         postgres    false    319            (           2606    16928 2   txscenarioposturecurve txscenarioposturecurve_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioposturecurve
    ADD CONSTRAINT txscenarioposturecurve_pkey PRIMARY KEY (scenario_posture_id);
 \   ALTER TABLE ONLY public.txscenarioposturecurve DROP CONSTRAINT txscenarioposturecurve_pkey;
       public         postgres    false    320            ,           2606    16930 (   txscenarioprocess txscenarioprocess_pkey 
   CONSTRAINT     w   ALTER TABLE ONLY public.txscenarioprocess
    ADD CONSTRAINT txscenarioprocess_pkey PRIMARY KEY (scenario_process_id);
 R   ALTER TABLE ONLY public.txscenarioprocess DROP CONSTRAINT txscenarioprocess_pkey;
       public         postgres    false    321            m           2606    16932    mdrol uniqueRolName 
   CONSTRAINT     T   ALTER TABLE ONLY public.mdrol
    ADD CONSTRAINT "uniqueRolName" UNIQUE (rol_name);
 ?   ALTER TABLE ONLY public.mdrol DROP CONSTRAINT "uniqueRolName";
       public         postgres    false    262            �           1259    16933    calendar_index    INDEX     N   CREATE INDEX calendar_index ON public.txcalendarday USING hash (calendar_id);
 "   DROP INDEX public.calendar_index;
       public         postgres    false    305            `           1259    16934    calendarid_index    INDEX     L   CREATE INDEX calendarid_index ON public.mdprocess USING hash (calendar_id);
 $   DROP INDEX public.calendarid_index;
       public         postgres    false    258            �           1259    16935 
   code_index    INDEX     H   CREATE UNIQUE INDEX code_index ON public.txcalendar USING btree (code);
    DROP INDEX public.code_index;
       public         postgres    false    304            �           1259    16936 
   date_index    INDEX     G   CREATE INDEX date_index ON public.txcalendarday USING hash (use_date);
    DROP INDEX public.date_index;
       public         postgres    false    305            2           1259    16937    fki_FK_ id_aba_time_unit    INDEX     p   CREATE INDEX "fki_FK_ id_aba_time_unit" ON public.aba_consumption_and_mortality USING btree (id_aba_time_unit);
 .   DROP INDEX public."fki_FK_ id_aba_time_unit";
       public         postgres    false    200            H           1259    16938    fki_FK_id_aba_breeds_and_stages    INDEX     �   CREATE INDEX "fki_FK_id_aba_breeds_and_stages" ON public.aba_stages_of_breeds_and_stages USING btree (id_aba_breeds_and_stages);
 5   DROP INDEX public."fki_FK_id_aba_breeds_and_stages";
       public         postgres    false    214            .           1259    16939 '   fki_FK_id_aba_consumption_and_mortality    INDEX     �   CREATE INDEX "fki_FK_id_aba_consumption_and_mortality" ON public.aba_breeds_and_stages USING btree (id_aba_consumption_and_mortality);
 =   DROP INDEX public."fki_FK_id_aba_consumption_and_mortality";
       public         postgres    false    198            7           1259    16940 (   fki_FK_id_aba_consumption_and_mortality2    INDEX     �   CREATE INDEX "fki_FK_id_aba_consumption_and_mortality2" ON public.aba_consumption_and_mortality_detail USING btree (id_aba_consumption_and_mortality);
 >   DROP INDEX public."fki_FK_id_aba_consumption_and_mortality2";
       public         postgres    false    202            <           1259    16941    fki_FK_id_aba_element    INDEX     m   CREATE INDEX "fki_FK_id_aba_element" ON public.aba_elements_and_concentrations USING btree (id_aba_element);
 +   DROP INDEX public."fki_FK_id_aba_element";
       public         postgres    false    206            E           1259    16942    fki_FK_id_aba_element2    INDEX     Z   CREATE INDEX "fki_FK_id_aba_element2" ON public.aba_results USING btree (id_aba_element);
 ,   DROP INDEX public."fki_FK_id_aba_element2";
       public         postgres    false    212            =           1259    16943    fki_FK_id_aba_element_property    INDEX        CREATE INDEX "fki_FK_id_aba_element_property" ON public.aba_elements_and_concentrations USING btree (id_aba_element_property);
 4   DROP INDEX public."fki_FK_id_aba_element_property";
       public         postgres    false    206            >           1259    16944    fki_FK_id_aba_formulation    INDEX     u   CREATE INDEX "fki_FK_id_aba_formulation" ON public.aba_elements_and_concentrations USING btree (id_aba_formulation);
 /   DROP INDEX public."fki_FK_id_aba_formulation";
       public         postgres    false    206            3           1259    16945    fki_FK_id_breed    INDEX     _   CREATE INDEX "fki_FK_id_breed" ON public.aba_consumption_and_mortality USING btree (id_breed);
 %   DROP INDEX public."fki_FK_id_breed";
       public         postgres    false    200            I           1259    16946    fki_FK_id_formulation    INDEX     m   CREATE INDEX "fki_FK_id_formulation" ON public.aba_stages_of_breeds_and_stages USING btree (id_formulation);
 +   DROP INDEX public."fki_FK_id_formulation";
       public         postgres    false    214            /           1259    16947    fki_FK_id_process    INDEX     [   CREATE INDEX "fki_FK_id_process" ON public.aba_breeds_and_stages USING btree (id_process);
 '   DROP INDEX public."fki_FK_id_process";
       public         postgres    false    198            4           1259    16948    fki_FK_id_stage    INDEX     _   CREATE INDEX "fki_FK_id_stage" ON public.aba_consumption_and_mortality USING btree (id_stage);
 %   DROP INDEX public."fki_FK_id_stage";
       public         postgres    false    200            N           1259    16949 )   fki_mdapplication_rol_application_id_fkey    INDEX     q   CREATE INDEX fki_mdapplication_rol_application_id_fkey ON public.mdapplication_rol USING btree (application_id);
 =   DROP INDEX public.fki_mdapplication_rol_application_id_fkey;
       public         postgres    false    249            O           1259    16950 !   fki_mdapplication_rol_rol_id_fkey    INDEX     a   CREATE INDEX fki_mdapplication_rol_rol_id_fkey ON public.mdapplication_rol USING btree (rol_id);
 5   DROP INDEX public.fki_mdapplication_rol_rol_id_fkey;
       public         postgres    false    249            \           1259    16951    fki_mdparameter_measure_id_fkey    INDEX     ]   CREATE INDEX fki_mdparameter_measure_id_fkey ON public.mdparameter USING btree (measure_id);
 3   DROP INDEX public.fki_mdparameter_measure_id_fkey;
       public         postgres    false    256            ]           1259    16952    fki_mdparameter_process_id_fkey    INDEX     ]   CREATE INDEX fki_mdparameter_process_id_fkey ON public.mdparameter USING btree (process_id);
 3   DROP INDEX public.fki_mdparameter_process_id_fkey;
       public         postgres    false    256            a           1259    16953    fki_mdprocess_breed_id_fkey    INDEX     U   CREATE INDEX fki_mdprocess_breed_id_fkey ON public.mdprocess USING btree (breed_id);
 /   DROP INDEX public.fki_mdprocess_breed_id_fkey;
       public         postgres    false    258            n           1259    16954    fki_mdscenario_calendar_id_fkey    INDEX     ]   CREATE INDEX fki_mdscenario_calendar_id_fkey ON public.mdscenario USING btree (calendar_id);
 3   DROP INDEX public.fki_mdscenario_calendar_id_fkey;
       public         postgres    false    264            y           1259    16955    fki_mduser_rol_id_fkey    INDEX     K   CREATE INDEX fki_mduser_rol_id_fkey ON public.mduser USING btree (rol_id);
 *   DROP INDEX public.fki_mduser_rol_id_fkey;
       public         postgres    false    270            ~           1259    16956    fki_oscenter_farm_id_fkey    INDEX     Q   CREATE INDEX fki_oscenter_farm_id_fkey ON public.oscenter USING btree (farm_id);
 -   DROP INDEX public.fki_oscenter_farm_id_fkey;
       public         postgres    false    271            �           1259    16957 '   fki_oscenter_oswarehouse_center_id_fkey    INDEX     m   CREATE INDEX fki_oscenter_oswarehouse_center_id_fkey ON public.oscenter_oswarehouse USING btree (center_id);
 ;   DROP INDEX public.fki_oscenter_oswarehouse_center_id_fkey;
       public         postgres    false    272            �           1259    16958 %   fki_oscenter_oswarehouse_farm_id_fkey    INDEX     i   CREATE INDEX fki_oscenter_oswarehouse_farm_id_fkey ON public.oscenter_oswarehouse USING btree (farm_id);
 9   DROP INDEX public.fki_oscenter_oswarehouse_farm_id_fkey;
       public         postgres    false    272            �           1259    16959 ,   fki_oscenter_oswarehouse_partnership_id_fkey    INDEX     w   CREATE INDEX fki_oscenter_oswarehouse_partnership_id_fkey ON public.oscenter_oswarehouse USING btree (partnership_id);
 @   DROP INDEX public.fki_oscenter_oswarehouse_partnership_id_fkey;
       public         postgres    false    272            �           1259    16960 *   fki_oscenter_oswarehouse_warehouse_id_fkey    INDEX     s   CREATE INDEX fki_oscenter_oswarehouse_warehouse_id_fkey ON public.oscenter_oswarehouse USING btree (warehouse_id);
 >   DROP INDEX public.fki_oscenter_oswarehouse_warehouse_id_fkey;
       public         postgres    false    272                       1259    16961     fki_oscenter_partnership_id_fkey    INDEX     _   CREATE INDEX fki_oscenter_partnership_id_fkey ON public.oscenter USING btree (partnership_id);
 4   DROP INDEX public.fki_oscenter_partnership_id_fkey;
       public         postgres    false    271            �           1259    16962    fki_osfarm_farm_type_id_fkey    INDEX     W   CREATE INDEX fki_osfarm_farm_type_id_fkey ON public.osfarm USING btree (farm_type_id);
 0   DROP INDEX public.fki_osfarm_farm_type_id_fkey;
       public         postgres    false    273            �           1259    16963    fki_osfarm_partnership_id_fkey    INDEX     [   CREATE INDEX fki_osfarm_partnership_id_fkey ON public.osfarm USING btree (partnership_id);
 2   DROP INDEX public.fki_osfarm_partnership_id_fkey;
       public         postgres    false    273            �           1259    16964 '   fki_osincubator_incubator_plant_id_fkey    INDEX     m   CREATE INDEX fki_osincubator_incubator_plant_id_fkey ON public.osincubator USING btree (incubator_plant_id);
 ;   DROP INDEX public.fki_osincubator_incubator_plant_id_fkey;
       public         postgres    false    274            �           1259    16965 (   fki_osincubatorplant_partnership_id_fkey    INDEX     o   CREATE INDEX fki_osincubatorplant_partnership_id_fkey ON public.osincubatorplant USING btree (partnership_id);
 <   DROP INDEX public.fki_osincubatorplant_partnership_id_fkey;
       public         postgres    false    275            �           1259    16966    fki_osshed_center_id_fkey    INDEX     Q   CREATE INDEX fki_osshed_center_id_fkey ON public.osshed USING btree (center_id);
 -   DROP INDEX public.fki_osshed_center_id_fkey;
       public         postgres    false    279            �           1259    16967    fki_osshed_farm_id_fkey    INDEX     M   CREATE INDEX fki_osshed_farm_id_fkey ON public.osshed USING btree (farm_id);
 +   DROP INDEX public.fki_osshed_farm_id_fkey;
       public         postgres    false    279            �           1259    16968    fki_osshed_partnership_id_fkey    INDEX     [   CREATE INDEX fki_osshed_partnership_id_fkey ON public.osshed USING btree (partnership_id);
 2   DROP INDEX public.fki_osshed_partnership_id_fkey;
       public         postgres    false    279            �           1259    16969    fki_osshed_statusshed_id_fkey    INDEX     Y   CREATE INDEX fki_osshed_statusshed_id_fkey ON public.osshed USING btree (statusshed_id);
 1   DROP INDEX public.fki_osshed_statusshed_id_fkey;
       public         postgres    false    279            �           1259    16970    fki_ossilo_center_id_fkey    INDEX     Q   CREATE INDEX fki_ossilo_center_id_fkey ON public.ossilo USING btree (center_id);
 -   DROP INDEX public.fki_ossilo_center_id_fkey;
       public         postgres    false    281            �           1259    16971    fki_ossilo_farm_id_fkey    INDEX     M   CREATE INDEX fki_ossilo_farm_id_fkey ON public.ossilo USING btree (farm_id);
 +   DROP INDEX public.fki_ossilo_farm_id_fkey;
       public         postgres    false    281            �           1259    16972     fki_ossilo_osshed_center_id_fkey    INDEX     _   CREATE INDEX fki_ossilo_osshed_center_id_fkey ON public.ossilo_osshed USING btree (center_id);
 4   DROP INDEX public.fki_ossilo_osshed_center_id_fkey;
       public         postgres    false    282            �           1259    16973    fki_ossilo_osshed_farm_id_fkey    INDEX     [   CREATE INDEX fki_ossilo_osshed_farm_id_fkey ON public.ossilo_osshed USING btree (farm_id);
 2   DROP INDEX public.fki_ossilo_osshed_farm_id_fkey;
       public         postgres    false    282            �           1259    16974 %   fki_ossilo_osshed_partnership_id_fkey    INDEX     i   CREATE INDEX fki_ossilo_osshed_partnership_id_fkey ON public.ossilo_osshed USING btree (partnership_id);
 9   DROP INDEX public.fki_ossilo_osshed_partnership_id_fkey;
       public         postgres    false    282            �           1259    16975    fki_ossilo_osshed_shed_id_fkey    INDEX     [   CREATE INDEX fki_ossilo_osshed_shed_id_fkey ON public.ossilo_osshed USING btree (shed_id);
 2   DROP INDEX public.fki_ossilo_osshed_shed_id_fkey;
       public         postgres    false    282            �           1259    16976    fki_ossilo_osshed_silo_id_fkey    INDEX     [   CREATE INDEX fki_ossilo_osshed_silo_id_fkey ON public.ossilo_osshed USING btree (silo_id);
 2   DROP INDEX public.fki_ossilo_osshed_silo_id_fkey;
       public         postgres    false    282            �           1259    16977    fki_ossilo_partnership_id_fkey    INDEX     [   CREATE INDEX fki_ossilo_partnership_id_fkey ON public.ossilo USING btree (partnership_id);
 2   DROP INDEX public.fki_ossilo_partnership_id_fkey;
       public         postgres    false    281            �           1259    16978    fki_oswarehouse_farm_id_fkey    INDEX     W   CREATE INDEX fki_oswarehouse_farm_id_fkey ON public.oswarehouse USING btree (farm_id);
 0   DROP INDEX public.fki_oswarehouse_farm_id_fkey;
       public         postgres    false    286            �           1259    16979 #   fki_oswarehouse_partnership_id_fkey    INDEX     e   CREATE INDEX fki_oswarehouse_partnership_id_fkey ON public.oswarehouse USING btree (partnership_id);
 7   DROP INDEX public.fki_oswarehouse_partnership_id_fkey;
       public         postgres    false    286            b           1259    16980    fki_process_product_id_fkey    INDEX     W   CREATE INDEX fki_process_product_id_fkey ON public.mdprocess USING btree (product_id);
 /   DROP INDEX public.fki_process_product_id_fkey;
       public         postgres    false    258            c           1259    16981    fki_process_stage_id_fkey    INDEX     S   CREATE INDEX fki_process_stage_id_fkey ON public.mdprocess USING btree (stage_id);
 -   DROP INDEX public.fki_process_stage_id_fkey;
       public         postgres    false    258            �           1259    16982 %   fki_txavailabilitysheds_lot_code_fkey    INDEX     i   CREATE INDEX fki_txavailabilitysheds_lot_code_fkey ON public.txavailabilitysheds USING btree (lot_code);
 9   DROP INDEX public.fki_txavailabilitysheds_lot_code_fkey;
       public         postgres    false    297            �           1259    16983 $   fki_txavailabilitysheds_shed_id_fkey    INDEX     g   CREATE INDEX fki_txavailabilitysheds_shed_id_fkey ON public.txavailabilitysheds USING btree (shed_id);
 8   DROP INDEX public.fki_txavailabilitysheds_shed_id_fkey;
       public         postgres    false    297            �           1259    16984 $   fki_txbroiler_detail_broiler_id_fkey    INDEX     g   CREATE INDEX fki_txbroiler_detail_broiler_id_fkey ON public.txbroiler_detail USING btree (broiler_id);
 8   DROP INDEX public.fki_txbroiler_detail_broiler_id_fkey;
       public         postgres    false    299            �           1259    16985 !   fki_txbroiler_detail_farm_id_fkey    INDEX     a   CREATE INDEX fki_txbroiler_detail_farm_id_fkey ON public.txbroiler_detail USING btree (farm_id);
 5   DROP INDEX public.fki_txbroiler_detail_farm_id_fkey;
       public         postgres    false    299            �           1259    16986 !   fki_txbroiler_detail_shed_id_fkey    INDEX     a   CREATE INDEX fki_txbroiler_detail_shed_id_fkey ON public.txbroiler_detail USING btree (shed_id);
 5   DROP INDEX public.fki_txbroiler_detail_shed_id_fkey;
       public         postgres    false    299            �           1259    16987 %   fki_txbroiler_programmed_eggs_id_fkey    INDEX     i   CREATE INDEX fki_txbroiler_programmed_eggs_id_fkey ON public.txbroiler USING btree (programmed_eggs_id);
 9   DROP INDEX public.fki_txbroiler_programmed_eggs_id_fkey;
       public         postgres    false    298            �           1259    16988 #   fki_txbroilereviction_breed_id_fkey    INDEX     e   CREATE INDEX fki_txbroilereviction_breed_id_fkey ON public.txbroilereviction USING btree (breed_id);
 7   DROP INDEX public.fki_txbroilereviction_breed_id_fkey;
       public         postgres    false    300            �           1259    16989 ,   fki_txbroilereviction_detail_broiler_id_fkey    INDEX        CREATE INDEX fki_txbroilereviction_detail_broiler_id_fkey ON public.txbroilereviction_detail USING btree (broilereviction_id);
 @   DROP INDEX public.fki_txbroilereviction_detail_broiler_id_fkey;
       public         postgres    false    301            �           1259    16990 4   fki_txbroilereviction_detail_broiler_product_id_fkey    INDEX     �   CREATE INDEX fki_txbroilereviction_detail_broiler_product_id_fkey ON public.txbroilereviction_detail USING btree (broiler_product_id);
 H   DROP INDEX public.fki_txbroilereviction_detail_broiler_product_id_fkey;
       public         postgres    false    301            �           1259    16991 )   fki_txbroilereviction_detail_farm_id_fkey    INDEX     q   CREATE INDEX fki_txbroilereviction_detail_farm_id_fkey ON public.txbroilereviction_detail USING btree (farm_id);
 =   DROP INDEX public.fki_txbroilereviction_detail_farm_id_fkey;
       public         postgres    false    301            �           1259    16992 )   fki_txbroilereviction_detail_shed_id_fkey    INDEX     q   CREATE INDEX fki_txbroilereviction_detail_shed_id_fkey ON public.txbroilereviction_detail USING btree (shed_id);
 =   DROP INDEX public.fki_txbroilereviction_detail_shed_id_fkey;
       public         postgres    false    301            �           1259    16993 3   fki_txbroilereviction_detail_slaughterhouse_id_fkey    INDEX     �   CREATE INDEX fki_txbroilereviction_detail_slaughterhouse_id_fkey ON public.txbroilereviction_detail USING btree (slaughterhouse_id);
 G   DROP INDEX public.fki_txbroilereviction_detail_slaughterhouse_id_fkey;
       public         postgres    false    301            �           1259    16994 )   fki_txbroilereviction_partnership_id_fkey    INDEX     q   CREATE INDEX fki_txbroilereviction_partnership_id_fkey ON public.txbroilereviction USING btree (partnership_id);
 =   DROP INDEX public.fki_txbroilereviction_partnership_id_fkey;
       public         postgres    false    300            �           1259    16995 &   fki_txbroilereviction_scenario_id_fkey    INDEX     k   CREATE INDEX fki_txbroilereviction_scenario_id_fkey ON public.txbroilereviction USING btree (scenario_id);
 :   DROP INDEX public.fki_txbroilereviction_scenario_id_fkey;
       public         postgres    false    300            �           1259    16996 /   fki_txbroilerproduct_detail_broiler_detail_fkey    INDEX     }   CREATE INDEX fki_txbroilerproduct_detail_broiler_detail_fkey ON public.txbroilerproduct_detail USING btree (broiler_detail);
 C   DROP INDEX public.fki_txbroilerproduct_detail_broiler_detail_fkey;
       public         postgres    false    302            �           1259    16997 "   fki_txbroodermachines_farm_id_fkey    INDEX     b   CREATE INDEX fki_txbroodermachines_farm_id_fkey ON public.txbroodermachine USING btree (farm_id);
 6   DROP INDEX public.fki_txbroodermachines_farm_id_fkey;
       public         postgres    false    303            �           1259    16998 )   fki_txbroodermachines_partnership_id_fkey    INDEX     p   CREATE INDEX fki_txbroodermachines_partnership_id_fkey ON public.txbroodermachine USING btree (partnership_id);
 =   DROP INDEX public.fki_txbroodermachines_partnership_id_fkey;
       public         postgres    false    303            �           1259    16999 !   fki_txeggs_planning_breed_id_fkey    INDEX     a   CREATE INDEX fki_txeggs_planning_breed_id_fkey ON public.txeggs_planning USING btree (breed_id);
 5   DROP INDEX public.fki_txeggs_planning_breed_id_fkey;
       public         postgres    false    306            �           1259    17000 $   fki_txeggs_planning_scenario_id_fkey    INDEX     g   CREATE INDEX fki_txeggs_planning_scenario_id_fkey ON public.txeggs_planning USING btree (scenario_id);
 8   DROP INDEX public.fki_txeggs_planning_scenario_id_fkey;
       public         postgres    false    306            �           1259    17001 !   fki_txeggs_required_breed_id_fkey    INDEX     a   CREATE INDEX fki_txeggs_required_breed_id_fkey ON public.txeggs_required USING btree (breed_id);
 5   DROP INDEX public.fki_txeggs_required_breed_id_fkey;
       public         postgres    false    307            �           1259    17002 $   fki_txeggs_required_scenario_id_fkey    INDEX     g   CREATE INDEX fki_txeggs_required_scenario_id_fkey ON public.txeggs_required USING btree (scenario_id);
 8   DROP INDEX public.fki_txeggs_required_scenario_id_fkey;
       public         postgres    false    307            �           1259    17003     fki_txeggs_storage_breed_id_fkey    INDEX     _   CREATE INDEX fki_txeggs_storage_breed_id_fkey ON public.txeggs_storage USING btree (breed_id);
 4   DROP INDEX public.fki_txeggs_storage_breed_id_fkey;
       public         postgres    false    308            �           1259    17004 *   fki_txeggs_storage_incubator_plant_id_fkey    INDEX     s   CREATE INDEX fki_txeggs_storage_incubator_plant_id_fkey ON public.txeggs_storage USING btree (incubator_plant_id);
 >   DROP INDEX public.fki_txeggs_storage_incubator_plant_id_fkey;
       public         postgres    false    308            �           1259    17005 #   fki_txeggs_storage_scenario_id_fkey    INDEX     e   CREATE INDEX fki_txeggs_storage_scenario_id_fkey ON public.txeggs_storage USING btree (scenario_id);
 7   DROP INDEX public.fki_txeggs_storage_scenario_id_fkey;
       public         postgres    false    308            �           1259    17006    fki_txfattening_breed_id_fkey    INDEX     W   CREATE INDEX fki_txfattening_breed_id_fkey ON public.txbroiler USING btree (breed_id);
 1   DROP INDEX public.fki_txfattening_breed_id_fkey;
       public         postgres    false    298            �           1259    17007 #   fki_txfattening_partnership_id_fkey    INDEX     c   CREATE INDEX fki_txfattening_partnership_id_fkey ON public.txbroiler USING btree (partnership_id);
 7   DROP INDEX public.fki_txfattening_partnership_id_fkey;
       public         postgres    false    298            �           1259    17008     fki_txfattening_scenario_id_fkey    INDEX     ]   CREATE INDEX fki_txfattening_scenario_id_fkey ON public.txbroiler USING btree (scenario_id);
 4   DROP INDEX public.fki_txfattening_scenario_id_fkey;
       public         postgres    false    298            �           1259    17009    fki_txgoals_erp_product_id_fkey    INDEX     ]   CREATE INDEX fki_txgoals_erp_product_id_fkey ON public.txgoals_erp USING btree (product_id);
 3   DROP INDEX public.fki_txgoals_erp_product_id_fkey;
       public         postgres    false    309            �           1259    17010     fki_txgoals_erp_scenario_id_fkey    INDEX     _   CREATE INDEX fki_txgoals_erp_scenario_id_fkey ON public.txgoals_erp USING btree (scenario_id);
 4   DROP INDEX public.fki_txgoals_erp_scenario_id_fkey;
       public         postgres    false    309            �           1259    17011    fki_txhousingway_breed_id_fkey    INDEX     [   CREATE INDEX fki_txhousingway_breed_id_fkey ON public.txhousingway USING btree (breed_id);
 2   DROP INDEX public.fki_txhousingway_breed_id_fkey;
       public         postgres    false    311            �           1259    17012 $   fki_txhousingway_detail_farm_id_fkey    INDEX     g   CREATE INDEX fki_txhousingway_detail_farm_id_fkey ON public.txhousingway_detail USING btree (farm_id);
 8   DROP INDEX public.fki_txhousingway_detail_farm_id_fkey;
       public         postgres    false    312            �           1259    17013 +   fki_txhousingway_detail_housing_way_id_fkey    INDEX     u   CREATE INDEX fki_txhousingway_detail_housing_way_id_fkey ON public.txhousingway_detail USING btree (housing_way_id);
 ?   DROP INDEX public.fki_txhousingway_detail_housing_way_id_fkey;
       public         postgres    false    312            �           1259    17014 /   fki_txhousingway_detail_incubator_plant_id_fkey    INDEX     }   CREATE INDEX fki_txhousingway_detail_incubator_plant_id_fkey ON public.txhousingway_detail USING btree (incubator_plant_id);
 C   DROP INDEX public.fki_txhousingway_detail_incubator_plant_id_fkey;
       public         postgres    false    312            �           1259    17015 $   fki_txhousingway_detail_shed_id_fkey    INDEX     g   CREATE INDEX fki_txhousingway_detail_shed_id_fkey ON public.txhousingway_detail USING btree (shed_id);
 8   DROP INDEX public.fki_txhousingway_detail_shed_id_fkey;
       public         postgres    false    312            �           1259    17016 $   fki_txhousingway_partnership_id_fkey    INDEX     g   CREATE INDEX fki_txhousingway_partnership_id_fkey ON public.txhousingway USING btree (partnership_id);
 8   DROP INDEX public.fki_txhousingway_partnership_id_fkey;
       public         postgres    false    311            �           1259    17017 !   fki_txhousingway_scenario_id_fkey    INDEX     a   CREATE INDEX fki_txhousingway_scenario_id_fkey ON public.txhousingway USING btree (scenario_id);
 5   DROP INDEX public.fki_txhousingway_scenario_id_fkey;
       public         postgres    false    311            �           1259    17018    fki_txhousingway_stage_id_fkey    INDEX     [   CREATE INDEX fki_txhousingway_stage_id_fkey ON public.txhousingway USING btree (stage_id);
 2   DROP INDEX public.fki_txhousingway_stage_id_fkey;
       public         postgres    false    311                       1259    17019    fki_txlot_breed_id_fkey    INDEX     M   CREATE INDEX fki_txlot_breed_id_fkey ON public.txlot USING btree (breed_id);
 +   DROP INDEX public.fki_txlot_breed_id_fkey;
       public         postgres    false    313                       1259    17020    fki_txlot_farm_id_fkey    INDEX     K   CREATE INDEX fki_txlot_farm_id_fkey ON public.txlot USING btree (farm_id);
 *   DROP INDEX public.fki_txlot_farm_id_fkey;
       public         postgres    false    313                       1259    17021    fki_txlot_housin_way_id_fkey    INDEX     X   CREATE INDEX fki_txlot_housin_way_id_fkey ON public.txlot USING btree (housing_way_id);
 0   DROP INDEX public.fki_txlot_housin_way_id_fkey;
       public         postgres    false    313                       1259    17022    fki_txlot_product_id_fkey    INDEX     Q   CREATE INDEX fki_txlot_product_id_fkey ON public.txlot USING btree (product_id);
 -   DROP INDEX public.fki_txlot_product_id_fkey;
       public         postgres    false    313                       1259    17023    fki_txlot_shed_id_fkey    INDEX     K   CREATE INDEX fki_txlot_shed_id_fkey ON public.txlot USING btree (shed_id);
 *   DROP INDEX public.fki_txlot_shed_id_fkey;
       public         postgres    false    313                       1259    17024     fki_txposturecurve_breed_id_fkey    INDEX     _   CREATE INDEX fki_txposturecurve_breed_id_fkey ON public.txposturecurve USING btree (breed_id);
 4   DROP INDEX public.fki_txposturecurve_breed_id_fkey;
       public         postgres    false    315                       1259    17025 #   fki_txprogrammed_eggs_breed_id_fkey    INDEX     e   CREATE INDEX fki_txprogrammed_eggs_breed_id_fkey ON public.txprogrammed_eggs USING btree (breed_id);
 7   DROP INDEX public.fki_txprogrammed_eggs_breed_id_fkey;
       public         postgres    false    316                       1259    17026 *   fki_txprogrammed_eggs_eggs_storage_id_fkey    INDEX     s   CREATE INDEX fki_txprogrammed_eggs_eggs_storage_id_fkey ON public.txprogrammed_eggs USING btree (eggs_storage_id);
 >   DROP INDEX public.fki_txprogrammed_eggs_eggs_storage_id_fkey;
       public         postgres    false    316                       1259    17027 '   fki_txprogrammed_eggs_incubator_id_fkey    INDEX     m   CREATE INDEX fki_txprogrammed_eggs_incubator_id_fkey ON public.txprogrammed_eggs USING btree (incubator_id);
 ;   DROP INDEX public.fki_txprogrammed_eggs_incubator_id_fkey;
       public         postgres    false    316                       1259    17028 %   fki_txscenarioformula_measure_id_fkey    INDEX     i   CREATE INDEX fki_txscenarioformula_measure_id_fkey ON public.txscenarioformula USING btree (measure_id);
 9   DROP INDEX public.fki_txscenarioformula_measure_id_fkey;
       public         postgres    false    317                       1259    17029 '   fki_txscenarioformula_parameter_id_fkey    INDEX     m   CREATE INDEX fki_txscenarioformula_parameter_id_fkey ON public.txscenarioformula USING btree (parameter_id);
 ;   DROP INDEX public.fki_txscenarioformula_parameter_id_fkey;
       public         postgres    false    317                       1259    17030 %   fki_txscenarioformula_process_id_fkey    INDEX     i   CREATE INDEX fki_txscenarioformula_process_id_fkey ON public.txscenarioformula USING btree (process_id);
 9   DROP INDEX public.fki_txscenarioformula_process_id_fkey;
       public         postgres    false    317                       1259    17031 &   fki_txscenarioformula_scenario_id_fkey    INDEX     k   CREATE INDEX fki_txscenarioformula_scenario_id_fkey ON public.txscenarioformula USING btree (scenario_id);
 :   DROP INDEX public.fki_txscenarioformula_scenario_id_fkey;
       public         postgres    false    317                       1259    17032 )   fki_txscenarioparameter_parameter_id_fkey    INDEX     q   CREATE INDEX fki_txscenarioparameter_parameter_id_fkey ON public.txscenarioparameter USING btree (parameter_id);
 =   DROP INDEX public.fki_txscenarioparameter_parameter_id_fkey;
       public         postgres    false    318                       1259    17033 '   fki_txscenarioparameter_process_id_fkey    INDEX     m   CREATE INDEX fki_txscenarioparameter_process_id_fkey ON public.txscenarioparameter USING btree (process_id);
 ;   DROP INDEX public.fki_txscenarioparameter_process_id_fkey;
       public         postgres    false    318                       1259    17034 (   fki_txscenarioparameter_scenario_id_fkey    INDEX     o   CREATE INDEX fki_txscenarioparameter_scenario_id_fkey ON public.txscenarioparameter USING btree (scenario_id);
 <   DROP INDEX public.fki_txscenarioparameter_scenario_id_fkey;
       public         postgres    false    318                       1259    17035 ,   fki_txscenarioparameterday_parameter_id_fkey    INDEX     w   CREATE INDEX fki_txscenarioparameterday_parameter_id_fkey ON public.txscenarioparameterday USING btree (parameter_id);
 @   DROP INDEX public.fki_txscenarioparameterday_parameter_id_fkey;
       public         postgres    false    319                        1259    17036 +   fki_txscenarioparameterday_scenario_id_fkey    INDEX     u   CREATE INDEX fki_txscenarioparameterday_scenario_id_fkey ON public.txscenarioparameterday USING btree (scenario_id);
 ?   DROP INDEX public.fki_txscenarioparameterday_scenario_id_fkey;
       public         postgres    false    319            #           1259    17037 (   fki_txscenarioposturecurve_breed_id_fkey    INDEX     o   CREATE INDEX fki_txscenarioposturecurve_breed_id_fkey ON public.txscenarioposturecurve USING btree (breed_id);
 <   DROP INDEX public.fki_txscenarioposturecurve_breed_id_fkey;
       public         postgres    false    320            $           1259    17038 4   fki_txscenarioposturecurve_housingway_detail_id_fkey    INDEX     �   CREATE INDEX fki_txscenarioposturecurve_housingway_detail_id_fkey ON public.txscenarioposturecurve USING btree (housingway_detail_id);
 H   DROP INDEX public.fki_txscenarioposturecurve_housingway_detail_id_fkey;
       public         postgres    false    320            %           1259    17039 +   fki_txscenarioposturecurve_scenario_id_fkey    INDEX     u   CREATE INDEX fki_txscenarioposturecurve_scenario_id_fkey ON public.txscenarioposturecurve USING btree (scenario_id);
 ?   DROP INDEX public.fki_txscenarioposturecurve_scenario_id_fkey;
       public         postgres    false    320            )           1259    17040 %   fki_txscenarioprocess_process_id_fkey    INDEX     i   CREATE INDEX fki_txscenarioprocess_process_id_fkey ON public.txscenarioprocess USING btree (process_id);
 9   DROP INDEX public.fki_txscenarioprocess_process_id_fkey;
       public         postgres    false    321            *           1259    17041 &   fki_txscenarioprocess_scenario_id_fkey    INDEX     k   CREATE INDEX fki_txscenarioprocess_scenario_id_fkey ON public.txscenarioprocess USING btree (scenario_id);
 :   DROP INDEX public.fki_txscenarioprocess_scenario_id_fkey;
       public         postgres    false    321            &           1259    17042    posturedate_index    INDEX     [   CREATE INDEX posturedate_index ON public.txscenarioposturecurve USING hash (posture_date);
 %   DROP INDEX public.posturedate_index;
       public         postgres    false    320            �           1259    17043    sequence_index    INDEX     L   CREATE INDEX sequence_index ON public.txcalendarday USING btree (sequence);
 "   DROP INDEX public.sequence_index;
       public         postgres    false    305            9           2606    17044 ;   aba_stages_of_breeds_and_stages FK_id_aba_breeds_and_stages    FK CONSTRAINT     �   ALTER TABLE ONLY public.aba_stages_of_breeds_and_stages
    ADD CONSTRAINT "FK_id_aba_breeds_and_stages" FOREIGN KEY (id_aba_breeds_and_stages) REFERENCES public.aba_breeds_and_stages(id) ON DELETE CASCADE;
 g   ALTER TABLE ONLY public.aba_stages_of_breeds_and_stages DROP CONSTRAINT "FK_id_aba_breeds_and_stages";
       public       postgres    false    214    198    3117            /           2606    17049 9   aba_breeds_and_stages FK_id_aba_consumption_and_mortality    FK CONSTRAINT     �   ALTER TABLE ONLY public.aba_breeds_and_stages
    ADD CONSTRAINT "FK_id_aba_consumption_and_mortality" FOREIGN KEY (id_aba_consumption_and_mortality) REFERENCES public.aba_consumption_and_mortality(id);
 e   ALTER TABLE ONLY public.aba_breeds_and_stages DROP CONSTRAINT "FK_id_aba_consumption_and_mortality";
       public       postgres    false    200    198    3121            4           2606    17054 I   aba_consumption_and_mortality_detail FK_id_aba_consumption_and_mortality2    FK CONSTRAINT     �   ALTER TABLE ONLY public.aba_consumption_and_mortality_detail
    ADD CONSTRAINT "FK_id_aba_consumption_and_mortality2" FOREIGN KEY (id_aba_consumption_and_mortality) REFERENCES public.aba_consumption_and_mortality(id) ON DELETE CASCADE;
 u   ALTER TABLE ONLY public.aba_consumption_and_mortality_detail DROP CONSTRAINT "FK_id_aba_consumption_and_mortality2";
       public       postgres    false    202    3121    200            5           2606    17059 1   aba_elements_and_concentrations FK_id_aba_element    FK CONSTRAINT     �   ALTER TABLE ONLY public.aba_elements_and_concentrations
    ADD CONSTRAINT "FK_id_aba_element" FOREIGN KEY (id_aba_element) REFERENCES public.aba_elements(id);
 ]   ALTER TABLE ONLY public.aba_elements_and_concentrations DROP CONSTRAINT "FK_id_aba_element";
       public       postgres    false    204    3129    206            8           2606    17064    aba_results FK_id_aba_element2    FK CONSTRAINT     �   ALTER TABLE ONLY public.aba_results
    ADD CONSTRAINT "FK_id_aba_element2" FOREIGN KEY (id_aba_element) REFERENCES public.aba_elements(id);
 J   ALTER TABLE ONLY public.aba_results DROP CONSTRAINT "FK_id_aba_element2";
       public       postgres    false    3129    212    204            6           2606    17069 :   aba_elements_and_concentrations FK_id_aba_element_property    FK CONSTRAINT     �   ALTER TABLE ONLY public.aba_elements_and_concentrations
    ADD CONSTRAINT "FK_id_aba_element_property" FOREIGN KEY (id_aba_element_property) REFERENCES public.aba_elements_properties(id);
 f   ALTER TABLE ONLY public.aba_elements_and_concentrations DROP CONSTRAINT "FK_id_aba_element_property";
       public       postgres    false    208    3136    206            7           2606    17074 5   aba_elements_and_concentrations FK_id_aba_formulation    FK CONSTRAINT     �   ALTER TABLE ONLY public.aba_elements_and_concentrations
    ADD CONSTRAINT "FK_id_aba_formulation" FOREIGN KEY (id_aba_formulation) REFERENCES public.aba_formulation(id) ON DELETE CASCADE;
 a   ALTER TABLE ONLY public.aba_elements_and_concentrations DROP CONSTRAINT "FK_id_aba_formulation";
       public       postgres    false    210    206    3138            1           2606    17079 1   aba_consumption_and_mortality FK_id_aba_time_unit    FK CONSTRAINT     �   ALTER TABLE ONLY public.aba_consumption_and_mortality
    ADD CONSTRAINT "FK_id_aba_time_unit" FOREIGN KEY (id_aba_time_unit) REFERENCES public.aba_time_unit(id);
 ]   ALTER TABLE ONLY public.aba_consumption_and_mortality DROP CONSTRAINT "FK_id_aba_time_unit";
       public       postgres    false    200    215    3147            2           2606    17084 )   aba_consumption_and_mortality FK_id_breed    FK CONSTRAINT     �   ALTER TABLE ONLY public.aba_consumption_and_mortality
    ADD CONSTRAINT "FK_id_breed" FOREIGN KEY (id_breed) REFERENCES public.mdbreed(breed_id);
 U   ALTER TABLE ONLY public.aba_consumption_and_mortality DROP CONSTRAINT "FK_id_breed";
       public       postgres    false    3157    250    200            :           2606    17089 1   aba_stages_of_breeds_and_stages FK_id_formulation    FK CONSTRAINT     �   ALTER TABLE ONLY public.aba_stages_of_breeds_and_stages
    ADD CONSTRAINT "FK_id_formulation" FOREIGN KEY (id_formulation) REFERENCES public.aba_formulation(id);
 ]   ALTER TABLE ONLY public.aba_stages_of_breeds_and_stages DROP CONSTRAINT "FK_id_formulation";
       public       postgres    false    214    3138    210            0           2606    17094 #   aba_breeds_and_stages FK_id_process    FK CONSTRAINT     �   ALTER TABLE ONLY public.aba_breeds_and_stages
    ADD CONSTRAINT "FK_id_process" FOREIGN KEY (id_process) REFERENCES public.mdprocess(process_id);
 O   ALTER TABLE ONLY public.aba_breeds_and_stages DROP CONSTRAINT "FK_id_process";
       public       postgres    false    3173    198    258            3           2606    17099 )   aba_consumption_and_mortality FK_id_stage    FK CONSTRAINT     �   ALTER TABLE ONLY public.aba_consumption_and_mortality
    ADD CONSTRAINT "FK_id_stage" FOREIGN KEY (id_stage) REFERENCES public.mdstage(stage_id);
 U   ALTER TABLE ONLY public.aba_consumption_and_mortality DROP CONSTRAINT "FK_id_stage";
       public       postgres    false    3192    200    268            �           2606    18798 *   txeggs_movements eggs_movements_storage_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.txeggs_movements
    ADD CONSTRAINT eggs_movements_storage_id FOREIGN KEY (eggs_storage_id) REFERENCES public.txeggs_storage(eggs_storage_id);
 T   ALTER TABLE ONLY public.txeggs_movements DROP CONSTRAINT eggs_movements_storage_id;
       public       postgres    false    326    308    3312            ;           2606    17104 7   mdapplication_rol mdapplication_rol_application_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.mdapplication_rol
    ADD CONSTRAINT mdapplication_rol_application_id_fkey FOREIGN KEY (application_id) REFERENCES public.mdapplication(application_id);
 a   ALTER TABLE ONLY public.mdapplication_rol DROP CONSTRAINT mdapplication_rol_application_id_fkey;
       public       postgres    false    3149    249    247            <           2606    17109 /   mdapplication_rol mdapplication_rol_rol_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.mdapplication_rol
    ADD CONSTRAINT mdapplication_rol_rol_id_fkey FOREIGN KEY (rol_id) REFERENCES public.mdrol(rol_id);
 Y   ALTER TABLE ONLY public.mdapplication_rol DROP CONSTRAINT mdapplication_rol_rol_id_fkey;
       public       postgres    false    3179    249    262            =           2606    17114 '   mdparameter mdparameter_measure_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.mdparameter
    ADD CONSTRAINT mdparameter_measure_id_fkey FOREIGN KEY (measure_id) REFERENCES public.mdmeasure(measure_id) ON UPDATE CASCADE ON DELETE CASCADE;
 Q   ALTER TABLE ONLY public.mdparameter DROP CONSTRAINT mdparameter_measure_id_fkey;
       public       postgres    false    3163    256    254            >           2606    17119 '   mdparameter mdparameter_process_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.mdparameter
    ADD CONSTRAINT mdparameter_process_id_fkey FOREIGN KEY (process_id) REFERENCES public.mdprocess(process_id) ON UPDATE CASCADE ON DELETE CASCADE;
 Q   ALTER TABLE ONLY public.mdparameter DROP CONSTRAINT mdparameter_process_id_fkey;
       public       postgres    false    3173    256    258            ?           2606    17124 !   mdprocess mdprocess_breed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.mdprocess
    ADD CONSTRAINT mdprocess_breed_id_fkey FOREIGN KEY (breed_id) REFERENCES public.mdbreed(breed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 K   ALTER TABLE ONLY public.mdprocess DROP CONSTRAINT mdprocess_breed_id_fkey;
       public       postgres    false    3157    258    250            @           2606    17129 $   mdprocess mdprocess_calendar_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.mdprocess
    ADD CONSTRAINT mdprocess_calendar_id_fkey FOREIGN KEY (calendar_id) REFERENCES public.txcalendar(calendar_id) ON UPDATE CASCADE ON DELETE CASCADE;
 N   ALTER TABLE ONLY public.mdprocess DROP CONSTRAINT mdprocess_calendar_id_fkey;
       public       postgres    false    3294    258    304            A           2606    17134 #   mdprocess mdprocess_product_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.mdprocess
    ADD CONSTRAINT mdprocess_product_id_fkey FOREIGN KEY (product_id) REFERENCES public.mdproduct(product_id) ON UPDATE CASCADE ON DELETE CASCADE;
 M   ALTER TABLE ONLY public.mdprocess DROP CONSTRAINT mdprocess_product_id_fkey;
       public       postgres    false    258    260    3177            B           2606    17139 !   mdprocess mdprocess_stage_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.mdprocess
    ADD CONSTRAINT mdprocess_stage_id_fkey FOREIGN KEY (stage_id) REFERENCES public.mdstage(stage_id) ON UPDATE CASCADE ON DELETE CASCADE;
 K   ALTER TABLE ONLY public.mdprocess DROP CONSTRAINT mdprocess_stage_id_fkey;
       public       postgres    false    258    268    3192            C           2606    17144 &   mdscenario mdscenario_calendar_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.mdscenario
    ADD CONSTRAINT mdscenario_calendar_id_fkey FOREIGN KEY (calendar_id) REFERENCES public.txcalendar(calendar_id);
 P   ALTER TABLE ONLY public.mdscenario DROP CONSTRAINT mdscenario_calendar_id_fkey;
       public       postgres    false    3294    304    264            D           2606    17149    mduser mduser_rol_id_fkey    FK CONSTRAINT     {   ALTER TABLE ONLY public.mduser
    ADD CONSTRAINT mduser_rol_id_fkey FOREIGN KEY (rol_id) REFERENCES public.mdrol(rol_id);
 C   ALTER TABLE ONLY public.mduser DROP CONSTRAINT mduser_rol_id_fkey;
       public       postgres    false    3179    270    262            E           2606    17154    oscenter oscenter_farm_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.oscenter
    ADD CONSTRAINT oscenter_farm_id_fkey FOREIGN KEY (farm_id) REFERENCES public.osfarm(farm_id) ON UPDATE CASCADE ON DELETE CASCADE;
 H   ALTER TABLE ONLY public.oscenter DROP CONSTRAINT oscenter_farm_id_fkey;
       public       postgres    false    3215    271    273            G           2606    17159 8   oscenter_oswarehouse oscenter_oswarehouse_center_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.oscenter_oswarehouse
    ADD CONSTRAINT oscenter_oswarehouse_center_id_fkey FOREIGN KEY (center_id) REFERENCES public.oscenter(center_id) ON UPDATE CASCADE ON DELETE CASCADE;
 b   ALTER TABLE ONLY public.oscenter_oswarehouse DROP CONSTRAINT oscenter_oswarehouse_center_id_fkey;
       public       postgres    false    3203    272    271            H           2606    17164 6   oscenter_oswarehouse oscenter_oswarehouse_farm_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.oscenter_oswarehouse
    ADD CONSTRAINT oscenter_oswarehouse_farm_id_fkey FOREIGN KEY (farm_id) REFERENCES public.osfarm(farm_id) ON UPDATE CASCADE ON DELETE CASCADE;
 `   ALTER TABLE ONLY public.oscenter_oswarehouse DROP CONSTRAINT oscenter_oswarehouse_farm_id_fkey;
       public       postgres    false    3215    272    273            I           2606    17169 =   oscenter_oswarehouse oscenter_oswarehouse_partnership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.oscenter_oswarehouse
    ADD CONSTRAINT oscenter_oswarehouse_partnership_id_fkey FOREIGN KEY (partnership_id) REFERENCES public.ospartnership(partnership_id) ON UPDATE CASCADE ON DELETE CASCADE;
 g   ALTER TABLE ONLY public.oscenter_oswarehouse DROP CONSTRAINT oscenter_oswarehouse_partnership_id_fkey;
       public       postgres    false    3229    272    277            J           2606    17174 ;   oscenter_oswarehouse oscenter_oswarehouse_warehouse_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.oscenter_oswarehouse
    ADD CONSTRAINT oscenter_oswarehouse_warehouse_id_fkey FOREIGN KEY (warehouse_id) REFERENCES public.oswarehouse(warehouse_id) ON UPDATE CASCADE ON DELETE CASCADE;
 e   ALTER TABLE ONLY public.oscenter_oswarehouse DROP CONSTRAINT oscenter_oswarehouse_warehouse_id_fkey;
       public       postgres    false    3257    272    286            F           2606    17179 %   oscenter oscenter_partnership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.oscenter
    ADD CONSTRAINT oscenter_partnership_id_fkey FOREIGN KEY (partnership_id) REFERENCES public.ospartnership(partnership_id) ON UPDATE CASCADE ON DELETE CASCADE;
 O   ALTER TABLE ONLY public.oscenter DROP CONSTRAINT oscenter_partnership_id_fkey;
       public       postgres    false    3229    271    277            K           2606    17184    osfarm osfarm_farm_type_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.osfarm
    ADD CONSTRAINT osfarm_farm_type_id_fkey FOREIGN KEY (farm_type_id) REFERENCES public.mdfarmtype(farm_type_id) ON UPDATE CASCADE ON DELETE CASCADE;
 I   ALTER TABLE ONLY public.osfarm DROP CONSTRAINT osfarm_farm_type_id_fkey;
       public       postgres    false    3161    273    252            L           2606    17189 !   osfarm osfarm_partnership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.osfarm
    ADD CONSTRAINT osfarm_partnership_id_fkey FOREIGN KEY (partnership_id) REFERENCES public.ospartnership(partnership_id) ON UPDATE CASCADE ON DELETE CASCADE;
 K   ALTER TABLE ONLY public.osfarm DROP CONSTRAINT osfarm_partnership_id_fkey;
       public       postgres    false    273    277    3229            M           2606    17194 /   osincubator osincubator_incubator_plant_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.osincubator
    ADD CONSTRAINT osincubator_incubator_plant_id_fkey FOREIGN KEY (incubator_plant_id) REFERENCES public.osincubatorplant(incubator_plant_id) ON UPDATE CASCADE ON DELETE CASCADE;
 Y   ALTER TABLE ONLY public.osincubator DROP CONSTRAINT osincubator_incubator_plant_id_fkey;
       public       postgres    false    274    3225    275            N           2606    17199 5   osincubatorplant osincubatorplant_partnership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.osincubatorplant
    ADD CONSTRAINT osincubatorplant_partnership_id_fkey FOREIGN KEY (partnership_id) REFERENCES public.ospartnership(partnership_id) ON UPDATE CASCADE ON DELETE CASCADE;
 _   ALTER TABLE ONLY public.osincubatorplant DROP CONSTRAINT osincubatorplant_partnership_id_fkey;
       public       postgres    false    275    277    3229            O           2606    17204    osshed osshed_center_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.osshed
    ADD CONSTRAINT osshed_center_id_fkey FOREIGN KEY (center_id) REFERENCES public.oscenter(center_id) ON UPDATE CASCADE ON DELETE CASCADE;
 F   ALTER TABLE ONLY public.osshed DROP CONSTRAINT osshed_center_id_fkey;
       public       postgres    false    279    271    3203            P           2606    17209    osshed osshed_farm_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.osshed
    ADD CONSTRAINT osshed_farm_id_fkey FOREIGN KEY (farm_id) REFERENCES public.osfarm(farm_id) ON UPDATE CASCADE ON DELETE CASCADE;
 D   ALTER TABLE ONLY public.osshed DROP CONSTRAINT osshed_farm_id_fkey;
       public       postgres    false    279    273    3215            Q           2606    17214 !   osshed osshed_partnership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.osshed
    ADD CONSTRAINT osshed_partnership_id_fkey FOREIGN KEY (partnership_id) REFERENCES public.ospartnership(partnership_id) ON UPDATE CASCADE ON DELETE CASCADE;
 K   ALTER TABLE ONLY public.osshed DROP CONSTRAINT osshed_partnership_id_fkey;
       public       postgres    false    279    277    3229            R           2606    17219     osshed osshed_statusshed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.osshed
    ADD CONSTRAINT osshed_statusshed_id_fkey FOREIGN KEY (statusshed_id) REFERENCES public.mdshedstatus(shed_status_id) ON UPDATE CASCADE ON DELETE CASCADE;
 J   ALTER TABLE ONLY public.osshed DROP CONSTRAINT osshed_statusshed_id_fkey;
       public       postgres    false    279    266    3188            S           2606    17224    ossilo ossilo_center_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ossilo
    ADD CONSTRAINT ossilo_center_id_fkey FOREIGN KEY (center_id) REFERENCES public.oscenter(center_id) ON UPDATE CASCADE ON DELETE CASCADE;
 F   ALTER TABLE ONLY public.ossilo DROP CONSTRAINT ossilo_center_id_fkey;
       public       postgres    false    281    271    3203            T           2606    17229    ossilo ossilo_farm_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ossilo
    ADD CONSTRAINT ossilo_farm_id_fkey FOREIGN KEY (farm_id) REFERENCES public.osfarm(farm_id) ON UPDATE CASCADE ON DELETE CASCADE;
 D   ALTER TABLE ONLY public.ossilo DROP CONSTRAINT ossilo_farm_id_fkey;
       public       postgres    false    281    273    3215            V           2606    17234 *   ossilo_osshed ossilo_osshed_center_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ossilo_osshed
    ADD CONSTRAINT ossilo_osshed_center_id_fkey FOREIGN KEY (center_id) REFERENCES public.oscenter(center_id) ON UPDATE CASCADE ON DELETE CASCADE;
 T   ALTER TABLE ONLY public.ossilo_osshed DROP CONSTRAINT ossilo_osshed_center_id_fkey;
       public       postgres    false    282    271    3203            W           2606    17239 (   ossilo_osshed ossilo_osshed_farm_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ossilo_osshed
    ADD CONSTRAINT ossilo_osshed_farm_id_fkey FOREIGN KEY (farm_id) REFERENCES public.osfarm(farm_id) ON UPDATE CASCADE ON DELETE CASCADE;
 R   ALTER TABLE ONLY public.ossilo_osshed DROP CONSTRAINT ossilo_osshed_farm_id_fkey;
       public       postgres    false    282    273    3215            X           2606    17244 /   ossilo_osshed ossilo_osshed_partnership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ossilo_osshed
    ADD CONSTRAINT ossilo_osshed_partnership_id_fkey FOREIGN KEY (partnership_id) REFERENCES public.ospartnership(partnership_id) ON UPDATE CASCADE ON DELETE CASCADE;
 Y   ALTER TABLE ONLY public.ossilo_osshed DROP CONSTRAINT ossilo_osshed_partnership_id_fkey;
       public       postgres    false    282    277    3229            Y           2606    17249 (   ossilo_osshed ossilo_osshed_shed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ossilo_osshed
    ADD CONSTRAINT ossilo_osshed_shed_id_fkey FOREIGN KEY (shed_id) REFERENCES public.osshed(shed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 R   ALTER TABLE ONLY public.ossilo_osshed DROP CONSTRAINT ossilo_osshed_shed_id_fkey;
       public       postgres    false    282    279    3235            Z           2606    17254 (   ossilo_osshed ossilo_osshed_silo_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ossilo_osshed
    ADD CONSTRAINT ossilo_osshed_silo_id_fkey FOREIGN KEY (silo_id) REFERENCES public.ossilo(silo_id) ON UPDATE CASCADE ON DELETE CASCADE;
 R   ALTER TABLE ONLY public.ossilo_osshed DROP CONSTRAINT ossilo_osshed_silo_id_fkey;
       public       postgres    false    282    281    3242            U           2606    17259 !   ossilo ossilo_partnership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ossilo
    ADD CONSTRAINT ossilo_partnership_id_fkey FOREIGN KEY (partnership_id) REFERENCES public.ospartnership(partnership_id) ON UPDATE CASCADE ON DELETE CASCADE;
 K   ALTER TABLE ONLY public.ossilo DROP CONSTRAINT ossilo_partnership_id_fkey;
       public       postgres    false    281    277    3229            [           2606    17264 $   oswarehouse oswarehouse_farm_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.oswarehouse
    ADD CONSTRAINT oswarehouse_farm_id_fkey FOREIGN KEY (farm_id) REFERENCES public.osfarm(farm_id) ON UPDATE CASCADE ON DELETE CASCADE;
 N   ALTER TABLE ONLY public.oswarehouse DROP CONSTRAINT oswarehouse_farm_id_fkey;
       public       postgres    false    286    273    3215            \           2606    17269 +   oswarehouse oswarehouse_partnership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.oswarehouse
    ADD CONSTRAINT oswarehouse_partnership_id_fkey FOREIGN KEY (partnership_id) REFERENCES public.ospartnership(partnership_id) ON UPDATE CASCADE ON DELETE CASCADE;
 U   ALTER TABLE ONLY public.oswarehouse DROP CONSTRAINT oswarehouse_partnership_id_fkey;
       public       postgres    false    286    277    3229            ]           2606    17274 5   txavailabilitysheds txavailabilitysheds_lot_code_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txavailabilitysheds
    ADD CONSTRAINT txavailabilitysheds_lot_code_fkey FOREIGN KEY (lot_code) REFERENCES public.txlot(lot_code) ON UPDATE CASCADE ON DELETE CASCADE;
 _   ALTER TABLE ONLY public.txavailabilitysheds DROP CONSTRAINT txavailabilitysheds_lot_code_fkey;
       public       postgres    false    297    313    3335            ^           2606    17279 4   txavailabilitysheds txavailabilitysheds_shed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txavailabilitysheds
    ADD CONSTRAINT txavailabilitysheds_shed_id_fkey FOREIGN KEY (shed_id) REFERENCES public.osshed(shed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 ^   ALTER TABLE ONLY public.txavailabilitysheds DROP CONSTRAINT txavailabilitysheds_shed_id_fkey;
       public       postgres    false    297    279    3235            _           2606    17284 !   txbroiler txbroiler_breed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroiler
    ADD CONSTRAINT txbroiler_breed_id_fkey FOREIGN KEY (breed_id) REFERENCES public.mdbreed(breed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 K   ALTER TABLE ONLY public.txbroiler DROP CONSTRAINT txbroiler_breed_id_fkey;
       public       postgres    false    298    250    3157            c           2606    17289 1   txbroiler_detail txbroiler_detail_broiler_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroiler_detail
    ADD CONSTRAINT txbroiler_detail_broiler_id_fkey FOREIGN KEY (broiler_id) REFERENCES public.txbroiler(broiler_id) ON UPDATE CASCADE ON DELETE CASCADE;
 [   ALTER TABLE ONLY public.txbroiler_detail DROP CONSTRAINT txbroiler_detail_broiler_id_fkey;
       public       postgres    false    3267    298    299            d           2606    17294 .   txbroiler_detail txbroiler_detail_farm_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroiler_detail
    ADD CONSTRAINT txbroiler_detail_farm_id_fkey FOREIGN KEY (farm_id) REFERENCES public.osfarm(farm_id) ON UPDATE CASCADE ON DELETE CASCADE;
 X   ALTER TABLE ONLY public.txbroiler_detail DROP CONSTRAINT txbroiler_detail_farm_id_fkey;
       public       postgres    false    299    273    3215            e           2606    17299 .   txbroiler_detail txbroiler_detail_shed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroiler_detail
    ADD CONSTRAINT txbroiler_detail_shed_id_fkey FOREIGN KEY (shed_id) REFERENCES public.osshed(shed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 X   ALTER TABLE ONLY public.txbroiler_detail DROP CONSTRAINT txbroiler_detail_shed_id_fkey;
       public       postgres    false    299    279    3235            `           2606    17304 '   txbroiler txbroiler_partnership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroiler
    ADD CONSTRAINT txbroiler_partnership_id_fkey FOREIGN KEY (partnership_id) REFERENCES public.ospartnership(partnership_id) ON UPDATE CASCADE ON DELETE CASCADE;
 Q   ALTER TABLE ONLY public.txbroiler DROP CONSTRAINT txbroiler_partnership_id_fkey;
       public       postgres    false    298    277    3229            a           2606    17309 +   txbroiler txbroiler_programmed_eggs_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroiler
    ADD CONSTRAINT txbroiler_programmed_eggs_id_fkey FOREIGN KEY (programmed_eggs_id) REFERENCES public.txprogrammed_eggs(programmed_eggs_id) ON UPDATE CASCADE ON DELETE CASCADE;
 U   ALTER TABLE ONLY public.txbroiler DROP CONSTRAINT txbroiler_programmed_eggs_id_fkey;
       public       postgres    false    298    316    3347            b           2606    17314 $   txbroiler txbroiler_scenario_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroiler
    ADD CONSTRAINT txbroiler_scenario_id_fkey FOREIGN KEY (scenario_id) REFERENCES public.mdscenario(scenario_id) ON UPDATE CASCADE ON DELETE CASCADE;
 N   ALTER TABLE ONLY public.txbroiler DROP CONSTRAINT txbroiler_scenario_id_fkey;
       public       postgres    false    298    264    3186            f           2606    17319 1   txbroilereviction txbroilereviction_breed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroilereviction
    ADD CONSTRAINT txbroilereviction_breed_id_fkey FOREIGN KEY (breed_id) REFERENCES public.mdbreed(breed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 [   ALTER TABLE ONLY public.txbroilereviction DROP CONSTRAINT txbroilereviction_breed_id_fkey;
       public       postgres    false    300    250    3157            g           2606    17324 :   txbroilereviction txbroilereviction_broiler_detail_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroilereviction
    ADD CONSTRAINT txbroilereviction_broiler_detail_id_fkey FOREIGN KEY (broiler_detail_id) REFERENCES public.txbroiler_detail(broiler_detail_id);
 d   ALTER TABLE ONLY public.txbroilereviction DROP CONSTRAINT txbroilereviction_broiler_detail_id_fkey;
       public       postgres    false    300    299    3272            j           2606    17329 A   txbroilereviction_detail txbroilereviction_detail_broiler_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroilereviction_detail
    ADD CONSTRAINT txbroilereviction_detail_broiler_id_fkey FOREIGN KEY (broilereviction_id) REFERENCES public.txbroilereviction(broilereviction_id) ON UPDATE CASCADE ON DELETE CASCADE;
 k   ALTER TABLE ONLY public.txbroilereviction_detail DROP CONSTRAINT txbroilereviction_detail_broiler_id_fkey;
       public       postgres    false    301    300    3277            k           2606    17334 I   txbroilereviction_detail txbroilereviction_detail_broiler_product_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroilereviction_detail
    ADD CONSTRAINT txbroilereviction_detail_broiler_product_id_fkey FOREIGN KEY (broiler_product_id) REFERENCES public.mdbroiler_product(broiler_product_id) ON UPDATE CASCADE ON DELETE CASCADE;
 s   ALTER TABLE ONLY public.txbroilereviction_detail DROP CONSTRAINT txbroilereviction_detail_broiler_product_id_fkey;
       public       postgres    false    301    251    3159            l           2606    17339 >   txbroilereviction_detail txbroilereviction_detail_farm_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroilereviction_detail
    ADD CONSTRAINT txbroilereviction_detail_farm_id_fkey FOREIGN KEY (farm_id) REFERENCES public.osfarm(farm_id) ON UPDATE CASCADE ON DELETE CASCADE;
 h   ALTER TABLE ONLY public.txbroilereviction_detail DROP CONSTRAINT txbroilereviction_detail_farm_id_fkey;
       public       postgres    false    301    273    3215            m           2606    17344 >   txbroilereviction_detail txbroilereviction_detail_shed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroilereviction_detail
    ADD CONSTRAINT txbroilereviction_detail_shed_id_fkey FOREIGN KEY (shed_id) REFERENCES public.osshed(shed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 h   ALTER TABLE ONLY public.txbroilereviction_detail DROP CONSTRAINT txbroilereviction_detail_shed_id_fkey;
       public       postgres    false    301    279    3235            n           2606    17349 H   txbroilereviction_detail txbroilereviction_detail_slaughterhouse_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroilereviction_detail
    ADD CONSTRAINT txbroilereviction_detail_slaughterhouse_id_fkey FOREIGN KEY (slaughterhouse_id) REFERENCES public.osslaughterhouse(slaughterhouse_id) ON UPDATE CASCADE ON DELETE CASCADE;
 r   ALTER TABLE ONLY public.txbroilereviction_detail DROP CONSTRAINT txbroilereviction_detail_slaughterhouse_id_fkey;
       public       postgres    false    301    284    3251            h           2606    17354 7   txbroilereviction txbroilereviction_partnership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroilereviction
    ADD CONSTRAINT txbroilereviction_partnership_id_fkey FOREIGN KEY (partnership_id) REFERENCES public.ospartnership(partnership_id) ON UPDATE CASCADE ON DELETE CASCADE;
 a   ALTER TABLE ONLY public.txbroilereviction DROP CONSTRAINT txbroilereviction_partnership_id_fkey;
       public       postgres    false    300    277    3229            i           2606    17359 4   txbroilereviction txbroilereviction_scenario_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroilereviction
    ADD CONSTRAINT txbroilereviction_scenario_id_fkey FOREIGN KEY (scenario_id) REFERENCES public.mdscenario(scenario_id) ON UPDATE CASCADE ON DELETE CASCADE;
 ^   ALTER TABLE ONLY public.txbroilereviction DROP CONSTRAINT txbroilereviction_scenario_id_fkey;
       public       postgres    false    300    264    3186            o           2606    17364 C   txbroilerproduct_detail txbroilerproduct_detail_broiler_detail_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroilerproduct_detail
    ADD CONSTRAINT txbroilerproduct_detail_broiler_detail_fkey FOREIGN KEY (broiler_detail) REFERENCES public.txbroiler_detail(broiler_detail_id) ON UPDATE CASCADE ON DELETE CASCADE;
 m   ALTER TABLE ONLY public.txbroilerproduct_detail DROP CONSTRAINT txbroilerproduct_detail_broiler_detail_fkey;
       public       postgres    false    302    299    3272            p           2606    17369 .   txbroodermachine txbroodermachine_farm_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroodermachine
    ADD CONSTRAINT txbroodermachine_farm_id_fkey FOREIGN KEY (farm_id) REFERENCES public.osfarm(farm_id) ON UPDATE CASCADE ON DELETE CASCADE;
 X   ALTER TABLE ONLY public.txbroodermachine DROP CONSTRAINT txbroodermachine_farm_id_fkey;
       public       postgres    false    303    273    3215            q           2606    17374 5   txbroodermachine txbroodermachine_partnership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroodermachine
    ADD CONSTRAINT txbroodermachine_partnership_id_fkey FOREIGN KEY (partnership_id) REFERENCES public.ospartnership(partnership_id) ON UPDATE CASCADE ON DELETE CASCADE;
 _   ALTER TABLE ONLY public.txbroodermachine DROP CONSTRAINT txbroodermachine_partnership_id_fkey;
       public       postgres    false    303    277    3229            r           2606    17379 ,   txcalendarday txcalendarday_calendar_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txcalendarday
    ADD CONSTRAINT txcalendarday_calendar_id_fkey FOREIGN KEY (calendar_id) REFERENCES public.txcalendar(calendar_id) ON UPDATE CASCADE ON DELETE CASCADE;
 V   ALTER TABLE ONLY public.txcalendarday DROP CONSTRAINT txcalendarday_calendar_id_fkey;
       public       postgres    false    305    3294    304            s           2606    17384 -   txeggs_planning txeggs_planning_breed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txeggs_planning
    ADD CONSTRAINT txeggs_planning_breed_id_fkey FOREIGN KEY (breed_id) REFERENCES public.mdbreed(breed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 W   ALTER TABLE ONLY public.txeggs_planning DROP CONSTRAINT txeggs_planning_breed_id_fkey;
       public       postgres    false    250    306    3157            t           2606    17389 0   txeggs_planning txeggs_planning_scenario_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txeggs_planning
    ADD CONSTRAINT txeggs_planning_scenario_id_fkey FOREIGN KEY (scenario_id) REFERENCES public.mdscenario(scenario_id);
 Z   ALTER TABLE ONLY public.txeggs_planning DROP CONSTRAINT txeggs_planning_scenario_id_fkey;
       public       postgres    false    3186    264    306            u           2606    17394 -   txeggs_required txeggs_required_breed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txeggs_required
    ADD CONSTRAINT txeggs_required_breed_id_fkey FOREIGN KEY (breed_id) REFERENCES public.mdbreed(breed_id);
 W   ALTER TABLE ONLY public.txeggs_required DROP CONSTRAINT txeggs_required_breed_id_fkey;
       public       postgres    false    307    3157    250            v           2606    17399 0   txeggs_required txeggs_required_scenario_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txeggs_required
    ADD CONSTRAINT txeggs_required_scenario_id_fkey FOREIGN KEY (scenario_id) REFERENCES public.mdscenario(scenario_id);
 Z   ALTER TABLE ONLY public.txeggs_required DROP CONSTRAINT txeggs_required_scenario_id_fkey;
       public       postgres    false    307    3186    264            w           2606    17404 +   txeggs_storage txeggs_storage_breed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txeggs_storage
    ADD CONSTRAINT txeggs_storage_breed_id_fkey FOREIGN KEY (breed_id) REFERENCES public.mdbreed(breed_id);
 U   ALTER TABLE ONLY public.txeggs_storage DROP CONSTRAINT txeggs_storage_breed_id_fkey;
       public       postgres    false    308    250    3157            x           2606    17409 5   txeggs_storage txeggs_storage_incubator_plant_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txeggs_storage
    ADD CONSTRAINT txeggs_storage_incubator_plant_id_fkey FOREIGN KEY (incubator_plant_id) REFERENCES public.osincubatorplant(incubator_plant_id);
 _   ALTER TABLE ONLY public.txeggs_storage DROP CONSTRAINT txeggs_storage_incubator_plant_id_fkey;
       public       postgres    false    308    275    3225            y           2606    17414 .   txeggs_storage txeggs_storage_scenario_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txeggs_storage
    ADD CONSTRAINT txeggs_storage_scenario_id_fkey FOREIGN KEY (scenario_id) REFERENCES public.mdscenario(scenario_id);
 X   ALTER TABLE ONLY public.txeggs_storage DROP CONSTRAINT txeggs_storage_scenario_id_fkey;
       public       postgres    false    3186    264    308            z           2606    17419 '   txgoals_erp txgoals_erp_product_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txgoals_erp
    ADD CONSTRAINT txgoals_erp_product_id_fkey FOREIGN KEY (product_id) REFERENCES public.mdproduct(product_id);
 Q   ALTER TABLE ONLY public.txgoals_erp DROP CONSTRAINT txgoals_erp_product_id_fkey;
       public       postgres    false    3177    260    309            {           2606    17424 (   txgoals_erp txgoals_erp_scenario_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txgoals_erp
    ADD CONSTRAINT txgoals_erp_scenario_id_fkey FOREIGN KEY (scenario_id) REFERENCES public.mdscenario(scenario_id);
 R   ALTER TABLE ONLY public.txgoals_erp DROP CONSTRAINT txgoals_erp_scenario_id_fkey;
       public       postgres    false    309    264    3186            |           2606    17429 '   txhousingway txhousingway_breed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txhousingway
    ADD CONSTRAINT txhousingway_breed_id_fkey FOREIGN KEY (breed_id) REFERENCES public.mdbreed(breed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 Q   ALTER TABLE ONLY public.txhousingway DROP CONSTRAINT txhousingway_breed_id_fkey;
       public       postgres    false    3157    250    311            �           2606    17434 4   txhousingway_detail txhousingway_detail_farm_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txhousingway_detail
    ADD CONSTRAINT txhousingway_detail_farm_id_fkey FOREIGN KEY (farm_id) REFERENCES public.osfarm(farm_id) ON UPDATE CASCADE ON DELETE CASCADE;
 ^   ALTER TABLE ONLY public.txhousingway_detail DROP CONSTRAINT txhousingway_detail_farm_id_fkey;
       public       postgres    false    312    273    3215            �           2606    17439 ;   txhousingway_detail txhousingway_detail_housing_way_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txhousingway_detail
    ADD CONSTRAINT txhousingway_detail_housing_way_id_fkey FOREIGN KEY (housing_way_id) REFERENCES public.txhousingway(housing_way_id) ON UPDATE CASCADE ON DELETE CASCADE;
 e   ALTER TABLE ONLY public.txhousingway_detail DROP CONSTRAINT txhousingway_detail_housing_way_id_fkey;
       public       postgres    false    3322    312    311            �           2606    17444 4   txhousingway_detail txhousingway_detail_shed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txhousingway_detail
    ADD CONSTRAINT txhousingway_detail_shed_id_fkey FOREIGN KEY (shed_id) REFERENCES public.osshed(shed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 ^   ALTER TABLE ONLY public.txhousingway_detail DROP CONSTRAINT txhousingway_detail_shed_id_fkey;
       public       postgres    false    3235    312    279            }           2606    17449 -   txhousingway txhousingway_partnership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txhousingway
    ADD CONSTRAINT txhousingway_partnership_id_fkey FOREIGN KEY (partnership_id) REFERENCES public.ospartnership(partnership_id) ON UPDATE CASCADE ON DELETE CASCADE;
 W   ALTER TABLE ONLY public.txhousingway DROP CONSTRAINT txhousingway_partnership_id_fkey;
       public       postgres    false    311    3229    277            ~           2606    17454 *   txhousingway txhousingway_scenario_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txhousingway
    ADD CONSTRAINT txhousingway_scenario_id_fkey FOREIGN KEY (scenario_id) REFERENCES public.mdscenario(scenario_id) ON UPDATE CASCADE ON DELETE CASCADE;
 T   ALTER TABLE ONLY public.txhousingway DROP CONSTRAINT txhousingway_scenario_id_fkey;
       public       postgres    false    311    3186    264                       2606    17459 '   txhousingway txhousingway_stage_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txhousingway
    ADD CONSTRAINT txhousingway_stage_id_fkey FOREIGN KEY (stage_id) REFERENCES public.mdstage(stage_id);
 Q   ALTER TABLE ONLY public.txhousingway DROP CONSTRAINT txhousingway_stage_id_fkey;
       public       postgres    false    311    3192    268            �           2606    17464    txlot txlot_breed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txlot
    ADD CONSTRAINT txlot_breed_id_fkey FOREIGN KEY (breed_id) REFERENCES public.mdbreed(breed_id);
 C   ALTER TABLE ONLY public.txlot DROP CONSTRAINT txlot_breed_id_fkey;
       public       postgres    false    313    3157    250            �           2606    17469    txlot txlot_farm_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txlot
    ADD CONSTRAINT txlot_farm_id_fkey FOREIGN KEY (farm_id) REFERENCES public.osfarm(farm_id) ON UPDATE CASCADE ON DELETE CASCADE;
 B   ALTER TABLE ONLY public.txlot DROP CONSTRAINT txlot_farm_id_fkey;
       public       postgres    false    3215    273    313            �           2606    17474    txlot txlot_housing_way_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txlot
    ADD CONSTRAINT txlot_housing_way_id_fkey FOREIGN KEY (housing_way_id) REFERENCES public.txhousingway(housing_way_id) ON UPDATE CASCADE ON DELETE CASCADE;
 I   ALTER TABLE ONLY public.txlot DROP CONSTRAINT txlot_housing_way_id_fkey;
       public       postgres    false    311    3322    313            �           2606    17479    txlot txlot_product_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txlot
    ADD CONSTRAINT txlot_product_id_fkey FOREIGN KEY (product_id) REFERENCES public.mdproduct(product_id);
 E   ALTER TABLE ONLY public.txlot DROP CONSTRAINT txlot_product_id_fkey;
       public       postgres    false    260    313    3177            �           2606    17484    txlot txlot_shed_id_fkey    FK CONSTRAINT     }   ALTER TABLE ONLY public.txlot
    ADD CONSTRAINT txlot_shed_id_fkey FOREIGN KEY (shed_id) REFERENCES public.osshed(shed_id);
 B   ALTER TABLE ONLY public.txlot DROP CONSTRAINT txlot_shed_id_fkey;
       public       postgres    false    3235    313    279            �           2606    17489 +   txposturecurve txposturecurve_breed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txposturecurve
    ADD CONSTRAINT txposturecurve_breed_id_fkey FOREIGN KEY (breed_id) REFERENCES public.mdbreed(breed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 U   ALTER TABLE ONLY public.txposturecurve DROP CONSTRAINT txposturecurve_breed_id_fkey;
       public       postgres    false    3157    315    250            �           2606    17494 1   txprogrammed_eggs txprogrammed_eggs_breed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txprogrammed_eggs
    ADD CONSTRAINT txprogrammed_eggs_breed_id_fkey FOREIGN KEY (breed_id) REFERENCES public.mdbreed(breed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 [   ALTER TABLE ONLY public.txprogrammed_eggs DROP CONSTRAINT txprogrammed_eggs_breed_id_fkey;
       public       postgres    false    3157    316    250            �           2606    17499 8   txprogrammed_eggs txprogrammed_eggs_eggs_storage_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txprogrammed_eggs
    ADD CONSTRAINT txprogrammed_eggs_eggs_storage_id_fkey FOREIGN KEY (eggs_storage_id) REFERENCES public.txeggs_storage(eggs_storage_id) ON UPDATE CASCADE ON DELETE CASCADE;
 b   ALTER TABLE ONLY public.txprogrammed_eggs DROP CONSTRAINT txprogrammed_eggs_eggs_storage_id_fkey;
       public       postgres    false    308    3312    316            �           2606    17504 5   txprogrammed_eggs txprogrammed_eggs_incubator_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txprogrammed_eggs
    ADD CONSTRAINT txprogrammed_eggs_incubator_id_fkey FOREIGN KEY (incubator_id) REFERENCES public.osincubator(incubator_id) ON UPDATE CASCADE ON DELETE CASCADE;
 _   ALTER TABLE ONLY public.txprogrammed_eggs DROP CONSTRAINT txprogrammed_eggs_incubator_id_fkey;
       public       postgres    false    316    274    3220            �           2606    17509 3   txscenarioformula txscenarioformula_measure_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioformula
    ADD CONSTRAINT txscenarioformula_measure_id_fkey FOREIGN KEY (measure_id) REFERENCES public.mdmeasure(measure_id) ON UPDATE CASCADE ON DELETE CASCADE;
 ]   ALTER TABLE ONLY public.txscenarioformula DROP CONSTRAINT txscenarioformula_measure_id_fkey;
       public       postgres    false    317    3163    254            �           2606    17514 5   txscenarioformula txscenarioformula_parameter_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioformula
    ADD CONSTRAINT txscenarioformula_parameter_id_fkey FOREIGN KEY (parameter_id) REFERENCES public.mdparameter(parameter_id) ON UPDATE CASCADE ON DELETE CASCADE;
 _   ALTER TABLE ONLY public.txscenarioformula DROP CONSTRAINT txscenarioformula_parameter_id_fkey;
       public       postgres    false    3167    256    317            �           2606    17519 3   txscenarioformula txscenarioformula_process_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioformula
    ADD CONSTRAINT txscenarioformula_process_id_fkey FOREIGN KEY (process_id) REFERENCES public.mdprocess(process_id) ON UPDATE CASCADE;
 ]   ALTER TABLE ONLY public.txscenarioformula DROP CONSTRAINT txscenarioformula_process_id_fkey;
       public       postgres    false    258    317    3173            �           2606    17524 4   txscenarioformula txscenarioformula_scenario_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioformula
    ADD CONSTRAINT txscenarioformula_scenario_id_fkey FOREIGN KEY (scenario_id) REFERENCES public.mdscenario(scenario_id) ON UPDATE CASCADE ON DELETE CASCADE;
 ^   ALTER TABLE ONLY public.txscenarioformula DROP CONSTRAINT txscenarioformula_scenario_id_fkey;
       public       postgres    false    264    317    3186            �           2606    17529 9   txscenarioparameter txscenarioparameter_parameter_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioparameter
    ADD CONSTRAINT txscenarioparameter_parameter_id_fkey FOREIGN KEY (parameter_id) REFERENCES public.mdparameter(parameter_id) ON UPDATE CASCADE ON DELETE CASCADE;
 c   ALTER TABLE ONLY public.txscenarioparameter DROP CONSTRAINT txscenarioparameter_parameter_id_fkey;
       public       postgres    false    256    3167    318            �           2606    17534 7   txscenarioparameter txscenarioparameter_process_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioparameter
    ADD CONSTRAINT txscenarioparameter_process_id_fkey FOREIGN KEY (process_id) REFERENCES public.mdprocess(process_id) ON UPDATE CASCADE ON DELETE CASCADE;
 a   ALTER TABLE ONLY public.txscenarioparameter DROP CONSTRAINT txscenarioparameter_process_id_fkey;
       public       postgres    false    258    3173    318            �           2606    17539 8   txscenarioparameter txscenarioparameter_scenario_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioparameter
    ADD CONSTRAINT txscenarioparameter_scenario_id_fkey FOREIGN KEY (scenario_id) REFERENCES public.mdscenario(scenario_id) ON UPDATE CASCADE ON DELETE CASCADE;
 b   ALTER TABLE ONLY public.txscenarioparameter DROP CONSTRAINT txscenarioparameter_scenario_id_fkey;
       public       postgres    false    318    3186    264            �           2606    17544 ?   txscenarioparameterday txscenarioparameterday_parameter_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioparameterday
    ADD CONSTRAINT txscenarioparameterday_parameter_id_fkey FOREIGN KEY (parameter_id) REFERENCES public.mdparameter(parameter_id);
 i   ALTER TABLE ONLY public.txscenarioparameterday DROP CONSTRAINT txscenarioparameterday_parameter_id_fkey;
       public       postgres    false    256    3167    319            �           2606    17549 >   txscenarioparameterday txscenarioparameterday_scenario_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioparameterday
    ADD CONSTRAINT txscenarioparameterday_scenario_id_fkey FOREIGN KEY (scenario_id) REFERENCES public.mdscenario(scenario_id);
 h   ALTER TABLE ONLY public.txscenarioparameterday DROP CONSTRAINT txscenarioparameterday_scenario_id_fkey;
       public       postgres    false    264    319    3186            �           2606    17554 ;   txscenarioposturecurve txscenarioposturecurve_breed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioposturecurve
    ADD CONSTRAINT txscenarioposturecurve_breed_id_fkey FOREIGN KEY (breed_id) REFERENCES public.mdbreed(breed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 e   ALTER TABLE ONLY public.txscenarioposturecurve DROP CONSTRAINT txscenarioposturecurve_breed_id_fkey;
       public       postgres    false    320    250    3157            �           2606    17559 G   txscenarioposturecurve txscenarioposturecurve_housingway_detail_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioposturecurve
    ADD CONSTRAINT txscenarioposturecurve_housingway_detail_id_fkey FOREIGN KEY (housingway_detail_id) REFERENCES public.txhousingway_detail(housingway_detail_id) ON UPDATE CASCADE ON DELETE CASCADE;
 q   ALTER TABLE ONLY public.txscenarioposturecurve DROP CONSTRAINT txscenarioposturecurve_housingway_detail_id_fkey;
       public       postgres    false    320    3328    312            �           2606    17564 >   txscenarioposturecurve txscenarioposturecurve_scenario_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioposturecurve
    ADD CONSTRAINT txscenarioposturecurve_scenario_id_fkey FOREIGN KEY (scenario_id) REFERENCES public.mdscenario(scenario_id) ON UPDATE CASCADE ON DELETE CASCADE;
 h   ALTER TABLE ONLY public.txscenarioposturecurve DROP CONSTRAINT txscenarioposturecurve_scenario_id_fkey;
       public       postgres    false    320    3186    264            �           2606    17569 3   txscenarioprocess txscenarioprocess_process_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioprocess
    ADD CONSTRAINT txscenarioprocess_process_id_fkey FOREIGN KEY (process_id) REFERENCES public.mdprocess(process_id) ON UPDATE CASCADE ON DELETE CASCADE;
 ]   ALTER TABLE ONLY public.txscenarioprocess DROP CONSTRAINT txscenarioprocess_process_id_fkey;
       public       postgres    false    3173    321    258            �           2606    17574 4   txscenarioprocess txscenarioprocess_scenario_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioprocess
    ADD CONSTRAINT txscenarioprocess_scenario_id_fkey FOREIGN KEY (scenario_id) REFERENCES public.mdscenario(scenario_id) ON UPDATE CASCADE ON DELETE CASCADE;
 ^   ALTER TABLE ONLY public.txscenarioprocess DROP CONSTRAINT txscenarioprocess_scenario_id_fkey;
       public       postgres    false    264    3186    321                  x������ � �            x������ � �            x������ � �            x������ � �            x������ � �          4   x�3�4�TUHIU((�/I��K�2�4��NN��2�4�H%&�f��r��qqq '[�      "      x������ � �      $      x������ � �      &      x������ � �      '   "   x�3�N�M�K�R�\��.��&��b�=... ��
�      G   �   x�}��j�@�gݻ��i��kC��<@�8��tw���ƄL>}�%5�ċ��/D�3��0�JhV�!�[봨�մ7�<bh֎
��ZYj1�th�`4)+(/��S�������ˈ:���P�k;_ÀRHx`��/�#�Z�Ž���Qs�F�j#͇l��,c�X9��x,�����B�d-
�4���#?���#��f��k�Ɖ�      I   K   x���	 1��d��mmw���X�'� ������NL��l��ƶ��E�r��zP�g�I����w�{H�*�r      J      x�3��200�t�wr�����  �'      K   +   x�32�����WHIUH�K�/JI�41�4�33@�\1z\\\ 0
�      L   9   x�3�J-(�O)MN�<�9�ˈ�5/=�(%�˘�'�,1�$U�R�����D�=... �6>      N   :   x�3����LIL���440�4�0����2���/rIMKM.�ʹpa	W� `�_      P      x�M�1�0�99�O���*�+�ob���'��IE]<|?w��j�J 3E�� O�!k4��#��"�qp��w��,�Z��.�,U-R�B�0��jMEKdk?*[`Ӳ�O�����S79�      R   �   x�}PA�0<OC�PK�l<h�ų�
hHL1��O�cn����a��t�f�@�C�L���F�u[���РT@{/�kqwͣ
�ǻlE��ԉ�xT��խmʾ(��+�	���<+��E�����4���y38tnCџ��(Iˍ���a��'p�G�9c9�o¥iK��	/���r�X�      T   s   x�U�K�0��)|�����˔��bd��z������}�y��\s��US�Y�d5ې�h���<r�?O�&Eiܿ�cB��ur�eRUz^НP�Ģ�|�0�j,�7��sC�#�"�      V   n   x�u�1�0F�99Hd��S���D�R�"��%#�o��8��{Y��M�gө@l�,q��Tg�(ڪ$*Q���ᒴX���eE}t�N��11/��H��c�F�+A      X   t   x�347�tvv�s��W
uurT0Tpuqtv�4204�54�50T02�2 !]�����1X�&l��_�����`hd`�i�ih�ehn�PT�
�m���`N�4���� r�#=      Z   S   x�3�t�,.���L�IU(H,JTp���J��L�+��'�eę�\Z�����L83�2�2s2K@��.cβ���k�W� 
�,{      \   N   x�3�4�t�K�/JI�2�4���K.MJL����2�4�J-(�O)M.�/J,�2�4�t.:�6Q�R�'�,1�$�+F��� ۏ�      ^   {   x�3�LL����,.)JL�/�T1JR14R	1�,t�p��J3Kr�ӏ�L��N7I��2-.N5N-NN	.L�
J���,+�t!���Y`�id`h�k D�
��V�V�&�&\1z\\\ �+�      _   �   x�%�A�0���p
N`h��p%�c\�� �&�5�o�<s��~:/U6�A�������e����g�|����PA�&��D�ٍx���Wc�b��ޅIs�B���i@O^�[q�a��Z.�-�b�>m���LI��C��k��Eв_I�p��0m����_���p��F~E_�m�$�?O      `      x������ � �      a   �   x�-�1�0��9EO���Ma����
!��b�D
	
e�6����Ƭ���4�)a}��?���e�{7�BK&�йDCLEW����^�,5��K�$[����0��#{��A�vt}�z�� �y�9� l(��|IԜ�U���5�݆5W9�|��2��C�prv���/!�q�9�      b   j   x�32�42��J�M-.O�T00��b��K3�2�K�S�����p��!Z Ÿ�LP�3�g��<��Y��2C5�h�1qf";���܄+F��� a>=      c   8   x�32�H,J�,��424rr�J<�K�S�9��9�9�9K@�+F��� ���      e   �   x�U�Q� D��{ �`�X�ZI�����Ky&/&�U&�13o�lpUI9ֽh�"�"�f{�F�aȕESD�p�ϻR7�2��P.k%URq0g��{oC0�2���<p�1Ћ�}ܰ0�q�e���F���[^�R.pr�6�k��~�7�      g   �   x�u��� C��.�bRLпN��s\ڪ�5ՉX�/���`+�u������3�"XPQ���zsK��3�"V��(��%�6N��7T���#E��e��I�'��Z�=K��~�Ʈ|��W�Z(Kh({��Z�Y,�OJ��:9�      i      x������ � �      j      x������ � �      l   E   x�36��I�+ITHIUpJ�KM�L��W00��M��L�G�M��V��E�ŉ)@N@Ŧ@����� OU�      n   {   x�u�=�0���9ENl��0�]�2 :nO�R�O���{-�(*I.�Z�6o��7���l����Dy@�h��
�2j@`b��"���p�Z�!Ȗ��l��ʟ��b�D�?$# ���� ��^a      y      x������ � �      z      x������ � �      {      x������ � �      |      x������ � �      }      x������ � �      ~      x������ � �            x������ � �      �   D   x�3��tN�I�KI,����IL�/JL�IEb��%+9�}\�\�<�������H�rr��qqq ȋ�      �      x�|}k��8��o�*z����/lD;,R�p�?�75Qu�*'�`�)��8�b�7�����_�������╾>�����B�Jb(����t�d�R�P���A�Ja(�����r�T�ҾP��(���/���{Pځ���B���(��|~.?�A�d(1|����iab�0�&ˏ�q�q}#��1}���q�#��1�T��q�#�±|�4��q�#�ı~�t��q�#�Ʊ}�ƃs\�H/r�?H~ƃs\�H�r_8Y~ƃs\�H/��?���xp���uN��&��9�s��9�/y 7N�}N�u��p������'z�S�:p���ω��T>����ω��T�p����ω��Ծp����ω��Կp����ω��4�p����ω����@����ω����?��9�s��9��$?�Ɖ�}��>��&ˏx`�uN4JI&J��)W�m��!Ʉ)?0���e���!��)?0�ʸ˱0�o"�@��_W9���I�����S�M��!i�*��\9���A��8���q��8`��C�>�����y��?��ъ�YH��)X�$WN�
�q:�1W��+�_9��+I�w`EѸ���,\I��t\�t\e�$WN�
�r:�2W��+g����,\I�8���ۜ������Y��9י�+=���q��q�Y��4\qVރ��������AW�}��}f�J�p����>������)W�}��}f�J�p���p��q�Y��4\q8�������,\I�8�~U��|�g�$W܁��q��q�Y��4\�?誸����,\�W��Y��9����z��L�ns	�=��_0�q�2�x��˓����u��rw�U�`��p�K>`����k�}���\�CnN�_���\���\������Eq�K;`���G��k\�C^�Y���zX��eX����k��G�{\�{ֳ̾~�;�~u\�z\��u|�������ī�&��&��c�S>��q��q���1é_8�b�.��.��c�Ӿp��q��q���1��_8�i��z���u�p�N�:�s=��z{3����;�s=���:&8)|X����y}3�������\�����N��Yo��v��D�s�_8�����ω��T�p�5p��q��ϩ~�k�>��&z�S�����}n�}N�>��u ^���9�������s7<�t8e����5As���(�����m���B�>E�>E^��ӊ4�bs�E�,��{eq�js�U�B�W��U~/�T���-�UJt�DW	�ο���\�e�J������b,���b+�R]�jw@J�@�1��R퀤n�P]l&X)�U����E3R�bS�&��j;ƬRe�y�HQ�������y�}������nn���$�y���:��h���ܷ�����۰ѷYӷ�; uPm_ӝR�7�gы�ti��!T�Mu���Y�� �=[FuW���DX�}Z FuW���D��j�vJuW���D����U��q���u����>A�*VG�����H�����\�F�h���-<R��@�y�	��jˢ�~�������
�5T�R� �R����]GP����4%s@sgtSǟg�bR�E���� ��6��#?U�j��I��a�; B���������ɕ�6q�";�-_�ߧ*r
�6u������@�}���	��9F�I����G�W�<�ۂ4�D�� t�j�ݒ�=���2� ���u��^��	ђ@��.�����A�mh���;~thk@t���о��d�f��ut������6��5U���td���ΧB_t�d�2��f���գ���U�+CjiF�\ۈ*n��@h�:y�=j��#���M�4�zpԂpt��u	P9m��y�R�̉�{	P�k��4xw�����2�NmC���f�v�uPi;K��c�ގ��H��4���q𦃱�؁u�?��?�����3����A�n�fB�x��A�����B�����+#"����Ǵ8^�[�D�[_� ۬�/c/�(Ey=T� ��6�"0��p��=�Y�z��j����H!{�ǿFƮ�2��9#��q@.$������
RȞ��է��O����cC
�s<�\Lz��'(�!{6\���z�|���䐽d��׶��W���\9d/� ra�Ծ�����j�|�P��+������x �(�z�7�+H"{3\W�u5\��ܯ ��=[�uU��of��C��<��U������#D�G�@��\�"��3@��\�+�C٧ẑr֐w�N_8�#������|���U$�#L���Y��_��7CZo~$�#V��Y�_��7�]��z�x ��Z��u�Ml��G9����>n����5���s>p����|=z�#O�è�J��T�j��]��?��#�5@��T��7�z��!G5TR�׸��ɿ�EE
9Z>pȟٸ�~���U$��M���Y�����*��ѫ�����:�>��1�D^i�,ԯ��qC�$�!���?Ͼ!y38��ϫ�翏�!usZ_κ��~�}C�8C=�ȟ������oHg���/g	P�z��v\y�J��+��~���z�| �P�Zi�L�1��7ʊ4~?�ƙ�D����ω�{�6��9F�|�W=�~o������}�L��׀F܍v�b��ƎT��&��>,�g�فxMH���"Ǿ#G�#u%h����"Ǿ#G&�-ra���Z�"Ǿ#G&Ή��!��Y���ȑ�s�_���b{�Y��w���9Q�&D�r ��r����S!�ĸ�ʵ; Ε�v ��r�Du׶�E����:'A��i,r�o���9Q�@�m�b�cߑ#S��ʃj���"Ǿ#G��I��m�	����|��@�m~`�c#G���r@u��_�?GT>P݋�aTw�کs�
��F9�92u��	 j��+�N��#�ں�X���ȑ�sҢ��u1!tl!ԫwgBT+�	�C�Pw@��Z�@L}#G�����Z�����:'2_��f�f�c#G&έg�N�j;�Y���ȑ�s"�j�9�"Ǿ#G&�I���M�,r�ן��bT۞K9�9���j�H#Ǿ#G$:�ڴ��ȱ�ȑ�s�٦�F�]#G&��w���@"l���o��v��H͞ct�ȑ�s��q�̈́g碞�m�]w7�!��̼�����| ��I��V��i}��Pڌ�1~A�	�Ź��،/~Xգ��9��:$�t 1��8�b�ؗ"�d�m��✨|�Z�@�pu��ʰ�4c����F_'�?��f���,��,<��| ܴ�#<)�N�[� ��mwrn�j��✈|�{w�Kۗ�Ĺu�b�Z8��>U�p�����w������;�d���y`=������q��G�#"���!Bh�ʺS��U�B�,B�V��_�zlH!�l�	�I+�N���CTKB�V֝:�����5s���Bջ�3!����X�v(�b���D��o�<Z98RȞ��M,c�t3uNd>p���=,������=�ia��翩_A�K�8��W�~�8un=|䐽��T؊�C|�*�!{m�)�E�!��|ລ�؊�C�z�H"{3\W�u5\��ܯ ��X�uU��:�>��>�D��ʵS���>�b\W�کs"�� �uU�}��"�4\;��j�}�>�oBU+�)�M���_Y�0EX"�A������U�E��,SB����<':_��� �Vk��v�܂B9��S�����s"��\,�+׾
sU�����pݕkw`=}���4ĸ�ʵ�\�t ��r������F�j�T
F
u
E9Z9p�ڸ�翙_E9z�8L
*�:}N\���7ĤСR�/�, P=�D^jC�Pw`��H �0TO*��-�:��@�,y�sK�N�kWC�8C�8L
�*�:}n�Hgh�I���vw4�3����M�B�>��8��3*վ����嚔aЄ�V ��    _�A����^����G�8s�H�����?뷿�!s�%H�nAjz����>kk6a�c��#��ʅ>�l�&,z�o����|�K([�	�������Y�	��iA�)�ZMX��w�Ȁ�f%�zMX��w��:Q���f<�7xd��j���zMX��w����6�b�&,x�;xd�x�a�^<�<Ro���c�&,x�;xdD���l���Gpm�&,x�;xd
�ă��zMX��w��:J���������)t����{��}�L��\[[1�<Ro����8Mh��w��-��Mh��w�Ȁ�pm烲����It���bɂǾ�G&��K\��,x�o��$�u�B��c�c�G&щ��c1��c��#��,=vT���%��c&4x�;xd@����c�L�౿�#��ֳG�PM����й�P'�-��%T�P<�<2�n}�&�jư����It2" T�y&,x�;x���T�A4x�;xdd�!�6 h��w�Ȁ�od��4x�<2�Nޜ`�'��c���It�}��|<v�D'Q
�6#
b�h�>:	,��Q?�I�D»2&Ռ(������	��3��Q?F��?~֊�a1ifJA����?�B'Z�<�������$�@'a艇U���z�n=:�L��S�$�����@�� \&�*�|F\<�_	�ʰ�43���f/��۝�|�a"�q�s�BZw N�f�&L�:O�t��hҬф)�c|f�"F�f�&L�{1���ѤY�	ӟ���:Q�@�u�0�y�٭T��2r�f�&�:�����x���M�3t쩗hŪH!�5�0c����@'�3��5�0_��s�@'J\=�i�l��`������[�	�6��jL��WA٭фM�{��<=�h�FR�+�q`=|d��Mج��g%1 �@u>~�ocϜ���~�)d�V�B<�">�W�Y�2�n�&�j�E_!N��!���:M
�B��B�>��>2�n�&����
�5������f ����z�H!���T�u5\;7A٭դR��r�����Avk5���\;}n��#���jR)�U�v��}��zM*�*�N��WE
٭��U>n��_׾sU���zM�j���[���4��6}];�~U���xM��A}��u��9���1^���d��wW��xM��B�jƭ���W��8M�^A�jQ�jN��!�1�����n5��W�Î'p��R�: B���	:�+�hчr�����D}R-�i�)��	�B�j�N�[��	�B�j�N�[PH �O0�:Tu�(}���'p��H��np_C�8�'��@��_������	&�B���s�ʎ'�T���:�N��®f�L��N�B�@'J�jv>��Z�T-�	t�wB�8�R-�
nW3n�����q�����}+xd��^4{�^������9N3�@�����H}�ی($�Ljt��;���MX��w���z�U([�	�>2��\h��m¢Ǿ�G&��\b�լۄE�}G�T�[@pZ�	�����g�j�&,z�;zd@�kO8k��z�_-���D4
k7a�c��#��gh*�n¢Ǿ�G&Љ�g�������	t2u��7a�c��#U����=�7zd�˂'pm�&,z�;zd@��k�7a�c��#�D��v�4����@'�+��c�Y��w��:Q�����ˢ��F�L���Xpm�&4z�;z�
���kc6��c��#; _�ڮ�c�c��#�� ��~8=�=2�N^
��n.c�c�G&�ɐY8{�F-=�=2�nA�P���X��w��:����[�X��w�H%��`�1.=�=�"���c׷�豿�#�Y��Bծa�c�G��-��'T��=�=2�N�>Pmw1����)t�������Ǿ�G�Љ���{=�=R	o�pmf������9l �(��c��!���͈=v��B'/a�m���k��:�����S#WD�QD���I5s
��ν��+t��o3� ~����Dû2\&�*�������ʰ�43� ~��ӽzI�>�z̬��7�Ž��+t��0�43� �Ebqo�
�bF�ffD��*�+�Љ���q�ZR�Zt�a5if\A�{��."ޕa6i���/ػe���e��^&@�T�q��դY�	ӟ�^���9�@���0�y\����v�jҬՄ��c��d��}��zM��<�ND�ϭ���Y�	s��wc�ﮂ�Y�	3���M��\[�	󆎽���d�Vf{���D胯'��f��n�b��zj� �����E��[�����G٭ӄm�{���D胫�:Mت��W�P�*H �u��$c� aD���jR�@�r}�s��#���iR�<�r�Y͙���n�&�J�E� N��T[�I�R�i�sc;�?vk4)T
-�q��|��:M\����j_��
��n�&N4N��|=z���M*��*�Hd>Pm�&�R}�nr#��n�&�R]�j�Ή���A5S�����:���?k5iT	m�e��9�pS�u���p�񪹩�H�u�8��_�U�>�4������4Y��T	u��z���q�H����ԫ����q��+�W-�W�)���06�^�͵S���G�8�|W� ���`���q�N4P_d$��
UB�QB��_�>;�`P%t���9�����O0�:T	u��z�H��O0�:T	u��|��(T	��:u._��
\���[	����!uv>�̀����`���	&UB�*�h�"q�v>��J�T%��s����fL��NUB�<�bG��(�bw�=���ѐ7N3�@jܬլ���5��L(�U���f57��!o�fD���^���N���͐9�݁/�i]�w5�u������#��څN�l�&,|�o���z�O([�	����<'�/�V�N=��~��#���u������w��	!�ZMX����}.]�p�X�	��=2�℩�:MX��w��Թ�6�b�&,z������mB�:MX��w��ԹEڄJ:�f�sU�٩s�\[�	��=R�n�kk5a�c��#; {@������������NW=�=2uN$@pm�����葩s�ۂk��E����:'Z;��[0Y��w���9�\�=�,z�;z�����k�	�E�}G���Spm7a�����Mpm7a�豿�#S�d�)L=v&����:'�j�鱛0Y��w���9��`鱫0Y��w���9�(KO<��L	��z�n=PXz�*L=�7zd��� \�M�,z�;zd@�;t	U�
�E�}G�L��\�]�,z�;zd��z���v&����<���^R�.L=�=2yN���l��F�]�G*�_l�4z�=���n3��F�]�G�$�=�6S
h��5zd����A�S#�C��C���I5c
���́_KG�Ǥ�1�o���C��!J=fN�H]|���C��we�L��T�E��ԿN,a4ifTAd��G���v�Ѥ�Q��ֺ�́�+��h�̨��vk=����~�9Q����U�r��k�l�s�a5ifZAd۵$�Ò��I�f�H����~wexMZ=�.&d�-d����դY�	S�oY�|����Մ���?L�jэ�դY�	Ӟo ]c���Nd��T3)4]���z�H!���0o��Y�{��A6�4a����1f��g_�A6�4a����@�
2�f�&�z�s�h%�H!���d��/_�/K�+:R�n�&���˗����C٭�$����N���k4ɴ:k������G٭��U=^ u���d�N'��@�J_��g��[�I���~�8���@v�3)T	-�q��|���L
UB��@�<�>�n}&�*�E_ N�[�!������P���	d�>W� �Z�������[��̀��vD���fR)�U�v@������TJuU��<׮���[�I�Jh�>չ����qX�I�Jh�ԩ���W����LUB����:'2=�f�/з|wU��#N{�)�M���@�*��a�`vJu    �T;���>��S���F5��Wd��.���T;qNT>Pm�`vJ�����Fd���&��T;qn=|d��T3ѹ����/WE�8�p���@�j_��g��q���
�C�P$*���	B�
�N�[O�㰳	B�
�N�[��a�*�B�8�~'$��'�T�[u�\���ag��p���/WC�8�l'��@��@���i�L*�NBP���i�L*�NB�8׮��q��R��N55���}I�4�	�P��ά����hH�O Hܨը��5$���'�%�)�3�M�kH�I�w���
 m��i¢��F�L�������౿�#�څ6�l]&,v�;vd�lK�SͺLX��w���9Q�`
�.;�;2qN6Z��cm&,v�o�HŻ���ǺLX���ؑ}."=�d�bǾcG$��0�X�	����8'Q=�d�bǾcG&��o��˄Ŏ���6�~	t	�2a�ccG��I>	��ɄŎ}ǎT��"���5��ر_�"�jc2��c߱#��6&;�7vdҜ���.�d�ccG&͉���L;�;2iNX�v&��ՏN�G�P�{0Y��w�H������Lr�*9�RG�v&��;2����k0Y��w�Ȥ9��
3�݃�bǾcG&͉�/�݃�bǾcG&ͭ�#T�L;�;2iN����c�`�ر��#����k0Y��w���\�=�,v�;vd@rU��]��bǾcG&ͭ����ja�ر�ؑIs�<8K�Y�IcǾcG&͉���"L;v��4��Ѱ�T3��Ǝ]cG*�-$�m��رk��N��&�m��رk�ȐD��f@A���BtR-�pɰ�T3�@��O-�O-��92�%�L(���Ԣ��"��'�@���Q�Ԩ�#p�+�b��b�Hv==H��U���I3S
�b��:��"���c�D�Y���Һp�43� ��Z��~�9��@��S�n�GR3����FM��T�r�I��ɍn�դY�I�2�j�N��|[�	Ӟ�[��2�&�M��|�s�뀨| �M��|kj�h=8M�u�0���5v���D٬Մ�Bo�Iq~7f��mR�AӮ�;qn} �l�h�\���V���/� �5�0S��W�N�,� �5�0O�Ծ�71R�n�&����W����k4�t?_�ns�ε� ���h�i�s�ns�Ή�S�5�d�����ܩs�?���M2�~��m�ԹEr�n�&��q��x���N��M�h���׳G
٭ϤP!�������ϤP!��ĩs��#���gR�Z��Թ��Cvk4���j�vF���[�I�T��|�������L\�8�j_��
R�nm&N3P�_�U�Bvk3���T;�~U��}B�~Y;uNT>Xz�ͤQ!�헵S��k�.�F�Ц/k�Ή�G�u�4*�6}Y;u._�k0]��R[���_�A�өf7P�����j��S�����G�r�%��Rݕj�Ή����N��J�S���G9�d�N��J�S��aG*���S��T�������`���q��N2{��u@T>Pm*������G�8�`�A�СB���Ր:�qP̈́СB��D��v2��B�ԧ�z��aL*�NB�8'*|S�@ԧ��P_����i'8�@[u�Ր5N;�`R!t��D�M͌&�����K͍�kH�M E�R��Rss���ifw��t{qn�VH��N En|���ܺH�I}��E��t����iZ����9~!����R�� 7R܏��G�z��饊�t��?�~���i���t��GZ��TӴSŏ�a��*YU�4Tŏ�b�BZX訚��*�.q�p�}���.�p�Q���C\|1'_-UӴT�� /���u��4U�`��"��u��?���U-�~ 1��՟��v_ռK(�42�q���2Tڪ�}Pq�N�yD���A�?mU9춪�UFn��~7���w_�}�ﴘ��>=�@�W���&[ s��+�.�kV��ʐ�����.��qO^o�����u��W!o������E䋐�zk��LBZ�� ׭@��z�����L^$2x������Opݧ��߹���m@\�z ���h|���zF�q�he+\�n����Z'YU�2̧�*�`�f+Z绢�����h<}U9�iq��4�����h<}U9���%���5���o���ȗ��0��5�jѾ�X�f�j�s��s�����Y}�f��7��P���`V>�M�F��S������fuv`Q���/�~ �dk^�����n��5�s>ٚ۬>I�͢���׃jF�mV��.��� ����̬>_����o�5����wQ�w�[��׽@�]��]�gM_c��-{5}��\�i�ػ���IC���kfV��Y}�ΛvM�j)D�ø�ʵ+�k"TK�@��\w�iMDjɾԘY}n��$�`�5�RK�����s��'�5}M�Ԓ}������>_�d#5fV�۬΀��G��l����s��'�YO�Z��3��mV��4$SS��՘Y}n��s෗~"TK6Tcf����'Y�>"�d#5fV�۬>٬�k"RK6Rcf������z��Ԓ�ԘY}n�:ZO�Z��3��mV���F�d#VK&V�n����Ԇ���Z2���O��??�w�"\K&\�~��~�Iv��2ʳ	بa}�a}���2ͧ jP���Xg'�?Q[��ufY�۲ΐ���i��9��elhxԙ�����/�n�|��T��s��D��@z���Lu�ORڑ�PAz�ˑ�'�jX�
�C:���[�l� ��$Fz�>�E� �v�DIOJ���� 7�4�-]$<�"aV	\o���t��؋�Y%p��m��m�"ᡋ�i��jn�n�"�q���	�)�`j�[�Hx�"a��"*���|�Y��,��RR�F�����^$�
��w�x;�xK	]$�
����v��.�����@))v��s�o5��R��w!����.u�RR�(���d��yESx���W���;�"�	�B`����.�v �RP��������;�"�	�B� dow�E�s/f�@�(��b�f���H�
���u�]����z�\�v �/������"�n���k�����ຍ�|��K�e�w�A����"��.fu���'���:G�:����;��� ��KgdT�dϱ:G�:�;�A���9��9�'�w�k�	n���X�]�ps���ܻ�+���5	7�x�B,d.���B�5�R����X�\4Gq�x�E�R+4\.��8 )(v���o�|q��ű:`�F�/�ہC~!��XP��}J�8��en_-^��[@�����}q�qk9���}qhQ��𛡚����ce�u��n��!Չ�_E�(�DPݓ�a����"W\�~��> �*:~cg�T�bϱWQ�W��^3��i�f�������+���z��P�7�G��Żdq�]�vU�vM�i)��P}�	�:aZ��4招�Ǫ��x��tP���]���
���WZ��4招�/�V	��7Z�a����c��G��l��|qs������Ә/nn_����8-�8�����ű*�z��Ӓ��&���8�*�T��Ә/nn_����0-�0�������2�5�%�1_�ܾ8v`=}j�j�7�/�I� �Z2�5��m�cU����M�F�qS�q�
(U$k�k�7�Ǫ�wAq�tW,�|{�XP�H�ز�ب9n�9��	�veXw��*��W�'v��ҁ�ҵ��W%W��[P�B�F��ce����s�P�Ub�XP�B�%P,C�Z{u����:oM��7ŗ�"X��@b�kw
)J���a�(�ڟ�O�%�w,������u���� FM3���+{]!���F�v��x������jF4�0"��p�BZ)��M3���+�����W�8�i��u�c�+d@+8�8�i��u�c�+du�Pc�4��¡�
Yp�RH4�@"��p�BV��"���D    "��p�BV�W������9/-s�:�ա����u��]W�>_A�	F���
�^WȀ�� � C����I�RN����B�ν��U���%�+�{]!����%酮+��BV\��K0�]W8��BZ&�:t�`t��p�u�쀔�u��U\��h=���ᚭ+��BV\��^�/˗�޹��}��-�rd�r�*�J����*ǻ��U����Q-dU��U_&�F��P����w��|}�%4���Ӎ����_8�	�N1Z�z}��"`�FF�S<~!1MQ\pQ��ֻ�7b�r��W	��wZ��.�q9_�%���k��h����:.Y�Nj����.�q9����C�v��ۅ9.�v\���5��m�s\���d�mD�K��.�q9_�%�Y��.�v�0��|��v'5@Pm�]��rn�%�ݭg�f�h�]��rn�%�ݕk��%ھd渜�q�jwy�jۗ���u\���5���Q	cT�k|}.5@�ٮd渜�qɀ�5[%ۖ��s;.Y��]�U�o"渜�q�jw�oo�d�D�q9_�%+ݭg�Q��s\��q�JwR�6�b�˹���wM�V��V�q9��ǁ��[%[1��܎K�>��T�Y�th��Jw2�\��j�2鼴]�����������rn�%+ݭ���*�؊9.�v\���z������rn�%��]�U��s\���d$�Gx�LxE-�s[.�]��h�cto�%+�I�V25]N5]�ҝT~dedQ��T�%+�I�qV6q�]N�]�ҝ�~i�hX��ŷ`Jj{��^��-u���̟��P�+g��O��2%w��l<��Q��2_��� �D6����)SS�/��]�q"�D��$qw��Z�]��9�GN�H�uW������G�*��r�z[��L3\��r�����mR�C���.@;9�vr�z���0_`����sh#'��I�UI3`��q����a��G������u`��00̈́��9v'���6h��&NVo[�;fL3c���~��ۤr�f�P$�T���0�v !]n�0��dWo�W����A��w���-4����{�;��?��`��~h��CH
w���t����X�m=~��T��f�>�m�W����A��w�+�I�T����{�+��gy#y�����������V�9V�L���Pݏ&7��������ս8�y�|�j�T�dqXa"ka�V�@��&�&\�M�v�z��dLY�ܖ�����2�-ӿ����ځC����R�CSQL�e�E_ he�ME&����U���.1`41 ��<�~gVn[�jD�1���v�Ҽ��(u2�P��9��Yn�G���T��p��vd��e2VڬZ�t���5��X��$+mV-m����Yn�GI�q]�kWp[Hsc;
êն������؎:3`5�����#ˍ����^Ҧ��h=}d�q%I�Kڴ��U�d�;�GM���6�%u������<*e�뾹v��rM$�q�2�uW�]�m}% �M�(�1��r�*n+>Er��Q�d\w��X�R���$�+��/ ��Q*c��qiC�ˋ&^j)�2V�����"�M�(���ء_��t�>2�T�B����*n��#�M�(J��ء_���>��T�J���W��|={�H�(����y�/���1 ��Nƾb�~ź��z����Q'c�1��ܭ��9 �5IV������z����Q%cT���r3 㝚�HG������F��ŀI i52F��,_'���a 952F��Z������pT&��.^��/��(�1��6��ݽ�5�x��h��n���;)!a$@NG���}h㇯��M�T�����8O��G1@(��(L��m��'�*@+�� �v�t�#�M�X��XJ�s;Y�N.��l�Rjq�jqdu;�	oW�Y�Lw��}��v�j؇<�>d�|9������4���^��d�������lD���C[/Y�n��a%�4+�i����KV��	���ȴ�r��KZٻ:�"O���]��d'���Zd�v9��!��c/�4{��n���	X�]a�@�m�I� �k�AHlH�v�+�BivfPD��F�3.��# 6 ��0��/�]a���|�܁��~L� ��Ǐ�0ؐ�5��w7�ܭ���0؈����w7��ITۀ���ν��U�ֳGDlD�:@��M�*w��#$6$d�s�&����mH�v�w7�|={D��F�l7�ܻ	�� ����v̽�����5;��&�{7+��k v2�M0�nV�+	��v�w7+��k ��K��\�sgF�Qd�rQ!�U�dlj��2U����*n�_!Z+W��z��Dg������B���
e�)��urUz��FW([������RP2en�\�:�ܨ�H���i��C�V._w(}�'W�N�Foc�>�tʁjk�j���������Ç�+Z#W����g�N&�-Pm���~��:��ʁj�w�o�nj7�����ލ�����Fx�B�8��T�+<������)�]�v@R݂eξ�:��+ծL&�rp��WѠ�a�	]��ě(Y9s�r¸�
~_�jf�j�ń���+��g53Y5�M��aj��O�	93Y9�W� ԾH���z�e�y}6��g93Y9s�f�y�����P3�U3'm&����2�z�P3�U3'U��87{rf�r�ۤ��D�2�tʁj�g�A�Od6׾N�d ���
��u�n��Fє�w���%h��h��]����>� kf#kJ��{ow�ƕ��^9�A:���� X�L�[�6s4��H���@�e'�h*�NI�1I�N�d`�L��.`'��X7�@�F����+��<�`�J V(���� �L�;��	�
ew�X7#A�R��KX�L��;�ͬ&���c�:��`7�4�	i���VG�$%7t��儴�qh�#+���a;��G��ot���
e���df=!�s���
eRrC?�YPH��nsd���scC�4
i���.GZJ[H ܬ(${^��ub��xs�p��Q�d�Zo̒���f=H�k��{3؎�#�)Lf��K3��f���k�*eRqC�yg��&q�R�n ޘ!eԂ�I�/�]��`�'}�5�s�����$>�s��8���U��$>�s����2���j;;�O����R�~%�N
vv�ݼ\fB�3�bxR�ÓX����{n�i���`�'��7P�M<��vuO
vx�������u@Jn��Oʴ�'k�Z�w ����cM<Y�x\�L�9�k;=)�&��M<�R&%7$��((�:P1u �X20=)��I�ց��B\�LJn(N��e_���Or+/�.��]Th�U4�r��EQy'���_������(%.fy�W��<^	j,G��^�����a@٥.V��Z�t57�r�-�G5��%��%]�mq�5֣�ȸ�ʵ��I��nG��5��q��u���~���;��;�w�]j�G���C��C��r��&�wH�w���D��QNd�Wt�.7p=�"�o�]ѭ^!V
G��qݕkWu�=����Ÿ�ʵ+�I���('2��r��׮�+ţ�ȸ�ʵ; ]n��������4:�⥖�Q�b���kWv�=�:E.V�򹲛����r�Y�~h���n��#�J�((���Аϕˤ�\ף�Ŋ��2U.��a�vT�X1y^���Pݎ"+&O������~�Y1yj���n��#�J�((�b�Ԑϕݤ�T���Eõ[qB�4��#{!}����{.F��ʴ���\ǀ(+��������uB�/Z9eEF�@�/�����xT�hK`��7�]ǀI�9.��&)_u�W0�����uI�M�Wc�F7�����I�Qʗ��*@����*R��Z�H��@��r��-m�\�r tf#tҹ[s��bH�
�n�N:xk��-Vt����rG����rһ���y����USv����͙͌�`^����Va�Hn�A><o�*%wzn6
ꚜ�dPB    ;<����Kt����ˀ�7��Z�Z�b'䆢�%��^�b�N�"?L�p��t��a�>u_􇶣XM�����Cݗ�����KkJ.�+����t����ˀT#��S�zR纃�ˀf�܏�N5���;!��B��m�h����	n{?n��ʷ��	$W�mM�z�o��$���fw �������}p���. ����@����oh��?��~�W w�c��?��}}.���[�Xn������m����?�h|I������8���r}�Ӌ �n��t8��g�\���$V����A'}<H�Ij��8�S���$�6�F�q\n׼� ������v�ƃT>� �z��z����T���K��=�ݷ��P����%�}<H����F	�q$���\
$���D�T��S!�YcD��a�ؓK���h�Q�(�H�o���M1��Qr�H��Ajz�i�E����U�\���mX�(b��������\n ����h�o�$W U�R�Er�獴���;�:Fi�@�M=��~����f�&��`��|�����Z�4���RO��N���$��h�o?�R���}�������N���p ������$�oQ�(��MVѤ�U4 r�
5���]7߮�XT	�1j��]�o����B�b�X,�*�N���b�$�wU���ꅄ*FM�o��&죡��@��t �?��c&$7 ���r=d�o���z
�������
�{�u��H��_csL(`�j~$YJ��� ���������7�~w�Bn_۰@��|�r�c�^$�w��G�@�Sm N��H�o7FS�"�:�]�ލ��"�ea��p��&����]�1�c��a��(z�p�)�.>��P$��rp�挦��Er�荔��	�ѵ�b�HN��r�B������Ӌu)`?~��6g�m�4,�S�n����nc���[.���O�ܾ�e��`��^w#��ܺ�i��v �����Pp�mK���[U�َ��5�\����w�P(���[+���Q����1�^�z�P��wG�~��Wf �M����9������I�َ�^�4�m)�}3�&�I?HorO 
X7�&�I�}����f7�&3J?P)}A�\e$��$��)��'\�X�nv�n2�t
�UH.K,�7��7�W�Aڬ;s��H8�I8�YPMO�<� ��&�de�j��k�
)g7)'��j|�������3��cD��M@��w�y=,���H��r�s��sF^���:I�K�rξs�ȫ3qWg"�A,� 9g�9g�ՙ��3���[���w�7��ķ:�8�����q �;���\n r�>��ՙ��3I,h�9������:Iߩ��s��,����]�y8O@E�9��8�S�s@�P�t�0 �u� \�t�X,��zy��p�bE�9R�H���FJ���!��H.n��{�"�%@�9�!�اS�}@���X�u�< �����.U�H:G)ȗ������K+��Q�E��;*���rŊ�s�v ��F�z�%�Y�h�o�m$El���@�9�8�����Ar�t�^,�*���-W I��"1����&$��I��@"|W��u�A���MV����#PvYBC�9�8�ȟ\�/p_��������.�$hD�9c�H�߭#I��!眱H������2j9�L�o�w$E����Րs�4 �w�|�NT�H9g.��ݕ���±!�%X$�wW�}�f!!圥H��|�r�rn�$�GR��
$W �v����=�G�Ka|�����������۞29�F�;��h�6��i#Ѹ��*[tm�F�;nHT�N�H�ƞ��% �ZqݒV���V��| i;]�?H�#�+qZ &�&�Ȍ�1x��j��КT�"3�c�ܹ��D�֤B�#�Εl�f�exC�@��[A�%[ �O��uA��k b�\)�@"Ѭ��;��s�V��C˰CK�$� ;�J�)�f��N��K�����,�oh��wK� ���Ԋ�e>w���f�b�[Xhvh����Ą�b;.Xhvh�p�:.ʌ)���X�U�ᛅ��-�-!v� "|��|��$��aZ �wU���'��u�X-�*�N��ۺj��*�N��;�}]5�Yh��20eP��w���k[iu���(�bJkS��!��,�Z�EbJ�Z����0�Z�D�qM�V����4�Z�,�oh��wK������7�N���V�ڦb|w��K��ׁo۝�B˰CKv@� ���c�eء%C� -s�vp��2��e ��-!�� �'7���?0Ҽ�i���:Tiuh�`�B=��+���u$c�e��dJ�P��+�"�% |3�u�����^g�Ti�Fiͮ� c�eK�1�u���?�c�r=����T��	r	0ǲ�h���:Uiu\L�l�[$��NUZ��wK� �*�l�ݣf5=�j��,[��1�n��'��T�Z=���;
%� ���G�87�Q�O.�̛�zf���u/��"X7��l�ˣfU=�v�� �M��ƾ@��
�v���ͤ�l����p���f7�&��.�� �f7�&����vk�r�a��~y�,e������l�Ժ�?˥�	g7	'�"��	�'d��d���ΈlnhzA��M������
�邏�q鑴���r�s��sF޿w�:C������3��L�����E@��w�yy&��S�� ��;猼<wy�)��»!��g�[�aʟ\$����(��2��+P�s�Q-SZ�����s�-SZ�����9��QZ���wk����L��%-��%74�"�aZ �V��(��I I��@"%Q̂�ʟ�GH:G��7��w�E�K@��}0��5D��d̭i�/ ��:G�����;�Jr�u�R$R/�Fqҟ\d��F�Ĥ֢o'��%@�9j?��Q�⤿[D��N�0ߢV��]�X�v�6-#�*��������^�p�$� i��"1«�?��v��$BxU�w�� |��0�[�R�5�D�!�s@䏮]�s�,6��3TĴ֦�p_���.Z$��6}���Ō�s�~ ��\�W�C�U����M&2ߪVV �wސv�4 �w��算�!뜹Z �wW���'W Y�,�"1�����?��:g��+߾�#H���X�[iK_��@�9�<�ȟ��Z����s�j���:Tkuڟ\��On1i�6
;�O�4'ek��Ȣ�Q�i���u�|$Q�(�?�:НT�%�7O��I|
H��{'�u����I�:bx�vNz$Q�МT�#�wNj��n�[Ko�ԾI/��%@wR��]�7�D��UDn-1d��#٩�.��@DwR��]�]��d­%�E�aG���-#�p�a�e��%C�K���bM1,�ot��w�� �zbXtvt��$W.�hO*�â˰���3�$�'�a�e���s�Pړ��İ�2���9�ڊ���5Ű�2��%���aB
�Ɏ^�p�$"����İ�2��2�OD$�'U�a�e��e ��-$d=1,�;�D��K��jM1,�ot��w� �zbXt��20yP�?�m-1,�;�d��?�ZK�.Î.�-$�oc���e��e ��4(Uc���ex��@�[G��C���F���rПT��F�aG���r���v��.Î.�	|�9,�;�dD>���Ccr��r��uD�m�.��2��e �H0�4;t�E�aG�������������������a>�S��I���lU4$�`��@6<
����#;u��������r`�iv�����F�I� 0�] �?$s���d`���@�<�_���$�������"���L3c���Iv��	3,0͌]`�?�;��+fx`����� ���-$�s3y�����W���"���A:    Ӹ��T���D�n�/�����;�����L���@�S��_DQ�l6�l���R��dW�mv�m���슞pYbA��M���<P9|���D؏L���<�V�B�	g7	'[��Nv��g7'(5�E�(��ݤ�l�$��'�Pʂ�����T{����� d��zb��1ያ����tk�aBw�3���H:��İM��t눠�ZbX�&�S�� ����
4qh��w� ܚbX�&����%@�٭'�h�.�0�O�$��zbX�&��	�[O+��]�aD?@�٭)�h�.�0�[H�)�3y���e�n*iE�9�)&���-ک�.8-�"���4�Hj�s�<LH��I��-���YP*��a]1�l���F��`!�pk�q�\zt�����d�úb
��ݢ]�B�;��sXWL�zk�W���n!�[WL�zk�W���� ���> �p�bE�9�/�R��&��r�v�,#�*ᾂ#���@�6�ؿ�B"�"1«�DB@�9�8���1�.Uh�;�4�7R��P���2���s�`�������?)*"H�g�g]�ؐwΘ,�[���}	GT;��8���1�����W2�wJx߄; �H<g�ޕp' 
�H<gn5�E5������s��~�d��H���!�v�B��w%�	������W9I��f�v.Yl�;���d9 }~.W Y紓\�
��1?��b>2)mv�(�Q���Hið o)F$k��ȣ�a�	��5�&�b�1��r�PzPT$�(k��=��BIPA��cx�vP��6�G�Xc��J�tK� �:cx��OzP.����ư�2��e ���(�ɴA;kK� ��G�Xg�/Î/ ��G�Xg�/Î/S	�[k�/Î/��hR*�����ƗH� z���ư�2����I~�)z���ư�2���9��(	=J�ZcX|v|�pF���b�1,�;�|�ޢ�&�b�1,�o|�@(��H���˰�Kv@*ehR����˰�K�$2����ư�2��2�aF��_�_" �%@�R��_�7�D �;�.�j�1,�;�D�;�.�j�1,�;�L"��[o�/Î/ف[K��C�˰�K�$� mJ�xch|��2P� ڔ�q���2��2	P� ڔ�1���2��2	P.|1�n`�e��e �-&�p;��ŗaǗ�i��܁p;��ŗ�/�� �S��_�7�d@�����_�_"�| GL���\�J�N��Dؐ���A%ס��� EH�!�c�;j�ӹ�~XM�����J��R��V�a�iv���� �%Ww@� �0͎_�Tr�O��n1|���J�S%W'�������q������/L3��{��j���dxa���@���K�r`�if �Hi�j��j��d�a���U�վN�E���!�,���� �p CL3C�[���Q��1�$��c�ET��)�� �l&����7�9�V���ݤ�d�������o�p��6�@!S$�&$��$�^�{���Q��pv�p�&Ǎv1A�M@��M�ɖ~ʜ��,�rv�r��S ���-�sv�s�5H �ߘ�DKd��:c����<L �����[cӺ�^CB����5ưM�v��A�5ưM��$� Ig��V���@�@��:�uưM|4L �K���[c+��]�a�H:�5ưM�& �R"��V���@CB��@�uưͭ�}}.��a�1��V�����sXcL&o��7R�]pjrE�9�1&����#-�s�t�4��W��i��sXgL&�3����F���"���&���j�'T��:c�(����@�9�3�P����!�J"�ΘBעo���%@�9�5�l������r	�t�!+�oaK�v~�AE�9z�@��;�O� r����D�vQ�v~�AE�9F�H�𪄻�  �cZ$FxU��d�H:�4���𷰥��_Аt�-\���'"����D�vQ�v~�AC�9c�HLpm�
w���HH:g�57&s���7d�3��r�[ت_��@�9s�@��;$��:g�5�E5������s�d?���eE�c-�uN;��,����RАuN;��l��ն���ɦH:��@V�?�]P$?�9�Ȏ��}��D���֘�D�8��Z�K)[k�G�=�N ��D��B�H4��MN ��9ڔ����>��F�@<ХT�5��Qj� o)^$k��]��DIP���E���D�=���-%�p��=���_@rХT�7�ŗ�/���
�I�XkL�-�Y���w+���ZcX|�|�s-ѤT�5�ŗaǗ������7�ŗaǗ�	����pk�a�ex�K��\4)�a�e��%C��Dn�1,�;�|8'JB�R��_�_>�%�I�Xs�/Î/$�[�ХT�;�ŗ�/��D?@�R��_�_&
�H���˰�Kv@
hR����˰�K�t+�p#Yw�/Î/����K�Z{�/�_��	�hR����˰��@��[I����˰��@�?�hR�v))�/Î/	�ۥ�,�;�d��K�ڥ�,�o|ɀ��K�ڝ�,�;�D���Dn����2��2�O.�1�n%e�e��e �߭$�p���ŗaǗ����cL�CX|��20}Pd;ng0��2�����Dx��_�_2$��`�ivà��P��	���'��0��:Tpu��H��4�]�:�Tp�k21��`�Tp����_�a��0��:Up�
��v�ێ`p������R"��3&\�
�I� �0��`U����A�V�a�if� q��9���v�ff0�&��v/�^ �[ ?L3CDI�n��Z��Z�a�if�@q�cR��[�a�if�W��x��"���9��v��A�I6�Ƕ���� E9@��L������f����B2馈r�☌�Υ��f�fO�f>&CzribA��M�������P��C2	���^�M�;!g7g�森��P���M������� (WIg/��� �)�k���[gS�F�v�7E�٭1�	��od�v~�)r�n�1�<�x�M�svk�a噸�3����:cXy&��C�K���[g+�ķ<��[G����3q�g��'�r�n�1�<wy����­1��g�.�0�O��:c\�$A�R �.�"���D9 ���oF$��t=b�	dI4$����b2����Fa��-$d}1�n{ʺ���r�rk�)t�S�>�?�H9������\��*2�am1�d��F�i�'�"���$�����#��zdrk��C�;��sX_L�|�ͷ��
 ��S)��R��_�P�p���R�����?�H8��HZ)��pP�#T$�î$u m�}�F�?�=���]���z��sؕ����ͨ�~BC�9�F�F�֦/p'�����sڕ����M_�N��eD���J�F�֦/p'�����s�`����Tmu�HH�8�]I�J&�Q��h��	2�i7�:E@1}�;��sڕ��ޕp�tˈ ��_��;�O.2�i0tJxW�'� �U[�V[��w�� ��_Tm��:�O� ��i�/��ɋ�K�HÆt3[[�G��y@�t(ek��ȡ�[�!I-J��b�����c���XK�(k��-����k� �C�XW��J���*"lH��(OqЏ6E�R���?��$��|[[.�\���
�C�XW.�\2 �hP*��iov�����n|[S.�.����A�XS.�.����A�XW.�\"��@�R��\�7�|>��MџT�'��a���-"�o�a�e��?J�p���\�\>�%�?�XS.�\>@��(�?�ZS.�\���r���j=1,�;�D��5Dx��'���:���͢���[#�*���GhO����Lۮ�m;$��O��Â����(rОT�)��a��(��­)��a��(r	ОT�RR\�\�    ��%@{R�KIYpvp�4(H �.%e�ex�K����'U����a���A�]Jʂ˰��@�?y�0�T����a��(�	CL�[IYpvp��'�Rb���������(��S��\�\&
,Hv�.�.�)k�����A�֡R�C�f�f�/*��Z��'��0�N_����i���ɰ�4;|aR�u��ꤿ[C�v�¤R�T��Ir	`�iv�¤R�T��k�"؁p;}��q/��: � F�f�/��}v�I.�0�L_��������'L3���������+L3�D���6�E��X��if�@qscRw��X��if��h�hg|v�yR��if���^�����M�)r��m��A�l�d��dSD4�Gp�N�� �f7�f�{�_(u7&�&�������ɥ�	g7	g����5ﵿ[E��$��o���V�E���d�^�{�R�:!W)g7)g��森��Pr�tvk�a*��j)�j���[W�Hmv��6E�٭)�iܷbW�l���[S���]�aڟ\�ݚbXu&��	|��o���+~}.�p$�ݚbXu&��tk���zbXu&����D=@�٭'�Ug╎n�iE�٭'�Ug��0�O�$�Úb2ݏ�/c!tS+��a=1��G̗���GD�9�%��q ڳ��y�#�qZ$��)�'�$r�a-1�.{ʺ��Ir�s�)t�S�R����
 ��S貧r���O*�H9�u������I�,�r�q%��ߦ&;?��"�����}�;��sXKL�|�+}�@�9�#�R��;���"�v#i�|W��Ir�q���R�����?��q���R�����?��q����LԱ��4d�î$uz���緄Ǘ�Hڨ�����D>B�9�J�F��s.��i��sڕ��J�M��N��	�L��N��o'��@�9�F�N��:�|+AC�9�BRW0y#���,�oN��ԩq/����7����)�]�vHr�o�z�ͤ��Ҷ�� ߜv� J��a\�ؐmN;waP�u���?�H6���0��:Tiu\�Ӷp��N�ћ#��5mנ�n�se��w�@j{���)�@�MQ=\��pM���~t��(@�mפ�n�sH�IxZ�J�-\�>����m�;��õ��Eb"�T�܍��I�OW	��kR�|�H�:�5�i�ZHJ�T^�1���2P�� �&.�xH/�r�}C��M�$���:!���s��%�¹�f��\�
�M'��f!�K/|���X��u����f�P{_��%7��u��?�U�ݜ�{��*t�nz�����|!H�� 릛�,^�k�~��@�u��?VV��
B~�	�MCW�#kb2��*}�u���̚�Ck�Q�X7]���:�Ə�`릥+�5Q���.p�v>M]%������Ĥ��J���}����zb�W�d�O���-�OSW����9�*�T�����4u�����~SȍԾ�D�z��J�M]�A�)�.�$�Q��e0���$Wź��pe�QAx6��j���ʮ�5*/� r�@����
4�]���t����+e��k�H��y[4���|�t ����!���������Md�\��{�@������D<�r&�@r)Gګsiԑ_#���,���^��H)H�� �g:�\�������x��OC8[���չ�s�2�C�ݲ�@^�J{u.C�,�q�Á�¨�W�>\��|�C%�f��,�����u<��=TRJ����^��p�����JJ�p�:7��s)�( <��Jx%cTf�%H�𪄻r	/�"1«��4^ӁD�J��-�F���V�wu��]�0;o� "t�2��dq�݃��W�&R�;0�wo�׎�^��p���{���嚾���|�7[���չH~B ߳@��~���X) ��!X �wW��E���n�f��]�v�:����ӁD��ʷ+	�:s4|�չ�]��|��).����D��:�}.�h��9�k�i��eH�Fƙs�H�v����DFt�@K:��;�]���C_� ��\�lunzW�&Ҧ#'�n�Z r�:���� $���k�i��M���>@��[;�ȟ������j��A_K{u.C�62�ܕo�:7�չ���G�L3�r ��չ�8��/�h�,c|��M�Og��H4�l�<�?�����KH� �|��M�����h�p�IW��w�T��T����0N�!���f1�&]��tu����� E���tun�չ����. �,&٤�s���M�QG���b�M�:7�����o�Â�&ߤ�s�^��Xg!!�,&ᤫs���e'�* �,&㤫s���ePr�r�r�չIW�&2�[�r�brN�:7���D\�r�u��uf�:7��s3�ӑ�����3�չy��}�~;RB�Yvҙ��ܼW�fV�$P����W��:����β���W��:�!�jd�5��������dF�,�A�Yc8��%�{un&&}�j�t��,�/��:7�VIH�t֔,�/��:�A����ΚƁ����^��YG�@x6���f�]m�>�;����`��k%��fI� ��Zځ��ּW�e2EQ.VL����͸�M�J�����ٔ�*mUpz�,B7��],܌o���@�LȦ������7�\("TS����͸�Mv@�6O[W	�������(������A��N�_�x�"��M�͸�M&���4v-�y ��p�Ɂ�&=�]+���1��M&�-(��tv�p3j�IC��������f�p����E�^ҙ��M%7��u��E�͸�M&�M�`ݴw�p3j���@�
���.nF7�(wa�u��E�ͨ�&�oe��/nF7�`("X7M^4܌;�d����i��f�p�A�������f�p�Ɂ"'D�n��h�5�dr�,hx��b�'e�}����z-(���p3��&��K���*qwz%n�nR�P�D�x )(�Z��ExZ�R�H>�L;�dH���w�W��f��&��T��,�Lo���@���z ��#i���r��"1�5i���r	:��"1�5i�����[>�H���x��% ��Y���p��� �WĲؼ�X��� |���I��a�(�F �[$�O��&]���"��"�v��7�2=P���OC8ۤ��M�T//@��{1�ޤ�Ǐ���$R�+�JqH���(ZH�"1��+�遷�8����T튾R�(�<���dg�tӻI��r*��1«��@��T^�D�J�E��[$FxU�݁[Y�5H��;$�5:��p�I7��t�(w`��V-`��Ý (���{<�H���;�	�r	&��"1��;�	����G>��k��;�+�� !�m�M�&]��\��g�@��;$��H<s�!�+�Nln�n��]	w����=����ޕp��$�9��&��n�e��|��3�z �?�q��S��s�@L����r�t��-S`�*����w�y�U`�"(w )g.�(��iX�Jg�� "W`n�)�r�fn�1v������m�n�<���sVI��2���8��"�M�_�j&A� �h7�yq%����Y��ɿJ��[%�{R�-����o���#IWUr���VI �&�*E�E���i\I!I�Jt��x..� �$]�.q�7�RJ{;�����^ӨהI�=�t�d�2�Apn�T��B�-\OB�i�l�$ݞI����t�FݦL�ퟶ�b��� :��2I�g���*�ߴ��^w�H�Uz��4�F�L�푤��@�h8�N���3IW*�.�t�8e�n�ИL=��E�N-�L�홤�J�U@�i�s�$ݞI�J����Ӣ�:Iw�I�J􋀦Ӣ�:IwD���]TB�i�t��;"IW�KY��t��;"IW
����h:�J�J��u�U%t�� ������B�RC�9[�BJ��)���h:g��JB��)�
O¿J7T���� �S�����t��l�� ;>��+����t�Ax����    &�I��ok\�J#�����9�][��h����!���c��l��Tr��lX���c��l��Tr��������޷���'9�����f{ߚJtˏ^~�ޚ-ޚR/�J�'��c��l��T�W����ۏ�[��[SUZ�"�I��X�5[�5��r�����j
�����@g\0�t��c��l��Tr�R9�?�o͖oM%�=���A��[��[S
�^
��d��l��T�"`x��_�NU�~U�٥�6 ˷f˷�R���i�|k�|k*5Яf�Z�,ߚ-ߚJ\�"@�6�e�E'�뒕�K�M���f���T]���ɕ$�=A/'��П���.�߿�k	_:��J�s�M���ұ���w���%%L05�ͨ�f�Ęe]Q� Sc�̏1˔J��dEؔ�5�c�2�������f~Y�TR,Y��m�ǌeJ%��_L05�ͨ�fߚJ�K��Ʈ�����Tb��8�f�[��[S��~	��i�Qo�oM%.Y��mF�5��֔b�k���m3��߷��ܯ \3�]3Cv�#��K��*ovͨ�f���@ٵ��5��5�Ru{��*-p����m3*U�G�����o��oF���7UWi���m��mF���HՕb�W�M�m3*U�G��:����6��6�Ru{��JKU�O�}3*U�G���g:gT�nSu��w ��ξ���#UWi�KU��Q��=Ru�� ��ξ���#UW��^	��qF���H�U��9��9�Ru����
��q��qF���H�UZ�R8gT�n�T]��%�q��qF���H�UZ�R8gd�n�T]��/q p2��T����J�����t���T�����R�T"׌L�푪�*}�Tڜ�
�+/Z��bé��T�n�T]�:��:�gP���M�UZ���s�v���#UW��KU༝A���HՕj��r�ΠRu{��KV༞A���H�U������A���7UW����F�9x=�J��o�����y;�J�푪��@?�ns�v���#UW��KV޼�A���HՕj��lZ� Su{��.!����A���H�U����i=�L�홪��@W�k�� Su{��*1p�0'������*���^�ؒش�^����&��4j6e�n�T]�z%ؓ�ٔ��=Su�	�	�6��M���3UW�Z�"@�vS���L�Ub�_��F��L�푪��@�	h8�N���#UW��KV��p�Tݞ��J����Ө㔩�=Su�\� :��2U�g��:�WM�]�u��xSuU!�h:-�ΡSuǛ�����)��9�N�����@ד�sZ��C��H�UR�x��C��H�UR�J�9g��C��HՕZ�+x��x%�j����)�:R�kO��TrA	=�l�VI��Z����_%��q%����^��_�
�����|��@��9�8�B���3%.Mx[�BU�|&�B����\��#��ԁ%*Bv�u���⥩�@&�:��!��f�������:��!��f{_�J
tېކ�^��}i*)�%lC~x�zi�xi*)p���&�:d��l�ҔZ�xp&�:d��l��T\K���A��K��KSU�&sK�C�/�/M%����A���K��KSI�KU�Y�4[�4��� cK�D�/͖/M%�E���A;��K��KSj�^	��Nd��l��T�"`l頝����J-U��Vd��l��TR�_�-�Y�4[�4��WcK�E���f�����&`n��VI[�ҁW�Uh7斎��J���W��K�gl����N���ɥ�y���'襔k	]j�z}i�����@��0���/�c�2t�":��ɥ�v�㕩�!pI��'�]��t�v�����Rc�̏���I�R���f�̏���I���]j�Q/��餼����K��2z�JOھ�KR�l�ћVz����� v��v�h%��T!�/�2��2��ߗ���.��.�^��}iJ���;��n�����T����-�^�=^����6��n��#bW	���]��]FE��7bW����-��-�"v���t@oT`��l�Q�="v��E���,�"v�Ƕu*�2k����?P�Gn��n��߈]Uȑ�[��[FE��7bWɀ~`��l�Q�="v��E��f��?��&%<0�t6˨���J���2}n���g� �:�W�l�Q����U����Lg�������*-A��[FE����U2�_�e:�ed�n��]%���2��22b�GĮ���2��22b���J��L'������+uB�{Yed�n��]u`]3ؓx+������*���*b�GĮ�@t���2����F�*�?@�9x+������d�%(p�ʠ"v{D�*�/���[T�n��]��� ���*b�GĮ:�lt���2����F�B��F�9x+������t�%(p�ʠ"v{D�*�/Z��[T�n��]��t���2����J��St���2Ȉ��R(t���Z��3bW�X�" ��2b�gĮ*��:�A�d�nψ]%���Nӎ��ҮǞ���w�7�������H}e����4j6e�n��]%.I�$j6e�nψ])�zcu�2b�gĮ:១�4j7e�nψ]Uʯ�M�~SF����UB��4�F��������%)tj8e�nψ]%�M@�i�qʈݞ�J����Ө��=#v�T� z4�J�됸~}�M�E�9t��x#vU��(��9������@W��sZ��CG쎈�UB��I�9-zΡ#vGD�*!��$4����V�k���]NB�9»p&(��;aQC�9��BJx�)�
/B��9g{�JBx���K����s�����SxN���t�Ax����F�*Я ��9������UTF�9mp��m���U:�_����T���R(�W�z�~���-���@s�;2�ȧf�������d�S��OM�.A�O�"#��-��Jt���H��l��TB���J:6��^�^��Jf��k)��-��R)t�����˧f���:�$E��z�J
�xj�R~0�t�{+��w/�U	\�"0ƥ���TJ�_�,vl��鱧��\Z:�PWO�OM�.M�ϾU����R��w }�\��zZ>5Չ%*�ӸT}��|j�R~0�tP��|j�|j*%Я���5�O�OM�.M�S��|j�xj*%�u�-�k*��-��J	\�"@�dS��l�ԔZ�+�p'Q��|j�|j�.&`r��]F=5�'%�RhI��'�[��p�'�wG]����f���)�%pi�p'�Y��h姧����7j7��e~LV�PR�@��\jc�[)���S\�"�f��zj�O�X�����Rc��ޱғ�/�����+�W�����^��^�a%�T!�� �2��2��ߧ�����+����}j*!�o �2��2���)�B���*���=��ꀿ���V�!ǵG�ȗJ��W��W�䴶��Z���(�V���滺���S��SƤ�j���KR�5��2&�W˟'E�N��N�«�ϓ��~o[e�>�*���|�E��N�)��W!�0�t6�L��L���$E��F�)�w��(��(3%�3�.B��6����9��z~~�KQ��9��z����z��(SĹ�w4~X�"�f��)���?~)�0�t�1=��z����_�2�sL/��xp	���cL/��x�O�%�9���x_�w���c�a[%����?�1��1m��r��C�9چ����K�%(D��.�w%��6�c���z��Zd�%(p��pK��NݵȀ~���}��]��]�� �����1A��^ǿ�W༐��s���k���J�i���H��IݵTZ�" �}��]��]�� }�����]��]��l���72<Rw}Rw-2�8mdp�D[�f(�aC�9h#C�K�J/䂽�R��V2Te.J]�Nxߏ6�h'��%���ǯR���4j3���;v:QVM�5�M0bK�J�u����NӨ�l?��[��E\�"LI�j�"ZD��ޕh���k���/yK�    �Z��:5�U��R��KR��n:c��w���_��F��W�޻�޻��zoh8�N'L��.P/B��t�FgӁ-)��w-�Q��t"E�H�*.M�G�9i��P����S4�M���P���KQ��s~�9���Tr9	=�E��դ�������K��=緒�;��;���M�<�.��N�k�K/�t���
I�]��A=�lƅ���SxN���s�~l�d�`��;z��O���מ«�!�s�{�H���?\�!�s�{+$�p##��$E nƕ���*e�`��zG�9��������L,rR�T7����f�צ�
]	���<2��⵩��p�G��l�kSZ�"6��CF�6[�6��B�ݑ2��⵩�@0��VȐ~'�!�%Е$-�]Q�6[�6��J�������f�צ�
�<I}p%�x�6�	�Z:�����v�*�� CKǘ\Ja�M��E���am+�=��N�ȜSK����⵩���)�9�J�~��~wSʅ��c>\��{Z�6�X��P?'���Mu����J4��͖�MU����4����M%.Q�S��|m�|m*-Х��S��|m�|m*-���.k*_�-_�Jt9�K�rM�k��kS��.���Ė��쟘���/Q%v��������\Q���Z'C
�t��VR�T�(ax��a��tej%E�k��Ǝ�Õ��-�o��[f�k���M�.Qx�cF�6�'V���<�͆�b�'w_��a��aFoX�+���ff�ܯR�@�p�4v̨�f_�J\�"�fÌzm�xm*1Я 3�3��㵩�@�m�0��0�^�=^�R-�J �3E�{+ݿ���Lcǌɉm�(��R�PbÌ�uY�����S�e:�eL���?P���/��/cR|���R�@o2����1)�Z�@)b�#�Lg�̔x�����^x�_�Ht(��s���.��.3%�3�.����Lg�̔x�Ļ��~`��l���x1���t�˜R|=I|�	�2��2�_�O�j@��Lg��)��3�W��%<��n��н����n��!��_��^*�_�2���0��x1p����3L/���I�].��2�#L/���x1p} w�1����J�����p'�N�K�}%�U-��Z��K�D�*�!��ϗ�o�d���z��Z
��7�d���z��Z�@�C�������.;֝��6�d���z��Z�@?�fs؆�_�_��TE �;
g�*=!�V���h5�d(
��Z�������_�_K%�h4�dx�����Z�@�c��������k����i)��/�}�滣Іm栥�rh��y�]>@�9h+C%L�T����7^
�$��P幷�z=���L�6��9�yǯRKV�-��ִ�H�eȠ��4�4�0��P`��o��F�f�Ao�tߕd���k���G.}�W-���M�f�2&Q�Z襀:��U����4�z���M�~�Y����W)�
h8�N/��w�z��.��4�8�ΣhHQ�@�h9�ZΦ)��(���Ӣ��
k҂g�y]~��Ӣ��3A�����Kx�<zN�н��_�WD�i�s~�5�x�*����Ӣ��V��Ǟ��^�^��ޥ��I|�u�%��y<[!i��ϫK=�l�)��*���眽q%%��_��w���_[%iy�UXzG�9�=ď���B~�t��p!��82O���~�tN�[��n]�2s�(J��霳q��n,103���;��c^߃rei�c-����9�97Ѵ�C�l�����X��1�5�"�u��{�1�y\�JS^(��L�Au�yMށl����{�1��K�M��Y�ՕJÀ1��K�M�R�(CPS^�� O	���~��x��yJ�g^������w O	�L��^'/A��,�J�;V&�N^�<%�3/�i���� ��U�}K!��:y�)l�^(��-91~0�u��SN*���o��b���-ȧ�T8�_�2� �]'oA>w�*�y��X��:'^�Y���_b���d�y\H~%���v��U�_	x�� s]'�u]�+/A1~	0�u�`�%��!�/&�N��y�{G��U�]'v݂<�B�y	�1�u�<�U�Y���sW�T7�u]<ץ�uG��������T��t]Uɗqa����.��;�t�!�w�5v]<إ�uǛ�;�g�|�����T��t�!Fu�
`���.��;"]w2�� �.�R�#�u�
�e�h�K��H�U�`���.��;"]WU�; 7�5O���t�!�w�-���:;�R�G���}�p]罕�G���:~`)�.]��H��� K�u[%5���f�~����O�$C-Z,W�'|�/LE�ӷRj����/�؊���2�"s-��n�S�/�k`%K�\��gSM���L���F���T��@a�&|Ew;��=7�T.�)a,�{�J)���ɚ������iSO�_�E�0.��7M��� s��"��������/�Ewx�L��Z��0��E��莦�t��E���I�h:�h:M��Z�뚠���鼣�4��k���=Px����pEhڛ��>�K���N.T��t]U�� ����[��ͳH�5���U4��}s��fZ�뚰�O���*�/ުD
�u��|�0����p�:��M�s�\��%K�y��d=�t>�o��?���뀯@����+U�Ƃ�Q�\�E��t�J��� hL$��%@��\4�4��_p��|�Ʌ*_`A�|+��D��X�*���A�=P��M�c7W�|�A�=P��M�3m�TZ�F�K���9	pE��KШB~�t>�Ʌ�3/A�~�t>W�*	�g^Vx�%@��\7WR���8����|n�*	�g^�u|)���!�Ac/A�������9�P�,u�g?�]�<��U*�h�FU��7q}��y7W�|�Ac"���JϷR����1�4&Vx�`Է�G'�Ac/Ac¹����vΣ�\H~%�eZ���ѷJ�+�D��5�����J�����m��W^*�%8�$�Ac/Ac"������_:4&x����ٹP%h,�}�7�>o�T	��ĸ�߁x_�U?�^��{���^�	oE��KШ�]d<��}n�� A�
��8��ӹP�,�j��n����Re,[�W����*���KИ�R�;r���Z�-	��Ĵ��$���ڹU����@{����l�s%�x4�߂o�9[����<U��z��]C<��?�>)��*�]snRi�8[ m+-�U:ƻ�IR��(׶����&1�5y���'��z��ILwM�~\sYP���tLwM^\t9T
͵p� ��ؤ�j���J�!`�k�����[�f ^$@�0�u���)�	x� ���ɮ��O	�L��������J
�	�u$v���J
��'�+w$@�s���k��� ��N^|J��L͵T�;������g�[��e���L���O�����	�/f�N^�|J��L͵H�~	0�uN���_x� �`��<R�_	x}��p��k�$ ��r`�� �ǹ.	����J��N��*ﾷ(S�y���n�wq+5׺Y�0�u�8�-5�;5ע��R�s�<�U�n�Jg(�=�q��ǹT��X])�rg��l�ď�;5�r��Mt]<Хbu���
���y���T��xcu��W �\�s�X���J\j"��q.�;"VWi�~0�u�<�����4@���h�K�ꎈՕ"�W�4�%cuG��~0�u͋+)�#VW�Zr" ?�R�G��� ��Ht��VJ`��J� '�u�*VwD��� �"�Itݛ�&�,"Ϣj��&���/�$�,Z
�U%�R �[)5N�����p]�åd�EFZ�R��Nt��d���*�E$؉nV0�*��l��"�Dw���=��Tpɉ7����R��6���U��������:��p��z]�l����~`)��Rd:V�>�d�Qr[&Ew8�L��Z��*pi�@    <zNӱ���~ϭ��L��w���cu-bu�� =�=��X]�X]� �%@�y_��/#ثRA�*������BJr�)H�KK���*	ɵ� U*�������$מ�TQ ]B@�y?s�$$��Iy��-K'z�� �����s��e��D������;y�lc=�r>ml�+�(�XO��O{�`�g,�u�£�|��J����gT%��9�A�+z�^zF	�~�r>��BJq���E \R" ��U,��/�_���=\I)����~	�r>sr%��Z*�U!�J �$�=c/=�>�;���9/.� �	x���D ~��� |&�E �K����� �	x ]B@��ܓ+)�g^@��s>��{�% �����y..��3תz����*	-�Lŵp��r���p%������J�"}[�y��U?��T\� �2ҷ�G'�=c/=�@�ߞ�_��)����KJ�cl��W^@����+)���*�.�p�[%����~	N >	pE��KϨB~. >/.��;ע .)��c�$~�ݩ��/��χ+)��Nŵ(�~	 ~ͭ��1w��Z@� �	pE��K�H�Ѕ; ~_[!q���ϗ����������Z*��Ԁ��p%��>��е�o�9�1�J�K���Z@����l-��=�@����J�VI ��3J\Z��np%�x�3R#t��P��R
�g�	��4'�c��9_@�]���d{Ly��J��) ���A���;>�P��j�Ĝ�ds��'���߮Z%1�4�c�I�*сr:��T����Z��. ov�YnU��w���)Mv�L���K�%$����{Gi��;U�1'�c��{�,�N�s���)�w��\D�;���Jឩp��U$�cN���D�c	[i��;U�1'�c�(�B!��~��9�sJ��L��TZJ" ���ՇJ!���/�1'�=>��z��Z�?�pǜ��g��ۥݮ�xcN����O������5�$k�HdA���Ց�g�IΘ*ʡR ^�%�3�$g��r��8�_��W�3�$gL}��J7	�u���s�/��j�o�k���w�-�$[�W�~�\����DX���ߨ�;ע���W�E��;"NW
�.���D��;�8]��aD"O�����*��%�"K������%%pr��8�q�J �K C�e�VI:{ZK�k78b.K�e��8]% .)���;2NWJ�.�r�� �tG��KL洈A�鎌�U��"�s�*�;2NW)�~`��h�������%u�� �tG��*��h��\��A�鎌�U
�:-c�q�#�t�F�
 �H��A�鎌�U'\?�5�}2NwD�����Dx�v;�=�T�E$Xcn�� �tG��*p��0#�B�;2NW)�~`��i%�����@�
���l�Qq����J��sv�(���8]��_�cn6�(��"NWUZZ"gs��-�t��� =����s[��*�o�����V�k�� ����������*�o zΛ�1��j�+B����|����q�D�y�7F�\-�tU%��t�l��~�O���lV:�t>��~�O���P����akLM����]�%%�֘B���t�ե'z·�1���`g�B��H�Q쌽��|)����1�����Q����|���vF�KI��Q�}��-8�r>�Q�;��?�h9��(v�^vF�KH�l�Q쌽쌔]���Q�;�,!x�3F�3����K�@���3F�3쌒�\?@����J��)p�o	� ��1�����Q���8��(vƂ�Q��aE:l�$�v-�v�,�`�9�#�vF�#�1Yc$;c�ΨJKH���1�����Q��c2�HvƂ�Q�������ڵ��Չ�kp^���vF�~	N �;;c��H}�+p����{���߁��
��X�3��8�`P�;��?�7 ����`g����༃A�3쌒����y�bg�eg�����y�bg,��z% �+;c�Ψ� 5��x�bg,�Uɕ���`P�;����$�mG;$;c��(�ϕ���`��%;��%%���m+% vF	�~����X�3R"t�{c�,��y��u��[bPi�9��x���RȕD*M�Ɣ�
���(נcNi�5��V,�=dE t)sJ��1&�b٧�ۮ�[00�4�cRp�\� ��&{cj�W�"�]�sJ��1E�C����KI���1S�=�R�U$8cNvƔw��gK�]+�%Θ�mJ��{&�E t	֘��1�\��O�ϯ �1'[cN)��!��o	����15��#�����pƜ�)��[���pƜ�����뙂k��w Θ�7�w_��Qin�e�d�s�1�I�]Z��@��-�$[�����.�.�_�bNr��Ai��e�`�s�)Fd�|+��ꕀ����+�r���bN2���Oz��_�����S_}(zk�����#�$G�W�V��V�??�\9�!�"C�������?א`���#3tGd�*��5$b.2����f�Jy���Dv��;"CW�; 7�En��;"CWU�K����02CwD������sY�*Isc��(2�a.K�e��]%�-!��
��;2CW�~���h�����+B��9-a��#3t�	�p�\��Af���U��&�s���;"CW�KH괇Af���U���RP�E2Cwd�����P�+�M2Cwd����\=�/�]2Cwd��T]����1�����R�D�e2Cwd��*������Ɛ ��v�z �o�M�d���]% �]�9榅2Cwd�� �.�s�=Fe��'��" .%��;F)��f�J��e;@��%t[d�~`����tn�]UɯzΛ�1J���U��4�7�c����]% .%x�9F�\���U�� ��7{c���"CW	��!����L��R!�J���1��j��������fs̐�{|ƯBKI���1C�����L����ak̐�#�w ��D���5�&�~+��.����9��(vƂ�Q��4�{c;c/;#B/����{���� ��;c;c�ΨJ~�s>�Q�;�@��9v�(vƂ�Q��4�[c;c/;���$ov�(v�^vF	�~�s>l�Q�;#B�����{
�x�~�s>l�Q�;�*�����ag�bg�eg����D��������(�����c;c��(���c6�(v�>�WقƘ��1���`g�@��Hd��쌽��|	�p"�/F�3��3��� �br�HvƂ�Q�_�	���*I���-'������`g���w�޶��J�R�.��8/`P쌽쌔]��A�3�:�w��A�3쌪���A�3쌒��< �0(vƂ�Q��8/`P쌽쌒�\A: 8�_P�;��?� �����`g�>�`�����`g��A;�v��A�3쌪��DX�h�dg,�%���8�kWR�;��?W�FZ� �KvF�~����X�3J��0�@����9__�.��m�ޘ�ʭJ+���Nk`Pi�5��F,�������;cʫ�bY��\HĜ�dg��}X�+Ȋ���&;cLʭ�rk������3%�3�.�+�S�쌙���ϋ	e48���k��w9�"�1'c��{&ޥ��H0Ɯl�)Ͼ�R�{,pƜ�9��z����\ɀ1�dc�)���V����bN�ŜRn=Sn-�_�bN�ŜRn=?=�v_D.�o�tB%��)��KH�OzI�����+ W����هB鴫�����I/���x�ϯ ,1'��^�+�.��8bNN'�$�W�]�%$oN'���z~���sr8i��P������7g��Rn�Sn-�\8���p���{+с��f���Io)��)��o�p �o%��)���%$�a.'}�����ZԿ%#�DV�I�]ȭ��� �p�"ɽ��_�`>��p�GʭOʭ���8���G*��6    ���pR�i�]����0�_�'��ٽ�W��o�0�_�R�f��f�
Sd0�\���_�����.^�^
����Ƚ�����b.Z��U�j�F��(\��s�WѴ�.4ע�9EG�E[���م�Z�?`��hC+y�o��\����D��hC��eo):q����V14i�$�4����lH���
ro�@��p	���v14j�-u�*��17-ch*��+�Q�`�l	k�M���5���u�%�17�c��\����먛-፹����%ڥ���M፹��t�U)�v�A� 9{c���mXB"gs�R��ïJ~�t��Q&W����M{�l9�t�l�QW/D��+���fo���.���v���9o��(������6[�nz�����_~0���M��昢ʭJ#���KG�鰭��$z���c4�{c���y����m�·�1C�g���E�[J"�H�Q�=��?��:6�(z�^zF�KH��Q���]��l�Q�=��@���5F�3􌪴�D���E�X�3J��;���ao��g�g����D���E��K�(��o �·�1����g���W M���E�X�3R �J���1����g���u>l�Q��*��D���E��K�(��c6�(zƂ�Q����a�9�M�Sz�K�(�o)�p"q8��g,�����c��ᤊ���������l1�����Q���)��hRE�X�3��_�	�y���g,���-%x��E�X�3J��;po^���{�%�-!x��E��K�(��o��y���g,���h�y���g�s�ǁ%$o޾��zFU�;� �k�%��z��Z�%$p^���{�%���t p޾��zF�. 5 ��=cA�(�o	�p���E�X�3Rt�N;޿��zFXB"�v�A�3��B"�I�_��%=��?WN�h��g,�%�-%��
I�X�3J��>s�5f����E��W,Ɣ&{c�/ɸ*Yl ��OW�`�ckL�P��x�1�����,7��J�$bLi�5��>+�������1����z���Z���$p��L	������&[c�|&�E t	֘��1S>���J�"�5��r����N��s�5fJ�g^*-)f$�����cI[)����k��֘S
��>��M�9�sJ��L���~��9�sJ��L���~��9�㕤�1���J���I�.�*]�w�ܯ |1'g�^����� W��Ѥ5��[(�.�_�bN�&�$�W�]@��Ĝ�MzI��Ļ�KJޜMzK�����.���Io)�ޟ����\���dҢʡP��k��\MzK�����W%����8���>�R����0G�>Rp}>?�?���Ykb���ڥ��(7����8����듂k���
�sq2�#��X7�b@��d�"ʡR���R�7%��S���W%�p�\�L*R�Q)}vGa�N���/��O��^ȫ�� V���/x)����)2xa.Z���G�n|A��ߒ����Tn��������P�U���j���.�s��Ѵ�.T�R�ub.Z�ॴϮ������1�apING	��Z��%%D����7��7�a�]@�+�MM�-Q+}v�u����iCS�f_ю|vGQ�`��iC�������bnZ��T��W��_����iCS�f�E�io�u�%�17{c��U[)����֘��1J�^�]Z펺�֘��1J�^��j��fKXcn��(���z�u�%�Λ�1J�^�~�k���fs�2���"�U�/�Λ�1��d��������,��Rz�Z�'�Λ�1���m��R�nz�����_J'���]���!o���*�l3�c�*�Bq�����t>l�rB{�/���E���9f��7��KJ���1��� h� � ]���E��K�(�� �·�1��� h��w ]���E�X4R"t���;F4�:��D ��E�X4��_���cAc/A�@�h;v�(�Ƃ�Q�����cAcA�(	�/�·�1��� h���D ��E�X4R#t���=F4�4�sW�w>�Q���*��6�1�cAcA�(	Љ�c6�(�Ƃ�Q��F�xRE�X4Jt%	昣ފ����Q�+I���N�{	)z!��ᤊ�� h��x��E�X4���7�_P�A�$@��8�7/`P���� �
\���/(��^�FI�~.���AcA�(	Я��y��"h,�z%���AcAШ~��Ac/A�
��t o޿�{	� ��t o^���F)��s�o^���l�J��&m�A4�R ]K�0��I�X4R"t�^;Z� 	�Fp-٤�0H�ƒ�Q�� ���I�ا�rcq!܈'m��A4��R �" ���8��b��}���w�]Ǎ�Z�����I0��®�F]�Gl}���2���tU�%~O#O;�ێMt���m;QV��Xw�h���H�o��WN�ž�vm����H����~b�q�����+ZDW��"�/�7��j:��evEM��w����Fc]MgW��+A=V7���[*@/���ga���h��/�w:DpA�Ja���l�\�K���o��������:X�5?�e�T%��hW�Ѯo���Jw(��ٮ�]�`%�֟��Ra4w���V*��W�RkSl��k��>peh𿞙��K�ٮn�Uj�}������0����*�oުt�:�� �]}W����uǯJ~	0���c�T�x�E��zY��`�����?V�Е�̃ٮ~u.T�)^����>��]���J�_�U�����]�6>X�!��y��>�q`��?W�B�W������nb�ɿ�B�!�ٮqt.T���P�D�:�4�v���*��cU���ߩ�q`�k4�Ju�i}�-���Ɓٮ��T���:����X��h��x�`�J3 ��)���s!�L�����{�$ �	x��`p3�� �	xI��Kp�yp%�L��2(���� ��^��e��V/ ~v.T�c/?��w���U*Ǫt偿c/��e\�r��.�_�< �>�J���	�XU�uB �	�K~�e��h :R�_	x��>�?�VI ~%�eܼ��5����W^� ���tk�VI ~%�W�s�:��[pǾ��~}޼�p�U����W%�Hױ~o��O���i0~	��cøR��>���������4;�J����x�yp��8��Gp���y�h8m����χ>�W ��͛U.�����t�vW�\���_���ٴ��*�/�.s�0�h6�J��5x�\HV��d����U���D�2Y�@����Bܿ�3O������q)��Z���e��^s�VJ`_'�]��^sR�ٚ �������啾�Oj6�J]�n����`t4���ͦbEW�fY�o�����Iݦ�}�xG���$��vsR��T��ԙ'�����~sR�YY�o��W�+tj8��,�*ݿ�E@�9��lj����R~�qN�8��-�k��;�0:Z�I-gS˅���<�w�at4��4j�����(V�i1�pl��zJuy�4��tZ+���Kc�Q���G���H-�4�_�j:A�%��Tj�����(V�	Rlql�S�/��F�^
��P-�4_u��Q�ql$T�/�ŗFUZ�(@'�Z~i,�4������I��_�/�b5��&�FZ���X~i����~l�����e�>�@��!V�FA���|*��n0��U"y��ƯN�A��U
:���/��v��=��/�(�Ö�O�F�-�Z���KVy��n�K��@�� WC��ꙟ�+ ����B�D��@N�� ��n�VI�cՓ����3� |N��H�D9�� ru?	�!�)����������B�7��+���Q�}�D���F�;\9M����p�*���V���"P��=�J���p�4��\�»���J��Ii:'��?�VH��-~���7z��\HqȖ?PJ%�� V���*��cc*�    �;�գM>�8d�(��t^b����x���0�~ U�~m��3�.��F��\H�=�Jy:	�ǳUx��8�w�޶Tx�ĻT�;p��8��?���w��� W����V��F�9������^M�7 ?����V��^M� �&WR�?�+�� �ݸ���Ϥ����� �7�B��:�
-j�?�)���0�N�4 �<[%���F�o��\I~%���tZy�WR�_	xa4�C^�5\�N�7tZ2�^�9����蘿����l��O�;)�R�/lL��(�;)��h�%@�iֶJ��ܝra4���4#�U��|C����Q >R�r�4��״�l�ė�I
�r�ND�sr%E!?I!�~	�i�նJ�[�$�\*�%@�iW.C�g�N+Js��@�\I!�ӊ�t"����VJ@�ӊ�tN��=�K)�#tZq�Ή�ӜG�J	нԝ'�}��^sR�Y��o�����bGo0�c��Z����K9'�fsR��TRڗӜY�o�8:��I�fSQi_"��/��I�W
�S�)��RW��;U7:��I�fS�S����Cu�*tj8+e�V�Mz:	Щ�4�[)@/'�&���q6�0�KD�_��*���r6�1�[����b��]�m��2JėF�pRLr��T_���j�񫒓b`��mC�2]0�+���Q���^*�����(N��Q���f"e�`VN��їH��T�Kc��Q��G:���_�/�$=����6)A�%���bG:���_�/�*��ڳ�DJ�s�h�4��S��T�Kc��Q��G14�3LM�/�#��R��S?��H)�|r&�(��L�'�~^I�T��9M���m&RJ/�~�:��&�:O0�����)�;
�ǆ����'��w L}l3��C��c�`�<������(G"�z0���6�8�}"�,�~0��y�I�"���B��yz%��L�i\��g��ܯ ����D�����/��Q���K*FsF���4�
`~���H��G�-J�Y�/u�_R1�3b4���Q��L&%t
�P�N�`~�?�Hd�`�7FSR�^�b��(?O�'�0�4�6�(d˟'��_<��b4g�h*Js�����)�w�4�
`|i�m"R�=?9Y<o������%�9#FSQ�~0�4l�Tx�Ļr�NDo�^R1�3b4ՁE�o_R1���T��
`|i�m RQ��'�!��m�^<��b4g�h*Fӯ ��Ƶ�C*
y�+-��q`zi\�AE!���0���4�$c4��)O�!����C*������Ōo]�1�3b4U%'D0�d�6���H�*�4.�dT��MEh.j�bm�[Q�WR���tZ�KֶyH� ߟ�,�;�
aV�M2*FsF��d<���c�T�r9�� CK�&�9#FSUZ�( g���ќ��M�h5Ͷ�H� ?� Bs1� �M2*FsF��"4��Ѵs��T�r!4���4vɨ�1����J �m2*FsF��:�� m�]�<����T�5
��&#c4g�h*B�Y���l�
��T��bh3��22Fsf��"47��<j3��B�t⯥��C�y�6Y7-���!����*a4�:�J2�����3bh5g��!�.�o��W)g��kN�5�J�Ґ9�����Juj6�����1��w�nt��sl��N���u�*u�7�Z ��!s �D�s�m R�����8	ԩ�,�[*P/';
ԩ�ljԷ�����4���lȯ���F��Nib:϶�H����u;��_�Ͷ�H���I���� �-ڐ_˯��4���hՆ��X~m����K$Ӯ�����(Nsѣ@��f"%�7�r�Ύb~�]�H�D=�	�әH�N�6����k�N8+�	�F7����k�J9/��F7����k�H�ŏbj���"��BDr��j1�c����ˇ>��z�0u�a��x%�SHM��0��y�����Oe=����1�л��)=
��6�H�௅�`���m(R��1�_IM��a�<ä�s~���~0��y�I����~%5�`��������Jj.zx��T���x��^IO�"���E*!}��B9�W 3L�g�T8،p0U�� f�:�0�p��`��tf3L�g�T8،p0�j.~�?�X�rc�ȣ��1�ԟm*R���C��W����#Lu�߷P�@���W���0�p��`�sca<¤��f���J���X��"��S��R2�0��ME*��>/�B&�O0�p��`���+�	��L*lF8�b5�`�i��
�&iO���6�X�3~����
`�i�m(R�����U�o �0�p��`���+���L*lF8�b5�`�i� ���X�ŏ�{�Tx_��(�B� ��6��>9YU���A�K2lF8�:�f��f�d8،p0U�Y�.ٱџ
�+�.���G10ֶ�H�"ߟ���Sc�\���D*���Dd�sKFsK2lF8�"5�
`l��%����g#bK��h�Z2vɨp��`���G��mx+�N�T�;�f�l�T,��ɉȣ^���.6#L��~�i�dT8،p0Ej�%@�il�Q�`3�����Q �>6#L��NE�k�T���`�� h3�|22lf8�*�<�L#����HM���fYed8��p0Ej.~�y�f�������tR��<��H�t��nW��sb�4'u�2�}fv��=�f�Ք��3���	��5'��2�}fv�*��Q�Nͦ�n��ݮHM�h7��F"%�P'�FG�9�ߔ��3����W礆Sf���nW���t��:N��>3�]Ҟ^
�S�)��gf��~�t6�n�_˯�"5�g��(�]~m,�6��tR�ys��9QO8*��m$RAdD-��Q�DR~���X~m����K%S����X~m���Q�N��kc�Q��ӣ�`j�6)Q�z!5��S���H�zO�+�饀:��˯���F�p1��(�]~m,�6���G17vls�R�!&�W[5Ƙz��"� ��Oa5��So�X��_��)���G16�cL?D�$}
��� sL��~h^I�T��+pd�C�1�_?�K�9�n�\�b�c���+�1�n�X�b�s������<Ƥ'�s����~0��y�I���~e5?
�y�I%��7�D��~0�ԯm.R��#�*��\$ ���H%���_(?
�y�I%��H<Q��� S�A&�x2#�D����<ɤO�x�XM'F0�4�m0R�Ȗ?Q
����ƱF*��'J�5A��1dR�'3O$��d$&�x�I%��H<QA��1�d����B~0�4�6� �	x�5�`�i�m2R>��k.��� �J<��x�xM�d<ȤOf$�(^s1� �'�T��|O$��& ?��H�#�������8�J9?�K�Őo�cR�'3O��w sL����ē�'��\)��u�*�d��'��tfSL��F#�ׇ>/:I��x��H���xW��+an��2*�dF�:��&���2*�dF≪�R̍�WF%��7�D�Ύa~��6�x�����G��cz��6�x�;y��k.�x�SF%��H<Q����.��P�#��#W��+o�ʨē�&����
�ݴ�F*��\�
�@�is��T<�<r�5A
��)�O�g�b[u֣�4�ʨē�'��\)�&��L<��x�hM��4���"�o��=����6���ub1����22�d~�����bh4��22�df≢5C��c]��I�hM���jN���o��Jk:+�Vs�m*R�7d|C�5C��<�5e$��HZI|:��Hj6e$��HZub1�@��MI;3�V��~sR�)#igD�*Zӯ�i�T�D=��UZs1�@�:NI;3�Vњ~�rNj9e$��HZEk.��S�)#igF�J��iMpYI+�6�_Ekz6�Q(���X|m��)��F���kc���ħs��ϛ�Td���ڨ�!�T$��ʯ���F�r��%�)�V~m,�6��\�<@��i����k�hMoI0��(�V~m,�6��tR3L�ަ"%�=Q/��bH�:e�ʯ���F��Eu
��_˯�:�by08ƃL?�/�{uVc�����+Y��Z �  �<�k�`�T`��)��"H17�sL?d�d}
��� �L��~�^��Zs�� p�dғ�1�/hO/�y�I��=�Ry �m���G�1�Zi1� ���N�W^�/&�:O2�)�⯼�_�2ue��Ay3b�(&�:O2)��a(�����6����#���J �'�j~ϷR�0���� �<�4��>r��TZ�< �g��/��*ɣ̌e<�d�H���Rx�˃ѱc�TD�叔�k.��c<�d�H���RxM��L�m���H���R�O�#1<��L�m\�f ^>�;�Y���LS>�Ri�� ��F*�g^x�Őp�e��^�/f��2M	�L���ry 83��H>�gx�5�`�i�,S�WF��Ñ�8�#��G*"�̟����H8�2��H>�gx�� �L�w���H>�gx!6W. 睿��"�G�j�d�����_	xa6W, ��H���fsq�c�L	XF�N4b5Xc���/S�{P��u`��`x�3�d��`�K!'�0�d엹%�|������m:R1�w2Ʌ�\)��t�b��d���w L�~�[2�w2Ʌ�\�<��3%byUz>4Y� :Nc�L����}�Ry��܆#��$�\*-�x��p�b�������ݦ����'���l�T�M~�,��JE-kh6��2^I�F�h�QԲ�^��m4R!�/����}z)@�l��
��C��	'��kYfd �� fUj��`J�zM�<#�Y�N��ٜ�l� �̊�\�<��k�d��p��Jl.�3z}����5��W��vS0�`�ԧ�� }l���XyV�M@�9���3�U���m����ҳ�k.���m0R��3���FG�9���3���W=礞S0�`V��_��u���������y��      �      x������ � �      �   o   x�M��!�3�#3"���_�g�=�0(S�5D]�n)��?y��-0���K��%ݑ[�֛��*�����j�##�'��B~�����gr������k���t*      �     x�u�Kn�0��a
~�(�.��9�T���J���gT	�i��$~���*�Ʌ��(N)dfM�^ē螔ć4BXz�imdz�8$��ċ8	���`R.�eֈN1I�c�m��`��s�,��3a���o�oy�K��'��nߌ�S��Qw�����;@��|*^�ӕ{W���ʽ+BCu�����"�]����m��"�����+w�	�HuEXX}��"��}�hu}_ h]�L��+��k]�w�sG���Օ[WȮ�\]�����J�	      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �   G  x���=n�0���0��?KW��vm���JH�K��� �@��4�d���l4o�������y0] �Ck<���ф<��Խ���ܠ�������0������˭]���a��͂�J��5�2�p����M�2��F���׶�q`ݰ$x��mT�e�S��,�Jg�7��!+��x�	~�{b�e�n.oC�b��l�@ݱX�׷F�yT*ā˯D&(>�z���b��l�vP̬�3%�sf|ν�d�A��ee͗�������e|��q���NF�,q�U-k`ulTuil�59%p�o!��%L�1�.e[
������~?���K�?�      �      x������ � �      �   �   x���Q
!�o[̠f�{y�_�k���
�r����B�@� P�b���y��q�D����{i#�t�I�#e�A-��ر��T�S۾�S�mM���b.��L�W�_%ڮ�	�7�L\�d�EWɄ�d>�{J)��_�      �   �  x�u�Qr� C��a:`�����h6�m�V�ˌŋ��8ӧ��eD��v��#j��E��鳗��Xͪ���R8R���E�H��H��������)KR�g�,E!w���o��jyǯ��o��T�P�Sr:et�BP�U������E�)�))&�)]֥6�tA�)<�r��0��U�w�
Q�;�E9n`��!K<���GQ3�PC�q�q�}h�<��r�����P�[j�|E��h����[bu�#`�mׇnoP�n]PЈ�P8��P
�t*eBQ-iĮs�q����2�}�2��CJ�;e%����}y(�&��FR��Y��H!w��x6 �!{O���lF��@� o+#��M-���1Z�*�嬓1����Я�^HF�>j!y莀�ܺڏb��� �I�@2�A2��sA3kbrB��ٺD�b6m��*���2'�Tg9J������j� �Q
�F��zjpN�Ng=�]A1��dVG��ő�Gi�����8~ 5-�      �      x��}k�5)���[{ɱ���^f��h��:���t�T)o�8�#��{�����\s�5�%k�/������G��2��9 ]�N7�Mt��v��x�C�r1"�Nt��1N���{<���c���w��H!`�僒6�#���L�r���+S��m�c�郒>��ٮۆA�ٮ�A�
r��l�Ȕ� ����ZcJV���k��Jf����0��YA��SS���N��Z?�����/ũi��ی�`���f��%���#N0bL����<��Aߌ1�Ճ�X���9Ƙ������G\��`�#�7�Sc��3ckg��B֨�83�3#{�#��V��������c�1Z��>���I�����!�o�����y�o��F߫/��8���1>�v�Ӎ�I��F�َܾ�E��7�G��W�1�s�yY��+�rn6�a�di�f�@�J�H��<�LL���!���v�a�s�y�)9��87�ǰ#S�����l��A�r�P��<�{ ��>��A��s�x$q�]����A�a�_�%e8�����;"�㎱��ݘ�=�~n�Q�F�@�qn�u|0���/b����?��H��W�%������g�%���G�僑5�r~�1fd�j�a���9���aWJ���\�7}�Q���/)�T:�씢��қ�����R�<f ��N;H����O�7�-R���W�(YöD)Z �d�3�hy�8��d�-�Ĕ� S�32rN�UL�r��&!#�R�����1vJ�r����A)Z�탿���咙�d����X�������<��j1�/��T���H-���>�[1�h9w�o�b���!!W�S��"��c�-�ƌ��S���#3�qR�-��Y�G$;V\"g�~@�S�%���L�N�㫳�,tVι|P��4:+���=� 0����߼�y�9�WA�F�oK~�7�k WA�F��]~���_��WA���]}n�������&�,<��'8C��*<�,�� �Ux;0%{�WA���L��Ux����>�a_l�*<��A�
�*<����WA��nL��WA�dfJ�1�gA�1��kێ� ��1����q���_�� ��11��� �@w�o3R�pj�1!{��g�"/�����@�Ff�~hę��Y1���_;��c�Hv��9�1/ȵ�B֨�<�����.���0#;F;��G���1R�5�� ���֚Ƿ���+�u������^P忐������f���Ds���p�M�@���Zk����� #�Z��$%g��k�^&Jv�\k�)Q���Z����$�Zs�خ$�Zs7��
�k��~�� �Z��K�� �Z��ɔ� �Zs+��d�Zk��Ȋ1q��/"��͵V/����z���یp��k���b�j���w1B���#/�c�Z�߶+�̵V�� �6�k�^�'�#�j��U$�1S�5���i�ȵV�#"F������'��Y��Z��,#�p�)O���q����ٳ����A���_����0�Qm6^��7�=R"m6�j�b���e��Z�#�~�;S�q`�B��lL�� ��x��2%+�Jy�W���Yk}�b;�;��^�'Jְ���>������{!�)�A���x/�%+HK���eM�
r��0�/�2%{؅�x/�3%k�Fy��������{m�(�A6����9�#}é����b�/Aqjbc�#~A�S��s ^����o�X��~���1��g&&^";�By���d�h����	3�R)���bd��p��Bz}����k���k���^ݙy;h���k}��x�뷾ɮ;�o�����������0���}�w��2��^gFְ����B��a��
�lt�<�&�dy~Zþ��d�<�+�L��_��ǰ#S�����֘�5�9���)�����v�i_A�Ly��D0%+��y�%�di��[gJ��+��0%+I��x�5F��o??��#���H=�������8����MF�9���7��<@y|5�o2R�]90R?�;+Ȃy�_��gg���x�Qb�#���x+��f�q_��c���!��1�|�O�}�Lp��q��s~?�1����d���H�����l_�L�M�'�`��6�V��+F��f|���c��f�F��=�F��W&��:ѝ6�v�m��� �͐�3H����#S����\Q�1%+H�hd��d��>v�Y��,�ٴ��\Ao6=1%;�ʛM�L���fcG��d�i�i��}ۛv������с7�����7�d���� ���@�}P2����$+2r�d*�ٴ�kd�h�ٴ���+o6�0�h��{�o3�i��=�7���;}�}�?��O6^�����;���'/����������tDۉN7z$�����=7���'���Ư:}�ן6<���6��b�t��41�clx��Z-3���d�uLbd�8�a���]��� ����F^F'FV�#�������������J��1,�5M��Q�ȯ8��=�JE#/�%ݨhd�P���T4�1Q����F~�D����/,��Q���=;�v���a�� ���ϔ,t������d��Ѻ5�i�A��F�?+�JE#�laJְ���(���T4�=Q������K������X����m�~}�+�#m7��{��vx��D����s���5���xMЀ���Z&�~�p�#�0���w�e����>F�xY�����^v��zK��@ہ��9^+�
�L������9�)���:�F�%�^of^6<sVo���h���oux�w�Fy�_y|�"����} ���(���r"f�sjo僘���~#�`E�%�~]�Ǎ�_����1+Ҟ(���~"f=s~o�E�<���	�ߦ}|1V�F��5}�"���ϋ"fG�8Ƿ�A̎�s�o������e�־���ڌ�s��&#��׼�w#/��e�l�kG���/�W7<�~���,��k�7$/��_���0j�Q�o�Bg�R�_˅N�K�t��w���}�00�s,���u�!�(��r"��Ո�3�pu�?F?xY���-�o_�����{��e�h0J�k��rZ)��c��@'�-}��������w����僗h���H�#'��}��V��胤��Åt�B)���1k0���n��H� 5},�=��'��?~~�6xDZ��/+��g��������~�"M���k���$�uh)Ҏ���.�<��hG�u��{��	�7^S<���ܾ%~tPW'Ջ�N��*�D�B�������f�hܼ����8y������L�
4���,2�)L8�=��6��%��y�,��d~}��~�q������?h\����3��z�0/���=tz���1��6�� �M�-������@?B�5n�eZ�B=��rYC_i?��/{���oY������Y��K�Y�'bv�O~��`G�����`��5�N'�"� f�z���ዘtr�&fE:;������"m��_`���l��,%���|��C�5�O�`h6g���_h�����_h�f�5d������
�V���KB{���x�5d%���\�o�R^hu=�}92;��r�V�m�;,#)�Ц�{���B�>����B�<
fDj.4_�0#Rr�y�,9����	Ⓜ��.�Jo��K�wx����z���Bk����[hmup��SZ���Wx����[��*��j�z���B�{���Yh}5o��S2����.{��������j���������V��3�,Zo{#��S��s#��S�
ݓF�!&c��]8 X�+to�i�W���=d��Џ|n$7XI+tO��e�����$ �2V|����
}���JV�oU��(��Bߢ</�U�~��U�4z����T�3oRQ�{�	??)�н�C�)��[{���z���TS�>.�X�)tg~}RK�{,����B�O��s�    �z��li!�����V�(t/�$ +����N�U���ǧD�ʹ��)���CV
�7�JA�����)��$[Z?��ɖ�O�)a���zʘli�
&[Z;�����	=UH��rBO�--��S�dK�&�|`��ez�liՄ>)}��DzN�lḯ�3&[Z2��ɖVL��0�҂	=W8�j���.��CVr	=w<�j��^<�j��^k�VB/�}ޕ�_R	�\��,�z���X
%�r�o��I��:�_`)���yl��S*	�\��;>%���ul���4�]���$�]��;>����<��CV	ݮc�V�ݮc�=d%���:��`����:���)q�n���?����<���)i�nױ�+e�^�c��F��:��`���k����}��F�T�V�y��(Bߢ:�+M��5u^��$���y���R�y�X	"�-��+=���t^�)9���t^�)5�ް��z����B�j�Z
�7�yj%�ި橅zÚ��A�j�Z�7�yj�ީ�Ez����@�k�Z�w�yj�ީ�z����?�j�Z��w�yj��ޱ��z����>H�A5O)}0�T��L5O)|0�X��5O){0�T�L5O)z0�T��5O)y0�X��T�L5O�w0�T�r�`#�j�a#�b[p#�Zl��H�����H�����wxB�``;f�R� ��,\�p�,\�p�,\��Y�,46c.%��y�C
l��K�����K}�n��Ky��혅Ku�t�*`Y�p�,\jp�,\Jp�,\*l�,\
l�,\��a.e�b.U�A.E�C.5ұ%q^`!i0���p�h0��p)h0�	��g0��)g0���f0�F{�3�J{CZ� �R�1�J�(e�J�`�e�J�`�#e�J�`�Ϗ�=d�b0��p)b0������0`�,\Ip�,\)���,�c.��ae�J�`�e�J�`�#e�J�`�d�R�``Ϗ���.؂Y�T.`�,\	������(W��!���d�R�``f�R�`��f�J�`�e�J�`�3���`xJ�``��Sr\)<�V0�Ób�q#QZ�hn$J�``m$J�`�#m$J�`�n$R�`�3n$J�``m$J�`��6%R0��6�Q0�7)Q0�7�&
ұZ��`!P0��6�O0��6%O0�	7�N0�7%N0��6�M0�F��&���T&���&����%H�8p#Q�h#Q�i#Q��p#���q#��\�D"	��D"	��D�p���#��'�F��l�z�'�6��Djl���"��'�D0�O$R�`��s����s���V,�H�nXΑ"ܱ�#5R��s���,�H��XΑ���#�8c9G�p�r�TX�r�����=0��9Rz`�;�s��@
���؀��;0��9Rv`��s��� g,�Hс.��h́6�I��� W�J���@7�K���@w�L��@
��T\�p�\\�p�d\�����Uht�t\Jt�|\+�AB���bF�u�aJ�e�SN.UR($�Zd`�f�Zc`�#��Zb`���Za`�3%�R``�d�Z_`�Ss-/0��'��R�ɹ�Nٹ�H�Hϵ�� �ϵ��@GLе��@'�Х��@gJѥ��@Jb���@n4RT`�+�4RS`�m5RR`�;�5RQ �z�f#:`�.�R�su)'0���u'��[1�Lٺ�!�Qi	�a�.��b�.��Q�.��jɾ�JG �v�d�1*����K����K�N��K��Δ�K	��.��K��6�ܫ�J����F����NɻTH�[d`II���K����K送���� �r�@g��p�@J�n�@c�������Vhj�עM]�Z3 Ń��d�@L�b� c��`��zM��Z.`���^�45�k����n{�0��n������R@���P�@Sǽ�	hl��2�=�Z%`���^�4u�k�����{-0��w���@�@S��H1R뽖h��� !��� �0��� �1��� ]0��� m��ka��f	�����x�|�B>N����k �Nt�г;-���Ot�г�,������x��DGR�I�W2%��C탒G�)�F�j��da�������BM��� +
�L��#� �Ӥkj��Y�&�t>X�&7�o1�WK�;���-Frdm;>����D�4)��{0�O;����1^v(��c\}�otg�6z5��)��f��Pj�/B�1��S�zZ1�NS�Qgfd��v�z��`F֨o��ژ��&;���k�<b�����Kd�v(�5M�� ��hJ��ꬑT�)����XK���)���պWi덮ǉ�����mzv�Eg�7��]n���3����|�gM��W0�r�#;���c؁)��.�n�@W�d���m�N�� O7�ǰ/�����c ��������H����A.t�C�,�D��o�v����bܥ���3�bܥ�7�0�R��caF֊Z����#��c�C�����1���ufFv����?F}|02�K;o�1#+�U�y;᤟1����ya�wi�c����wi�=����H9��teFv�F)Z�o�op:�])E���o��9�F)�˨���J�;�h-�]�J��y:��N;�(Ekgt�78R���	��HO���(.��R4��e�V��R��m���k�6�R����b�Q���锢��/�7I��<�@���DJ�Z���v:"�h= !�'J�z��4cLG��Պ��c��5|���Q��BaF���yڕ�i��a7N�z�%���<��D�
2��+3%3�(E[z�@�
r>ńa'��5������Wg����24}u��Y�������Y�e���$�*����U�.�@�Ju5 EzU����`�C�R�W���"�*Օ��U�.�H�Ju EzU�� )V(:�*����,� )ҫR]H�^��@J�
β��*����U�>����J��?%zU�O�)ѫR}�O�^��J��T�S�W���2�*������?ezU���)',2�SZMOo�:��\��,��)U��?�Jfy�O�˨SrK���2���~*$�����~u��i?�/�n�:��e��:��e�=du�O����I?�/��!��~*(��������>�BRz�������!?�y�xY���|?�ƪ#~2���'�d$�����
Rz�|������uD�cy������<ܻ�0H�鳽󂔞>ڻr-�,O���
˃��I���՝A-T�]*�B���u��C+�S�����g�)\� ���.ɖ>ѻz5$[�@���_����*��'��Sv8Ax�4?uy;��a�Un!��gy�dK�S��}y�O����A~�*���s|�����N��?N���!�ӭ�>�w���q��t�����J���Ӎ�>�w�����N����>�v� �S�����̗Gw`�c�<��b3k��}���Z�ۧ�0��ձ��
������L���w�w&h��27ƈh!�_�>� �T��'��2��6,���_<�z`��/3_I��L�6`���/��"�J�x��m%�_|s
ȶR�/��������_C\SJ�L[=�I!�_���S���'�FI�߂N���/�IE\�J���o>�T����CF����I.�(t��gQ�Q	��3"ΣR�/�G\�J����b���0��gF�������_<U�83J��x2q�)���:}���`i�?�Rw���b)<X��b�<X��b)=X�Ab�=X��b)>X��b�>X��b)?X��b�?X���{�B�����o�P ,���{ؑ)YA�K�����!�"����qn�
aqy��1
�2����aq}}�K!�2���aq�}�K)��}��aq���1F^P+��b�FX\e    ?�R����>~��aq�}�%HXz��T$,.��b)IX\i?�R����>~��(�Mq�	�F�A����U +sq��Oj�2��z�#Z����]R�"0؋�P"0�k��"0�{[��E`���vHZ��\c�m����t�~[�ؑN��X�����Z	���E\PJ��\g/�R��J{H��$����d+Qs���d+U�Z{���,���^�%�t	����m%L`���� �	ʪ�8x���T���RA�.��
�w���C*h��B��������2�(���ױ��A{//�D�6_*^dy7���T�d�n�~�����a���Ȁl+��n��VL�͊cBt��d�����=��A�0�34���TZ,� �Sq��K�p"�%OK�Ǩ#�㎱A�h�NL�=��$�S�ŸH�Sq��1V��3���k��T�=1r���t'�_��c*nNqE)C��c�-R
0�;n�R����n�R����n�R����n�R����P��
s��B)d����)u����)����j�Pb.�0�l+)�2-��m��\�c( �J��L�!\RJ����P�5��K��ð�s�FCȶd.�4�m��\���)%�\�k(�R��e��r�̥OE\�Q�2w�Ӈ�e.n7����\�oOR����N�Rf.�8�3����;��Cj3�i9�3�ę�{��C�3�T~9��%`�D�1��c�D�1�+c�D2��c�D+2��GTI2��7Ti2�Q*>�(���0V>�*���0�>�,���0�.u�̅��s)�d�<����d�=��-�d.?����d��Dkq&;̨�ՙ�E�����e�����l
���M�Z��z�`EZգ�D��1��F��܌J��s�+�ƕ=�@7����N�qi�Y��a�O9dt�Ҹ���ȥq�9ЉJ��$s�3�ƥK�@*�K�́6*�+���TWF�ܸ4��2��w_Ze�V��K�́T�f��4��28Qi\�ep�Ҹ���B�4�h��A:fojéQ��ܨ4�<3�si\�f�f��5s�m�6s�#�Q*��N|G��3:Ӿ �3�ж �3�hW�ޙ��"�fPk����'�Q�/'RZk�i�w�[��c$�qxқ��h_�%�H�Q������5��F�e���(�I���`��m$]6+���Ƒȶ��(�:���c�L�Hd[�l���KJ�l�i�kJ�l��D���F����vF�$qG�Ne:H�RVeZH�R^�׆;�4�(�"�S��6��H�*�6��H�e���(�#I���p���$]6+Ǎ�g_?0",7J����(n%�;�4�(�%I���u���$]6Wa�a��PRG��@>ZH��B+>ZI��DK>ZJ��Gk>ZKӦ��������J5Ms/	�t�r��nt�,�4�%��Y
j�[J�IEM;z��g)�in*A7�RS��V���������=KUM;f[:=�r��t�,u5��%��Y
k��K���T�4���h)�i�2���H��6H��47��;h��in5A��R^��l�n�������Z
l�t�@֥¦M�	d]Jl�t���%56m�N��"�g���?��a.?��y�Q��)?Y�`U�0W�|���(C���o��B�kO�'����;>Q�0W��,J�ʓ��E?*���E?
溓0%��`�:	3��梓fDUl�N��M�IXr��`.9� >Uz�ޯ�~T�'a�T����$̞�;Xŏd.�o��x,p�od�˸�n�<�ٶ�'2�˹=\虀�z�2�˚�":�2�˜�nם	l����\�����>���%3Ț��k`J��}s�L��ѷ1�ĔltŜ4��v� ���샒d��4�Ȕ� �A	i��)Y�n����2S���[�t4����5��0�x�,FZ�d4[��o2�
墹&�o3b���ڙ��H�L4cB��&��"/�fk���% �ufFV����{fd��#&��ԏ�z�0͖>Y�������B�@s-�Ȏ�(�-0#���� ��K��o�rg��ae�8���ʟq�_=*�Ɓ�T�T�\�"�,ظ"�<�rER�4t�[G��8Н*�ʦю�����i��Ie�8Б+�ʩq�_8*�Ɓ�T�l«q�W$�Y�@W$�[�@W�H*�ƁnT��~�ݩ"��Xz�����"�,:RERz6t���4m�LI��8��*�ʶq��+�ʷq�+W$�q��^x+�F����[Y7��´kk��v�7��g!N�6g��Z3��l�W��օH��g07/�ȋ2h0w/������8�ڢ����_{4����&"��j�sC�Qi�`nd����i0�2��X5���v��,D���V"��j�sGCܑ�Y���!n�ڭ����dm�`nl��K�sgCܕ�a���!n�ڱ����em�`�o���l0w8D֥i�M�Cd]�6�[�ެmlz�����.��;k��kN�i��1��UI��9Љ��ʺs�3�$�w�@�R�l|�{�@W�	R����"H�wt�{ e�inA�.)I)�d�VJ����]�B���1X�p��O�w�L�pp���T�07�����0�������0�������0�����T�0w��� ���$ԕ�����d ������$ԕ�����$ԕ���M�I�?��a�4�+K�s�I�+K�s�I�+K�s�I�+K�s�I�+K��n���P�0����S��N�PW���F�fOIn�MFX�Jq��gfD	n��L(�s�� ��6�����6�e�"�8%�a��K+�6,}i�ԆE�/�Tڰh���B���Rg��x�S�̆M�	�NQ*6m'*9|�l�t�H ���p��6lzN��ذi9,+}���B^��p��R]�b�/��0���<��a�6�7xJZ��l��RY��k��RX��k��RW�b�/��հ��K+U5�}&�O�j��L��Vjj��L��VJj��L��(Es�	�RU���z��r���j��<nh1O3�+�4fʖ�`)�1�`���	��"����ԍ��F����јIt�+�H_���D4<�{����H������$4�8p,4�%u ����Td�5$�3��,")��@�T��w�H����= �R;c��E$�3���"����Ӏ���p� ,��a.;�eY���aɲ�|�HV\PX�E����o��	��C �eI`�*�2��'H��dA`eK�ʎ2�0O=�(�\5�7g�I)[��JR�W�|7��T��Y,�����pi`<�^�<�h����y
B��\��U6](w���87�^�Jhte��5���� �tD�͠6̟��}���0zN��c�JLt[�L6̅&p�ˆ�`�(�s��*P>v�D?�����~`����������
���;����`�=_]�Q+�������mm|{j���ꎴ��������~�ݢ���>�-5�=���4l~���Pi������!�4�������4��-w�C�i�ߎ?�=~4�=~�3px;z��g��v����4�oG��L�?��=~H3��=~(3����0�����]�G��L#AG��L|��ޫH�2����&������I�G��L|��ދH2��EO�1���������5�G�bL�<
�?��,�G-)�d���Α���#����	#���M��!@�0�{0fO�0ٴ`< >!�d��G-��dӀ1XH0��/�QK*0Y:�� �M�E`Y�/��/�QK�/�4_�U�ԗ̽�%ŗ̽�e��di]���Bz��x�ZRyɦ�",#%�dn�G-��d�v֧�]27]�Q�K斋P���K掋P���K憋P���K��?�O(.YZ�į�%K[��zKS��TRn��&~��Ԗ��._�[��]�����T�H-y>%�JKs+�h-�����Y�}�A�Rf�S���PY�i����Ȋ1^��;F����漇-%�r    <�7X*,�N��,�{7����W��cP�!��變l������񇸒k����m%����J���(+�9`;ǯ��
��򇮒?��+H-�d.�w�?��8��?��9���{������aݟ�`[��]�."է�)}R��*09����H�JsJ+���F+wJ��ņ�=��^�b�ҟ�<oE�aiPi�������$6,-*����"(6,M*�"�?�Ri.y�b�Ҧ�\<& ʧ�\�ņ�Q���
�8�gUq*} ��<�K"�a�Ui��b�Ҭ�\ ņ�[������*�[�k�Wi.f�b�Ұ�\�ņ�c��v�K�J�,�~hop��AJop��A*ol��A
ol�+t��5�S)�5���T��
	���F�JJsk�;�Rr�b:0[��[0Y��[�b��z[�b��r[�b��j[��Q�۲+u@(�-s�thRR[6�ҡ�D)m�4K�6%�e�,z͔Ζ�Y:��)�-s�t`Y�l���C�'E�l���Jc˦[:�"%�e�-��Rز�dyR`��-�<��e�.XV�Z6��a)u-s�t�5S�Z�r�����2�K�Q�Z6��aF�P�2wK�^3%�e�-����2wK�^3%�en�ӧT����a�����Y:�����M�t�=%�en���fU)j�~
�KhA-#��	���rP��!��{9�Lh5��5Bτ��%@%V.�=�͈���%QϙsN��K��3-���ur�(u�r9�B�aK�\*��i�\�~�h�r]��h�������	-W� �k�%r�h�ϴ���`����r�l?��Y.b��gZ=k����-ų�:���R;�%'�MKg�x��i�,��&4-��J-�����5����,W�>4���؈�E�\�;Ѵf�'��Ť,��3/�ܐ���yLh����Y�mH�z�}�6�m��΂>ʷ�VF��z�����s�y��]LҺ�<��.&�]o�`g�4��RO�G�½ޖ0	��}���v1I�zs��b����w�����`�,�ͅ��Ly؛���cgibo.c�o�����>u�6����2�|��*衳2�7o�w����\a�9K+{s�|�,��trߕf�V����@7{sl'�v��
�E'���et��Nڛ��v;߿�t]E�����Z�������ۉ�z�}k���K����(��/~��z��'��T`��d8���?�=�\\�C쮯��b��C�>a��f�҇�=����H���K1�ҁ�!2}3�z�T탾6��>�nm��-t������������bJ� Ja�Θ�I]�.��IY�6���*� WH��(��6���&� _/�!I@����O*p��O
p�#��؄'j)8��Rp����� ��� W8MK)��mp��J���,-� -���Rp����� G8HK��Mp��"���-5 x�ռF,$ ��- ��Z
 l�#���؎'h)�gq;������,��,�UԾ����,�)鿁5*�)��X�S�ܨF��pǋ%�g��Q��bD��p����7��:a��� g���i����J�o*1@MT����^�H�?�c�&����&�rSbi���`��7�4�/��\j/F�ҟ7`���s�`Y��M�`Y��M!��J�?W
��)��R��"5��0���$�\NfD*�Mi�)�7u^��-��\
���+�`c�T��l�=�?��<�������
��i?��a�(�����C��7 D��y�[�)���#L�U?��`����2L�M?�N�Y�!闗J�+H��ʧ��ˍ�����N�#?��r��j~�c��1�ܱ}䇖_��?�C�/wj ���;u���s� [���
�?d�\T6�*~.���?��������jy[J���	l�?�\�v�~.�[��>���=��|_���KP��}V�y�9J�����t��|�W�������ot�Ы�s$��f�]H��}27�.�*���G����bu\O���̊�q�N>��ߎ8�B~	�aJ��a_��teJ6��!��?��<��?��y�;��?���6xw3ڍN�����Α�[1�ǘ0���5�p*�>����5�K�1����B��@K��Ȏ���I�c�g�c�$d�F��Ff�2� #��9�%��Aߌ1�c�>�4�c�c��4�c<�`Iߑ9�x��>��#�5���nAL�u�������D�a��3��-��ģ�v���gM��½�nE2����Q���"Ab�Ϛ/�x��"�����K�t��{��\j�d�Y�%�P ��Ɵ5�#� ����� ��|g�P��]Y���D�ZR���/*[뵴�2�؆���8�:��.]S[<��B�㘫��5��]����mz��\�����d��u�Il��}�&#��6��2�w2�XlÝ��#�m4��ߎ��n�C�m4�|�w�b���{2!g����WL��3�|[=��5��?_�$bF�<^/O��H�����1���zy�@/���n�L���nmL���7�=�a�W���Qn�M�^A�����W�%�]|ÞA��w�~����o���Ӵ�a�o�;w�a�ٷ�Y�y�A����!YA�o�W�{��kA��6���__�u=9}�[<��F����ǻ�l�׭��8a�{>�љ����3��`_�u�DYOp�(��d��NV��r��:s���(7_v@��v��5�W���v���m��{��QZ�3�[3'3J����Uq���D�k�Ɯ�agR`��C��:�z�Qf�(,�'M�mp��{-Ӛڔ4ʊ�b��ޔtʊ�~��^�LQr�1��*��SE\���z��͛� [�X$3F���8���^1z�&��z�E�c,x�r;s���a���'}��R��]�y��Q������;UI����1n��g�H��~�O��<�_`%x�->�R�4[|��0��Z|ŧtO��א�v|�n�*�i���<}_`�	�m_�)I�����*E��d��<=_`�xZ,��Sr�7R�������'����+�5����0{R	���A�Z�'����:>qԅ��鉣�+�[���*+�[����B���Q�-�t�UM���Q��-�tYU�-�t�UA�w���z��+`U9�SP���j�gr�K��O����k	���/U���#ħ*	��/U��t���#TOS�*�u::p!��8�*���aBT�N7G�UB�n��TYA���V�* �c�����e=��/��K��	X�N��#�%�p?���_ҙC=D	.q��8�z����!3>$�@ʙC=З�-a���K����u��?m��p���uF�':]�yg8ef��ۉ�7z������':\�y�2Qx���8��g��#�p���B�,�MS�'� #V���'�o3b|��k W��������4���\1v�ij	9c��4�̌���2�G}|02R#�cFV�5�-L�L�v�[�~����.|-ڍ�F�S��� +�S�ј�$_�����A�Y�<�n��<��Q�[���>�}��"�܎�Ӿblg�1�³����������U�}��Ǵ�aW���1��7���L���˸a�o��KT�n�{�7���5k�-R)7�f��E�kd��$*�#�/�L� QC|:.|����G;���p�7ר8܃�ý*�O|��g�ۨ@?�-��p;��G�ዞn?ˁ�po�+��U��4Q~�s��#հ���zV�=Q{�탞n�T���/zV�/Ђ� ��5�B�����^��'�>�ڎ��>������t�6���u�;�
�3ڄ�?�����4��1�uh="]�����o|�k�1��1�u�=�J�_?�Ύ��U�X�k��Z�|{�����T�j�ȜVڎ��ec�6ߴvv���G������4[�����פֿ����E��R��s9Ц�r~��6�V���鵘_�NDw|�I$�;j�k)����^��dlp@mz-�����������zsv�"    r|{�鵊_�鵈__��!���ڲ�_�ʳ��k�M��z�M����J�W�������{�g�/FЦ��}����x_ߝT�+����^`%�ק��{ȪЧ���� �x�7t��������[�ߐE�zLykǞO�d����V%�:�`Q����NY ����Ny��s��������+N�uf�	���:s�`q�����Ny���jnzY���4�����?��A�=bq�`��7���?��p�S� �s�<�,9'�3=29'�#� |!.O���Wq��=2_��~u�xXC�_�2ި��~u�xx�*���፪<�W7��Q�~4*�|��>������+��y?f��kK)i��@5�OtEt��f/�Dۅ��J� Ϛ��.7z;&:i�z��|�W��46� �	�8��r%1%{��r�3%]Y���%+��r50%;�Nr9"#[�N5	 _S�����Ԕ���Sd9;>�[I,�a���Bg���[���g��3�/FOX���f������it����c��U��l��A�%9��s ɒ�y�+FG�Z-�c�3FW�ǵj�c���d�,��kd�X��s�%r�hT�%��AV* �R?�:+�F�n�c��H:��u&p�/�� `�;���W��?���@�<��>�� Pۅ���\w]�s��ߣ)uך�F���}�]k�~X䐇��=���a��6��_áO����	�$�
}<�_?.��h
>��]|��k4��L�h�|���Q�勜��3W!�|����W!k�Z8nU�Z��Y���$7v䕳VBaÁS�������;o��7�����F����n|��Ah�ύO%�fӵ����F�����Ɨ�:gk|n�}T�[��s��G��|n|��1��ύ��r������Z��>�\����s]��>'�[�L]��������Zυ�$"��zN	'����-8��W��K�qO>��v�S�CVoj}gă�|R�>$��ZO�� ��z6	�����&,s��� �e��ֻ�A�L?��� K����Z��!���zJ1�X����K���zH���Xő�h���8��?� �5���_L o��zw�ƪ7�ެ�;��ֻ��`�_��v*0ܣPh{�n�z?��A�����
(<�׳ދ5FY���o��d-�K��V��:7�
`Q���[B3�,DT�*�]�!�1�L��D��=�=dQ��s�l E��#4��D����e	��}���-��*k��*�\��U��a���>p�WY|�aJq������*K�TV8a�,<p�PYw����0����0�;xe�a��t�ʚ� w���%��+��8tV��v���V�哲�� ���s�ߣ�"���\�H�~m��=�g�=]�ѝji!}{��]�;c�6���£^1zލ��x�����.�b�C�Zx�R0�µ�>�}it��KN�o�r-<%&p�q-<��i��p֙O����ܘ}*!bsd�~�º�uI|l�Gn�\��I25H�OK���Ư�j����§�J5���5���	8�J����ߨ�u��=;�N=�#��E�7���],ѳ{BÑn�#?F�
G����{'�_�z6>Sg���=+�����=;\Þ�p�L�s-�F{|���iԗ����ڎ�Sg��6~���M��|�`E;�1��1�5��A���<�9|��Z+�k1��gzl0��k1,|�xDm�c1�p��üM�9��4�|F>��f粴|��ׇg�s�Z>�M_�a'_��O|�`uǬ�}$����}���K���+ߗ���	��/}O��\_z28hӗ�C��P�/����7i��"��'��7i�������?�����T���ʪ�X���|�&�^<���o��ŏ���MZ���ީJ�?��;Ui��|�&}^� ��ߤ͋���7���Ux�*M^�2N��ǋiᝪ�x�C�S�/~@�w���ů?�5��w��2[�����+P���.~d�"�6w�Ca��K=J�"��v�G�T�Q�.�(��0�إŨ�|]��X�Q�.uf�pu�G�𥕦.uf�P�P�.u�P�P�.�3<,�(G��a��K=f��;>��RCmi�R��<̞rs�~o�U
e�R�V��˥�.�=dQx��F��u^��(T١z�|�dաzS |�dѡ��=�8Us��TfycEɡ��wXp��P��kh��P��kh��P������P�F����P�}8�B�_�c�[��߄c�[�j�g��Y��Qi�
W,?
}u}?��0�/D�jh^���2x�{�E���x3��0�	/F�<���
À��Q`p�C�e}a�+^p�(/x�Ⱥ�.��~Hs�!e�z�in�&�����VC��՟�b�d	�?΄]�������u>}��QiV��c��՟�a�	�Y���(�a="��K9�zl]�א�a�'�x�Ub�5��t�E8���s)E8��R�p���+!���W:�5x�
��*���-E8aO�� ����Z�p�4�H5l`��\��_�p�w:J~p�;ޡk���-R|p`�xH������!�8��K�<8��^�(��.Р#u֠AG�l��:8�;X��� wz��4k��LRrp`dLRqp`#�KRpp�=dQz���!���q��a7��ǕW�f��^��+��DgD�e�� ] �?�%z;ц��b��'�"�;x�q����>�Mt�vb���h��'�f��.�&�M_h������f�_�ƍ��� �g��0��Ź�O{G�Po 7<pC����������3�p�G�z��%�}f�dn����=��=�3���������ƾS��#;���`�q���fp�{0�������`n���_����`n�'����`�����<�5��/�=�	�:�}�CA³��z+
=,���+�m�����k�J"���׽T�`J��UopJ'�sy&����Ot���I�4�< �ˉ�7z$��č���{m-��֑	cl'8����c�	��W}�1��1^��>Y1^�G���1��G���}�Afd��>�������t��dyW	A&�dY϶�ǰ����7�@S��}5�>�?(Yþ:G��A�B����23%;ȳw�1��Ӿ�lW��]���{��>(��nW��ݙ�5����h��!i��N�g}�X΄��n�����c����5�z�쏩9���:�*Dݏą�Z�-��W�j��/���ժ���9X
�Ր�~�ՐKQ?d�j��Q?T�j��Q?D�j�R?4�j(gE�iEݧ+�r�4CW�jտ�PV�!�V�7,a�JO�ίA�H�1+��K5��6,��S���Zjް�CJm�;�~(��`g���R���#�:j}�9p%�6�W��hD�o�U�x���ڀ�ՎG�JCm��z��$�����ox��C@m����^?��
���a�R>���C��i�p�R<m����N����?�����崑��%���x�5i�!�Q���cg�?$��!K��x���K��t<Β&�\�㱖�+��X�jJ���<��k���<�ә��CNk�:��!)���*���Ǳ#-/��GO�h}ɼ<�
����ǇD�?d��Q��y|���Cn*�����v)��Cv�si���<�[���دH��=.[X�*C���U�H��& ���c�V�!I�۞ �He�5�2<�IopZ��4���`K'8�`/��~І��_z/�h���h�г����l�n�ބ�U^O���'b���c���,0��M�an�H�6�0���l4]1� Jv�h?f��^SW��$<�bJ<�|P�@X�P2��j��B��a��Bh�)�h�R��Ҵ� �Am��sA�dI}��3$�dI��!�Ɣ�aS�b���d�e��V��g`d�~󁽊!X�5�	Ԭ<�e�&#������-F�+� �  /"�#���7�������i��YAb�b��v�Z�����b�zC���ی4:�x�8W��DK��:k�ЉX�>S�Z<v"*y��r�DT�L5g+۝�Kq���tސ�L���!���>(3����0Ӏ��H]�G���LM�q?T�_��eh|O�C�i��E�I�G��LM>r?���o^�1�*f���ŲЁ�R�i�?�YR�i��m�-��\A�RLN/T~(1��9\
1x墣�a�ƕ;)�4���_R���Do�~�0x�"��`p�!�S��jT_�
L�Q��LN��~�/�q]H�/�)���%)�Ov���z?�KR
􍖽%)�GT����y��,I)S�$KR�lB����������*4dWIJ���GSIJ�D���$叴P����O�Q���Z5�����F�~�t�X�h�N���QMv���qT��$)�d/I�WG��o��=���)�I����kP6�$���q�>�T"zdI*���$�uI*���$�Z��T>�|�A��G����$��7�?^������<@�w���Y�|�?�	A������?��5��z�?���o?����د�����7�X����;�,�x�?�)w-����p���@�?�|�?З�^?V��%��\>����W/�k���K"�l���D��?�u)�8>�u-ҧ�:u1��a����5q�y|�L�z���2���Wˤ�H,O��$y|��d:ֳi�*J�p|�cʪd�UJL�%S��l�u�>��ua2m� ��L��pU�L�>�=em2�T���4�
c��:��Wk�,O�x|������rߩ,P&�D��
e�9ċ*Q��ѭ.k�)��nY�L�VV)S�j�e����������=�       �      x������ � �      �   `   x���;�0Cgs�O	ܥ�?G��%�PՓ�l��J��p�H�TJ��@�C	{G1�&Q�" ,!���U(C�j��i��PB��}B��� �.&�q�/     