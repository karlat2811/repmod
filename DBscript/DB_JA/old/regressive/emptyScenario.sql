\c planning

-- DELETE FROM public.txcalendar ;
-- DELETE FROM public.txcalendarday ;

-- DELETE FROM public.mdparameter ;
-- DELETE FROM public.mdprocess ;
-- DELETE FROM public.mdbreed ;
-- DELETE FROM public.mdproduct ;


DELETE FROM public.txholiday ;
DELETE FROM public.txscenarioformula ;
DELETE FROM public.txscenarioparameterday ;
DELETE FROM public.txscenarioprocess ;
DELETE FROM public.txeggs_required ;
DELETE FROM public.txeggs_planning ;
DELETE FROM public.txeggs_storage;

DELETE FROM public.mdscenario ;
