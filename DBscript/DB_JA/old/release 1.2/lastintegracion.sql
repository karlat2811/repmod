PGDMP     )    (    	        
    v            last    10.3    10.3     �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            �           1262    27342    last    DATABASE     �   CREATE DATABASE last WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_United States.1252' LC_CTYPE = 'English_United States.1252';
    DROP DATABASE last;
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            �           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3                        3079    12924    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            �           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1259    27343    abaTimeUnit_id_seq    SEQUENCE     �   CREATE SEQUENCE public."abaTimeUnit_id_seq"
    START WITH 2
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 +   DROP SEQUENCE public."abaTimeUnit_id_seq";
       public       postgres    false    3            �            1259    27345    aba_breeds_and_stages_id_seq    SEQUENCE     �   CREATE SEQUENCE public.aba_breeds_and_stages_id_seq
    START WITH 8
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 3   DROP SEQUENCE public.aba_breeds_and_stages_id_seq;
       public       postgres    false    3            �            1259    27347    aba_breeds_and_stages    TABLE       CREATE TABLE public.aba_breeds_and_stages (
    id integer DEFAULT nextval('public.aba_breeds_and_stages_id_seq'::regclass) NOT NULL,
    code character varying(100),
    name character varying(100),
    id_aba_consumption_and_mortality integer,
    id_process integer
);
 )   DROP TABLE public.aba_breeds_and_stages;
       public         postgres    false    197    3            �           0    0    TABLE aba_breeds_and_stages    COMMENT     o   COMMENT ON TABLE public.aba_breeds_and_stages IS 'Relaciona los procesos de ARP con el consumo y mortalidad ';
            public       postgres    false    198            �           0    0    COLUMN aba_breeds_and_stages.id    COMMENT     o   COMMENT ON COLUMN public.aba_breeds_and_stages.id IS 'Id de la relacion entre proceso y consumo y mortalidad';
            public       postgres    false    198            �           0    0 !   COLUMN aba_breeds_and_stages.code    COMMENT     u   COMMENT ON COLUMN public.aba_breeds_and_stages.code IS 'Codigo de la relacion entre proceso y consumo y mortalidad';
            public       postgres    false    198            �           0    0 !   COLUMN aba_breeds_and_stages.name    COMMENT     u   COMMENT ON COLUMN public.aba_breeds_and_stages.name IS 'Nombre de la relacion entre proceso y consumo y mortalidad';
            public       postgres    false    198            �           0    0 =   COLUMN aba_breeds_and_stages.id_aba_consumption_and_mortality    COMMENT     �   COMMENT ON COLUMN public.aba_breeds_and_stages.id_aba_consumption_and_mortality IS 'Id de tabla aba_consumption_and_mortality (FK)';
            public       postgres    false    198            �           0    0 '   COLUMN aba_breeds_and_stages.id_process    COMMENT     Y   COMMENT ON COLUMN public.aba_breeds_and_stages.id_process IS 'Id de la tabla mdprocess';
            public       postgres    false    198            �            1259    27351 $   aba_consumption_and_mortality_id_seq    SEQUENCE     �   CREATE SEQUENCE public.aba_consumption_and_mortality_id_seq
    START WITH 8
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 ;   DROP SEQUENCE public.aba_consumption_and_mortality_id_seq;
       public       postgres    false    3            �            1259    27353    aba_consumption_and_mortality    TABLE     $  CREATE TABLE public.aba_consumption_and_mortality (
    id integer DEFAULT nextval('public.aba_consumption_and_mortality_id_seq'::regclass) NOT NULL,
    code character varying(100),
    name character varying(100),
    id_breed integer,
    id_stage integer,
    id_aba_time_unit integer
);
 1   DROP TABLE public.aba_consumption_and_mortality;
       public         postgres    false    199    3            �           0    0 #   TABLE aba_consumption_and_mortality    COMMENT     �   COMMENT ON TABLE public.aba_consumption_and_mortality IS 'Almacena la información del consumo y mortalidad asociados a la combinacion de raza y etapa';
            public       postgres    false    200            �           0    0 '   COLUMN aba_consumption_and_mortality.id    COMMENT     �   COMMENT ON COLUMN public.aba_consumption_and_mortality.id IS 'Id de los datos de consumo y mortalidad asociados a una raza y una etapa';
            public       postgres    false    200            �           0    0 )   COLUMN aba_consumption_and_mortality.code    COMMENT     �   COMMENT ON COLUMN public.aba_consumption_and_mortality.code IS 'Codigo de los datos de consumo y mortalidad asociados a una raza y una etapa ';
            public       postgres    false    200            �           0    0 )   COLUMN aba_consumption_and_mortality.name    COMMENT     �   COMMENT ON COLUMN public.aba_consumption_and_mortality.name IS 'Nombre de los datos de consumo y mortalidad asociados a una raza y una etapa';
            public       postgres    false    200            �           0    0 -   COLUMN aba_consumption_and_mortality.id_breed    COMMENT     �   COMMENT ON COLUMN public.aba_consumption_and_mortality.id_breed IS 'Id de la raza asociada a los datos de consumo y mortalidad';
            public       postgres    false    200            �           0    0 -   COLUMN aba_consumption_and_mortality.id_stage    COMMENT     �   COMMENT ON COLUMN public.aba_consumption_and_mortality.id_stage IS 'id de la etapa en la que se encuentran los datos de consumo y mortalidad ';
            public       postgres    false    200            �           0    0 5   COLUMN aba_consumption_and_mortality.id_aba_time_unit    COMMENT     �   COMMENT ON COLUMN public.aba_consumption_and_mortality.id_aba_time_unit IS 'Id de la unidad de tiempo utilizada en los datos cargados en consumo y mortalidad (dias o semanas)';
            public       postgres    false    200            �            1259    27357 +   aba_consumption_and_mortality_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.aba_consumption_and_mortality_detail_id_seq
    START WITH 203
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 B   DROP SEQUENCE public.aba_consumption_and_mortality_detail_id_seq;
       public       postgres    false    3            �            1259    27359 $   aba_consumption_and_mortality_detail    TABLE     =  CREATE TABLE public.aba_consumption_and_mortality_detail (
    id integer DEFAULT nextval('public.aba_consumption_and_mortality_detail_id_seq'::regclass) NOT NULL,
    id_aba_consumption_and_mortality integer NOT NULL,
    time_unit_number integer,
    consumption double precision,
    mortality double precision
);
 8   DROP TABLE public.aba_consumption_and_mortality_detail;
       public         postgres    false    201    3            �           0    0 *   TABLE aba_consumption_and_mortality_detail    COMMENT     �   COMMENT ON TABLE public.aba_consumption_and_mortality_detail IS 'Almacena los detalles para la unidad de tiempo asociada a una determinada agrupación de consumo y mortalidad ';
            public       postgres    false    202            �           0    0 .   COLUMN aba_consumption_and_mortality_detail.id    COMMENT     �   COMMENT ON COLUMN public.aba_consumption_and_mortality_detail.id IS 'Id de los detalles para la unidad de tiempo asociada a una determinada agrupación de consumo y mortalidad ';
            public       postgres    false    202            �           0    0 L   COLUMN aba_consumption_and_mortality_detail.id_aba_consumption_and_mortality    COMMENT     �   COMMENT ON COLUMN public.aba_consumption_and_mortality_detail.id_aba_consumption_and_mortality IS 'Id de la agrupación de consumo y mortalidad asociada';
            public       postgres    false    202            �           0    0 <   COLUMN aba_consumption_and_mortality_detail.time_unit_number    COMMENT     �   COMMENT ON COLUMN public.aba_consumption_and_mortality_detail.time_unit_number IS 'Indica la unidad de tiempo asociada a la agrupacion de consumo y mortalidad';
            public       postgres    false    202            �           0    0 7   COLUMN aba_consumption_and_mortality_detail.consumption    COMMENT     �   COMMENT ON COLUMN public.aba_consumption_and_mortality_detail.consumption IS 'Consumo asociado a una determinada agrupación de consumo y mortalidad ';
            public       postgres    false    202            �           0    0 5   COLUMN aba_consumption_and_mortality_detail.mortality    COMMENT     �   COMMENT ON COLUMN public.aba_consumption_and_mortality_detail.mortality IS 'Mortalidad asociada a una determinada agrupación de consumo y mortalidad ';
            public       postgres    false    202            �            1259    27363    aba_elements_id_seq    SEQUENCE     �   CREATE SEQUENCE public.aba_elements_id_seq
    START WITH 22
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 *   DROP SEQUENCE public.aba_elements_id_seq;
       public       postgres    false    3            �            1259    27365    aba_elements    TABLE       CREATE TABLE public.aba_elements (
    id integer DEFAULT nextval('public.aba_elements_id_seq'::regclass) NOT NULL,
    code character varying(100),
    name character varying(100),
    id_aba_element_property integer,
    equivalent_quantity double precision
);
     DROP TABLE public.aba_elements;
       public         postgres    false    203    3            �           0    0    TABLE aba_elements    COMMENT     T   COMMENT ON TABLE public.aba_elements IS 'Almacena los datos de los macroelementos';
            public       postgres    false    204            �           0    0    COLUMN aba_elements.id    COMMENT     D   COMMENT ON COLUMN public.aba_elements.id IS 'Id del macroelemento';
            public       postgres    false    204            �           0    0    COLUMN aba_elements.code    COMMENT     J   COMMENT ON COLUMN public.aba_elements.code IS 'Codigo del macroelemento';
            public       postgres    false    204            �           0    0    COLUMN aba_elements.name    COMMENT     J   COMMENT ON COLUMN public.aba_elements.name IS 'Nombre del macroelemento';
            public       postgres    false    204            �           0    0 +   COLUMN aba_elements.id_aba_element_property    COMMENT     q   COMMENT ON COLUMN public.aba_elements.id_aba_element_property IS 'Id de la propiedad asociada al macroelemento';
            public       postgres    false    204            �           0    0 '   COLUMN aba_elements.equivalent_quantity    COMMENT     �   COMMENT ON COLUMN public.aba_elements.equivalent_quantity IS 'Cantidad de la propiedad asociada al macroelemento con el fin de realizar equivalencias';
            public       postgres    false    204            �            1259    27369 &   aba_elements_and_concentrations_id_seq    SEQUENCE     �   CREATE SEQUENCE public.aba_elements_and_concentrations_id_seq
    START WITH 105
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 =   DROP SEQUENCE public.aba_elements_and_concentrations_id_seq;
       public       postgres    false    3            �            1259    27371    aba_elements_and_concentrations    TABLE     k  CREATE TABLE public.aba_elements_and_concentrations (
    id integer DEFAULT nextval('public.aba_elements_and_concentrations_id_seq'::regclass) NOT NULL,
    id_aba_element integer,
    id_aba_formulation integer,
    proportion double precision,
    id_element_equivalent integer,
    id_aba_element_property integer,
    equivalent_quantity double precision
);
 3   DROP TABLE public.aba_elements_and_concentrations;
       public         postgres    false    205    3            �           0    0 %   TABLE aba_elements_and_concentrations    COMMENT     x   COMMENT ON TABLE public.aba_elements_and_concentrations IS 'Asocia una formula con los macroelementos que la componen';
            public       postgres    false    206            �           0    0 )   COLUMN aba_elements_and_concentrations.id    COMMENT     �   COMMENT ON COLUMN public.aba_elements_and_concentrations.id IS 'Id de la asociación entre una formula con los macroelementos que la componen';
            public       postgres    false    206            �           0    0 5   COLUMN aba_elements_and_concentrations.id_aba_element    COMMENT     g   COMMENT ON COLUMN public.aba_elements_and_concentrations.id_aba_element IS 'Id del elemento asociado';
            public       postgres    false    206            �           0    0 9   COLUMN aba_elements_and_concentrations.id_aba_formulation    COMMENT     l   COMMENT ON COLUMN public.aba_elements_and_concentrations.id_aba_formulation IS 'Id de la formula asociado';
            public       postgres    false    206            �           0    0 1   COLUMN aba_elements_and_concentrations.proportion    COMMENT     x   COMMENT ON COLUMN public.aba_elements_and_concentrations.proportion IS 'Proporción del elemento dentro de la formula';
            public       postgres    false    206            �            1259    27375    aba_elements_properties_id_seq    SEQUENCE     �   CREATE SEQUENCE public.aba_elements_properties_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 5   DROP SEQUENCE public.aba_elements_properties_id_seq;
       public       postgres    false    3            �            1259    27377    aba_elements_properties    TABLE     �   CREATE TABLE public.aba_elements_properties (
    id integer DEFAULT nextval('public.aba_elements_properties_id_seq'::regclass) NOT NULL,
    code character varying(100),
    name character varying(100)
);
 +   DROP TABLE public.aba_elements_properties;
       public         postgres    false    207    3            �           0    0    TABLE aba_elements_properties    COMMENT     �   COMMENT ON TABLE public.aba_elements_properties IS 'Almacena las propiedades que pueden llegar a tener los macroelementos para realizar la equivalencia';
            public       postgres    false    208            �           0    0 !   COLUMN aba_elements_properties.id    COMMENT     Z   COMMENT ON COLUMN public.aba_elements_properties.id IS 'Id de la propiedad del elemento';
            public       postgres    false    208            �           0    0 #   COLUMN aba_elements_properties.code    COMMENT     _   COMMENT ON COLUMN public.aba_elements_properties.code IS 'Codigode la propiedad del elemento';
            public       postgres    false    208            �           0    0 #   COLUMN aba_elements_properties.name    COMMENT     `   COMMENT ON COLUMN public.aba_elements_properties.name IS 'Nombre de la propiedad del elemento';
            public       postgres    false    208            �            1259    27381    aba_formulation_id_seq    SEQUENCE     �   CREATE SEQUENCE public.aba_formulation_id_seq
    START WITH 68
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 -   DROP SEQUENCE public.aba_formulation_id_seq;
       public       postgres    false    3            �            1259    27383    aba_formulation    TABLE     �   CREATE TABLE public.aba_formulation (
    id integer DEFAULT nextval('public.aba_formulation_id_seq'::regclass) NOT NULL,
    code character varying(100),
    name character varying(100)
);
 #   DROP TABLE public.aba_formulation;
       public         postgres    false    209    3            �           0    0    TABLE aba_formulation    COMMENT     g   COMMENT ON TABLE public.aba_formulation IS 'Almacena los datos del alimento balanceado para animales';
            public       postgres    false    210            �           0    0    COLUMN aba_formulation.id    COMMENT     [   COMMENT ON COLUMN public.aba_formulation.id IS 'Id del alimento balanceado para animales';
            public       postgres    false    210            �           0    0    COLUMN aba_formulation.code    COMMENT     a   COMMENT ON COLUMN public.aba_formulation.code IS 'Codigo del alimento balanceado para animales';
            public       postgres    false    210            �           0    0    COLUMN aba_formulation.name    COMMENT     a   COMMENT ON COLUMN public.aba_formulation.name IS 'Nombre del alimento balanceado para animales';
            public       postgres    false    210            �            1259    27387    aba_results_id_seq    SEQUENCE     �   CREATE SEQUENCE public.aba_results_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 )   DROP SEQUENCE public.aba_results_id_seq;
       public       postgres    false    3            �            1259    27389    aba_results    TABLE     �   CREATE TABLE public.aba_results (
    id integer DEFAULT nextval('public.aba_results_id_seq'::regclass) NOT NULL,
    id_aba_element integer,
    quantity double precision
);
    DROP TABLE public.aba_results;
       public         postgres    false    211    3            �            1259    27393 &   aba_stages_of_breeds_and_stages_id_seq    SEQUENCE     �   CREATE SEQUENCE public.aba_stages_of_breeds_and_stages_id_seq
    START WITH 24
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 =   DROP SEQUENCE public.aba_stages_of_breeds_and_stages_id_seq;
       public       postgres    false    3            �            1259    27395    aba_stages_of_breeds_and_stages    TABLE       CREATE TABLE public.aba_stages_of_breeds_and_stages (
    id integer DEFAULT nextval('public.aba_stages_of_breeds_and_stages_id_seq'::regclass) NOT NULL,
    id_aba_breeds_and_stages integer,
    id_formulation integer,
    name character varying(100),
    duration integer
);
 3   DROP TABLE public.aba_stages_of_breeds_and_stages;
       public         postgres    false    213    3            �           0    0 %   TABLE aba_stages_of_breeds_and_stages    COMMENT     �   COMMENT ON TABLE public.aba_stages_of_breeds_and_stages IS 'Almacena las fases asociadas a los animales considerados en la tabla de consumo y mortalidad y asocia el alimento a ser proporcionado en dicha fase';
            public       postgres    false    214            �           0    0 )   COLUMN aba_stages_of_breeds_and_stages.id    COMMENT     �   COMMENT ON COLUMN public.aba_stages_of_breeds_and_stages.id IS 'Id de la fase asociadas a los animales considerados en la tabla de consumo y mortalidad ';
            public       postgres    false    214            �           0    0 ?   COLUMN aba_stages_of_breeds_and_stages.id_aba_breeds_and_stages    COMMENT     �   COMMENT ON COLUMN public.aba_stages_of_breeds_and_stages.id_aba_breeds_and_stages IS 'Id de la tabla que almacena la relacion entre proceso y consumo y mortalidad';
            public       postgres    false    214            �           0    0 5   COLUMN aba_stages_of_breeds_and_stages.id_formulation    COMMENT     �   COMMENT ON COLUMN public.aba_stages_of_breeds_and_stages.id_formulation IS 'Id del alimento balanceado para animales asociado a la fase';
            public       postgres    false    214            �           0    0 +   COLUMN aba_stages_of_breeds_and_stages.name    COMMENT     �   COMMENT ON COLUMN public.aba_stages_of_breeds_and_stages.name IS 'Nombre de la fase asociadas a los animales considerados en la tabla de consumo y mortalidad ';
            public       postgres    false    214            �           0    0 /   COLUMN aba_stages_of_breeds_and_stages.duration    COMMENT     �   COMMENT ON COLUMN public.aba_stages_of_breeds_and_stages.duration IS 'Duracion de la fase asociadas a los animales considerados en la tabla de consumo y mortalidad ';
            public       postgres    false    214            �            1259    27399    aba_time_unit    TABLE     �   CREATE TABLE public.aba_time_unit (
    id integer DEFAULT nextval('public."abaTimeUnit_id_seq"'::regclass) NOT NULL,
    singular_name character varying(100),
    plural_name character varying(100)
);
 !   DROP TABLE public.aba_time_unit;
       public         postgres    false    196    3            �           0    0    TABLE aba_time_unit    COMMENT     L   COMMENT ON TABLE public.aba_time_unit IS 'Almacena las unidades de tiempo';
            public       postgres    false    215            �           0    0    COLUMN aba_time_unit.id    COMMENT     K   COMMENT ON COLUMN public.aba_time_unit.id IS 'Id de la unidad de tiempo
';
            public       postgres    false    215            �           0    0 "   COLUMN aba_time_unit.singular_name    COMMENT     e   COMMENT ON COLUMN public.aba_time_unit.singular_name IS 'Nombre en singular de la unidad de tiempo';
            public       postgres    false    215            �           0    0     COLUMN aba_time_unit.plural_name    COMMENT     a   COMMENT ON COLUMN public.aba_time_unit.plural_name IS 'Nombre en plural de la unidad de tiempo';
            public       postgres    false    215            �            1259    27403    availability_shed_id_seq    SEQUENCE     �   CREATE SEQUENCE public.availability_shed_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.availability_shed_id_seq;
       public       postgres    false    3            �            1259    27405    base_day_id_seq    SEQUENCE     x   CREATE SEQUENCE public.base_day_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.base_day_id_seq;
       public       postgres    false    3            �            1259    27407    breed_id_seq    SEQUENCE     u   CREATE SEQUENCE public.breed_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.breed_id_seq;
       public       postgres    false    3            �            1259    27409    broiler_detail_id_seq    SEQUENCE     ~   CREATE SEQUENCE public.broiler_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.broiler_detail_id_seq;
       public       postgres    false    3            �            1259    27411    broiler_id_seq    SEQUENCE     w   CREATE SEQUENCE public.broiler_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.broiler_id_seq;
       public       postgres    false    3            �            1259    27413    broiler_product_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.broiler_product_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public.broiler_product_detail_id_seq;
       public       postgres    false    3            �            1259    27415    broiler_product_id_seq    SEQUENCE        CREATE SEQUENCE public.broiler_product_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.broiler_product_id_seq;
       public       postgres    false    3            �            1259    27417    broilereviction_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.broilereviction_detail_id_seq
    START WITH 124
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public.broilereviction_detail_id_seq;
       public       postgres    false    3            �            1259    27419    broilereviction_id_seq    SEQUENCE     �   CREATE SEQUENCE public.broilereviction_id_seq
    START WITH 70
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.broilereviction_id_seq;
       public       postgres    false    3            �            1259    27421    brooder_id_seq    SEQUENCE     w   CREATE SEQUENCE public.brooder_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.brooder_id_seq;
       public       postgres    false    3            �            1259    27423    brooder_machines_id_seq    SEQUENCE     �   CREATE SEQUENCE public.brooder_machines_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.brooder_machines_id_seq;
       public       postgres    false    3            �            1259    27425    calendar_day_id_seq    SEQUENCE     |   CREATE SEQUENCE public.calendar_day_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.calendar_day_id_seq;
       public       postgres    false    3            �            1259    27427    calendar_id_seq    SEQUENCE     x   CREATE SEQUENCE public.calendar_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.calendar_id_seq;
       public       postgres    false    3            �            1259    27429    center_id_seq    SEQUENCE     v   CREATE SEQUENCE public.center_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.center_id_seq;
       public       postgres    false    3            �            1259    27431    egg_planning_id_seq    SEQUENCE     |   CREATE SEQUENCE public.egg_planning_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.egg_planning_id_seq;
       public       postgres    false    3            �            1259    27433    egg_required_id_seq    SEQUENCE     |   CREATE SEQUENCE public.egg_required_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.egg_required_id_seq;
       public       postgres    false    3            �            1259    27435    eggs_storage_id_seq    SEQUENCE     |   CREATE SEQUENCE public.eggs_storage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.eggs_storage_id_seq;
       public       postgres    false    3            �            1259    27437    farm_id_seq    SEQUENCE     t   CREATE SEQUENCE public.farm_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE public.farm_id_seq;
       public       postgres    false    3            �            1259    27439    farm_type_id_seq    SEQUENCE     y   CREATE SEQUENCE public.farm_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.farm_type_id_seq;
       public       postgres    false    3            �            1259    27441    holiday_id_seq    SEQUENCE     w   CREATE SEQUENCE public.holiday_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.holiday_id_seq;
       public       postgres    false    3            �            1259    27443    housing_way_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.housing_way_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.housing_way_detail_id_seq;
       public       postgres    false    3            �            1259    27445    housing_way_id_seq    SEQUENCE     {   CREATE SEQUENCE public.housing_way_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.housing_way_id_seq;
       public       postgres    false    3            �            1259    27447    incubator_id_seq    SEQUENCE     y   CREATE SEQUENCE public.incubator_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.incubator_id_seq;
       public       postgres    false    3            �            1259    27449    incubator_plant_id_seq    SEQUENCE        CREATE SEQUENCE public.incubator_plant_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.incubator_plant_id_seq;
       public       postgres    false    3            �            1259    27451    industry_id_seq    SEQUENCE     x   CREATE SEQUENCE public.industry_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.industry_id_seq;
       public       postgres    false    3            �            1259    27453    line_id_seq    SEQUENCE     t   CREATE SEQUENCE public.line_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE public.line_id_seq;
       public       postgres    false    3            �            1259    27455    lot_eggs_id_seq    SEQUENCE     x   CREATE SEQUENCE public.lot_eggs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.lot_eggs_id_seq;
       public       postgres    false    3            �            1259    27457    lot_fattening_id_seq    SEQUENCE     }   CREATE SEQUENCE public.lot_fattening_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.lot_fattening_id_seq;
       public       postgres    false    3            �            1259    27459 
   lot_id_seq    SEQUENCE     s   CREATE SEQUENCE public.lot_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 !   DROP SEQUENCE public.lot_id_seq;
       public       postgres    false    3            �            1259    27461    lot_liftbreeding_id_seq    SEQUENCE     �   CREATE SEQUENCE public.lot_liftbreeding_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.lot_liftbreeding_id_seq;
       public       postgres    false    3            �            1259    27463     mdapplication_application_id_seq    SEQUENCE     �   CREATE SEQUENCE public.mdapplication_application_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999999999999
    CACHE 1;
 7   DROP SEQUENCE public.mdapplication_application_id_seq;
       public       postgres    false    3            �            1259    27465    mdapplication    TABLE     �   CREATE TABLE public.mdapplication (
    application_id integer DEFAULT nextval('public.mdapplication_application_id_seq'::regclass) NOT NULL,
    application_name character varying(30) NOT NULL,
    type character varying(20)
);
 !   DROP TABLE public.mdapplication;
       public         postgres    false    246    3            �           0    0    TABLE mdapplication    COMMENT     X   COMMENT ON TABLE public.mdapplication IS 'Almacena la informacion de las aplicaciones';
            public       postgres    false    247            �           0    0 #   COLUMN mdapplication.application_id    COMMENT     P   COMMENT ON COLUMN public.mdapplication.application_id IS 'Id de la aplicació';
            public       postgres    false    247            �           0    0 %   COLUMN mdapplication.application_name    COMMENT     W   COMMENT ON COLUMN public.mdapplication.application_name IS 'Nombre de la aplicación';
            public       postgres    false    247            �           0    0    COLUMN mdapplication.type    COMMENT     W   COMMENT ON COLUMN public.mdapplication.type IS 'A qué tipo pertenece la aplicación';
            public       postgres    false    247            �            1259    27469    mdapplication_rol_id_seq    SEQUENCE     �   CREATE SEQUENCE public.mdapplication_rol_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9999999999999
    CACHE 1;
 /   DROP SEQUENCE public.mdapplication_rol_id_seq;
       public       postgres    false    3            �            1259    27471    mdapplication_rol    TABLE     �   CREATE TABLE public.mdapplication_rol (
    id integer DEFAULT nextval('public.mdapplication_rol_id_seq'::regclass) NOT NULL,
    application_id integer NOT NULL,
    rol_id integer NOT NULL
);
 %   DROP TABLE public.mdapplication_rol;
       public         postgres    false    248    3            �           0    0    TABLE mdapplication_rol    COMMENT     _   COMMENT ON TABLE public.mdapplication_rol IS 'Contiene los id de aplicación y los id de rol';
            public       postgres    false    249            �           0    0    COLUMN mdapplication_rol.id    COMMENT     \   COMMENT ON COLUMN public.mdapplication_rol.id IS 'Id la combinacion de aplicación y rol ';
            public       postgres    false    249            �           0    0 '   COLUMN mdapplication_rol.application_id    COMMENT     `   COMMENT ON COLUMN public.mdapplication_rol.application_id IS 'Identificador de la aplicación';
            public       postgres    false    249            �           0    0    COLUMN mdapplication_rol.rol_id    COMMENT     N   COMMENT ON COLUMN public.mdapplication_rol.rol_id IS 'Identificador del rol';
            public       postgres    false    249            �            1259    27475    mdbreed    TABLE     �   CREATE TABLE public.mdbreed (
    breed_id integer DEFAULT nextval('public.breed_id_seq'::regclass) NOT NULL,
    code character varying(20) NOT NULL,
    name character varying(45) NOT NULL
);
    DROP TABLE public.mdbreed;
       public         postgres    false    218    3            �           0    0    TABLE mdbreed    COMMENT     U   COMMENT ON TABLE public.mdbreed IS 'Tabla donde se almacenan las razas de las aves';
            public       postgres    false    250            �           0    0    COLUMN mdbreed.breed_id    COMMENT     >   COMMENT ON COLUMN public.mdbreed.breed_id IS 'Id de la raza';
            public       postgres    false    250            �           0    0    COLUMN mdbreed.code    COMMENT     >   COMMENT ON COLUMN public.mdbreed.code IS 'Codigo de la raza';
            public       postgres    false    250            �           0    0    COLUMN mdbreed.name    COMMENT     >   COMMENT ON COLUMN public.mdbreed.name IS 'Nombre de la Raza';
            public       postgres    false    250            �            1259    27479    mdbroiler_product    TABLE     �   CREATE TABLE public.mdbroiler_product (
    broiler_product_id integer DEFAULT nextval('public.broiler_product_id_seq'::regclass) NOT NULL,
    name character varying(45) NOT NULL,
    days_eviction integer,
    weight double precision
);
 %   DROP TABLE public.mdbroiler_product;
       public         postgres    false    222    3            �           0    0    TABLE mdbroiler_product    COMMENT     w   COMMENT ON TABLE public.mdbroiler_product IS 'Almacena los productos de salida de la etapa de engorda hacia desalojo';
            public       postgres    false    251            �           0    0 +   COLUMN mdbroiler_product.broiler_product_id    COMMENT     ^   COMMENT ON COLUMN public.mdbroiler_product.broiler_product_id IS 'Id de producto de engorde';
            public       postgres    false    251            �           0    0    COLUMN mdbroiler_product.name    COMMENT     T   COMMENT ON COLUMN public.mdbroiler_product.name IS 'Nombre de producto de engorde';
            public       postgres    false    251            �           0    0 &   COLUMN mdbroiler_product.days_eviction    COMMENT     y   COMMENT ON COLUMN public.mdbroiler_product.days_eviction IS 'Días necesarios para el desalojo del producto de engorde';
            public       postgres    false    251            �           0    0    COLUMN mdbroiler_product.weight    COMMENT     b   COMMENT ON COLUMN public.mdbroiler_product.weight IS 'Peso estimado del producto para su salida';
            public       postgres    false    251            �            1259    27483 
   mdfarmtype    TABLE     �   CREATE TABLE public.mdfarmtype (
    farm_type_id integer DEFAULT nextval('public.farm_type_id_seq'::regclass) NOT NULL,
    name character varying(45) NOT NULL
);
    DROP TABLE public.mdfarmtype;
       public         postgres    false    234    3            �           0    0    TABLE mdfarmtype    COMMENT     D   COMMENT ON TABLE public.mdfarmtype IS 'Define los tipos de granja';
            public       postgres    false    252            �           0    0    COLUMN mdfarmtype.farm_type_id    COMMENT     L   COMMENT ON COLUMN public.mdfarmtype.farm_type_id IS 'Id de tipo de granja';
            public       postgres    false    252            �           0    0    COLUMN mdfarmtype.name    COMMENT     O   COMMENT ON COLUMN public.mdfarmtype.name IS 'Nombre de la etapa de la granja';
            public       postgres    false    252            �            1259    27487    measure_id_seq    SEQUENCE     w   CREATE SEQUENCE public.measure_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.measure_id_seq;
       public       postgres    false    3            �            1259    27489 	   mdmeasure    TABLE     $  CREATE TABLE public.mdmeasure (
    measure_id integer DEFAULT nextval('public.measure_id_seq'::regclass) NOT NULL,
    name character varying(10) NOT NULL,
    abbreviation character varying(5) NOT NULL,
    originvalue double precision,
    valuekg double precision,
    is_unit boolean
);
    DROP TABLE public.mdmeasure;
       public         postgres    false    253    3            �           0    0    TABLE mdmeasure    COMMENT     _   COMMENT ON TABLE public.mdmeasure IS 'Almacena las medidas a utilizar en las planificaciones';
            public       postgres    false    254            �           0    0    COLUMN mdmeasure.measure_id    COMMENT     D   COMMENT ON COLUMN public.mdmeasure.measure_id IS 'Id de la medida';
            public       postgres    false    254            �           0    0    COLUMN mdmeasure.name    COMMENT     B   COMMENT ON COLUMN public.mdmeasure.name IS 'Nombre de la medida';
            public       postgres    false    254            �           0    0    COLUMN mdmeasure.abbreviation    COMMENT     O   COMMENT ON COLUMN public.mdmeasure.abbreviation IS 'Abreviatura de la medida';
            public       postgres    false    254            �           0    0    COLUMN mdmeasure.originvalue    COMMENT     Q   COMMENT ON COLUMN public.mdmeasure.originvalue IS 'Valor original de la medida';
            public       postgres    false    254            �           0    0    COLUMN mdmeasure.valuekg    COMMENT     R   COMMENT ON COLUMN public.mdmeasure.valuekg IS 'Valor en Kilogramos de la medida';
            public       postgres    false    254            �            1259    27493    parameter_id_seq    SEQUENCE     y   CREATE SEQUENCE public.parameter_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.parameter_id_seq;
       public       postgres    false    3                        1259    27495    mdparameter    TABLE     '  CREATE TABLE public.mdparameter (
    parameter_id integer DEFAULT nextval('public.parameter_id_seq'::regclass) NOT NULL,
    description character varying(250) NOT NULL,
    type character varying(10),
    measure_id integer,
    process_id integer,
    name character varying(250) NOT NULL
);
    DROP TABLE public.mdparameter;
       public         postgres    false    255    3            �           0    0    TABLE mdparameter    COMMENT     �   COMMENT ON TABLE public.mdparameter IS 'Almacena la definición de los parámetros a utilizar en la planificación regresiva junto a sus respectivas características';
            public       postgres    false    256            �           0    0    COLUMN mdparameter.parameter_id    COMMENT     N   COMMENT ON COLUMN public.mdparameter.parameter_id IS 'Id de los parámetros';
            public       postgres    false    256            �           0    0    COLUMN mdparameter.description    COMMENT     W   COMMENT ON COLUMN public.mdparameter.description IS 'Descripción de los parámetros';
            public       postgres    false    256            �           0    0    COLUMN mdparameter.type    COMMENT     D   COMMENT ON COLUMN public.mdparameter.type IS 'Tipo de parámetros';
            public       postgres    false    256            �           0    0    COLUMN mdparameter.measure_id    COMMENT     F   COMMENT ON COLUMN public.mdparameter.measure_id IS 'Id de la medida';
            public       postgres    false    256            �           0    0    COLUMN mdparameter.process_id    COMMENT     E   COMMENT ON COLUMN public.mdparameter.process_id IS 'Id del proceso';
            public       postgres    false    256            �           0    0    COLUMN mdparameter.name    COMMENT     F   COMMENT ON COLUMN public.mdparameter.name IS 'Nombre del parámetro';
            public       postgres    false    256                       1259    27502    process_id_seq    SEQUENCE     w   CREATE SEQUENCE public.process_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.process_id_seq;
       public       postgres    false    3                       1259    27504 	   mdprocess    TABLE     B  CREATE TABLE public.mdprocess (
    process_id integer DEFAULT nextval('public.process_id_seq'::regclass) NOT NULL,
    process_order integer NOT NULL,
    product_id integer NOT NULL,
    stage_id integer NOT NULL,
    historical_decrease double precision NOT NULL,
    theoretical_decrease double precision NOT NULL,
    historical_weight double precision NOT NULL,
    theoretical_weight double precision NOT NULL,
    historical_duration integer NOT NULL,
    theoretical_duration integer NOT NULL,
    calendar_id integer NOT NULL,
    visible boolean,
    name character varying(250) NOT NULL,
    predecessor_id integer,
    capacity integer NOT NULL,
    breed_id integer NOT NULL,
    gender character varying(30),
    fattening_goal double precision,
    type_posture character varying(30),
    biological_active boolean
);
    DROP TABLE public.mdprocess;
       public         postgres    false    257    3            �           0    0    TABLE mdprocess    COMMENT     �   COMMENT ON TABLE public.mdprocess IS 'Almacena los procesos definidos para la planificación progresiva junto a sus respectivas características';
            public       postgres    false    258            �           0    0    COLUMN mdprocess.process_id    COMMENT     G   COMMENT ON COLUMN public.mdprocess.process_id IS 'Id de los procesos';
            public       postgres    false    258            �           0    0    COLUMN mdprocess.process_order    COMMENT     M   COMMENT ON COLUMN public.mdprocess.process_order IS 'Orden de los procesos';
            public       postgres    false    258            �           0    0    COLUMN mdprocess.product_id    COMMENT     D   COMMENT ON COLUMN public.mdprocess.product_id IS 'Id del producto';
            public       postgres    false    258            �           0    0    COLUMN mdprocess.stage_id    COMMENT     >   COMMENT ON COLUMN public.mdprocess.stage_id IS 'Id de etapa';
            public       postgres    false    258            �           0    0 $   COLUMN mdprocess.historical_decrease    COMMENT     Y   COMMENT ON COLUMN public.mdprocess.historical_decrease IS 'Merma historica del proceso';
            public       postgres    false    258                        0    0 %   COLUMN mdprocess.theoretical_decrease    COMMENT     Y   COMMENT ON COLUMN public.mdprocess.theoretical_decrease IS 'Merma teórica del proceso';
            public       postgres    false    258                       0    0 "   COLUMN mdprocess.historical_weight    COMMENT     V   COMMENT ON COLUMN public.mdprocess.historical_weight IS 'Peso historico del proceso';
            public       postgres    false    258                       0    0 #   COLUMN mdprocess.theoretical_weight    COMMENT     V   COMMENT ON COLUMN public.mdprocess.theoretical_weight IS 'Peso teórico del proceso';
            public       postgres    false    258                       0    0 $   COLUMN mdprocess.historical_duration    COMMENT     ^   COMMENT ON COLUMN public.mdprocess.historical_duration IS 'Duración histórica del proceso';
            public       postgres    false    258                       0    0 %   COLUMN mdprocess.theoretical_duration    COMMENT     ]   COMMENT ON COLUMN public.mdprocess.theoretical_duration IS 'Duración teórica del proceso';
            public       postgres    false    258                       0    0    COLUMN mdprocess.calendar_id    COMMENT     G   COMMENT ON COLUMN public.mdprocess.calendar_id IS 'Id del calendario';
            public       postgres    false    258                       0    0    COLUMN mdprocess.visible    COMMENT     I   COMMENT ON COLUMN public.mdprocess.visible IS 'Visibilidad del proceso';
            public       postgres    false    258                       0    0    COLUMN mdprocess.name    COMMENT     A   COMMENT ON COLUMN public.mdprocess.name IS 'Nombre del proceso';
            public       postgres    false    258                       0    0    COLUMN mdprocess.predecessor_id    COMMENT     J   COMMENT ON COLUMN public.mdprocess.predecessor_id IS 'Id del predecesor';
            public       postgres    false    258            	           0    0    COLUMN mdprocess.capacity    COMMENT     X   COMMENT ON COLUMN public.mdprocess.capacity IS 'Capacidad semanal asociada al proceso';
            public       postgres    false    258            
           0    0    COLUMN mdprocess.breed_id    COMMENT     @   COMMENT ON COLUMN public.mdprocess.breed_id IS 'Id de la raza';
            public       postgres    false    258                       0    0    COLUMN mdprocess.gender    COMMENT     N   COMMENT ON COLUMN public.mdprocess.gender IS 'Genero del producto de salida';
            public       postgres    false    258                       0    0    COLUMN mdprocess.fattening_goal    COMMENT     H   COMMENT ON COLUMN public.mdprocess.fattening_goal IS 'Meta de engorde';
            public       postgres    false    258                       0    0    COLUMN mdprocess.type_posture    COMMENT     s   COMMENT ON COLUMN public.mdprocess.type_posture IS 'Define el tipo de postura de acuerdo a la edad de la gallina';
            public       postgres    false    258                       0    0 "   COLUMN mdprocess.biological_active    COMMENT     h   COMMENT ON COLUMN public.mdprocess.biological_active IS 'Define si el proceso es un activo biológico';
            public       postgres    false    258                       1259    27508    product_id_seq    SEQUENCE     w   CREATE SEQUENCE public.product_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.product_id_seq;
       public       postgres    false    3                       1259    27510 	   mdproduct    TABLE     �   CREATE TABLE public.mdproduct (
    product_id integer DEFAULT nextval('public.product_id_seq'::regclass) NOT NULL,
    code character varying(20) NOT NULL,
    name character varying(45) NOT NULL
);
    DROP TABLE public.mdproduct;
       public         postgres    false    259    3                       0    0    TABLE mdproduct    COMMENT     Z   COMMENT ON TABLE public.mdproduct IS 'Almacena los productos utilizados en los procesos';
            public       postgres    false    260                       0    0    COLUMN mdproduct.product_id    COMMENT     D   COMMENT ON COLUMN public.mdproduct.product_id IS 'Id del producto';
            public       postgres    false    260                       0    0    COLUMN mdproduct.code    COMMENT     B   COMMENT ON COLUMN public.mdproduct.code IS 'Codigo del producto';
            public       postgres    false    260                       0    0    COLUMN mdproduct.name    COMMENT     B   COMMENT ON COLUMN public.mdproduct.name IS 'Nombre del producto';
            public       postgres    false    260                       1259    27514    mdrol_rol_id_seq    SEQUENCE        CREATE SEQUENCE public.mdrol_rol_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 10000000
    CACHE 1;
 '   DROP SEQUENCE public.mdrol_rol_id_seq;
       public       postgres    false    3                       1259    27516    mdrol    TABLE     	  CREATE TABLE public.mdrol (
    rol_id integer DEFAULT nextval('public.mdrol_rol_id_seq'::regclass) NOT NULL,
    rol_name character varying(80) NOT NULL,
    admin_user_creator character varying(80) NOT NULL,
    creation_date timestamp with time zone NOT NULL
);
    DROP TABLE public.mdrol;
       public         postgres    false    261    3                       0    0    TABLE mdrol    COMMENT     O   COMMENT ON TABLE public.mdrol IS 'Almacena los datos de los diferentes roles';
            public       postgres    false    262                       0    0    COLUMN mdrol.rol_id    COMMENT     7   COMMENT ON COLUMN public.mdrol.rol_id IS 'Id del rol';
            public       postgres    false    262                       0    0    COLUMN mdrol.rol_name    COMMENT     =   COMMENT ON COLUMN public.mdrol.rol_name IS 'Nombre del rol';
            public       postgres    false    262                       0    0    COLUMN mdrol.admin_user_creator    COMMENT     [   COMMENT ON COLUMN public.mdrol.admin_user_creator IS 'Especifica que usuario creo el rol';
            public       postgres    false    262                       0    0    COLUMN mdrol.creation_date    COMMENT     N   COMMENT ON COLUMN public.mdrol.creation_date IS 'Fecha de creación del rol';
            public       postgres    false    262                       1259    27520    scenario_id_seq    SEQUENCE     x   CREATE SEQUENCE public.scenario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.scenario_id_seq;
       public       postgres    false    3                       1259    27522 
   mdscenario    TABLE     d  CREATE TABLE public.mdscenario (
    scenario_id integer DEFAULT nextval('public.scenario_id_seq'::regclass) NOT NULL,
    description character varying(250) NOT NULL,
    date_start timestamp with time zone,
    date_end timestamp with time zone,
    name character varying(250) NOT NULL,
    status integer DEFAULT 0,
    calendar_id integer NOT NULL
);
    DROP TABLE public.mdscenario;
       public         postgres    false    263    3                       0    0    TABLE mdscenario    COMMENT     [   COMMENT ON TABLE public.mdscenario IS 'Almacena información de los distintos escenarios';
            public       postgres    false    264                       0    0    COLUMN mdscenario.scenario_id    COMMENT     G   COMMENT ON COLUMN public.mdscenario.scenario_id IS 'Id del escenario';
            public       postgres    false    264                       0    0    COLUMN mdscenario.description    COMMENT     P   COMMENT ON COLUMN public.mdscenario.description IS 'Descripcion del escenario';
            public       postgres    false    264                       0    0    COLUMN mdscenario.date_start    COMMENT     S   COMMENT ON COLUMN public.mdscenario.date_start IS 'Fecha de inicio del escenario';
            public       postgres    false    264                       0    0    COLUMN mdscenario.date_end    COMMENT     N   COMMENT ON COLUMN public.mdscenario.date_end IS 'Fecha de fin del escenario';
            public       postgres    false    264                       0    0    COLUMN mdscenario.name    COMMENT     D   COMMENT ON COLUMN public.mdscenario.name IS 'Nombre del escenario';
            public       postgres    false    264                       0    0    COLUMN mdscenario.status    COMMENT     F   COMMENT ON COLUMN public.mdscenario.status IS 'Estado del escenario';
            public       postgres    false    264            	           1259    27530    status_shed_id_seq    SEQUENCE     {   CREATE SEQUENCE public.status_shed_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.status_shed_id_seq;
       public       postgres    false    3            
           1259    27532    mdshedstatus    TABLE     �   CREATE TABLE public.mdshedstatus (
    shed_status_id integer DEFAULT nextval('public.status_shed_id_seq'::regclass) NOT NULL,
    name character varying(50) NOT NULL,
    description character varying(250) NOT NULL
);
     DROP TABLE public.mdshedstatus;
       public         postgres    false    265    3                       0    0    TABLE mdshedstatus    COMMENT     b   COMMENT ON TABLE public.mdshedstatus IS 'Almaceno los estatus de disponibilidad de los galpones';
            public       postgres    false    266                        0    0 "   COLUMN mdshedstatus.shed_status_id    COMMENT     T   COMMENT ON COLUMN public.mdshedstatus.shed_status_id IS 'Id del estado del galpon';
            public       postgres    false    266            !           0    0    COLUMN mdshedstatus.name    COMMENT     a   COMMENT ON COLUMN public.mdshedstatus.name IS 'Nombre del estado en que se encuentra el galpon';
            public       postgres    false    266            "           0    0    COLUMN mdshedstatus.description    COMMENT     [   COMMENT ON COLUMN public.mdshedstatus.description IS 'Descripcion del estado del galpon
';
            public       postgres    false    266                       1259    27536    stage_id_seq    SEQUENCE     u   CREATE SEQUENCE public.stage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.stage_id_seq;
       public       postgres    false    3                       1259    27538    mdstage    TABLE     �   CREATE TABLE public.mdstage (
    stage_id integer DEFAULT nextval('public.stage_id_seq'::regclass) NOT NULL,
    order_ integer,
    name character varying(250) NOT NULL
);
    DROP TABLE public.mdstage;
       public         postgres    false    267    3            #           0    0    TABLE mdstage    COMMENT     d   COMMENT ON TABLE public.mdstage IS 'Almacena las etapas a utilizar en el proceso de planificacion';
            public       postgres    false    268            $           0    0    COLUMN mdstage.stage_id    COMMENT     ?   COMMENT ON COLUMN public.mdstage.stage_id IS 'Id de la etapa';
            public       postgres    false    268            %           0    0    COLUMN mdstage.order_    COMMENT     U   COMMENT ON COLUMN public.mdstage.order_ IS 'Orden en el que se muestras las etapas';
            public       postgres    false    268            &           0    0    COLUMN mdstage.name    COMMENT     ?   COMMENT ON COLUMN public.mdstage.name IS 'Nombre de la etapa';
            public       postgres    false    268                       1259    27542    mduser_user_id_seq    SEQUENCE     �   CREATE SEQUENCE public.mduser_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9999999999
    CACHE 1;
 )   DROP SEQUENCE public.mduser_user_id_seq;
       public       postgres    false    3                       1259    27544    mduser    TABLE     �  CREATE TABLE public.mduser (
    user_id integer DEFAULT nextval('public.mduser_user_id_seq'::regclass) NOT NULL,
    username character varying(80) NOT NULL,
    password character varying(80) NOT NULL,
    name character varying(80) NOT NULL,
    lastname character varying(80) NOT NULL,
    active boolean NOT NULL,
    admi_user_creator character varying(80) NOT NULL,
    rol_id integer NOT NULL,
    creation_date timestamp with time zone NOT NULL
);
    DROP TABLE public.mduser;
       public         postgres    false    269    3                       1259    27548    oscenter    TABLE       CREATE TABLE public.oscenter (
    center_id integer DEFAULT nextval('public.center_id_seq'::regclass) NOT NULL,
    partnership_id integer NOT NULL,
    farm_id integer NOT NULL,
    name character varying(45) NOT NULL,
    code character varying(20) NOT NULL,
    "order" integer
);
    DROP TABLE public.oscenter;
       public         postgres    false    229    3            '           0    0    TABLE oscenter    COMMENT     S   COMMENT ON TABLE public.oscenter IS 'Almacena los datos referentes a los nucleos';
            public       postgres    false    271            (           0    0    COLUMN oscenter.center_id    COMMENT     @   COMMENT ON COLUMN public.oscenter.center_id IS 'Id del nucleo';
            public       postgres    false    271            )           0    0    COLUMN oscenter.partnership_id    COMMENT     H   COMMENT ON COLUMN public.oscenter.partnership_id IS 'Id de la empresa';
            public       postgres    false    271            *           0    0    COLUMN oscenter.farm_id    COMMENT     @   COMMENT ON COLUMN public.oscenter.farm_id IS 'Id de la granja';
            public       postgres    false    271            +           0    0    COLUMN oscenter.name    COMMENT     @   COMMENT ON COLUMN public.oscenter.name IS 'Nombre del nucleo
';
            public       postgres    false    271            ,           0    0    COLUMN oscenter.code    COMMENT     ?   COMMENT ON COLUMN public.oscenter.code IS 'Codigo del nucleo';
            public       postgres    false    271                       1259    27552    oscenter_oswarehouse    TABLE     �   CREATE TABLE public.oscenter_oswarehouse (
    client_id integer NOT NULL,
    partnership_id integer NOT NULL,
    farm_id integer NOT NULL,
    center_id integer NOT NULL,
    warehouse_id integer NOT NULL,
    delete_mark integer
);
 (   DROP TABLE public.oscenter_oswarehouse;
       public         postgres    false    3            -           0    0    TABLE oscenter_oswarehouse    COMMENT     p   COMMENT ON TABLE public.oscenter_oswarehouse IS 'Relación que une los núcleos con sus respectivos almacenes';
            public       postgres    false    272            .           0    0 %   COLUMN oscenter_oswarehouse.client_id    COMMENT     M   COMMENT ON COLUMN public.oscenter_oswarehouse.client_id IS 'Id del cliente';
            public       postgres    false    272            /           0    0 *   COLUMN oscenter_oswarehouse.partnership_id    COMMENT     T   COMMENT ON COLUMN public.oscenter_oswarehouse.partnership_id IS 'Id de la empresa';
            public       postgres    false    272            0           0    0 #   COLUMN oscenter_oswarehouse.farm_id    COMMENT     L   COMMENT ON COLUMN public.oscenter_oswarehouse.farm_id IS 'Id de la granja';
            public       postgres    false    272            1           0    0 %   COLUMN oscenter_oswarehouse.center_id    COMMENT     L   COMMENT ON COLUMN public.oscenter_oswarehouse.center_id IS 'Id del nucleo';
            public       postgres    false    272            2           0    0 (   COLUMN oscenter_oswarehouse.warehouse_id    COMMENT     P   COMMENT ON COLUMN public.oscenter_oswarehouse.warehouse_id IS 'Id del almacen';
            public       postgres    false    272            3           0    0 '   COLUMN oscenter_oswarehouse.delete_mark    COMMENT     Q   COMMENT ON COLUMN public.oscenter_oswarehouse.delete_mark IS 'Marca de borrado';
            public       postgres    false    272                       1259    27555    osfarm    TABLE       CREATE TABLE public.osfarm (
    farm_id integer DEFAULT nextval('public.farm_id_seq'::regclass) NOT NULL,
    partnership_id integer,
    code character varying(20) NOT NULL,
    name character varying(45) NOT NULL,
    farm_type_id integer NOT NULL,
    "order" integer
);
    DROP TABLE public.osfarm;
       public         postgres    false    233    3            4           0    0    TABLE osfarm    COMMENT     p   COMMENT ON TABLE public.osfarm IS 'Almacena la información de la granja con sus respectivas características';
            public       postgres    false    273            5           0    0    COLUMN osfarm.farm_id    COMMENT     >   COMMENT ON COLUMN public.osfarm.farm_id IS 'Id de la granja';
            public       postgres    false    273            6           0    0    COLUMN osfarm.partnership_id    COMMENT     F   COMMENT ON COLUMN public.osfarm.partnership_id IS 'Id de la empresa';
            public       postgres    false    273            7           0    0    COLUMN osfarm.code    COMMENT     ?   COMMENT ON COLUMN public.osfarm.code IS 'Codigo de la granja';
            public       postgres    false    273            8           0    0    COLUMN osfarm.name    COMMENT     ?   COMMENT ON COLUMN public.osfarm.name IS 'Nombre de la granja';
            public       postgres    false    273            9           0    0    COLUMN osfarm.farm_type_id    COMMENT     I   COMMENT ON COLUMN public.osfarm.farm_type_id IS 'Id del tipo de granja';
            public       postgres    false    273                       1259    27559    osincubator    TABLE     �  CREATE TABLE public.osincubator (
    incubator_id integer DEFAULT nextval('public.incubator_id_seq'::regclass) NOT NULL,
    incubator_plant_id integer,
    name character varying(45) NOT NULL,
    code character varying(20) NOT NULL,
    description character varying(250) NOT NULL,
    capacity integer,
    sunday integer,
    monday integer,
    tuesday integer,
    wednesday integer,
    thursday integer,
    friday integer,
    saturday integer,
    available integer
);
    DROP TABLE public.osincubator;
       public         postgres    false    238    3            :           0    0    TABLE osincubator    COMMENT     y   COMMENT ON TABLE public.osincubator IS 'Almacena las máquinas de incubación pertenecientes a cada una de las plantas';
            public       postgres    false    274            ;           0    0    COLUMN osincubator.incubator_id    COMMENT     L   COMMENT ON COLUMN public.osincubator.incubator_id IS 'Id de la incubadora';
            public       postgres    false    274            <           0    0 %   COLUMN osincubator.incubator_plant_id    COMMENT     Y   COMMENT ON COLUMN public.osincubator.incubator_plant_id IS 'Id de la planta incubadora';
            public       postgres    false    274            =           0    0    COLUMN osincubator.name    COMMENT     H   COMMENT ON COLUMN public.osincubator.name IS 'Nombre de la incubadora';
            public       postgres    false    274            >           0    0    COLUMN osincubator.code    COMMENT     H   COMMENT ON COLUMN public.osincubator.code IS 'Codigo de la incubadora';
            public       postgres    false    274            ?           0    0    COLUMN osincubator.description    COMMENT     T   COMMENT ON COLUMN public.osincubator.description IS 'Descripcion de la incubadora';
            public       postgres    false    274            @           0    0    COLUMN osincubator.capacity    COMMENT     O   COMMENT ON COLUMN public.osincubator.capacity IS 'Capacidad de la incubadora';
            public       postgres    false    274            A           0    0    COLUMN osincubator.sunday    COMMENT     ]   COMMENT ON COLUMN public.osincubator.sunday IS 'Marca los dias de trabajo de la incubadora';
            public       postgres    false    274            B           0    0    COLUMN osincubator.monday    COMMENT     ^   COMMENT ON COLUMN public.osincubator.monday IS 'Marca los días de trabajo de la incubadora';
            public       postgres    false    274            C           0    0    COLUMN osincubator.tuesday    COMMENT     _   COMMENT ON COLUMN public.osincubator.tuesday IS 'Marca los días de trabajo de la incubadora';
            public       postgres    false    274            D           0    0    COLUMN osincubator.wednesday    COMMENT     a   COMMENT ON COLUMN public.osincubator.wednesday IS 'Marca los días de trabajo de la incubadora';
            public       postgres    false    274            E           0    0    COLUMN osincubator.thursday    COMMENT     `   COMMENT ON COLUMN public.osincubator.thursday IS 'Marca los días de trabajo de la incubadora';
            public       postgres    false    274            F           0    0    COLUMN osincubator.friday    COMMENT     ^   COMMENT ON COLUMN public.osincubator.friday IS 'Marca los días de trabajo de la incubadora';
            public       postgres    false    274            G           0    0    COLUMN osincubator.saturday    COMMENT     `   COMMENT ON COLUMN public.osincubator.saturday IS 'Marca los días de trabajo de la incubadora';
            public       postgres    false    274                       1259    27563    osincubatorplant    TABLE     �  CREATE TABLE public.osincubatorplant (
    incubator_plant_id integer DEFAULT nextval('public.incubator_plant_id_seq'::regclass) NOT NULL,
    name character varying(45) NOT NULL,
    code character varying(20) NOT NULL,
    description character varying(250),
    partnership_id integer,
    max_storage integer,
    min_storage integer,
    acclimatized boolean,
    suitable boolean,
    expired boolean
);
 $   DROP TABLE public.osincubatorplant;
       public         postgres    false    239    3            H           0    0    TABLE osincubatorplant    COMMENT     }   COMMENT ON TABLE public.osincubatorplant IS 'Almacena la información de la planta incubadora perteneciente a cada empresa';
            public       postgres    false    275            I           0    0 *   COLUMN osincubatorplant.incubator_plant_id    COMMENT     ^   COMMENT ON COLUMN public.osincubatorplant.incubator_plant_id IS 'Id de la planta incubadora';
            public       postgres    false    275            J           0    0    COLUMN osincubatorplant.name    COMMENT     T   COMMENT ON COLUMN public.osincubatorplant.name IS 'Nombre de la planta incubadora';
            public       postgres    false    275            K           0    0    COLUMN osincubatorplant.code    COMMENT     T   COMMENT ON COLUMN public.osincubatorplant.code IS 'Codigo de la planta incubadora';
            public       postgres    false    275            L           0    0 #   COLUMN osincubatorplant.description    COMMENT     a   COMMENT ON COLUMN public.osincubatorplant.description IS 'Descripción de la planta incubadora';
            public       postgres    false    275            M           0    0 &   COLUMN osincubatorplant.partnership_id    COMMENT     P   COMMENT ON COLUMN public.osincubatorplant.partnership_id IS 'Id de la empresa';
            public       postgres    false    275            N           0    0 #   COLUMN osincubatorplant.max_storage    COMMENT     ]   COMMENT ON COLUMN public.osincubatorplant.max_storage IS 'Numero máximo de almacenamiento';
            public       postgres    false    275            O           0    0 #   COLUMN osincubatorplant.min_storage    COMMENT     \   COMMENT ON COLUMN public.osincubatorplant.min_storage IS 'Numero minimo de almacenamiento';
            public       postgres    false    275                       1259    27567    partnership_id_seq    SEQUENCE     {   CREATE SEQUENCE public.partnership_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.partnership_id_seq;
       public       postgres    false    3                       1259    27569    ospartnership    TABLE     2  CREATE TABLE public.ospartnership (
    partnership_id integer DEFAULT nextval('public.partnership_id_seq'::regclass) NOT NULL,
    name character varying(45) NOT NULL,
    address character varying(250) NOT NULL,
    description character varying(250) NOT NULL,
    code character varying(20) NOT NULL
);
 !   DROP TABLE public.ospartnership;
       public         postgres    false    276    3            P           0    0    TABLE ospartnership    COMMENT     j   COMMENT ON TABLE public.ospartnership IS 'Almacena la información referente a las empresas registradas';
            public       postgres    false    277            Q           0    0 #   COLUMN ospartnership.partnership_id    COMMENT     M   COMMENT ON COLUMN public.ospartnership.partnership_id IS 'Id de la empresa';
            public       postgres    false    277            R           0    0    COLUMN ospartnership.name    COMMENT     G   COMMENT ON COLUMN public.ospartnership.name IS 'Nombre de la empresa';
            public       postgres    false    277            S           0    0    COLUMN ospartnership.address    COMMENT     M   COMMENT ON COLUMN public.ospartnership.address IS 'Direccion de la empresa';
            public       postgres    false    277            T           0    0     COLUMN ospartnership.description    COMMENT     T   COMMENT ON COLUMN public.ospartnership.description IS 'Descripción de la empresa';
            public       postgres    false    277            U           0    0    COLUMN ospartnership.code    COMMENT     G   COMMENT ON COLUMN public.ospartnership.code IS 'Codigo de la empresa';
            public       postgres    false    277                       1259    27576    shed_id_seq    SEQUENCE     t   CREATE SEQUENCE public.shed_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE public.shed_id_seq;
       public       postgres    false    3                       1259    27578    osshed    TABLE       CREATE TABLE public.osshed (
    shed_id integer DEFAULT nextval('public.shed_id_seq'::regclass) NOT NULL,
    partnership_id integer NOT NULL,
    farm_id integer NOT NULL,
    center_id integer NOT NULL,
    code character varying(20) NOT NULL,
    statusshed_id integer NOT NULL,
    type_id integer,
    building_date date,
    stall_width double precision NOT NULL,
    stall_height double precision NOT NULL,
    capacity_min double precision NOT NULL,
    capacity_max double precision NOT NULL,
    environment_id integer,
    rotation_days integer DEFAULT 0 NOT NULL,
    nests_quantity integer DEFAULT 0,
    cages_quantity integer DEFAULT 0,
    birds_quantity integer DEFAULT 0,
    capacity_theoretical integer DEFAULT 0,
    avaliable_date date,
    "order" integer
);
    DROP TABLE public.osshed;
       public         postgres    false    278    3            V           0    0    TABLE osshed    COMMENT     d   COMMENT ON TABLE public.osshed IS 'Almacena la informacion de los galpones asociados a la empresa';
            public       postgres    false    279            W           0    0    COLUMN osshed.shed_id    COMMENT     <   COMMENT ON COLUMN public.osshed.shed_id IS 'Id del galpon';
            public       postgres    false    279            X           0    0    COLUMN osshed.partnership_id    COMMENT     F   COMMENT ON COLUMN public.osshed.partnership_id IS 'Id de la empresa';
            public       postgres    false    279            Y           0    0    COLUMN osshed.farm_id    COMMENT     >   COMMENT ON COLUMN public.osshed.farm_id IS 'Id de la granja';
            public       postgres    false    279            Z           0    0    COLUMN osshed.center_id    COMMENT     >   COMMENT ON COLUMN public.osshed.center_id IS 'Id del nucleo';
            public       postgres    false    279            [           0    0    COLUMN osshed.code    COMMENT     =   COMMENT ON COLUMN public.osshed.code IS 'Codigo del galpon';
            public       postgres    false    279            \           0    0    COLUMN osshed.statusshed_id    COMMENT     _   COMMENT ON COLUMN public.osshed.statusshed_id IS 'Identificador del estado actual del galpon';
            public       postgres    false    279            ]           0    0    COLUMN osshed.type_id    COMMENT     D   COMMENT ON COLUMN public.osshed.type_id IS 'Id del tipo de galpon';
            public       postgres    false    279            ^           0    0    COLUMN osshed.building_date    COMMENT     c   COMMENT ON COLUMN public.osshed.building_date IS 'Almacena la fecha de construccion del edificio';
            public       postgres    false    279            _           0    0    COLUMN osshed.stall_width    COMMENT     M   COMMENT ON COLUMN public.osshed.stall_width IS 'Indica el ancho del galpon';
            public       postgres    false    279            `           0    0    COLUMN osshed.stall_height    COMMENT     M   COMMENT ON COLUMN public.osshed.stall_height IS 'Indica el alto del galpon';
            public       postgres    false    279            a           0    0    COLUMN osshed.capacity_min    COMMENT     D   COMMENT ON COLUMN public.osshed.capacity_min IS 'Capacidad minima';
            public       postgres    false    279            b           0    0    COLUMN osshed.capacity_max    COMMENT     F   COMMENT ON COLUMN public.osshed.capacity_max IS 'Capacidad máxima ';
            public       postgres    false    279            c           0    0    COLUMN osshed.environment_id    COMMENT     E   COMMENT ON COLUMN public.osshed.environment_id IS 'Id del ambiente';
            public       postgres    false    279            d           0    0    COLUMN osshed.rotation_days    COMMENT     H   COMMENT ON COLUMN public.osshed.rotation_days IS 'Días de rotación
';
            public       postgres    false    279            e           0    0    COLUMN osshed.nests_quantity    COMMENT     I   COMMENT ON COLUMN public.osshed.nests_quantity IS 'Cantidad de nidales';
            public       postgres    false    279            f           0    0    COLUMN osshed.cages_quantity    COMMENT     H   COMMENT ON COLUMN public.osshed.cages_quantity IS 'Cantidad de jaulas';
            public       postgres    false    279            g           0    0    COLUMN osshed.birds_quantity    COMMENT     F   COMMENT ON COLUMN public.osshed.birds_quantity IS 'Cantidad de aves';
            public       postgres    false    279            h           0    0 "   COLUMN osshed.capacity_theoretical    COMMENT     O   COMMENT ON COLUMN public.osshed.capacity_theoretical IS '	Capacidad teórica';
            public       postgres    false    279                       1259    27587    silo_id_seq    SEQUENCE     t   CREATE SEQUENCE public.silo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE public.silo_id_seq;
       public       postgres    false    3                       1259    27589    ossilo    TABLE     �  CREATE TABLE public.ossilo (
    silo_id integer DEFAULT nextval('public.silo_id_seq'::regclass) NOT NULL,
    client_id integer,
    partnership_id integer NOT NULL,
    farm_id integer NOT NULL,
    center_id integer NOT NULL,
    name character varying(45) NOT NULL,
    rings_height double precision,
    rings_height_id integer,
    height double precision NOT NULL,
    height_unit_id integer,
    diameter double precision NOT NULL,
    diameter_unit_id integer,
    total_rings_quantity integer,
    measuring_mechanism integer,
    cone_degrees double precision,
    total_capacity_1 double precision,
    total_capacity_2 double precision,
    capacity_unit_id_1 integer,
    capacity_unit_id_2 integer,
    central character varying(20)
);
    DROP TABLE public.ossilo;
       public         postgres    false    280    3            i           0    0    TABLE ossilo    COMMENT     E   COMMENT ON TABLE public.ossilo IS 'Almacena los datos de los silos';
            public       postgres    false    281            j           0    0    COLUMN ossilo.silo_id    COMMENT     :   COMMENT ON COLUMN public.ossilo.silo_id IS 'Id del silo';
            public       postgres    false    281            k           0    0    COLUMN ossilo.client_id    COMMENT     ?   COMMENT ON COLUMN public.ossilo.client_id IS 'Id del cliente';
            public       postgres    false    281            l           0    0    COLUMN ossilo.partnership_id    COMMENT     F   COMMENT ON COLUMN public.ossilo.partnership_id IS 'Id de la empresa';
            public       postgres    false    281            m           0    0    COLUMN ossilo.farm_id    COMMENT     >   COMMENT ON COLUMN public.ossilo.farm_id IS 'Id de la granja';
            public       postgres    false    281            n           0    0    COLUMN ossilo.center_id    COMMENT     >   COMMENT ON COLUMN public.ossilo.center_id IS 'Id del nucleo';
            public       postgres    false    281            o           0    0    COLUMN ossilo.name    COMMENT     ;   COMMENT ON COLUMN public.ossilo.name IS 'Nombre del silo';
            public       postgres    false    281            p           0    0    COLUMN ossilo.rings_height    COMMENT     E   COMMENT ON COLUMN public.ossilo.rings_height IS 'Numero de anillos';
            public       postgres    false    281            q           0    0    COLUMN ossilo.rings_height_id    COMMENT     R   COMMENT ON COLUMN public.ossilo.rings_height_id IS 'Unidad de medida del anillo';
            public       postgres    false    281            r           0    0    COLUMN ossilo.height    COMMENT     =   COMMENT ON COLUMN public.ossilo.height IS 'Altura del silo';
            public       postgres    false    281            s           0    0    COLUMN ossilo.height_unit_id    COMMENT     R   COMMENT ON COLUMN public.ossilo.height_unit_id IS 'Unidad de media de la altura';
            public       postgres    false    281            t           0    0    COLUMN ossilo.diameter    COMMENT     A   COMMENT ON COLUMN public.ossilo.diameter IS 'Diametro del silo';
            public       postgres    false    281            u           0    0    COLUMN ossilo.diameter_unit_id    COMMENT     T   COMMENT ON COLUMN public.ossilo.diameter_unit_id IS 'Unidad de media del diametro';
            public       postgres    false    281            v           0    0 "   COLUMN ossilo.total_rings_quantity    COMMENT     U   COMMENT ON COLUMN public.ossilo.total_rings_quantity IS 'Total de anillos del silo';
            public       postgres    false    281            w           0    0 !   COLUMN ossilo.measuring_mechanism    COMMENT     Y   COMMENT ON COLUMN public.ossilo.measuring_mechanism IS 'Mecanismo de medidad del silo
';
            public       postgres    false    281            x           0    0    COLUMN ossilo.cone_degrees    COMMENT     C   COMMENT ON COLUMN public.ossilo.cone_degrees IS 'Grados del cono';
            public       postgres    false    281            y           0    0    COLUMN ossilo.total_capacity_1    COMMENT     L   COMMENT ON COLUMN public.ossilo.total_capacity_1 IS 'Total de capacidad 1';
            public       postgres    false    281            z           0    0    COLUMN ossilo.total_capacity_2    COMMENT     L   COMMENT ON COLUMN public.ossilo.total_capacity_2 IS 'Total de capacidad 2';
            public       postgres    false    281            {           0    0     COLUMN ossilo.capacity_unit_id_1    COMMENT     X   COMMENT ON COLUMN public.ossilo.capacity_unit_id_1 IS 'Id de Capacidad de la unidad 1';
            public       postgres    false    281            |           0    0     COLUMN ossilo.capacity_unit_id_2    COMMENT     X   COMMENT ON COLUMN public.ossilo.capacity_unit_id_2 IS 'Id de Capacidad de la unidad 2';
            public       postgres    false    281            }           0    0    COLUMN ossilo.central    COMMENT     6   COMMENT ON COLUMN public.ossilo.central IS 'Central';
            public       postgres    false    281                       1259    27593    ossilo_osshed    TABLE     �   CREATE TABLE public.ossilo_osshed (
    silo_id integer NOT NULL,
    shed_id integer NOT NULL,
    center_id integer NOT NULL,
    farm_id integer NOT NULL,
    partnership_id integer NOT NULL,
    client_id integer NOT NULL,
    deleted_mark integer
);
 !   DROP TABLE public.ossilo_osshed;
       public         postgres    false    3            ~           0    0    TABLE ossilo_osshed    COMMENT     R   COMMENT ON TABLE public.ossilo_osshed IS 'Tabla union de las tablas silo y shed';
            public       postgres    false    282                       0    0    COLUMN ossilo_osshed.silo_id    COMMENT     A   COMMENT ON COLUMN public.ossilo_osshed.silo_id IS 'Id del silo';
            public       postgres    false    282            �           0    0    COLUMN ossilo_osshed.shed_id    COMMENT     C   COMMENT ON COLUMN public.ossilo_osshed.shed_id IS 'Id del galpon';
            public       postgres    false    282            �           0    0    COLUMN ossilo_osshed.center_id    COMMENT     E   COMMENT ON COLUMN public.ossilo_osshed.center_id IS 'Id del nucleo';
            public       postgres    false    282            �           0    0    COLUMN ossilo_osshed.farm_id    COMMENT     E   COMMENT ON COLUMN public.ossilo_osshed.farm_id IS 'Id de la granja';
            public       postgres    false    282            �           0    0 #   COLUMN ossilo_osshed.partnership_id    COMMENT     M   COMMENT ON COLUMN public.ossilo_osshed.partnership_id IS 'Id de la empresa';
            public       postgres    false    282            �           0    0    COLUMN ossilo_osshed.client_id    COMMENT     F   COMMENT ON COLUMN public.ossilo_osshed.client_id IS 'Id del cliente';
            public       postgres    false    282            �           0    0 !   COLUMN ossilo_osshed.deleted_mark    COMMENT     K   COMMENT ON COLUMN public.ossilo_osshed.deleted_mark IS 'Marca de borrado';
            public       postgres    false    282                       1259    27596    slaughterhouse_id_seq    SEQUENCE        CREATE SEQUENCE public.slaughterhouse_id_seq
    START WITH 33
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.slaughterhouse_id_seq;
       public       postgres    false    3                       1259    27598    osslaughterhouse    TABLE     Z  CREATE TABLE public.osslaughterhouse (
    slaughterhouse_id integer DEFAULT nextval('public.slaughterhouse_id_seq'::regclass) NOT NULL,
    name character varying(45) NOT NULL,
    address character varying(250) NOT NULL,
    description character varying(250) NOT NULL,
    code character varying(20) NOT NULL,
    capacity double precision
);
 $   DROP TABLE public.osslaughterhouse;
       public         postgres    false    283    3                       1259    27605    warehouse_id_seq    SEQUENCE     y   CREATE SEQUENCE public.warehouse_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.warehouse_id_seq;
       public       postgres    false    3                       1259    27607    oswarehouse    TABLE       CREATE TABLE public.oswarehouse (
    warehouse_id integer DEFAULT nextval('public.warehouse_id_seq'::regclass) NOT NULL,
    partnership_id integer NOT NULL,
    farm_id integer NOT NULL,
    name character varying(45) NOT NULL,
    code character varying(20) NOT NULL
);
    DROP TABLE public.oswarehouse;
       public         postgres    false    285    3            �           0    0    TABLE oswarehouse    COMMENT     \   COMMENT ON TABLE public.oswarehouse IS 'Almacena la informacion referente a los almacenes';
            public       postgres    false    286            �           0    0    COLUMN oswarehouse.warehouse_id    COMMENT     G   COMMENT ON COLUMN public.oswarehouse.warehouse_id IS 'Id del almacen';
            public       postgres    false    286            �           0    0 !   COLUMN oswarehouse.partnership_id    COMMENT     ^   COMMENT ON COLUMN public.oswarehouse.partnership_id IS 'Id de la empresa dueña del almacen';
            public       postgres    false    286            �           0    0    COLUMN oswarehouse.farm_id    COMMENT     C   COMMENT ON COLUMN public.oswarehouse.farm_id IS 'Id de la granja';
            public       postgres    false    286            �           0    0    COLUMN oswarehouse.name    COMMENT     C   COMMENT ON COLUMN public.oswarehouse.name IS 'Nombre del almacen';
            public       postgres    false    286            �           0    0    COLUMN oswarehouse.code    COMMENT     C   COMMENT ON COLUMN public.oswarehouse.code IS 'Codigo del almacen';
            public       postgres    false    286                       1259    27611    posture_curve_id_seq    SEQUENCE     }   CREATE SEQUENCE public.posture_curve_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.posture_curve_id_seq;
       public       postgres    false    3                        1259    27613    predecessor_id_seq    SEQUENCE     {   CREATE SEQUENCE public.predecessor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.predecessor_id_seq;
       public       postgres    false    3            !           1259    27615    process_class_id_seq    SEQUENCE     }   CREATE SEQUENCE public.process_class_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.process_class_id_seq;
       public       postgres    false    3            "           1259    27617    programmed_eggs_id_seq    SEQUENCE        CREATE SEQUENCE public.programmed_eggs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.programmed_eggs_id_seq;
       public       postgres    false    3            #           1259    27619    raspberry_id_seq    SEQUENCE     y   CREATE SEQUENCE public.raspberry_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.raspberry_id_seq;
       public       postgres    false    3            $           1259    27621    scenario_formula_id_seq    SEQUENCE     �   CREATE SEQUENCE public.scenario_formula_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.scenario_formula_id_seq;
       public       postgres    false    3            %           1259    27623    scenario_parameter_day_seq    SEQUENCE     �   CREATE SEQUENCE public.scenario_parameter_day_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.scenario_parameter_day_seq;
       public       postgres    false    3            &           1259    27625    scenario_parameter_id_seq    SEQUENCE     �   CREATE SEQUENCE public.scenario_parameter_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.scenario_parameter_id_seq;
       public       postgres    false    3            '           1259    27627    scenario_posture_id_seq    SEQUENCE     �   CREATE SEQUENCE public.scenario_posture_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.scenario_posture_id_seq;
       public       postgres    false    3            (           1259    27629    scenario_process_id_seq    SEQUENCE     �   CREATE SEQUENCE public.scenario_process_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.scenario_process_id_seq;
       public       postgres    false    3            )           1259    27631    txavailabilitysheds    TABLE       CREATE TABLE public.txavailabilitysheds (
    availability_shed_id integer DEFAULT nextval('public.availability_shed_id_seq'::regclass) NOT NULL,
    shed_id integer NOT NULL,
    init_date date,
    end_date date,
    lot_code character varying(20) NOT NULL
);
 '   DROP TABLE public.txavailabilitysheds;
       public         postgres    false    216    3            �           0    0    TABLE txavailabilitysheds    COMMENT     �   COMMENT ON TABLE public.txavailabilitysheds IS 'Almacena la disponibilidad en fechas de los galpones de acuerdo a la programación establecida';
            public       postgres    false    297            �           0    0 /   COLUMN txavailabilitysheds.availability_shed_id    COMMENT     �   COMMENT ON COLUMN public.txavailabilitysheds.availability_shed_id IS 'Id de la disponibilidad del almacen, indicando si este esta disponible';
            public       postgres    false    297            �           0    0 "   COLUMN txavailabilitysheds.shed_id    COMMENT     I   COMMENT ON COLUMN public.txavailabilitysheds.shed_id IS 'Id del galpon';
            public       postgres    false    297            �           0    0 $   COLUMN txavailabilitysheds.init_date    COMMENT     r   COMMENT ON COLUMN public.txavailabilitysheds.init_date IS 'Fecha de inicio de la programacion de uso del galpon';
            public       postgres    false    297            �           0    0 #   COLUMN txavailabilitysheds.end_date    COMMENT     r   COMMENT ON COLUMN public.txavailabilitysheds.end_date IS 'Fecha de cerrado de la programacion de uso del galpon';
            public       postgres    false    297            �           0    0 #   COLUMN txavailabilitysheds.lot_code    COMMENT     W   COMMENT ON COLUMN public.txavailabilitysheds.lot_code IS 'codigo del lote del galpon';
            public       postgres    false    297            *           1259    27635 	   txbroiler    TABLE     L  CREATE TABLE public.txbroiler (
    broiler_id integer DEFAULT nextval('public.broiler_id_seq'::regclass) NOT NULL,
    projected_date date,
    projected_quantity integer,
    partnership_id integer,
    scenario_id integer,
    breed_id integer,
    lot_incubator character varying(45) NOT NULL,
    programmed_eggs_id integer
);
    DROP TABLE public.txbroiler;
       public         postgres    false    220    3            �           0    0    TABLE txbroiler    COMMENT     c   COMMENT ON TABLE public.txbroiler IS 'Almacena la proyeccion realizada para el modulo de engorde';
            public       postgres    false    298            �           0    0    COLUMN txbroiler.broiler_id    COMMENT     U   COMMENT ON COLUMN public.txbroiler.broiler_id IS 'Id de la programacion de engorde';
            public       postgres    false    298            �           0    0    COLUMN txbroiler.projected_date    COMMENT     X   COMMENT ON COLUMN public.txbroiler.projected_date IS 'Fecha de proyección de engorde';
            public       postgres    false    298            �           0    0 #   COLUMN txbroiler.projected_quantity    COMMENT     `   COMMENT ON COLUMN public.txbroiler.projected_quantity IS 'Cantidad proyectada para el engorde';
            public       postgres    false    298            �           0    0    COLUMN txbroiler.partnership_id    COMMENT     I   COMMENT ON COLUMN public.txbroiler.partnership_id IS 'Id de la empresa';
            public       postgres    false    298            �           0    0    COLUMN txbroiler.scenario_id    COMMENT     G   COMMENT ON COLUMN public.txbroiler.scenario_id IS 'Id edl escenario ';
            public       postgres    false    298            �           0    0    COLUMN txbroiler.breed_id    COMMENT     K   COMMENT ON COLUMN public.txbroiler.breed_id IS 'Id de la raza a engordar';
            public       postgres    false    298            �           0    0    COLUMN txbroiler.lot_incubator    COMMENT     u   COMMENT ON COLUMN public.txbroiler.lot_incubator IS 'Lote de incubación de donde provienen los huevos proyectados';
            public       postgres    false    298            �           0    0 #   COLUMN txbroiler.programmed_eggs_id    COMMENT     Y   COMMENT ON COLUMN public.txbroiler.programmed_eggs_id IS 'Id de los huevos programados';
            public       postgres    false    298            +           1259    27639    txbroiler_detail    TABLE       CREATE TABLE public.txbroiler_detail (
    broiler_detail_id integer DEFAULT nextval('public.broiler_detail_id_seq'::regclass) NOT NULL,
    broiler_id integer NOT NULL,
    scheduled_date date,
    scheduled_quantity integer,
    farm_id integer NOT NULL,
    shed_id integer NOT NULL,
    confirm integer,
    execution_date date,
    execution_quantity integer,
    lot integer NOT NULL,
    broiler_product_id integer,
    center_id integer,
    executionfarm_id integer,
    executioncenter_id integer,
    executionshed_id integer
);
 $   DROP TABLE public.txbroiler_detail;
       public         postgres    false    219    3            �           0    0    TABLE txbroiler_detail    COMMENT     l   COMMENT ON TABLE public.txbroiler_detail IS 'Almacena la programacion y ejecuccion del proceso de engorde';
            public       postgres    false    299            �           0    0 )   COLUMN txbroiler_detail.broiler_detail_id    COMMENT     `   COMMENT ON COLUMN public.txbroiler_detail.broiler_detail_id IS 'Id de los detalles de engorde';
            public       postgres    false    299            �           0    0 "   COLUMN txbroiler_detail.broiler_id    COMMENT     \   COMMENT ON COLUMN public.txbroiler_detail.broiler_id IS 'Id de la programacion de engorde';
            public       postgres    false    299            �           0    0 &   COLUMN txbroiler_detail.scheduled_date    COMMENT     k   COMMENT ON COLUMN public.txbroiler_detail.scheduled_date IS 'Fecha programada para el proceso de engorde';
            public       postgres    false    299            �           0    0 *   COLUMN txbroiler_detail.scheduled_quantity    COMMENT     r   COMMENT ON COLUMN public.txbroiler_detail.scheduled_quantity IS 'Cantidad programada para el proceso de engorde';
            public       postgres    false    299            �           0    0    COLUMN txbroiler_detail.farm_id    COMMENT     H   COMMENT ON COLUMN public.txbroiler_detail.farm_id IS 'Id de la granja';
            public       postgres    false    299            �           0    0    COLUMN txbroiler_detail.shed_id    COMMENT     F   COMMENT ON COLUMN public.txbroiler_detail.shed_id IS 'Id del galpon';
            public       postgres    false    299            �           0    0    COLUMN txbroiler_detail.confirm    COMMENT     E   COMMENT ON COLUMN public.txbroiler_detail.confirm IS 'Confirmacion';
            public       postgres    false    299            �           0    0 &   COLUMN txbroiler_detail.execution_date    COMMENT     p   COMMENT ON COLUMN public.txbroiler_detail.execution_date IS 'Fecha de ejeccion de la planificacion de engorde';
            public       postgres    false    299            �           0    0 *   COLUMN txbroiler_detail.execution_quantity    COMMENT     u   COMMENT ON COLUMN public.txbroiler_detail.execution_quantity IS 'Cantidad ejecutada de la programación de engorde';
            public       postgres    false    299            �           0    0    COLUMN txbroiler_detail.lot    COMMENT     D   COMMENT ON COLUMN public.txbroiler_detail.lot IS 'Lote de engorde';
            public       postgres    false    299            �           0    0 *   COLUMN txbroiler_detail.broiler_product_id    COMMENT     ^   COMMENT ON COLUMN public.txbroiler_detail.broiler_product_id IS 'Id del producto de engorde';
            public       postgres    false    299            ,           1259    27643    txbroilereviction    TABLE     �  CREATE TABLE public.txbroilereviction (
    broilereviction_id integer DEFAULT nextval('public.broilereviction_id_seq'::regclass) NOT NULL,
    projected_date date,
    projected_quantity integer,
    partnership_id integer NOT NULL,
    scenario_id integer NOT NULL,
    breed_id integer NOT NULL,
    lot_incubator character varying(45) NOT NULL,
    broiler_detail_id integer NOT NULL
);
 %   DROP TABLE public.txbroilereviction;
       public         postgres    false    224    3            �           0    0    TABLE txbroilereviction    COMMENT     _   COMMENT ON TABLE public.txbroilereviction IS 'Almacena las proyeccion del modula de desalojo';
            public       postgres    false    300            �           0    0 +   COLUMN txbroilereviction.broilereviction_id    COMMENT     ^   COMMENT ON COLUMN public.txbroilereviction.broilereviction_id IS 'Id del modulo de desalojo';
            public       postgres    false    300            �           0    0 '   COLUMN txbroilereviction.projected_date    COMMENT     b   COMMENT ON COLUMN public.txbroilereviction.projected_date IS 'Fecha proyectada para el desalojo';
            public       postgres    false    300            �           0    0 +   COLUMN txbroilereviction.projected_quantity    COMMENT     i   COMMENT ON COLUMN public.txbroilereviction.projected_quantity IS 'Cantidad proyectada para el desalojo';
            public       postgres    false    300            �           0    0 '   COLUMN txbroilereviction.partnership_id    COMMENT     Q   COMMENT ON COLUMN public.txbroilereviction.partnership_id IS 'Id de la empresa';
            public       postgres    false    300            �           0    0 $   COLUMN txbroilereviction.scenario_id    COMMENT     N   COMMENT ON COLUMN public.txbroilereviction.scenario_id IS 'Id del escenario';
            public       postgres    false    300            �           0    0 !   COLUMN txbroilereviction.breed_id    COMMENT     H   COMMENT ON COLUMN public.txbroilereviction.breed_id IS 'Id de la raza';
            public       postgres    false    300            �           0    0 &   COLUMN txbroilereviction.lot_incubator    COMMENT     R   COMMENT ON COLUMN public.txbroilereviction.lot_incubator IS 'Lote de incubacion';
            public       postgres    false    300            -           1259    27647    txbroilereviction_detail    TABLE     9  CREATE TABLE public.txbroilereviction_detail (
    broilereviction_detail_id integer DEFAULT nextval('public.broilereviction_detail_id_seq'::regclass) NOT NULL,
    broilereviction_id integer NOT NULL,
    scheduled_date date,
    scheduled_quantity integer,
    farm_id integer NOT NULL,
    shed_id integer NOT NULL,
    confirm integer,
    execution_date date,
    execution_quantity integer,
    lot integer NOT NULL,
    broiler_product_id integer NOT NULL,
    slaughterhouse_id integer NOT NULL,
    center_id integer,
    executionslaughterhouse_id integer
);
 ,   DROP TABLE public.txbroilereviction_detail;
       public         postgres    false    223    3            �           0    0    TABLE txbroilereviction_detail    COMMENT     v   COMMENT ON TABLE public.txbroilereviction_detail IS 'Almacena la programación y ejecución del módulo de desalojo';
            public       postgres    false    301            �           0    0 9   COLUMN txbroilereviction_detail.broilereviction_detail_id    COMMENT     ~   COMMENT ON COLUMN public.txbroilereviction_detail.broilereviction_detail_id IS 'Id de los detalles del modulo de desarrollo';
            public       postgres    false    301            �           0    0 2   COLUMN txbroilereviction_detail.broilereviction_id    COMMENT     e   COMMENT ON COLUMN public.txbroilereviction_detail.broilereviction_id IS 'Id del modulo de desalojo';
            public       postgres    false    301            �           0    0 .   COLUMN txbroilereviction_detail.scheduled_date    COMMENT     i   COMMENT ON COLUMN public.txbroilereviction_detail.scheduled_date IS 'Fecha programada para el desalojo';
            public       postgres    false    301            �           0    0 2   COLUMN txbroilereviction_detail.scheduled_quantity    COMMENT     p   COMMENT ON COLUMN public.txbroilereviction_detail.scheduled_quantity IS 'Cantidad programada para el desalojo';
            public       postgres    false    301            �           0    0 '   COLUMN txbroilereviction_detail.farm_id    COMMENT     P   COMMENT ON COLUMN public.txbroilereviction_detail.farm_id IS 'Id de la granja';
            public       postgres    false    301            �           0    0 '   COLUMN txbroilereviction_detail.shed_id    COMMENT     N   COMMENT ON COLUMN public.txbroilereviction_detail.shed_id IS 'Id del galpon';
            public       postgres    false    301            �           0    0 '   COLUMN txbroilereviction_detail.confirm    COMMENT     M   COMMENT ON COLUMN public.txbroilereviction_detail.confirm IS 'Confirmacion';
            public       postgres    false    301            �           0    0 .   COLUMN txbroilereviction_detail.execution_date    COMMENT     \   COMMENT ON COLUMN public.txbroilereviction_detail.execution_date IS 'Fecha de ejecución ';
            public       postgres    false    301            �           0    0 2   COLUMN txbroilereviction_detail.execution_quantity    COMMENT     c   COMMENT ON COLUMN public.txbroilereviction_detail.execution_quantity IS 'Cantidad de ejecución ';
            public       postgres    false    301            �           0    0 #   COLUMN txbroilereviction_detail.lot    COMMENT     X   COMMENT ON COLUMN public.txbroilereviction_detail.lot IS 'Lote del modulo de desalojo';
            public       postgres    false    301            �           0    0 2   COLUMN txbroilereviction_detail.broiler_product_id    COMMENT     f   COMMENT ON COLUMN public.txbroilereviction_detail.broiler_product_id IS 'Id del producto de engorde';
            public       postgres    false    301            �           0    0 1   COLUMN txbroilereviction_detail.slaughterhouse_id    COMMENT     g   COMMENT ON COLUMN public.txbroilereviction_detail.slaughterhouse_id IS 'Id de la planta de beneficio';
            public       postgres    false    301            .           1259    27651    txbroilerproduct_detail    TABLE     �   CREATE TABLE public.txbroilerproduct_detail (
    broilerproduct_detail_id integer DEFAULT nextval('public.broiler_product_detail_id_seq'::regclass) NOT NULL,
    broiler_detail integer NOT NULL,
    broiler_product_id integer,
    quantity integer
);
 +   DROP TABLE public.txbroilerproduct_detail;
       public         postgres    false    221    3            �           0    0    TABLE txbroilerproduct_detail    COMMENT     h   COMMENT ON TABLE public.txbroilerproduct_detail IS 'Almacena los detalles de la produccion de engorde';
            public       postgres    false    302            �           0    0 7   COLUMN txbroilerproduct_detail.broilerproduct_detail_id    COMMENT     |   COMMENT ON COLUMN public.txbroilerproduct_detail.broilerproduct_detail_id IS 'Id de los detalles de produccion de engorde';
            public       postgres    false    302            �           0    0 -   COLUMN txbroilerproduct_detail.broiler_detail    COMMENT     Z   COMMENT ON COLUMN public.txbroilerproduct_detail.broiler_detail IS 'Detalles de engorde';
            public       postgres    false    302            �           0    0 1   COLUMN txbroilerproduct_detail.broiler_product_id    COMMENT     e   COMMENT ON COLUMN public.txbroilerproduct_detail.broiler_product_id IS 'Id del producto de engorde';
            public       postgres    false    302            �           0    0 '   COLUMN txbroilerproduct_detail.quantity    COMMENT     `   COMMENT ON COLUMN public.txbroilerproduct_detail.quantity IS 'Cantidad de producto de engorde';
            public       postgres    false    302            /           1259    27655    txbroodermachine    TABLE     �  CREATE TABLE public.txbroodermachine (
    brooder_machine_id_seq integer DEFAULT nextval('public.brooder_machines_id_seq'::regclass) NOT NULL,
    partnership_id integer NOT NULL,
    farm_id integer NOT NULL,
    capacity integer,
    sunday integer,
    monday integer,
    tuesday integer,
    wednesday integer,
    thursday integer,
    friday integer,
    saturday integer,
    name character varying(250)
);
 $   DROP TABLE public.txbroodermachine;
       public         postgres    false    226    3            �           0    0    TABLE txbroodermachine    COMMENT     ]   COMMENT ON TABLE public.txbroodermachine IS 'Almacena los datos de las maquinas de engorde';
            public       postgres    false    303            �           0    0 .   COLUMN txbroodermachine.brooder_machine_id_seq    COMMENT     c   COMMENT ON COLUMN public.txbroodermachine.brooder_machine_id_seq IS 'Id de la maquina de engorde';
            public       postgres    false    303            �           0    0 &   COLUMN txbroodermachine.partnership_id    COMMENT     P   COMMENT ON COLUMN public.txbroodermachine.partnership_id IS 'Id de la empresa';
            public       postgres    false    303            �           0    0    COLUMN txbroodermachine.farm_id    COMMENT     H   COMMENT ON COLUMN public.txbroodermachine.farm_id IS 'Id de la granja';
            public       postgres    false    303            �           0    0     COLUMN txbroodermachine.capacity    COMMENT     Q   COMMENT ON COLUMN public.txbroodermachine.capacity IS 'Capacidad de la maquina';
            public       postgres    false    303            �           0    0    COLUMN txbroodermachine.sunday    COMMENT     ?   COMMENT ON COLUMN public.txbroodermachine.sunday IS 'Domingo';
            public       postgres    false    303            �           0    0    COLUMN txbroodermachine.monday    COMMENT     =   COMMENT ON COLUMN public.txbroodermachine.monday IS 'Lunes';
            public       postgres    false    303            �           0    0    COLUMN txbroodermachine.tuesday    COMMENT     ?   COMMENT ON COLUMN public.txbroodermachine.tuesday IS 'Martes';
            public       postgres    false    303            �           0    0 !   COLUMN txbroodermachine.wednesday    COMMENT     D   COMMENT ON COLUMN public.txbroodermachine.wednesday IS 'Miercoles';
            public       postgres    false    303            �           0    0     COLUMN txbroodermachine.thursday    COMMENT     @   COMMENT ON COLUMN public.txbroodermachine.thursday IS 'Jueves';
            public       postgres    false    303            �           0    0    COLUMN txbroodermachine.friday    COMMENT     ?   COMMENT ON COLUMN public.txbroodermachine.friday IS 'Viernes';
            public       postgres    false    303            �           0    0     COLUMN txbroodermachine.saturday    COMMENT     @   COMMENT ON COLUMN public.txbroodermachine.saturday IS 'Sabado';
            public       postgres    false    303            �           0    0    COLUMN txbroodermachine.name    COMMENT     J   COMMENT ON COLUMN public.txbroodermachine.name IS 'Nombre de la maquina';
            public       postgres    false    303            0           1259    27659 
   txcalendar    TABLE     |  CREATE TABLE public.txcalendar (
    calendar_id integer DEFAULT nextval('public.calendar_id_seq'::regclass) NOT NULL,
    description character varying(250) NOT NULL,
    saturday character varying(15),
    sunday character varying(15),
    week_start character varying(15),
    code character(20) NOT NULL,
    year_start integer,
    year_end integer,
    generated integer
);
    DROP TABLE public.txcalendar;
       public         postgres    false    228    3            �           0    0    TABLE txcalendar    COMMENT     n   COMMENT ON TABLE public.txcalendar IS 'Almacena la informacion del calendario con la que trabaja el sistema';
            public       postgres    false    304            �           0    0    COLUMN txcalendar.calendar_id    COMMENT     H   COMMENT ON COLUMN public.txcalendar.calendar_id IS 'Id del calendario';
            public       postgres    false    304            �           0    0    COLUMN txcalendar.description    COMMENT     S   COMMENT ON COLUMN public.txcalendar.description IS 'Descripción del calendario
';
            public       postgres    false    304            �           0    0    COLUMN txcalendar.saturday    COMMENT     Z   COMMENT ON COLUMN public.txcalendar.saturday IS 'Indica si el día sábado es laborable';
            public       postgres    false    304            �           0    0    COLUMN txcalendar.sunday    COMMENT     X   COMMENT ON COLUMN public.txcalendar.sunday IS 'Indica si el día Domingo es laborable';
            public       postgres    false    304            �           0    0    COLUMN txcalendar.week_start    COMMENT     [   COMMENT ON COLUMN public.txcalendar.week_start IS 'Semana en la que inicia el calendario';
            public       postgres    false    304            �           0    0    COLUMN txcalendar.code    COMMENT     E   COMMENT ON COLUMN public.txcalendar.code IS 'Codigo del calendario';
            public       postgres    false    304            �           0    0    COLUMN txcalendar.year_start    COMMENT     Y   COMMENT ON COLUMN public.txcalendar.year_start IS 'Año en el que inicia el calendario';
            public       postgres    false    304            �           0    0    COLUMN txcalendar.year_end    COMMENT     Y   COMMENT ON COLUMN public.txcalendar.year_end IS 'Año en el que finaliza el calendario';
            public       postgres    false    304            �           0    0    COLUMN txcalendar.generated    COMMENT     u   COMMENT ON COLUMN public.txcalendar.generated IS 'Indica si el calendario fue generado a partir de otro calendario';
            public       postgres    false    304            1           1259    27663    txcalendarday    TABLE     �  CREATE TABLE public.txcalendarday (
    calendar_day_id integer DEFAULT nextval('public.calendar_day_id_seq'::regclass) NOT NULL,
    calendar_id integer NOT NULL,
    use_date timestamp with time zone NOT NULL,
    use_year integer NOT NULL,
    use_month integer NOT NULL,
    use_day integer NOT NULL,
    use_week timestamp with time zone NOT NULL,
    week_day integer NOT NULL,
    sequence integer NOT NULL,
    working_day integer NOT NULL
);
 !   DROP TABLE public.txcalendarday;
       public         postgres    false    227    3            �           0    0    TABLE txcalendarday    COMMENT     _   COMMENT ON TABLE public.txcalendarday IS 'Almacena los datos de los dias que son laborables ';
            public       postgres    false    305            �           0    0 $   COLUMN txcalendarday.calendar_day_id    COMMENT     W   COMMENT ON COLUMN public.txcalendarday.calendar_day_id IS 'Id del dia del calendario';
            public       postgres    false    305            �           0    0     COLUMN txcalendarday.calendar_id    COMMENT     K   COMMENT ON COLUMN public.txcalendarday.calendar_id IS 'Id del calendario';
            public       postgres    false    305            �           0    0    COLUMN txcalendarday.use_date    COMMENT     d   COMMENT ON COLUMN public.txcalendarday.use_date IS 'Fecha en el que se encuentra el dia laborable';
            public       postgres    false    305            �           0    0    COLUMN txcalendarday.use_year    COMMENT     c   COMMENT ON COLUMN public.txcalendarday.use_year IS 'Año en el que se encuentra el dia laborable';
            public       postgres    false    305            �           0    0    COLUMN txcalendarday.use_month    COMMENT     c   COMMENT ON COLUMN public.txcalendarday.use_month IS 'Mes en el que se encuentra el dia laborable';
            public       postgres    false    305            �           0    0    COLUMN txcalendarday.use_day    COMMENT     a   COMMENT ON COLUMN public.txcalendarday.use_day IS 'Dia en el que se encuentra el dia laborable';
            public       postgres    false    305            �           0    0    COLUMN txcalendarday.use_week    COMMENT     e   COMMENT ON COLUMN public.txcalendarday.use_week IS 'Semana en el que se encuentra el dia laborable';
            public       postgres    false    305            �           0    0    COLUMN txcalendarday.week_day    COMMENT     l   COMMENT ON COLUMN public.txcalendarday.week_day IS 'Dia de semana en el que se encuentra el dia laborable';
            public       postgres    false    305            �           0    0     COLUMN txcalendarday.working_day    COMMENT     Z   COMMENT ON COLUMN public.txcalendarday.working_day IS 'Indica si el dia es laboral o no';
            public       postgres    false    305            2           1259    27667    txeggs_movements_id_seq    SEQUENCE     �   CREATE SEQUENCE public.txeggs_movements_id_seq
    START WITH 170
    INCREMENT BY 2041
    NO MINVALUE
    MAXVALUE 9999999999999999
    CACHE 1;
 .   DROP SEQUENCE public.txeggs_movements_id_seq;
       public       postgres    false    3            3           1259    27669    txeggs_movements    TABLE     l  CREATE TABLE public.txeggs_movements (
    eggs_movements_id integer DEFAULT nextval('public.txeggs_movements_id_seq'::regclass) NOT NULL,
    fecha_movements date NOT NULL,
    lot integer NOT NULL,
    quantity integer NOT NULL,
    type_movements character varying NOT NULL,
    eggs_storage_id integer NOT NULL,
    description_adjustment character varying
);
 $   DROP TABLE public.txeggs_movements;
       public         postgres    false    306    3            4           1259    27676    txeggs_planning    TABLE       CREATE TABLE public.txeggs_planning (
    egg_planning_id integer DEFAULT nextval('public.egg_planning_id_seq'::regclass) NOT NULL,
    month_planning integer,
    year_planning integer,
    scenario_id integer,
    planned double precision,
    breed_id integer NOT NULL
);
 #   DROP TABLE public.txeggs_planning;
       public         postgres    false    230    3            �           0    0    TABLE txeggs_planning    COMMENT     g   COMMENT ON TABLE public.txeggs_planning IS 'Almacena los detalles de la planificación de los huevos';
            public       postgres    false    308            �           0    0 &   COLUMN txeggs_planning.egg_planning_id    COMMENT     [   COMMENT ON COLUMN public.txeggs_planning.egg_planning_id IS 'Id de planeación de huevos';
            public       postgres    false    308            �           0    0 %   COLUMN txeggs_planning.month_planning    COMMENT     c   COMMENT ON COLUMN public.txeggs_planning.month_planning IS 'Mes de planificación de los huevos
';
            public       postgres    false    308            �           0    0 $   COLUMN txeggs_planning.year_planning    COMMENT     b   COMMENT ON COLUMN public.txeggs_planning.year_planning IS 'Año de planificación de los huevos';
            public       postgres    false    308            �           0    0 "   COLUMN txeggs_planning.scenario_id    COMMENT     p   COMMENT ON COLUMN public.txeggs_planning.scenario_id IS 'Escenario al cual pertenecen los huevos planificados';
            public       postgres    false    308            �           0    0    COLUMN txeggs_planning.planned    COMMENT     X   COMMENT ON COLUMN public.txeggs_planning.planned IS 'Cantidad de huevos planificados
';
            public       postgres    false    308            �           0    0    COLUMN txeggs_planning.breed_id    COMMENT     T   COMMENT ON COLUMN public.txeggs_planning.breed_id IS 'Id de la raza de los huevos';
            public       postgres    false    308            5           1259    27680    txeggs_required    TABLE     
  CREATE TABLE public.txeggs_required (
    egg_required_id integer DEFAULT nextval('public.egg_required_id_seq'::regclass) NOT NULL,
    use_month integer,
    use_year integer,
    scenario_id integer NOT NULL,
    required double precision,
    breed_id integer
);
 #   DROP TABLE public.txeggs_required;
       public         postgres    false    231    3            �           0    0    TABLE txeggs_required    COMMENT     V   COMMENT ON TABLE public.txeggs_required IS 'Almacena los datos de huevos requeridos';
            public       postgres    false    309            �           0    0 &   COLUMN txeggs_required.egg_required_id    COMMENT     [   COMMENT ON COLUMN public.txeggs_required.egg_required_id IS 'Id de los huevos requeridos';
            public       postgres    false    309            �           0    0     COLUMN txeggs_required.use_month    COMMENT     =   COMMENT ON COLUMN public.txeggs_required.use_month IS 'Mes';
            public       postgres    false    309            �           0    0    COLUMN txeggs_required.use_year    COMMENT     =   COMMENT ON COLUMN public.txeggs_required.use_year IS 'Año';
            public       postgres    false    309            �           0    0 "   COLUMN txeggs_required.scenario_id    COMMENT     L   COMMENT ON COLUMN public.txeggs_required.scenario_id IS 'Id del escenario';
            public       postgres    false    309            �           0    0    COLUMN txeggs_required.required    COMMENT     K   COMMENT ON COLUMN public.txeggs_required.required IS 'Cantidad requerida';
            public       postgres    false    309            �           0    0    COLUMN txeggs_required.breed_id    COMMENT     F   COMMENT ON COLUMN public.txeggs_required.breed_id IS 'Id de la raza';
            public       postgres    false    309            6           1259    27684    txeggs_storage    TABLE     b  CREATE TABLE public.txeggs_storage (
    eggs_storage_id integer DEFAULT nextval('public.eggs_storage_id_seq'::regclass) NOT NULL,
    incubator_plant_id integer NOT NULL,
    scenario_id integer NOT NULL,
    breed_id integer NOT NULL,
    init_date date,
    end_date date,
    lot character varying(45),
    eggs integer,
    eggs_executed integer
);
 "   DROP TABLE public.txeggs_storage;
       public         postgres    false    232    3            �           0    0    TABLE txeggs_storage    COMMENT     ~   COMMENT ON TABLE public.txeggs_storage IS 'Guarda la informacion de almacenamiento de los huevos en las plantas incubadoras';
            public       postgres    false    310            �           0    0 %   COLUMN txeggs_storage.eggs_storage_id    COMMENT     W   COMMENT ON COLUMN public.txeggs_storage.eggs_storage_id IS 'Id del almacen de huevos';
            public       postgres    false    310            �           0    0 (   COLUMN txeggs_storage.incubator_plant_id    COMMENT     Y   COMMENT ON COLUMN public.txeggs_storage.incubator_plant_id IS 'Id de planta incubadora';
            public       postgres    false    310            �           0    0 !   COLUMN txeggs_storage.scenario_id    COMMENT     K   COMMENT ON COLUMN public.txeggs_storage.scenario_id IS 'Id del escenario';
            public       postgres    false    310            �           0    0    COLUMN txeggs_storage.breed_id    COMMENT     E   COMMENT ON COLUMN public.txeggs_storage.breed_id IS 'Id de la raza';
            public       postgres    false    310            �           0    0    COLUMN txeggs_storage.init_date    COMMENT     H   COMMENT ON COLUMN public.txeggs_storage.init_date IS 'Fecha de inicio';
            public       postgres    false    310            �           0    0    COLUMN txeggs_storage.end_date    COMMENT     J   COMMENT ON COLUMN public.txeggs_storage.end_date IS 'Fecha de terminado';
            public       postgres    false    310            �           0    0    COLUMN txeggs_storage.lot    COMMENT     7   COMMENT ON COLUMN public.txeggs_storage.lot IS 'Lote';
            public       postgres    false    310            �           0    0    COLUMN txeggs_storage.eggs    COMMENT     F   COMMENT ON COLUMN public.txeggs_storage.eggs IS 'Cantidad de huevos';
            public       postgres    false    310            7           1259    27688    txgoals_erp    TABLE     �   CREATE TABLE public.txgoals_erp (
    goals_erp_id bigint NOT NULL,
    use_week date,
    use_value integer,
    product_id integer NOT NULL,
    code character varying(10),
    scenario_id integer NOT NULL
);
    DROP TABLE public.txgoals_erp;
       public         postgres    false    3            �           0    0    TABLE txgoals_erp    COMMENT     �   COMMENT ON TABLE public.txgoals_erp IS 'Almacena los datos generados de las metas de producción de la planificación regresiva para ser enviados al ERP';
            public       postgres    false    311            �           0    0    COLUMN txgoals_erp.goals_erp_id    COMMENT     N   COMMENT ON COLUMN public.txgoals_erp.goals_erp_id IS 'Id de la meta del ERP';
            public       postgres    false    311            �           0    0    COLUMN txgoals_erp.use_week    COMMENT     ;   COMMENT ON COLUMN public.txgoals_erp.use_week IS 'Semana';
            public       postgres    false    311            �           0    0    COLUMN txgoals_erp.use_value    COMMENT     D   COMMENT ON COLUMN public.txgoals_erp.use_value IS 'Valor objetivo';
            public       postgres    false    311            �           0    0    COLUMN txgoals_erp.product_id    COMMENT     F   COMMENT ON COLUMN public.txgoals_erp.product_id IS 'Id del producto';
            public       postgres    false    311            �           0    0    COLUMN txgoals_erp.code    COMMENT     D   COMMENT ON COLUMN public.txgoals_erp.code IS 'Codigo del producto';
            public       postgres    false    311            �           0    0    COLUMN txgoals_erp.scenario_id    COMMENT     H   COMMENT ON COLUMN public.txgoals_erp.scenario_id IS 'Id del escenario';
            public       postgres    false    311            8           1259    27691    txgoals_erp_goals_erp_id_seq    SEQUENCE     �   CREATE SEQUENCE public.txgoals_erp_goals_erp_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.txgoals_erp_goals_erp_id_seq;
       public       postgres    false    311    3                        0    0    txgoals_erp_goals_erp_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.txgoals_erp_goals_erp_id_seq OWNED BY public.txgoals_erp.goals_erp_id;
            public       postgres    false    312            9           1259    27693    txhousingway    TABLE     d  CREATE TABLE public.txhousingway (
    housing_way_id integer DEFAULT nextval('public.housing_way_id_seq'::regclass) NOT NULL,
    projected_quantity integer,
    projected_date date,
    stage_id integer NOT NULL,
    partnership_id integer NOT NULL,
    scenario_id integer NOT NULL,
    breed_id integer NOT NULL,
    predecessor_id integer NOT NULL
);
     DROP TABLE public.txhousingway;
       public         postgres    false    237    3                       0    0    TABLE txhousingway    COMMENT     t   COMMENT ON TABLE public.txhousingway IS 'Almacena la proyección de los módulos de levante, cría y reproductora';
            public       postgres    false    313                       0    0 "   COLUMN txhousingway.housing_way_id    COMMENT     �   COMMENT ON COLUMN public.txhousingway.housing_way_id IS 'Id de las proyecciones  de los módulos de levante, cría y reproductora';
            public       postgres    false    313                       0    0 &   COLUMN txhousingway.projected_quantity    COMMENT     S   COMMENT ON COLUMN public.txhousingway.projected_quantity IS 'Cantidad proyectada';
            public       postgres    false    313                       0    0 "   COLUMN txhousingway.projected_date    COMMENT     L   COMMENT ON COLUMN public.txhousingway.projected_date IS 'Fecha proyectada';
            public       postgres    false    313                       0    0    COLUMN txhousingway.stage_id    COMMENT     D   COMMENT ON COLUMN public.txhousingway.stage_id IS 'Id de la etapa';
            public       postgres    false    313                       0    0 "   COLUMN txhousingway.partnership_id    COMMENT     L   COMMENT ON COLUMN public.txhousingway.partnership_id IS 'Id de la empresa';
            public       postgres    false    313                       0    0    COLUMN txhousingway.breed_id    COMMENT     C   COMMENT ON COLUMN public.txhousingway.breed_id IS 'Id de la raza';
            public       postgres    false    313                       0    0 "   COLUMN txhousingway.predecessor_id    COMMENT     N   COMMENT ON COLUMN public.txhousingway.predecessor_id IS 'Id del predecesor ';
            public       postgres    false    313            :           1259    27697    txhousingway_detail    TABLE     /  CREATE TABLE public.txhousingway_detail (
    housingway_detail_id integer DEFAULT nextval('public.housing_way_detail_id_seq'::regclass) NOT NULL,
    housing_way_id integer NOT NULL,
    scheduled_date date,
    scheduled_quantity integer,
    farm_id integer NOT NULL,
    shed_id integer NOT NULL,
    confirm integer,
    execution_date date,
    execution_quantity integer,
    lot character varying(45),
    incubator_plant_id integer,
    center_id integer,
    executionfarm_id integer,
    executioncenter_id integer,
    executionshed_id integer
);
 '   DROP TABLE public.txhousingway_detail;
       public         postgres    false    236    3            	           0    0    TABLE txhousingway_detail    COMMENT     �   COMMENT ON TABLE public.txhousingway_detail IS 'Almacena la programación y la ejecución de los módulos de levante y cría y reproductora';
            public       postgres    false    314            
           0    0 /   COLUMN txhousingway_detail.housingway_detail_id    COMMENT     �   COMMENT ON COLUMN public.txhousingway_detail.housingway_detail_id IS 'Id de la programación y ejecución de los modelos de levante y cría y reproductora';
            public       postgres    false    314                       0    0 )   COLUMN txhousingway_detail.housing_way_id    COMMENT     �   COMMENT ON COLUMN public.txhousingway_detail.housing_way_id IS 'Id de las proyecciones  de los módulos de levante, cría y reproductora';
            public       postgres    false    314                       0    0 )   COLUMN txhousingway_detail.scheduled_date    COMMENT     S   COMMENT ON COLUMN public.txhousingway_detail.scheduled_date IS 'Fecha programada';
            public       postgres    false    314                       0    0 -   COLUMN txhousingway_detail.scheduled_quantity    COMMENT     Z   COMMENT ON COLUMN public.txhousingway_detail.scheduled_quantity IS 'Cantidad programada';
            public       postgres    false    314                       0    0 "   COLUMN txhousingway_detail.farm_id    COMMENT     K   COMMENT ON COLUMN public.txhousingway_detail.farm_id IS 'Id de la granja';
            public       postgres    false    314                       0    0 "   COLUMN txhousingway_detail.shed_id    COMMENT     S   COMMENT ON COLUMN public.txhousingway_detail.shed_id IS 'Id del galpon utilizado';
            public       postgres    false    314                       0    0 "   COLUMN txhousingway_detail.confirm    COMMENT     [   COMMENT ON COLUMN public.txhousingway_detail.confirm IS 'Confirmacion de sincronizacion ';
            public       postgres    false    314                       0    0 )   COLUMN txhousingway_detail.execution_date    COMMENT     V   COMMENT ON COLUMN public.txhousingway_detail.execution_date IS 'Fecha de ejecución';
            public       postgres    false    314                       0    0 -   COLUMN txhousingway_detail.execution_quantity    COMMENT     Z   COMMENT ON COLUMN public.txhousingway_detail.execution_quantity IS 'Cantidad a ejecutar';
            public       postgres    false    314                       0    0    COLUMN txhousingway_detail.lot    COMMENT     I   COMMENT ON COLUMN public.txhousingway_detail.lot IS 'Lote seleccionado';
            public       postgres    false    314                       0    0 -   COLUMN txhousingway_detail.incubator_plant_id    COMMENT     a   COMMENT ON COLUMN public.txhousingway_detail.incubator_plant_id IS 'Id de la planta incubadora';
            public       postgres    false    314            ;           1259    27701    txlot    TABLE     n  CREATE TABLE public.txlot (
    lot_id integer DEFAULT nextval('public.lot_id_seq'::regclass) NOT NULL,
    lot_code character varying(20) NOT NULL,
    lot_origin character varying(150),
    status integer,
    proyected_date date,
    sheduled_date date,
    proyected_quantity integer,
    sheduled_quantity integer,
    released_quantity integer,
    product_id integer NOT NULL,
    breed_id integer NOT NULL,
    gender character varying(30),
    type_posture character varying(30),
    shed_id integer NOT NULL,
    origin character varying(30),
    farm_id integer NOT NULL,
    housing_way_id integer NOT NULL
);
    DROP TABLE public.txlot;
       public         postgres    false    244    3                       0    0    TABLE txlot    COMMENT     T   COMMENT ON TABLE public.txlot IS 'Almacena la informacion de los diferentes lotes';
            public       postgres    false    315                       0    0    COLUMN txlot.lot_id    COMMENT     8   COMMENT ON COLUMN public.txlot.lot_id IS 'Id del lote';
            public       postgres    false    315                       0    0    COLUMN txlot.lot_code    COMMENT     >   COMMENT ON COLUMN public.txlot.lot_code IS 'Codigo del lote';
            public       postgres    false    315                       0    0    COLUMN txlot.lot_origin    COMMENT     @   COMMENT ON COLUMN public.txlot.lot_origin IS 'Origen del lote';
            public       postgres    false    315                       0    0    COLUMN txlot.status    COMMENT     <   COMMENT ON COLUMN public.txlot.status IS 'Estado del lote';
            public       postgres    false    315                       0    0    COLUMN txlot.proyected_date    COMMENT     E   COMMENT ON COLUMN public.txlot.proyected_date IS 'Fecha proyectada';
            public       postgres    false    315                       0    0    COLUMN txlot.sheduled_date    COMMENT     D   COMMENT ON COLUMN public.txlot.sheduled_date IS 'Fecha programada';
            public       postgres    false    315                       0    0    COLUMN txlot.proyected_quantity    COMMENT     L   COMMENT ON COLUMN public.txlot.proyected_quantity IS 'Cantidad proyectada';
            public       postgres    false    315                       0    0    COLUMN txlot.sheduled_quantity    COMMENT     K   COMMENT ON COLUMN public.txlot.sheduled_quantity IS 'Cantidad programada';
            public       postgres    false    315                       0    0    COLUMN txlot.released_quantity    COMMENT     I   COMMENT ON COLUMN public.txlot.released_quantity IS 'Cantidad liberada';
            public       postgres    false    315                       0    0    COLUMN txlot.product_id    COMMENT     @   COMMENT ON COLUMN public.txlot.product_id IS 'Id del producto';
            public       postgres    false    315                        0    0    COLUMN txlot.breed_id    COMMENT     <   COMMENT ON COLUMN public.txlot.breed_id IS 'Id de la raza';
            public       postgres    false    315            !           0    0    COLUMN txlot.gender    COMMENT     <   COMMENT ON COLUMN public.txlot.gender IS 'Genero del lote';
            public       postgres    false    315            "           0    0    COLUMN txlot.type_posture    COMMENT     B   COMMENT ON COLUMN public.txlot.type_posture IS 'Tipo de postura';
            public       postgres    false    315            #           0    0    COLUMN txlot.shed_id    COMMENT     ;   COMMENT ON COLUMN public.txlot.shed_id IS 'Id del galpon';
            public       postgres    false    315            $           0    0    COLUMN txlot.origin    COMMENT     3   COMMENT ON COLUMN public.txlot.origin IS 'Origen';
            public       postgres    false    315            %           0    0    COLUMN txlot.farm_id    COMMENT     =   COMMENT ON COLUMN public.txlot.farm_id IS 'Id de la granja';
            public       postgres    false    315            &           0    0    COLUMN txlot.housing_way_id    COMMENT     ~   COMMENT ON COLUMN public.txlot.housing_way_id IS 'Id del almacenamientos de la proyecciones de levante, cria y reproductora';
            public       postgres    false    315            <           1259    27705 
   txlot_eggs    TABLE     �   CREATE TABLE public.txlot_eggs (
    lot_eggs_id integer DEFAULT nextval('public.lot_eggs_id_seq'::regclass) NOT NULL,
    theorical_performance double precision,
    week_date date,
    week integer
);
    DROP TABLE public.txlot_eggs;
       public         postgres    false    242    3            '           0    0    TABLE txlot_eggs    COMMENT     S   COMMENT ON TABLE public.txlot_eggs IS 'Almacena los datos de los lotes de huevos';
            public       postgres    false    316            (           0    0    COLUMN txlot_eggs.lot_eggs_id    COMMENT     L   COMMENT ON COLUMN public.txlot_eggs.lot_eggs_id IS 'Id del lote de huevos';
            public       postgres    false    316            )           0    0 '   COLUMN txlot_eggs.theorical_performance    COMMENT     T   COMMENT ON COLUMN public.txlot_eggs.theorical_performance IS 'Rendimiento teorico';
            public       postgres    false    316            *           0    0    COLUMN txlot_eggs.week_date    COMMENT     G   COMMENT ON COLUMN public.txlot_eggs.week_date IS 'Fecha de la semana';
            public       postgres    false    316            +           0    0    COLUMN txlot_eggs.week    COMMENT     6   COMMENT ON COLUMN public.txlot_eggs.week IS 'Semana';
            public       postgres    false    316            =           1259    27709    txposturecurve    TABLE     �  CREATE TABLE public.txposturecurve (
    posture_curve_id integer DEFAULT nextval('public.posture_curve_id_seq'::regclass) NOT NULL,
    week integer NOT NULL,
    breed_id integer NOT NULL,
    theorical_performance double precision NOT NULL,
    historical_performance double precision,
    theorical_accum_mortality integer,
    historical_accum_mortality integer,
    theorical_uniformity double precision,
    historical_uniformity double precision,
    type_posture character varying(30) NOT NULL
);
 "   DROP TABLE public.txposturecurve;
       public         postgres    false    287    3            ,           0    0    TABLE txposturecurve    COMMENT        COMMENT ON TABLE public.txposturecurve IS 'Almacena la información de la curva de postura por cada raza separada por semana';
            public       postgres    false    317            -           0    0 &   COLUMN txposturecurve.posture_curve_id    COMMENT     Y   COMMENT ON COLUMN public.txposturecurve.posture_curve_id IS 'Id de la curva de postura';
            public       postgres    false    317            .           0    0    COLUMN txposturecurve.week    COMMENT     _   COMMENT ON COLUMN public.txposturecurve.week IS 'Semana en la que inicia la curva de postura';
            public       postgres    false    317            /           0    0    COLUMN txposturecurve.breed_id    COMMENT     P   COMMENT ON COLUMN public.txposturecurve.breed_id IS 'Identificador de la raza';
            public       postgres    false    317            0           0    0 +   COLUMN txposturecurve.theorical_performance    COMMENT     X   COMMENT ON COLUMN public.txposturecurve.theorical_performance IS 'Desempeño teórico';
            public       postgres    false    317            1           0    0 ,   COLUMN txposturecurve.historical_performance    COMMENT     [   COMMENT ON COLUMN public.txposturecurve.historical_performance IS 'Desempeño histórico';
            public       postgres    false    317            2           0    0 /   COLUMN txposturecurve.theorical_accum_mortality    COMMENT     h   COMMENT ON COLUMN public.txposturecurve.theorical_accum_mortality IS 'Acumulado de mortalidad teorico';
            public       postgres    false    317            3           0    0 0   COLUMN txposturecurve.historical_accum_mortality    COMMENT     k   COMMENT ON COLUMN public.txposturecurve.historical_accum_mortality IS 'Acumulado de mortalidad historico';
            public       postgres    false    317            4           0    0 *   COLUMN txposturecurve.theorical_uniformity    COMMENT     W   COMMENT ON COLUMN public.txposturecurve.theorical_uniformity IS 'Uniformidad teorica';
            public       postgres    false    317            5           0    0 +   COLUMN txposturecurve.historical_uniformity    COMMENT     Z   COMMENT ON COLUMN public.txposturecurve.historical_uniformity IS 'Uniformidad historica';
            public       postgres    false    317            6           0    0 "   COLUMN txposturecurve.type_posture    COMMENT     K   COMMENT ON COLUMN public.txposturecurve.type_posture IS 'Tipo de postura';
            public       postgres    false    317            >           1259    27713    txprogrammed_eggs    TABLE     �  CREATE TABLE public.txprogrammed_eggs (
    programmed_eggs_id integer DEFAULT nextval('public.programmed_eggs_id_seq'::regclass) NOT NULL,
    incubator_id integer NOT NULL,
    lot_breed character varying(45),
    lot_incubator character varying(45),
    use_date date,
    eggs integer,
    breed_id integer NOT NULL,
    execution_quantity integer,
    eggs_storage_id integer NOT NULL,
    confirm integer,
    released boolean,
    eggs_movements_id integer
);
 %   DROP TABLE public.txprogrammed_eggs;
       public         postgres    false    290    3            7           0    0    TABLE txprogrammed_eggs    COMMENT        COMMENT ON TABLE public.txprogrammed_eggs IS 'Almacena la proyección, programación y ejecución del módulo de incubadoras';
            public       postgres    false    318            8           0    0 +   COLUMN txprogrammed_eggs.programmed_eggs_id    COMMENT     j   COMMENT ON COLUMN public.txprogrammed_eggs.programmed_eggs_id IS 'Id de las programacion de incubadoras';
            public       postgres    false    318            9           0    0 %   COLUMN txprogrammed_eggs.incubator_id    COMMENT     O   COMMENT ON COLUMN public.txprogrammed_eggs.incubator_id IS 'Id de incubadora';
            public       postgres    false    318            :           0    0 "   COLUMN txprogrammed_eggs.lot_breed    COMMENT     I   COMMENT ON COLUMN public.txprogrammed_eggs.lot_breed IS 'Lote por raza';
            public       postgres    false    318            ;           0    0 &   COLUMN txprogrammed_eggs.lot_incubator    COMMENT     S   COMMENT ON COLUMN public.txprogrammed_eggs.lot_incubator IS 'Lote de incubadoras';
            public       postgres    false    318            <           0    0    COLUMN txprogrammed_eggs.eggs    COMMENT     I   COMMENT ON COLUMN public.txprogrammed_eggs.eggs IS 'Cantidad de huevos';
            public       postgres    false    318            =           0    0 !   COLUMN txprogrammed_eggs.breed_id    COMMENT     E   COMMENT ON COLUMN public.txprogrammed_eggs.breed_id IS 'Id de raza';
            public       postgres    false    318            >           0    0 +   COLUMN txprogrammed_eggs.execution_quantity    COMMENT     [   COMMENT ON COLUMN public.txprogrammed_eggs.execution_quantity IS 'Cantidad de ejecución';
            public       postgres    false    318            ?           1259    27717    txscenarioformula    TABLE     �  CREATE TABLE public.txscenarioformula (
    scenario_formula_id integer DEFAULT nextval('public.scenario_formula_id_seq'::regclass) NOT NULL,
    process_id integer NOT NULL,
    predecessor_id integer NOT NULL,
    parameter_id integer NOT NULL,
    sign integer,
    divider double precision,
    duration integer,
    scenario_id integer NOT NULL,
    measure_id integer NOT NULL
);
 %   DROP TABLE public.txscenarioformula;
       public         postgres    false    292    3            ?           0    0    TABLE txscenarioformula    COMMENT     �   COMMENT ON TABLE public.txscenarioformula IS 'Almacena los datos para la formulación de salida de la planificación regresiva';
            public       postgres    false    319            @           0    0 ,   COLUMN txscenarioformula.scenario_formula_id    COMMENT     d   COMMENT ON COLUMN public.txscenarioformula.scenario_formula_id IS 'Id de la formula del escenario';
            public       postgres    false    319            A           0    0 #   COLUMN txscenarioformula.process_id    COMMENT     K   COMMENT ON COLUMN public.txscenarioformula.process_id IS 'Id del proceso';
            public       postgres    false    319            B           0    0 '   COLUMN txscenarioformula.predecessor_id    COMMENT     R   COMMENT ON COLUMN public.txscenarioformula.predecessor_id IS 'Id del predecesor';
            public       postgres    false    319            C           0    0 %   COLUMN txscenarioformula.parameter_id    COMMENT     O   COMMENT ON COLUMN public.txscenarioformula.parameter_id IS 'Id del parametro';
            public       postgres    false    319            D           0    0    COLUMN txscenarioformula.sign    COMMENT     E   COMMENT ON COLUMN public.txscenarioformula.sign IS 'Firma de datos';
            public       postgres    false    319            E           0    0     COLUMN txscenarioformula.divider    COMMENT     J   COMMENT ON COLUMN public.txscenarioformula.divider IS 'divisor de datos';
            public       postgres    false    319            F           0    0 !   COLUMN txscenarioformula.duration    COMMENT     Q   COMMENT ON COLUMN public.txscenarioformula.duration IS 'Duracion de la formula';
            public       postgres    false    319            G           0    0 $   COLUMN txscenarioformula.scenario_id    COMMENT     N   COMMENT ON COLUMN public.txscenarioformula.scenario_id IS 'Id del escenario';
            public       postgres    false    319            H           0    0 #   COLUMN txscenarioformula.measure_id    COMMENT     M   COMMENT ON COLUMN public.txscenarioformula.measure_id IS 'Id de la medida
';
            public       postgres    false    319            @           1259    27721    txscenarioparameter    TABLE     c  CREATE TABLE public.txscenarioparameter (
    scenario_parameter_id integer DEFAULT nextval('public.scenario_parameter_id_seq'::regclass) NOT NULL,
    process_id integer NOT NULL,
    parameter_id integer NOT NULL,
    use_year integer,
    use_month integer,
    use_value integer,
    scenario_id integer NOT NULL,
    value_units integer DEFAULT 0
);
 '   DROP TABLE public.txscenarioparameter;
       public         postgres    false    294    3            I           0    0    TABLE txscenarioparameter    COMMENT     s   COMMENT ON TABLE public.txscenarioparameter IS 'Almacena las metas de producción ingresadas para los escenarios';
            public       postgres    false    320            J           0    0 0   COLUMN txscenarioparameter.scenario_parameter_id    COMMENT     l   COMMENT ON COLUMN public.txscenarioparameter.scenario_parameter_id IS 'Id de los parametros del escenario';
            public       postgres    false    320            K           0    0 %   COLUMN txscenarioparameter.process_id    COMMENT     M   COMMENT ON COLUMN public.txscenarioparameter.process_id IS 'Id del proceso';
            public       postgres    false    320            L           0    0 '   COLUMN txscenarioparameter.parameter_id    COMMENT     Q   COMMENT ON COLUMN public.txscenarioparameter.parameter_id IS 'Id del parametro';
            public       postgres    false    320            M           0    0 #   COLUMN txscenarioparameter.use_year    COMMENT     O   COMMENT ON COLUMN public.txscenarioparameter.use_year IS 'Año del parametro';
            public       postgres    false    320            N           0    0 $   COLUMN txscenarioparameter.use_month    COMMENT     O   COMMENT ON COLUMN public.txscenarioparameter.use_month IS 'Mes del parametro';
            public       postgres    false    320            O           0    0 $   COLUMN txscenarioparameter.use_value    COMMENT     Q   COMMENT ON COLUMN public.txscenarioparameter.use_value IS 'Valor del parametro';
            public       postgres    false    320            P           0    0 &   COLUMN txscenarioparameter.scenario_id    COMMENT     P   COMMENT ON COLUMN public.txscenarioparameter.scenario_id IS 'Id del escenario';
            public       postgres    false    320            Q           0    0 &   COLUMN txscenarioparameter.value_units    COMMENT     U   COMMENT ON COLUMN public.txscenarioparameter.value_units IS 'Valor de las unidades';
            public       postgres    false    320            A           1259    27726    txscenarioparameterday    TABLE     {  CREATE TABLE public.txscenarioparameterday (
    scenario_parameter_day_id integer DEFAULT nextval('public.scenario_parameter_day_seq'::regclass) NOT NULL,
    use_day integer,
    parameter_id integer NOT NULL,
    units_day integer,
    scenario_id integer NOT NULL,
    sequence integer,
    use_month integer,
    use_year integer,
    week_day integer,
    use_week date
);
 *   DROP TABLE public.txscenarioparameterday;
       public         postgres    false    293    3            R           0    0    TABLE txscenarioparameterday    COMMENT     V   COMMENT ON TABLE public.txscenarioparameterday IS 'Almcacena los parametros por dia';
            public       postgres    false    321            S           0    0 7   COLUMN txscenarioparameterday.scenario_parameter_day_id    COMMENT     m   COMMENT ON COLUMN public.txscenarioparameterday.scenario_parameter_day_id IS 'Id de los parametros del dia';
            public       postgres    false    321            T           0    0 %   COLUMN txscenarioparameterday.use_day    COMMENT     B   COMMENT ON COLUMN public.txscenarioparameterday.use_day IS 'Dia';
            public       postgres    false    321            U           0    0 *   COLUMN txscenarioparameterday.parameter_id    COMMENT     c   COMMENT ON COLUMN public.txscenarioparameterday.parameter_id IS 'Id de los parametros necesarios';
            public       postgres    false    321            V           0    0 '   COLUMN txscenarioparameterday.units_day    COMMENT     U   COMMENT ON COLUMN public.txscenarioparameterday.units_day IS 'Cantidad de material';
            public       postgres    false    321            W           0    0 )   COLUMN txscenarioparameterday.scenario_id    COMMENT     u   COMMENT ON COLUMN public.txscenarioparameterday.scenario_id IS 'Escenario al cual pertenece el scanrioparameterday';
            public       postgres    false    321            X           0    0 &   COLUMN txscenarioparameterday.sequence    COMMENT     R   COMMENT ON COLUMN public.txscenarioparameterday.sequence IS 'Secuencia del dia
';
            public       postgres    false    321            Y           0    0 '   COLUMN txscenarioparameterday.use_month    COMMENT     ]   COMMENT ON COLUMN public.txscenarioparameterday.use_month IS 'Mes en que se ubica el día ';
            public       postgres    false    321            Z           0    0 &   COLUMN txscenarioparameterday.use_year    COMMENT     ]   COMMENT ON COLUMN public.txscenarioparameterday.use_year IS 'Año en que se ubica el día ';
            public       postgres    false    321            [           0    0 &   COLUMN txscenarioparameterday.week_day    COMMENT     P   COMMENT ON COLUMN public.txscenarioparameterday.week_day IS 'Dia de la semana';
            public       postgres    false    321            \           0    0 &   COLUMN txscenarioparameterday.use_week    COMMENT     F   COMMENT ON COLUMN public.txscenarioparameterday.use_week IS 'Semana';
            public       postgres    false    321            B           1259    27730    txscenarioposturecurve    TABLE     3  CREATE TABLE public.txscenarioposturecurve (
    scenario_posture_id integer DEFAULT nextval('public.scenario_posture_id_seq'::regclass) NOT NULL,
    posture_date date,
    eggs double precision,
    scenario_id integer NOT NULL,
    housingway_detail_id integer NOT NULL,
    breed_id integer NOT NULL
);
 *   DROP TABLE public.txscenarioposturecurve;
       public         postgres    false    295    3            ]           0    0    TABLE txscenarioposturecurve    COMMENT     o   COMMENT ON TABLE public.txscenarioposturecurve IS 'Almacena los datos que se utilizan en la curva de postura';
            public       postgres    false    322            ^           0    0 1   COLUMN txscenarioposturecurve.scenario_posture_id    COMMENT     i   COMMENT ON COLUMN public.txscenarioposturecurve.scenario_posture_id IS 'Id de la postura del escenario';
            public       postgres    false    322            _           0    0 *   COLUMN txscenarioposturecurve.posture_date    COMMENT     W   COMMENT ON COLUMN public.txscenarioposturecurve.posture_date IS 'Fecha de la postura';
            public       postgres    false    322            `           0    0 "   COLUMN txscenarioposturecurve.eggs    COMMENT     N   COMMENT ON COLUMN public.txscenarioposturecurve.eggs IS 'Cantidad de huevos';
            public       postgres    false    322            a           0    0 )   COLUMN txscenarioposturecurve.scenario_id    COMMENT     R   COMMENT ON COLUMN public.txscenarioposturecurve.scenario_id IS 'Id del scenario';
            public       postgres    false    322            b           0    0 2   COLUMN txscenarioposturecurve.housingway_detail_id    COMMENT     �   COMMENT ON COLUMN public.txscenarioposturecurve.housingway_detail_id IS 'Id de la programación y ejecución de los modelos de levante y cría y reproductora';
            public       postgres    false    322            c           0    0 &   COLUMN txscenarioposturecurve.breed_id    COMMENT     M   COMMENT ON COLUMN public.txscenarioposturecurve.breed_id IS 'Id de la raza';
            public       postgres    false    322            C           1259    27734    txscenarioprocess    TABLE     4  CREATE TABLE public.txscenarioprocess (
    scenario_process_id integer DEFAULT nextval('public.scenario_process_id_seq'::regclass) NOT NULL,
    process_id integer NOT NULL,
    decrease_goal double precision,
    weight_goal double precision,
    duration_goal integer,
    scenario_id integer NOT NULL
);
 %   DROP TABLE public.txscenarioprocess;
       public         postgres    false    296    3            d           0    0    TABLE txscenarioprocess    COMMENT     m   COMMENT ON TABLE public.txscenarioprocess IS 'Almacena los procesos asociados a cada uno de los escenarios';
            public       postgres    false    323            e           0    0 ,   COLUMN txscenarioprocess.scenario_process_id    COMMENT     a   COMMENT ON COLUMN public.txscenarioprocess.scenario_process_id IS 'Id del proceso de escenario';
            public       postgres    false    323            f           0    0 #   COLUMN txscenarioprocess.process_id    COMMENT     V   COMMENT ON COLUMN public.txscenarioprocess.process_id IS 'Id del proceso a utilizar';
            public       postgres    false    323            g           0    0 &   COLUMN txscenarioprocess.decrease_goal    COMMENT     v   COMMENT ON COLUMN public.txscenarioprocess.decrease_goal IS 'Guarda los datos de la merma historia en dicho proceso';
            public       postgres    false    323            h           0    0 $   COLUMN txscenarioprocess.weight_goal    COMMENT     q   COMMENT ON COLUMN public.txscenarioprocess.weight_goal IS 'Guarda los datos del peso historio en dicho proceso';
            public       postgres    false    323            i           0    0 &   COLUMN txscenarioprocess.duration_goal    COMMENT     y   COMMENT ON COLUMN public.txscenarioprocess.duration_goal IS 'Guarda los datos de la duracion historia en dicho proceso';
            public       postgres    false    323            j           0    0 $   COLUMN txscenarioprocess.scenario_id    COMMENT     X   COMMENT ON COLUMN public.txscenarioprocess.scenario_id IS 'Id del escenario utilizado';
            public       postgres    false    323            D           1259    27738 #   user_application_application_id_seq    SEQUENCE     �   CREATE SEQUENCE public.user_application_application_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 :   DROP SEQUENCE public.user_application_application_id_seq;
       public       postgres    false    3            E           1259    27740     user_application_user_app_id_seq    SEQUENCE     �   CREATE SEQUENCE public.user_application_user_app_id_seq
    START WITH 215
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 7   DROP SEQUENCE public.user_application_user_app_id_seq;
       public       postgres    false    3            F           1259    27742    user_application_user_id_seq    SEQUENCE     �   CREATE SEQUENCE public.user_application_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 3   DROP SEQUENCE public.user_application_user_id_seq;
       public       postgres    false    3                       2604    27744    txgoals_erp goals_erp_id    DEFAULT     �   ALTER TABLE ONLY public.txgoals_erp ALTER COLUMN goals_erp_id SET DEFAULT nextval('public.txgoals_erp_goals_erp_id_seq'::regclass);
 G   ALTER TABLE public.txgoals_erp ALTER COLUMN goals_erp_id DROP DEFAULT;
       public       postgres    false    312    311                       0    27347    aba_breeds_and_stages 
   TABLE DATA               m   COPY public.aba_breeds_and_stages (id, code, name, id_aba_consumption_and_mortality, id_process) FROM stdin;
    public       postgres    false    198   ��      "          0    27353    aba_consumption_and_mortality 
   TABLE DATA               m   COPY public.aba_consumption_and_mortality (id, code, name, id_breed, id_stage, id_aba_time_unit) FROM stdin;
    public       postgres    false    200   ��      $          0    27359 $   aba_consumption_and_mortality_detail 
   TABLE DATA               �   COPY public.aba_consumption_and_mortality_detail (id, id_aba_consumption_and_mortality, time_unit_number, consumption, mortality) FROM stdin;
    public       postgres    false    202   ��      &          0    27365    aba_elements 
   TABLE DATA               d   COPY public.aba_elements (id, code, name, id_aba_element_property, equivalent_quantity) FROM stdin;
    public       postgres    false    204   ��      (          0    27371    aba_elements_and_concentrations 
   TABLE DATA               �   COPY public.aba_elements_and_concentrations (id, id_aba_element, id_aba_formulation, proportion, id_element_equivalent, id_aba_element_property, equivalent_quantity) FROM stdin;
    public       postgres    false    206   ��      *          0    27377    aba_elements_properties 
   TABLE DATA               A   COPY public.aba_elements_properties (id, code, name) FROM stdin;
    public       postgres    false    208   �      ,          0    27383    aba_formulation 
   TABLE DATA               9   COPY public.aba_formulation (id, code, name) FROM stdin;
    public       postgres    false    210   X�      .          0    27389    aba_results 
   TABLE DATA               C   COPY public.aba_results (id, id_aba_element, quantity) FROM stdin;
    public       postgres    false    212   u�      0          0    27395    aba_stages_of_breeds_and_stages 
   TABLE DATA               w   COPY public.aba_stages_of_breeds_and_stages (id, id_aba_breeds_and_stages, id_formulation, name, duration) FROM stdin;
    public       postgres    false    214   ��      1          0    27399    aba_time_unit 
   TABLE DATA               G   COPY public.aba_time_unit (id, singular_name, plural_name) FROM stdin;
    public       postgres    false    215   ��      Q          0    27465    mdapplication 
   TABLE DATA               O   COPY public.mdapplication (application_id, application_name, type) FROM stdin;
    public       postgres    false    247   ��      S          0    27471    mdapplication_rol 
   TABLE DATA               G   COPY public.mdapplication_rol (id, application_id, rol_id) FROM stdin;
    public       postgres    false    249   ��      T          0    27475    mdbreed 
   TABLE DATA               7   COPY public.mdbreed (breed_id, code, name) FROM stdin;
    public       postgres    false    250   "�      U          0    27479    mdbroiler_product 
   TABLE DATA               \   COPY public.mdbroiler_product (broiler_product_id, name, days_eviction, weight) FROM stdin;
    public       postgres    false    251   ^�      V          0    27483 
   mdfarmtype 
   TABLE DATA               8   COPY public.mdfarmtype (farm_type_id, name) FROM stdin;
    public       postgres    false    252   y�      X          0    27489 	   mdmeasure 
   TABLE DATA               b   COPY public.mdmeasure (measure_id, name, abbreviation, originvalue, valuekg, is_unit) FROM stdin;
    public       postgres    false    254   ��      Z          0    27495    mdparameter 
   TABLE DATA               d   COPY public.mdparameter (parameter_id, description, type, measure_id, process_id, name) FROM stdin;
    public       postgres    false    256   ��      \          0    27504 	   mdprocess 
   TABLE DATA               J  COPY public.mdprocess (process_id, process_order, product_id, stage_id, historical_decrease, theoretical_decrease, historical_weight, theoretical_weight, historical_duration, theoretical_duration, calendar_id, visible, name, predecessor_id, capacity, breed_id, gender, fattening_goal, type_posture, biological_active) FROM stdin;
    public       postgres    false    258   ��      ^          0    27510 	   mdproduct 
   TABLE DATA               ;   COPY public.mdproduct (product_id, code, name) FROM stdin;
    public       postgres    false    260   D�      `          0    27516    mdrol 
   TABLE DATA               T   COPY public.mdrol (rol_id, rol_name, admin_user_creator, creation_date) FROM stdin;
    public       postgres    false    262   ��      b          0    27522 
   mdscenario 
   TABLE DATA               o   COPY public.mdscenario (scenario_id, description, date_start, date_end, name, status, calendar_id) FROM stdin;
    public       postgres    false    264   �      d          0    27532    mdshedstatus 
   TABLE DATA               I   COPY public.mdshedstatus (shed_status_id, name, description) FROM stdin;
    public       postgres    false    266   ��      f          0    27538    mdstage 
   TABLE DATA               9   COPY public.mdstage (stage_id, order_, name) FROM stdin;
    public       postgres    false    268   "�      h          0    27544    mduser 
   TABLE DATA                  COPY public.mduser (user_id, username, password, name, lastname, active, admi_user_creator, rol_id, creation_date) FROM stdin;
    public       postgres    false    270   ��      i          0    27548    oscenter 
   TABLE DATA               [   COPY public.oscenter (center_id, partnership_id, farm_id, name, code, "order") FROM stdin;
    public       postgres    false    271   �      j          0    27552    oscenter_oswarehouse 
   TABLE DATA               x   COPY public.oscenter_oswarehouse (client_id, partnership_id, farm_id, center_id, warehouse_id, delete_mark) FROM stdin;
    public       postgres    false    272   �      k          0    27555    osfarm 
   TABLE DATA               \   COPY public.osfarm (farm_id, partnership_id, code, name, farm_type_id, "order") FROM stdin;
    public       postgres    false    273   8�      l          0    27559    osincubator 
   TABLE DATA               �   COPY public.osincubator (incubator_id, incubator_plant_id, name, code, description, capacity, sunday, monday, tuesday, wednesday, thursday, friday, saturday, available) FROM stdin;
    public       postgres    false    274   ��      m          0    27563    osincubatorplant 
   TABLE DATA               �   COPY public.osincubatorplant (incubator_plant_id, name, code, description, partnership_id, max_storage, min_storage, acclimatized, suitable, expired) FROM stdin;
    public       postgres    false    275   ��      o          0    27569    ospartnership 
   TABLE DATA               Y   COPY public.ospartnership (partnership_id, name, address, description, code) FROM stdin;
    public       postgres    false    277   +�      q          0    27578    osshed 
   TABLE DATA               /  COPY public.osshed (shed_id, partnership_id, farm_id, center_id, code, statusshed_id, type_id, building_date, stall_width, stall_height, capacity_min, capacity_max, environment_id, rotation_days, nests_quantity, cages_quantity, birds_quantity, capacity_theoretical, avaliable_date, "order") FROM stdin;
    public       postgres    false    279   ��      s          0    27589    ossilo 
   TABLE DATA               ?  COPY public.ossilo (silo_id, client_id, partnership_id, farm_id, center_id, name, rings_height, rings_height_id, height, height_unit_id, diameter, diameter_unit_id, total_rings_quantity, measuring_mechanism, cone_degrees, total_capacity_1, total_capacity_2, capacity_unit_id_1, capacity_unit_id_2, central) FROM stdin;
    public       postgres    false    281   ��      t          0    27593    ossilo_osshed 
   TABLE DATA               v   COPY public.ossilo_osshed (silo_id, shed_id, center_id, farm_id, partnership_id, client_id, deleted_mark) FROM stdin;
    public       postgres    false    282   ��      v          0    27598    osslaughterhouse 
   TABLE DATA               i   COPY public.osslaughterhouse (slaughterhouse_id, name, address, description, code, capacity) FROM stdin;
    public       postgres    false    284   ��      x          0    27607    oswarehouse 
   TABLE DATA               X   COPY public.oswarehouse (warehouse_id, partnership_id, farm_id, name, code) FROM stdin;
    public       postgres    false    286   0�      �          0    27631    txavailabilitysheds 
   TABLE DATA               k   COPY public.txavailabilitysheds (availability_shed_id, shed_id, init_date, end_date, lot_code) FROM stdin;
    public       postgres    false    297   M�      �          0    27635 	   txbroiler 
   TABLE DATA               �   COPY public.txbroiler (broiler_id, projected_date, projected_quantity, partnership_id, scenario_id, breed_id, lot_incubator, programmed_eggs_id) FROM stdin;
    public       postgres    false    298   j�      �          0    27639    txbroiler_detail 
   TABLE DATA                 COPY public.txbroiler_detail (broiler_detail_id, broiler_id, scheduled_date, scheduled_quantity, farm_id, shed_id, confirm, execution_date, execution_quantity, lot, broiler_product_id, center_id, executionfarm_id, executioncenter_id, executionshed_id) FROM stdin;
    public       postgres    false    299   ��      �          0    27643    txbroilereviction 
   TABLE DATA               �   COPY public.txbroilereviction (broilereviction_id, projected_date, projected_quantity, partnership_id, scenario_id, breed_id, lot_incubator, broiler_detail_id) FROM stdin;
    public       postgres    false    300   ��      �          0    27647    txbroilereviction_detail 
   TABLE DATA                 COPY public.txbroilereviction_detail (broilereviction_detail_id, broilereviction_id, scheduled_date, scheduled_quantity, farm_id, shed_id, confirm, execution_date, execution_quantity, lot, broiler_product_id, slaughterhouse_id, center_id, executionslaughterhouse_id) FROM stdin;
    public       postgres    false    301   ��      �          0    27651    txbroilerproduct_detail 
   TABLE DATA               y   COPY public.txbroilerproduct_detail (broilerproduct_detail_id, broiler_detail, broiler_product_id, quantity) FROM stdin;
    public       postgres    false    302   ��      �          0    27655    txbroodermachine 
   TABLE DATA               �   COPY public.txbroodermachine (brooder_machine_id_seq, partnership_id, farm_id, capacity, sunday, monday, tuesday, wednesday, thursday, friday, saturday, name) FROM stdin;
    public       postgres    false    303   ��      �          0    27659 
   txcalendar 
   TABLE DATA               �   COPY public.txcalendar (calendar_id, description, saturday, sunday, week_start, code, year_start, year_end, generated) FROM stdin;
    public       postgres    false    304   �      �          0    27663    txcalendarday 
   TABLE DATA               �   COPY public.txcalendarday (calendar_day_id, calendar_id, use_date, use_year, use_month, use_day, use_week, week_day, sequence, working_day) FROM stdin;
    public       postgres    false    305   ��      �          0    27669    txeggs_movements 
   TABLE DATA               �   COPY public.txeggs_movements (eggs_movements_id, fecha_movements, lot, quantity, type_movements, eggs_storage_id, description_adjustment) FROM stdin;
    public       postgres    false    307   �a      �          0    27676    txeggs_planning 
   TABLE DATA               y   COPY public.txeggs_planning (egg_planning_id, month_planning, year_planning, scenario_id, planned, breed_id) FROM stdin;
    public       postgres    false    308   �a      �          0    27680    txeggs_required 
   TABLE DATA               p   COPY public.txeggs_required (egg_required_id, use_month, use_year, scenario_id, required, breed_id) FROM stdin;
    public       postgres    false    309   �a      �          0    27684    txeggs_storage 
   TABLE DATA               �   COPY public.txeggs_storage (eggs_storage_id, incubator_plant_id, scenario_id, breed_id, init_date, end_date, lot, eggs, eggs_executed) FROM stdin;
    public       postgres    false    310   �d      �          0    27688    txgoals_erp 
   TABLE DATA               g   COPY public.txgoals_erp (goals_erp_id, use_week, use_value, product_id, code, scenario_id) FROM stdin;
    public       postgres    false    311   �d      �          0    27693    txhousingway 
   TABLE DATA               �   COPY public.txhousingway (housing_way_id, projected_quantity, projected_date, stage_id, partnership_id, scenario_id, breed_id, predecessor_id) FROM stdin;
    public       postgres    false    313   e      �          0    27697    txhousingway_detail 
   TABLE DATA                 COPY public.txhousingway_detail (housingway_detail_id, housing_way_id, scheduled_date, scheduled_quantity, farm_id, shed_id, confirm, execution_date, execution_quantity, lot, incubator_plant_id, center_id, executionfarm_id, executioncenter_id, executionshed_id) FROM stdin;
    public       postgres    false    314   %e      �          0    27701    txlot 
   TABLE DATA               �   COPY public.txlot (lot_id, lot_code, lot_origin, status, proyected_date, sheduled_date, proyected_quantity, sheduled_quantity, released_quantity, product_id, breed_id, gender, type_posture, shed_id, origin, farm_id, housing_way_id) FROM stdin;
    public       postgres    false    315   Be      �          0    27705 
   txlot_eggs 
   TABLE DATA               Y   COPY public.txlot_eggs (lot_eggs_id, theorical_performance, week_date, week) FROM stdin;
    public       postgres    false    316   _e      �          0    27709    txposturecurve 
   TABLE DATA               �   COPY public.txposturecurve (posture_curve_id, week, breed_id, theorical_performance, historical_performance, theorical_accum_mortality, historical_accum_mortality, theorical_uniformity, historical_uniformity, type_posture) FROM stdin;
    public       postgres    false    317   |e      �          0    27713    txprogrammed_eggs 
   TABLE DATA               �   COPY public.txprogrammed_eggs (programmed_eggs_id, incubator_id, lot_breed, lot_incubator, use_date, eggs, breed_id, execution_quantity, eggs_storage_id, confirm, released, eggs_movements_id) FROM stdin;
    public       postgres    false    318   i      �          0    27717    txscenarioformula 
   TABLE DATA               �   COPY public.txscenarioformula (scenario_formula_id, process_id, predecessor_id, parameter_id, sign, divider, duration, scenario_id, measure_id) FROM stdin;
    public       postgres    false    319   9i      �          0    27721    txscenarioparameter 
   TABLE DATA               �   COPY public.txscenarioparameter (scenario_parameter_id, process_id, parameter_id, use_year, use_month, use_value, scenario_id, value_units) FROM stdin;
    public       postgres    false    320   �k      �          0    27726    txscenarioparameterday 
   TABLE DATA               �   COPY public.txscenarioparameterday (scenario_parameter_day_id, use_day, parameter_id, units_day, scenario_id, sequence, use_month, use_year, week_day, use_week) FROM stdin;
    public       postgres    false    321   zw      �          0    27730    txscenarioposturecurve 
   TABLE DATA               �   COPY public.txscenarioposturecurve (scenario_posture_id, posture_date, eggs, scenario_id, housingway_detail_id, breed_id) FROM stdin;
    public       postgres    false    322   -,      �          0    27734    txscenarioprocess 
   TABLE DATA               �   COPY public.txscenarioprocess (scenario_process_id, process_id, decrease_goal, weight_goal, duration_goal, scenario_id) FROM stdin;
    public       postgres    false    323   J,      k           0    0    abaTimeUnit_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public."abaTimeUnit_id_seq"', 2, false);
            public       postgres    false    196            l           0    0    aba_breeds_and_stages_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.aba_breeds_and_stages_id_seq', 8, false);
            public       postgres    false    197            m           0    0 +   aba_consumption_and_mortality_detail_id_seq    SEQUENCE SET     \   SELECT pg_catalog.setval('public.aba_consumption_and_mortality_detail_id_seq', 203, false);
            public       postgres    false    201            n           0    0 $   aba_consumption_and_mortality_id_seq    SEQUENCE SET     S   SELECT pg_catalog.setval('public.aba_consumption_and_mortality_id_seq', 8, false);
            public       postgres    false    199            o           0    0 &   aba_elements_and_concentrations_id_seq    SEQUENCE SET     V   SELECT pg_catalog.setval('public.aba_elements_and_concentrations_id_seq', 105, true);
            public       postgres    false    205            p           0    0    aba_elements_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.aba_elements_id_seq', 22, true);
            public       postgres    false    203            q           0    0    aba_elements_properties_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.aba_elements_properties_id_seq', 1, false);
            public       postgres    false    207            r           0    0    aba_formulation_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.aba_formulation_id_seq', 68, true);
            public       postgres    false    209            s           0    0    aba_results_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.aba_results_id_seq', 1, false);
            public       postgres    false    211            t           0    0 &   aba_stages_of_breeds_and_stages_id_seq    SEQUENCE SET     V   SELECT pg_catalog.setval('public.aba_stages_of_breeds_and_stages_id_seq', 24, false);
            public       postgres    false    213            u           0    0    availability_shed_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.availability_shed_id_seq', 301, true);
            public       postgres    false    216            v           0    0    base_day_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.base_day_id_seq', 3, true);
            public       postgres    false    217            w           0    0    breed_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.breed_id_seq', 17, true);
            public       postgres    false    218            x           0    0    broiler_detail_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.broiler_detail_id_seq', 66, true);
            public       postgres    false    219            y           0    0    broiler_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.broiler_id_seq', 135, true);
            public       postgres    false    220            z           0    0    broiler_product_detail_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.broiler_product_detail_id_seq', 2, true);
            public       postgres    false    221            {           0    0    broiler_product_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.broiler_product_id_seq', 21, true);
            public       postgres    false    222            |           0    0    broilereviction_detail_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.broilereviction_detail_id_seq', 140, true);
            public       postgres    false    223            }           0    0    broilereviction_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.broilereviction_id_seq', 91, true);
            public       postgres    false    224            ~           0    0    brooder_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.brooder_id_seq', 52, true);
            public       postgres    false    225                       0    0    brooder_machines_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.brooder_machines_id_seq', 7, true);
            public       postgres    false    226            �           0    0    calendar_day_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.calendar_day_id_seq', 22279, true);
            public       postgres    false    227            �           0    0    calendar_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.calendar_id_seq', 18, true);
            public       postgres    false    228            �           0    0    center_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.center_id_seq', 159, true);
            public       postgres    false    229            �           0    0    egg_planning_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.egg_planning_id_seq', 3162, true);
            public       postgres    false    230            �           0    0    egg_required_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.egg_required_id_seq', 3393, true);
            public       postgres    false    231            �           0    0    eggs_storage_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.eggs_storage_id_seq', 34245, true);
            public       postgres    false    232            �           0    0    farm_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.farm_id_seq', 165, true);
            public       postgres    false    233            �           0    0    farm_type_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.farm_type_id_seq', 3, true);
            public       postgres    false    234            �           0    0    holiday_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.holiday_id_seq', 297, true);
            public       postgres    false    235            �           0    0    housing_way_detail_id_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.housing_way_detail_id_seq', 647, true);
            public       postgres    false    236            �           0    0    housing_way_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.housing_way_id_seq', 834, true);
            public       postgres    false    237            �           0    0    incubator_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.incubator_id_seq', 23, true);
            public       postgres    false    238            �           0    0    incubator_plant_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.incubator_plant_id_seq', 19, true);
            public       postgres    false    239            �           0    0    industry_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.industry_id_seq', 1, true);
            public       postgres    false    240            �           0    0    line_id_seq    SEQUENCE SET     9   SELECT pg_catalog.setval('public.line_id_seq', 1, true);
            public       postgres    false    241            �           0    0    lot_eggs_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.lot_eggs_id_seq', 108, true);
            public       postgres    false    242            �           0    0    lot_fattening_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.lot_fattening_id_seq', 1, false);
            public       postgres    false    243            �           0    0 
   lot_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.lot_id_seq', 316, true);
            public       postgres    false    244            �           0    0    lot_liftbreeding_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.lot_liftbreeding_id_seq', 4, true);
            public       postgres    false    245            �           0    0     mdapplication_application_id_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('public.mdapplication_application_id_seq', 1, false);
            public       postgres    false    246            �           0    0    mdapplication_rol_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.mdapplication_rol_id_seq', 14, true);
            public       postgres    false    248            �           0    0    mdrol_rol_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.mdrol_rol_id_seq', 1, false);
            public       postgres    false    261            �           0    0    mduser_user_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.mduser_user_id_seq', 1, false);
            public       postgres    false    269            �           0    0    measure_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.measure_id_seq', 17, true);
            public       postgres    false    253            �           0    0    parameter_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.parameter_id_seq', 56, true);
            public       postgres    false    255            �           0    0    partnership_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.partnership_id_seq', 31, true);
            public       postgres    false    276            �           0    0    posture_curve_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.posture_curve_id_seq', 325, true);
            public       postgres    false    287            �           0    0    predecessor_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.predecessor_id_seq', 13, true);
            public       postgres    false    288            �           0    0    process_class_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.process_class_id_seq', 5, true);
            public       postgres    false    289            �           0    0    process_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.process_id_seq', 90, true);
            public       postgres    false    257            �           0    0    product_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.product_id_seq', 94, true);
            public       postgres    false    259            �           0    0    programmed_eggs_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.programmed_eggs_id_seq', 391, true);
            public       postgres    false    290            �           0    0    raspberry_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.raspberry_id_seq', 5, true);
            public       postgres    false    291            �           0    0    scenario_formula_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.scenario_formula_id_seq', 1288, true);
            public       postgres    false    292            �           0    0    scenario_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.scenario_id_seq', 170, true);
            public       postgres    false    263            �           0    0    scenario_parameter_day_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.scenario_parameter_day_seq', 50126, true);
            public       postgres    false    293            �           0    0    scenario_parameter_id_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('public.scenario_parameter_id_seq', 6389, true);
            public       postgres    false    294            �           0    0    scenario_posture_id_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.scenario_posture_id_seq', 59884, true);
            public       postgres    false    295            �           0    0    scenario_process_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.scenario_process_id_seq', 835, true);
            public       postgres    false    296            �           0    0    shed_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.shed_id_seq', 365, true);
            public       postgres    false    278            �           0    0    silo_id_seq    SEQUENCE SET     9   SELECT pg_catalog.setval('public.silo_id_seq', 4, true);
            public       postgres    false    280            �           0    0    slaughterhouse_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.slaughterhouse_id_seq', 36, true);
            public       postgres    false    283            �           0    0    stage_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.stage_id_seq', 27, true);
            public       postgres    false    267            �           0    0    status_shed_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.status_shed_id_seq', 10, true);
            public       postgres    false    265            �           0    0    txeggs_movements_id_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('public.txeggs_movements_id_seq', 202229, true);
            public       postgres    false    306            �           0    0    txgoals_erp_goals_erp_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.txgoals_erp_goals_erp_id_seq', 1920, true);
            public       postgres    false    312            �           0    0 #   user_application_application_id_seq    SEQUENCE SET     R   SELECT pg_catalog.setval('public.user_application_application_id_seq', 1, false);
            public       postgres    false    324            �           0    0     user_application_user_app_id_seq    SEQUENCE SET     P   SELECT pg_catalog.setval('public.user_application_user_app_id_seq', 215, true);
            public       postgres    false    325            �           0    0    user_application_user_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.user_application_user_id_seq', 1, false);
            public       postgres    false    326            �           0    0    warehouse_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.warehouse_id_seq', 124, true);
            public       postgres    false    285            K           2606    27746    aba_time_unit abaTimeUnit_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.aba_time_unit
    ADD CONSTRAINT "abaTimeUnit_pkey" PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.aba_time_unit DROP CONSTRAINT "abaTimeUnit_pkey";
       public         postgres    false    215            -           2606    27748 0   aba_breeds_and_stages aba_breeds_and_stages_pkey 
   CONSTRAINT     n   ALTER TABLE ONLY public.aba_breeds_and_stages
    ADD CONSTRAINT aba_breeds_and_stages_pkey PRIMARY KEY (id);
 Z   ALTER TABLE ONLY public.aba_breeds_and_stages DROP CONSTRAINT aba_breeds_and_stages_pkey;
       public         postgres    false    198            6           2606    27750 N   aba_consumption_and_mortality_detail aba_consumption_and_mortality_detail_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.aba_consumption_and_mortality_detail
    ADD CONSTRAINT aba_consumption_and_mortality_detail_pkey PRIMARY KEY (id);
 x   ALTER TABLE ONLY public.aba_consumption_and_mortality_detail DROP CONSTRAINT aba_consumption_and_mortality_detail_pkey;
       public         postgres    false    202            1           2606    27752 @   aba_consumption_and_mortality aba_consumption_and_mortality_pkey 
   CONSTRAINT     ~   ALTER TABLE ONLY public.aba_consumption_and_mortality
    ADD CONSTRAINT aba_consumption_and_mortality_pkey PRIMARY KEY (id);
 j   ALTER TABLE ONLY public.aba_consumption_and_mortality DROP CONSTRAINT aba_consumption_and_mortality_pkey;
       public         postgres    false    200            ;           2606    27754 D   aba_elements_and_concentrations aba_elements_and_concentrations_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.aba_elements_and_concentrations
    ADD CONSTRAINT aba_elements_and_concentrations_pkey PRIMARY KEY (id);
 n   ALTER TABLE ONLY public.aba_elements_and_concentrations DROP CONSTRAINT aba_elements_and_concentrations_pkey;
       public         postgres    false    206            9           2606    27756    aba_elements aba_elements_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.aba_elements
    ADD CONSTRAINT aba_elements_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.aba_elements DROP CONSTRAINT aba_elements_pkey;
       public         postgres    false    204            @           2606    27758 4   aba_elements_properties aba_elements_properties_pkey 
   CONSTRAINT     r   ALTER TABLE ONLY public.aba_elements_properties
    ADD CONSTRAINT aba_elements_properties_pkey PRIMARY KEY (id);
 ^   ALTER TABLE ONLY public.aba_elements_properties DROP CONSTRAINT aba_elements_properties_pkey;
       public         postgres    false    208            B           2606    27760 $   aba_formulation aba_formulation_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.aba_formulation
    ADD CONSTRAINT aba_formulation_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.aba_formulation DROP CONSTRAINT aba_formulation_pkey;
       public         postgres    false    210            D           2606    27762    aba_results aba_results_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.aba_results
    ADD CONSTRAINT aba_results_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.aba_results DROP CONSTRAINT aba_results_pkey;
       public         postgres    false    212            G           2606    27764 D   aba_stages_of_breeds_and_stages aba_stages_of_breeds_and_stages_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.aba_stages_of_breeds_and_stages
    ADD CONSTRAINT aba_stages_of_breeds_and_stages_pkey PRIMARY KEY (id);
 n   ALTER TABLE ONLY public.aba_stages_of_breeds_and_stages DROP CONSTRAINT aba_stages_of_breeds_and_stages_pkey;
       public         postgres    false    214            M           2606    27766    mdapplication application_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public.mdapplication
    ADD CONSTRAINT application_pkey PRIMARY KEY (application_id);
 H   ALTER TABLE ONLY public.mdapplication DROP CONSTRAINT application_pkey;
       public         postgres    false    247            Q           2606    27768 (   mdapplication_rol mdapplication_rol_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.mdapplication_rol
    ADD CONSTRAINT mdapplication_rol_pkey PRIMARY KEY (id);
 R   ALTER TABLE ONLY public.mdapplication_rol DROP CONSTRAINT mdapplication_rol_pkey;
       public         postgres    false    249            S           2606    27770    mdbreed mdbreed_code_key 
   CONSTRAINT     S   ALTER TABLE ONLY public.mdbreed
    ADD CONSTRAINT mdbreed_code_key UNIQUE (code);
 B   ALTER TABLE ONLY public.mdbreed DROP CONSTRAINT mdbreed_code_key;
       public         postgres    false    250            U           2606    27772    mdbreed mdbreed_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.mdbreed
    ADD CONSTRAINT mdbreed_pkey PRIMARY KEY (breed_id);
 >   ALTER TABLE ONLY public.mdbreed DROP CONSTRAINT mdbreed_pkey;
       public         postgres    false    250            W           2606    27774 (   mdbroiler_product mdbroiler_product_pkey 
   CONSTRAINT     v   ALTER TABLE ONLY public.mdbroiler_product
    ADD CONSTRAINT mdbroiler_product_pkey PRIMARY KEY (broiler_product_id);
 R   ALTER TABLE ONLY public.mdbroiler_product DROP CONSTRAINT mdbroiler_product_pkey;
       public         postgres    false    251            Y           2606    27776    mdfarmtype mdfarmtype_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.mdfarmtype
    ADD CONSTRAINT mdfarmtype_pkey PRIMARY KEY (farm_type_id);
 D   ALTER TABLE ONLY public.mdfarmtype DROP CONSTRAINT mdfarmtype_pkey;
       public         postgres    false    252            [           2606    27778    mdmeasure mdmeasure_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.mdmeasure
    ADD CONSTRAINT mdmeasure_pkey PRIMARY KEY (measure_id);
 B   ALTER TABLE ONLY public.mdmeasure DROP CONSTRAINT mdmeasure_pkey;
       public         postgres    false    254            _           2606    27780    mdparameter mdparameter_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.mdparameter
    ADD CONSTRAINT mdparameter_pkey PRIMARY KEY (parameter_id);
 F   ALTER TABLE ONLY public.mdparameter DROP CONSTRAINT mdparameter_pkey;
       public         postgres    false    256            e           2606    27782    mdprocess mdprocess_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.mdprocess
    ADD CONSTRAINT mdprocess_pkey PRIMARY KEY (process_id);
 B   ALTER TABLE ONLY public.mdprocess DROP CONSTRAINT mdprocess_pkey;
       public         postgres    false    258            g           2606    27784    mdproduct mdproduct_code_unique 
   CONSTRAINT     Z   ALTER TABLE ONLY public.mdproduct
    ADD CONSTRAINT mdproduct_code_unique UNIQUE (code);
 I   ALTER TABLE ONLY public.mdproduct DROP CONSTRAINT mdproduct_code_unique;
       public         postgres    false    260            i           2606    27786    mdproduct mdproduct_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.mdproduct
    ADD CONSTRAINT mdproduct_pkey PRIMARY KEY (product_id);
 B   ALTER TABLE ONLY public.mdproduct DROP CONSTRAINT mdproduct_pkey;
       public         postgres    false    260            p           2606    27788 !   mdscenario mdscenario_name_unique 
   CONSTRAINT     \   ALTER TABLE ONLY public.mdscenario
    ADD CONSTRAINT mdscenario_name_unique UNIQUE (name);
 K   ALTER TABLE ONLY public.mdscenario DROP CONSTRAINT mdscenario_name_unique;
       public         postgres    false    264            r           2606    27790    mdscenario mdscenario_pkey 
   CONSTRAINT     a   ALTER TABLE ONLY public.mdscenario
    ADD CONSTRAINT mdscenario_pkey PRIMARY KEY (scenario_id);
 D   ALTER TABLE ONLY public.mdscenario DROP CONSTRAINT mdscenario_pkey;
       public         postgres    false    264            t           2606    27792    mdshedstatus mdshedstatus_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public.mdshedstatus
    ADD CONSTRAINT mdshedstatus_pkey PRIMARY KEY (shed_status_id);
 H   ALTER TABLE ONLY public.mdshedstatus DROP CONSTRAINT mdshedstatus_pkey;
       public         postgres    false    266            v           2606    27794    mdstage mdstage_name_unique 
   CONSTRAINT     V   ALTER TABLE ONLY public.mdstage
    ADD CONSTRAINT mdstage_name_unique UNIQUE (name);
 E   ALTER TABLE ONLY public.mdstage DROP CONSTRAINT mdstage_name_unique;
       public         postgres    false    268            x           2606    27796    mdstage mdstage_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.mdstage
    ADD CONSTRAINT mdstage_pkey PRIMARY KEY (stage_id);
 >   ALTER TABLE ONLY public.mdstage DROP CONSTRAINT mdstage_pkey;
       public         postgres    false    268            {           2606    27798    mduser mduser_user_id_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY public.mduser
    ADD CONSTRAINT mduser_user_id_pkey PRIMARY KEY (user_id);
 D   ALTER TABLE ONLY public.mduser DROP CONSTRAINT mduser_user_id_pkey;
       public         postgres    false    270            }           2606    27800    mduser mduser_username_unique 
   CONSTRAINT     \   ALTER TABLE ONLY public.mduser
    ADD CONSTRAINT mduser_username_unique UNIQUE (username);
 G   ALTER TABLE ONLY public.mduser DROP CONSTRAINT mduser_username_unique;
       public         postgres    false    270            �           2606    27802 .   oscenter_oswarehouse oscenter_oswarehouse_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.oscenter_oswarehouse
    ADD CONSTRAINT oscenter_oswarehouse_pkey PRIMARY KEY (client_id, partnership_id, farm_id, center_id, warehouse_id);
 X   ALTER TABLE ONLY public.oscenter_oswarehouse DROP CONSTRAINT oscenter_oswarehouse_pkey;
       public         postgres    false    272    272    272    272    272            �           2606    27804 .   oscenter oscenter_partnership_farm_code_unique 
   CONSTRAINT     �   ALTER TABLE ONLY public.oscenter
    ADD CONSTRAINT oscenter_partnership_farm_code_unique UNIQUE (partnership_id, farm_id, code);
 X   ALTER TABLE ONLY public.oscenter DROP CONSTRAINT oscenter_partnership_farm_code_unique;
       public         postgres    false    271    271    271            �           2606    27806    oscenter oscenter_pkey 
   CONSTRAINT     [   ALTER TABLE ONLY public.oscenter
    ADD CONSTRAINT oscenter_pkey PRIMARY KEY (center_id);
 @   ALTER TABLE ONLY public.oscenter DROP CONSTRAINT oscenter_pkey;
       public         postgres    false    271            �           2606    27808 %   osfarm osfarm_partnership_code_unique 
   CONSTRAINT     p   ALTER TABLE ONLY public.osfarm
    ADD CONSTRAINT osfarm_partnership_code_unique UNIQUE (partnership_id, code);
 O   ALTER TABLE ONLY public.osfarm DROP CONSTRAINT osfarm_partnership_code_unique;
       public         postgres    false    273    273            �           2606    27810    osfarm osfarm_pkey 
   CONSTRAINT     U   ALTER TABLE ONLY public.osfarm
    ADD CONSTRAINT osfarm_pkey PRIMARY KEY (farm_id);
 <   ALTER TABLE ONLY public.osfarm DROP CONSTRAINT osfarm_pkey;
       public         postgres    false    273            �           2606    27812    osshed oshed_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.osshed
    ADD CONSTRAINT oshed_pkey PRIMARY KEY (shed_id);
 ;   ALTER TABLE ONLY public.osshed DROP CONSTRAINT oshed_pkey;
       public         postgres    false    279            �           2606    27814 2   osincubator osincubator_incubatorplant_code_unique 
   CONSTRAINT     �   ALTER TABLE ONLY public.osincubator
    ADD CONSTRAINT osincubator_incubatorplant_code_unique UNIQUE (incubator_plant_id, code);
 \   ALTER TABLE ONLY public.osincubator DROP CONSTRAINT osincubator_incubatorplant_code_unique;
       public         postgres    false    274    274            �           2606    27816    osincubator osincubator_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.osincubator
    ADD CONSTRAINT osincubator_pkey PRIMARY KEY (incubator_id);
 F   ALTER TABLE ONLY public.osincubator DROP CONSTRAINT osincubator_pkey;
       public         postgres    false    274            �           2606    27818 9   osincubatorplant osincubatorplant_partnership_code_unique 
   CONSTRAINT     �   ALTER TABLE ONLY public.osincubatorplant
    ADD CONSTRAINT osincubatorplant_partnership_code_unique UNIQUE (partnership_id, code);
 c   ALTER TABLE ONLY public.osincubatorplant DROP CONSTRAINT osincubatorplant_partnership_code_unique;
       public         postgres    false    275    275            �           2606    27820 &   osincubatorplant osincubatorplant_pkey 
   CONSTRAINT     t   ALTER TABLE ONLY public.osincubatorplant
    ADD CONSTRAINT osincubatorplant_pkey PRIMARY KEY (incubator_plant_id);
 P   ALTER TABLE ONLY public.osincubatorplant DROP CONSTRAINT osincubatorplant_pkey;
       public         postgres    false    275            �           2606    27822 '   ospartnership ospartnership_code_unique 
   CONSTRAINT     b   ALTER TABLE ONLY public.ospartnership
    ADD CONSTRAINT ospartnership_code_unique UNIQUE (code);
 Q   ALTER TABLE ONLY public.ospartnership DROP CONSTRAINT ospartnership_code_unique;
       public         postgres    false    277            �           2606    27824     ospartnership ospartnership_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY public.ospartnership
    ADD CONSTRAINT ospartnership_pkey PRIMARY KEY (partnership_id);
 J   ALTER TABLE ONLY public.ospartnership DROP CONSTRAINT ospartnership_pkey;
       public         postgres    false    277            �           2606    27826 1   osshed osshed_partnership_farm_center_code_unique 
   CONSTRAINT     �   ALTER TABLE ONLY public.osshed
    ADD CONSTRAINT osshed_partnership_farm_center_code_unique UNIQUE (partnership_id, farm_id, center_id, code);
 [   ALTER TABLE ONLY public.osshed DROP CONSTRAINT osshed_partnership_farm_center_code_unique;
       public         postgres    false    279    279    279    279            �           2606    27828     ossilo_osshed ossilo_osshed_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.ossilo_osshed
    ADD CONSTRAINT ossilo_osshed_pkey PRIMARY KEY (silo_id, shed_id);
 J   ALTER TABLE ONLY public.ossilo_osshed DROP CONSTRAINT ossilo_osshed_pkey;
       public         postgres    false    282    282            �           2606    27830    ossilo ossilo_pkey 
   CONSTRAINT     U   ALTER TABLE ONLY public.ossilo
    ADD CONSTRAINT ossilo_pkey PRIMARY KEY (silo_id);
 <   ALTER TABLE ONLY public.ossilo DROP CONSTRAINT ossilo_pkey;
       public         postgres    false    281            �           2606    27832 &   osslaughterhouse osslaughterhouse_pkey 
   CONSTRAINT     s   ALTER TABLE ONLY public.osslaughterhouse
    ADD CONSTRAINT osslaughterhouse_pkey PRIMARY KEY (slaughterhouse_id);
 P   ALTER TABLE ONLY public.osslaughterhouse DROP CONSTRAINT osslaughterhouse_pkey;
       public         postgres    false    284            �           2606    27834 4   oswarehouse oswarehouse_partnership_farm_code_unique 
   CONSTRAINT     �   ALTER TABLE ONLY public.oswarehouse
    ADD CONSTRAINT oswarehouse_partnership_farm_code_unique UNIQUE (partnership_id, farm_id, code);
 ^   ALTER TABLE ONLY public.oswarehouse DROP CONSTRAINT oswarehouse_partnership_farm_code_unique;
       public         postgres    false    286    286    286            �           2606    27836    oswarehouse oswarehouse_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.oswarehouse
    ADD CONSTRAINT oswarehouse_pkey PRIMARY KEY (warehouse_id);
 F   ALTER TABLE ONLY public.oswarehouse DROP CONSTRAINT oswarehouse_pkey;
       public         postgres    false    286            k           2606    27838    mdrol rol_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.mdrol
    ADD CONSTRAINT rol_pkey PRIMARY KEY (rol_id);
 8   ALTER TABLE ONLY public.mdrol DROP CONSTRAINT rol_pkey;
       public         postgres    false    262            �           2606    27840 ,   txavailabilitysheds txavailabilitysheds_pkey 
   CONSTRAINT     |   ALTER TABLE ONLY public.txavailabilitysheds
    ADD CONSTRAINT txavailabilitysheds_pkey PRIMARY KEY (availability_shed_id);
 V   ALTER TABLE ONLY public.txavailabilitysheds DROP CONSTRAINT txavailabilitysheds_pkey;
       public         postgres    false    297            �           2606    27842 &   txbroiler_detail txbroiler_detail_pkey 
   CONSTRAINT     s   ALTER TABLE ONLY public.txbroiler_detail
    ADD CONSTRAINT txbroiler_detail_pkey PRIMARY KEY (broiler_detail_id);
 P   ALTER TABLE ONLY public.txbroiler_detail DROP CONSTRAINT txbroiler_detail_pkey;
       public         postgres    false    299            �           2606    27844    txbroiler txbroiler_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.txbroiler
    ADD CONSTRAINT txbroiler_pkey PRIMARY KEY (broiler_id);
 B   ALTER TABLE ONLY public.txbroiler DROP CONSTRAINT txbroiler_pkey;
       public         postgres    false    298            �           2606    27846 6   txbroilereviction_detail txbroilereviction_detail_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.txbroilereviction_detail
    ADD CONSTRAINT txbroilereviction_detail_pkey PRIMARY KEY (broilereviction_detail_id);
 `   ALTER TABLE ONLY public.txbroilereviction_detail DROP CONSTRAINT txbroilereviction_detail_pkey;
       public         postgres    false    301            �           2606    27848 (   txbroilereviction txbroilereviction_pkey 
   CONSTRAINT     v   ALTER TABLE ONLY public.txbroilereviction
    ADD CONSTRAINT txbroilereviction_pkey PRIMARY KEY (broilereviction_id);
 R   ALTER TABLE ONLY public.txbroilereviction DROP CONSTRAINT txbroilereviction_pkey;
       public         postgres    false    300            �           2606    27850 4   txbroilerproduct_detail txbroilerproduct_detail_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.txbroilerproduct_detail
    ADD CONSTRAINT txbroilerproduct_detail_pkey PRIMARY KEY (broilerproduct_detail_id);
 ^   ALTER TABLE ONLY public.txbroilerproduct_detail DROP CONSTRAINT txbroilerproduct_detail_pkey;
       public         postgres    false    302            �           2606    27852 &   txbroodermachine txbroodermachine_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.txbroodermachine
    ADD CONSTRAINT txbroodermachine_pkey PRIMARY KEY (brooder_machine_id_seq);
 P   ALTER TABLE ONLY public.txbroodermachine DROP CONSTRAINT txbroodermachine_pkey;
       public         postgres    false    303            �           2606    27854    txcalendar txcalendar_pkey 
   CONSTRAINT     a   ALTER TABLE ONLY public.txcalendar
    ADD CONSTRAINT txcalendar_pkey PRIMARY KEY (calendar_id);
 D   ALTER TABLE ONLY public.txcalendar DROP CONSTRAINT txcalendar_pkey;
       public         postgres    false    304            �           2606    27856     txcalendarday txcalendarday_pkey 
   CONSTRAINT     k   ALTER TABLE ONLY public.txcalendarday
    ADD CONSTRAINT txcalendarday_pkey PRIMARY KEY (calendar_day_id);
 J   ALTER TABLE ONLY public.txcalendarday DROP CONSTRAINT txcalendarday_pkey;
       public         postgres    false    305            �           2606    27858 &   txeggs_movements txeggs_movements_pkey 
   CONSTRAINT     s   ALTER TABLE ONLY public.txeggs_movements
    ADD CONSTRAINT txeggs_movements_pkey PRIMARY KEY (eggs_movements_id);
 P   ALTER TABLE ONLY public.txeggs_movements DROP CONSTRAINT txeggs_movements_pkey;
       public         postgres    false    307            �           2606    27860 $   txeggs_planning txeggs_planning_pkey 
   CONSTRAINT     o   ALTER TABLE ONLY public.txeggs_planning
    ADD CONSTRAINT txeggs_planning_pkey PRIMARY KEY (egg_planning_id);
 N   ALTER TABLE ONLY public.txeggs_planning DROP CONSTRAINT txeggs_planning_pkey;
       public         postgres    false    308            �           2606    27862 $   txeggs_required txeggs_required_pkey 
   CONSTRAINT     o   ALTER TABLE ONLY public.txeggs_required
    ADD CONSTRAINT txeggs_required_pkey PRIMARY KEY (egg_required_id);
 N   ALTER TABLE ONLY public.txeggs_required DROP CONSTRAINT txeggs_required_pkey;
       public         postgres    false    309            �           2606    27864 "   txeggs_storage txeggs_storage_pkey 
   CONSTRAINT     m   ALTER TABLE ONLY public.txeggs_storage
    ADD CONSTRAINT txeggs_storage_pkey PRIMARY KEY (eggs_storage_id);
 L   ALTER TABLE ONLY public.txeggs_storage DROP CONSTRAINT txeggs_storage_pkey;
       public         postgres    false    310            �           2606    27866    txgoals_erp txgoals_erp_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.txgoals_erp
    ADD CONSTRAINT txgoals_erp_pkey PRIMARY KEY (goals_erp_id);
 F   ALTER TABLE ONLY public.txgoals_erp DROP CONSTRAINT txgoals_erp_pkey;
       public         postgres    false    311                       2606    27868 ,   txhousingway_detail txhousingway_detail_pkey 
   CONSTRAINT     |   ALTER TABLE ONLY public.txhousingway_detail
    ADD CONSTRAINT txhousingway_detail_pkey PRIMARY KEY (housingway_detail_id);
 V   ALTER TABLE ONLY public.txhousingway_detail DROP CONSTRAINT txhousingway_detail_pkey;
       public         postgres    false    314            �           2606    27870    txhousingway txhousingway_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public.txhousingway
    ADD CONSTRAINT txhousingway_pkey PRIMARY KEY (housing_way_id);
 H   ALTER TABLE ONLY public.txhousingway DROP CONSTRAINT txhousingway_pkey;
       public         postgres    false    313                       2606    27872    txlot_eggs txlot_eggs_pkey 
   CONSTRAINT     a   ALTER TABLE ONLY public.txlot_eggs
    ADD CONSTRAINT txlot_eggs_pkey PRIMARY KEY (lot_eggs_id);
 D   ALTER TABLE ONLY public.txlot_eggs DROP CONSTRAINT txlot_eggs_pkey;
       public         postgres    false    316            	           2606    27874    txlot txlot_lot_code_key 
   CONSTRAINT     W   ALTER TABLE ONLY public.txlot
    ADD CONSTRAINT txlot_lot_code_key UNIQUE (lot_code);
 B   ALTER TABLE ONLY public.txlot DROP CONSTRAINT txlot_lot_code_key;
       public         postgres    false    315                       2606    27876    txlot txlot_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.txlot
    ADD CONSTRAINT txlot_pkey PRIMARY KEY (lot_id);
 :   ALTER TABLE ONLY public.txlot DROP CONSTRAINT txlot_pkey;
       public         postgres    false    315                       2606    27878 "   txposturecurve txposturecurve_pkey 
   CONSTRAINT     n   ALTER TABLE ONLY public.txposturecurve
    ADD CONSTRAINT txposturecurve_pkey PRIMARY KEY (posture_curve_id);
 L   ALTER TABLE ONLY public.txposturecurve DROP CONSTRAINT txposturecurve_pkey;
       public         postgres    false    317                       2606    27880 (   txprogrammed_eggs txprogrammed_eggs_pkey 
   CONSTRAINT     v   ALTER TABLE ONLY public.txprogrammed_eggs
    ADD CONSTRAINT txprogrammed_eggs_pkey PRIMARY KEY (programmed_eggs_id);
 R   ALTER TABLE ONLY public.txprogrammed_eggs DROP CONSTRAINT txprogrammed_eggs_pkey;
       public         postgres    false    318                       2606    27882 (   txscenarioformula txscenarioformula_pkey 
   CONSTRAINT     w   ALTER TABLE ONLY public.txscenarioformula
    ADD CONSTRAINT txscenarioformula_pkey PRIMARY KEY (scenario_formula_id);
 R   ALTER TABLE ONLY public.txscenarioformula DROP CONSTRAINT txscenarioformula_pkey;
       public         postgres    false    319                        2606    27884 ,   txscenarioparameter txscenarioparameter_pkey 
   CONSTRAINT     }   ALTER TABLE ONLY public.txscenarioparameter
    ADD CONSTRAINT txscenarioparameter_pkey PRIMARY KEY (scenario_parameter_id);
 V   ALTER TABLE ONLY public.txscenarioparameter DROP CONSTRAINT txscenarioparameter_pkey;
       public         postgres    false    320            $           2606    27886 2   txscenarioparameterday txscenarioparameterday_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioparameterday
    ADD CONSTRAINT txscenarioparameterday_pkey PRIMARY KEY (scenario_parameter_day_id);
 \   ALTER TABLE ONLY public.txscenarioparameterday DROP CONSTRAINT txscenarioparameterday_pkey;
       public         postgres    false    321            *           2606    27888 2   txscenarioposturecurve txscenarioposturecurve_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioposturecurve
    ADD CONSTRAINT txscenarioposturecurve_pkey PRIMARY KEY (scenario_posture_id);
 \   ALTER TABLE ONLY public.txscenarioposturecurve DROP CONSTRAINT txscenarioposturecurve_pkey;
       public         postgres    false    322            .           2606    27890 (   txscenarioprocess txscenarioprocess_pkey 
   CONSTRAINT     w   ALTER TABLE ONLY public.txscenarioprocess
    ADD CONSTRAINT txscenarioprocess_pkey PRIMARY KEY (scenario_process_id);
 R   ALTER TABLE ONLY public.txscenarioprocess DROP CONSTRAINT txscenarioprocess_pkey;
       public         postgres    false    323            m           2606    27892    mdrol uniqueRolName 
   CONSTRAINT     T   ALTER TABLE ONLY public.mdrol
    ADD CONSTRAINT "uniqueRolName" UNIQUE (rol_name);
 ?   ALTER TABLE ONLY public.mdrol DROP CONSTRAINT "uniqueRolName";
       public         postgres    false    262            �           1259    27893    calendar_index    INDEX     N   CREATE INDEX calendar_index ON public.txcalendarday USING hash (calendar_id);
 "   DROP INDEX public.calendar_index;
       public         postgres    false    305            `           1259    27894    calendarid_index    INDEX     L   CREATE INDEX calendarid_index ON public.mdprocess USING hash (calendar_id);
 $   DROP INDEX public.calendarid_index;
       public         postgres    false    258            �           1259    27895 
   code_index    INDEX     H   CREATE UNIQUE INDEX code_index ON public.txcalendar USING btree (code);
    DROP INDEX public.code_index;
       public         postgres    false    304            �           1259    27896 
   date_index    INDEX     G   CREATE INDEX date_index ON public.txcalendarday USING hash (use_date);
    DROP INDEX public.date_index;
       public         postgres    false    305            2           1259    27897    fki_FK_ id_aba_time_unit    INDEX     p   CREATE INDEX "fki_FK_ id_aba_time_unit" ON public.aba_consumption_and_mortality USING btree (id_aba_time_unit);
 .   DROP INDEX public."fki_FK_ id_aba_time_unit";
       public         postgres    false    200            H           1259    27898    fki_FK_id_aba_breeds_and_stages    INDEX     �   CREATE INDEX "fki_FK_id_aba_breeds_and_stages" ON public.aba_stages_of_breeds_and_stages USING btree (id_aba_breeds_and_stages);
 5   DROP INDEX public."fki_FK_id_aba_breeds_and_stages";
       public         postgres    false    214            .           1259    27899 '   fki_FK_id_aba_consumption_and_mortality    INDEX     �   CREATE INDEX "fki_FK_id_aba_consumption_and_mortality" ON public.aba_breeds_and_stages USING btree (id_aba_consumption_and_mortality);
 =   DROP INDEX public."fki_FK_id_aba_consumption_and_mortality";
       public         postgres    false    198            7           1259    27900 (   fki_FK_id_aba_consumption_and_mortality2    INDEX     �   CREATE INDEX "fki_FK_id_aba_consumption_and_mortality2" ON public.aba_consumption_and_mortality_detail USING btree (id_aba_consumption_and_mortality);
 >   DROP INDEX public."fki_FK_id_aba_consumption_and_mortality2";
       public         postgres    false    202            <           1259    27901    fki_FK_id_aba_element    INDEX     m   CREATE INDEX "fki_FK_id_aba_element" ON public.aba_elements_and_concentrations USING btree (id_aba_element);
 +   DROP INDEX public."fki_FK_id_aba_element";
       public         postgres    false    206            E           1259    27902    fki_FK_id_aba_element2    INDEX     Z   CREATE INDEX "fki_FK_id_aba_element2" ON public.aba_results USING btree (id_aba_element);
 ,   DROP INDEX public."fki_FK_id_aba_element2";
       public         postgres    false    212            =           1259    27903    fki_FK_id_aba_element_property    INDEX        CREATE INDEX "fki_FK_id_aba_element_property" ON public.aba_elements_and_concentrations USING btree (id_aba_element_property);
 4   DROP INDEX public."fki_FK_id_aba_element_property";
       public         postgres    false    206            >           1259    27904    fki_FK_id_aba_formulation    INDEX     u   CREATE INDEX "fki_FK_id_aba_formulation" ON public.aba_elements_and_concentrations USING btree (id_aba_formulation);
 /   DROP INDEX public."fki_FK_id_aba_formulation";
       public         postgres    false    206            3           1259    27905    fki_FK_id_breed    INDEX     _   CREATE INDEX "fki_FK_id_breed" ON public.aba_consumption_and_mortality USING btree (id_breed);
 %   DROP INDEX public."fki_FK_id_breed";
       public         postgres    false    200            I           1259    27906    fki_FK_id_formulation    INDEX     m   CREATE INDEX "fki_FK_id_formulation" ON public.aba_stages_of_breeds_and_stages USING btree (id_formulation);
 +   DROP INDEX public."fki_FK_id_formulation";
       public         postgres    false    214            /           1259    27907    fki_FK_id_process    INDEX     [   CREATE INDEX "fki_FK_id_process" ON public.aba_breeds_and_stages USING btree (id_process);
 '   DROP INDEX public."fki_FK_id_process";
       public         postgres    false    198            4           1259    27908    fki_FK_id_stage    INDEX     _   CREATE INDEX "fki_FK_id_stage" ON public.aba_consumption_and_mortality USING btree (id_stage);
 %   DROP INDEX public."fki_FK_id_stage";
       public         postgres    false    200            N           1259    27909 )   fki_mdapplication_rol_application_id_fkey    INDEX     q   CREATE INDEX fki_mdapplication_rol_application_id_fkey ON public.mdapplication_rol USING btree (application_id);
 =   DROP INDEX public.fki_mdapplication_rol_application_id_fkey;
       public         postgres    false    249            O           1259    27910 !   fki_mdapplication_rol_rol_id_fkey    INDEX     a   CREATE INDEX fki_mdapplication_rol_rol_id_fkey ON public.mdapplication_rol USING btree (rol_id);
 5   DROP INDEX public.fki_mdapplication_rol_rol_id_fkey;
       public         postgres    false    249            \           1259    27911    fki_mdparameter_measure_id_fkey    INDEX     ]   CREATE INDEX fki_mdparameter_measure_id_fkey ON public.mdparameter USING btree (measure_id);
 3   DROP INDEX public.fki_mdparameter_measure_id_fkey;
       public         postgres    false    256            ]           1259    27912    fki_mdparameter_process_id_fkey    INDEX     ]   CREATE INDEX fki_mdparameter_process_id_fkey ON public.mdparameter USING btree (process_id);
 3   DROP INDEX public.fki_mdparameter_process_id_fkey;
       public         postgres    false    256            a           1259    27913    fki_mdprocess_breed_id_fkey    INDEX     U   CREATE INDEX fki_mdprocess_breed_id_fkey ON public.mdprocess USING btree (breed_id);
 /   DROP INDEX public.fki_mdprocess_breed_id_fkey;
       public         postgres    false    258            n           1259    27914    fki_mdscenario_calendar_id_fkey    INDEX     ]   CREATE INDEX fki_mdscenario_calendar_id_fkey ON public.mdscenario USING btree (calendar_id);
 3   DROP INDEX public.fki_mdscenario_calendar_id_fkey;
       public         postgres    false    264            y           1259    27915    fki_mduser_rol_id_fkey    INDEX     K   CREATE INDEX fki_mduser_rol_id_fkey ON public.mduser USING btree (rol_id);
 *   DROP INDEX public.fki_mduser_rol_id_fkey;
       public         postgres    false    270            ~           1259    27916    fki_oscenter_farm_id_fkey    INDEX     Q   CREATE INDEX fki_oscenter_farm_id_fkey ON public.oscenter USING btree (farm_id);
 -   DROP INDEX public.fki_oscenter_farm_id_fkey;
       public         postgres    false    271            �           1259    27917 '   fki_oscenter_oswarehouse_center_id_fkey    INDEX     m   CREATE INDEX fki_oscenter_oswarehouse_center_id_fkey ON public.oscenter_oswarehouse USING btree (center_id);
 ;   DROP INDEX public.fki_oscenter_oswarehouse_center_id_fkey;
       public         postgres    false    272            �           1259    27918 %   fki_oscenter_oswarehouse_farm_id_fkey    INDEX     i   CREATE INDEX fki_oscenter_oswarehouse_farm_id_fkey ON public.oscenter_oswarehouse USING btree (farm_id);
 9   DROP INDEX public.fki_oscenter_oswarehouse_farm_id_fkey;
       public         postgres    false    272            �           1259    27919 ,   fki_oscenter_oswarehouse_partnership_id_fkey    INDEX     w   CREATE INDEX fki_oscenter_oswarehouse_partnership_id_fkey ON public.oscenter_oswarehouse USING btree (partnership_id);
 @   DROP INDEX public.fki_oscenter_oswarehouse_partnership_id_fkey;
       public         postgres    false    272            �           1259    27920 *   fki_oscenter_oswarehouse_warehouse_id_fkey    INDEX     s   CREATE INDEX fki_oscenter_oswarehouse_warehouse_id_fkey ON public.oscenter_oswarehouse USING btree (warehouse_id);
 >   DROP INDEX public.fki_oscenter_oswarehouse_warehouse_id_fkey;
       public         postgres    false    272                       1259    27921     fki_oscenter_partnership_id_fkey    INDEX     _   CREATE INDEX fki_oscenter_partnership_id_fkey ON public.oscenter USING btree (partnership_id);
 4   DROP INDEX public.fki_oscenter_partnership_id_fkey;
       public         postgres    false    271            �           1259    27922    fki_osfarm_farm_type_id_fkey    INDEX     W   CREATE INDEX fki_osfarm_farm_type_id_fkey ON public.osfarm USING btree (farm_type_id);
 0   DROP INDEX public.fki_osfarm_farm_type_id_fkey;
       public         postgres    false    273            �           1259    27923    fki_osfarm_partnership_id_fkey    INDEX     [   CREATE INDEX fki_osfarm_partnership_id_fkey ON public.osfarm USING btree (partnership_id);
 2   DROP INDEX public.fki_osfarm_partnership_id_fkey;
       public         postgres    false    273            �           1259    27924 '   fki_osincubator_incubator_plant_id_fkey    INDEX     m   CREATE INDEX fki_osincubator_incubator_plant_id_fkey ON public.osincubator USING btree (incubator_plant_id);
 ;   DROP INDEX public.fki_osincubator_incubator_plant_id_fkey;
       public         postgres    false    274            �           1259    27925 (   fki_osincubatorplant_partnership_id_fkey    INDEX     o   CREATE INDEX fki_osincubatorplant_partnership_id_fkey ON public.osincubatorplant USING btree (partnership_id);
 <   DROP INDEX public.fki_osincubatorplant_partnership_id_fkey;
       public         postgres    false    275            �           1259    27926    fki_osshed_center_id_fkey    INDEX     Q   CREATE INDEX fki_osshed_center_id_fkey ON public.osshed USING btree (center_id);
 -   DROP INDEX public.fki_osshed_center_id_fkey;
       public         postgres    false    279            �           1259    27927    fki_osshed_farm_id_fkey    INDEX     M   CREATE INDEX fki_osshed_farm_id_fkey ON public.osshed USING btree (farm_id);
 +   DROP INDEX public.fki_osshed_farm_id_fkey;
       public         postgres    false    279            �           1259    27928    fki_osshed_partnership_id_fkey    INDEX     [   CREATE INDEX fki_osshed_partnership_id_fkey ON public.osshed USING btree (partnership_id);
 2   DROP INDEX public.fki_osshed_partnership_id_fkey;
       public         postgres    false    279            �           1259    27929    fki_osshed_statusshed_id_fkey    INDEX     Y   CREATE INDEX fki_osshed_statusshed_id_fkey ON public.osshed USING btree (statusshed_id);
 1   DROP INDEX public.fki_osshed_statusshed_id_fkey;
       public         postgres    false    279            �           1259    27930    fki_ossilo_center_id_fkey    INDEX     Q   CREATE INDEX fki_ossilo_center_id_fkey ON public.ossilo USING btree (center_id);
 -   DROP INDEX public.fki_ossilo_center_id_fkey;
       public         postgres    false    281            �           1259    27931    fki_ossilo_farm_id_fkey    INDEX     M   CREATE INDEX fki_ossilo_farm_id_fkey ON public.ossilo USING btree (farm_id);
 +   DROP INDEX public.fki_ossilo_farm_id_fkey;
       public         postgres    false    281            �           1259    27932     fki_ossilo_osshed_center_id_fkey    INDEX     _   CREATE INDEX fki_ossilo_osshed_center_id_fkey ON public.ossilo_osshed USING btree (center_id);
 4   DROP INDEX public.fki_ossilo_osshed_center_id_fkey;
       public         postgres    false    282            �           1259    27933    fki_ossilo_osshed_farm_id_fkey    INDEX     [   CREATE INDEX fki_ossilo_osshed_farm_id_fkey ON public.ossilo_osshed USING btree (farm_id);
 2   DROP INDEX public.fki_ossilo_osshed_farm_id_fkey;
       public         postgres    false    282            �           1259    27934 %   fki_ossilo_osshed_partnership_id_fkey    INDEX     i   CREATE INDEX fki_ossilo_osshed_partnership_id_fkey ON public.ossilo_osshed USING btree (partnership_id);
 9   DROP INDEX public.fki_ossilo_osshed_partnership_id_fkey;
       public         postgres    false    282            �           1259    27935    fki_ossilo_osshed_shed_id_fkey    INDEX     [   CREATE INDEX fki_ossilo_osshed_shed_id_fkey ON public.ossilo_osshed USING btree (shed_id);
 2   DROP INDEX public.fki_ossilo_osshed_shed_id_fkey;
       public         postgres    false    282            �           1259    27936    fki_ossilo_osshed_silo_id_fkey    INDEX     [   CREATE INDEX fki_ossilo_osshed_silo_id_fkey ON public.ossilo_osshed USING btree (silo_id);
 2   DROP INDEX public.fki_ossilo_osshed_silo_id_fkey;
       public         postgres    false    282            �           1259    27937    fki_ossilo_partnership_id_fkey    INDEX     [   CREATE INDEX fki_ossilo_partnership_id_fkey ON public.ossilo USING btree (partnership_id);
 2   DROP INDEX public.fki_ossilo_partnership_id_fkey;
       public         postgres    false    281            �           1259    27938    fki_oswarehouse_farm_id_fkey    INDEX     W   CREATE INDEX fki_oswarehouse_farm_id_fkey ON public.oswarehouse USING btree (farm_id);
 0   DROP INDEX public.fki_oswarehouse_farm_id_fkey;
       public         postgres    false    286            �           1259    27939 #   fki_oswarehouse_partnership_id_fkey    INDEX     e   CREATE INDEX fki_oswarehouse_partnership_id_fkey ON public.oswarehouse USING btree (partnership_id);
 7   DROP INDEX public.fki_oswarehouse_partnership_id_fkey;
       public         postgres    false    286            b           1259    27940    fki_process_product_id_fkey    INDEX     W   CREATE INDEX fki_process_product_id_fkey ON public.mdprocess USING btree (product_id);
 /   DROP INDEX public.fki_process_product_id_fkey;
       public         postgres    false    258            c           1259    27941    fki_process_stage_id_fkey    INDEX     S   CREATE INDEX fki_process_stage_id_fkey ON public.mdprocess USING btree (stage_id);
 -   DROP INDEX public.fki_process_stage_id_fkey;
       public         postgres    false    258            �           1259    27942 %   fki_txavailabilitysheds_lot_code_fkey    INDEX     i   CREATE INDEX fki_txavailabilitysheds_lot_code_fkey ON public.txavailabilitysheds USING btree (lot_code);
 9   DROP INDEX public.fki_txavailabilitysheds_lot_code_fkey;
       public         postgres    false    297            �           1259    27943 $   fki_txavailabilitysheds_shed_id_fkey    INDEX     g   CREATE INDEX fki_txavailabilitysheds_shed_id_fkey ON public.txavailabilitysheds USING btree (shed_id);
 8   DROP INDEX public.fki_txavailabilitysheds_shed_id_fkey;
       public         postgres    false    297            �           1259    27944 $   fki_txbroiler_detail_broiler_id_fkey    INDEX     g   CREATE INDEX fki_txbroiler_detail_broiler_id_fkey ON public.txbroiler_detail USING btree (broiler_id);
 8   DROP INDEX public.fki_txbroiler_detail_broiler_id_fkey;
       public         postgres    false    299            �           1259    27945 !   fki_txbroiler_detail_farm_id_fkey    INDEX     a   CREATE INDEX fki_txbroiler_detail_farm_id_fkey ON public.txbroiler_detail USING btree (farm_id);
 5   DROP INDEX public.fki_txbroiler_detail_farm_id_fkey;
       public         postgres    false    299            �           1259    27946 !   fki_txbroiler_detail_shed_id_fkey    INDEX     a   CREATE INDEX fki_txbroiler_detail_shed_id_fkey ON public.txbroiler_detail USING btree (shed_id);
 5   DROP INDEX public.fki_txbroiler_detail_shed_id_fkey;
       public         postgres    false    299            �           1259    27947 %   fki_txbroiler_programmed_eggs_id_fkey    INDEX     i   CREATE INDEX fki_txbroiler_programmed_eggs_id_fkey ON public.txbroiler USING btree (programmed_eggs_id);
 9   DROP INDEX public.fki_txbroiler_programmed_eggs_id_fkey;
       public         postgres    false    298            �           1259    27948 #   fki_txbroilereviction_breed_id_fkey    INDEX     e   CREATE INDEX fki_txbroilereviction_breed_id_fkey ON public.txbroilereviction USING btree (breed_id);
 7   DROP INDEX public.fki_txbroilereviction_breed_id_fkey;
       public         postgres    false    300            �           1259    27949 ,   fki_txbroilereviction_detail_broiler_id_fkey    INDEX        CREATE INDEX fki_txbroilereviction_detail_broiler_id_fkey ON public.txbroilereviction_detail USING btree (broilereviction_id);
 @   DROP INDEX public.fki_txbroilereviction_detail_broiler_id_fkey;
       public         postgres    false    301            �           1259    27950 4   fki_txbroilereviction_detail_broiler_product_id_fkey    INDEX     �   CREATE INDEX fki_txbroilereviction_detail_broiler_product_id_fkey ON public.txbroilereviction_detail USING btree (broiler_product_id);
 H   DROP INDEX public.fki_txbroilereviction_detail_broiler_product_id_fkey;
       public         postgres    false    301            �           1259    27951 )   fki_txbroilereviction_detail_farm_id_fkey    INDEX     q   CREATE INDEX fki_txbroilereviction_detail_farm_id_fkey ON public.txbroilereviction_detail USING btree (farm_id);
 =   DROP INDEX public.fki_txbroilereviction_detail_farm_id_fkey;
       public         postgres    false    301            �           1259    27952 )   fki_txbroilereviction_detail_shed_id_fkey    INDEX     q   CREATE INDEX fki_txbroilereviction_detail_shed_id_fkey ON public.txbroilereviction_detail USING btree (shed_id);
 =   DROP INDEX public.fki_txbroilereviction_detail_shed_id_fkey;
       public         postgres    false    301            �           1259    27953 3   fki_txbroilereviction_detail_slaughterhouse_id_fkey    INDEX     �   CREATE INDEX fki_txbroilereviction_detail_slaughterhouse_id_fkey ON public.txbroilereviction_detail USING btree (slaughterhouse_id);
 G   DROP INDEX public.fki_txbroilereviction_detail_slaughterhouse_id_fkey;
       public         postgres    false    301            �           1259    27954 )   fki_txbroilereviction_partnership_id_fkey    INDEX     q   CREATE INDEX fki_txbroilereviction_partnership_id_fkey ON public.txbroilereviction USING btree (partnership_id);
 =   DROP INDEX public.fki_txbroilereviction_partnership_id_fkey;
       public         postgres    false    300            �           1259    27955 &   fki_txbroilereviction_scenario_id_fkey    INDEX     k   CREATE INDEX fki_txbroilereviction_scenario_id_fkey ON public.txbroilereviction USING btree (scenario_id);
 :   DROP INDEX public.fki_txbroilereviction_scenario_id_fkey;
       public         postgres    false    300            �           1259    27956 /   fki_txbroilerproduct_detail_broiler_detail_fkey    INDEX     }   CREATE INDEX fki_txbroilerproduct_detail_broiler_detail_fkey ON public.txbroilerproduct_detail USING btree (broiler_detail);
 C   DROP INDEX public.fki_txbroilerproduct_detail_broiler_detail_fkey;
       public         postgres    false    302            �           1259    27957 "   fki_txbroodermachines_farm_id_fkey    INDEX     b   CREATE INDEX fki_txbroodermachines_farm_id_fkey ON public.txbroodermachine USING btree (farm_id);
 6   DROP INDEX public.fki_txbroodermachines_farm_id_fkey;
       public         postgres    false    303            �           1259    27958 )   fki_txbroodermachines_partnership_id_fkey    INDEX     p   CREATE INDEX fki_txbroodermachines_partnership_id_fkey ON public.txbroodermachine USING btree (partnership_id);
 =   DROP INDEX public.fki_txbroodermachines_partnership_id_fkey;
       public         postgres    false    303            �           1259    27959 !   fki_txeggs_planning_breed_id_fkey    INDEX     a   CREATE INDEX fki_txeggs_planning_breed_id_fkey ON public.txeggs_planning USING btree (breed_id);
 5   DROP INDEX public.fki_txeggs_planning_breed_id_fkey;
       public         postgres    false    308            �           1259    27960 $   fki_txeggs_planning_scenario_id_fkey    INDEX     g   CREATE INDEX fki_txeggs_planning_scenario_id_fkey ON public.txeggs_planning USING btree (scenario_id);
 8   DROP INDEX public.fki_txeggs_planning_scenario_id_fkey;
       public         postgres    false    308            �           1259    27961 !   fki_txeggs_required_breed_id_fkey    INDEX     a   CREATE INDEX fki_txeggs_required_breed_id_fkey ON public.txeggs_required USING btree (breed_id);
 5   DROP INDEX public.fki_txeggs_required_breed_id_fkey;
       public         postgres    false    309            �           1259    27962 $   fki_txeggs_required_scenario_id_fkey    INDEX     g   CREATE INDEX fki_txeggs_required_scenario_id_fkey ON public.txeggs_required USING btree (scenario_id);
 8   DROP INDEX public.fki_txeggs_required_scenario_id_fkey;
       public         postgres    false    309            �           1259    27963     fki_txeggs_storage_breed_id_fkey    INDEX     _   CREATE INDEX fki_txeggs_storage_breed_id_fkey ON public.txeggs_storage USING btree (breed_id);
 4   DROP INDEX public.fki_txeggs_storage_breed_id_fkey;
       public         postgres    false    310            �           1259    27964 *   fki_txeggs_storage_incubator_plant_id_fkey    INDEX     s   CREATE INDEX fki_txeggs_storage_incubator_plant_id_fkey ON public.txeggs_storage USING btree (incubator_plant_id);
 >   DROP INDEX public.fki_txeggs_storage_incubator_plant_id_fkey;
       public         postgres    false    310            �           1259    27965 #   fki_txeggs_storage_scenario_id_fkey    INDEX     e   CREATE INDEX fki_txeggs_storage_scenario_id_fkey ON public.txeggs_storage USING btree (scenario_id);
 7   DROP INDEX public.fki_txeggs_storage_scenario_id_fkey;
       public         postgres    false    310            �           1259    27966    fki_txfattening_breed_id_fkey    INDEX     W   CREATE INDEX fki_txfattening_breed_id_fkey ON public.txbroiler USING btree (breed_id);
 1   DROP INDEX public.fki_txfattening_breed_id_fkey;
       public         postgres    false    298            �           1259    27967 #   fki_txfattening_partnership_id_fkey    INDEX     c   CREATE INDEX fki_txfattening_partnership_id_fkey ON public.txbroiler USING btree (partnership_id);
 7   DROP INDEX public.fki_txfattening_partnership_id_fkey;
       public         postgres    false    298            �           1259    27968     fki_txfattening_scenario_id_fkey    INDEX     ]   CREATE INDEX fki_txfattening_scenario_id_fkey ON public.txbroiler USING btree (scenario_id);
 4   DROP INDEX public.fki_txfattening_scenario_id_fkey;
       public         postgres    false    298            �           1259    27969    fki_txgoals_erp_product_id_fkey    INDEX     ]   CREATE INDEX fki_txgoals_erp_product_id_fkey ON public.txgoals_erp USING btree (product_id);
 3   DROP INDEX public.fki_txgoals_erp_product_id_fkey;
       public         postgres    false    311            �           1259    27970     fki_txgoals_erp_scenario_id_fkey    INDEX     _   CREATE INDEX fki_txgoals_erp_scenario_id_fkey ON public.txgoals_erp USING btree (scenario_id);
 4   DROP INDEX public.fki_txgoals_erp_scenario_id_fkey;
       public         postgres    false    311            �           1259    27971    fki_txhousingway_breed_id_fkey    INDEX     [   CREATE INDEX fki_txhousingway_breed_id_fkey ON public.txhousingway USING btree (breed_id);
 2   DROP INDEX public.fki_txhousingway_breed_id_fkey;
       public         postgres    false    313            �           1259    27972 $   fki_txhousingway_detail_farm_id_fkey    INDEX     g   CREATE INDEX fki_txhousingway_detail_farm_id_fkey ON public.txhousingway_detail USING btree (farm_id);
 8   DROP INDEX public.fki_txhousingway_detail_farm_id_fkey;
       public         postgres    false    314            �           1259    27973 +   fki_txhousingway_detail_housing_way_id_fkey    INDEX     u   CREATE INDEX fki_txhousingway_detail_housing_way_id_fkey ON public.txhousingway_detail USING btree (housing_way_id);
 ?   DROP INDEX public.fki_txhousingway_detail_housing_way_id_fkey;
       public         postgres    false    314            �           1259    27974 /   fki_txhousingway_detail_incubator_plant_id_fkey    INDEX     }   CREATE INDEX fki_txhousingway_detail_incubator_plant_id_fkey ON public.txhousingway_detail USING btree (incubator_plant_id);
 C   DROP INDEX public.fki_txhousingway_detail_incubator_plant_id_fkey;
       public         postgres    false    314                        1259    27975 $   fki_txhousingway_detail_shed_id_fkey    INDEX     g   CREATE INDEX fki_txhousingway_detail_shed_id_fkey ON public.txhousingway_detail USING btree (shed_id);
 8   DROP INDEX public.fki_txhousingway_detail_shed_id_fkey;
       public         postgres    false    314            �           1259    27976 $   fki_txhousingway_partnership_id_fkey    INDEX     g   CREATE INDEX fki_txhousingway_partnership_id_fkey ON public.txhousingway USING btree (partnership_id);
 8   DROP INDEX public.fki_txhousingway_partnership_id_fkey;
       public         postgres    false    313            �           1259    27977 !   fki_txhousingway_scenario_id_fkey    INDEX     a   CREATE INDEX fki_txhousingway_scenario_id_fkey ON public.txhousingway USING btree (scenario_id);
 5   DROP INDEX public.fki_txhousingway_scenario_id_fkey;
       public         postgres    false    313            �           1259    27978    fki_txhousingway_stage_id_fkey    INDEX     [   CREATE INDEX fki_txhousingway_stage_id_fkey ON public.txhousingway USING btree (stage_id);
 2   DROP INDEX public.fki_txhousingway_stage_id_fkey;
       public         postgres    false    313                       1259    27979    fki_txlot_breed_id_fkey    INDEX     M   CREATE INDEX fki_txlot_breed_id_fkey ON public.txlot USING btree (breed_id);
 +   DROP INDEX public.fki_txlot_breed_id_fkey;
       public         postgres    false    315                       1259    27980    fki_txlot_farm_id_fkey    INDEX     K   CREATE INDEX fki_txlot_farm_id_fkey ON public.txlot USING btree (farm_id);
 *   DROP INDEX public.fki_txlot_farm_id_fkey;
       public         postgres    false    315                       1259    27981    fki_txlot_housin_way_id_fkey    INDEX     X   CREATE INDEX fki_txlot_housin_way_id_fkey ON public.txlot USING btree (housing_way_id);
 0   DROP INDEX public.fki_txlot_housin_way_id_fkey;
       public         postgres    false    315                       1259    27982    fki_txlot_product_id_fkey    INDEX     Q   CREATE INDEX fki_txlot_product_id_fkey ON public.txlot USING btree (product_id);
 -   DROP INDEX public.fki_txlot_product_id_fkey;
       public         postgres    false    315                       1259    27983    fki_txlot_shed_id_fkey    INDEX     K   CREATE INDEX fki_txlot_shed_id_fkey ON public.txlot USING btree (shed_id);
 *   DROP INDEX public.fki_txlot_shed_id_fkey;
       public         postgres    false    315                       1259    27984     fki_txposturecurve_breed_id_fkey    INDEX     _   CREATE INDEX fki_txposturecurve_breed_id_fkey ON public.txposturecurve USING btree (breed_id);
 4   DROP INDEX public.fki_txposturecurve_breed_id_fkey;
       public         postgres    false    317                       1259    27985 #   fki_txprogrammed_eggs_breed_id_fkey    INDEX     e   CREATE INDEX fki_txprogrammed_eggs_breed_id_fkey ON public.txprogrammed_eggs USING btree (breed_id);
 7   DROP INDEX public.fki_txprogrammed_eggs_breed_id_fkey;
       public         postgres    false    318                       1259    27986 *   fki_txprogrammed_eggs_eggs_storage_id_fkey    INDEX     s   CREATE INDEX fki_txprogrammed_eggs_eggs_storage_id_fkey ON public.txprogrammed_eggs USING btree (eggs_storage_id);
 >   DROP INDEX public.fki_txprogrammed_eggs_eggs_storage_id_fkey;
       public         postgres    false    318                       1259    27987 '   fki_txprogrammed_eggs_incubator_id_fkey    INDEX     m   CREATE INDEX fki_txprogrammed_eggs_incubator_id_fkey ON public.txprogrammed_eggs USING btree (incubator_id);
 ;   DROP INDEX public.fki_txprogrammed_eggs_incubator_id_fkey;
       public         postgres    false    318                       1259    27988 %   fki_txscenarioformula_measure_id_fkey    INDEX     i   CREATE INDEX fki_txscenarioformula_measure_id_fkey ON public.txscenarioformula USING btree (measure_id);
 9   DROP INDEX public.fki_txscenarioformula_measure_id_fkey;
       public         postgres    false    319                       1259    27989 '   fki_txscenarioformula_parameter_id_fkey    INDEX     m   CREATE INDEX fki_txscenarioformula_parameter_id_fkey ON public.txscenarioformula USING btree (parameter_id);
 ;   DROP INDEX public.fki_txscenarioformula_parameter_id_fkey;
       public         postgres    false    319                       1259    27990 %   fki_txscenarioformula_process_id_fkey    INDEX     i   CREATE INDEX fki_txscenarioformula_process_id_fkey ON public.txscenarioformula USING btree (process_id);
 9   DROP INDEX public.fki_txscenarioformula_process_id_fkey;
       public         postgres    false    319                       1259    27991 &   fki_txscenarioformula_scenario_id_fkey    INDEX     k   CREATE INDEX fki_txscenarioformula_scenario_id_fkey ON public.txscenarioformula USING btree (scenario_id);
 :   DROP INDEX public.fki_txscenarioformula_scenario_id_fkey;
       public         postgres    false    319                       1259    27992 )   fki_txscenarioparameter_parameter_id_fkey    INDEX     q   CREATE INDEX fki_txscenarioparameter_parameter_id_fkey ON public.txscenarioparameter USING btree (parameter_id);
 =   DROP INDEX public.fki_txscenarioparameter_parameter_id_fkey;
       public         postgres    false    320                       1259    27993 '   fki_txscenarioparameter_process_id_fkey    INDEX     m   CREATE INDEX fki_txscenarioparameter_process_id_fkey ON public.txscenarioparameter USING btree (process_id);
 ;   DROP INDEX public.fki_txscenarioparameter_process_id_fkey;
       public         postgres    false    320                       1259    27994 (   fki_txscenarioparameter_scenario_id_fkey    INDEX     o   CREATE INDEX fki_txscenarioparameter_scenario_id_fkey ON public.txscenarioparameter USING btree (scenario_id);
 <   DROP INDEX public.fki_txscenarioparameter_scenario_id_fkey;
       public         postgres    false    320            !           1259    27995 ,   fki_txscenarioparameterday_parameter_id_fkey    INDEX     w   CREATE INDEX fki_txscenarioparameterday_parameter_id_fkey ON public.txscenarioparameterday USING btree (parameter_id);
 @   DROP INDEX public.fki_txscenarioparameterday_parameter_id_fkey;
       public         postgres    false    321            "           1259    27996 +   fki_txscenarioparameterday_scenario_id_fkey    INDEX     u   CREATE INDEX fki_txscenarioparameterday_scenario_id_fkey ON public.txscenarioparameterday USING btree (scenario_id);
 ?   DROP INDEX public.fki_txscenarioparameterday_scenario_id_fkey;
       public         postgres    false    321            %           1259    27997 (   fki_txscenarioposturecurve_breed_id_fkey    INDEX     o   CREATE INDEX fki_txscenarioposturecurve_breed_id_fkey ON public.txscenarioposturecurve USING btree (breed_id);
 <   DROP INDEX public.fki_txscenarioposturecurve_breed_id_fkey;
       public         postgres    false    322            &           1259    27998 4   fki_txscenarioposturecurve_housingway_detail_id_fkey    INDEX     �   CREATE INDEX fki_txscenarioposturecurve_housingway_detail_id_fkey ON public.txscenarioposturecurve USING btree (housingway_detail_id);
 H   DROP INDEX public.fki_txscenarioposturecurve_housingway_detail_id_fkey;
       public         postgres    false    322            '           1259    27999 +   fki_txscenarioposturecurve_scenario_id_fkey    INDEX     u   CREATE INDEX fki_txscenarioposturecurve_scenario_id_fkey ON public.txscenarioposturecurve USING btree (scenario_id);
 ?   DROP INDEX public.fki_txscenarioposturecurve_scenario_id_fkey;
       public         postgres    false    322            +           1259    28000 %   fki_txscenarioprocess_process_id_fkey    INDEX     i   CREATE INDEX fki_txscenarioprocess_process_id_fkey ON public.txscenarioprocess USING btree (process_id);
 9   DROP INDEX public.fki_txscenarioprocess_process_id_fkey;
       public         postgres    false    323            ,           1259    28001 &   fki_txscenarioprocess_scenario_id_fkey    INDEX     k   CREATE INDEX fki_txscenarioprocess_scenario_id_fkey ON public.txscenarioprocess USING btree (scenario_id);
 :   DROP INDEX public.fki_txscenarioprocess_scenario_id_fkey;
       public         postgres    false    323            (           1259    28002    posturedate_index    INDEX     [   CREATE INDEX posturedate_index ON public.txscenarioposturecurve USING hash (posture_date);
 %   DROP INDEX public.posturedate_index;
       public         postgres    false    322            �           1259    28003    sequence_index    INDEX     L   CREATE INDEX sequence_index ON public.txcalendarday USING btree (sequence);
 "   DROP INDEX public.sequence_index;
       public         postgres    false    305            9           2606    28004 ;   aba_stages_of_breeds_and_stages FK_id_aba_breeds_and_stages    FK CONSTRAINT     �   ALTER TABLE ONLY public.aba_stages_of_breeds_and_stages
    ADD CONSTRAINT "FK_id_aba_breeds_and_stages" FOREIGN KEY (id_aba_breeds_and_stages) REFERENCES public.aba_breeds_and_stages(id) ON DELETE CASCADE;
 g   ALTER TABLE ONLY public.aba_stages_of_breeds_and_stages DROP CONSTRAINT "FK_id_aba_breeds_and_stages";
       public       postgres    false    214    3117    198            /           2606    28009 9   aba_breeds_and_stages FK_id_aba_consumption_and_mortality    FK CONSTRAINT     �   ALTER TABLE ONLY public.aba_breeds_and_stages
    ADD CONSTRAINT "FK_id_aba_consumption_and_mortality" FOREIGN KEY (id_aba_consumption_and_mortality) REFERENCES public.aba_consumption_and_mortality(id);
 e   ALTER TABLE ONLY public.aba_breeds_and_stages DROP CONSTRAINT "FK_id_aba_consumption_and_mortality";
       public       postgres    false    200    198    3121            4           2606    28014 I   aba_consumption_and_mortality_detail FK_id_aba_consumption_and_mortality2    FK CONSTRAINT     �   ALTER TABLE ONLY public.aba_consumption_and_mortality_detail
    ADD CONSTRAINT "FK_id_aba_consumption_and_mortality2" FOREIGN KEY (id_aba_consumption_and_mortality) REFERENCES public.aba_consumption_and_mortality(id) ON DELETE CASCADE;
 u   ALTER TABLE ONLY public.aba_consumption_and_mortality_detail DROP CONSTRAINT "FK_id_aba_consumption_and_mortality2";
       public       postgres    false    200    3121    202            5           2606    28019 1   aba_elements_and_concentrations FK_id_aba_element    FK CONSTRAINT     �   ALTER TABLE ONLY public.aba_elements_and_concentrations
    ADD CONSTRAINT "FK_id_aba_element" FOREIGN KEY (id_aba_element) REFERENCES public.aba_elements(id);
 ]   ALTER TABLE ONLY public.aba_elements_and_concentrations DROP CONSTRAINT "FK_id_aba_element";
       public       postgres    false    3129    206    204            8           2606    28024    aba_results FK_id_aba_element2    FK CONSTRAINT     �   ALTER TABLE ONLY public.aba_results
    ADD CONSTRAINT "FK_id_aba_element2" FOREIGN KEY (id_aba_element) REFERENCES public.aba_elements(id);
 J   ALTER TABLE ONLY public.aba_results DROP CONSTRAINT "FK_id_aba_element2";
       public       postgres    false    3129    204    212            6           2606    28029 :   aba_elements_and_concentrations FK_id_aba_element_property    FK CONSTRAINT     �   ALTER TABLE ONLY public.aba_elements_and_concentrations
    ADD CONSTRAINT "FK_id_aba_element_property" FOREIGN KEY (id_aba_element_property) REFERENCES public.aba_elements_properties(id);
 f   ALTER TABLE ONLY public.aba_elements_and_concentrations DROP CONSTRAINT "FK_id_aba_element_property";
       public       postgres    false    208    3136    206            7           2606    28034 5   aba_elements_and_concentrations FK_id_aba_formulation    FK CONSTRAINT     �   ALTER TABLE ONLY public.aba_elements_and_concentrations
    ADD CONSTRAINT "FK_id_aba_formulation" FOREIGN KEY (id_aba_formulation) REFERENCES public.aba_formulation(id) ON DELETE CASCADE;
 a   ALTER TABLE ONLY public.aba_elements_and_concentrations DROP CONSTRAINT "FK_id_aba_formulation";
       public       postgres    false    206    3138    210            1           2606    28039 1   aba_consumption_and_mortality FK_id_aba_time_unit    FK CONSTRAINT     �   ALTER TABLE ONLY public.aba_consumption_and_mortality
    ADD CONSTRAINT "FK_id_aba_time_unit" FOREIGN KEY (id_aba_time_unit) REFERENCES public.aba_time_unit(id);
 ]   ALTER TABLE ONLY public.aba_consumption_and_mortality DROP CONSTRAINT "FK_id_aba_time_unit";
       public       postgres    false    215    200    3147            2           2606    28044 )   aba_consumption_and_mortality FK_id_breed    FK CONSTRAINT     �   ALTER TABLE ONLY public.aba_consumption_and_mortality
    ADD CONSTRAINT "FK_id_breed" FOREIGN KEY (id_breed) REFERENCES public.mdbreed(breed_id);
 U   ALTER TABLE ONLY public.aba_consumption_and_mortality DROP CONSTRAINT "FK_id_breed";
       public       postgres    false    3157    250    200            :           2606    28049 1   aba_stages_of_breeds_and_stages FK_id_formulation    FK CONSTRAINT     �   ALTER TABLE ONLY public.aba_stages_of_breeds_and_stages
    ADD CONSTRAINT "FK_id_formulation" FOREIGN KEY (id_formulation) REFERENCES public.aba_formulation(id);
 ]   ALTER TABLE ONLY public.aba_stages_of_breeds_and_stages DROP CONSTRAINT "FK_id_formulation";
       public       postgres    false    3138    214    210            0           2606    28054 #   aba_breeds_and_stages FK_id_process    FK CONSTRAINT     �   ALTER TABLE ONLY public.aba_breeds_and_stages
    ADD CONSTRAINT "FK_id_process" FOREIGN KEY (id_process) REFERENCES public.mdprocess(process_id);
 O   ALTER TABLE ONLY public.aba_breeds_and_stages DROP CONSTRAINT "FK_id_process";
       public       postgres    false    258    3173    198            3           2606    28059 )   aba_consumption_and_mortality FK_id_stage    FK CONSTRAINT     �   ALTER TABLE ONLY public.aba_consumption_and_mortality
    ADD CONSTRAINT "FK_id_stage" FOREIGN KEY (id_stage) REFERENCES public.mdstage(stage_id);
 U   ALTER TABLE ONLY public.aba_consumption_and_mortality DROP CONSTRAINT "FK_id_stage";
       public       postgres    false    3192    268    200            x           2606    28064 *   txeggs_movements eggs_movements_storage_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.txeggs_movements
    ADD CONSTRAINT eggs_movements_storage_id FOREIGN KEY (eggs_storage_id) REFERENCES public.txeggs_storage(eggs_storage_id);
 T   ALTER TABLE ONLY public.txeggs_movements DROP CONSTRAINT eggs_movements_storage_id;
       public       postgres    false    310    3314    307            ;           2606    28069 7   mdapplication_rol mdapplication_rol_application_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.mdapplication_rol
    ADD CONSTRAINT mdapplication_rol_application_id_fkey FOREIGN KEY (application_id) REFERENCES public.mdapplication(application_id);
 a   ALTER TABLE ONLY public.mdapplication_rol DROP CONSTRAINT mdapplication_rol_application_id_fkey;
       public       postgres    false    247    3149    249            <           2606    28074 /   mdapplication_rol mdapplication_rol_rol_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.mdapplication_rol
    ADD CONSTRAINT mdapplication_rol_rol_id_fkey FOREIGN KEY (rol_id) REFERENCES public.mdrol(rol_id);
 Y   ALTER TABLE ONLY public.mdapplication_rol DROP CONSTRAINT mdapplication_rol_rol_id_fkey;
       public       postgres    false    249    3179    262            =           2606    28079 '   mdparameter mdparameter_measure_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.mdparameter
    ADD CONSTRAINT mdparameter_measure_id_fkey FOREIGN KEY (measure_id) REFERENCES public.mdmeasure(measure_id) ON UPDATE CASCADE ON DELETE CASCADE;
 Q   ALTER TABLE ONLY public.mdparameter DROP CONSTRAINT mdparameter_measure_id_fkey;
       public       postgres    false    256    3163    254            >           2606    28084 '   mdparameter mdparameter_process_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.mdparameter
    ADD CONSTRAINT mdparameter_process_id_fkey FOREIGN KEY (process_id) REFERENCES public.mdprocess(process_id) ON UPDATE CASCADE ON DELETE CASCADE;
 Q   ALTER TABLE ONLY public.mdparameter DROP CONSTRAINT mdparameter_process_id_fkey;
       public       postgres    false    256    3173    258            ?           2606    28089 !   mdprocess mdprocess_breed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.mdprocess
    ADD CONSTRAINT mdprocess_breed_id_fkey FOREIGN KEY (breed_id) REFERENCES public.mdbreed(breed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 K   ALTER TABLE ONLY public.mdprocess DROP CONSTRAINT mdprocess_breed_id_fkey;
       public       postgres    false    250    3157    258            @           2606    28094 $   mdprocess mdprocess_calendar_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.mdprocess
    ADD CONSTRAINT mdprocess_calendar_id_fkey FOREIGN KEY (calendar_id) REFERENCES public.txcalendar(calendar_id) ON UPDATE CASCADE ON DELETE CASCADE;
 N   ALTER TABLE ONLY public.mdprocess DROP CONSTRAINT mdprocess_calendar_id_fkey;
       public       postgres    false    258    304    3294            A           2606    28099 #   mdprocess mdprocess_product_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.mdprocess
    ADD CONSTRAINT mdprocess_product_id_fkey FOREIGN KEY (product_id) REFERENCES public.mdproduct(product_id) ON UPDATE CASCADE ON DELETE CASCADE;
 M   ALTER TABLE ONLY public.mdprocess DROP CONSTRAINT mdprocess_product_id_fkey;
       public       postgres    false    260    3177    258            B           2606    28104 !   mdprocess mdprocess_stage_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.mdprocess
    ADD CONSTRAINT mdprocess_stage_id_fkey FOREIGN KEY (stage_id) REFERENCES public.mdstage(stage_id) ON UPDATE CASCADE ON DELETE CASCADE;
 K   ALTER TABLE ONLY public.mdprocess DROP CONSTRAINT mdprocess_stage_id_fkey;
       public       postgres    false    3192    268    258            C           2606    28109 &   mdscenario mdscenario_calendar_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.mdscenario
    ADD CONSTRAINT mdscenario_calendar_id_fkey FOREIGN KEY (calendar_id) REFERENCES public.txcalendar(calendar_id);
 P   ALTER TABLE ONLY public.mdscenario DROP CONSTRAINT mdscenario_calendar_id_fkey;
       public       postgres    false    264    3294    304            D           2606    28114    mduser mduser_rol_id_fkey    FK CONSTRAINT     {   ALTER TABLE ONLY public.mduser
    ADD CONSTRAINT mduser_rol_id_fkey FOREIGN KEY (rol_id) REFERENCES public.mdrol(rol_id);
 C   ALTER TABLE ONLY public.mduser DROP CONSTRAINT mduser_rol_id_fkey;
       public       postgres    false    262    270    3179            E           2606    28119    oscenter oscenter_farm_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.oscenter
    ADD CONSTRAINT oscenter_farm_id_fkey FOREIGN KEY (farm_id) REFERENCES public.osfarm(farm_id) ON UPDATE CASCADE ON DELETE CASCADE;
 H   ALTER TABLE ONLY public.oscenter DROP CONSTRAINT oscenter_farm_id_fkey;
       public       postgres    false    3215    273    271            G           2606    28124 8   oscenter_oswarehouse oscenter_oswarehouse_center_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.oscenter_oswarehouse
    ADD CONSTRAINT oscenter_oswarehouse_center_id_fkey FOREIGN KEY (center_id) REFERENCES public.oscenter(center_id) ON UPDATE CASCADE ON DELETE CASCADE;
 b   ALTER TABLE ONLY public.oscenter_oswarehouse DROP CONSTRAINT oscenter_oswarehouse_center_id_fkey;
       public       postgres    false    3203    272    271            H           2606    28129 6   oscenter_oswarehouse oscenter_oswarehouse_farm_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.oscenter_oswarehouse
    ADD CONSTRAINT oscenter_oswarehouse_farm_id_fkey FOREIGN KEY (farm_id) REFERENCES public.osfarm(farm_id) ON UPDATE CASCADE ON DELETE CASCADE;
 `   ALTER TABLE ONLY public.oscenter_oswarehouse DROP CONSTRAINT oscenter_oswarehouse_farm_id_fkey;
       public       postgres    false    3215    272    273            I           2606    28134 =   oscenter_oswarehouse oscenter_oswarehouse_partnership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.oscenter_oswarehouse
    ADD CONSTRAINT oscenter_oswarehouse_partnership_id_fkey FOREIGN KEY (partnership_id) REFERENCES public.ospartnership(partnership_id) ON UPDATE CASCADE ON DELETE CASCADE;
 g   ALTER TABLE ONLY public.oscenter_oswarehouse DROP CONSTRAINT oscenter_oswarehouse_partnership_id_fkey;
       public       postgres    false    277    272    3229            J           2606    28139 ;   oscenter_oswarehouse oscenter_oswarehouse_warehouse_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.oscenter_oswarehouse
    ADD CONSTRAINT oscenter_oswarehouse_warehouse_id_fkey FOREIGN KEY (warehouse_id) REFERENCES public.oswarehouse(warehouse_id) ON UPDATE CASCADE ON DELETE CASCADE;
 e   ALTER TABLE ONLY public.oscenter_oswarehouse DROP CONSTRAINT oscenter_oswarehouse_warehouse_id_fkey;
       public       postgres    false    3257    286    272            F           2606    28144 %   oscenter oscenter_partnership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.oscenter
    ADD CONSTRAINT oscenter_partnership_id_fkey FOREIGN KEY (partnership_id) REFERENCES public.ospartnership(partnership_id) ON UPDATE CASCADE ON DELETE CASCADE;
 O   ALTER TABLE ONLY public.oscenter DROP CONSTRAINT oscenter_partnership_id_fkey;
       public       postgres    false    3229    277    271            K           2606    28149    osfarm osfarm_farm_type_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.osfarm
    ADD CONSTRAINT osfarm_farm_type_id_fkey FOREIGN KEY (farm_type_id) REFERENCES public.mdfarmtype(farm_type_id) ON UPDATE CASCADE ON DELETE CASCADE;
 I   ALTER TABLE ONLY public.osfarm DROP CONSTRAINT osfarm_farm_type_id_fkey;
       public       postgres    false    252    273    3161            L           2606    28154 !   osfarm osfarm_partnership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.osfarm
    ADD CONSTRAINT osfarm_partnership_id_fkey FOREIGN KEY (partnership_id) REFERENCES public.ospartnership(partnership_id) ON UPDATE CASCADE ON DELETE CASCADE;
 K   ALTER TABLE ONLY public.osfarm DROP CONSTRAINT osfarm_partnership_id_fkey;
       public       postgres    false    273    277    3229            M           2606    28159 /   osincubator osincubator_incubator_plant_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.osincubator
    ADD CONSTRAINT osincubator_incubator_plant_id_fkey FOREIGN KEY (incubator_plant_id) REFERENCES public.osincubatorplant(incubator_plant_id) ON UPDATE CASCADE ON DELETE CASCADE;
 Y   ALTER TABLE ONLY public.osincubator DROP CONSTRAINT osincubator_incubator_plant_id_fkey;
       public       postgres    false    3225    274    275            N           2606    28164 5   osincubatorplant osincubatorplant_partnership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.osincubatorplant
    ADD CONSTRAINT osincubatorplant_partnership_id_fkey FOREIGN KEY (partnership_id) REFERENCES public.ospartnership(partnership_id) ON UPDATE CASCADE ON DELETE CASCADE;
 _   ALTER TABLE ONLY public.osincubatorplant DROP CONSTRAINT osincubatorplant_partnership_id_fkey;
       public       postgres    false    277    275    3229            O           2606    28169    osshed osshed_center_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.osshed
    ADD CONSTRAINT osshed_center_id_fkey FOREIGN KEY (center_id) REFERENCES public.oscenter(center_id) ON UPDATE CASCADE ON DELETE CASCADE;
 F   ALTER TABLE ONLY public.osshed DROP CONSTRAINT osshed_center_id_fkey;
       public       postgres    false    271    279    3203            P           2606    28174    osshed osshed_farm_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.osshed
    ADD CONSTRAINT osshed_farm_id_fkey FOREIGN KEY (farm_id) REFERENCES public.osfarm(farm_id) ON UPDATE CASCADE ON DELETE CASCADE;
 D   ALTER TABLE ONLY public.osshed DROP CONSTRAINT osshed_farm_id_fkey;
       public       postgres    false    3215    273    279            Q           2606    28179 !   osshed osshed_partnership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.osshed
    ADD CONSTRAINT osshed_partnership_id_fkey FOREIGN KEY (partnership_id) REFERENCES public.ospartnership(partnership_id) ON UPDATE CASCADE ON DELETE CASCADE;
 K   ALTER TABLE ONLY public.osshed DROP CONSTRAINT osshed_partnership_id_fkey;
       public       postgres    false    277    279    3229            R           2606    28184     osshed osshed_statusshed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.osshed
    ADD CONSTRAINT osshed_statusshed_id_fkey FOREIGN KEY (statusshed_id) REFERENCES public.mdshedstatus(shed_status_id) ON UPDATE CASCADE ON DELETE CASCADE;
 J   ALTER TABLE ONLY public.osshed DROP CONSTRAINT osshed_statusshed_id_fkey;
       public       postgres    false    266    3188    279            S           2606    28189    ossilo ossilo_center_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ossilo
    ADD CONSTRAINT ossilo_center_id_fkey FOREIGN KEY (center_id) REFERENCES public.oscenter(center_id) ON UPDATE CASCADE ON DELETE CASCADE;
 F   ALTER TABLE ONLY public.ossilo DROP CONSTRAINT ossilo_center_id_fkey;
       public       postgres    false    3203    271    281            T           2606    28194    ossilo ossilo_farm_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ossilo
    ADD CONSTRAINT ossilo_farm_id_fkey FOREIGN KEY (farm_id) REFERENCES public.osfarm(farm_id) ON UPDATE CASCADE ON DELETE CASCADE;
 D   ALTER TABLE ONLY public.ossilo DROP CONSTRAINT ossilo_farm_id_fkey;
       public       postgres    false    3215    273    281            V           2606    28199 *   ossilo_osshed ossilo_osshed_center_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ossilo_osshed
    ADD CONSTRAINT ossilo_osshed_center_id_fkey FOREIGN KEY (center_id) REFERENCES public.oscenter(center_id) ON UPDATE CASCADE ON DELETE CASCADE;
 T   ALTER TABLE ONLY public.ossilo_osshed DROP CONSTRAINT ossilo_osshed_center_id_fkey;
       public       postgres    false    271    282    3203            W           2606    28204 (   ossilo_osshed ossilo_osshed_farm_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ossilo_osshed
    ADD CONSTRAINT ossilo_osshed_farm_id_fkey FOREIGN KEY (farm_id) REFERENCES public.osfarm(farm_id) ON UPDATE CASCADE ON DELETE CASCADE;
 R   ALTER TABLE ONLY public.ossilo_osshed DROP CONSTRAINT ossilo_osshed_farm_id_fkey;
       public       postgres    false    273    282    3215            X           2606    28209 /   ossilo_osshed ossilo_osshed_partnership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ossilo_osshed
    ADD CONSTRAINT ossilo_osshed_partnership_id_fkey FOREIGN KEY (partnership_id) REFERENCES public.ospartnership(partnership_id) ON UPDATE CASCADE ON DELETE CASCADE;
 Y   ALTER TABLE ONLY public.ossilo_osshed DROP CONSTRAINT ossilo_osshed_partnership_id_fkey;
       public       postgres    false    277    282    3229            Y           2606    28214 (   ossilo_osshed ossilo_osshed_shed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ossilo_osshed
    ADD CONSTRAINT ossilo_osshed_shed_id_fkey FOREIGN KEY (shed_id) REFERENCES public.osshed(shed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 R   ALTER TABLE ONLY public.ossilo_osshed DROP CONSTRAINT ossilo_osshed_shed_id_fkey;
       public       postgres    false    279    3235    282            Z           2606    28219 (   ossilo_osshed ossilo_osshed_silo_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ossilo_osshed
    ADD CONSTRAINT ossilo_osshed_silo_id_fkey FOREIGN KEY (silo_id) REFERENCES public.ossilo(silo_id) ON UPDATE CASCADE ON DELETE CASCADE;
 R   ALTER TABLE ONLY public.ossilo_osshed DROP CONSTRAINT ossilo_osshed_silo_id_fkey;
       public       postgres    false    281    3242    282            U           2606    28224 !   ossilo ossilo_partnership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ossilo
    ADD CONSTRAINT ossilo_partnership_id_fkey FOREIGN KEY (partnership_id) REFERENCES public.ospartnership(partnership_id) ON UPDATE CASCADE ON DELETE CASCADE;
 K   ALTER TABLE ONLY public.ossilo DROP CONSTRAINT ossilo_partnership_id_fkey;
       public       postgres    false    3229    281    277            [           2606    28229 $   oswarehouse oswarehouse_farm_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.oswarehouse
    ADD CONSTRAINT oswarehouse_farm_id_fkey FOREIGN KEY (farm_id) REFERENCES public.osfarm(farm_id) ON UPDATE CASCADE ON DELETE CASCADE;
 N   ALTER TABLE ONLY public.oswarehouse DROP CONSTRAINT oswarehouse_farm_id_fkey;
       public       postgres    false    273    286    3215            \           2606    28234 +   oswarehouse oswarehouse_partnership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.oswarehouse
    ADD CONSTRAINT oswarehouse_partnership_id_fkey FOREIGN KEY (partnership_id) REFERENCES public.ospartnership(partnership_id) ON UPDATE CASCADE ON DELETE CASCADE;
 U   ALTER TABLE ONLY public.oswarehouse DROP CONSTRAINT oswarehouse_partnership_id_fkey;
       public       postgres    false    286    277    3229            ]           2606    28239 5   txavailabilitysheds txavailabilitysheds_lot_code_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txavailabilitysheds
    ADD CONSTRAINT txavailabilitysheds_lot_code_fkey FOREIGN KEY (lot_code) REFERENCES public.txlot(lot_code) ON UPDATE CASCADE ON DELETE CASCADE;
 _   ALTER TABLE ONLY public.txavailabilitysheds DROP CONSTRAINT txavailabilitysheds_lot_code_fkey;
       public       postgres    false    315    3337    297            ^           2606    28244 4   txavailabilitysheds txavailabilitysheds_shed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txavailabilitysheds
    ADD CONSTRAINT txavailabilitysheds_shed_id_fkey FOREIGN KEY (shed_id) REFERENCES public.osshed(shed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 ^   ALTER TABLE ONLY public.txavailabilitysheds DROP CONSTRAINT txavailabilitysheds_shed_id_fkey;
       public       postgres    false    279    3235    297            _           2606    28249 !   txbroiler txbroiler_breed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroiler
    ADD CONSTRAINT txbroiler_breed_id_fkey FOREIGN KEY (breed_id) REFERENCES public.mdbreed(breed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 K   ALTER TABLE ONLY public.txbroiler DROP CONSTRAINT txbroiler_breed_id_fkey;
       public       postgres    false    3157    298    250            c           2606    28254 1   txbroiler_detail txbroiler_detail_broiler_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroiler_detail
    ADD CONSTRAINT txbroiler_detail_broiler_id_fkey FOREIGN KEY (broiler_id) REFERENCES public.txbroiler(broiler_id) ON UPDATE CASCADE ON DELETE CASCADE;
 [   ALTER TABLE ONLY public.txbroiler_detail DROP CONSTRAINT txbroiler_detail_broiler_id_fkey;
       public       postgres    false    298    3267    299            d           2606    28259 0   txbroiler_detail txbroiler_detail_center_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroiler_detail
    ADD CONSTRAINT txbroiler_detail_center_id_fkey FOREIGN KEY (center_id) REFERENCES public.oscenter(center_id);
 Z   ALTER TABLE ONLY public.txbroiler_detail DROP CONSTRAINT txbroiler_detail_center_id_fkey;
       public       postgres    false    271    3203    299            e           2606    28264 9   txbroiler_detail txbroiler_detail_executioncenter_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroiler_detail
    ADD CONSTRAINT txbroiler_detail_executioncenter_id_fkey FOREIGN KEY (executioncenter_id) REFERENCES public.oscenter(center_id);
 c   ALTER TABLE ONLY public.txbroiler_detail DROP CONSTRAINT txbroiler_detail_executioncenter_id_fkey;
       public       postgres    false    271    3203    299            f           2606    28269 7   txbroiler_detail txbroiler_detail_executionfarm_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroiler_detail
    ADD CONSTRAINT txbroiler_detail_executionfarm_id_fkey FOREIGN KEY (executionfarm_id) REFERENCES public.osfarm(farm_id);
 a   ALTER TABLE ONLY public.txbroiler_detail DROP CONSTRAINT txbroiler_detail_executionfarm_id_fkey;
       public       postgres    false    3215    273    299            g           2606    28274 7   txbroiler_detail txbroiler_detail_executionshed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroiler_detail
    ADD CONSTRAINT txbroiler_detail_executionshed_id_fkey FOREIGN KEY (executionshed_id) REFERENCES public.osshed(shed_id);
 a   ALTER TABLE ONLY public.txbroiler_detail DROP CONSTRAINT txbroiler_detail_executionshed_id_fkey;
       public       postgres    false    279    3235    299            h           2606    28279 .   txbroiler_detail txbroiler_detail_farm_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroiler_detail
    ADD CONSTRAINT txbroiler_detail_farm_id_fkey FOREIGN KEY (farm_id) REFERENCES public.osfarm(farm_id) ON UPDATE CASCADE ON DELETE CASCADE;
 X   ALTER TABLE ONLY public.txbroiler_detail DROP CONSTRAINT txbroiler_detail_farm_id_fkey;
       public       postgres    false    3215    299    273            i           2606    28284 .   txbroiler_detail txbroiler_detail_shed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroiler_detail
    ADD CONSTRAINT txbroiler_detail_shed_id_fkey FOREIGN KEY (shed_id) REFERENCES public.osshed(shed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 X   ALTER TABLE ONLY public.txbroiler_detail DROP CONSTRAINT txbroiler_detail_shed_id_fkey;
       public       postgres    false    279    3235    299            `           2606    28289 '   txbroiler txbroiler_partnership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroiler
    ADD CONSTRAINT txbroiler_partnership_id_fkey FOREIGN KEY (partnership_id) REFERENCES public.ospartnership(partnership_id) ON UPDATE CASCADE ON DELETE CASCADE;
 Q   ALTER TABLE ONLY public.txbroiler DROP CONSTRAINT txbroiler_partnership_id_fkey;
       public       postgres    false    277    3229    298            a           2606    28294 +   txbroiler txbroiler_programmed_eggs_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroiler
    ADD CONSTRAINT txbroiler_programmed_eggs_id_fkey FOREIGN KEY (programmed_eggs_id) REFERENCES public.txprogrammed_eggs(programmed_eggs_id) ON UPDATE CASCADE ON DELETE CASCADE;
 U   ALTER TABLE ONLY public.txbroiler DROP CONSTRAINT txbroiler_programmed_eggs_id_fkey;
       public       postgres    false    3349    298    318            b           2606    28299 $   txbroiler txbroiler_scenario_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroiler
    ADD CONSTRAINT txbroiler_scenario_id_fkey FOREIGN KEY (scenario_id) REFERENCES public.mdscenario(scenario_id) ON UPDATE CASCADE ON DELETE CASCADE;
 N   ALTER TABLE ONLY public.txbroiler DROP CONSTRAINT txbroiler_scenario_id_fkey;
       public       postgres    false    264    3186    298            j           2606    28304 1   txbroilereviction txbroilereviction_breed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroilereviction
    ADD CONSTRAINT txbroilereviction_breed_id_fkey FOREIGN KEY (breed_id) REFERENCES public.mdbreed(breed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 [   ALTER TABLE ONLY public.txbroilereviction DROP CONSTRAINT txbroilereviction_breed_id_fkey;
       public       postgres    false    250    3157    300            m           2606    28309 A   txbroilereviction_detail txbroilereviction_detail_broiler_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroilereviction_detail
    ADD CONSTRAINT txbroilereviction_detail_broiler_id_fkey FOREIGN KEY (broilereviction_id) REFERENCES public.txbroilereviction(broilereviction_id) ON UPDATE CASCADE ON DELETE CASCADE;
 k   ALTER TABLE ONLY public.txbroilereviction_detail DROP CONSTRAINT txbroilereviction_detail_broiler_id_fkey;
       public       postgres    false    300    3277    301            n           2606    28314 I   txbroilereviction_detail txbroilereviction_detail_broiler_product_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroilereviction_detail
    ADD CONSTRAINT txbroilereviction_detail_broiler_product_id_fkey FOREIGN KEY (broiler_product_id) REFERENCES public.mdbroiler_product(broiler_product_id) ON UPDATE CASCADE ON DELETE CASCADE;
 s   ALTER TABLE ONLY public.txbroilereviction_detail DROP CONSTRAINT txbroilereviction_detail_broiler_product_id_fkey;
       public       postgres    false    251    3159    301            o           2606    28319 @   txbroilereviction_detail txbroilereviction_detail_center_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroilereviction_detail
    ADD CONSTRAINT txbroilereviction_detail_center_id_fkey FOREIGN KEY (center_id) REFERENCES public.oscenter(center_id);
 j   ALTER TABLE ONLY public.txbroilereviction_detail DROP CONSTRAINT txbroilereviction_detail_center_id_fkey;
       public       postgres    false    271    3203    301            s           2606    28605 M   txbroilereviction_detail txbroilereviction_detail_execution_slaughterhouse_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroilereviction_detail
    ADD CONSTRAINT txbroilereviction_detail_execution_slaughterhouse_id FOREIGN KEY (executionslaughterhouse_id) REFERENCES public.osslaughterhouse(slaughterhouse_id);
 w   ALTER TABLE ONLY public.txbroilereviction_detail DROP CONSTRAINT txbroilereviction_detail_execution_slaughterhouse_id;
       public       postgres    false    284    301    3251            p           2606    28339 >   txbroilereviction_detail txbroilereviction_detail_farm_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroilereviction_detail
    ADD CONSTRAINT txbroilereviction_detail_farm_id_fkey FOREIGN KEY (farm_id) REFERENCES public.osfarm(farm_id) ON UPDATE CASCADE ON DELETE CASCADE;
 h   ALTER TABLE ONLY public.txbroilereviction_detail DROP CONSTRAINT txbroilereviction_detail_farm_id_fkey;
       public       postgres    false    273    301    3215            q           2606    28344 >   txbroilereviction_detail txbroilereviction_detail_shed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroilereviction_detail
    ADD CONSTRAINT txbroilereviction_detail_shed_id_fkey FOREIGN KEY (shed_id) REFERENCES public.osshed(shed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 h   ALTER TABLE ONLY public.txbroilereviction_detail DROP CONSTRAINT txbroilereviction_detail_shed_id_fkey;
       public       postgres    false    279    301    3235            r           2606    28349 H   txbroilereviction_detail txbroilereviction_detail_slaughterhouse_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroilereviction_detail
    ADD CONSTRAINT txbroilereviction_detail_slaughterhouse_id_fkey FOREIGN KEY (slaughterhouse_id) REFERENCES public.osslaughterhouse(slaughterhouse_id) ON UPDATE CASCADE ON DELETE CASCADE;
 r   ALTER TABLE ONLY public.txbroilereviction_detail DROP CONSTRAINT txbroilereviction_detail_slaughterhouse_id_fkey;
       public       postgres    false    284    3251    301            k           2606    28354 7   txbroilereviction txbroilereviction_partnership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroilereviction
    ADD CONSTRAINT txbroilereviction_partnership_id_fkey FOREIGN KEY (partnership_id) REFERENCES public.ospartnership(partnership_id) ON UPDATE CASCADE ON DELETE CASCADE;
 a   ALTER TABLE ONLY public.txbroilereviction DROP CONSTRAINT txbroilereviction_partnership_id_fkey;
       public       postgres    false    277    3229    300            l           2606    28359 4   txbroilereviction txbroilereviction_scenario_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroilereviction
    ADD CONSTRAINT txbroilereviction_scenario_id_fkey FOREIGN KEY (scenario_id) REFERENCES public.mdscenario(scenario_id) ON UPDATE CASCADE ON DELETE CASCADE;
 ^   ALTER TABLE ONLY public.txbroilereviction DROP CONSTRAINT txbroilereviction_scenario_id_fkey;
       public       postgres    false    264    3186    300            t           2606    28364 C   txbroilerproduct_detail txbroilerproduct_detail_broiler_detail_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroilerproduct_detail
    ADD CONSTRAINT txbroilerproduct_detail_broiler_detail_fkey FOREIGN KEY (broiler_detail) REFERENCES public.txbroiler_detail(broiler_detail_id) ON UPDATE CASCADE ON DELETE CASCADE;
 m   ALTER TABLE ONLY public.txbroilerproduct_detail DROP CONSTRAINT txbroilerproduct_detail_broiler_detail_fkey;
       public       postgres    false    299    3272    302            u           2606    28369 .   txbroodermachine txbroodermachine_farm_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroodermachine
    ADD CONSTRAINT txbroodermachine_farm_id_fkey FOREIGN KEY (farm_id) REFERENCES public.osfarm(farm_id) ON UPDATE CASCADE ON DELETE CASCADE;
 X   ALTER TABLE ONLY public.txbroodermachine DROP CONSTRAINT txbroodermachine_farm_id_fkey;
       public       postgres    false    273    3215    303            v           2606    28374 5   txbroodermachine txbroodermachine_partnership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txbroodermachine
    ADD CONSTRAINT txbroodermachine_partnership_id_fkey FOREIGN KEY (partnership_id) REFERENCES public.ospartnership(partnership_id) ON UPDATE CASCADE ON DELETE CASCADE;
 _   ALTER TABLE ONLY public.txbroodermachine DROP CONSTRAINT txbroodermachine_partnership_id_fkey;
       public       postgres    false    277    3229    303            w           2606    28379 ,   txcalendarday txcalendarday_calendar_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txcalendarday
    ADD CONSTRAINT txcalendarday_calendar_id_fkey FOREIGN KEY (calendar_id) REFERENCES public.txcalendar(calendar_id) ON UPDATE CASCADE ON DELETE CASCADE;
 V   ALTER TABLE ONLY public.txcalendarday DROP CONSTRAINT txcalendarday_calendar_id_fkey;
       public       postgres    false    304    3294    305            y           2606    28384 -   txeggs_planning txeggs_planning_breed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txeggs_planning
    ADD CONSTRAINT txeggs_planning_breed_id_fkey FOREIGN KEY (breed_id) REFERENCES public.mdbreed(breed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 W   ALTER TABLE ONLY public.txeggs_planning DROP CONSTRAINT txeggs_planning_breed_id_fkey;
       public       postgres    false    250    3157    308            z           2606    28389 0   txeggs_planning txeggs_planning_scenario_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txeggs_planning
    ADD CONSTRAINT txeggs_planning_scenario_id_fkey FOREIGN KEY (scenario_id) REFERENCES public.mdscenario(scenario_id);
 Z   ALTER TABLE ONLY public.txeggs_planning DROP CONSTRAINT txeggs_planning_scenario_id_fkey;
       public       postgres    false    264    3186    308            {           2606    28394 -   txeggs_required txeggs_required_breed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txeggs_required
    ADD CONSTRAINT txeggs_required_breed_id_fkey FOREIGN KEY (breed_id) REFERENCES public.mdbreed(breed_id);
 W   ALTER TABLE ONLY public.txeggs_required DROP CONSTRAINT txeggs_required_breed_id_fkey;
       public       postgres    false    250    309    3157            |           2606    28399 0   txeggs_required txeggs_required_scenario_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txeggs_required
    ADD CONSTRAINT txeggs_required_scenario_id_fkey FOREIGN KEY (scenario_id) REFERENCES public.mdscenario(scenario_id);
 Z   ALTER TABLE ONLY public.txeggs_required DROP CONSTRAINT txeggs_required_scenario_id_fkey;
       public       postgres    false    264    309    3186            }           2606    28404 +   txeggs_storage txeggs_storage_breed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txeggs_storage
    ADD CONSTRAINT txeggs_storage_breed_id_fkey FOREIGN KEY (breed_id) REFERENCES public.mdbreed(breed_id);
 U   ALTER TABLE ONLY public.txeggs_storage DROP CONSTRAINT txeggs_storage_breed_id_fkey;
       public       postgres    false    310    3157    250            ~           2606    28409 5   txeggs_storage txeggs_storage_incubator_plant_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txeggs_storage
    ADD CONSTRAINT txeggs_storage_incubator_plant_id_fkey FOREIGN KEY (incubator_plant_id) REFERENCES public.osincubatorplant(incubator_plant_id);
 _   ALTER TABLE ONLY public.txeggs_storage DROP CONSTRAINT txeggs_storage_incubator_plant_id_fkey;
       public       postgres    false    310    3225    275                       2606    28414 .   txeggs_storage txeggs_storage_scenario_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txeggs_storage
    ADD CONSTRAINT txeggs_storage_scenario_id_fkey FOREIGN KEY (scenario_id) REFERENCES public.mdscenario(scenario_id);
 X   ALTER TABLE ONLY public.txeggs_storage DROP CONSTRAINT txeggs_storage_scenario_id_fkey;
       public       postgres    false    264    310    3186            �           2606    28419 '   txgoals_erp txgoals_erp_product_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txgoals_erp
    ADD CONSTRAINT txgoals_erp_product_id_fkey FOREIGN KEY (product_id) REFERENCES public.mdproduct(product_id);
 Q   ALTER TABLE ONLY public.txgoals_erp DROP CONSTRAINT txgoals_erp_product_id_fkey;
       public       postgres    false    260    3177    311            �           2606    28424 (   txgoals_erp txgoals_erp_scenario_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txgoals_erp
    ADD CONSTRAINT txgoals_erp_scenario_id_fkey FOREIGN KEY (scenario_id) REFERENCES public.mdscenario(scenario_id);
 R   ALTER TABLE ONLY public.txgoals_erp DROP CONSTRAINT txgoals_erp_scenario_id_fkey;
       public       postgres    false    3186    311    264            �           2606    28429 '   txhousingway txhousingway_breed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txhousingway
    ADD CONSTRAINT txhousingway_breed_id_fkey FOREIGN KEY (breed_id) REFERENCES public.mdbreed(breed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 Q   ALTER TABLE ONLY public.txhousingway DROP CONSTRAINT txhousingway_breed_id_fkey;
       public       postgres    false    313    250    3157            �           2606    28434 6   txhousingway_detail txhousingway_detail_center_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txhousingway_detail
    ADD CONSTRAINT txhousingway_detail_center_id_fkey FOREIGN KEY (center_id) REFERENCES public.oscenter(center_id);
 `   ALTER TABLE ONLY public.txhousingway_detail DROP CONSTRAINT txhousingway_detail_center_id_fkey;
       public       postgres    false    314    3203    271            �           2606    28439 @   txhousingway_detail txhousingway_detail_execution_center_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txhousingway_detail
    ADD CONSTRAINT txhousingway_detail_execution_center_id_fkey FOREIGN KEY (executioncenter_id) REFERENCES public.oscenter(center_id);
 j   ALTER TABLE ONLY public.txhousingway_detail DROP CONSTRAINT txhousingway_detail_execution_center_id_fkey;
       public       postgres    false    314    271    3203            �           2606    28444 >   txhousingway_detail txhousingway_detail_execution_farm_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txhousingway_detail
    ADD CONSTRAINT txhousingway_detail_execution_farm_id_fkey FOREIGN KEY (executionfarm_id) REFERENCES public.osfarm(farm_id);
 h   ALTER TABLE ONLY public.txhousingway_detail DROP CONSTRAINT txhousingway_detail_execution_farm_id_fkey;
       public       postgres    false    314    273    3215            �           2606    28449 >   txhousingway_detail txhousingway_detail_execution_shed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txhousingway_detail
    ADD CONSTRAINT txhousingway_detail_execution_shed_id_fkey FOREIGN KEY (executionshed_id) REFERENCES public.osshed(shed_id);
 h   ALTER TABLE ONLY public.txhousingway_detail DROP CONSTRAINT txhousingway_detail_execution_shed_id_fkey;
       public       postgres    false    314    3235    279            �           2606    28454 4   txhousingway_detail txhousingway_detail_farm_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txhousingway_detail
    ADD CONSTRAINT txhousingway_detail_farm_id_fkey FOREIGN KEY (farm_id) REFERENCES public.osfarm(farm_id) ON UPDATE CASCADE ON DELETE CASCADE;
 ^   ALTER TABLE ONLY public.txhousingway_detail DROP CONSTRAINT txhousingway_detail_farm_id_fkey;
       public       postgres    false    3215    314    273            �           2606    28459 ;   txhousingway_detail txhousingway_detail_housing_way_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txhousingway_detail
    ADD CONSTRAINT txhousingway_detail_housing_way_id_fkey FOREIGN KEY (housing_way_id) REFERENCES public.txhousingway(housing_way_id) ON UPDATE CASCADE ON DELETE CASCADE;
 e   ALTER TABLE ONLY public.txhousingway_detail DROP CONSTRAINT txhousingway_detail_housing_way_id_fkey;
       public       postgres    false    314    313    3324            �           2606    28464 4   txhousingway_detail txhousingway_detail_shed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txhousingway_detail
    ADD CONSTRAINT txhousingway_detail_shed_id_fkey FOREIGN KEY (shed_id) REFERENCES public.osshed(shed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 ^   ALTER TABLE ONLY public.txhousingway_detail DROP CONSTRAINT txhousingway_detail_shed_id_fkey;
       public       postgres    false    3235    314    279            �           2606    28469 -   txhousingway txhousingway_partnership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txhousingway
    ADD CONSTRAINT txhousingway_partnership_id_fkey FOREIGN KEY (partnership_id) REFERENCES public.ospartnership(partnership_id) ON UPDATE CASCADE ON DELETE CASCADE;
 W   ALTER TABLE ONLY public.txhousingway DROP CONSTRAINT txhousingway_partnership_id_fkey;
       public       postgres    false    313    277    3229            �           2606    28474 *   txhousingway txhousingway_scenario_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txhousingway
    ADD CONSTRAINT txhousingway_scenario_id_fkey FOREIGN KEY (scenario_id) REFERENCES public.mdscenario(scenario_id) ON UPDATE CASCADE ON DELETE CASCADE;
 T   ALTER TABLE ONLY public.txhousingway DROP CONSTRAINT txhousingway_scenario_id_fkey;
       public       postgres    false    3186    313    264            �           2606    28479 '   txhousingway txhousingway_stage_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txhousingway
    ADD CONSTRAINT txhousingway_stage_id_fkey FOREIGN KEY (stage_id) REFERENCES public.mdstage(stage_id);
 Q   ALTER TABLE ONLY public.txhousingway DROP CONSTRAINT txhousingway_stage_id_fkey;
       public       postgres    false    313    268    3192            �           2606    28484    txlot txlot_breed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txlot
    ADD CONSTRAINT txlot_breed_id_fkey FOREIGN KEY (breed_id) REFERENCES public.mdbreed(breed_id);
 C   ALTER TABLE ONLY public.txlot DROP CONSTRAINT txlot_breed_id_fkey;
       public       postgres    false    250    315    3157            �           2606    28489    txlot txlot_farm_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txlot
    ADD CONSTRAINT txlot_farm_id_fkey FOREIGN KEY (farm_id) REFERENCES public.osfarm(farm_id) ON UPDATE CASCADE ON DELETE CASCADE;
 B   ALTER TABLE ONLY public.txlot DROP CONSTRAINT txlot_farm_id_fkey;
       public       postgres    false    315    273    3215            �           2606    28494    txlot txlot_housing_way_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txlot
    ADD CONSTRAINT txlot_housing_way_id_fkey FOREIGN KEY (housing_way_id) REFERENCES public.txhousingway(housing_way_id) ON UPDATE CASCADE ON DELETE CASCADE;
 I   ALTER TABLE ONLY public.txlot DROP CONSTRAINT txlot_housing_way_id_fkey;
       public       postgres    false    315    3324    313            �           2606    28499    txlot txlot_product_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txlot
    ADD CONSTRAINT txlot_product_id_fkey FOREIGN KEY (product_id) REFERENCES public.mdproduct(product_id);
 E   ALTER TABLE ONLY public.txlot DROP CONSTRAINT txlot_product_id_fkey;
       public       postgres    false    3177    315    260            �           2606    28504    txlot txlot_shed_id_fkey    FK CONSTRAINT     }   ALTER TABLE ONLY public.txlot
    ADD CONSTRAINT txlot_shed_id_fkey FOREIGN KEY (shed_id) REFERENCES public.osshed(shed_id);
 B   ALTER TABLE ONLY public.txlot DROP CONSTRAINT txlot_shed_id_fkey;
       public       postgres    false    315    279    3235            �           2606    28509 +   txposturecurve txposturecurve_breed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txposturecurve
    ADD CONSTRAINT txposturecurve_breed_id_fkey FOREIGN KEY (breed_id) REFERENCES public.mdbreed(breed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 U   ALTER TABLE ONLY public.txposturecurve DROP CONSTRAINT txposturecurve_breed_id_fkey;
       public       postgres    false    3157    317    250            �           2606    28514 1   txprogrammed_eggs txprogrammed_eggs_breed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txprogrammed_eggs
    ADD CONSTRAINT txprogrammed_eggs_breed_id_fkey FOREIGN KEY (breed_id) REFERENCES public.mdbreed(breed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 [   ALTER TABLE ONLY public.txprogrammed_eggs DROP CONSTRAINT txprogrammed_eggs_breed_id_fkey;
       public       postgres    false    318    250    3157            �           2606    28519 5   txprogrammed_eggs txprogrammed_eggs_eggs_movements_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.txprogrammed_eggs
    ADD CONSTRAINT txprogrammed_eggs_eggs_movements_id FOREIGN KEY (eggs_movements_id) REFERENCES public.txeggs_movements(eggs_movements_id);
 _   ALTER TABLE ONLY public.txprogrammed_eggs DROP CONSTRAINT txprogrammed_eggs_eggs_movements_id;
       public       postgres    false    3301    318    307            �           2606    28524 8   txprogrammed_eggs txprogrammed_eggs_eggs_storage_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txprogrammed_eggs
    ADD CONSTRAINT txprogrammed_eggs_eggs_storage_id_fkey FOREIGN KEY (eggs_storage_id) REFERENCES public.txeggs_storage(eggs_storage_id) ON UPDATE CASCADE ON DELETE CASCADE;
 b   ALTER TABLE ONLY public.txprogrammed_eggs DROP CONSTRAINT txprogrammed_eggs_eggs_storage_id_fkey;
       public       postgres    false    318    3314    310            �           2606    28529 5   txprogrammed_eggs txprogrammed_eggs_incubator_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txprogrammed_eggs
    ADD CONSTRAINT txprogrammed_eggs_incubator_id_fkey FOREIGN KEY (incubator_id) REFERENCES public.osincubator(incubator_id) ON UPDATE CASCADE ON DELETE CASCADE;
 _   ALTER TABLE ONLY public.txprogrammed_eggs DROP CONSTRAINT txprogrammed_eggs_incubator_id_fkey;
       public       postgres    false    274    3220    318            �           2606    28534 3   txscenarioformula txscenarioformula_measure_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioformula
    ADD CONSTRAINT txscenarioformula_measure_id_fkey FOREIGN KEY (measure_id) REFERENCES public.mdmeasure(measure_id) ON UPDATE CASCADE ON DELETE CASCADE;
 ]   ALTER TABLE ONLY public.txscenarioformula DROP CONSTRAINT txscenarioformula_measure_id_fkey;
       public       postgres    false    3163    254    319            �           2606    28539 5   txscenarioformula txscenarioformula_parameter_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioformula
    ADD CONSTRAINT txscenarioformula_parameter_id_fkey FOREIGN KEY (parameter_id) REFERENCES public.mdparameter(parameter_id) ON UPDATE CASCADE ON DELETE CASCADE;
 _   ALTER TABLE ONLY public.txscenarioformula DROP CONSTRAINT txscenarioformula_parameter_id_fkey;
       public       postgres    false    256    319    3167            �           2606    28544 3   txscenarioformula txscenarioformula_process_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioformula
    ADD CONSTRAINT txscenarioformula_process_id_fkey FOREIGN KEY (process_id) REFERENCES public.mdprocess(process_id) ON UPDATE CASCADE;
 ]   ALTER TABLE ONLY public.txscenarioformula DROP CONSTRAINT txscenarioformula_process_id_fkey;
       public       postgres    false    319    3173    258            �           2606    28549 4   txscenarioformula txscenarioformula_scenario_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioformula
    ADD CONSTRAINT txscenarioformula_scenario_id_fkey FOREIGN KEY (scenario_id) REFERENCES public.mdscenario(scenario_id) ON UPDATE CASCADE ON DELETE CASCADE;
 ^   ALTER TABLE ONLY public.txscenarioformula DROP CONSTRAINT txscenarioformula_scenario_id_fkey;
       public       postgres    false    3186    319    264            �           2606    28554 9   txscenarioparameter txscenarioparameter_parameter_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioparameter
    ADD CONSTRAINT txscenarioparameter_parameter_id_fkey FOREIGN KEY (parameter_id) REFERENCES public.mdparameter(parameter_id) ON UPDATE CASCADE ON DELETE CASCADE;
 c   ALTER TABLE ONLY public.txscenarioparameter DROP CONSTRAINT txscenarioparameter_parameter_id_fkey;
       public       postgres    false    320    256    3167            �           2606    28559 7   txscenarioparameter txscenarioparameter_process_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioparameter
    ADD CONSTRAINT txscenarioparameter_process_id_fkey FOREIGN KEY (process_id) REFERENCES public.mdprocess(process_id) ON UPDATE CASCADE ON DELETE CASCADE;
 a   ALTER TABLE ONLY public.txscenarioparameter DROP CONSTRAINT txscenarioparameter_process_id_fkey;
       public       postgres    false    3173    320    258            �           2606    28564 8   txscenarioparameter txscenarioparameter_scenario_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioparameter
    ADD CONSTRAINT txscenarioparameter_scenario_id_fkey FOREIGN KEY (scenario_id) REFERENCES public.mdscenario(scenario_id) ON UPDATE CASCADE ON DELETE CASCADE;
 b   ALTER TABLE ONLY public.txscenarioparameter DROP CONSTRAINT txscenarioparameter_scenario_id_fkey;
       public       postgres    false    320    264    3186            �           2606    28569 ?   txscenarioparameterday txscenarioparameterday_parameter_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioparameterday
    ADD CONSTRAINT txscenarioparameterday_parameter_id_fkey FOREIGN KEY (parameter_id) REFERENCES public.mdparameter(parameter_id);
 i   ALTER TABLE ONLY public.txscenarioparameterday DROP CONSTRAINT txscenarioparameterday_parameter_id_fkey;
       public       postgres    false    3167    321    256            �           2606    28574 >   txscenarioparameterday txscenarioparameterday_scenario_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioparameterday
    ADD CONSTRAINT txscenarioparameterday_scenario_id_fkey FOREIGN KEY (scenario_id) REFERENCES public.mdscenario(scenario_id);
 h   ALTER TABLE ONLY public.txscenarioparameterday DROP CONSTRAINT txscenarioparameterday_scenario_id_fkey;
       public       postgres    false    321    264    3186            �           2606    28579 ;   txscenarioposturecurve txscenarioposturecurve_breed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioposturecurve
    ADD CONSTRAINT txscenarioposturecurve_breed_id_fkey FOREIGN KEY (breed_id) REFERENCES public.mdbreed(breed_id) ON UPDATE CASCADE ON DELETE CASCADE;
 e   ALTER TABLE ONLY public.txscenarioposturecurve DROP CONSTRAINT txscenarioposturecurve_breed_id_fkey;
       public       postgres    false    3157    322    250            �           2606    28584 G   txscenarioposturecurve txscenarioposturecurve_housingway_detail_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioposturecurve
    ADD CONSTRAINT txscenarioposturecurve_housingway_detail_id_fkey FOREIGN KEY (housingway_detail_id) REFERENCES public.txhousingway_detail(housingway_detail_id) ON UPDATE CASCADE ON DELETE CASCADE;
 q   ALTER TABLE ONLY public.txscenarioposturecurve DROP CONSTRAINT txscenarioposturecurve_housingway_detail_id_fkey;
       public       postgres    false    322    314    3330            �           2606    28589 >   txscenarioposturecurve txscenarioposturecurve_scenario_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioposturecurve
    ADD CONSTRAINT txscenarioposturecurve_scenario_id_fkey FOREIGN KEY (scenario_id) REFERENCES public.mdscenario(scenario_id) ON UPDATE CASCADE ON DELETE CASCADE;
 h   ALTER TABLE ONLY public.txscenarioposturecurve DROP CONSTRAINT txscenarioposturecurve_scenario_id_fkey;
       public       postgres    false    264    322    3186            �           2606    28594 3   txscenarioprocess txscenarioprocess_process_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioprocess
    ADD CONSTRAINT txscenarioprocess_process_id_fkey FOREIGN KEY (process_id) REFERENCES public.mdprocess(process_id) ON UPDATE CASCADE ON DELETE CASCADE;
 ]   ALTER TABLE ONLY public.txscenarioprocess DROP CONSTRAINT txscenarioprocess_process_id_fkey;
       public       postgres    false    323    258    3173            �           2606    28599 4   txscenarioprocess txscenarioprocess_scenario_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.txscenarioprocess
    ADD CONSTRAINT txscenarioprocess_scenario_id_fkey FOREIGN KEY (scenario_id) REFERENCES public.mdscenario(scenario_id) ON UPDATE CASCADE ON DELETE CASCADE;
 ^   ALTER TABLE ONLY public.txscenarioprocess DROP CONSTRAINT txscenarioprocess_scenario_id_fkey;
       public       postgres    false    264    323    3186                   x������ � �      "      x������ � �      $      x������ � �      &      x������ � �      (      x������ � �      *   4   x�3�4�TUHIU((�/I��K�2�4��NN��2�4�H%&�f��r��qqq '[�      ,      x������ � �      .      x������ � �      0      x������ � �      1   "   x�3�N�M�K�R�\��.��&��b�=... ��
�      Q   �   x�}��j1���d��^z�a!�܋�U6[
���>}��²���f4�t��ĳ��7D���0�J�60 �kﴪ��t4*<a趎*~�Z]� b"�X�h�l�R<;������{�O�͈��P�g��BF�$�����H�Vq��Ά頥6�}���G��_Ƴc�DVn�'`��^�M�
���(8R�G�>�2_�8����v��4|�����>����      S   E   x���	�@�����K��#�y�FE��$m�����i�n�=����ǵ��UPr�.T�{"�R�B      T   ,   x�34�200��/.�24q�8�󓒸�AcN�=... ��C      U     x�e�Kn�0��)|A"����AЍ��&H���K*m�NC��33}<��m,��~��E
��5�p��c9��@�-�v������j����A{���`Փ	�����'�	@-||h��}���D�W���k2,�ͤ"���\�ۅ��`�5��њB ���٠�sߞ#h�9���0����b~�u�+{f6��a�4�����&�GF2�8�u����d�C�ϟ!�W����F"�^��2�L镓j��/��)���      V   9   x�3�J-(�O)MN�<�9�ˈ�5/=�(%�˘�'�,1�$U�R�����D�=... �6>      X   !   x�3�,�/rIMKM.����4��=... m%�      Z   �   x���A�0E��)��
��İ$�u3@55նx+O��Lm�����$�JiXkk����T��B��v��;YJM6�d����y��~B��O��(�1�C����$�G=��,���Yx1��Fۑd]5���UQf��O�WQ��i/cP]/a�l��j򑤭R�/ �	9���Q2�\FyJ��VO*��l���N(�7S��%      \   [  x��S�N�0<O��/�l���� ��̥M���׏�	�����$E�_��5;3s�r42�);x]��	��p�uw��xK�n��p^6�����X<���[��	^/Ȳ��{���+�|���v��ҖbMsm�td���~�n�3ZGS�dUD4���Hi�ȡ/���\3ǪF��W`��D�Hɩ�`b9?�
D����<)}S�*r��V����O*A�z�T���!���P��"�IcK,�H�Lٓx9�����@H^Q�	��>?V���y*�u�yv�6���8�J�g0�*��JW��J�Xi"J��9�8�
��_S��bV�S	b�	ꫩ����!n      ^   v   x�-̻� ��>�O`��Rf�a$�N&n&����p`�z��j���ML-�7�¤[ ] �Ùr&�&2��M��0���$�/'�I8z! �Ђ	 �~6q����z�@Ҋ"�@�HD?�0�      `   >   x�3�tL����,.)JL�/�LD�Z�X��(XZ�Y����Z���p��qqq ��{      b   �   x�����0E��^ �?ma
���*UMT�`��ŨK.H�,=�=4L��Ŕ�hK�LNp�+`'gѱҮح���X>�S�k�EV�����.���g	a+������3^�NC���s|.YO��+�݃6�c��66      d   `   x�3�t�,.���L�IU(H,JTp���J��L�+��'�eę�\Z�����L83�2�2s2K@��.cβ���k��)gQjqjQH!������ P4b      f   N   x�3�4�t�K�/JI�2�4���K.MJL����2�4�J-(�O)M.�/J,�2�4�t.:�6Q�R�'�,1�$�+F��� ۏ�      h   {   x�3�LL����,.)JL�/�T1JR14R	5)��I4�Lw�H7����I����*u�Nr+�(4H�/�0M��w��H�J�t�t!���Y`�id`h�k D�
��V�V�&�&\1z\\\ �+B      i      x�}VKr�6]C��&E��o)�3��d�e9��,"(���*�69B���bi�A@����z���ݤYN2JhI������&Y>�f�d���|_/hV ���>ʞL?��sFV"Y�^l�X���A���k�9'�u�<Eg&��b�"w*iųjD0�XX'��a���L�,�&ގg.&Oɝً6��1�$b+�pa�#��LӚ�ɗ_���H6�z�q%�1�Vݳh#P!��{�"f���S��+��{-^L�L��iA+'�$Tt�)bX�Q�\4G5��>�R�X_��uJ���h��q�I� ;��3�I�'`4HEɥТ�Ɯ(L�O�O<�菊��\�V��H�G]������S���N�3A��˒Y���r�R��A�[ӊ&d,�*�ۈ<2�V��D]*N.M7�}й��Ha{�ˀx=��d�m|���0�N[�Q�����M�~�rT�˨�A��Lh�k��l� �Ӈ�JԤ�?���+�s�څ���T���(M�F��ջ�)}' " (ed��l�*"�A*;1��ǠX鍒������4*>P��В<�'�ٻ%`������*�I*�w���1_�������!�G��H1�am��
�`�<v⨶?$Y����$ur|�A(��q��8f*����#�9rr=BW�* (��7eG�RK�YMl�
��RO�� ~D�AJrC���?�F52�#��qF�(��+�fgGkr3�=�H����d�'�KC�5*����do��$��nr+���(U6�n{&�@�`oL`�jƧ��D�s�j� 	k�:��E���!��~VSr�;���B� ���{�^ĺ��p�i�e�}�=.�1�Vm�B�`� ��1ur���ݵ;c�Ի�)��b�Ǿ�#�+P�C��kXFv�8�&+�6`�}��+-ZcOt�l©�U��k�s#��m�md�P�����zXwv�8RI�Dө],�z�R�C02��5�Q�� 4�i��=@+8�L��y�h��]��p6�_����_��9��B4'��Sqfd��`e�Wb=kMc1؂��}�����'��N��e`x,����4L��g�gsx�8��9���#^�����7í��]��C(�
��=�Y�3�L��3�PN�X��=��by6���z����l��ŬX2�?�t��Ӿw��3����������f�&}����c�c/�:,���Wb��,|�?f���gY��`��YZb+`6�s����yp��]����2�l���t��!f;�h���S��-̕.���X,���k�      j      x������ � �      k   @  x�mV]n�6~�O�laR�~���fS'5�`�(&��K�]J
�ܦ�C�#�bR3t@��p�o~H��K���R|�0� �Ð��G'��� r��n%�5�)6ۆ�0�/�-�޽���	�bc'7fm�Y�>���� ��� ���7��\P)�g�m�v�B-�,�?w#�ce.0Ӎ�`�1:K!%x�Ϛ>�xH�*	*�)l��rPEP%�� S:��ﵸ�,2���%���lĕ7#Ul������@65S*08��=�{M�T2=��M7U���O��jb�������H�t�����_M�TTyc&?"��c��$j�⁭���e���:
p�ugg��P�U��Ue��4ؕ;׃�nt���fy����G�<I�R���?�H�	|��Z�2��T*cEv����He,�}7��:I����=����$����̀�CjM�Ot�y!�$*u�����1���a�n��1B��*$�#�+SE����Pk%�ŗ�f0�ut�u�O�H���8ۢO��"eC�~�Ƅ��:b.��z��#X�&���}b/Ib�Z|�F9zh]v=c�?xa#���n8$�$�R�qIe7`z�t%I!\-������YV�U��&�*I�8��<枨�͌��>%�%K����cK��|)�4�<�J1�F|��ϔbx�p6����"�E��㲓{\�-X�`��x�-�<�����SfD��1"���$��Z|3anlw���ɛ����b��ʉr-�����%�x��Z\�q�-'p�.�_�T̜%��Uǭ�A(�8��t�������Ω�E�>��HH1\�K���)A֭����>Hn��f7��G���f��� �
�}�]�QV�"G�e�q:ـd�ž�aCY�`V���1���uj����7����E��jdgkR�Ҩ�"�q�%��5� O��[�H$-#�ӧ��Q�֝Rkk��x�.݁J�l�w�?AJN�\Z�]h�4��qw�j�lY��&qT-�_%M|����r��%����'w��B#�������[4�hR��M�qA� ?�4���>��9>Y�X.'�'Z4(V�Y�V�ؖ|	      l   J  x����N�@D��W� ۻ�dKJ(�i���N"B'��L8��%�����dό'�AKx>ަ��}�6����p|�����p��4�I��a��[3C2 ����{!C:@:/�0� R��dQ�!�S��1�2DQ/$2$�V!�օ]c~!��1�c�ֹ�$�"lZ��-VX�u����8'	9Iް�;w���N��v�����qG��;���D�'Z��V��n!�L�t���tܟ���_A��M��L�%�����艁U,;����X�!��X眢gg�s
����w�N�9���ꭘ^�Y�&Z��z�$���<�5M��C�A      m   9   x�3����K.MJL�/JTp�,.)J,K-�t��4E�IIE�46�4�4�,A�=... =�      o   J   x�36�t�,.)�L*�L�/JTp,;�69?'Q!X�Q/X�ө49�(171/=QG�9?'?7)3�NWC�=... g��      q   �
  x��\[�+���Kg B<>g����u����Peݱv�]�zfo^d_���9��O���^������e��R|�Wxqxِ^f�tƦ_~m,�Ⴏ4�M(4�#Mx�P\hr��"���a�I����{A@�˛�=0�������]���㼲X���槌�����
��J�4�uU���PvǦ���nV�S�����*�������$���m��L�g&]*Tn��/�S1Pѿ�
@�g*o�����ک(��"L1Iԕ-.d�/��` �c�3W�>슶9\�a��x������y��9U�hr�҃W劎��SHhpxB8��o��Bh�+�B[�;s�ݶ����ר�JU�W*�Ek��PY�
,ڸDE@倪����7�Z�[�-�B�D�@�!�^v^�f2. F�˩�� \C�4Q>I�GE�X�8��cIc6�'����J�<�a��ؼ����yB��@�y"�ه�]fT�'��"����S]Qy�9!���
a�W�a�r_a*:�]﵈�5#��%p_S���6p��D�;��W�C�nK)7�߸�Y�8�))\��l\��{��Up7�����wA��EMWg����/4��Ì���fRU�PN�oda�YD�;q�^i�O$+���7(v<5��+�te�X�Թ�&�j�� ��c3[�w��^���-��2-:������M�`Ოl�2p-{K��۾���ʂ�Ɇq���Y�jQ}c]�'��S�gjᤖ��萋luĉ�������d�n)5�j�\�^�NKt�z����U�����;�����	��Rқ ˉ�w2��A��=nYҋ�& O��ą\��p1�gِ|���
��)�L	�sy���^~N\P�
�҂��t\ڝ�t�G*
�B4&wia,��&a���M����1�v�$r�H�k�t��
��b�;�b�L\�����O\���!Ww}oZ��H,��ˏ�V#�߹\�����Q��2�?WWwޚqZS��\�\��*�.\� 1���u�&�z!�2��M�k��w%.w�����>�V��S+�{'9�	���pe�����8:,���~i�K��a-`���XMZ&0IY�>b��i�%f>�x���5�:9�)wYJ�kY*��1�a���(:$��F�|MRYMR�q�A�U���VUMH��o� ��b+�V���yD�Sm��*��Z$�s�9L�y��������y^k�ы�aԮ���!xM��h�>�EZ	�6��!�� x��`/�#��9b4z��� �_GpN�Mdj�F{���� �Z,�
���x7+)ɝ779����)\�Q.�G�D}���H�mv-�nʶ��&�j�� ���N ���k͐>�� �گ���`�'�^������e�U�m\0)Au�l�kUh� �PE�n����B4*���L��v��>{�mw0x�lϓC�۬y��(EPj<\	��lH��\��BWK����|���ƍ����q���# 3f%u�)M��מ�S6c6���~�S�9�1)kOB�+��y:6�>\���zTˠl��X[�� �y�͵�m�$J��z����̇K��!��t�TȄ����D��iHgwck 嘽4oe�͒��i7���4���4K�k�n��R��-��d!���a��*py�"�:M�..Ȼ�������l�����t����>~�J�<B���޿���%�1��Ԏ��y]6j�8&��[����5;\`�^Z׉��NUd��DI�SИ{jGr+�f�'�"pA�8�K
��^��}L�ݖ�JX�krk*������~�G�Ͷ�K}����A��t��F��BN�0���ǭ��&<���"�'G�ՠvr��L�*�T5cZ���&	���_ICOڬ�W�J��/��k����^Ҵ�v���=T�*�TPw,�j�%�{��%��I�~!�CX���wz��.ϙ�]
�h~��Z���aI3���p����h�A����J�|:F�G��J&��ڕL��vU11��l\8{���k������ b�שB����բp�z�q쓲��5F���$�����W�Ӑ)Q�����b�I8�q%��F�b�'�+1S���@x�j-G��;���$ȷ�L1���w��q�<|�S,O�|�\�ז�y`��,P=<c��PmU���z�x�C�����H�'���&^����4���ߩc��7W�F?\==K�4z-La�¹��B������.h���7N1-�i+���K�,���$�O�ïî=� V��(��*\yQ ���*���Mr�@�y�Ӣ`6 �M��gp��P���)ްPn��B8��w|�1a+���2$*^���ː�PY�:�ː�"P9��j'��l�v�k��̯�%C�� gn�nl���j��K�*�×"�{c�Jy)"�-��/E$t��蜰�}iO7K�����e�~�c�|��Β1�j�ڝQj4~z��?��ҳͱ�T��>3�#���ղa��Dg*7v����k�WBP<�ҵ9���*���B�n��8�U�/&�<�yf���������>��pwPn������Y�^�����(� ��4�pPv?N��z� ����"����{����)O+�����r<(���[�F���x!)�'�c��������-*����i+Ic�S���"kWʧ�F9�05_T�%�Ru+Y��Q�o�&�R�k4w1���aN����A���v���˗��=v�0����ں"�TU9�J�z���V��\Z�.�zc��\����5���^r`6���[m���?����� ߸hy      s      x������ � �      t      x������ � �      v   Y   x�36��I�+ITHIUpJ�KM�L��Wp�Qp�L�J�r:�&'���:
��9��I��Xu9.��%E�e�Ŝ�����@����� oh!�      x      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �   �   x���M�0���=�i+.��
���̀�4!����X���I]@@J�E�&�/�MC�4�d-K,�VԇsF,t�ESM�Vw|A��O�㡹"`ě�b�̻�v��`fzn9=����bl�~�����BP�}x�]U ����M�js�T��Z�w#��6�'�/P�	�ŝ��z�[��q帹��j�⟺��W3I���mC�ul�q      �      x�|]m�-;������@K�o���������W�����EU��֕��4�{'��r��}���{����J���I��ߚ�v ����"�~ ɁR�@����dRH����RHc �D��@��d|�t��id0��2���;��@����d1�t��?�y�����/��?��6Q��������mu�T�`~����������G�`��&꺩}��?缉:o�_~?���7Q�M������8Nԁ���)�#~0΅uᴾ`����s�D�8�_0M��yq�^���r��8/�<�~�r�P��8S/�����G�`�g�Ź~��?�řzqn_0E��yq�^��L��q^������G�`�g��y~�t�?�řzq^_0C��yq�^\���q^\����s�G�`�����D)�eN�i��Ͽ�^���
�*�#��"����+;�M�G"��_���U
�"�#��2��8��|�NE��b��Ws(�k�w.�U�]ݡ�oA��H@K?޻�C!_�����"�~�wM�B��ґ #@��u°|$[>`�U޻n�,��0�j~`���|$[>Hv\��X>�-�?���q��l�H�)Wm��`��d�G�����q.��l�H����8f�H�|$����㜘�#�� #_��b��d�G��d��y1�G��#��\���优�#�� S��xqr^��l�H��W{�89/f�H�|$�ȿǋ��b��d�GL�����y1�G��#f\���优�#��p@�xqr^��l�H�9W{�89/f�H�����q�t8q���r}������P�ה�|���ηC!NS���"E���99�3�j_(����P�˔�����?���C!S��;���?C!S��;�ȯ�xon����BK?Λ�C!A�أ;������a��b�� #�~�7;�e���3�k<��3�w�w�n��u ]�����W���~��k<\��3���/q�ǃ��`y�2��S��pq�3���/!����|X��f~��k<N\�K�f0�F�xqq^,�\��b�ǋ��by�2���s��xqq^,�\����w����8S/���^����8S/���]����8S/���_����8S/��f\����8S/���@�����yq�^���Ϲ����yq�^������꼸P/.�����꼸P/.�F�xq=ܯj`��i�q���Zr(���������xM+�xM�O�R��X�5�B�]�s7��k=Vj�DI�J՞��@��Cos�&J���ч�Cpw��T��T�F���(f��j���#���ݑ��5<�硹;�YH������;�Y0���|#yb�3���h8�B���p<� X- ͢��㙅�j�+���4��p<��S-����6�:�獆�v���f���l|7/ڎF�1�gǺр�v�0���Vs���>׿����72��������t����W>{��k9�Y�h3��1Oov-�2��bF���<4/�k���,fa�鉮�xf1�Y�8��=</�3��bF8������vD���,h �.
��,j4�H�� �fa�Y�@�k 9�Y�h7ZH�h� ����@}�=��������!�xp������U��യ�U��gw�k�Op��V� ��u��	��ߡ���xN���$�D��ͪ�n�uaJ��tvL�¸[a�4��Q�j�n5m ��:�ΎkV�v+G���Avvd�J�[%���ő͊�nE` ^�.�lV�u����2@vqd�ҫ[��C� �]٬j�V5 �{��P4��+}�A'�>K�A#��$|����,N� cG���m0}���F��#H��'&�>�A#��$��;�su<�2,�����7D7G4� �"H ��n�iA�E� ��eP��,�� ဎ����fdX	@���͑�"Ȱ�~���,�� H�f���,�� #d;���lA�E� �G@vwd�2,� M�@vwd��ɰ�I8��E ��'����������*q�2h�2(���>��I령렀���*q�Bh�Bh�P%8 ��'���UBH+u=Ѭ��7�[����c��B�j�p@�@�tT�bhZ1��#���UCӪ� ��	Ȟ�lV)��F��٬�V��d/G6����CH>( {9�Y=4�
@�2����f�дz(�Y>��٬�V m�?d��f\4�Y;�	8ZN��C����L��bLgŸh6�v6�B����pH�_;�	8�[[�Y�������9.xN�g��,�f�����Ͳ�e�L R�L'�4�f�e3H>@ɘ���e3˲� �Oqp��,�Y�ͬ��+��#�e3˲� �Q�GGT���Q�OiC��GYk�C!���>O�z��
1W�_��R��(Y͵��ע�E@i�z��
1T�_��"�?�Qs첯D��D�>�������Q�p�w�0���Q�p����{)�r���}��a�;��ס��!�f������P��p�w��<�,����-����e��2�(~���p<�ľZbpԷ����������m����Հ�9�O_��s�X��L�����o|O�L�:���mǋ �����s��р�v�0�`x8>���mG�(}��P<�,d4G����r���,dm8>$/G2�BF8�o=,/�2�BF�����hf!�Y�8�7?</�3�BF�Ѿ��v�#�Ōf1#���fA�Y�@*JW 9�Y�h5���JV �5��F8����#�������A�9��i;�_���:*�N�ᐺ��h������N�}���!8`�y���w; ��3D�Ӫ�����6�AtvD�v@��m܃��f�n� �x�:;�Y;�[;  �n
����)�i~�n��#��~/�m����]٬Э�4��ő����H5�]٬Э軓 �8�Y;�[;  �n��#�����6�A�9�7h�矮�{p}�B��Q�Xi��sdn�2��b�JL��n����CH�Q��gE5�g��p@;� �9�Y��Iڹ��1�BȰ�4 ��Q�BȰ�Tg ��q�BȰ��͑�BȰ����lB��� �3��lB���p@�� �;�YB�&L �;�YB�&L �;�YB�6�A�Y&NZ��S���=�>��I�y�%4o�>��I�y�%�n�>��(:<8��sm@��R��.��6���t<�ZhZ-��q��#�Cӊ� ��0=Ӭ�VE�C�@�tT�rhZ9�T���q��i�P ��=�^�lVM+��m܃���fѴ�( �>�^�lVM+��V] {9�YA4� 
@�ۀ���fѴ�(�8o��٬ �V m�g d/�ͬ+�h2X�S	�kg3�s]�Ё���kg3GE�	��pH�_;�	8Z'��`\4�Y�� �܃��fٌo퇠��1%�4�f�e3H���:9�Y6�,�	@*܀��f�̲l&�M ;;�Y6�,�	@��
�Ύl��,�f�Nn���$��Ţ �5���$��Ţp@9������bѲX��#HI���E�bQ ���c".�4��/��N�@�;&���-@���eLĥc"N���}iKdqz��-@�H��@�1��������]�,�Q����=��c��4�T��t*tWG7Kj�}!i����e5�d��Z�«#��5����T� ��1�����IG&�xs��`�H�넮���1΢�"�/$}��7�8'�4���ƛc��EZ_HZ:���gɍ�c�o%&H`���K�f7i��@��?&�R��M���"�v���1!�>����y�n.�Ǆ\��}���E }�lr�cu����H�x�ñM�Υ��:������q).�$�=�tߛ-|�H���K�Nw�ٲ��$>�Q�4tϚ-Z�'�	0+��c��H�%iI� �ri:��~3[p��r    i:��n2[N��0.��c����b).0�
���r�ӝ`�,"�``.s�o�ҏ���	00������h��G+��s��K|V�K�"�� ��10����7XE ���\>��>�l�TD��\��4�dw����˷��F[���06�o�7�(��)"��$G8�(�n)���\N�qQ�E��$N�ٹ��4�d�(I�0<��c�F�l% �70��4�d�(q�� ����%[D	H�����1N#J������UΎ�!p��|�;����0'�2<m�p�@��w�9���i;�#� ��̻��|���G���2��r������;�`�ϻ��|����u���e.�踇nH,��2�t\!7�J[v@���2Gu��6�6Z���uu\��ms/�e@��2WGvܹ6�X$�Gm��#;�K�{}�dkbdWGv�t6��U$�Ge��#;.)�{i*��͑��ͽ���͑W�ν����QV�v�͒���e8���2��pB��P�#�GI��r8!��w��dKD���2��pB؟�jN�#u ��܋�	A�����̽��J�d���̽̒��Q����P2 a	�d���̽B��Ñs���?N�eR�@�pdǔf�ōH̏Z2Gv�h�޹Ȁt*����̽.���ӑ�72 1?
�<�ٖ��n)d8B��<��!��~��WP=��!��͏;L��pAe���d^��g,��@bzԏy9��4��%�`z9��4�.<$V@����:�@s��c@��[?f'L�ɏe�A+�aS�	Sl�c��GS�3S�	Sl�c��G R �Tq���X6��T�0u��m�~����1`��XC�>��_�gإc�N������;=G����" �>총N����mH�"3F�Rul3�5��u����K���DWE�_H:���㛩��ԿNh��WG8�]i~!� ƛc��?���T ��1΄WE�����p0��,�(�1�WN$0��,�(R�BR)�7�8�'�4��T ��1��J��	mV��c�.%��Z�  ��B~�ۥ���t����d��]:��&?j_@���c�.}�d-�(�H���y��q�Ͼ�'� ��#�^�c7�D$q �ܥ�ئ����:�t�{8���8v3NDR%|�7���n��H������؍4I5 0>��2�M��)�Ƨc�^c7�D$�E�O�8���nq�H�������,� `|9���)v{JDR �/�8��d_Y���~��%~�H���D ��ߥc�.�BR>Z�����]�}�|�$� �p�s; ����#���O��%;}�� ^��4���I #x�vtӀbW[D$�&��4�صI% ��4�ؕDL$0�#P���$N�A���4�d(I� �x99�i@�P�	��xr�Ӏ�-�$��xv�Ӏ�-�$�xv������0��w�9�m��nc$R����]`N~�ⴻ#��@`{ח�߄8�&Ĉ�B ���%�0����� `�d�C�ɺH���2G4�O�����2�4�O&�R U�@uqT��d�.EL����2W�5�O&�O �@vud���;������-sud��d��d8 �Gi��#��'��'��@ �:�Y2Y2 � ��#��'��'�* �9���:��nTL ���LmEr�p�~-p��Yr3߻���@��M�Cd)�6�s�t�Q���0�@t���Ry��G/Ͻ�8Lmͦ��!@ �;��ښMm�Z� ���fjk6�5 � ��������� �2 ��k��fS[�1>��<�Lmͦ� �>j�<�Lmͦ� U�@�pd3�5���T �ӑ���ljk�RdOG6S[˕�p��(#�<�f71��&&�#�G�gw8������s������T�?� ���p(?�GU ��þ�ž�H,��1/Gt�O��� *��^��8�>�u?HE P��q:}�z���-�S�b�v�z�p�I�r;Y*6n�fg��|� ȩN�o;�9H�a��70} QYj� H9 r�T���}/; _Lٕۑ��s_)À��F��ٱg;�m0H�"��+� ��L���4�#�Ǆ]I��oHݡ(^#8�:�C� u���#�0]W�r8��Qw(
8bz�֕�Nx�`�����byL֕�x���̽=���1ZW�#���j�( ��1[W�c���j�( ��1\W�����j�(�sULו⸎��̽A��?�ו�Ȏ��̽B���1^W�#;�%3�5v@̏�R��%����1 1?��Jqd�wd�ޢƀ���+Ց_��{��c����ul��|���p��1�u圮c���H���?וs��mR��&5���Ƃ�s���R��*5����u��c���K���1YW��E�f$ ��1YW�#�E�f$ 鍸`�9�YiA�U2����E�f$ �����,�4� ���lA�E�p@���ݑ�"H��$��t]�lA�E� $.����,�4� h\�ue8�YiA�u2��#;�c7�F5$���9]�6��w���c����ul��|7�������+�lۨ6ߍjGoi��hۨ6ߍjGL�ɺrNֱ�jsoTc@bz�֕�f�c���@�a��L�4��֓@��a��LG5��֓@b{Lו�f�P�z(�c��,G6��ލjH��麲٬�V 1?���rd�z�[=�����+ˑ��n�P�O�j����f�P�z( ����ގlVu����5c=kF�Qm����b}���,�F��nTc8b|T����F��nTc8b{��,�F��nTc8bzԋ5��e!ò������ȃqX��/{:!5��<φ%#��`����ٰ�! ��i�f�K�%H��`͎#�<K����r4I�%H�u�G6K�%H�y�G6K�%H�`-�l�<K�4٧<8�7��P4���T'�����q����������s�=��zj��~����1=��zJ��v��oga@bzH��:�Y�0���!��α�aZ�W�]�`m�j�<LK��q�ks\��aZ�����ksd��Ὕ��C�͑͒�i�C �C�͑͒�i�C jW�:X�#�%Ӓ� $O;ȃ�;�Y�0-y���dwG6K�%H�}��� ��e���0) �Sd����v�y�:��z���v�����p����Sd����v�#��6XOm���2��Y�X�`�g��,KB�4���;��=�=��m�g 9��=�=�P�*F�~�U�r�jW��\�������Hu��#�4�G~�G~TG�������_������#�=�=���+g2�n���-��s�>r�r�����c�pT��i:�~�m��3%fw���.{���}}f��6��o�������dG4{�{KIqɎi�ȯ��@����f��j��p@;��8��#��#? igdG6{�W{� �>2�R��_����ȈKqd�G~�G~�GdG6{�W{� �����f��j�� ��}�]��_�����3+f7���f{�#�GR\Τ�]m?߫���:��̉����۞�hoL�)1��~���O���>3bv��ܷ�3 ����4G4!^?��\Udĥ9�YiB�v@usT��,� m���f!�Y	@���ݑ�BH����BF\�#���f!$�#��#���f!$ ����#���f!$ iodG6!�BH8��}�=�,�4!H̏���Y1��~�w�3�>��rf�����q�pT�����~�w�O��\�r��cw����{���}}N˱;��{�=Ñ_�re:�Y�Эj���+��Z��Z�Hu0=�L@�&� ����f=�n=� ��}p�׬ �VEyD�@�rd���[A����\Y�lVu+��
; {9�YAԭ 
�����vd���[A������vd���[A���(�Y4Aƕ�p����Y3    �k^��%�w���d�g�8hW��cM���"ȸ���cM���"ȸ���byԋ��4����$@P��S�X23��+C���;ǒ�a�L R��3%g2���� $��͜`Ų�aYQ8����+�ˊ�|�0zW��ˊ�{i�|�0zW��ˊ�eEH9�ղ�ˊ�eEQ� X-;�YV4,+
@*Z����fYѰ�( �a��G6ˊ�eEဘ�w���4���S�Q�
\��w�ƴyĴPX5��s�nҘ6��
����z��M��i3p��ϱ�Ic��1-��_���Z�,+�� ի@tuD��hZV�`��c���i�( �d����eEӲ� $���]m�k�Mˊ��w�9�YV4-+��� ����fYѴ�( �b��#�eEӲ� ����l�Mˊ$@ �;�YV4-+
@E�@vwd�6�6q R�
d��w�^���K�pT������f���L&�\�w��c����^�#F��]=��ؽ4󽗆��1vWϱ;v/���Ұ�W���fi���OR�
L�2�����wu:�Y6�,�	@b{����f�̲lf�[�:F��td�l潗���0zW�#�e3˲� $���]��l��,�f�1?F��rd�lfY6�����ˑͲ�e�L �c��.G6�f�e3H̏ѻz���]ks�Z������ݵ6��5$v��];F�讵�w�1 1?F��1zGw�ͽk��c���wt�ڴ]kI��v;�Y9*ˈ$��v;�Y ٻ��x �Zr|�H�w�Mvu� ���g�d�ZcHb<��-9�Y,ٻ��8�Ж�,��]k�x
і�,��]kI� dˎqN��5�$^�
�e�8�'{�C/@	ٲc���km��k	�5$ݵ6��5$�Cَ��Z�{�@َ2},kOo����lGIw�ͽk����lGIw�ͽk����lőM������$�2��6]�n���5WG!٪��nl���IL�R�U�7]�n;�#�� ��V�tg�-m�'�PM���K�mk{D'@9٪c�nm���I�>ԓ�9���v����PP���{�mq;��F��xs������="���l�1N7��u$H� %e;JJ�k�T��7%e;JJ�k�Ծ�� ����~@�H,���%%ݵ�Z_@C�@�QR�]k�ɐ��(*[wlӀbKI"�<�PV���Ŷ�D�.H�{8�i@��$� J�6�4��^��$>�ڲ�8(�J@롸l�1NJ����P]���%[@�k�	�O�8(�J@/@}٦c��l% ���l�1�PJ�:!^�
��
s�]k�ݵ�p�P`�]`.�km����X�e�����ֻk���Q^�]^.�km�����Fp���.ߵ���5$�Guٖ#:�Z[�g��b�e�ݹ�km�]k쀘�e��q��ڻ����e��q��ڻ����e��q��ڻ��X�e��q��ڻ����(-{rd�]kk�Zc@b~T�=9�㮵�w�1 1?
˞�q��ڻ��1?�ʞ�q��ڻ���eeO��km�����Ee��,�Y�5�#�GI�sq8!��w��b���������]kGL�r���pB�_�5�#�G5��:qbb���5v@,�j�Gt�k�޵ƀ��&{qLǴf�]kH��j�Gu�j�޵ƀ���&{q\Ǥf�]k��[#@ �8�cN���5$�G-٫#;�4k�Zc@b~���:�cF���5v@̏J�WGvLh�޵ƀ��(${ud�|f������U%ϩ�E��e�fP����^�a��2y3 i�S-�\\T 4�H����z�-*o.�7�����ӽ����e�f R�Dӑ���e�f Ҿ;�^�l&o.�7�x!@ {9����L�@b7����f��2y3 ��1W��8z���Wݳ*���c0�^u?�U�H��7�復N�~��g@�u� ����^uπ�����3@����{*^P�#�MI����.H�mG�U�I{��k,����	������f�d_uϐ�0!ג#���}�=C�;U-9�Y,�W�3$U^�xr��`�������1�ֲc�E�}�=CҦ;ώqN�U�I� �V��g�d_u�N��ݪ��l^b_uϐTy���U������g@�w���U������g@�v���U�������� ����*z���W�3 �^�j��U���iW�3$�]�vql���'������nH��I���:��tۑ����V�:��t[�����U��q�$ݶ��B��xu��-�&="i��7�8]�n{�#�v��xs��=�(=�/��V�9��t۔���Uk�q�)�V�G$�^��������H� �x׊^u����_�������Կ�$8ݠ�x׊^u��_@R{�]�v�kE��z���	^�vwlӈ��������F�6"i�|�iD�[a#�� ޷j�N#�]K�A��1N#J���T{��1N#J�����Ƨc�F�l%�Ю;��qQ�E��$^��M�8�(�"J@��l�1N#J���T{��1��6>Ջk��l��\���u�C�,�����s��ו����+�˶�K�<8��s��e����Wݯ��{���x����Wݯ}�=Ҿ��v�#�5�5���3�Ӭ!��!� � rT��`��` ۣ���5�5��.@�l�L�����#�5�5���Avrd��`��` ����ɑ����H̏²'G6k�T� ���Q�\��{��w���Yr�ޫ�����u���@Hm��b|��=W�Cd d6Gl�����p�
���pTs�y:"e�7�6�AtqD3y�1�7X	�.�e:�/ 1=��^�L��&o �=��^�L��&o�������f���0 m�����f�f6y3 i�dWG6�7�ɛH̏R�WG6�7�ɛQ� �]�L��&o �]@v=�fWݯ��{�#�G���pEe���b|��e�C*��p���ez9��(W����b���-9��([%���@�j�ᐇG�IQ�{aڮ��p����p�^��8qb;y�i�5�戎��/�a@嚘���1��k_LÀ� ����/�a@C��uw\�V�ڻ���F�@vwd�N�ڻ�P ��ٱ���n6ԯ�I�>ٱ���n6v 	����F^{7�cήGv�"������1fׇ#;6����ƀ������ ��f[�n���/�L����Nh!�w7��c®��p������p�����9��.�zw��������s�8�m��w�1 1=���tDǗe�U����1]חc:�+��n6$��x]_�����ڻٞߔ�����qߔY{7;`��/Gv|Qf��lH������=��w��b~؍ۑ_�Y{7�c�n܎����ڻ���v��������v�vd�wd���� �}Efb�n�vl7�zw�1�>���9_�v��w7��c�n��ul7�zw��������t�Ͷ��lGL��qױ�lk�fc@�30\7�#������\L�麑�,�4!����Q�BH���������,�4!H�����,�4!�Xv#;�YiB��v�8�YiB��v�8�YiB��v�8�YiB��5��#���f!$ ��1a7�	;��m����Xv��c��ֻ��}.��|�8���n���fc8b{�׍s���f[�n6�#��t�8���n���fc8byL׍�xfQ��(^]sMT��9�YAԭ 
@by���9�YAԭ 
@bzԌ�9�YAԭ 
��(Gs\���[A���(Gsd���[A�ڵP4���fQ��( �ݑ�
�nQ��F�@vwd���[A�� ����fQ��( �ӣhg��v��w7�<]5�8kF��m���NP}��l7�zw�1�=*�qV�l7�zw�11=
�q�l7�zw�-vq�����xfdX	@by�c:�YA�X㘎iA�E�p@L��qLG5� �"H ۣf�q�"Ȱ�����td�2,� 1(jƱ�,�� q� ����fdX	@b~Ԍc9�YA��5�X�    lA�E�p@̏�q,G6k�k� 1?j�y֌��C������(�Y2NZ�]1>*�yV�q�ك���e�B�8ςq�zh�z(���Q/γ^���V 1=
ƙѬ��8 �G�8�c��C�� $�G�8����C�� $�G�8���C�� $�G�8�#��C�7�-]�jƙ٬�V 1?jƙ٬�V 1��NV;f��e�cP	�50>�[w8D�+�He�L��L�}�>4S1=׻ә�?�� ��tM�	M���c ����������c8 �5��zwd3,�H�������c �V:��l���1 i+dG6�ϋ��Q� �=٬>/V� 1?��pd���X}�TI�� �]_�������M��v8$�;�8�L�3;��ԝ]�=&���=w��ԝ]1=�����$!ug�Q��<�,���] �����f�u��: i'L/�4ˮ�e�ဘs}9�Yv]-�@b{L���f�u��: ����#�e�ղ� ���pG6ˮ�e�Q�� G6ˮ�e�H̏�����D���H̏��q;�Yv]-�TH� :�f�ׯ��z���tp}Ṉ���{}=��^:�N����ABr=1/7�y9v}�z��_L�}�˱���{}=�Q<'�3� �"H �F:�ΎhA�E�p@�`:;�YiA���r#;�YiA��s#;�YiA��( ;;�YiA�qML̍��f�Y	@�H�ő�"H������ő�"H���117�#�E�f$ ����#������ ��t�}Ṉ���{}=��f:�>����뽾~11Bp@�9/Ǯ�_���Gl�q�q�˱���{}=�QD��r������g����#��C�?I;�`�9�Y=ԭ
@bz̍�f�P�z( ��Q3��s�u2�Þቚq4G6������v�Avwd�z�[=�����#��C��p 	��lVu��P ��٬�V R@vwd�z�[=����Ϛ�]_����S#\�%#��~���31>*�qV�����^_�p��(�Y0����{}=�\U}֋���opa@�J���B���o��t0=�,�!HL��qLG5!�BH�#\O�5!�BH R%dOG6!��+H[� {9�YB�m�����f!dX	@b~�c9�YB��E�X�lB��� �J
�^�lB����G\E�<�Fv��z/^a8�M�������,���B�8ϒ�]��ދW��b{T����+�x�ᨊ2��z�� �� 
8�I����
�iQ �N:�N�hVM+��!@`:9�YA4� 
@bzԌ39�YA4� 
@*����fѴ�(�h�ّ�
�iQ �h�ّ�
�iQ �h�ّ�
�iQ �h�ّ�
�iь�s/�3;�YA4� 
@b����&��p@��);���i�1 �� 6e'6��8-8 �f��93�hp\W���adn�#s�ǵ��7�-L��sbn��vp8j20}�-��G������ܢ�q��>�c^nV�3�˂c �c`nVG4�˂c �cbnV�4�˂c �cdnVG5�˂c�sF��uu\��,8 1>f�fsd��,8 �>f�fsd��,8�b~�����f�qYp@b~�����f�qYp@b~�����f�qYp@ZT`hnCsto��{��tF�@�15G�����!��!�����ܶ��6��]���cn��n[{u;��087��9��m��mI�q����g�d/ocP:��9�,���mJ� ts8�Y@����|F�@�p������1(��ևc�����A�+`�nNG;+{�;���Q�9�,��nJ��ts:�Yh�;��:���t��ಗ�1(u���hg�eoq[��#us9�Y;z�qcP����P���7������yLե����m(��
���X���&7������y���Unk�rcH��[�`��l��RG�lݺ�t��aaG���(�9��nK�#�:&���H�[�m{�RO��ݺ�t�-b�'�G�٭?Lp������A���t�-c�P��[��N���:��΀a���t�-d'W�(hO�v���V�G(uܭ�h�;�m){�Rg���:F��Z���:�����uԜt���~!�+��\G�I�����z��uT�t����������se']��C�&�oW(P^�4�d����.TmP�s`lJ�RG@���#�[�O�'�]űN��B�P�
�BWu�� �-�(�ԡ�:�i��`�:*�U�4�d0q��B���h�&[�	P��FWu�� �-�(uԣ�9�i��`�	u��"-��ǒ�i�|h�hc}�s~!6d����\*[�/�s|A�G�����#�-��9�������p�����9�������p�����9��� ^�`� ]�LX�W��
���i&�-�����:����LX�����	k˄� �]���LX{� f@�� �͑̈́�e�Z �c~a6G6֖	k�5$@ �9�Y�xY�8 iSd7G6�/��_Klw�6k/k����>&�-�k�̐��t��c��^��5�IL�a���x�{�ү�Ə!z���ސ6!��1�@o^v0���3@�p��p������8�sP�]��:��p����/fP��X�uT�m���� ��1��ʾ�A���}:�Y`����B���hg�e_̠�0�0����}#0��5�@�t���f'~о�,��;�ԯ�ڏ�z)�ڗ3$�8�c���
����I]�p���}-0�)8?F��k�̐~��M��!��{�mD��32��t�����'�0��O;���U�p��0�n�9]qk;n#ԯ�5�H�Knm�m���3�zr��5���4�
�'G;]tk�n#���Vr��U���6B�3`�a%G;]vk�n�_o�'G;]wk�n#ԯ�ڳ��.������� ���NW��k�"����1�@/	~j<;��l}�H�!1��Uz����� �&��4��~�P~�������:��f���14�؅zJ� ��*�r_�J��~��s�9�/v�^����@zq���b��ސ� �zu���b�E�_7�WG;�/��K�R_@����Ɨl�%�Pg@%����Ɨl�%@�Z[��:�i|�_�:����4�d�/J���j����R����!����u|R�Դ���s��A�%3���S���j�0���U7�>(ݡT�R�P~��e8��P����?��P:C�_(��ȃ��`(�E���P��P&C�_��GJr(���ϟr����*W����:���7q�M_8�wq翉�o�_8�F��<8Q�6��q�É�p�_8�I��yq�^��ׁ�g���D�8��t���8ԓ����mD���ˉ�r�_8�m��3G�ޜ���-���s����/��B���L�9�/��2���s��8�}��8�ԟ���0��k�Ǩ?��������8�ԟs�©�[~m���s�_8�B��7�y�P���c�3�?g��y~��^}xp�?g��y}�}��8.ԟ����.�l~��璾p~+��ß3�R��	S/�}~C��Z�#M�#��yvC����B��;�M�0�K��D�̸03��M��;S	��p��J�{�ȏ�0w��^��;W	0bp8�J�|!�eoF?^����C��J� ��*'KW�Ƹ�ӯw j97f�J�t%�����-��,]�;��I���92KW�~�����d����u�\�(�r��ҕl�J������/�t����8��s�G�:p�3��#v��q����I/��p�9�;��u@
�8ΟY���k�t=�e�8v�P�ɟ�����q�b��l��p@�.�+���(W���W=�9���_0z�:`��!�Sv(,1>��gN�`X�)|�^}
��p�X�=V7�q ���j{�z�c���/����)��Yg}��_��~���u�n�Nw8��/]'��p&řv��3��Ys���s����sx��/N����    �}���t0��u�F_�{`Zr0��u?�����`�	��W��8?�@��W{���l� ���4��_���|�4��Vh׀H�á�&�s��v��g<��ڟmTh|�"��Y&?WVW��h>}֥��t�>�z���u�v8�
�W�uW�^ݶW?[���3���su�fn���p볶hԭ���s���}�[��|�n�̭��	n}�Ѝ�u3����>��Fݺ�[ ��p�3Klԭ��u �&����L�u�fn��>�°@�[���_�G���L�C��wI�l>���ڔRr8��}WaGl��8�ؾ[�hk1h8 b�n�F �g>��Dl�-�@�!d��@�����3�MC����z������n�t �߶h9 Ӻ%�=$:�.���֭��4,����փ���n>�F.ܺL�C�zl�8��[��p�[���G����k=q�[s������-ܺD�z�[ ��n�nD�z�[������u�an�~F Pw@ĭ��u8����OlP���H���O�iP���H{�p�S왿j�/м���&��)�������W���O1c���3���	n}��~w�w�_@b��>���V��T��[���IK�i%g �#n}��'�9�՜H=n}��'-:����H���>ۮ�V�Ӫ� �����Ꜵ�Vvΐ�H��^έY�9��@�{���nޢI��>?W=���|v�M��N���88��㯝��#g�8,I_����_ m"<n��퀈�%�H��� *��~Y���sA�fP?�X��,I@��U -Dbڲ$=P��q�_,9�HP[�� �Ko ��%�˒� ��� �[c��_�w��|7Wx��uI����JR��]�"������+���]�	�<{Ϧ2$���k��!��[�	U;�۵8$��{p�!ə��Dbν-0$�
��u9$��{�~����ܻe�D�{�F`H���}Oa<���mH�D>��CKXg��4W�R~�
���=D��?�s���Ҟ]�@BxB[�|d:_�-���\��q��g�����$'0���l�-*# wr=p&l�+��/��Jr-pf�r���Ӆ�'�g�*����F�0�8SK��Wg\��wvr���UWA�(�}~\�Np�N����V[駆�����a���O�Eo� �tXeU��
��](N�a�U��*��"l�8�����{�_���7+��.��`��,�߬������7��3��7+��UQ����VT9i��k��0���AFx��ׁ�R	�N�aEU��* ����7gEU��* i�ݝ�Ê�jEU,4����uXQU��
@�ԄO��f>�.�t�>�%�S��fN�������z��7��v���U��[O��fn�̭��n=���ܺ��7V{í�q�Fݺ�[�B��^N�an�̭��	n}N�4����:J#�l�[/'�0�n��H~ۊ~��D����:P-��۹5�4� UV�O�]��U�����~P�Sn��f5U���}�c�$%��f%U����A�fJr�����
�4"@h���$�T�zH]]��4VOu���ŠMxΓtZNu˥���(Iى:���L mhí�uX1�-���&op��DVKu� �n]\���������^$̔���̭�տpT��[W��fn=������í�St�[s� �� ���*ԭ��u �kx���i:̭��u,T��[��%���0�@j�us�s�an�� nݝ���z�[ %nݝ���z�[���ܺ��7��C���%���7��!�񋌹�4\�	z������c%i��7��U�H�g�[�s%����J�(��z:M�՜�j� �y���,	2�k��뀾���N�aU紪3 �L���DVvN+;�j1p��DVwN�;�`!�%&K�r�o����S��g�z��7K��Nң0"8hޮ�͒�u�/M��]�%�˒�p@ߨAg�+Y4I_��IZs�3���Ò�eIz R-��s�d�$}Y��k�����Ò�eIz ҉
t��uX��,I�҈ �񝝨Ò�eIz ��%����{3�~7i����ٵ��_�۔�j1p���̱���H_w�g��f��7 1�B�S̖�c��R�B������:̷��"*����/��i�����0`��v�w�=C섾�n���{/4CR9�ݜ���{�bH�����spV��U�L��;���ǐ	���th{�� N��I>�L�ǋ���o���o?�L���������7�"�C �WB�� Y�7�8��
f��pƅG�}>2}�1[x�E����'@�/㕫|����N�	�V.�{!�������ʵ�`��A�0;=��W��p ]�wv�+���WG�dhf���Xupƅ�'��Xq$�=8N�a�U��*�����q�+���VQ����$VY���^p	߻���%�������^�F/��B�߬�����ѥ��qz���UUA��-��StXUU��
@�ud r����UUQѵ<��ê*�?_@�f�;U�UUժ�p@�\���a�J��jUU RC���SuXUU��
@*����q�F}�>�$�	���̩���aL2a���ͼ�]�����
�z��7s�fn���p��4����:P3���u�[7s� �f�[/��0�n��Hg���ˉ6̭��uP,T��[�����u3�@�R *�Su�[7s먍*�k���_�ʇ��~���߬��Y>�-h�$��f5U���*1�$��f%U�fA �7#�MIN�a�?l�q�����
�n͂ ���M�����n͂���w}��TVNu˥�~	�'<GJ� j_tWܺ8U�Sݲ� �O;��9T2�[�+}�h�n]\����8����E�PI�����z\�G��uu�o����:J#�p��$���/�,I�i:̭��u8�bܺ9Q���0�@znݜ���z�[ ���-ԭ��uP,���[w��0���HɃ[w�֬E0L��ڈ%����߬C0E/`dL����LћW��\���p�o���k}��	���$VqN�8���z8M����JΠX���N�a5級3 �p��TVtN+:�6"@p�s�dҪsZ��4Ӂ[/�갲sZ���>GK&�;�՝H�`�$-��fI�����hޮ�͒�uY����`I�]�%��_8��Dk�v�Kҗ%�Q to�lPMg'�H�t�uX��,I��t�SuX���n���
D��-Y4I_�� }����T��/K҃b�b��hɢI��$= �������(�}���Mw~]|󟷽���1p���̱���}/�]\ӛy����!����d������O`�vu���}�&,�?ŀI�N�aν�X`H�b2���0����[$�ws�s�}-CҺ�}̘�71^��uBߌ��C&��  ��S��mp�c��~��/ c��ǐI�XV���F�BW����!���#֖�F�.Hp��H�[bmMlD��,���j�YS�$�h�&�	�V�F$�됹��x��Gm�h�[N	<�~�m���3"�߇�-O'����`3�G�ӿ�FK[i��Ñ��#{K|�c��#�8��|do�/\L{�b
�!����#{K|�a�[#�.�D8����
������� �r�o�o��w2$����#�K|w�^X�N� �+G����}� C@W�$.�}u{�<Cҝ����#�K|cݾpb	�'@W�4���N���x���W�/[�[��.a/H��N�_��l�zđ�Y\�Y��k�׵�������ĕ��-�,|�e�H̏$��$n����J�����H��N�_���:�E�w�S��N��?���i$�GWv����>/�YZ���;�[|������q�����_i��Jcv@̏�T��q���;��_�úlm��U*G��쭴�p���ŷq�o�*r�҆�!�ߗ�D�.8p�~�8q���`[;+@p�^���V��k�n�wDl��    �p@�2���	��]�}]$	.&����Ĵl/ ݼ����f�����>F��8ܚ����Wv�X�e&�C�V�k}�2S��W��p��ʾ�4�ט.s�8�.]��]v@wí��BE��k�H�a>���BU��{�Hl����	�ҵ_�]dLFl�Ზ
5��o�2 1>&��P(I�~uw���Wňp��
����2 1?���}�5{yw�/������n�������p���j����$$���1V�8q�~����P�=&�j�������1#Vsq@��Ւ�������; b�jIH �m�@�������b|��ՒP趭��.����9��m[��]$�ǠX-�[��w�����w��bP���p�[�í�|iŠX����nۭ�bӫbR��q�0�n��H~W̊�v; ���������sĭ��u ��	�b�uDܺ�[�!@p�N ����:�����b�gDܺ�[�~5L���dn�̭�jS�[w��Q]��]$�}L��q�5{w���ϓ���Gu8��}��G~#��11~߹u��cX���ĉ�����.2.�c�ֳ8 b�n�u �cZ������q�� ���:��,����Hl�i���"1�[n��1-VWs@$�u˭��N��k�@Q]��]$�ǴX��f�����E�e������q��:�躠��p�[���q���<n��}�0���Hl�i�_ 9��[s�p@l�i���"n=̭��N�ki�@̭��u ���n9; ����:�ˈ�1-�rs@ĭ��u �cZ��y1���q�� ���܆/zݝ�wOh�Hǔp��`�n��H����cL8}\�fw�E$����pr�D�nQ�2F$�=��=��2��,"�H�g:��^f7���*��y:��^�ewsE$��
r�t�pz���*P�j�r�p��Ӿ;*�����ӗ��}MH\ �|�'~AҾ���x�ۉ=���N���",�S{���-@D2�
��vrup[c����s�$'�P�Ev�4h�$'�P�UvI� O����C<��$�<
JrMq�b�=�A�P����z�Ŗ���x %��8k���:��A��|0={�[O��ix�����}�(;�|�8�'�}k��ɀ��.�i=Q�[�"M$���p�N�R��7W.�c�DǮN�J��WE2 �>�KuZO�־�q1�D����5řΗ�O=D�us=q���݋�KŎ���\K��|�:��#���K�'����"� �uwB���_��ھbj�t��0�/[/" �p����e�Eć� ����z�Η��T��[��0�ϮH���?)�i=L��֋@b~L����L�+��o�
�z��8���e-�(�a�L�g�\����s'e���Z�j����z9���Z�j� ��z9���Z�j�p@l�������j�b�V �;���zX�U��
@*Ѡ�r;���Z�j��c�M�h��N�a�V�Z+ ��1sRo��#��}�w1�Dp�eI�''P��N/��cޤ&����}��}.�ǴI=�M�[�k��ˀ��N��Sz��W-		@*Ѡw��p�t�jIH�1��8��i=L竖� �=NjqZ���%!Q1 �uqZ���%!H����Z���t�jIH8 ���I-�)�ܺn�N+�Mju=q���p�0vZ1nR�k�3�n�u��
��i�zN���z�~����1mR�Sz�[7s먘ܺ9���u3�@��MjsZs�fn���p������: �D��N�an�̭иNjwZs�fnt�"@p���L��ק�
�z��8���έ�^r5����Z�L������cڤ��&�޵��e��7��	=L����I�����Sz���-�@b{�����u˭��!���I]N�a:_��: �D�^N�a:_��:*&W��I]�K��u˭�ޯ�^�)��z\��sUh��]O����np�7¸I�]K��������&�6ao���V�":���&-9����W^�Ne4̛������:*&W��IK�2���Hl��������z�[�*�@��N�an=̭��N8i�i=̭��u �c�e��L�&_AD%�w]�{�!v��[2���cib��R�B�w(�?����6^��u"	���6�Ć��!!	S��y:��l�[@R�r�tr[���m�7}�r�t�pv�{�W���g�f8��=�c��o�B� �,���@�k|�;�z�k��W���N�����z����,�h�^��9.������P��r;��:x62�� ��I��n�d��"�� �%9��:���#�� �ɒ��C��E$}�]��<�?����x���Ŗ�ϟ������Y�-z�}����Z�Ŗ.����x��6����T��s���	�d-���\���8��M�'k� ��0xX��C&f�T��u@̉��R���&�����J4p��6�����=
�uuZO�~�hZϟ��������8���pT��[7�g:_޽����*&Ks-q���k~��7ܺ9���|�z�*4p������^D ��(���I=L��֋@R$��9x���{�M�_�W�����e�E �h���i=L��֋����b���g�\������2]O��se�Z�sh��ӵę<W�񅣯Q����3y�X�����;)�	=���/l�����;)�)=��*Vk }�n����j�b�VTLn}Zk���Ѝ��z;���Z�j�p@%�Yn���Z�X���E
�Yn�֬�*Vk 1?fNjrMq6SY���`}L��B�q������G�&Kr-q6SY/눇�ʊi�����t�jIH R������UKB�}������UKB����I=N*���%!Hl���Z���t�jIH�1T��[��0��Z��E
�uqZ���%!Q1 �uuMq���p�pijżI��'�ܺ]��s}�n]]K��u����ôI�N�an�̭�*4p������:��ܺ9���u3�@]�����I�w��G���r����ڝ��ܺ�[ �h���i=̭��u8��Q����z�[7s� $�N�pMq���+�B��'�t��s� d�{p��Z�L�����i�:���t�n�uTLn=���t�n�u ��(���I=L��[�b{��s�S��[n�t�?�z9���|�r� ��z9���|�r�c�{p����u����'�vMq���:��;��0o�n�gn=�[}�-�۵ę[�k~}.�ǴI�]G���0�@��@�IN�an=̭��F�'9����0�:��'�8ԭ��u �c�e��0���Q1 h=�i=̭��u �c�e��0���ဘ'�8�T�ק�
����&�z������4��iڤ_����s�����3K���1��#��o�$�U��zL�z �jdi���&�zL�z�x�xC���4mүǴ�G �F���<�]��oPx9yZ;�4v����1 IN���3Oc��}�;���3Oc��}�����3O[�뱮�3n}�i��3�j��5~G���,mQ|��L���>�4v����=@mߑ��3Kc��k��8�iZ;�4v�����b{�im��Z3�Z3HXB���<�]��oH��<��y��m��٧*�G���<�]��oH̏<�y��m��~@a�uG�֎<�^��oH̏<�y��m���1?�~�i�e�1$�	�Z?25z�ۺ���_Z �Z?r5z�۲�٨*�l����m�oI\ �Z?�5z�۲ޞ3ڎ|����m�oI� 	[?6z�۲��	�dl=;g��}�C����#e���}�'@�֏��^��o�]�z@�֏��^��oH\ 9[?f��K��^�]l��1$܏!a�Rﲗz�^c?���K��^�e'�A�	�cN��Ի�^�$.�I�~L
ӗz���ː�0+܏Ya�Rﲗz��'��p?���K��^�eH���Ǵ0}�w�z[�zL�cZ��Ի�K�Hx´p?���K�k���$    �������e/�2$��b\������e/�2�!Hp�c^��Ի��E�e� ����a�RﲗzR���13L_�]�R�b{P	~Lӗz���ː�)��cl��Ի�^vB� �c��7}�7��K�G| cc��_��/� L��wj8=��3��`��wh�9��ί4�@b~��wh@�� U;�����Ό��_�?@�HH���xGƞsA�Lv�cbl�c8�7�=@�?I����������� �/ �>���;/�s��������q���&S�Jϕ�p��i8b�g���cTl����?#�G(¨����	S��u�cVl���?~���b#/w���H�2�[���1������@a|����)��b�LDb�q�����ب�����E
$,aXl�íɖS*W��\��Y�Q��!ٳ���1*6��p�Vv�8b{���VN��H���h]❦�b�u���~��P �u[�o�� ;��Ű���
���d�P�&��Fo�o�� }��f�[����P���������4n=�&[N�-�N��tbVl��p���U�p����v8��uG�8+#8p�YN��ny���Hl�a�1�"���8 �ǰؘ˝#������bceDl_-Z �=��Zq�q�B��N��³�ǵ�٠Ǽ�;�W;��뎳A�y�/	ֈ��:͇zL�T�A�:чzL�@�;!��T6�1m�# B�f7��}�4T�A�9هzL�@R�#��d6�1m�#�'���9#�n�[�8�j����Ǚ䷮��y�:�~�=�$��%��#!���L�[�����N�a��2�/H*����}��L�@b{�6���$�e�_On}N��־��Z��N�a��2�/�#��d&�-��������av��7�1 Uk���5ƙ[�7�1IC�������̯����3�m��8s��8*�z*��3��7�1$�k�S����\{� �N�`J��N�a��o�cHBƄ�Wc�s����5�$��0��7�1EC| ��=9凹���!�`X�����e7�Q�D��7̮1Nſt}J#�������?����L�����ޟ}�O�0-܋�Ӌ�柈�j�8ه����ߕ`�(�8݇^�cw�D$q̢��z��]�C�A��W��������$��Qzu���Ǯ��'�	0�ҏq�~��{��5p����Z���.I͎y��\_�:�^x�"@p�������ƻ��]W�:���#� ����}��{�^�70�һ�}���һxB�٘J��	?��m�]DR�>��C���E$�̥�����.*�L��`
}�w���I�8�t��\eS��zr������A��u�?U�	�)}��x��~8��s� ����A���/�j���i>A�{���$��\J_N�	����@�>�R�}; �����j:.��}���G�?�S)�v�OP���$,a(e�C)Tix���Uk�qIN�	������#)#��8k"�+��1�2�k��&r�Mdro��8�Ȯ;Κ���_8�ՠ�����$�lMdrk��0��N�a�_�&2��s`e��0�/[���90�2��}���{)���(�8هI~ٚ���΁Q�Q���O6�'\�90�2��}�䗭�L��D���Y�~�r�Dp��յ�Y�.W���0�2�뎇F�g}}�Zܺ9�'4%"�kbe4'��F�Ծ�� ���S}B#�2��O�D��uw�Oh$>B������N�	���:��V�[�倨����5p��d��H|���:����1�&O����#��������C���#���ɘ��������Z�zzłk>�Hl�ɓ1���$�j�:�'�^N�a�_�h�T��[/'�0ɯZ����=i���� �<�Åm�m�8�=���e��;�3����-���O�@*֠�X��æ��
�7�]A�U�����i��H�5�~��R�'��I�� oN�a������5h�7�����i��H_�@��9݇O���<��[w�����  �\�ywr��������;�~�?�4�u�/I�۹�4���ଯ�U��['�0�o�����
��p����i~Hl��߆�}��L����a�M��0�o���T��[O��0�o��Փ�#��t��-���Xq�-��0�o���\�^�3N�����'������̯���1s؎E�
������dP�M���>̳U`�_H�j�*�~�k+�����.Hh��N�a��H�I< c�=9�9�>���	l�ZIN�aޭH�I_�@o%9釹�"�/$q������&��yB�`��av�q��ٴp җ+�7̮1N� �q᨞چǪ��qо(�\/N��� �-@�Z/N��� �5@Ix�(J/N��� �=@I\ �(�:9��dEIC8xu���n�H�j�:��dW�D����I?�* �("�`o�3N��ڻ��}��\c�:x>����?R_~�����r���8up[%}�ޝ�C���E�.Hp���� ��'�N��n�"�
6p����/��W+���I?��m^D'�dJ�N������
6p����l.��� i�������l.>��� ��s���?k��m�	�{��8��O6i���
8�r���O�8 �,�R�r���O6i����K���T�I�H�t\n'����ds��)�/W��r;ه��'��@b}��t; *�d�}��'��Krn4�G�|�C߯@#1��8k"���s�>RFZ�����j5h#f�g�_����\�.bv����5����(#;чI~ٚ���΁i�Q���$�lMdr}��0�(N�a�_�&2��s`e'�0�/[�\�90�2��}�䗭�L���D���Y�.W����+��յ�Y�.�\|���ʨ��?�\�G�xus�q6_,ZG�䚘A�i>l.�3�w����hN�as�Ţu8 %fPFw���/���5p��d��G|����/W����}B�����(Kr�}g�/I�6�e��5�R@!�w��{U����l(c$٧� Y�ZZ�T��G|���}�_^��k�}z/ڗ"̬>�'ch¡�"��s�j�k9��+�\ZK�|��w�Ӻ ���y2$�>L��i]$�Y{XO�&ՇI~�?���5��&هI~�?��x�	�'C�߈}?.�~��OC�64�������G��44j����~\���)�H�F�^�j۱�F�&�OkU>�q����h�$�i�^�j��~���F�&�Kk�����_�&�ѥI���� �:�m��6���~��~��G�&�Ok�����_SV7A�&g����Ӻ ��Q�ب5J��.f�ѨIl��usZ�Y~4j�I�f����4ˏFMb�V��f@}���O�اu�����~���G�&�M���߿����	�4�mZ�����4k�>Mb���c��c���G�&�Q������_��vh�$6j�>��?�И@�ul��� t��߰8E�&�Q���ѽI/@��h�$6j�:=�;=j��h�$6j��zZ��HE�&�O���K���1q@�ئJ��u���G�&�K���i]���@�&�M��zP��݌�O�اJ��.@��h�$6j��z8�k���c�6(��Ӻ ��Q�بJ��.f�ѨIl���pZ�Y~4jM�$���~Jqf����&�7N��K�a�h&�7��pf����&�7���T�2��& R{qS���CX�C���>@cu`�a�a~��l_���"��"L�O��<���F�0	?}����T��5��I���+�Y~��5��I��#�����v&h=�$��ƹV8���k����g�>���WL�CXi��ޤ�Y{��4:��6����x�f1���,y��(��YL�CXi��ޤ�Y{��4Z�I���mҫ[f���#L�O���ՇYL�I���>@�:`��b\�,���� U  �S�dy��i�	����Z!ٛf���f������c�cL�Q���_�����0�i�
�T@��6�	��0�@T_A����0�@��N���f!��K�
eT�{L�i���������i��T@�����;d*��l }sP)��2��cqU����	\�a��� ��Q.2�u,
�A��cld��X����ʘ���=�ױ0T@��1�q����S��X�]lLx�d��X��6�ʘ�x��=Dر<T@]~�73nB)����q�;V������C�KD?��	#�c'{ξP\"=�m�������C��D}��
ɨ����=�ٱP�I��ov�D����Rݕ�
eL�z�c���j~���v���H;��
(YAT�v,���R��0&�b�X2*���QA@�lǢQ5VP��C��F?w����M��FHҗ��r��R��k<@�&���/��/�Ҳ��ؓ��Ɨ�Ǘ�|6�W��gfSy��N�8X\�i�̦���ʫ1q��8Җ�M�m��iX[i�̦��Sy9pl��I<aSy� ���~+�yi�zr����ǝI>aS������+p�ʙ�F��.@��p�ʙ�F�洮"��?I>a�nN�4��|Ҟ�M/};V����z+��ff�K��.g�[�Җ�M.}+�<@�+-����}*/���=\�r%�M-I��K��l������}*�"��[RO���}*/@s��VZ��ټ�}*/f+���-�'lZ�>� S=@��6�t�����[�i��h=��?��Z��ff���%lQa����̌�c�%s	�T�me�%3��pZ�Y{xme$���_f�a���9(��Ӻ ��ZKRO��Ӻ ���j+��F��.��,>��"I>a�N�d�h�I>a�N�*Bl
��h�5��+�h�Ѵgfҙl��g�a��=���t&[_��%Ş��L:�MW8��pp螴&��KgE����C�����>@md��GRO�t&.�UbSx8�H�	��ĥ�4k�I>aҙ�tV��-ܙ�&��KghV=�|¤3q� ���ág�3�&]7����蠰p�'��Y��A:����-3k�u�%s�ZT8���֤�7�h��������S=@�+�'�IWo�Ь=<z%��5��Mz����C����&]�I/Ҁ��uK�	k�՛�dm%Lڒ~ºt�.���b��Zf�F�(E�ul��f�Q9���-|��=-��$J�l�#���Wf�~�D)��<�rh�r�(Q@]~���2�@�DF�7K�B��БTF�7L��2�A��#�(��o�(���]���Ɖ���:T����R(#�*i�L��c[j�X;T�f�޹:��*���kZ,�[W�kWɘ s�j�+�{W~���r�w�PQ@-�[��byO
�{嗯�	��6�{Q��+�~U��	��=�(���_��P����Tz˯`Ue��бZ>��B�`�%�
ed�X-i�L/~ߺCTC�f�L�eJ�3P�D/N$,�ϴX�T��y�0&�ϴW�T�w�T(c� Ճ�ŋj�k����I
����*��?ID�T�7�T(c��꟤�P��;x�aP��������
e\PP�J*
������02(�~%����K������E�E��痢�M`��h��� 4V,v
��萻��r��R�����Ş�l,o��7��=��X�6_3� �AN��ffcy�t�cn�-�$�����X^U�	��ő�6�7�Ь=�qr&�������r�dl�?�ѱ��X^��폍ܙ�F��.@��0�I4�5J��.ڀ���'�'���i]���Z�/���R}8����lv�߱����Z_i��&����ss���WO���},/@����ɕ�6�t�Ь=��Ғ|�f��cy�L� �[�O��bϏ���A��6�t˫
1�@�����},/@&{��=�'lV�>�����{Z43Z�@�����0�IO{fF�h]R�8i��h=6�2��I=A둶̌��i]�f�a����F�ᴮ*��%�'���i]�L� �%�'���i]����$�0Z�u�ŇNb�栴N�d�h�I?a�N����ִhf�lK�aVѴgfڙ��o�3q���Ӛ�ig��������Ӗ�ig��Y90k��I<aڙ��'���áGRO�v&�� �=��8�|´3q��h���z�H�	��ĵ�4��F	�|�|����L l�Τ�0�L\;+@����L�fڙ�vV���ġgZ4�&]��2����=3k�5hg�_���¡��efM�n�d.��
�~�xt�&� ���áWROX��ޤWb��W�OX��ޤ s���W�OX��ޤ���0qh4q�0��Z��Z����&]�I/@���nI@a]�z�^��g� vK�eFl�qR�[� �{�,3j#��*�-�A�ˌ��H�̀�C{�P��DNzps�H
#��I�n����	(YA�F�|$����<�2`L��C����r�ZA���.IEaDc9O*C�L���0������ k�JZ-�Wz ����P]�f���ɜ�v�������H��;T���^��fs�L0&\X,�IB`��x�9)�-}`��'����M�<�a�|�������{��ʘаZ�w�H>'��'n���#�(�o@'����X-IEa/�x:)���c�|��2���^"'6�gޱ��r[!�N,�ϴX�T��������ʔ���
eL��'i(����z�>@�OQ(��E6�6��꟤�P���l*�1A@�`�`I��
'~�B8�@�+�(���:�
e\PP�J*
���І�eT��ߟ?���3L      �      x������ � �      �      x������ � �      �     x�]�ۍc!@�o�Y��hb+���X&"��N��=ƃ��g����?�/Ar�f5F's�f�9PN��4�]� ��� ���`;�Z����m���"f�n����1�4��c�T�=��2/��$o�1� ��#�D�&��A܄��<v�D@����K��{G���P����s>���+oF�+S�|��00�������Q#U��oЪ�"D�E6�U�E<|ر�|�o���!Y�� |�g
ɥ������Q��N�H��M�ԍ�Z#�ĩ��N!O��!�1��a�&x閰�1tٖD��Q^���e+��/���tb��tc�7,��@�#��H^��Uׁ~ԅ�\e����w�K^��5�趗�<N�D�>��b��_!���D�@�����2X(���B�B"u���a]6�^kBR�F�E��ix��)����`To/w�t�ܵ����St�j临\V֎Y�x���uAc���x�k*�\֡y���Լ�̆�$�۫}�(��	y�l���H�ˍ��4��rO�ײ�r�q=)���\6"d1o�厪c�]��j�`�ջ:������Nı���I��2�F;��u%� �q�EUE����*6�'�1��eS����v�4��v?]�5ψ&q�T�|�<MJ5D_����"�O���nsx��S:5j�@߄/�&�]���Kbu���N�|�&�K��� �B~d4ȍ�(Ք�Q��ƽ�.]©/�*���%awI+��G�g��˥������|>��@B��      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �   �  x���Mr1���0]!]!g�6�d��9yH1�Y�[�{�1& t�z����������cв0=�������h<����W����h�輖]	�@�׺=�	�,<��	K�0��WY��w]���$� �'�ir=�x΀;_p��� 9`���gN�;�"L�z�L`Āǽp���沐�ہ���fGtb��}�+y�����U#���hʧ�U����ӈ��z�����O+�'Jb�}VK4�p6��i&��'v��>��Њ�
��9KfV�'�D7�-K������x�̥ZvU2�Kcmݝ���	�f���,����K	]�M�`ĲP���������t�o2$k�G<�J��`�ٻ�X�>���?���}�»W���}�3i���&��Lj��g	����T*��9�{����p�:�j9n]EiXCx��F��e;b�Fl�j�Xw٣CϮ.��]��t���
�z�.Uc�A��a����R�m�c�;�Ŧ�pr��4#<o1H�od89Gl��k�*,�;���J�X�ӣ�Uٵ�E�����P=(:%�A��R��;G��'9�\��/��cۃ_�(9z��U��U�z��o<T�;�\�<*�d"��n7�g�!��P>Y�?��VY67��X������\��\����-ﲿ��y��TW���
��mH$C+�r�R��8���(��t�1��ף��t�rth���wΉڎ��c�sN�1�{���@U���=�&�qW��1�n׃4�r���ڝsV���w	�{��e�c��H�N��}�y�����<J��Nd0�{J������	�a�)S���f���*��ٮ��,p�[��P�D����o�o�f�}9���������{4      �      x������ � �      �   �  x���[�� D��Ō���r���	Nb#��Z]�q�fF����J)���	Ƿ���Y۩�_��=%e��u8���bqfI�o���
#���/o�Ts��B���g����p��y���N0�>_ǩv3�|2~V=��|e85�y��p�y��ꜯP���|�%��4o�)���m>%�/�|�6_��/�|�C>�����u)�|j-�gI��Z"��u���4x>���.�^\B�q�η����oo)�%I�B�q���"84��e�( �t���v��S�
!7���NQi��))�˗���Q 7.\���]��P���嫁�|�Q �\3���<�5X(`
F
l�5
���r�ͷ��H��._	��SG��|[](`J��ǿ�if�@��t{q��)(�-ɞD�Ɠ]��.�X���u
�]��S`��L_O1�-}���Fq�Wzg��M+:<���"���cL�@�&�w�y�J��ȗ����M��W�O=�j!�w���+ݦҾ�8�Lw|��N�G�~�ݎ���h�>��#��m*�e�1s�t�#%	@ی��
��藵���m;Rjh��2�@�
~�4��M/�@�J@ct%�+)h}���n
��e�T4m3�h����m{p3�@�
~ ڰ����~�9K-      �   �  x�u�[��*D�������r�?�� "0����q0!�֞�O�O
�?1>�%<�O����'�B	�>=?��/����xC�,��hPnH�%�P~��Rn��n���ʄ�~e}��便�������UYz�<D�t�n7�&�b�vɰL�a{�%�\�bX���)�7�����労}�
����:�t�l'�f�����#'����Ӳ�t�����;*Eޞ�U��������0]��K8��{�"�L���C��Eu��,WD�Nz$���w�隄��=0"�"=�$U�Y6e�_�劆��7����r�{]l [����9��/���#���5��fC�}[����Ҋ"�hE괢)m"���DE�D%�D	m"�2�2�B�L��+G�׀M�*]4Α���*�y�����4]�p>��l2��zYk�,W(�&g��b��L�&y$�5���� ��6GXD� ����m�����������On=���0]S�3���3�Ni��N�\��3��,Ϙ�
E���:������!��3D=c�魸�G���������,��1M��L�g(���1��=&����a[�}Ӳ]%�����i�Ξ�k2���3
�e}j�m��a���XV�O��@|�KÑ��x`\#9L�t�=_�/Hr@���[�k�O�U	�Ҭ3=��~��A�4|FF����̚tX�(��E(u�&�\Q�3փ^�;Ǉ隆��t�E� ѽg��cqO䍀⣩{��t@7�l����`b�������ڏ_�	%@[��`"D*!j���A!��@����l0�#!jv���B�L��A�,�+!jPa�vֳ�F�̄��R��3�����MU�l�I�S�RM�Uth��}w^�i��UT��/[5mSuA��jئ�����Pf�������P�1������m���Qe��y{bڦ�U�/��m�}e�6Y*�)��3!����&1巵a�PV:>~�_mMӇHPcم?bj��DL[{����kj�$Q�a�f�ؒ�>�f�>45�v���g���i�������ƞ�g^YOcB��5��{0��æ��V���GB��g���ג�>d������;d5��}I��A�~[w�7�tMG�����i���x�%���5`	5����է�E��E�7���VQ�^�o����U��nb4��
��ڑ�z$�g_eIj}[r��S�g�wLC��UPa�x˿	���R1{�����-RQ���~3'͉�+*�S��
�|v>��S��CPa'���dX�h��ĘЋ"q�*�$��x�y���h��a��(��O�5�f#�VTX[��a�r�S*lJ����R@����:~X�h�������MF����)}MD��${�)V���E,�Ћ"q�0�$��x�yՆFfc�a�������}��"]\����GT䛢��\J5��UhH�h7���_��B/�$G��+�p�f.�vԀ/��T�yR�I�階�t�p9H���P��EM�j��m��� �iWM��wMC�^5)տ�H�Q/���Q�������:�k>�ͷ#�����ȮN�z?Yj>��ߓ���\O��#��4��ϑHK����H���b�泚~�r-T���r�R����OX�B��KXҜ���w/(GU���\��*r�o"��Y�/W�r����1�*����Iv
g������2mW�\a��g,�:M������3��h��s_5T�\�C�P4]X�p����}%��t<?F���~!"�[�S���vUF�X�%�UA��k
�E��"��Hj�i�+��1�Y�s�yG������\_g��_��)�_�D��&��~8M�DP:R@rv����`(7��C��wx�vU����g��Q��kn{�!�Qy�\r^��,Wd�d<����V
n�-��ʢ}��KťM=hX%ұ,J�B�-dے�9�lSU
O~�5��0]CaE���X�-��
��=�]�b7���k�Σ�lb������_�/D��H�#��8b�k�[�˔�����	C5L�@'f�!�#������K�7[�I�W7\ ��%)��;�tQ��B}(�F�>��T�a�,�%|3��M[����a���&�̫!��0]�w�f�K	v����G���������A�K	~+!���"��@}�k	��NB�8��q�O�G�����]F��8<��>?M��,��N'*��ռ�<,��A�!�ث�ZL�g��I����7]D���FtE�Q��)���W�i��b��+���nEf��a��KJ�x'�K<Z(_��B����x�P�\tX�_.:,$�r�a!���Y��k�Q���b�r3 ��E�����B�^��^.�M��ԑ�P:ʓ��(��GqP9��ꑆ��,����Bg��ځ~��ʑf�ߗ#)r$q@��ȑ,9 ���E�����Z9q޿���\ �S ��I �	 ��)
�D��2�"�K
e�ty߹v.����MZ�TH��H��p

��(�򽼳Eό�&��D(H"��PP$B��B"�{g��U�Х-"tU�]�"BW��(]�"B��Ѕ,"�ĳ]ȪH��zE��^�+WD��S}��,B���ﵜE�ؐP��؈PI����P����D��E$~��,�8�"�]�+XD
��@*��@��@�7s^��Z!I�9/9s�8[B���dΑ�΍�TΊ��o.�H���7�D�G�(]�ޅ��z�#�T�<P�d��Kv�P?�[@zd��zB�.����y+�|d���%O]�^�ԅڑ��G~
H��t�.��B��D�#$G�	(_2΅�%�\�^2΅g�@:g�@��[NR���$��t��Y"�uX"��t���/R�M������D�I��D"�䁤o%~�7%����E�� H�r@��D��(�-��������"�-��|k�N�������<�W�      �      x���۲�)ʭ�g�KE���������;�u�zjv��)��R�/���rj~��b�����?�?���i��/Z�2-����;������z����_�pd8������:����U,L��5������kc���M�'��������;��o��-^�w��lke������V���`���"Y+[Wp��Y+�Wp��X+{|W0�I׿h���]�M�)��e��Sqpn����0�l��L<a��,^�G��o���k��JYky}d���Xk{}d�c]��ͽ>�<*��Uke��l���,�^�����V���#k��_�V���#���/X+�{Wp��[+{xWp��/Z+{���Mo�q{k�m�n�� N�[�3\-���
��g�?F�����e{�G���+{�Ag���{�A��^h�V�����]{�����ڳ�t��'t`o=��.���]"{�A�w7��[��+���ުt��
�<���z��]�M�.U��f�*m����@��H�~��$���gް߁��w�788��n��#�>6}���F�>6}љ������A����\��k�/_�E7�}|m�98�}|m&=.�`��]��~���v�	&���=>Ȇ�>��2\-��w��a�qo� ��G��}8��wБ�b����A��^���Y��-���;�n�O辿�Ǌ$k$/߸����Q`����^ׂ������^7�W���k���:Y:�W����!k�ơ譍��.���_u��{]dӍ弄����t�{���禯c�#׺_��{ў弄��{с弄��O'�OX+�7~Ӊ������o:�ay|���%[���ay���K��ʇ�A��/٢��>�rѝ��J��岒�ke�^n�s�t����M�@�����7����]8��0�4B�	��	w���wA��GԖ��,�.�pd�+<��sA���hi.����-����v��\8�����s�N:��i.�t~p�\8���`���I�w7���n�]�MsA�㻂��+j8]�|l��:t,�>�}�}E�F��m��8����Ǯ/����Ǘf�ȸ�5���o͢�N�}|m�v�p�{�o�ȈU����w�6]��t��]�M�}9�tyWp�m_N'��\>2�`�l�]�E��9X+[xWp�~_N'�>�7}�r�����^��c/X�V�+���r�*�.��U��]�9��ʄ�hU82-�G��ba���J����z�*�����|$:���4�4f��֣U�h�Oh=Z���7i=Z���
nZ�V�����֣Uh��ܴ�J�wC�4�ZO:�+����:`�F��[�����pq|�;k���b��k��� V:�n:�A�ty}d�Ib���#ˣJ��Xwݽ>��"�����Ms�����#�nr+]>|dzT�r+�>|d���A���ܴ��X���-��W��J��o٢w�uܮ�2AQay>Ȃw���;��9ʈ�'�k�<Ίr�w���#���\k=��t�4�ZOZLB���\k=i�1�q������'4�ZO:����b7���t~p�\k=���হ�z��]�Ms����W0�Is���㻂������t�a��������Y���v���\j����p�a��^79l8���"ӡ��cM��E�9l86�}�Ȣ�>\dѕÆ�N_��؍Æ�._�Ew�}|Ɉ.TܱV&���t�B�ke
�
n:p�p��]�M���:���{��ԕ
�G�w����hU8{����h=��p����]>>G�����>>Ȣ;{��%0���M'����NL'K{�����],أ��[33�B?�eC��f̚l:�Gtx�dә=�ӻ;�.�tyWpF��N�*�O����3�	��///���.Hv��,8쟱N8�;���3�	����t� Q�A��h�Z�~|�EK�z���c/Z��á�Ǘlх�������Y�Վ����ߦG�_��t�����=.�`פ����:<5X+��{Ӂ�����zo:r�p���<��jg�ڎҋ	���|�g�qn��b��1�a�eL�r��pb8Z�I��ta:[�K����X��ĸBw�1��Z�H�[#{��'t�W�d�O�(1���]@?W{\��Z�ۻ����2M�����.�*��t�W���`(�n�*]��t�W��Z#���W�nm�s��N⭉�q���ˣr�W����$1���u�Mg�q��.2*�q�E���,�J��t�p�E7�q��.��.1����K6W{\�Z��Ǘl�^b\u(��%[t�W����r?z��-���t��뻂��۵S��y��_�{�e����D�������ڥn�>��p�pۮ}���l�]��������Z�� �ܘ�z�������������_^�c?�8��p��~�q��������Ƕ,���I��}Y4?�8��?6f�������,��t��]??��X+C}pӞ弄��
n:�t��
�8����A�w7��^:����3�K]��t����}8�\oz�c��!|xբ���5�+�� >��ɢGd�w2�wM6�� >�Ľk����A�׫�
��%X+czWpӉ�.�
n:�$�V7��j>It�.�0Q:y����>M:
],��qr�Y轗ޭ�եw>O�
o�R��ڙ��lq�'��Y�|�x��8����K�[Ss�˨x�C���]G�3�*�?r>���ǚZ��Bn��r��c!7��`9�����|�x�X��,�R��:o����Uz�c]�YC�;28�`�T:��ࠓ5S�c��.V.�硷f���.������.���Ç�,��r�x�p��ˑ{���]��=���.�C�p��.ӻ���>r�r�x�Xȍ˹{���{7}���ZSK���-�lW+'��_؏��]]i�I�?�|�m��AG�����ֳ��]�������4t:[ڳ���Y-��ojg|���tZC[|Q�Į~��YE����׏e��k䇓'�?�q��r����Xȍ�����q~;y��c!�Ś���׏��8�+Ǧ�磳7&NX:<�\h�X:}|��0�S��.�{Ӓ�x�p��K�r��r��/I��_�pI[<}9��%o9��� ϒ�x��&�Uϒ�྾I������7iᒻx�X��^Y��/�q�^�},����͍6xҳjj>����zƒw��+)��ރ�BGK��{�Y�l���AW�����=p53(�'.��^�L����㳈���{��cg2U��^?�q�r�x�XǍ���x��q9x<~,�L�j������B.���{��c!7�%�P�,�
Kk�\P}�=���χӆ_3�&��N_��̹����_]$T�}}�{�*��:����-�I.�x���-�K.�x���M�.k�/˸܋JF�T�>�q�ArAu �����*>r�U���!},�Ƴ䂊����x�\P�����_���H��K��6�tF*u>60Ra��~��a��P;���T�0�Tj�͗�F*���KC#��g��MC��g󥡑
C�����H����|ih��P�l�44Rahn6_�4Tah� �Z	U�8(���0�� �8 Rah�A���
C��̶������T��UC#��NkC#���[KC#���[K������Q�Ϸ��F*mF�F*�Ϸ��F*�Ϸ��F*�Ϸ�ƣܻ���o-�T��o-�T��G��1Vah�Y�0����
C��Ua���֋ C#���9C#��^�0���[��*-�V��
CU�Ui���BoU�0���[�F*-:�V��
C�^�Ui��Ш(d]
�0��kn`���b�@@7�0��9P8��yÅ��
C�U��
C�M��
C�]�F���(}�FB���J#���J#���J#����PH���,����]��~� h�>�v6���R��Oa���R��Oa���R��O`������F*-{9��F*�~K�6B�F��Y#�
C�� �Fu#���F*m�5��0�����0Rah��W]i����k"TahT�6B�Fk$T    ah%ʽ�4Rah%ɽ�4Rah%˽�4Rah�Ƚ�4Rah�ʽ�4Rah�ɽ$4Tah�K�4Rah�[?�*�z9,�F*�9,�F*�F9,�F*�&9,�J���j��Ii���j��Ii���*Z�O}�
C�\8`���*Xz��
Ck\P�0���F*�IAࠑ
CkR8h��К���Z
JC�֤ p�H��5)4RahM
�TZ���A�wݥ �4Tah]
�T=�.�TZ�;j8`���z�Qñ�H���g�F*���0�^9j8h���z�᠑
C띣�c�
Cw�/��F*�y���0t�r:h���]��頑
Cw�/�ã�
Cw�/��F*����0t'GkL,ـT���Ua��Н�
w��н�C���hU�0t�G��H��{=Z�F*��Ѫt����ɻף�i��н�J#����H��{=Z�F*��ѪtWp�A�V��
Cz�*�Tz�Z�A#���z8 Ra�k��Tz�Z��H����0Ra�Aj��Tz�Z�A#���z�H��G����Tz�Z�A#���z�H��G��4Ra�Qj��Tz�Z��QH��G��4Ra�Qj��Tz�Z��܆�
C�\k=`����Z�*=q����
CORk=h��Г�Z�0�$�փF*=I�Ui��Г�Z�0�$�փF*=I����
CORk=h��г�Z��*=K����
C_�̆F*��;��
C�����
C�y�Ƕ#�>���Tz�;l8`���s�᠑
C��5�0t*�X�
C��5�0t*�X+�
C�N/k%Ta�TܱVB�N�k%Ta�TܱVB�N�k%Ta腏��{�0Ra腏�F*��Ѫ0Ta蕏�F*�jت4Ra�U�V��
C�IQ��
C�YQ��
C�EQ��
C�UQ��
C�MQ��
C�]Q��
CoNQ��
Co^Q��
Co�3��RH��7�뀑
	���H��7�뀑
Co�3�#��4jU�0��Q��H��7�Z�F*���z8Ra��K��4Ra�=H��4Ra�=J��4Ra�=I��4Ra�=K��4Ra�H��ΊTz��*�Tzo�*�T�|0�Y��0d:ד��
À=�
�>�*���J�A'�m��
à����H�a�E�Ui��0�*ު4PatoU�0���*T��N��i��0h/ު4PatoU�0:��*T�?I�
T�?I0Pa0�$y8	Pa0�$y�@�!�?�*��$�0�l�I��4Pa��$y�Pa��$y�@�a�Q"��
à�D$J�Ag�H�*�.��C�AW�H�*�n�(T�%"Q�0dGŝ>�/-�0�s�&0Rap��Ma��0����@�a���7��
À��oJ�A�C�c9�
À����
À�����C�*٭~WC�A�C��*���J#�A�C��*�NR{Q�0:�t�@�a�Ej/J�AW���Tݤ��4Pat�ڋ�@�!��Ek%Ra��b�Z	T�ؠ4Pat�b��@�a�I�J�Ag)6(T]�ؠkT]�ؠ4Pat�b��@�a�]�J��H�g��%�Ra���Dh��0�����H�aБ���
à��'�#�<�~��	�0�ȉ"8Tax�#Eq��0�&g��H�a�]ő
Cv�ɩ�8Ra��cEp��0� ��H�a�Qő
����,�#��g��y�
à��0�rd�4Rat��@i��0�α��H�!���kp��0p=ou�݇�l\\ő
����U�0\�\ő
����U�0\]u��.ӻ����#��뱫8Ra����#��h4�5�0�sv��H�aЁ�;��
à#�wJ#���Oi��0����H�a�ER<ő
����x�C��7I�G*��)�T�'Ě
U.'�0\^N*U.O'�0\�N8Ra�<�<p��0p�W�Mu@�aМ�4Ra4g,�T�)�A#���,�T.I˱�����x�Z�0\ҖG*�����
��%q9p��0p�\p_ߤ�K�r�H�a���8Ra�$/�T���8Ra8�}�j�~�0�^��
à��=h��0h>x�0�ރF*������
����U�0\�G*�����
C�Q�G*�����
����U�0\�G*�����
����=p��0py�~�H����p.�>��
à�J#�Aw��F*�'ǹ��H�a�^rAő
����� ��k��(���H�a�IrAő
���䂊#��>��0�J.��>r�MrAő
C���P�!��$T�0�K.�8Ra���z�7�Z�`(�0��������߬/H0v�d}�H�a����]}����./�/xu]^0P_ȞR+c_�깼`��0��ry�@za�����W����~�F�.�,�` �0�9�톁�������`�����ٯ�?�^�oɅ���?��U^0\�zVy�@oa��U����G��V��/������ד�Z^/*/H-x=��`���}[�)/'r��mx=��`��0������vOz�X��0ش�SY �0ؼݓY��0ز�SY ��4.�������@`a���Sa���=m���+�@RvO������@\a���Sa����L0"i�gvO������@Xa�u���E@Wa�m���Y���}���U�Lӳ�a���`��ە�
�|�+$�nW(*8�ݮ���nW�)��ݮ0�Sp�]a��0��w��@La���v�!����w|�+����PRl�XX �0��7��@Ga��o`f���`������"
�r4��H(�s�.kr���a���`=�����������F؅�	�M���%`h'�H�.0�Np���a��0�&��@8a�]""��nB�ID$0�M���H`��0� ��@4a�Q""��f�DDɄg�u�c ń�u�	�����$��%�K�#�������{�uZ	�}Vl����rڮ,PJ,������r�.,�I,������Jڮ0PI���
��Kڮ0�H�!K�.0�H���
��Kڮ0H���
}�Kڮ0�G���#u�Kڮ0Gp�q�z�Fl�q��@a�}��}@!��v�,F��8@a��0��q��@a��� ��*�[s��D���Qh"��0�Dp�{Ga��0�����@a���u"���Cu|�(����Q�!��� �C,��-����l��B,��"%����,B��
���0�A��
w�l��;�&�'�Pa�rx*4,���@a�rx*,����]���)0�?��
��K�Sa�~0`�y����r�SY�}�C皧�5�>,�<�������@�`�R�T�Xj�
كK�S��Xj�
уK�Sa�y0`�y*$,5O���A�Nj��D@�`�R�T�Xj�
��s͓�@���r�SY v0X�y
��h\�vOe���`��0P:��<B����@� G/5O�����R�T�Xj�
��K�Sa�q0`�y*$,5O�������08pe�T���@@]���@@Y�n�#-��k n0X�e���`�����(��8P8s �;t\8P��Q9P��q �05p�@@a�i0�=��! i0`ρ��@�`���������,[���Oe���`��65����,3���-�k�)0�2�Sü�0R2�$��K4)0�1p�hR` c0�(�$�H�`�I�I������D���&y�/
�������t��~����#e�|�`�#e�z�`5��X#O��v��5�HXO�!�\0�"a��@�`�U�T��n�����قw	S��v�
D�%L���
$%L(8q���S    
�Y��X�W@�O�a�\=�j�Ejq5<�0+�ﮌuP�`�l1�A���J���
�;c *�7]�u
��L0B�z n�J��	��H��^��f`�Q@�q�30�(XO�n�D
�t'	�+�l��+�jX$O@ֺa�:Al��H�`>n3�Am��zȘ�	�K8cT&�O���P����c �%�wI�e	�]�Ƈ�*=b
��H���n`�I@/��70�$X�����n)�c�x�P� β��"=z�R����U7,R#�G0���z�u/�"�3��]$E@﹊a�Ar�TBY$D@O����0�!HN�J(�d蝘7�Az��}P���yc � �o|��P�`�@3B�� ��s5c ������P~�޶Ec THk&�m Hk"�#�z����J�5�����@Z��o	�5��^�;���F�iM!�a�:���F���j;�V���9@/}�cɁ4�7�]P30��Z|n���7&&(7@���1C�zv䍑Xl�ށyc%��WJ�X����X0Vb�z���Xh�^�c%��7P�X�e�4c%V�'S�xkC"��Խ�i�#�l`(1@﫪��� �(������ �?��D,/@o�����s%o������+�� �n
�J,-@/Â�+�c�`�����,+�� ��
�J,+@�����
�9b|F�eG�XT �	��5�0~�PR �*�CE�4ǋ�0Hk��MC=����_4�Hk��MC5��F��4HE^*�R�'�JC)�T�M��XI yT�4HE^*uR�g�JCz��V:�"���0�n��PC��5C	z�.�R��Iޘ����76b� z�䍑X>��yc%V��O�X���m^0Vb� z*��X:���c%V��U�X���mU4V"݀�̦���@/g7+w�uY��mG0=�4pg8[�6���7����Gp��O:3�M�-ݘ���{��AsC�~��w_�S��Љ�b��ǟt�֯��5�狧��ָ��'��8����
�Á��Ó�˻����f'�$�N�gKf�ݵ"'L�3�wA�֬/��˻ ������� kk�T?5[�����:�.���o�������qf�ƿ.���x���å�Fv�v�F���co:�Df��e�	S����5qnMZ������2�f��k�\l*�y�R-�N��HA���Ƿ�Oz��1t���,z>B4[^'��f���$mҼ�y�0L������X���/�n<�y#83\�e#�1���D/�4w���-yЉi�����Fte:YZ6Ri���Z6�㻀��9�5��[���<�Ͻ6���;��k��<��� w�A��ĵ5A��c���$��;���	rGt�p�E�yl�pmM�;�ӻ��.�nd�F�FR�`�Ͽ6��n�c�����η�A׏��\����;��}|���;���=Xt�[�ˇ����o���N��ķޱ5�]��5T�#ؗ��4��Av����5'�W�rܫ�ښ�H���ޚ��L{�����5'ݘN�^��o�[uk��{kN:1�����J���:�9h�;p�vJ; 9������ �Z#y#S���#��Ƶ7��ُ�k�۾�N��&����;�����)�f;|8�\���=v���I�=v���I�=vn�p�vN; 9�������!�%፜�fn��O��b�wA6���w��]�M�}�t���̍,n�z�b��o͢���N:~���z=e0��Ztܷ�I�w�j��o�s#û���;3<��� �q�"�����v� �lj30(�>߃�0*D�x����AA �ī��'
���M��@��Wk�O�x,
�&^�m?hT�4�jm�I��@�#��N��@��W�c��@�}Ϭ6(�>_�b_�cC��@�3�����@�3����@��W�: *D�y���@��W�: *ľ^"ͼ�"����"M��b_��
�/�c�|� i���.�
�&^�R� �h�U�.�
�f^�R� �h�U�.�
��^��,s���AA 9�)0,$y�-
�9�ȃ�D��F*�
�9�ȃ�4'^yK��@����QA ͉W�Ҡ �h╷[
�^�ρ
�^ُ�
�^�;�AA ��+-
i�a=
Ҵĺ*$�y�K��@��W��*$�y�K��@rs��F��@��W�s��@��W�c��@rK��Р ��T+��
�^y�R� ���14($x�K��@��W��*$y��&��7
�F^e��@��W��� ���+C��@��W{kͼ�[�4*$z���AA �ԫ�5
��^��Q����A��@r336{
��FS�~� ��|=d`PH4��[�B�D3���(TH4��[�B�4g^Y�B�4g^Y�B�D3��u)TH4�*Z�B�4g^ٍ�D#��F��f�YlPHn=44($x����@��W��*$x���@r���Р ���+뀨 �h�U��
�^E��� �h�Ղ;����4�A����0 �s��0k��X����LG���I�sЍ�bi	v�f�)��)�C�;��Ά��t}�/�IK��4K���i	v:�+�iv�����k�+;�����s�s���E�bW	v�|�Ȣ%�9���$��`�X��:��	v:�N��&��A�w��b7	v����n:��z���I���� �}|�޴�g?G�_��?�1J�p��ح��z���G]�F=�Ͻ&��n|�t|�dӝoԃ.��,�;�Q��뷶�*;֥ZxpӁoԃ�N27�8��o=��,x�'��|~����Y$H?Ɣ�!��֓�LgK��zҍi/����UiQ�ҍ��֓NLכ�QV�h=�j�o���G�A�F�.��|��t|W0�Is���˻��޵�s'�]�����Z��ή����z��]���4��[���]�Ms����� {k��znMxdўk�'�_�Z[��z��u�Ms�����Ԧ��z��å�F�]�;7�Y#yo�.ѝ��_�j�]�;����a.ѝt}M�4����s��{k�Dw��u�E.ѝt�����\�;���Y4��έ	N2�&p����,�72��	�u�����
����e�y#83\-,yЍ�(�_"�4�V��-yЉ��\�(yЕ�li�H�Y��\��&-y��]�M�FtyWp���~l_����g?up����~���p���˻ kk�6���� ��|�[�Y�R�1t�p��"��z��å�F=�ƿ����XL�]�M�yld�F�F&�#���S�+5n&�?���ѽ/�ǜ�u��0�G�[50��G7~70���4)�?Fu���.�DB�yMxc#��GW�7F�yrzc%��G�J0V������f"��t<
g��ǣ((+�l<�j���ƣP9+�d�<�7ޡ�xt�'ùx�'4/O��S�(<��e�����߻c�nҰ,4�G��7F�x�:xc%��g�����n:Hò��X�MGiXNãh6+�0<
����£�'+�(<���k�S���G����n����6���l6l��g'G�婒q�h��G�Q�up)Pzc�G��7��Qx�xc �Gʫ�G�Q��pE>�G�f0�Qxc �G!U4�QxyIG^.�F��%y��2�d�t����d�t�ņ׸�D+>����᥻{�h^^O�o��~��tw��v�_,'JKw������o8�~��٠p�h���G�n0�Qx��c �GҤ�G�R�/�ݯ�Qx�c��Fᑪd�X<
�4O�a��}�)�{p���?��v����8o��F��H����(<�^��@8
��-�1��#��`���H	3}�n��m�T����7����m8����b?2{Q��n?X4
��e��>4
o	l�lx?�f˺�6�;�ٺ��.�No������(<���<8
��m����#�Oo���H
7�(<��@����,8����!4
��C�1    ��#��}���Y4
/ϟ_o��#a�bX~-�aOH[ʏGᑈ)���Qx$�upI�zc�G����G��y��k�F�e4,���o^7�F�Xe5���#��nX4
//!�F��H����(<���<8
/������H1�����H�5����H^3����H)6����H�3�K�����ID�p��no�KD$0�G����G*����G�����G�����G���FF��H?4�(<��@8
��F��Pyl�Gb�Ѱh^q��va�aA>0�����,���;o��#So����H����(��\�w�lg�����H ��(<��@8
��T�1��#��`���H�2�(<R'�@8
����"4
��L�a�(�2���>�z��Sa�(<���TG`4
��B��Qx���yp��zc�G�G��j0�Qx����p�c �WV7��D�]�gIuF��H?4�(<R���:o��+QOa�(<&͆���&9<����Hô����88
o���(�� ĩ��8�m�7�&���(<R��hIS�h`4
�DWC6p�n�YO��(<h�@8
�$/�1��#1�`\��#u�dX4
�d_��k4
�t4�a�(�-{�hޔ�4��QxSL֘G�⴨�[�F�M��f`4
��<�30�G���``4
�d?C20�G���G�Dh0�Qx$~��pɉ.��zƣ�Jᚧ���,b? 1�r�p	�6âQxS���"��:8
o�D��(��hj샣𦢤1��#��`���H}2�(<�J�@8
��*�1��#Y�`���H�2�(�)�j\��#�hX4
��p��k4
��2�a�(<�u�E����QxS�XG�MNc�7�b�}p��4�Qx�,��p^YZ�7�F�
m0�Qx$��p)�c �W:��r�h��J�E���9J�ٌF�UǇ��h�|���F��GY��h�|�Ro�/����(<z�g����W0��£�3ۉ����iX0�Qx��&�(<zF��p�|�c��£gf-�(���#�k4
�z��HY4
�z��HY4
o�bsF�����F���7�僣��"�p^]��7�F���F�
G��K�`V�£�I���G��Y8
��1c �G/�1�£'O�G��k>c�W����Qxum7�F���IG��_���jL�/�Qx�:�yp^��qO8
����'�WW���Qx5��=�(���uO<
�&o��«�ڥ�«)Z�ģ�jJ�X���j��X���j*�X���j��X���jj�X���j�O� G��잠�«�?'-�Wsx"8
��cc �Wi$�1�«�F��WGpe"<
��c�j`4
��c�n`4
����Dx��_lg�«ų+3G��ؕ�E���o�«%�+�F�ՒŕF��j)|��B�Qx�T�r��Qx�4�r�E��j�|��F������«�˕#0�W+?�P�«5ʕ#0�W��b���*=t1�Qx���(�J5c �W��b���*�\��p^�!��@8
��c�僣�*=v1�>8
�R��|��(�J�]���«4��|��(�:�`���*j0�QxuW�G�Uj�7�Qx4i�s~�«ݛ8��(�ڃ	T�«�b�a8
��dC���j�6V�1
��b����j�6Z�1
��f����j�6^�1
������+8�%�x�x^��/�J<
���Ĭx^s�	Z�(�沉Z��k�����(�6»�F�5�L��c^�w��Qx�;���׼���Qx�����|���Qx�'���׼��B��t��Qx��'�ţ�oO�G�5ߟ��k��k�X<
�Q]�>��(��c�(�F�g��Fᵐl �G���0�ז�M���^��O��G��1��k�ע�.�Qx-�'^���Z�6`���=�1V�Qx-�'d���ZLÒGᵘ����k��4�(��T��k��5�(��d��k��6�(���t��k��7�(��ބ��k��8�(��ޔ��k��9�(��ޤ��k��:�(��޴��k��;�(<��h�a���� -����a���hU8"a���hU
��G��P�e9Z��� -��*4hY�V��W����hU
�"G��P�9Z��� ���*4hE�V���
Τ�9Z��� ��Ѫ4h��l4h4K�l<h��Ca�V�>��� ��}+�Z�|+�Z|�Ca�F���4�Z��X�Q d��� ��9�J��s��X�QA�X��Ut��X��3c%h��A�4h��w����uڔB�P$���%�D�*@k���� ��߁O4M�����E� �����E� �����E� ������z �������ݩl����"5��W���"1��W���"-��W���B)���l^l|�lŚ4b�
��p�t2��ֻ�E2 }=�97� ��j�D�@w����"��╋E ݭ��'� �[���E ݭ׆'����[�O��w���,���n=5<Y����zix����~=4<Y��O���v��قNh�E��}Ω�P��ߧ�������GAQ���dN5��uG^,������ɢ���#�����(,j��Ԗt������y��ݿ���(,���!�?2��{(�¢^�꾮�qP��}��:�;����F�ݾ��Ͽ�_�.�����u�wzo5������-C=�=f���ۻ��׵��ÿ��׵������׵������׵�j����u-lx�l����讐��~RCI7�z��|�}�������1Fag_�H],j���1&,������ml���1&,���ﯯ�j����BQO�~}E-�=����ϊ:�{���+(j��9��WX������0���sᯯ����S	�66����6�����6�����6u����65�w*��.�P/�ofa/��aQ/��<������G�������G�������G�������G�؆�{u�����{������{��oMؽO�@㍢��^9���1�'
[�ih�QԹ߫$�¢��9��6���韷e�m�f��4صO�?�mlڟs?o�`�>M��m�e���m��4�3ܶ��}��n�`�>���v}���nu��d�75�����BQ�>��7���U�F|��0ة�{�]XԨO�=�m�ӧ������p�p���i�g�m�M�4�3ܶ�}��n�P�~�S=o�P�~����5��蹾j�M?����y��=�8�ǣ���;��$�z��9��9�
z�sr<
Z�˜�y�:�˜�y[������i�/���N�o��9���,��/4�3ěM��w�|��]�����,j�/4�3ܶ���BC;�mj�/�?����%+������G�`Ѝ_hRg�QЌ_hL�>J�����=
Z�����e��ЈN_����eN�l7�����fA~��!�,��/s6g�YЄ_�d��6ԃ_�\��6Ԃ_h*g�mC���]{�[�6�w�QQ�_\ܵGAQ�}qq����E�=*�\{T�����,h�/.q�QX�y_\�ڣ���ĵGeA�}q�k�ʂ�����]��%�=
���K\{T���epO����u�������f*{��ᾐ4e�7��7{�/�����ot����o�P�}�A��6�ڗ)�q[�Z�˜�y��:�˜�yۆ�M��m�Ͼ����m�;�隗ۀ.�2gk޶�&�B#��m�/4Ws}�<��ž���
:���,��M�l7
��+>*��+>*����Y/5����FX�[_������2�hޟu֗9B3_,j�/4A3ܶ���B4�mj�/4?3޶���2�n�M��Fgޫ�z��ͼ���y�A��&f�K����ܷ�����аL�P;}�Y�����n�B�2��
����I����җ9'��Z�V�BS2ý������:�F��'d^,�/s>�mj�/4    3ܶ�.�B�1�mj�/4�6���y[�Z��żC��Ͷ��eļ�B��ŭ�����4������4�����4��z���4��Z���yۆ:�����6��y�#�/4��G�6_h��폰k��闷?���B�/�c���9vQ�|������:�����a�иK�~�B�.M����4G)�/s��mj�/s��m�/4��D�U��)��m�S����v�(_��ˋ��-���fA�|��-�͂.�B�-�M���O��l�o���jYot�?��/4��g�o�<�/s���=��(�t�U�_|܏��X���FX��DX�_h���C��e��-C��e���MC}�e��mCm�FW��6�_��ʋM�e���mC=�e��mC-�e���mC�&V��6�?��G����[�(����Q*,�/�o��]C����o����z���(�u ���|�
:��(4�6�Q*,�l�#M�?�r���~���5q�`�l���vm��qz:n��{o��p���Gq���'r���gs���t���Wu���7v���ww���q��;��}��w�����w�6ޱ'nzl��O��>�l�O��>�b�O��>�j�O��>�fP��>�n"P��>gBP��>`obP��>�`�P��>�h�P��>�d�P��>�l�P��>�bQ��>�j"Q��>�fBQ��>�nbQ��>B)g�Q��>Xo�Q��>�`���^���|¨�}�� ��}��F ��}�ņ ��}��� ��}����}��F��}�TΆ��}�6G��탵I�k��`[�`m� ����	��}�O� {��d
��}�O� ;���
��}d'O� ���d��}�O� ������}�O� {��d��}�|x�u�R��'��ˇ����}�|x
���G�����}�rx
���,�'ð�}�rx
�Z�,�����}�rx
��,����}�rx2��,����{}�rx
���G�/����w}��\YaԺ>�OZ�m:��I+,j\l�'���o}�y��¢���>iF]��|��V��/Ԃ�����w>iF-�#�w|�
�:����5�8�I+[�>�nÑOZ�Q������n�g>iF��.|�
�z�<a����AlV����bA��`�o���K�?�^,�W����bA��`W������-�^=pz����N5�xu�]0�Z�j��`ж>��w��o}���A�z	eu0�\��'������Nz�<�\.��;_�\,�^�|�r���n_�|�r��}�������G{:��^�]0ha/��1����w{����]0�b�z�w���}�����>���{��w�6��]0�d�z�w���}�m��T&J?z�۷{*��Khn�'���}�~�����}���Sa��>���0hhpb�Tt�8�{
�Z�\�==���
���7vO�AW��;�� �����=F}���
����}�������}�+Z���ݮ�z���ݮlx?�f˾ەM��탭|�+����nW����ݮ��K�`�؇:���nW8}|Iڄ��
��G��M�N|���.�g����M��s������6���v������蝽�a������a������a������a����	�a��`�	�a��`�	�a��`�	�a��`�	�a��`��a�{����P�����a�����a������`������`������`������`�{!3���7���w�������1@�����u`����u`����u`����u`����u`����u`��9m����m���]Y�?XNە��%&NۅE����]Y�
?XI�����]a�?`I�����]`�?`I�����]a�?`I����]a�_b��]`�?`I�M�;P/]�;P��6�8@���6�8@Y�?��q� 3~��� �Ak���
���w�t�@s|���0���{Ga�?���� ?~����A����;�D�C���[�����G~U-��6�SY�\$�K�*���m�ښ'j��E`�|�O�w��ҩ����ڔ1���:�mn����n`�-OEY�vy�z�8 ��S���a�Jd&��T�5q n��z��p�<ՆM���c�5O�5Oed ¶��l���ϊ��a��,��`��,O�`�����ըw~ֲ��Q�<M�����m"|�>O�E���y���7�Ϫ�1v�S��D���~Ր�S����M ���s&�]��y�6��p}r���>9�y*�:��6�����^���0j�O��@ w�'�m ���gA����6��i��^0ꨧڰ7.�Z�R�z��<��FM�Tԫ�E]�i�F�,j����ɢp_=U�M��Ӟ�|�4꬧R�ɢpk=UM�{�g����Ff�(�]OEK�E��z���,
��S��dQ����o�r��T5wCB-�)��),챧ک�2j�O�	=q�}�O���h�I�gOEYM�F{��hv�S�F��՞ʋ6����T��$l��Z��&a���Ea�=U���W��fQ�=��R��{*q���-�T�3�Q�}JO䉛��g"O�uO�Sy�{*�,
��S�Ն���*�6L���T��a*l��2�Sa�=�!m�
��,d�T�}OUK���{*"�0��S����W��ba>�C�u���S�8؃�Vo��&|�dY_�]�Th�����e���SUֺ'lħ�uO؉O%\랰�
j�=q/>�{�{�f|������T6���S�?��~|*$�c6�SU�Eu�S��˰%�
��X�=��>?��|*ڠv�S-۞��-�*�6}�T��ļ2��0`g>�!m�[��c"ܛOUKa��|��w�S��D�=?͇Wg���� z��AU�nu�S���Sq���՛sèI?�b*#�K�j���]ԦO5�bXԧ�:?�P5�Sy�TFp�~v�VFp�>��Le��S]�TFp�>�Lew�S�TFp�>U�Le��S��TFp�>��Lew�Sy�TFp�>��Le���=r�25��=q��Q�~��/���=o��Q�~��/5��=m�ZԹ����F��T2ܻ����F��y�ҺZܽOQ����Ur�aؿO�S��~*P�P�G^:7[���e��=�T�5��&~*�p�G?�rM������e&`���O�_��F~*�ِw�S��Ƭ���jq6hŽ�TW6an����ֽﰛ���&����T�3q�秊��{pC?�M�������&v���O%D����r��^4�S�ф�?���zn��m�T/2쏾~�g�7�S}Ɇ�����6�ŭ�T��A,���k��yh����&-�0��ꪹhp{�*v�0���k��W�����勆�yOZ>�濚����I�{��|Ѱ�?����?���?��D��?yT�4l��{��E�N�\�4�V�g�P�����ٟ��&T���T4�*n��J��6p�?�m�����n���OF�p����ی��SAҦ����6��]�T!�In��r��:p�?U�lځ��<j���?O��ƭ�TM�/���T�����T{����T�����?�b����T3Q� �뚨� ��L��C���&j��@�65�����~� Pa�D?d ��l��: T�3Q�! �I���@��~��C) �`'C- �C1�U�a�@E����(�#޻� ��hҷ� TK7��E *W���$ ��M��C���&}�!
@�M���P�"�I�~�P=Ԥo?t�hhҷ� T?��)LL�� Tb���@���b7���Yo
����"a �\zc��7�Aa *szc���7�5�w_�
�y�^0��[0Ba *�c �Z]0Ba ��c ��^0Ba *�Fc (�    !��rH���4���zHs�5(�!��"a �-ҡ{�H����X���yP�J�����j鍁P������j��P�
�������P��������P�J�Ӿ:dI���v����|�x�X��\ͰH`�oo	���悑0���&#a�Y=3�Aa�Y6Ba ��c �r0Ba ��c �zs0Ba ��c ��i�EH��}ѰH����l(����"a �ww�& 0���H`�ƣ��0�,6�0�,���0�,L�0 %�������b��"f0Ba *"c ���}�ca�2��n	Pq�	Pm�>��0���:#a�Y�2�Aa�Yt5�Aa�Y�2�Aa�Y�5k���<���������V��E� T��E� ��v�H`և�uP`��yP`֒�}P`���Aa �;��P��DDX�j�&"�� T"4�z����0 �MD�����>([����0@�Z������`$P�V�e �[����0 �4M�����i{u|*Ca��v�~�H����,�n���Ba��v�~�H�:N�	T�i�#a��8m?`$0KZ�>(P=���������0@���0����F� �sڮpC� �s�~�H�����"$P���,��S��>$@��fX$0����H`��n	̊�1
���
��1
P�0�0 U1
Pi1�0 U̓1
��8�Q	����s�H�F[��� 5ښ'���<�0@��扅j�5O,P�S��� 5>5O,P�S��� 5=5O,P�S��� 5=5O,P�S��� 5=5O,P�S��� 5=5O,P�S��� 5ٚ'���<�0�H�l��j�5O,P�S��� 5?5O,P�S��� 5?5O,P�S��� 5?5O,P�S��� 5?5O,P�S��� �<5O,P�S��� �D`a�Z�	�0@-�X��b,PK�� ���@ �"5O��0@��X�Vo,P��<F� �F`a�Z��0@��X��b,P�V�#a�:�]	��M��js&��� �y�Eaa�ڂɢ�0@]��n	T*��0@�#���F� �
/�>(P��b�� �����P�R���*^��P�R���*^��P��h�v,P{2w;�=���^�ݎ�jBO,P�zba���MBa���MBa���MBa���MBa���MBa���MBa���MBa���MBa��I��0@s�$FX��n#,м3�hޛ�4�D�X��'��� �?�'h>�,
4_�0
4_�0
4ߞ0
4ߟ0
���0
���0
��0
��0
4z�bX$�B�'-h�½Y$�B�'-h�=���z�x�H��V�1
����� -��=�0@��qO(�H���Z��=�0@�ź'h�Z��� -6�X�E��h��䇅Z�?�aa���O~X�%��h������䇅Zz~��� -=?�aa�F�]��P��k#(�H���i�f�CH��c�j`$��K70h�#�70h9���\Lƅ�hhA6,h3μY$�r7h�ٌ��Me�Le�Me��Le��me�Rle��O%�� �4[�� �t[�� ��?S�� ���0@���1
4���0@���1
��me�=��2	�=����0@ۃ�/	�=����0@ۃ�/	�qә� h{0�#a��S_0h{0�#a�F�1�A��Z+&N�!�Z5��a�6���� �u��h�.ט��0@�G��F,���1�m��C�Q���4�>2Vba�F_��X���c%h���X��Z�OЊ�:���V(Н7a�a�>}�a(�]4��a�\t�m��C��bc�� �U�������0@w݆�?��w6~�!н��a����ba����ba����ba�N#���X����硍��\}�X���a(������ }ͮ�`(���ꋆ� }O�>i,���ꋆ� }ϯ�h(�C~�,�Cy,�C}",�C{B,�Cb,У{�,��i`a��T���5!a��d��fX��7��� =����M8�0@�oƁ�zzS,�ӛs`a��ޤ��fX��7��� =�y�ب�TM��C��f��� =u5��ٙ��0@��F?�z6j�!�s�Q�a����~��<��!���Z��0@��s�� =?���<��!���b��0@/ϓ�� ��e��;$������ �$����%���0@_!�MCa�^�M�~t
���X��nӷ� ��8c%�T�1Vz(Щ�c��� �
:�J,Щ�c��� ���+�0@�Ŧo?�:)����6����n���Y�=��82�-Lς���?��2<?z���s��̋ݙN���p�IL,��/�-6z��˾N1�52�k�Nz��6t����cnC�kOz�����k��snC�w7=�s:�+��>t��+8�P�>_t��+�iz�m6�]F>5�`8\6�0=n4p�L<a�����pyԜ�m������Mw��lz��1tx]d��e����"�P+��F��Ȧ��nC��E6=w�Mw.�&=_w:|�Ȣ��nC�w7=�w��+��������KV'=_x�tq_�E���#�w�����^p߮}���j��"���a��a�'�߮}���d�]���Oʦٵ��b��ϥ�KߚX��h�̮}�ٮ�Ѕ]���]>�+��A�w��Z�Ʈ�t��n��kt|Wp���kt~W�^nڳkt}Wp�a��V�Aأ�3[8��c��|�j��~��p{�qÅ�cgܻ���t��ǹ�qp��c�98�򱏓��〃n߄���sp���&,:pp��㛰��q�A��o¢�]�\3����t��_�AՄٺ�co��V��c�X����b��$���NG�G8�W�1�+G����a���N��>�jWC�ħ�A�w9����8ݜ�cӅO჎v��|
t��'t�S���~�SS�S������^�qn��w7��>������� ���]�MG0��+����A�w7�G������M����?�i9Z��v�3�2��z��cs-G�A��͙t����ˇ{ϭ,r�t�p�E��z,��p�E��z��ý-G�A�w�V9Z��+���)�A�w7=��)���S��Ϯ��϶������aP�j�iP���hT�j��iP�,���o���o�ڻ/���o��o�k����AA`Ы���AA`г���xP�l�aP���aP�l�aP���aP�궹7��mn�Z�o�,�j��iX� `6�j��iX����aA�����:�o�j��h\�]{J�������60*x�ٵF���¨ �]��K��J���wY\[iT�k+�
�Uqm�QA��&�-4,x�ŵ�F�Ҩ ��V�Է3[�
�G�F��
������ �QA��"q��*�����QA��&q�Ҩ �}�8@iT��I�4*��%�}D��J���Q� �QA��$q�Ҩ �C�8@iT����c�GA�	Z�$hU|��UaT�S��� ��SXaT�1�)�0*������ �c�SXiT�1�)�4.�X�V&Q-�
>69��F���J���ONNa�aA�'/��Ҩ �S�SXiT�)J��4*��$�P|��U7|ңUiT�I�V]nT�I�V�QA���/�F��hU|֣UiT�Y�V]nT�Y�V�QA�g=Z�F��hU|.�b)�
>ϳux*�x 0�y�^,*�<O֋E� _f5�bQ1��W�7�j    ��Z�	�R�/�p���˪\0*��
�� ��:��2�/�p°
�˪\0*��� �j ���������* ~
�߻�
 ���Ţ��י�_,J��l9�Y���:���Eɿ_O��F���+��`����R�F��o+�`�����Fy�o+�v�������Q�����/%������Q�����/�������a���lb�ܟ	������|j��,L��߮�,��{خ�,L�׫�ƹ~O��
�T�gve�a�����0��]Ya���Ʈ,0N�{gWVe���i0�$?8Ϯ�0�����7����}�+�2��ҾڕE	~��7����
_��!(��q���>��W��(���ծ0��w|�+�R��=_��{(�>�ծ0J샏|�+����_�
��>��W��(�������>x	<��9}�x
�R��%�e�!8��E	}�#aQ>B��AX�·��~�(�!I� 0J�C�1���!�F�|U"�Q&B��A`�ȇ�%b��!:��i|�^"�Qb��A`�ć%b��!&�F)|�|x�n�>D9<F	|�rx�2��=D9<F�{�rx*������T%�!��0��C��S���!��0��C��Sa���$���(o�H^$0J�C��爏�G�w��<=/��!���bQ��L�/��a���0J�C^i�	ô=䕶_0J�C^i���=䕶_0J�C^i���=䕶�0L�C^i���=䕶_0J�CYi���=���_0J�C�i���(me�����Pf�~�(mS
�fQ��L�/�������i������Fi{(+m�`������Fi{�+m�`������k�Q��J�/�������Q��J�/�������Q��J�O��a�ӟ��d��a�_,J�Ò�X���%/|�(mK^�bQ����	ô=ly�Fi{��������/��a�_0J�Ö>a���-/|�0m�����-/|�0m�a]��´��u�,L�{ZW��´}*��,L�{�W��!0m�u_������~�0m�}_�����ܾ�����}������¾�����}�0JۣK�j?`��G���~�(m���m��?���$�d���I�),Jۣ��SX�����,J���q�(m�>p� ,J���Y��G�$b���g�6����D��=�*��(m��I� 0Jۣ�1����Dô=/��(m�!H� 0J�c�1����D��=��y��6J�c(�	�����E��(m���Fi{]�"�Q����H`���ȇ���=� y�,3J�c��	�����E��=�,y��(m��H^$0J��lm��R������t�(i����BQ����O��q)^,��cZ��=����,��cZ��ɢt=����,��cZ��ɢd=���,��cZ��ɢT=����,��c^��ɢD=敨�,���Lծ-Fiz�@/e�1�,�DQ��L�O��1��DQ��J���Ez�+C?Y��Ǽ��E�y,+??Y��ǲ��E�y,+;?�%籬��dQn���O�汬��dQf���O%汬��`a^Kۮ[��=J�c��uEYy�n���()�s�����<ְ]WP��ǥx�0#�5��
��X3���(����
���X+���(����2��X;���(�ͱ�
�R��<���(�3q��%�q�Q�P��ǖ�m-(J��TܿP���V���}@Ixl�okaQ[��ZX������f���m-,L����Z���=�m-,L�{��ZX�}�ķ��0��okaa���>��ٟ�w���Q�yw��w��QP�w��l\(J�Ӝz�(�N.�sTP�t'�9*(ʹ�K|�
�R��2����wr��QaQ\�sTX�o'�����u>G�E�v��Qfa����sTX�k'����G�E�v�C aQ��<���(�N^�GaQ������(�N^�GaQ����¢;9�E)v
r<
�2��x��E	v
r<
����x��)��(,ʮS(�����ڄ(�m��uZ]B��봚�.��)���bQ���J�/e�)���a���J�/��)���Q���J�/e�)�,��Q���J�O��)�<��Q���J�/e�)�L��Q���J�/��i���n�d;��l_,ʶS���Ţt;��n_,ʷS���Ţ�;��p_;�2�V�}�(�Ni���r�W�}�(�Ny%����W�}�4J�S^i���;�w_0J�S^����;�y_0J�S^��	��;�?C,�l��|�ܷ++���T�veeQ���śE�w*a���(O%�+3�T���(O%�++�r�T
���(	O��++��p�T�@����ٕFyx��]Ya����ٕF�x��齁();ڕE����jW%��}�+���T_��!(O��ծ0��Sm|�+��T;_�
��<5�W��;�d[V��O�%Ҝ��G @r����,��IHm)�R����=����hW%�'>�FYyv�QZ�{�]a���^�i��_O�����x��<�������far>��1��|Dc���H�ƀ��=���a�>��1�����o����9�����G�>��1�,�<��1�4�<��p�^��o8Q/O�7���'�N��S|^s���W^���4��l�<�y*������|����E8a/A6O�Q�^B�yN�KH>/�9{	��E8i/���g�%T�ᴽ�F +h����.��e�O�,J�K|�8���^�`�^����i{�;m�`����_�`�^byMR�i{�;m�`����^s`�^b�Q�i{��5E��%=�!
0m/)�f(�������6J�KZi�Ţ����'�����������ɓ7�����k�L�K�)60m/i��������5���%�����_l`�^rz��i{��5���%������\_�k`�^r{��i���_3��?5�K;����R�3�NY���θ;eQ�^J<��Ei{)���	��R2ϺS���u�0J�K�<�Na����xН�(m/��;�a�^��1w
���ԇ��)���R�S�텇%����$â���$â���$â��Ƞ$�����d`���d`����d`���d`����d����"#����"����"����"�������9�����x´��������.�0m/��7���Uڝ80m/=�L�KO�� ����&�0J�K/�ƀ��ҫ�1ഽ��o8m/��N�K���G�>c�#m���H�G�7�?�����Ᏼ}dc�#m�1s8m�)s8m��E���5c}�ᴽ>�ϋp�^�<����D�ᴽ>��E8m�O�yN��S|^�����fˡ��f�\"�����t\�`.���	ng.���Q���y������m<�Ǚ4a��tP�]ָVGW���Ùai�y����7��@�!�P�AX��-xhr1��:�7�+����Ԃ˞sb��>�,\�
��3�ҝ���~F�z���rM�o(�g���3����9C@,ݘn�g^�����e��XK����K׷�X4e�Α�m��yy��.��"�?���9p;#@,]?j9��(:K�����8C=�k�� �5t��X�;�ΘK����k�
�������K���Y,�>Bj�}������+[Ι��W]��G����.�#��k� nG�����������Eo!AG��A�#�_]�!u������7�\C����<t����#��#�R��^�.�O���p=^7�`�x��+<v�����l���u��{�в��hz��=/���#�'���LWOG����Q�e�=
�����'tf�:��{���m�C�_�|��dG���N��x������<��5�x��e�5��GԈ�x��ǯ��;H��5t�����ǯ1v��Mg>~��\�!���x��-x���݃��Ǒ$2�C���o\�Y"�>�{{��C>Q����aY����Cj��aӁOTC׏(Y?r�����#J6��D5֎Q���'���ۂ�7��O��}f
�Lü7<��r�vܮ�oz��j-���.Wv��;�    Q�ezם=��q;R��n7tf:�o��5O7����]��9�	���Noƴh�`f������4|�?r�;�!i�����C����,����G�,�$9$���G�lZIC�w�lc'9$��Arh9$����~��2����m�C�!i������4_�2w�C����C�0�n!��-��1��#��rH:}ɦ�4t}�vM�C���$��C�;������4ty��rH��-��E���ׄ��mέ�м�*M~�J͛����g%ᙇ�ew5x<;��������T\�W�g���!/\6X�7���e�U��8{�q�1,\�X�'oG�e�5x�0d��|�||���痞o�i�?��]�?��\?̲}D����,�/��l�P��R>�r��;���G|<��l�>�k���j�ñ��:x�����+��K����1�_*N��痒���J��_In�s��*>>����;x�6x����贽��*�#`�x�6�������O�>�j���6^x�6x����0W����Y.m��R���ׇ�?�iv�����o��>�.�RCw�����Tqz�w?[�xX���K�/��	_?��K�����R�����~�1�D��K>>y���Ko��0��~��[:}��8i���[�0˦��G��ǇY��5>�f�.��^���<��k��_�.T
��8�W|m���k��a��#���q�C��Rc�������g����=V�����w.z������w��p����h�Q�'x���6.g���W��g�S�=~�Ƌ�-N��a��#�@�>�F�0����K`�Ow�Y/}�<��t������g�t:(M		i!���T����(��ų����Z�	�_�"�����z�7^��3�����k��aǃ��k��a�u�Y�����0 �l��Nv�.g�t�0˦#o�_ѵ|y�>�_�q�x-^>̲}y�x������k�>����Z<�q�4��k��aȃ���<=��@�ӽ��ׇ��4~~�O����tݾ>�M��O[�?_�\�Ĳ<���k�����f�>����0>�������G�Ì�G���1�;��}���Ð�)��)ѤG�~���4��XK[@qp�?��p����M~[p�񚆖^����SX-/����T��4�^��T���,]x��?c�6I�I�J��w�J7ji�n�u+"a��=9>zM�s�l�I�'�K7�~.�X8���[zd��D�k��6ȡw��M��A�k�L��ǷAM�c�.�:t����h"َ�R�����t��#o�����nG��G�Tz"����h����h���k�ћn�xhڗ����O�Ʀ����Ar������M"R��W�Ѝg�|t���YC4�mGVvd���`iuhn�H��#����e�Ǝ4�t/T�#-��ͣ���8R���Zi����4q��?:a-�T:~t&Zi���}�m|F�4�#�5|F�=�߸����4pz���i������3�����k����دN��4�GC�6v�3��&��3Ҹ�Oi���i菶�C�_���G�#���u<��������O<p�S����;X��O=c���;�t�S�Я�4�3�z��hڮ�O=C����g\����]��W]�.��g��Ý]����7��ڃ]#p������8Z�
�{��� �QZރgOGq�����"t�(��%���w���=*�.���Gs��vޕG4����Bb�?22�~�y������H��7��AG��O���EG�ڢ��r���?����w����F��r�)��$r�"瘺棁h[;��{Ct~����c�&aGnف�o�ߌ�_t
o�lx�z��9t�SO����,G�J�3���M�$����e�5����֠C9���h���U�ȏ~�C7���vh��S> ��|/$�մ�f�d'��<ӆ|/C`��(]�NJ/�%��(ݙ���ˎ��oG���z�?Og������t{�o=�^�����N�2����tz[��r�Qߴ��<�ˎ��y��g��(�>Bd;�eG��#��ˎ��#H6��5v|�vM�ˎ��$��ˎ��m�m쬗uMx��MNT��G��E��_H�_�����u���!,�<?���?Bd�9Q�n1��('���y��ml����j�$��r�*]�Ar�"'���m��������m�C79Q�.A�Yxk���HD���a�Z�f��Xp�U�6�{��G���Ѕ��i�ZݙB��h�Z�.K��r�в�:3�<-[����_!��ekU�~�m@�ek5tz[0�EK����m�Cs��x�y��'�Z��o���j��6�vd�Z���� ��Z���� �5Mj��5�m�CK����R�5Mj����:��Z�k�;�-�VC珐Z�l\�3���G�o:�����7nkw.�8�⁥Dg�������1�����5]Jt�N� 9���]?��e�.%:C���f�R�3��A�\ӥ�j��$�>��Q�b�Ǚv�՘M__��(�ٳǍ��v��Y�����zup�W����܀^��fX��M'�2�`X�΋�ك
��m�ՏM!�h���܁��o�x�ܒC��8��Pw+����%tP���6�a�9D\ߦX�/(����m��s��ķ)\�	j��E����Q��v�O������х\�o�����}��>v_8ǡ1\x���pNC�"��I��!��@����-��\A����J-�#�(�JK3�EZ �GEPg�>�5α����?��:%�)h`�����7�t�pdq4�YXʑ��Hg���s
�,,���h����#���� �44�Y�-�/�,P��U��@g�_4�YX�~�Pg������,��{�u�Y��3� �:��F�Pg����,ж} B����$�Y��/�,Ж�Wuh��:t~�Pg�N����,��| B��rF��4�Y�O$� �:d���,�>} B�����zGS��Y �E#�ڢ����}���Hga�J�EC��%,�uXY����񺡑��ҖL7�u��d�4�YX���Hg�N���th��F:�#�:�(��P�`���Hga�L���:t�FRPg�v蘽���}رy�,�1�O#�������t���`��@��C�,�3#���Y��(���:Kl���YXr�>����u�xk#���b�4�YX����Hga)������BY�䋅*��6�"��%;�8i,�줅����F
Kvr8	,�ƿ��0�WX����H^a�Nv#u�%;�,���8'�a����,nI+��碁����VX��.�����E�U�M6�(��
�GEPT�N��j*����rHR�����P�s	*ԇOCc7��@�NpA��"\A5:l��"(�@�YtQ�hC�.���}>�ETR g�%7~$��������}���PF��`�*
��Ti,�@���A5(��jh(�@���C��

���M��P@�Bx��	t�$�I,�P�K��܏7�x��C�:�ܢ�vm��-K'З\�a�:��@,�@�lt�u賎. �l��� VM�M<� Ģ	u���5ꪄ�0�L���C�ڍ���|��C׳�%��\Ha���)��@�]t!����.��VB=�W/J%�|�Bq�X)���HC���p0�IX{t�i,��>��h����;�$����4�H�mc;��P"�bs;��P!�Ζ�HCC���%9s?/��#K��Ucy���y7��^w�Pa}P.��6�:]�ai���� ����F�X��� uh��. �,B��4��t��u`Q���2�&��a\@aI�uCp����B
"��]Ha=�Z��4Ɔr�DRX�]Ha1�H�Bz�ń?�����R���`�����x�Xa}���Pa�Ow4�A���l�Bc�6�֪4A��<[��P�Ζ��*%֖����L�^Ｕ*��r���k�s����r��ց����.[    ��P��>��K�a] b�ڣ�@,|@Ar�ĺt��ecك�n�0T=�{|�CуsC���|E�r$,.����ڽ\ b��}D�
�3��r�; �F�X�6�����G�e?s�Z�|�H���v�P砝ɭ�E*+��"����׋�
k�7��(�苵,R7����W�Bmڀ�k�,R6��M�נ�A[�ėא�A[�/�>�,J��P�h��{�P�`�w�A5��y�A��(�w�A%��dPǀ>�xT1�:�A5hW�� �zOyY��U&�P�^p��E�kw��*���8P����xT-����Y@�U�8P��F�;p�^���J�&M��^��%�f�f�������4�z����Lg�`��R��0�P�r9w�֎z�pK%y�r�xO%u��`���=���J�P�p[�����2�W3�Q�f�ƚWzu�pg���8����\�ὕ�Ѹ�Û+��r!�wWCv!��Wj�r!�����y�n����ކ�;l����,�b˺��0�c�^�o���Ex����"��R��E�gI��E�hk^�Ý���F�E�0��Ջź04Vx8���D��>��*� >��´&�Si¬����Hf͚�7a�t��`$�f�9� =����S�!9����S�����s�)��`h�mHFZ04a9� �R047�(�J0k��"(�&�(�:0kv��"(�����C*04��-����Z�f�L�w�a$C�|�"� C�|��"( C�|��"��B�w��"������ETi��Bۡ�H������"헶�+���/�TW.)��S\�`$��Nm��P�����#ٗ~*+�T_�)�X� ї~�*�4_���%H�FͺE@��X�� _�.��0�{�����{���ET{�Q��E{�y��E�z����E�zY�q]A����^h��[1�y�y�n�P�f��`��B���8(�Bz��8��B���9(���#���H�� ���H��(������$�ɻ��W�^�E�.=��EY$��#_]���.=��Ea$�ң\]F�.=��E`(�ғ\]F�.=��Ea$�ғ\]��.=��Ea$�B�8�*��KO|uQ�=�wX��(�]z����HХ'��(��\z���ɹ�,W���K�ruQ���,W��r�9�A�0�r��$�݇�\������\z�?����s�Ra$��s�R����1���Ra$������Hå����H¥���z)�����T	��"���E�-���),�o�E6Of�zK/�y
��[�OJF�-���y`,�ҫn�#�^u�	�П�x�d������H��W�y*�T[z嚧�����5O���m!,�<�E�-�J�Sa��ҫ�<F�-�I�S=��Zz����H��7�y*��Zz���z���&5O��VKo\4S�!��޸h��CJ-�q�LY$�қ�F:-{������u�#]�f
#��ޥh�0i�]�f
#��ޥh�A-�K�Sa����q_|J=,Rh���OY����q�a�BK��>#����}
C��>�}F
-}���Z�`�)Z�`�)����30Rh高}�)��Qn�P����s�
-��.n�P�eE�8��2�͗�8��2�pJ#��AUrP�ePm؅ThT�t!Z�s�>�>��2�s��!���ϰH�e<|�)����>c7��2�}F
-#��g`��2�}F
-#��g<�ZF���H�e����$�-#���-H�e����H�e����H�ek�0gQ�`$0�fQ�`$0�fQ�`�0TfQ�`$0fQ�>��.ˈ��eYF�?��TYFl?�`(�2b��.	�4YF��%��$�H��uI<�YF����H�e�(]鱌��K�$�2R��.	��X�������q�FZ,#��[1�b��܂��HC�%0b���E�a9ȟ�F2,��..�
хa$��Bj�z��BJ��5|]i;˵ �A�1�E,#wN(���+#N(�E�+�<�P
��WF	�P2�Wa��H{e�S���Hze�S�t�Xye��ou0^��;�tWFi��,�dWF��X��2���"ѕQI(F�+�I(��Hre��iEPqeP��E\4�;;##��AE��`$�2j��R`��2V��[�ԸY��2v��#��AUDPieл+EPhe�IFPgeP�Ņ�YTu)��Hee��ow0Y4�;8i���Xl�{8,�X���Pce����Hce�G2J�����A2J���ʠ����Hce�$��Hce�[�x�Pce��oq0�X$���4VFo?�
��2z��E@����ϭj���HF)0�X#HF)FF+cD�(F+c��g`��2F��R��4V�(�Q
�4Vƨ�Q
�4V�h?�
��2F��E@��1�ϭi���y�'v+����@ce�|�h�L8I9N`��2�,�812�X�p�r��@ce�U�q��	��O�*�X���C��	��)0�X)O��S�4V&�(�PceҺ�
�4V&�[��HceҺ�25V&����HceҺ�
�4V&�Αi�����sˆ+e�톑�Jy��s��+��
=>r�(��&~d�&&�y�F+�β�
�?B��E�^16�X�t��Wh��2�&���Hce���5V&<~nPc�<�.p�Hce�Av`1�X�t�-Xh��2�${��Hce�Y6a���W��M3�\HA��IWن�F+�n�ݿ�d92�#�sY��ʄّ
#���dv��h�LX)4�X��8Ri��2iq��Hce��H���ʤőJ#��I�#�F+�G*=��JY�v�F+?�j��R�U��a��2��'���� ��|��k�|�rdI|�*�4V&��DU��Բ�i\ B��B�. ��ʤ��J#��I��D+�3Ra��R��g��Hce�rF����ʤ�Ti�LZ�H���ʤ�Tz|ɲH�3R���d�rF*�4V&-g���Â��5�c��	�֪0�X)O�U�4V&,[��PceҲ�*�4V&-[��HceҲ�
5V&-[��HceҲ�*�4V&-[��h�L��V57�X�0o�
#���t�ZF+�鲵*=�j���֪��_�i�Z�F+���U��R���U��4V&-[��HceҲ�*�4V&�9Yj�Lz�ܲ��Jy��F+?�h��2���i�L:�F�4�X�t�Xi��2�����#�]y#V���m�P��@��2�����Â���9��맹����3Y���d&�F��Lt��/�Lv��l���l^_�a�$�d�(�E���A"�� �m�{5,���"bY 0�=BĲ@���zxy�Lt���Z ]�C,
� &�G�X(L6�/Բ�m�툰ǆXGķ���X� L��oӲ�8�ia�� " �뻴,� (!R���~�4��~�P �(3/ Lt���'�׵���O��K�eA��d�Y4�O���e���پ�C����2dY��_B:N[�^���=Nc��O�8MQ��?Qv������4aQ��d�iʂ��ɲӔ-��e�	�:�'�NS4�O��i���_B>g�Z��O��i��n���,h��,�iʂ^����;nP��d�LSt�O��4eA��d�LSG��ɶ#2�iʂ6�Ɏ_������{����w/ ��O4�sJY��?�t�)5��l>甲����Sʂ����sN)��'��9���o�mG�KZ]#�������v}Š����!,��ldG�'��̢���fv������!,��leG0[@S�d۹0(z�'��PK�D��^��/a�R,
�'����O6��#,h�l�G���'���4�O���#,��l�GZ�'�΅AY��?������':~�jQ	�nb���'��t�O6�9%,h�l�sJ�z�'�����O��9%,���l�sJX��?���8��O���KYо?Y��P�[���/a�EDPм?Q��0�z�'*aA��d�"",�ܟ�\D�E��������o�r��OV."̢����EDXд?Y���'@�~�_D�b�e�|t�OT."����EDXЯ_VY��    �?Y����'+aA��d�""� ����r�1Z�';~�P�~����+@���{�O�Q�?fA��d��l0Х?�,���I�E�?fA��d��̂��69���C�]�?fA��d����1�������=ۣ��;��=*��'�ۣ��7��=2[�'�ۣ��3��=*�'�ۣ��/��=*��'˵GeAW~����k�)�����}�=�GEAK�D���,�ȟ,�����ڣ:��O�k�ʂv��r�QYЍ?Y�=�#@3�d���,��/1�2�:��O����b�����(hğ(���}���2��y����e,eA�d���,h,���=���2�:��O�k�ʂ�;���6�O���(꿟(;MP�~?��8MX�}?Yq���~��4aA��d�i��ɊӘE����	�'+N�ݗX�߽\�v?��W���'�bQ��D����2����>����~���?aA��d�~��v>����~���?aA�}���4qh��(�ib1�k?Q>����3MX�i?���ib0�h?Y9ӄ}���3MX�f?Y9ӄ]���3M��'+g����~nE�=�pP�b?Q�-��~��=

�'*ۣ���~��=2���'+ۣ���~��=
��'+�#���~��=
Z�'+ۣ���~n/�=��@c�Dy{낾����((h���l������(,h���l���S?Y��-����QX�Q?Y����~��=
�����pz ,h��l���E����{���~��w/�җ�q܋E���-���qA'�d+o�F��6�J�}�����
��';x+G�.�y�zx+4�O6��r�K)ꡟh<���O4�2�:�'���-�_aA��d+��Y�>?���t�O�����������y�zؿ�������s~��n������_�Q�7?��7
��'Z��t�O��Q)�M�m|T
z�'�����Ov�Q)���B��Q),h��l�RX�/?���c��O4���E���|�	��'[����^��V>����m|�	:�'���4�Ov��'�}�3�|�����@S�Y�%?ٸ��ظ�4�O4m�z�'���-�-��ʂ����`aQ��d�q���?~��8XY�?�q,,ꎟI�s�,h��l8V��O�jj2�?�s�)
:�'z5EAc�D�PS��O�55.h��,jʂ����,h��,jj\�?�H>Ԕ-��CMY�?�s���@C�Dϡ�(臟�9�E���C����7�~=�����xl������	O���8����;p`�~3Nh:;)n�O��Gfq|j�A
[>�Q������a���5��-��=m�a��������d����wz�{�I�a�;��?������^c�����^�Kƽ��6�^1n}O+7�X���v���������8�����!�����r�Cw�S?A�C7����x��y�7��9��N�
�*p�;�z����u�����;�
lw�������%j��7��G�w�^�����"��^��Q�;��Ay��7��縏ޟ.����Oق���sܧ,�r��O`��v�°�=?�>�a�{~�}���}
���}
����}
����'Y3����'Y6��=Ȳ��=��@����L�����L�p��Y��8�°�=��@�Ȱ�=��@�]��L�0ljߍ��`O;����[��5���No��C�����;�p?;=Tw�vvz?�(����n9�Q����l�������xGne�o����i���9��)lA��9��),�c���g�d��ӻݳy
���9��<�M���l��vz|6O�a;��?�����}�n6��k�{ɸ�z ���uzz~/w��,����y���p�w���;�p�:=�w���u�[�w���uz/��}���:�!���WK��?ܵNO��%�uz�}���ӳ�{��e�^�{��c�^��;�p�:�w��~uz���]�^��;�p�:�F�w��fuz�ý���vZ��~���[}?tw0�W��On7;��΃]@�:=�����Q�:�ަ���a�:=�sӨs����{Өw�ކ�'{Ѱ{�%�7{Ө� ��$�`���~ٰ��^<�U�.vj.���}�y�.�F����:�����Թ| �nvz2} �~vz�} vꋈ> aO;=ȏ> aW{�c�nG�������C��y
�a�۞Ϥ��F����
�͇���tӨ�=�aA7�z��tӨ�=�qA�kP�{>�n�|�r�`G�]H��ם^\������wx�����HC���<đJÞwzF|ih��N���#�����q�Ұ�)G:��wj�5�����u��wz �;�/ᜑ�F=���=$O�.xz]|D�>x�z>�`'<=��>�`/<�ݎ>�`7<�TDR���`s�D��a��_x��m�P~�+��$ٰ/���Q�3�ިR�7�^<GR�;���FRb�OO�)�!_v�����8tèG����.��K6��˩��4�/�hsӨW����E�n�r�67���˩��4�/�ps��̗S��������/���g?�9_v����'��9fh�=_N��Q�|9���ب����M��r*87����)�ܮA}���pnuҗ]Ĺ�z�ˮ��0�/��s�ӗSǹi�Q_N!�QO}9���ب���R�M���Z�9���F���sn�֗S͹����)��4�/E.;1��þ��(�z��C��]���eGi�g_�^v�F����eGh�k_�^v�F����eGi�o_�^v�����eGi�sOM~!����u��{zL�;�"�������>������R;������m}H�|z�}H�|z}H�.|��>�`>5���N|zd��{�O�/v�S�A����{�;��{�!{���t�!����n�!���|�!;��qv�!{�K�Zk>b؝_��ZF����9v�Ӄc��
�z��u��Z�F]���\j���}���Yj�B�N}��Z�ШW��K��iحO����*4�ק΄<�:��I|��D=�eh�U��x���V�Q�~��*4�ܧW����ݧ��мkP�>�叏�Q�>�!��Ө��6�@��O�� ����K�}��R��P'?u!�^���Z�Ш��޿��~~zx�w ;���o������1{u�ӻ��C
��Ӄ��C
v�� �������đ����Z�����u�Ҩß^��#��=�5�#�F]�5�#�F}�5�#����5�#�F��5�#�F��u������}[u��=*��Q�=��ou��3,��Q�=Ӣoנ��z�E�4���g^�M���zF߮A���L��i� @���B� 5��u@ j`�ˀ: �p>���J ����(��^_���j ��7���z �2?���� ��;���� ��8���� 5˟��j�a�P��K`�@ϔ���6��(��F� �Mzq4�XO����B �+p��4��Uj�?c)�t�����R�zM���j��X���[���^�y��h������������TXM > �n �9�> �r ��D�P;�:�@�@ړ�$����l� @�~�PC����E�"��Z�����z���*	���> �� =ۏ> �� ���> �� =4�> �� 5D�PS`�+�0�@7�T�s�0�����`�,P�?�`�-p�G;��^��h�/�^�7O#�z|�^WiP�{�i�2@O���J#�z��^W)PF�DZ�z�У��a�7��;|@AŁ�!�#
j��>��� 5�DRPw��b��F���=6O#��LI���� �6�؆���އ6T XM/>���z��#
����>���z��c
*л��c
j���X������i�G@��S�4R$h{���g��I������m���a�K����F�팞�h�M����F�팞�i�O���鋆
팞�i�Q����F*-�!i�t
蕿_T*�g�~P�`���
H�`5���z    �	�)�X@�%ч�,���C
�����C
�P�J�!�Z�C�i�ȇ���zA�|H���
g>�`В��F-�!ih�bВ��F:-�!i���Z�C��Hˠ%9$��Z�5�5{�O=��xoU+�ě���5�4N�s˲���,۫�Xנe�_���A˲�j�,;��Xݠe�b�Z�=��P�ѣ5�S�q�r���c����ϯ�����±�A+��e�C��V"���GP��������A+�wf�CŃF#�|8b̓F��>��A#Y`�X�����k�����_
�>h����`��Vo�ƊP����;���B���h�C�FE_X�� %_X�Q���VBh��6mp���*�TƧ����*���P�5v���"Bk�Rű&Bk�R��~���KuZ�*��Z�j#�&.58TGhM\jp��������P!��QK75ڞ�t�P%���K7uڙ�t�*%�3v�ơVB;��n�%�3|�6:�Khg�ҍ�;n�L75Z����&��g�����V|�*'�!g��"�NhC�R�C��6�,58�OhC�R�C�6�,5F�
m�Yjp��І���:
m����Y�TRh�7^CC-����kh����x�z
�������� �q�x5�#��XU�?���*�G6^�Ce����kp����x�١�Bx�54�W�7^CC��d�58�X�A6^�#��Ѓl��:=��kp��Ѓl���Pk��x�?�]d�58�[��sB���B_�/gv���i��t���6~~�Xw��(�%��Py��$�%������/t��pP�S�ȇ#V`�4�ɇ#�`�ˣ	š
C���W�F@(�`�i�2$�_�i��R�/��Gv)/��'v�.t��E��B'9�q�.t��/�-t�T,
�z�ú�tzڳ��Dz^cM����B�k���*�W�y51$�V�y�41(V�t���
�*t��]a%:U~���z
�d3^a�z��L����B'i�+���B/k��q�P�{ԓ1P�{ғ!��7m��=�ĠH:�Ӗr�
�M�g̓A�hB?S����dP$��ό'c���ψ'�"���'<Q�ޚ��gͷ�O�%߾�<]0���3��a���IO����~f=]t���l�iO����{�4.��3��a����OK����&+������a���5��a���5��a�����a���A��y{�#N.�x;��q�K���1'��a���ܱ@\��}�:�hX��Z}o��n��,��au�w�s���n��|��v���hX���{���NeR��۩J�B
�u;	���e�>/HɅ���!���_E�>đÚn�H�aI�u$Ӹ���x�}�#����>ԑL�rn�H�a5�u�а�;u�а�;��s�ƥ��ğ[5��'�ܢq!w<YNT�awPeʅ.㎧ʉ*4�⎧ɉ*4,⎧ˉ*Ɔ5�A<. q	w�GNT�aw9#ŀ��;������rF
˷#�)����oG�3RhX�A�H�a�v=#�ذr;���B���zF
�c	�{�܆k>�}w���5�coP�y�0�<L�tpg8��������ț΁����HGg��Ыwp��HG7����#o�<L����ˑ�N�~���jh�����jUK�ę;�i}�ں\߿q[{�js�x���3�;���������5i����� 9�:#���;H�������C�3���#H�k�:#o�>A�iZ�3w�&���tF:8[�\0��no�l��ۯ3��6ȡ����6�vM^g���� �^g���;���H����l��3���R�.�tt���Ȳ��9�����t<n/����x�M����v�yq������..�v�WB|92�N�Q*��Й����n7tc�y��ە���đqw�S=���������m��i8Hi�{z�-x������HvdM?��Z߿q���_F�x��'�1v|��m�Z�D5ty��C7>Q�?�d���'�1_��M>Q�?�d������m��
dR�y[����R��Gns�������~�y��>Q�>���V�D5t��6��D5���m��'�1v��M>Q]���Ʀ/ۇT�o�o�i��:ⱅ��i��pt���0�=̷Vw�����*4=?H�k��+��rk5tf�zZn��nLG���G�[�ҁ� �Zn��No��?�.�VC׷7=��j�����j̝�?r�{�����̷V��Y�rk5Ǝ1�i���|ɦ��j����!�Vc���C˭���m�cl����Ht}��*-6�����j��I��;a��8|6q�|k5p��j6,�Vc��#�Ƣ��j��Q��[���� �I˭���R��[�qM|�ԡ��j��6�vd�[���ۂ��[�q{x[p�}	�H��z0�5P�bA�`�k��ł��d�M�Q�`�{�������=�A�`�{���Qݠ��Mr0(Ե��`P5����0(LxMV��j�]�U/�&�f�^,�Lv�on#�����t���	����r���|��n�Z0�u��aP,��~��V0�=b�¨T0�5c�23�Lv�X�XP(�욱z��N0ٕ0�Fe�	���^���"��W�xàF0�,�%���+�0�Lx��7
�3V/_������5���&����Ձɖ�keAq`��}-0�L�����	w�� 20����S�� .0���V�&��
�����ϭ&�n�&0��sk@%��V>(�Ƞ"0���  0��� 0������S>(Հ	>(ŀ	G>(F��	��[*L6��"P%`���ր
��|P*� n|P*� �|P*� |P��A�>��RaP�p��RaP��y6"��0�s�4,( L��<�����S`��O�o������i`��O�o�
���|�40H�����i`��O�o������Ә���=7OÂ����iX��O�o��� �0�<r�	���� �0�<��@�?a�y$��|�40���<���87Ocf��s�4,L�ǹy���o���0�|�40���<��7O�<���x���ᛧ�Q��y%��ᛧ�5��ÚE�czH�w����͢?,ٛ�E9~x����0�Ͼ�\0��ó�.�r��쫋���C�W�F9~��r�(�a_].��a�Q�º�\,��� �Y���Ls��!���?�}u�`�ㇰ�.��P�⾺\0��C�W�F9~�{:��a����.3�?ĕ�],��C\y�Ţ?ĝ�]FF9~�;�`�ㇸ�F9~�;�`�ㇴ��#(�i�}�r��v�w�(�i�Ĺ|�r���Uփ���?��}mX��T���r�����0��a'�?�q|m`�㯁��a����r�@
���(�$`��r|j�v��9~���s���ϭ��!�sP#�?�N���!�sP���_��"�����(�9~ �E��U]\�?��s��9~(��s�P�ϭ����sP���vD0�e����(�TuqQs�@UE0����(�9~�|��_k�?T�y*�r�P��,��C��'�8�Un�
�?T�y*�r�P��)0��C����(�Mn�
�?4�y*�r����fF9~h|�T�����SY��&7O52��C����(�Mn�
�?4�y��P���<F9~�r�T�����S`���7O53��C盧�(��o�ʢ?t�y��Q���<F9~�r�T�����Sa��!7O����<�9�����0�r�T_������5�+�_�.f�Kb�Ba��{M,�����-����طˢ�>>��bX���g�Y,�r���+�eQj�}c�,���:Z.��>>�bQ���g]W,����;.㢬~?�Y���g�U,�r��U���1웊eQFþ�X%�1��|���|+ǳ�E�|+ų(��cX�EQ2�N�qQ.���,�R�vzgY��Ǹ�;ˢD>Ɲ�YG�<>Ɲ�Y��1��β(��q��FI|ܓ�&��?r���DY��qO��(���eX l  ���3!ʲ(�g>�eQ��t(���=��P�E�{<��,�r�x�BY��qO���E�{�3�,���'BY���̃��Ei{<Ӡ,���xfAY%��L��C9{<s�,�R�x�@Ye��̀2,L�� e͋����?Y��qO�C�z<��,�����¢\=ҵ����3��e���|�,J���dY���·�����·GAQ��EIz,r{<,��c�ۣ�(E�En�¢=V�=2t:E��#,��c�ۣ�(=�Un�¢�<V�=�yQr+�E�y�|{����Q��2�X��(,J�c�ۣ�(/�Mn�b0���&�GaQV���EI9}_����ƷG1/J�c�ۣ�(#��o����<6�=�qQ>���E�xlr{e���QX���.�Gq��c�ۣ�(�]n�¢L<v�=��Q";���4���T<vOi`�����)���E(n&��xJC��<�GTq��I�ؗ���i�����4L�GI\�aj>d<��ar>d<����C�S*�!�)�)��"C�ƆI���J�4==���+���� W�T==Q$ȕF�zz�(�+�����xJ�aO��F){zd<��(iO���T����">��F�{zt<��(uO��q�Q򞂎�T��)DWנ>�$��J�>O�4J�S���v�Ƨ�S�`YU�ȧ�3��R�x���Q2���PS���_��(�OQf���$k��aR���=���e���Qb���P34J�S�9���(�O����)�Q�?E�3j��R�D�Q0�_Ò}H�4?Q�ŇL�U\|H�T?Q�ŇL�S�9�J�t?��&on��5S��(�Ok�3J�S�/�4J��j��4J�]�|H��?QƇL�Ua|H��?QƇ, �̷ְ�%�,�,C�FE��e��¨���Z�ƅ����jhT
HEn��FŀT�֪4,�"�VC��@*rk54*	�"�VC��@*|k5�Fe�T��j`TH�o�F��T��j����ȭ�Ш<���Z�
�ʭ՘�R�[��Q� U���	HB(�����T��j̍J$�=�������\���Z��Q�`I��� ��t��`�`�g�� �H�>� ���F�> a�Թ�@X< �����������^���      �      x������ � �      �   �   x���Q��0D��a��`�]���XCS)��Xq��1d�V���������ue��_(X�̏��T	x٫����PǏ����dA�����nv�f[�����͍(�h*7��3Q7��~d�5��v�۹��r�b��칈s-���E�X4���ۀf�v*����c�n�3�Q�d?�F��$���$���$�Yj苗{�Q�����m����9V�h�U@^c��MH�ۄD��0x�?/l�&     