delete from txincubator_lot;
delete from txeggs_planning;
delete from txprogrammed_eggs;
delete from txeggs_movements;
delete from txbroilereviction;
delete from txbroilereviction_detail;
delete from txhousingway_lot;
delete from txhousingway_detail;
delete from txhousingway;
delete from txbroiler_lot;
delete from txbroiler;
delete from txbroiler_detail;
delete from txeggs_storage;
delete from txscenarioposturecurve;

update osshed set statusshed_id = 1;
update osshed set avaliable_date = null