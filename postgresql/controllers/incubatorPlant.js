const DBlayer = require("../models/incubatorPlant");
const utils = require("../../lib/utils");
const DBpartnership = require("../models/partnership");

exports.findIncPlantByPartnetship = async function(req, res) {
//   console.log("Aqui: ", req.body);
    try {
        let data  = await DBlayer.DBfindIncPlantByPartnetship(req.body.partnership_id);

        res.status(200).json({
            statusCode: 200,
            data: data
        });
    } catch (err) {
    //   console.log(err);
        res.status(500).send(err);
    }
};

exports.incubatorStatus = function(req, res) {
    let a = req.body.incubator_plant_id;
    console.log("que es lo que");
    console.log(req.body.incubator_plant_id);
 
    DBlayer.DBOptiDisp(req.body.incubator_plant_id).then(function(data) {
        console.log(data);
        res.status(200).json({
            statusCode: 200,
            data: data
        });
    })
        .catch(function(err) {
            console.log(err);
            res.status(500).send(err);
        });
};


exports.bulkAddIncubatorPlant2 = utils.wrap(async function(req, res) {
    const incubatorPlants = req.body.registers;
    const partnerships = await DBpartnership.DbKnowPartnership_id2(incubatorPlants);
    const errors = [];

    for (const incubatorPlant of incubatorPlants) {
        const partnershipMatch = partnerships.find(partnership => partnership.code === incubatorPlant.partnershipCode);
        if (partnershipMatch !== undefined) {
            incubatorPlant.partnership_id = partnershipMatch.partnership_id;
        }
        else {
            errors.push({object: incubatorPlant, message: `la empresa con el codigo: ${incubatorPlant.partnershipCode} no existe`});
        }

        const duplicatedIncubatorPlant = partnerships.find(p => p.code === incubatorPlant.partnershipCode && p.incplant_code === incubatorPlant.code)
        if (duplicatedIncubatorPlant !== undefined) {
            errors.push({object: incubatorPlant, message: `La combinacion de empresa: ${incubatorPlant.partnershipCode} y planta incubadora: ${incubatorPlant.code} ya existe`})
        }

    
    }

    if (errors.length > 0) {
        throw new Error(errors[0].message);
    }
    
    const result = await DBlayer.DBbulkAddIncubatorPlant(incubatorPlants);
    res.status(200).json({
        statusCode: 200,
        data: result
    });
});

exports.bulkAddIncubatorPlant = function(req, res) {
    let J = 0;
    let band = false;
    DBpartnership.DbKnowPartnership_id(req.body.registers).then(function (pa_id) {
        for (let index = 0; index < req.body.registers.length; index++) {
            while (J < pa_id.length && !band) {
                if (req.body.registers[index].partnership_id == pa_id[J].code) {
                    req.body.registers[index].partnership_id = pa_id[J].partnership_id;
                    band = true;
                }
                J++;
            }
            band = false;
            J = 0;
        }
        let incubatorPlants = req.body.registers;
        utils.cleanObjects(incubatorPlants);
        DBlayer.DBbulkAddIncubatorPlant(incubatorPlants).then(function(data){
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        }).catch(function(err){
            console.log(err);
            res.status(500).send(err);
        });
    }).catch(function (err) {
        console.log(err);
        res.status(500).send(err);
    });
   
};

exports.addIncubatorPlant = async function(req, res) {
//   console.log(req.body);
    try {
        let data  = await DBlayer.DBaddIncubatorPlant(req.body.name, req.body.code,
            req.body.description, req.body.partnership_id, req.body.max_storage,
            req.body.min_storage,req.body.acclimatized,req.body.suitable,req.body.expired);

        res.status(200).json({
            statusCode: 200,
            data: data
        });
    } catch (err) {
    //   console.log(err);
        res.status(500).send(err);
    }
};

exports.updateIncubatorPlant = function(req, res) {
    DBlayer.DBupdateIncubatorPlant(req.body.incubator_plant_id, req.body.name,
        req.body.code, req.body.description, req.body.max_storage,
        req.body.min_storage,req.body.acclimatized,req.body.suitable,req.body.expired)
        .then(function(data) {
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
        //   console.log(err);
            res.status(500).send(err);
        });
};


exports.deleteIncubatorPlant = function(req, res) {
    DBlayer.DBdeleteIncubatorPlant(req.body.incubator_plant_id)
        .then(function(data) {
            res.status(200).json({
                statusCode: 200,
                mgs: "Planta Eliminada"
            });
        })
        .catch(function(err) {
        //   console.log(err);
            res.status(500).send(err);
        });
};


exports.findIncubatorByPartnerships = function(req, res) {
 
    DBlayer.DBfindIncubatorByPartnerships( req.body.partnership_id)
    .then(function(data) {
      res.status(200).json({
        statusCode: 200,
        data: data,
        asd: "asd"
      });
    })
    .catch(function(err) {
      console.log(err);
      res.status(500).send(err);
    });
  };