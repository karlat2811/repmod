const DBlayer = require("../models/process");

function getAllProcess(req, res) {
    DBlayer.DBgetAllProcess()
        .then(function(data) {
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
            res.status(500).send(err);
        });
}

function getAllProcessJ(req, res) {
    DBlayer.DBgetAllProcessJ()
        .then(function(data) {
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
            res.status(500).send(err);
        });
}


function addProcess(req, res) {
    console.log(req.body);

    DBlayer.DBaddProcess(req.body)
        .then(function(data) {
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
            res.status(500).send(err);
            console.log(err);
        });
}

function updateProcess(req, res) {
    console.log(req.body);
    DBlayer.DBupdateProcess(req.body)
        .then(function(data) {
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
        // console.log(err);
            res.status(500).send(err);
        });
}

function deleteProcess(req, res) {
//   console.log("Eliminar: "+ req.body.process_id);
    DBlayer.DBdeleteProcess(req.body.process_id)
        .then(function(data) {
            res.status(200).json({
                statusCode: 200
            });
        })
        .catch(function(err) {
            res.status(500).send(err);
        });
}

function findProcessByStage(req, res) {
//   console.log("req.body.stage_id: ", req.body.stage_id);
    DBlayer.DBfindProcessByStage(req.body.stage_id)
        .then(function(data) {
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
            res.status(500).send(err);
        });
}

function findProcessBreedByStage(req, res) {
//   console.log("req.body.stage_id: ", req.body.stage_id);
    DBlayer.DBfindProcessBreedByStage(req.body.stage_id)
        .then(function(data) {
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
            res.status(500).send(err);
        });
}

function findProcessByStageBreed(req, res) {

    DBlayer.DBfindProcessByStageBreed(req.body.stage_id, req.body.breed_id)
        .then(function(data) {
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
            res.status(500).send(err);
        });
}

module.exports = {
    getAllProcess,
    getAllProcessJ,
    addProcess,
    updateProcess,
    deleteProcess,
    findProcessByStage,
    findProcessBreedByStage,
    findProcessByStageBreed
};
