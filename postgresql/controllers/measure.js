const DBlayer = require("../models/measure");

/*
Funcion GET que devuelve todos los measure
*/
exports.findAllMeasure = function(req, res) {
    DBlayer.DBfindAllMeasure()
        .then(function(data){
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
            res.status(500).send(err);
        });
};

exports.addMeasure = function(req, res) {
    DBlayer.DBaddMeasure(req.body.name, req.body.abbreviation, req.body.originvalue, req.body.valuekg, req.body.is_unit)
        .then(function(data) {
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
        //   console.log(err);
            res.status(500).send(err);
        });
};

exports.bulkAddMeasure = function(req, res) {
    //console.log(req.body.registers);
    let measures = req.body.registers;
    //utils.cleanObjects(measures);
    DBlayer.DBbulkAddMeasure(measures).then(function(data){
        res.status(200).json({
            statusCode: 200,
            data: data
        });
    }).catch(function(err){
        console.log(err);
        res.status(500).send(err);
    });
};

exports.updateMeasure = function(req, res) {
//   console.log(req.body.name, req.body.abbreviation, req.body.originvalue, req.body.valuekg, req.body.measure_id);
    DBlayer.DBupdateMeasure(req.body.name, req.body.abbreviation, req.body.originvalue, req.body.valuekg, req.body.measure_id, req.body.is_unit)
        .then(function(data) {
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
        //   console.log(err);
            res.status(500).send(err);
        });
};


exports.deleteMeasure = function(req, res) {
    DBlayer.DBdeleteMeasure(req.body.measure_id)
        .then(function(data) {
            res.status(200).json({
                statusCode: 200,
                mgs: "Medida Eliminada"
            });
        })
        .catch(function(err) {
        //   console.log(err);
            res.status(500).send(err);
        });
};


exports.changeName = function(req, res) {

    let name = req.body.name;
    let excep = req.body.diff;
    DBlayer.DBchechNameMeasure(name,excep)
        .then(function(data){
            // console.log('result: ', data)
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
            res.status(500).send(err);
        });
        
};

exports.changeAbrev = function(req, res) {
    let name = req.body.name;
    let excep = req.body.diff;
    // console.log('in:: ',name, excep )
    DBlayer.DBchechAbrevMeasure(name,excep)
        .then(function(data){
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
            res.status(500).send(err);
        });
};