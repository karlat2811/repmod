const DBlayer = require("../models/breed");


/*
Funcion GET que devuelve todas las razas
*/
exports.findAllBreedWP = function(req, res) {

    DBlayer.DBfindAllBreedWP("Plexus")
        .then(function(data) {
            console.log(data);
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
            console.log(err);
            res.status(500).send(err);
        });
};

exports.findAllBreed = function(req, res) {

    DBlayer.DBfindAllBreed()
        .then(function(data) {
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
            res.status(500).send(err);
        });
};

exports.addBreed = function(req, res) {
    console.log("llegue: "+ req);
    DBlayer.DBaddBreed(req.body.code, req.body.name)
        .then(function(data) {
            console.log(data);
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
            console.log(err);
            res.status(500).send(err);
        });
};

exports.updateBreed = function(req, res) {
    console.log(req.body.breed_id+" "+req.body.code+" "+req.body.type);
    DBlayer.DBupdateBreed(req.body.breed_id, req.body.code, req.body.name)
        .then(function(data) {
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
            console.log(err);
            res.status(500).send(err);
        });
};


exports.deleteBreed = function(req, res) {
    DBlayer.DBdeleteBreed(req.body.breed_id)
        .then(function(data) {
            res.status(200).json({
                statusCode: 200,
                mgs: "Raza Eliminada"
            });
        })
        .catch(function(err) {
            console.log(err);
            res.status(500).send(err);
        });
};

exports.findBreedByCode = function(req, res){
    DBlayer.DBfindBreedByCode(req.params.code)
        .then(function(data) {
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
            console.log(err);
            res.status(500).send(err);
        });
};


exports.findBreedByCurve = function(req, res){
    DBlayer.DBfindBreedByCurve()
        .then(function(data) {
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
            console.log(err);
            res.status(500).send(err);
        });
};



exports.changeName = function(req, res) {
    let name = req.body.name;
    let excep = req.body.diff;
    DBlayer.DBcheckNameBreed(name,excep)
        .then(function(data){
            // console.log('result: ', data)
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
            res.status(500).send(err);
        });
        
};

exports.changeCode = function(req, res) {
    let name = req.body.name;
    let excep = req.body.diff;
    // console.log('in:: ',name, excep )
    DBlayer.DBcheckCodeBreed(name,excep)
        .then(function(data){
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
            res.status(500).send(err);
        });

};

exports.isBeingUsed = function(req, res) {
    DBlayer.DBisBeingUsed(req.body.breed_id)
        .then(function(data) {
            res.status(200).json({
                statusCode: 200,
                data: data
            });
        })
        .catch(function(err) {
            console.log(err);
            res.status(500).send(err);
        });
};