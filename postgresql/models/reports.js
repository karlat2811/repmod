const config = require("../../config");
const conn = require("../db");







exports.DBfindLiftBreeding = function(init_date, end_date, breed_id) {

    console.log("modelo de reportes de levante y cria");
    console.log(init_date);
    console.log(end_date);
    console.log(breed_id);



    return conn.db.any(`SELECT a.scheduled_date, TO_CHAR(a.scheduled_date, 'DD/MM/YYYY') as scheduled_date, a.scheduled_quantity, 
                       TO_CHAR(a.execution_date, 'DD/MM/YYYY') as execution_date, a.execution_quantity, a.lot, 
                       c.name as farm_name, e.name as center_name, b.code as shed_name, 
					   f.name as executionfarm, g.name as executioncenter, h.code as executionshed,
                       a.execution_quantity - a.scheduled_quantity as diferentquantity, 
                       (a.execution_date::DATE - a.scheduled_date::DATE) as diferentday 
                       FROM public.txhousingway_detail a 
                       LEFT JOIN public.osshed b on a.shed_id = b.shed_id 
                       LEFT JOIN public.osfarm c on a.farm_id = c.farm_id 
					   LEFT JOIN oscenter e on a.center_id = e.center_id
					   LEFT JOIN osfarm f on a.executionfarm_id = f.farm_id
					   LEFT JOIN oscenter g on a.executioncenter_id = g.center_id
					   LEFT JOIN osshed h on a.executionshed_id = h.shed_id
                       LEFT JOIN public.txhousingway d on a.housing_way_id = d.housing_way_id 
                       WHERE a.scheduled_date BETWEEN $1 and $2 and d.breed_id = $3 and a.incubator_plant_id = 0 and a.programmed_disable is null or false`,
    [init_date, end_date, breed_id]);

};
exports.DBfindAllLiftBreeding = function(init_date, end_date) {

    console.log("modelo de reportes de levante y cria");
    console.log(init_date);
    console.log(end_date);



    return conn.db.any(`SELECT a.scheduled_date, TO_CHAR(a.scheduled_date, 'DD/MM/YYYY') as scheduled_date, a.scheduled_quantity, 
                       TO_CHAR(a.execution_date, 'DD/MM/YYYY') as execution_date, a.execution_quantity, a.lot, w.name as breed,  
                       c.name as farm_name, e.name as center_name, b.code as shed_name, 
					   f.name as executionfarm, g.name as executioncenter, h.code as executionshed,
                       a.execution_quantity - a.scheduled_quantity as diferentquantity, 
                       (a.execution_date::DATE - a.scheduled_date::DATE) as diferentday 
                       FROM public.txhousingway_detail a 
                       LEFT JOIN public.osshed b on a.shed_id = b.shed_id 
                       LEFT JOIN public.osfarm c on a.farm_id = c.farm_id 
					   LEFT JOIN oscenter e on a.center_id = e.center_id
					   LEFT JOIN osfarm f on a.executionfarm_id = f.farm_id
					   LEFT JOIN oscenter g on a.executioncenter_id = g.center_id
					   LEFT JOIN osshed h on a.executionshed_id = h.shed_id
                       LEFT JOIN public.txhousingway d on a.housing_way_id = d.housing_way_id 
                       LEFT JOIN mdbreed w on w.breed_id = d.breed_id
                       WHERE a.scheduled_date BETWEEN $1 and $2 and a.incubator_plant_id = 0 and a.programmed_disable is null or false`,
    [init_date, end_date]);

};

exports.DBfindBreeding = function(init_date, end_date, breed_id) {

    console.log("modelo de reportes de reproductora");
    console.log(init_date);
    console.log(end_date);
    console.log(breed_id);



    return conn.db.any(`SELECT a.scheduled_date, TO_CHAR(a.scheduled_date, 'DD/MM/YYYY') as scheduled_date, a.scheduled_quantity, 
                        TO_CHAR(a.execution_date, 'DD/MM/YYYY') as execution_date, a.execution_quantity, a.lot, 
                        c.name as farm_name, e.name as center_name, b.code as shed_name,   
                        f.name as executionfarm, g.name as executioncenter, h.code as executionshed,
                        a.execution_quantity - a.scheduled_quantity as diferentquantity, 
                        
                        (a.execution_date::DATE - a.scheduled_date::DATE) as diferentday 
                        FROM public.txhousingway_detail a 
                        LEFT JOIN txhousingway t on a.housing_way_id = t.housing_way_id
                        LEFT JOIN public.osshed b on a.shed_id = b.shed_id 
                        LEFT JOIN public.osfarm c on a.farm_id = c.farm_id 
                        LEFT JOIN oscenter e on a.center_id = e.center_id
                        LEFT JOIN osfarm f on a.executionfarm_id = f.farm_id
                        LEFT JOIN oscenter g on a.executioncenter_id = g.center_id
                        LEFT JOIN osshed h on a.executionshed_id = h.shed_id
                        WHERE a.scheduled_date BETWEEN $1 and $2 and t.breed_id = $3 and a.incubator_plant_id <> 0 and a.programmed_disable is null or false`, [init_date, end_date, breed_id]);            
};
exports.DBfindAllBreeding = function(init_date, end_date) {

    console.log("modelo de reportes de reproductora");
    console.log(init_date);
    console.log(end_date);



    return conn.db.any(`SELECT a.scheduled_date, TO_CHAR(a.scheduled_date, 'DD/MM/YYYY') as scheduled_date, a.scheduled_quantity, 
                        TO_CHAR(a.execution_date, 'DD/MM/YYYY') as execution_date, a.execution_quantity, a.lot, w.name as breed,  
                        c.name as farm_name, e.name as center_name, b.code as shed_name,   
                        f.name as executionfarm, g.name as executioncenter, h.code as executionshed,
                        a.execution_quantity - a.scheduled_quantity as diferentquantity, 
                        
                        (a.execution_date::DATE - a.scheduled_date::DATE) as diferentday 
                        FROM public.txhousingway_detail a 
                        LEFT JOIN txhousingway t on a.housing_way_id = t.housing_way_id
                        LEFT JOIN mdbreed w on w.breed_id = t.breed_id
                        LEFT JOIN public.osshed b on a.shed_id = b.shed_id 
                        LEFT JOIN public.osfarm c on a.farm_id = c.farm_id 
                        LEFT JOIN oscenter e on a.center_id = e.center_id
                        LEFT JOIN osfarm f on a.executionfarm_id = f.farm_id
                        LEFT JOIN oscenter g on a.executioncenter_id = g.center_id
                        LEFT JOIN osshed h on a.executionshed_id = h.shed_id
                        WHERE a.scheduled_date BETWEEN $1 and $2 and a.incubator_plant_id <> 0 and a.programmed_disable is null or false`, [init_date, end_date]);            
};


exports.DBfindBroiler = function(init_date, end_date, breed_id) {
    console.log("modelo de reportes de engorde");
    console.log(init_date);
    console.log(end_date);
    console.log(breed_id);

    return conn.db.any(`SELECT a.scheduled_date, TO_CHAR(a.scheduled_date, 'DD/MM/YYYY') as scheduled_date, a.scheduled_quantity, 
                      TO_CHAR(a.execution_date, 'DD/MM/YYYY') as execution_date, a.execution_quantity, 
                      c.name as farm_name, e.name as center_name, b.code as shed_name, a.lot, 
                      f.name as executionfarm, g.name as executioncenter, h.code as executionshed,
                      a.execution_quantity - a.scheduled_quantity as diferentquantity, 
                      (a.execution_date::DATE - a.scheduled_date::DATE) as diferentday 
                      FROM public.txbroiler_detail a 
                      LEFT JOIN public.osshed b on a.shed_id = b.shed_id 
                      LEFT JOIN public.osfarm c on a.farm_id = c.farm_id 
                      LEFT JOIN public.txbroiler d on a.broiler_id = d.broiler_id 
                      LEFT JOIN oscenter e on a.center_id = e.center_id
                      LEFT JOIN osfarm f on a.executionfarm_id = f.farm_id
                      LEFT JOIN oscenter g on a.executioncenter_id = g.center_id
                      LEFT JOIN osshed h on a.executionshed_id = h.shed_id
                      WHERE a.scheduled_date BETWEEN $1 and $2 and d.breed_id = $3 and a.programmed_disable is null or false`, [init_date, end_date, breed_id]);

};
exports.DBfindAllBroiler = function(init_date, end_date) {
    console.log("modelo de reportes de engorde");
    console.log(init_date);
    console.log(end_date);

    return conn.db.any(`SELECT a.scheduled_date, TO_CHAR(a.scheduled_date, 'DD/MM/YYYY') as scheduled_date, a.scheduled_quantity, 
                      TO_CHAR(a.execution_date, 'DD/MM/YYYY') as execution_date, a.execution_quantity, a.lot, w.name as breed, 
                      c.name as farm_name, e.name as center_name, b.code as shed_name, 
                      f.name as executionfarm, g.name as executioncenter, h.code as executionshed,
                      a.execution_quantity - a.scheduled_quantity as diferentquantity, 
                      (a.execution_date::DATE - a.scheduled_date::DATE) as diferentday 
                      FROM public.txbroiler_detail a 
                      LEFT JOIN public.osshed b on a.shed_id = b.shed_id 
                      LEFT JOIN public.osfarm c on a.farm_id = c.farm_id 
                      LEFT JOIN public.txbroiler d on a.broiler_id = d.broiler_id 
                      LEFT JOIN mdbreed w on w.breed_id = d.breed_id
                      LEFT JOIN oscenter e on a.center_id = e.center_id
                      LEFT JOIN osfarm f on a.executionfarm_id = f.farm_id
                      LEFT JOIN oscenter g on a.executioncenter_id = g.center_id
                      LEFT JOIN osshed h on a.executionshed_id = h.shed_id
                      WHERE a.scheduled_date BETWEEN $1 and $2 and a.programmed_disable is null or false`, [init_date, end_date]);

};



exports.DBfindBroilerEviction = function(init_date, end_date, breed_id) {
    console.log("modelo de reportes de desalojo");
    console.log(init_date);
    console.log(end_date);
    console.log(breed_id);

    return conn.db.any(`SELECT a.scheduled_date, TO_CHAR(a.scheduled_date, 'DD/MM/YYYY') as scheduled_date, a.scheduled_quantity, 
                       TO_CHAR(a.execution_date, 'DD/MM/YYYY') as execution_date, a.execution_quantity, a.lot, 
                       c.name as farm_name, e.name as center_name, b.code as shed_name,  
                       a.execution_quantity - a.scheduled_quantity as diferentquantity, 
                       (a.execution_date::DATE - a.scheduled_date::DATE) as diferentday 
                       FROM public.txbroilereviction_detail a 
                       LEFT JOIN public.osshed b on a.shed_id = b.shed_id 
                       LEFT JOIN public.osfarm c on a.farm_id = c.farm_id 
                       LEFT JOIN public.txbroilereviction d on a.broilereviction_id = d.broilereviction_id 
                       LEFT JOIN oscenter e on a.center_id = e.center_id
                       WHERE a.scheduled_date BETWEEN $1 and $2 and d.breed_id = $3 and a.programmed_disable is null or false`, [init_date, end_date, breed_id]);

};
exports.DBfindAllBroilerEviction = function(init_date, end_date) {
    console.log("modelo de reportes de desalojo");
    console.log(init_date);
    console.log(end_date);

    return conn.db.any(`SELECT a.scheduled_date, TO_CHAR(a.scheduled_date, 'DD/MM/YYYY') as scheduled_date, a.scheduled_quantity, 
                       TO_CHAR(a.execution_date, 'DD/MM/YYYY') as execution_date, a.execution_quantity, a.lot, w.name as breed, 
                       c.name as farm_name, e.name as center_name, b.code as shed_name,  
                       a.execution_quantity - a.scheduled_quantity as diferentquantity, 
                       (a.execution_date::DATE - a.scheduled_date::DATE) as diferentday 
                       FROM public.txbroilereviction_detail a 
                       LEFT JOIN public.osshed b on a.shed_id = b.shed_id 
                       LEFT JOIN public.osfarm c on a.farm_id = c.farm_id 
                       LEFT JOIN public.txbroilereviction d on a.broilereviction_id = d.broilereviction_id 
                       LEFT JOIN mdbreed w on w.breed_id = d.breed_id
                       LEFT JOIN oscenter e on a.center_id = e.center_id
                       WHERE a.scheduled_date BETWEEN $1 and $2 and a.programmed_disable is null or false`, [init_date, end_date]);

};



exports.DBFindIncubator = function(init_date, end_date, breed_id) {
    console.log("modelo de reportes de incubadora");
    console.log(init_date);
    console.log(end_date);
    console.log(breed_id);

    return conn.db.any("SELECT TO_CHAR(a.use_date, 'DD/MM/YYYY') as _date, " +
                        "a.lot_breed, b.name as incubator, " +
                        "a.eggs, a.execution_quantity, " +
                        "a.execution_quantity - a.eggs as diferentquantity " +
                        "FROM public.txprogrammed_eggs a "+
                        "LEFT JOIN public.osincubator b on a.incubator_id = b.incubator_id "+
                        "WHERE a.use_date BETWEEN $1 and $2 and a.breed_id = $3",
    [init_date, end_date, breed_id]);

};
exports.DBfindIncubatorNew = function(init_date, end_date, breed_id, lot) {
    console.log("modelo de reportes de incubadora");
    console.log(init_date);
    console.log(end_date);
    console.log(breed_id);
    return conn.db.any(`SELECT TO_CHAR(a.use_date, 'DD/MM/YYYY') as _date,
    a.lot_breed, b.name as incubator,
    a.eggs, a.execution_quantity, a.lot_incubator,
    a.execution_quantity - a.eggs as diferentquantity, b.capacity
    FROM public.txprogrammed_eggs a
    LEFT JOIN public.osincubator b on a.incubator_id = b.incubator_id
    WHERE a.use_date BETWEEN $1 and $2 and a.breed_id = $3 and a.lot_breed = $4`,
    [init_date, end_date, breed_id, lot]);

};


                  

exports.DBFindIncubatorAllBreed = function(init_date, end_date) {
    console.log("modelo de reportes de incubadora all breed_id");
    console.log(init_date);
    console.log(end_date);
    // console.log(breed_id)

    return conn.db.any("SELECT TO_CHAR(a.use_date, 'DD/MM/YYYY') as _date, " +
                        "a.lot_breed, b.name as incubator, " +
                        "a.eggs, a.execution_quantity, " +
                        "a.execution_quantity - a.eggs as diferentquantity " +
                        "FROM public.txprogrammed_eggs a "+
                        "LEFT JOIN public.osincubator b on a.incubator_id = b.incubator_id "+
                        "WHERE a.use_date BETWEEN $1 and $2",
    [init_date, end_date]);

};