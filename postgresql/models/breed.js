const config = require("../../config");
const conn = require("../db");

exports.DBfindAllBreed = function() {
    console.log("llego con plexus");
    return conn.db.any("SELECT * FROM public.mdbreed order by name ASC");
};

exports.DBfindAllBreedWP = function(raza) {
    console.log("llego sin plexus");
    return conn.db.any("SELECT * FROM public.mdbreed WHERE name != $1 order by name ASC", [raza] );
};

exports.DBaddBreed = function(code, name) {
    return conn.db.one("INSERT INTO public.mdbreed (code, name) VALUES ($1, $2) RETURNING breed_id", [code, name]);
};

exports.DBupdateBreed = function(breed_id, code, name) {
    return conn.db.none("UPDATE public.mdbreed SET code = $1, name = $2 "+
                        "WHERE breed_id = $3", [code, name, breed_id]);
};

exports.DBdeleteBreed = function(breed_id) {
    return conn.db.none("DELETE FROM public.mdbreed WHERE breed_id = $1",[breed_id]);
};

exports.DBfindBreedByCode = function(breed_id) {
    return conn.db.any("SELECT * FROM public.mdbreed WHERE breed_id = $1;", [breed_id]);

};


exports.DBfindBreedByCurve = function() {
    return conn.db.any("SELECT name, code, breed_id FROM mdbreed WHERE breed_id not in (SELECT breed_id FROM txposturecurve group by breed_id)");

};

exports.DbKnowBreedid = function(register) {
    let string = "SELECT name,breed_id FROM public.mdbreed WHERE name = ";
    var index = 0;
    while(index < register.length){
        if (index == 0) {
            string = string +  "'"  +register[index].breedName +  "'" ;
        }else{
            string = string +" or " + "name = "+ "'" + register[index].breedName + "'";
        }  
        index++;
    }
    return conn.db.any(string);
};

exports.DBcheckNameBreed = function(name, excep) {
    console.log("DBcheckNameBreed", name, excep);
    return conn.db.manyOrNone(	"SELECT name, code FROM public.mdbreed "+
    							" WHERE mdbreed.name = $1 and  mdbreed.name <> $2 limit 1",
    							[name.trim(), excep.trim()]);
};

exports.DBcheckCodeBreed = function(name, excep) {
    console.log("DBcheckCodeBreed", name, excep);
    return conn.db.manyOrNone(	"SELECT name, code FROM public.mdbreed "+
    							" WHERE mdbreed.code = $1 and  mdbreed.code <> $2 limit 1",
    							[name.trim(), excep.trim()]);
};

exports.DBisBeingUsed = function(breed_id) {
    return conn.db.one(`SELECT ((SELECT DISTINCT CASE WHEN b.housing_way_id IS NOT NULL THEN TRUE ELSE FALSE END
                            FROM public.mdbreed a
                            LEFT JOIN txhousingway b on b.breed_id = a.breed_id
                            WHERE a.breed_id=$1)
                        OR (SELECT DISTINCT CASE WHEN b.broiler_id IS NOT NULL THEN TRUE ELSE FALSE END
                            FROM public.mdbreed a
                            LEFT JOIN txbroiler b on b.breed_id = a.breed_id
                        WHERE a.breed_id=$1)
                        OR (SELECT DISTINCT CASE WHEN b.broilereviction_id IS NOT NULL THEN TRUE ELSE FALSE END
                            FROM public.mdbreed a
                            LEFT JOIN txbroilereviction b on b.breed_id = a.breed_id 
                        WHERE a.breed_id=$1)
                        OR (SELECT DISTINCT CASE WHEN b.process_id IS NOT NULL THEN TRUE ELSE FALSE END
                            FROM public.mdbreed a
                            LEFT JOIN mdprocess b on b.breed_id = a.breed_id
                            WHERE a.breed_id = $1)
                        OR (SELECT DISTINCT CASE WHEN b.shed_id IS NOT NULL THEN TRUE ELSE FALSE END
                            FROM public.mdbreed a
                            LEFT JOIN osshed b on b.breed_id = a.breed_id
                        WHERE a.breed_id = $1)) as used`,[breed_id]);
};