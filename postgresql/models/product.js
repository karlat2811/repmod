const config = require("../../config");
const conn = require("../db");

/*
Funcion que añade un dia feriado a un calendario
*/
exports.DBfindAllProduct = function() {
    return conn.db.any("SELECT * FROM public.mdproduct order by name ASC");
};

exports.DBaddProduct = function(product) {
    return conn.db.one("INSERT INTO public.mdproduct (code, name) VALUES ($1, $2) RETURNING product_id", [product.code, product.name]);
};

exports.DBbulkAddProduct = function(products) {
    cs = conn.pgp.helpers.ColumnSet(["code", "name"], {table: "mdproduct", schema: "public"});
    return conn.db.none(conn.pgp.helpers.insert(products, cs));
};

exports.DBupdateProduct = function(product) {
    return conn.db.none("UPDATE public.mdproduct SET name = $1, code = $2 WHERE product_id = $3", [product.name, product.code, product.product_id]);
};

exports.DBdeleteProduct = function(product_id) {
    return conn.db.none("DELETE FROM public.mdproduct WHERE product_id = $1", [product_id]);
};


exports.DBcheckNameProduct = function(name, excep) {
    console.log("DBcheckNameProduct", name, excep);
    return conn.db.manyOrNone(	"SELECT name, code FROM public.mdproduct "+
    							" WHERE mdproduct.name = $1 and  mdproduct.name <> $2 limit 1",
    							[name.trim(), excep.trim()]);
};

exports.DBcheckCodeProduct = function(name, excep) {
    console.log("DBcheckCodeBreed", name, excep);
    return conn.db.manyOrNone(	"SELECT name, code FROM public.mdproduct "+
    							" WHERE mdproduct.code = $1 and  mdproduct.code <> $2 limit 1",
    							[name.trim(), excep.trim()]);
};

exports.DBisBeingUsed = function(product_id) {
    console.log("model ", product_id);
    return conn.db.one(`SELECT ((SELECT DISTINCT CASE WHEN b.process_id IS NOT NULL THEN TRUE ELSE FALSE END
                        FROM public.mdproduct a
                        LEFT JOIN mdprocess b on b.product_id = a.product_id
                        WHERE a.product_id = $1)) as used `,[product_id]);
};